theory tmp
imports
    Pi_pmf
begin

lemma sum_exp_le_integral_exp:
  assumes  "a > 0" "k \<ge> 0" "n \<ge> 0" "i > 0"
  shows "(\<Sum>i \<in> {a+k..a+k+n}. exp i) \<le> integral {k..a+k+n} exp"
proof -
  
  have "i + i = 2* i"

  have "integral {k..a+k+n} exp = exp (a+k+n) - exp k"

qed


end