

# Forkable_Strings

(All sections follow https://eprint.iacr.org/2016/889.pdf unless stated otherwise.)
(Update 18/09/2019)

This repo contains a short and incomplete Isabelle proof script for Section 4 (specifically from subsection 4.3): Analysis of the Static Stake Protocol in Aggelos Kiayias et al's Ouroboros: A Provably Secure Proof-of-Stake Blockchain Protocol (https://eprint.iacr.org/2016/889.pdf)

I separated Ouro.thy into two theory files, as it has grown too big to easily handle in one file. The first one (Ouro) focuses on basic properties of forkable strings up until defining margin and its behaviors (properties), while the second one (Ouro2) focusing on using results from Ouro theory proving common prefix.

### Ouro.thy
Ouro.thy file contains definitions, lemmas, and theorems related to Subsection 4.3: The Fork Abstraction and Subsection 4.6: Common Prefix as listed:

- Definitions 4.1 - 4.14, 4.23, 4.25 - 4.26,

- Proposition 4.27, and

- Lemma 4.28:

### Ouro2.thy
Ouro2.thy file contains defintions, lemmas, and theorems after Lemma 4.28 (and mainly the ones related to proving Theorems 4.30) as listed:

- Definition 4.15, 4.29

- Theorem 4.30 is very tedious one to be formalised; it is actually the longest one by far in the category of formalisation of discrete struture proofs. 

- [In process] Intead of formalising Theorem 4.24, I follow the first bound of this paper "Forkable Strings are Rare" (https://www.semanticscholar.org/paper/Forkable-Strings-are-Rare-Russell-Moore/25113e52b9bfb6e723254eaa79ef8c91109be51e, as it has better and more straightforward results.).

To deal with sequence of independent random variables, I used Pi_pmf.thy written by Manuel Eberl. The plan to finish formalising this result is to assume Azuma-Hoeffding's Inequality in Isabelle proof assistant as there is no formalisation of it in the Archive of formal proofs yet (Manuel Eberl is working on it). 
We formalise supermartingale for discrete random variables using Isabelle probability mass function theory.

- [In process] Theorem 4.31 needs Theorem 4.24 to prove, so it is also unfinised.


## Future Plan
Improving formal proof for probabilistic proofs as it cound and should be done in a simpler manner. This will be helpful to finishing Common Prefix formal proof and then continue on to that of Chain Growth and Chain Quality.
