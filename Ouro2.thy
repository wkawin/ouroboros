(*  Title:      Ouro.thy
    Author:     Kawin Worrasangasilpa, University of Cambridge
    Author:     Lawrence C. Paulson, University of Cambridge
*)

theory Ouro2
  imports
  Ouro               
"HOL-Library.Landau_Symbols"
begin
                      
context
  fixes \<epsilon>::real  assumes ep_gt_0 : "0 < \<epsilon>" and ep_le_1: "\<epsilon> \<le> 1"  
  fixes \<alpha>::real  assumes \<alpha>:"\<alpha> = (1 + \<epsilon>)/(2*\<epsilon>)"    
begin

inductive viable :: "rtree \<Rightarrow> bool list \<Rightarrow> nat list \<Rightarrow> bool" where
  "\<lbrakk>F \<turnstile> w ; t \<in> tines F; 
\<And>h. (h \<in> honest_indices w \<and> h \<le> last (trace_tine F t)) \<Longrightarrow> depth F h \<le> length t\<rbrakk> 
\<Longrightarrow> viable F w t"  
  
fun longest_common_prefix_tines :: "nat list \<Rightarrow> nat list \<Rightarrow> nat list"
  where 
  "longest_common_prefix_tines [] _ = []" 
| "longest_common_prefix_tines _ [] = []"
| "longest_common_prefix_tines (h1#tl1) (h2#tl2) = 
  (if h1 = h2 then h1#(longest_common_prefix_tines tl1 tl2) else [])"

lemma longest_common_prefix_size_le:"size (longest_common_prefix_tines t1 t2) \<le> size t1"
proof (induction t1 arbitrary: t2)
  case Nil
  then show ?case by simp
next
  case (Cons h1 t1)
  then show ?case proof (cases t2)
case Nil
  then show ?thesis
    by simp 
next
case (Cons h2 t2)
  then show ?thesis
    by (simp add: Cons.IH)
qed
qed

lemma longest_common_prefix_tines_commute :
"longest_common_prefix_tines t1 t2 = longest_common_prefix_tines t2 t1"
proof (induction t1 arbitrary: t2)
case Nil
then show ?case
  by (metis (no_types, hide_lams) list.distinct(1) longest_common_prefix_tines.elims) 
next
  case (Cons h t)
  then show ?case 
  proof (induction t2)
    case Nil
then show ?case by simp
next
  case (Cons h' t')
  have "longest_common_prefix_tines t t' = 
        longest_common_prefix_tines t' t"
    by (simp add: Cons.prems)
  then show ?case
    by simp 
qed
qed

lemma longest_common_prefix_tines_self:
  "longest_common_prefix_tines t t =t"
proof (induction t)
  case Nil
  then show ?case
    by simp 
next
  case (Cons k t)
  then show ?case
    by simp
qed

definition lb :: "rtree \<Rightarrow> nat list \<Rightarrow> nat" where 
  "lb F t = (trace_tine F t) ! (size t)"

lemma lb_in_F:
  assumes "tinesP F t"
  shows "lb F t \<in># mset_of_tree F"
    using assms lb_def tines_exist_last_label_exist by auto

lemma lb_in_F_last_trace_tine:
  assumes "tinesP F t"
  shows "lb F t = last (trace_tine F t)"
  by (metis Suc_length_conv assms last_is_nth_size lb_def suc_tine_size_eq_label_list)


lemma lb_limit:
  assumes "tinesP F t" "F \<turnstile> w"
  shows "lb F t \<le> size w"
  by (metis (full_types) assms(1) assms(2) fork_imp_trace_tine_label_le_size lb_def lessI nth_mem suc_tine_size_eq_label_list)

definition divs :: "rtree \<Rightarrow>  nat list \<Rightarrow> nat list \<Rightarrow> nat" 
  where "divs F t1 t2 \<equiv> (if lb F t1 \<le> lb F t2 then size t1 - size (longest_common_prefix_tines t1 t2) else 0)"

definition div_tines :: "rtree \<Rightarrow> nat list \<Rightarrow> nat list \<Rightarrow> nat"
  where 
    "div_tines F t1 t2 \<equiv> max (divs F t1 t2) (divs F t2 t1)"    

lemma div_tines_commutative: 
  "div_tines F t1 t2 = div_tines F t2 t1"
  by (simp add: div_tines_def max.commute)

lemma viable_bound:
  assumes "viable F w t"
  shows "size t \<le> height F"
proof -   
  have "t \<in> tines F"
    using viable.simps
    using assms by blast
  thus"size t \<le> height F"
    using height_def
    using size_tine_le_height by blast 
qed

lemma div_tines_upperbound:
  assumes "viable F w t1" "viable F w t2" "F\<turnstile>w"
  shows "div_tines F t1 t2 \<le> height F"
proof - 
  have "div_tines F t1 t2 = max (divs F t1 t2) (divs F t2 t1)"
    using div_tines_def
    by simp
  hence "div_tines F t1 t2 \<le> max (size t1) (size t2)"
    using divs_def
    using less_imp_le by auto
  thus ?thesis using viable_bound assms
    by (metis dual_order.trans max_def)
qed

definition div_f :: "rtree \<Rightarrow> bool list \<Rightarrow> nat" where
  "div_f F w \<equiv> Max (\<Union>t1 \<in> tines F. 
 (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2}))"

lemma union_tines_set_finite:
assumes "\<And> t1. t1 \<in> tines F \<Longrightarrow> finite (f t1)"  
shows "finite (\<Union>t1 \<in> tines F. f t1)"
  using tines_finite
  using assms by blast

lemma unique_div_tines:"\<And>t1 t2. t1 \<in> tines F \<and> t2 \<in> tines F \<Longrightarrow> 
card {x. x = div_tines F t1 t2 \<and> viable F w t1 \<and> viable F w t2} \<le> 1"
proof - 
  fix t1 t2 
  assume "t1 \<in> tines F \<and> t2 \<in> tines F"
  obtain y where y:"y = div_tines F t1 t2"
    by simp
  thus "card {x. x = div_tines F t1 t2 \<and> viable F w t1 \<and> viable F w t2} \<le> 1"
  proof (cases "viable F w t1 \<and> viable F w t2")
    case True
    have "{x. x = div_tines F t1 t2 \<and> viable F w t1 \<and> viable F w t2}= 
{x. x = div_tines F t1 t2 \<and> (viable F w t1 \<and> viable F w t2)}"
    by simp
  hence "{x. x = div_tines F t1 t2 \<and> viable F w t1 \<and> viable F w t2}= 
{x. x = div_tines F t1 t2}"
    using \<open>viable F w t1 \<and> viable F w t2\<close> by simp
 hence "{x. x = div_tines F t1 t2 \<and> viable F w t1 \<and> viable F w t2}= 
{y}"
    using y by simp
    then show ?thesis
      by simp 
  next
    case False
have "{x. x = div_tines F t1 t2 \<and> viable F w t1 \<and> viable F w t2}= 
{x. x = div_tines F t1 t2 \<and> (viable F w t1 \<and> viable F w t2)}"
    by simp
  hence "{x. x = div_tines F t1 t2 \<and> viable F w t1 \<and> viable F w t2}= 
{x. x = div_tines F t1 t2 \<and> False}"
    using False by simp
  hence "{x. x = div_tines F t1 t2 \<and> viable F w t1 \<and> viable F w t2}= {}"
    by blast
    then show ?thesis
      by (simp add: \<open>{x. x = div_tines F t1 t2 \<and> viable F w t1 \<and> viable F w t2} = {}\<close>) 
  qed
qed    

lemma finite_div_tines:"\<And>t1 t2. t1 \<in> tines F \<and> t2 \<in> tines F \<Longrightarrow> 
finite {x. x = div_tines F t1 t2 \<and> viable F w t1 \<and> viable F w t2}"
  using unique_div_tines by simp

lemma finite_div_tines_set :"finite (\<Union>t1 \<in> tines F. 
 (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2}))"
proof- 
  have "\<And>t1. t1 \<in> tines F \<Longrightarrow>
finite (\<Union>t2 \<in> tines F. (\<lambda>t. {x. x = div_tines F t1 t \<and> viable F w t1 \<and> viable F w t}) t2)"
  using finite_div_tines union_tines_set_finite
proof - 
  fix t1 
  assume "t1 \<in> tines F"
  have "(\<And>t2. t2 \<in> tines F \<Longrightarrow> 
finite ((\<lambda>t. {x. x = div_tines F t1 t \<and> viable F w t1 \<and> viable F w t}) t2))"
    by auto
  thus "finite (\<Union>t2 \<in> tines F. (\<lambda>t. {x. x = div_tines F t1 t \<and> viable F w t1 \<and> viable F w t}) t2)"
    by (simp add: union_tines_set_finite)
qed
  hence "\<And>t1. t1 \<in> tines F \<Longrightarrow>
finite ((\<lambda>t'. (\<Union>t2 \<in> tines F. (\<lambda>t. {x. x = div_tines F t' t \<and> viable F w t' \<and> viable F w t}) t2)) t1)"
    by simp
  hence "finite  (\<Union>t1 \<in> tines F. ((\<lambda>t'. (\<Union>t2 \<in> tines F. (\<lambda>t. {x. x = div_tines F t' t \<and> viable F w t' \<and> viable F w t}) t2)) t1))"
    by (simp add: union_tines_set_finite)
  thus "finite (\<Union>t1 \<in> tines F. 
 (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2}))"
    by simp
qed

lemma div_tines_le_div_f:
  assumes "t1 \<in> tines F" "t2 \<in> tines F" "viable F w t1" "viable F w t2"
  shows "div_tines F t1 t2 \<le> div_f F w"
proof - 
  have "div_tines F t1 t2  \<in> (\<Union>t1 \<in> tines F. 
 (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2}))"
    using assms by auto
  hence "div_tines F t1 t2 \<le> Max (\<Union>t1 \<in> tines F. 
 (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2}))"
    using finite_div_tines_set
    by auto
  thus ?thesis
    by (simp add: div_f_def) 
qed

lemma empty_viable:
  assumes "F \<turnstile> w"
  shows "viable F w []"
proof -
  have 1:"[] \<in> tines F"
    by (simp add: tinesP.Nil tines_def)
  have 2:"\<And>h. 
(h \<in> honest_indices w \<and> h \<le> last 
(trace_tine F [])) \<Longrightarrow> depth F h \<le> length []"
    by (metis (no_types, lifting) assms diff_is_0_eq' diff_zero fork_F fork_root0 forks_def gr_implies_not0 last_ConsL list.case(1) not_le_imp_less rtree.exhaust_sel trace_tine.simps)
  thus ?thesis using 1 2 assms
    using viable.intros by blast
qed

lemma nonempty_div_tines_set: 
  assumes "F \<turnstile> w"
  shows "(\<Union>t1 \<in> tines F. (\<Union>t2 \<in> tines F.
{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})) \<noteq> {}"
proof - 
  have "div_tines F [] [] = 0"
    by (simp add: div_tines_def divs_def)
  have "{x. viable F w [] \<and> viable F w [] \<and>
        x = div_tines F [] []} = {x. viable F w [] \<and> viable F w [] \<and>
        x = 0} "
    by (simp add: \<open>div_tines F [] [] = 0\<close>)
  hence "{x. viable F w [] \<and> viable F w [] \<and>
        x = div_tines F [] []} = {x. viable F w [] \<and> x = 0} "
    by simp
hence "{x. viable F w [] \<and> viable F w [] \<and>
        x = div_tines F [] []} = {x. True \<and> x = 0} "
  using empty_viable
  by (simp add: assms)
hence "{x. viable F w [] \<and> viable F w [] \<and>
        x = div_tines F [] []} = {0} "
  by simp
  hence "0 \<in> (\<Union>t2 \<in> tines F.
{x. viable F w [] \<and> viable F w t2 \<and> x = div_tines F [] t2})"
    by (smt UN_iff \<open>div_tines F [] [] = 0\<close> assms empty_viable mem_Collect_eq viable.cases)
hence "0 \<in> (\<Union>t1 \<in> tines F.(\<Union>t2 \<in> tines F.
{x. viable F w [] \<and> viable F w t2 \<and> x = div_tines F [] t2}))"
  by (smt UN_iff \<open>div_tines F [] [] = 0\<close> assms empty_viable mem_Collect_eq viable.cases)
  thus ?thesis
    by blast
qed

lemma div_f_upperbound_forks:
  assumes  "F \<turnstile> w"
  shows "div_f F w \<le> height F"
proof -
  have "\<forall>t1 \<in> tines F. \<forall>t2 \<in> tines F.
 viable F w t1 \<and> viable F w t2 \<longrightarrow>
 div_tines F t1 t2 \<le> height F"
    using div_tines_upperbound
    using assms by blast
  have "\<forall>x \<in> (\<Union>t1 \<in> tines F. 
 (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})).
\<exists>t t'. 
t \<in> tines F \<and> t' \<in> tines F \<and> 
viable F w t \<and> viable F w t' \<and> x = div_tines F t t'"
    by blast
  hence "\<forall>x \<in> (\<Union>t1 \<in> tines F. 
 (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})).
 x \<le> height F"
    using div_tines_upperbound
    using assms by blast
  thus ?thesis using finite_div_tines_set nonempty_div_tines_set
    by (simp add: assms div_f_def)
qed

definition div_s :: "bool list \<Rightarrow> nat" where
  "div_s w \<equiv> Max ((\<lambda>x. div_f x w) ` (forks_set w))"

lemma div_f_set_nonempty: 
  "((\<lambda>x. div_f x w) ` (forks_set w)) \<noteq> {}"
  by (metis closed_fork.cases empty_Collect_eq empty_is_image exist_closed_fork forks_set_def)

lemma div_f_upperbound_string:
  assumes "F \<turnstile> w"
  shows "div_f F w \<le> size w"
  using div_f_upperbound_forks forks_height_le_size_cha_string
  using assms dual_order.trans by blast

lemma finite_div_f_set:
"finite ((\<lambda>x. div_f x w) ` (forks_set w))"
proof -
  have "((\<lambda>x. div_f x w) ` (forks_set w)) \<subseteq> {0..(size w)}"
    using div_f_upperbound_string forks_set_def by auto
  thus ?thesis
    using subset_eq_atLeast0_atMost_finite by auto
qed

lemma div_s_ge_div_f:
  assumes "F \<turnstile> w"
  shows "div_s w \<ge> div_f F w"
  using assms div_f_upperbound_string finite_div_f_set
  by (simp add: div_s_def forks_set_def)

lemma honest_indices_viable:
  assumes "F \<turnstile> w""t \<in> tines F" "honest F t (honest_indices w)"
  shows "viable F w t"
  using assms
proof - 
  have "\<forall>h. h \<in> honest_indices w \<and> h \<le> last (trace_tine F t) \<longrightarrow> depth F h \<le> depth F (last (trace_tine F t))"
    using assms
    using fork_F forks_def honest_def le_eq_less_or_eq by auto
  hence "\<forall>h. h \<in> honest_indices w \<and> h \<le> last (trace_tine F t) \<longrightarrow> depth F h \<le> size t"
    using One_nat_def assms(1) assms(2) assms(3) fork_F forks_def honest_def mem_Collect_eq reaching_tine_length_eq_label_depth tines_def by fastforce
  thus ?thesis
    using assms(1) assms(2) viable.intros by blast
qed


definition viable_tines_div_pairs_set:: "bool list \<Rightarrow> (rtree \<times> ((nat list) \<times> (nat list))) set" where
  "viable_tines_div_pairs_set w = 
  {p. \<exists>F t1 t2. F \<turnstile> w \<and> p = (F, (t1,t2)) \<and> div_f F w = div_s w
      \<and> viable F w t1 \<and> viable F w t2 \<and> div_f F w = divs F t1 t2}"

definition optimal_diff_tine_pair :: "(rtree \<times> ((nat list) \<times> (nat list))) set \<Rightarrow> int" where
  "optimal_diff_tine_pair S = Min ((\<lambda>p. abs (int (lb (fst p) (fst (snd p))) - int (lb (fst p) (snd (snd p))))) ` S)"

lemma finite_abs_diff_pair:
  "finite ((\<lambda>p. abs (int (lb (fst p) (fst (snd p))) - int (lb (fst p) (snd (snd p))))) ` (viable_tines_div_pairs_set w))"
proof -
  have "\<forall>p \<in> viable_tines_div_pairs_set w. fst p \<turnstile> w"
    by (simp add: viable_tines_div_pairs_set_def)
  hence "\<forall>p \<in> viable_tines_div_pairs_set w. fst (snd p) \<in> tines (fst p) \<and> snd (snd p) \<in> tines (fst p)"
    using fst_conv snd_conv viable.cases viable_tines_div_pairs_set_def by auto
  hence "\<forall>p \<in> viable_tines_div_pairs_set w. (lb (fst p) (fst (snd p)) \<le> size w) \<and> (lb (fst p) (snd (snd p))\<le> size w)"
    using \<open>\<forall>p\<in>viable_tines_div_pairs_set w. fst p \<turnstile> w\<close> lb_limit tines_def by auto
  hence "\<forall>p \<in> viable_tines_div_pairs_set w. 
    abs (int (lb (fst p) (fst (snd p))) - int (lb (fst p) (snd (snd p)))) \<le> abs (int (size w))"
using abs_triangle_ineq2_sym
  by auto
  hence "\<forall>x \<in> ((\<lambda>p. abs (int (lb (fst p) (fst (snd p))) - int (lb (fst p) (snd (snd p))))) ` (viable_tines_div_pairs_set w)). 
          x \<le> int (size w) \<and> x \<ge> 0"
    by simp
  hence "((\<lambda>p. abs (int (lb (fst p) (fst (snd p))) - int (lb (fst p) (snd (snd p))))) ` (viable_tines_div_pairs_set w)) 
        \<subseteq> set [0..(int (size w))]"
    by (simp add: \<open>\<forall>x\<in>(\<lambda>p. \<bar>int (lb (fst p) (fst (snd p))) - int (lb (fst p) (snd (snd p)))\<bar>) ` viable_tines_div_pairs_set w. x \<le> int (length w) \<and> 0 \<le> x\<close> subsetI)
  thus ?thesis
    using rev_finite_subset by auto
qed

lemma nonempty_abs_diff_pair:
  "((\<lambda>p. abs (int (lb (fst p) (fst (snd p))) - int (lb (fst p) (snd (snd p))))) ` (viable_tines_div_pairs_set w)) \<noteq> {}"
proof - 
  obtain F where "F \<turnstile> w" "div_s w = div_f F w"
    using closed_fork.cases exist_closed_fork
    by (metis (mono_tags, lifting) Max_in div_f_set_nonempty div_s_def finite_div_f_set forks_set_def image_iff mem_Collect_eq) 
  then obtain t1 t2 where "viable F w t1" "viable F w t2" "div_tines F t1 t2 = div_f F w"
    by (smt Max_in UN_iff div_f_def div_tines_commutative finite_div_tines_set linear mem_Collect_eq nonempty_div_tines_set)
  hence "divs F t1 t2 = div_tines F t1 t2 \<or> divs F t2 t1 = div_tines F t1 t2"
    using div_tines_def by auto
  hence "(F,(t1,t2)) \<in> (viable_tines_div_pairs_set w) \<or> (F,(t2,t1)) \<in> (viable_tines_div_pairs_set w) "
    using \<open>F \<turnstile> w\<close> \<open>div_s w = div_f F w\<close> \<open>div_tines F t1 t2 = div_f F w\<close> \<open>viable F w t1\<close> \<open>viable F w t2\<close> viable_tines_div_pairs_set_def by auto
  thus ?thesis
    by auto
qed
                      
lemma longest_common_prefix_tines_take_exist:
"\<exists>i. i \<le> size t1 \<and> i \<le> size t2 \<and> longest_common_prefix_tines t1 t2 = take i t1
\<and> longest_common_prefix_tines t1 t2 = take i t2"
proof (induction t1 arbitrary: t2)
  case Nil
  then show ?case
    by simp 
next
  case (Cons h1 t1)
  then show ?case 
  proof (induction t2)
    case Nil
then show ?case
  by simp 
next
  case (Cons h2 t2)
  then show ?case 
  proof (cases "h1 = h2")
    case True
    obtain i where i:"i \<le> size t1 \<and> i \<le> size t2 \<and> longest_common_prefix_tines t1 t2 = take i t1
      \<and> longest_common_prefix_tines t1 t2 = take i t2"
      using Cons.prems by blast
    hence "Suc i \<le> size (h1#t1) \<and> Suc i \<le> size (h2#t2)"
      by simp
    have "longest_common_prefix_tines (h1#t1) (h2#t2)= take (Suc i) (h1#t1)
      \<and> longest_common_prefix_tines (h1#t1) (h2#t2) = take (Suc i) (h2#t2)"
      using i
      by (simp add: i True)
    then show ?thesis
      using \<open>Suc i \<le> length (h1 # t1) \<and> Suc i \<le> length (h2 # t2)\<close> by blast 
next
  case False
  then show ?thesis
    by auto
qed
qed
qed

lemma strictly_inc_dif_ge:
  assumes "i < length l" "i + d < length l" "strictly_inc_list l"
  shows "(l ! (i + d)) - (l ! i) \<ge> d"
  using assms
proof (induction d)
  case 0
  then show ?case
    by simp 
next
  case (Suc d)
  hence "d \<le> l ! (i + d) - l ! i \<and> i + Suc d < length l"
    by simp
  have "l ! (i + d) < l ! Suc (i + d)"
    using \<open>d \<le> l ! (i + d) - l ! i \<and> i + Suc d < length l\<close> assms(3) strictly_inc_list_as_def by auto
  hence "l ! (i + d) - l ! i  < l ! Suc (i + d) - l ! i"
    using \<open>d \<le> l ! (i + d) - l ! i \<and> i + Suc d < length l\<close> not_le by fastforce
  then show ?case
    using \<open>d \<le> l ! (i + d) - l ! i \<and> i + Suc d < length l\<close> by auto
qed

lemma strictly_inc_list_sublist:
  assumes "strictly_inc_list l" "i < length l" "i + d < length l" 
  shows "strictly_inc_list (take d (drop i l))"
  using assms
  using strictly_inc_list_drop strictly_inc_list_take by blast

lemma strictly_inc_list_trace_tine:
  assumes "F \<turnstile> w" "t \<in> tines F"
  shows "strictly_inc_list (trace_tine F t)"
  using assms(1) fork_F forks_def strictly_inc_imp_strictly_inc_tine strictly_inc_tine_def by auto

lemma Extend_tree_mset_of_tree:
  assumes "tinesP F t"
  shows "mset_of_tree (Extend_tree F t rt) = mset_of_tree F + mset_of_tree rt"
  using assms
proof (induction F arbitrary: t)
  case (Tree l Fs)
  then show ?case 
  proof (cases "t=[]")
    case True
    then show ?thesis
      by simp 
next
  case False
  then obtain k tt where t:"t = k#tt"
    using list.exhaust by blast
  hence "k < size Fs"
    using Tree.prems tinesP_Cons_iff by auto
  hence "Extend_tree (Tree l Fs) t rt = Tree l ((take k Fs) @ (Extend_tree (Fs ! k) tt rt) # (drop (Suc k) Fs))"
    using t
    by simp
  hence "mset_of_tree (Extend_tree (Tree l Fs) t rt) = 
          {#l#} + (\<Sum>x \<in># mset ((take k Fs) @ (Extend_tree (Fs ! k) tt rt) # (drop (Suc k) Fs)). mset_of_tree x)"
    by simp
  also have "... =
          {#l#} + (\<Sum>x \<in># mset (take k Fs). mset_of_tree x) +
(\<Sum>x \<in># mset ((Extend_tree (Fs ! k) tt rt) # (drop (Suc k) Fs)). mset_of_tree x)"
    by simp
also have "... =
          {#l#} + (\<Sum>x \<in># mset (take k Fs). mset_of_tree x) + mset_of_tree (Extend_tree (Fs ! k) tt rt) 
            + (\<Sum>x \<in># mset (drop (Suc k) Fs). mset_of_tree x)"
  by simp
also have "... =
          {#l#} + (\<Sum>x \<in># mset (take k Fs). mset_of_tree x) + 
              mset_of_tree (Fs ! k) + mset_of_tree rt
            + (\<Sum>x \<in># mset (drop (Suc k) Fs). mset_of_tree x)"
  using Tree.IH
  using Tree.prems t tinesP_Cons_iff by auto
also have "... =
          {#l#} + (\<Sum>x \<in># mset (take k Fs). mset_of_tree x) + 
              mset_of_tree (Fs ! k) 
            + (\<Sum>x \<in># mset (drop (Suc k) Fs). mset_of_tree x)
+ mset_of_tree rt"
  by simp
also have "... =
          {#l#} + (\<Sum>x \<in># mset (take k Fs) . mset_of_tree x)  
            + (\<Sum>x \<in># mset ((Fs !k) # (drop (Suc k) Fs)). mset_of_tree x)
+ mset_of_tree rt"
  by simp
also have "... =
          {#l#} + (\<Sum>x \<in># mset ((take k Fs) @ (Fs !k) # (drop (Suc k) Fs)). mset_of_tree x)
+ mset_of_tree rt"
  by simp
  thus ?thesis
    by (simp add: Cons_nth_drop_Suc \<open>k < length Fs\<close> calculation)
qed
qed

lemma Cut_subtree_preserve_cut_tines: 
  assumes "tinesP F t" 
  shows "t \<in> tines (Cut_subtree F t k)"
  using assms
proof (induction)
case (Sucs k F t)
  then show ?case
    by (smt Cons_nth_drop_Suc Cut_subtree.simps(2) diff_add_inverse2 id_take_nth_drop length_Cons length_append length_drop mem_Collect_eq nth_append_length rtree.exhaust_sel rtree.sel(2) tinesP_Cons_iff tines_def) 
next
  case (Nil F)
  then show ?case
    by (simp add: tinesP.Nil tines_def) 
qed

lemma butlast_tinesP:
  assumes "tinesP F t"
  shows "tinesP F (butlast t)"
  by (simp add: assms butlast_conv_take take_tine_is_tine)

lemma Extend_tree_Cut_subtree_preserves_unique_honest:
  assumes "F \<turnstile> w" "t@[k] \<in> tines F" "adversarial F t (honest_indices w)" "h \<in> honest_indices w"
  shows "count (mset_of_tree (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])]))) h = 1"
proof - 
  have "t \<in> tines F"
    using assms butlast_tinesP
    using tines_def by fastforce
  hence "t \<in> tines (Cut_subtree F t k)"
    using Cut_subtree_preserve_cut_tines tines_def by auto
  hence "butlast t \<in> tines (Cut_subtree F t k)"
    by (simp add: butlast_tinesP tines_def)
  hence "mset_of_tree (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) 
        = mset_of_tree (Cut_subtree F t k) + mset_of_tree (Tree (lb F t) [get_subtree F (t@[k])])"
    by (simp add: Extend_tree_mset_of_tree tines_def)
hence "mset_of_tree (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) 
        = mset_of_tree F - mset_of_tree (get_subtree F (t@[k])) + mset_of_tree (Tree (lb F t) [get_subtree F (t@[k])])"
  using Cut_subtree_mset_of_tree assms(2) last_snoc tines_def by auto
hence "mset_of_tree (Extend_tree (Cut_subtree F t k)  (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) 
        = mset_of_tree F - mset_of_tree (get_subtree F (t@[k]))  
+ {#(lb F t)#} 
+ (\<Sum>x \<in># mset [get_subtree F (t@[k])]. mset_of_tree x)"
  by simp
hence "mset_of_tree (Extend_tree (Cut_subtree F t k)  (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) 
        = mset_of_tree F - mset_of_tree (get_subtree F (t@[k]))  
        + {#(lb F t)#} + mset_of_tree (get_subtree F (t@[k]))"
  by simp
hence "mset_of_tree (Extend_tree (Cut_subtree F t k)  (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) 
        = mset_of_tree F + {#(lb F t)#} "
  using Cut_subtree_mset_of_tree assms(2) list.simps(3) tines_def by auto
  hence "count (mset_of_tree (Extend_tree (Cut_subtree F t k)  (butlast t) (Tree (lb F t) [get_subtree F (t@[k])]))) h 
= count (mset_of_tree F + {#(lb F t)#}) h "
    by simp
  hence  "count (mset_of_tree (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])]))) h 
= count (mset_of_tree F) h + count ({#(lb F t)#}) h"
    by simp
  hence  "count (mset_of_tree (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])]))) h 
= 1+ count ({#(lb F t)#}) h"
    using assms(1)
    by (simp add: assms(4) fork_F forks_def)
  thus ?thesis
  proof -
    have "count ({#(lb F t)#}) h = 0"
      using assms(3) assms(4)
    proof -
      have "lb F t \<notin> honest_indices w"
        using assms(3)
        by (metis \<open>t \<in> tines F\<close> append_Nil2 append_eq_conv_conj honest_def lb_def lessI mem_Collect_eq suc_tine_size_eq_label_list take_Suc_nth tines_def)
 
      thus ?thesis
        using assms(4) by auto
    qed
    thus ?thesis
      using \<open>count (mset_of_tree (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t @ [k])]))) h = 1 + count {#lb F t#} h\<close> by linarith
    
  qed
qed

lemma Extend_tree_Cut_subtree_preserves_unique_honest_2:
  assumes "F \<turnstile> w" "t@[k] \<in> tines F" "t' \<in> tines (Cut_subtree F t k)" "h \<in> honest_indices w"
  shows "count (mset_of_tree (Extend_tree (Cut_subtree F t k) t' (get_subtree F (t@[k])))) h = 1"
proof -
have "mset_of_tree (Extend_tree (Cut_subtree F t k) t' (get_subtree F (t@[k])))
 = mset_of_tree (Cut_subtree F t k) + mset_of_tree (get_subtree F (t@[k]))"
     using Extend_tree_mset_of_tree assms(3) tine_prefix_imp_both_tines by blast
  hence tmp:"...
 =mset_of_tree F "
    using Cut_subtree_mset_of_tree assms(2) tines_def by auto
  hence "count (mset_of_tree (Extend_tree (Cut_subtree F t k) t' (get_subtree F (t@[k])))) h = 
count (mset_of_tree F) h"
    by (simp add: \<open>mset_of_tree (Extend_tree (Cut_subtree F t k) t' (get_subtree F (t @ [k]))) = mset_of_tree (Cut_subtree F t k) + mset_of_tree (get_subtree F (t @ [k]))\<close>)
  have "count (mset_of_tree F) h = 1"
    using \<open>F \<turnstile>w\<close> \<open>h \<in> honest_indices w\<close> fork_F forks_def
    by simp
  thus ?thesis
    using assms
    using \<open>count (mset_of_tree (Extend_tree (Cut_subtree F t k) t' (get_subtree F (t @ [k])))) h = count (mset_of_tree F) h\<close> by linarith
qed

lemma Extend_tree_implies_prefix:
 assumes "tine \<in> tines F" 
 shows "F \<le> Extend_tree F tine tree"  
  using assms
proof (induction F arbitrary: tine)
  case (Tree l Fs) 
  then show ?case 
    proof (cases tine)
      case Nil
      have "match prefix Fs (tree#Fs)"
        by (simp add: less_eq_rtree.transfer match_extra reflp_aux)
      then show ?thesis
        by (metis Extend_tree.simps(1) dual_order.refl less_eq_rtree.transfer local.Nil match_extra prefix.simps)
    next
      case (Cons k t)
      hence "k < size Fs"
        using Tree.prems tine_prefix_refl by fastforce
      have "match prefix (drop k Fs) ([(Extend_tree (Fs ! k) t tree)] @ (drop (Suc k) Fs))"
        by (smt Cons_nth_drop_Suc Tree.IH Tree.prems append_Cons append_self_conv2 less_eq_rtree.transfer local.Cons match_true mem_Collect_eq nth_mem order_refl reflp_aux rtree.sel(2) tine_prefix_Subtree tine_prefix_imp_both_tines tine_prefix_refl tines_def)
      hence "match prefix ((take k Fs)@(drop k Fs)) ((take k Fs)@([(Extend_tree (Fs ! k) t tree)] @ (drop (Suc k) Fs)))"
        using match_Append_prefix by blast     
      then show ?thesis
        by (simp add: less_eq_rtree.abs_eq local.Cons) 
    qed
qed    

lemma Extend_tree_with_no_added_honest_preserves_honest_depth:
  assumes "F \<turnstile> w" "t \<in> tines F"  "h \<in> honest_indices w" "count (mset_of_tree st) h = 0"
  shows "depth F h = depth (Extend_tree F t st) h"
  using assms prefix_eq_depth_unique Extend_tree_implies_prefix Extend_tree_mset_of_tree
  by (simp add: fork_F forks_def tines_def)

lemma Extend_tree_new_depth:
  assumes "tinesP F t" "count (mset_of_tree F) h = 0" "count (mset_of_tree st) h = 1" 
  shows "depth (Extend_tree F t st) h = Suc (size t) + depth st h"
  using assms
proof (induction arbitrary: h)
  case (Sucs k F t)
  then obtain l Fs where "F =Tree l Fs "
    using rtree.exhaust_sel by blast
  hence 0:"Extend_tree F (k#t) st = 
          Tree l ((take k Fs) @ (Extend_tree (Fs ! k) t st) # (drop (Suc k) Fs))"
    by simp
  hence 1:"count (mset_of_tree (Extend_tree F (k#t) st)) h = 1"
    using assms
  proof -
    show ?thesis
      by (metis (no_types) Extend_tree_mset_of_tree One_nat_def Suc_eq_plus1 Sucs.hyps(1) Sucs.hyps(2) Sucs.prems(1) Sucs.prems(2) count_union tinesP.Sucs)
  qed
  then obtain th where th:"th \<in> tines (Extend_tree F (k#t) st) \<and> last (trace_tine  (Extend_tree F (k#t) st) th) = h "
    using exist_unique_label_exist_unique_tine by blast
  have 2:"count (mset_of_tree (Extend_tree (Fs ! k) t st)) h = 1"
    using assms
    by (smt Extend_tree_mset_of_tree One_nat_def Suc_eq_plus1 Suc_less_eq Sucs.hyps(1) Sucs.hyps(2) Sucs.prems(1) Sucs.prems(2) \<open>F = Tree l Fs\<close> add_eq_0_iff_both_eq_0 count_brance_le_count_tree count_greater_eq_one_iff count_inI count_union leD le_neq_implies_less nth_mem rtree.sel(2))
  then obtain th' where th':"th' \<in> tines (Extend_tree (Fs ! k) t st) \<and>  last (trace_tine  (Extend_tree (Fs ! k) t st) th') = h "
    using exist_unique_label_exist_unique_tine by blast
  hence "trace_tine (Extend_tree F (k#t) st) (k#th') = l#(trace_tine  (Extend_tree (Fs ! k) t st) th')"
    by (smt "0" Sucs.hyps(1) \<open>F = Tree l Fs\<close> diff_add_inverse2 id_take_nth_drop length_Cons length_append length_drop list.case(2) nth_append_length rtree.sel(2) trace_tine.simps)
  hence "(k#th') \<in> tines (Extend_tree F (k#t) st) \<and> last (trace_tine (Extend_tree F (k#t) st) (k#th')) = h"
    using th'
    by (smt "0" Sucs.hyps(1) \<open>F = Tree l Fs\<close> add_right_cancel id_take_nth_drop last_ConsR length_Cons length_append length_drop mem_Collect_eq nth_append_length rtree.sel(2) tinesP.Sucs tines_def trace_tine_non_empty)
  hence thkth':"k#th' = th"
    using th 1 exist_unique_label_exist_unique_tine
    by blast
  have "depth (Extend_tree (Fs ! k) t st) h = size th'"
    using th' 2
    using reaching_tine_length_eq_label_depth tines_def by auto
  hence "size th' = Suc (size t) + depth st h"
    using "2" Extend_tree_mset_of_tree Sucs.IH Sucs.hyps(2) Sucs.prems(2) \<open>F = Tree l Fs\<close> by auto
  then show ?case 
    using thkth' 0 1 2 th th'
    by (metis Suc_eq_plus1 ab_semigroup_add_class.add_ac(1) add.commute length_Cons mem_Collect_eq reaching_tine_length_eq_label_depth tines_def)
next
  case (Nil F)
  then obtain l Fs where "F =Tree l Fs "
    using rtree.exhaust_sel by blast
  hence "Extend_tree F [] st = Tree l (st#Fs)"
    by simp
  hence "count (mset_of_tree (Extend_tree F [] st)) h = 1"
    using assms
  proof -
    have "count {#l#} h + count (\<Union>#image_mset mset_of_tree (mset Fs)) h = 0"
using Nil.prems(1) \<open>F = Tree l Fs\<close> by force
then have "count (mset_of_tree (Extend_tree F [] st)) h = 1 + 0"
  using Nil.prems(2) \<open>Extend_tree F [] st = Tree l (st # Fs)\<close> by force
then show ?thesis
by presburger
qed
  obtain th where "th \<in> tines (Extend_tree F [] st) \<and> last (trace_tine (Extend_tree F [] st) th) = h"
    using \<open>count (mset_of_tree (Extend_tree F [] st)) h = 1\<close> exist_unique_label_exist_unique_tine by blast
  obtain th' where "th' \<in> tines st \<and> last (trace_tine st th') = h"
    using assms
    using Nil.prems(2) exist_unique_label_exist_unique_tine by blast
  hence "last (trace_tine (Extend_tree F [] st) (0#th')) = h"
    by (simp add: \<open>Extend_tree F [] st = Tree l (st # Fs)\<close> trace_tine_non_empty)
  hence "th = 0#th'"
    by (metis Sucs \<open>Extend_tree F [] st = Tree l (st # Fs)\<close> \<open>count (mset_of_tree (Extend_tree F [] st)) h = 1\<close> \<open>th \<in> tines (Extend_tree F [] st) \<and> last (trace_tine (Extend_tree F [] st) th) = h\<close> \<open>th' \<in> tines st \<and> last (trace_tine st th') = h\<close> exist_unique_label_exist_unique_tine length_Cons mem_Collect_eq nth_Cons_0 rtree.sel(2) tines_def zero_less_Suc)
  hence "depth (Extend_tree F [] st) h = 1 + depth st h"
    by (metis Nat.add_0_right Nil.prems(2) One_nat_def \<open>count (mset_of_tree (Extend_tree F [] st)) h = 1\<close> \<open>th \<in> tines (Extend_tree F [] st) \<and> last (trace_tine (Extend_tree F [] st) th) = h\<close> \<open>th' \<in> tines st \<and> last (trace_tine st th') = h\<close> add.commute add_Suc_right length_Cons mem_Collect_eq reaching_tine_length_eq_label_depth tines_def)
  then show ?case
    by simp
qed

lemma get_subtree_new_depth:
  assumes "tinesP F t" "count (mset_of_tree F) h = 1" "count (mset_of_tree (get_subtree F t)) h = 1" 
  shows "depth (get_subtree F t) h = depth F h - size t"
  using assms 
proof (induction)
  case (Sucs k F t)
  then obtain l Fs where lFs:"F = Tree l Fs"
    using rtree.exhaust by blast
  have pre:"count (mset_of_tree F) h = 1 \<and> count (mset_of_tree (get_subtree F (k#t))) h = 1"
    using Sucs by blast 
  hence assm2:"count (mset_of_tree (get_subtree (Fs!k) t)) h = 1"
    using lFs
    using Sucs.hyps(1) by auto
  hence assm:"count (mset_of_tree (Fs!k)) h = 1"
    using pre
    by (metis Sucs.hyps(1) count_brance_le_count_tree count_subtree_le_count get_subtree_is_subtree lFs le_antisym nth_mem rtree.sel(2))
  hence "depth (get_subtree (Fs!k) t) h = depth (Fs!k) h - size t"
    using assm2
    using Sucs.IH lFs by auto
  hence "depth (get_subtree F (k#t)) h = depth (Fs!k) h - size t"
    using Sucs.hyps(1) lFs by auto
 hence "depth (get_subtree F (k#t)) h = Suc (depth (Fs!k) h) - Suc (size t)"
   by auto
  obtain th' where "th' \<in> tines (Fs!k) \<and> last (trace_tine (Fs!k)  th') = h"
    using assm
    using exist_unique_label_exist_unique_tine by blast
  hence 0:"k#th' \<in> tines F"
    using Sucs.hyps(1) lFs tinesP_Cons_iff tines_def by auto
  have 1:"last (trace_tine F (k#th')) = h"
    using Sucs.hyps(1) \<open>th' \<in> tines (Fs ! k) \<and> last (trace_tine (Fs ! k) th') = h\<close> lFs trace_tine_non_empty by auto
  obtain th where "th \<in> tines F \<and> last (trace_tine F th) = h"
    using Sucs.prems(1) exist_unique_label_exist_unique_tine by blast
  hence "k#th' = th"
    using 0 1
    using Sucs.prems(1) exist_unique_label_exist_unique_tine by blast
  hence "depth F h = Suc (size th')"
    using "0" "1" Sucs.prems(1) reaching_tine_length_eq_label_depth tines_def by fastforce
  hence "depth F h = Suc (depth (Fs!k) h)"
    by (metis Sucs.hyps(1) assm count_one_tree_count_zero_branch_dis_pred_depth lFs nth_mem pre rtree.sel(2) zero_neq_one)
  hence "depth (get_subtree F (k#t)) h = depth F h - (size (k#t))"
    by (simp add: \<open>depth (get_subtree F (k # t)) h = Suc (depth (Fs ! k) h) - Suc (length t)\<close>)
  then show ?case
    by blast 
next
  case (Nil F)
  hence "get_subtree F [] = F"
    by (smt get_subtree.elims list.simps(4))
  then show ?case
    by simp 
qed

lemma Extend_tree_Cut_subtree_preserves_honest_depth:
  assumes "F \<turnstile> w" "t@[k] \<in> tines F" "adversarial F t (honest_indices w)" "h \<in> honest_indices w" 
  shows "depth F h = depth (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) h"
proof (cases "count (mset_of_tree (get_subtree F (t@[k]))) h = 0")
  case True
  have "count (mset_of_tree (Cut_subtree F t k)) h = 1"
    using Cut_subtree_mset_of_tree True assms(1) assms(2) assms(4) fork_F forks_def tines_def by auto
  hence "count (mset_of_tree (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])]))) h = 1"
    using Extend_tree_Cut_subtree_preserves_unique_honest assms(1) assms(2) assms(3) assms(4)
    by blast 
  have "Cut_subtree F t k \<le> F"
    using Cut_subtree_hd_prefix assms(2) tines_def by fastforce
  hence "depth (Cut_subtree F t k) h = depth F h"
    using \<open>count (mset_of_tree (Cut_subtree F t k)) h = 1\<close> assms(1) assms(4) fork_F forks_def prefix_eq_depth_unique by auto  
  have "butlast t \<in> tines (Cut_subtree F t k)"
    using assms butlast_tinesP tines_def
    by (metis Cut_subtree_preserve_cut_tines butlast_snoc mem_Collect_eq)
  have "Cut_subtree F t k \<le> (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])]))"
    using Extend_tree_implies_prefix \<open>butlast t \<in> tines (Cut_subtree F t k)\<close> by blast
  hence "depth (Cut_subtree F t k) h  = depth (Extend_tree (Cut_subtree F t k) t (Tree (lb F t) [get_subtree F (t@[k])])) h"
    using \<open>count (mset_of_tree (Cut_subtree F t k)) h = 1\<close> \<open>count (mset_of_tree (Extend_tree (Cut_subtree F t k)(butlast t) (Tree (lb F t) [get_subtree F (t @ [k])]))) h = 1\<close> prefix_eq_depth_unique
    by (metis (full_types) Cut_subtree_preserve_cut_tines Extend_tree_implies_prefix Extend_tree_mset_of_tree assms(2) butlast_snoc butlast_tinesP mem_Collect_eq tines_def) 
  then show ?thesis
    using \<open>depth (Cut_subtree F t k) h = depth F h\<close>
    using \<open>Cut_subtree F t k \<le> Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t @ [k])])\<close> \<open>count (mset_of_tree (Cut_subtree F t k)) h = 1\<close> \<open>count (mset_of_tree (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t @ [k])]))) h = 1\<close> prefix_eq_depth_unique by auto 
next
  case False
  hence "count (mset_of_tree (get_subtree F (t@[k]))) h \<le> 1"
    using assms
    by (metis One_nat_def count_subtree_le_count fork_F forks_def get_subtree_is_subtree)
  hence count1:"count (mset_of_tree (get_subtree F (t@[k]))) h = 1" 
    using False
    by linarith 
  hence "depth (get_subtree F (t@[k])) h = depth F h - size (t@[k])"
    by (metis (full_types) One_nat_def assms(1) assms(2) assms(4) fork_F forks_def get_subtree_new_depth mem_Collect_eq tines_def)
  hence x: "depth (Tree (lb F t) [get_subtree F (t@[k])]) h = Suc (depth (get_subtree F (t@[k])) h)"
  proof - 
    obtain th where th:"th \<in> tines (get_subtree F (t@[k])) \<and> last (trace_tine (get_subtree F (t@[k])) th) = h"
      using count1 exist_unique_label_exist_unique_tine by blast
    hence tineh:"0#th \<in> tines (Tree (lb F t) [get_subtree F (t@[k])])"
      by (simp add: tinesP_Cons_iff tines_def)
    have lasth:"last (trace_tine (Tree (lb F t) [get_subtree F (t@[k])]) (0#th)) = h"
      using th
      by (simp add: trace_tine_non_empty)
    have "(lb F t) \<notin> honest_indices w"
      using assms
      by (metis butlast_snoc butlast_tinesP honest_def lb_def lessI mem_Collect_eq order_refl suc_tine_size_eq_label_list take_Suc_nth take_all tines_def) 
    hence "(lb F t) \<noteq> h"
      using assms
      by blast
    hence "count (mset_of_tree (Tree (lb F t) [get_subtree F (t@[k])])) h = 1"
      using count1 mset_of_tree.simps
      by simp
    hence "depth (Tree (lb F t) [get_subtree F (t@[k])]) h = size (0#th)"
      using th tineh lasth
      by (metis mem_Collect_eq reaching_tine_length_eq_label_depth tines_def)
    hence "depth (Tree (lb F t) [get_subtree F (t@[k])]) h = Suc (size th)"
      by simp
    then show ?thesis 
      using th
      using count1 mem_Collect_eq reaching_tine_length_eq_label_depth tines_def by auto
  qed
  hence "depth (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) h
= Suc (size (butlast t)) + depth (Tree (lb F t) [get_subtree F (t@[k])]) h"
  proof - 
    have 0:"tinesP (Cut_subtree F t k) t"
      by (metis Cut_subtree_preserve_cut_tines append_eq_conv_conj assms(2) mem_Collect_eq take_tine_is_tine tines_def)
    have 1:"count (mset_of_tree (Cut_subtree F t k)) h = 0"
      using Cut_subtree_mset_of_tree assms(1) assms(2) assms(4) count1 fork_F forks_def tines_def by auto
    thus ?thesis
      using Extend_tree_new_depth 0
      by (smt Extend_tree_Cut_subtree_preserves_unique_honest Extend_tree_mset_of_tree One_nat_def add_is_1 assms(1) assms(2) assms(3) assms(4) butlast_tinesP count_union)   
  qed
  hence "depth (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) h
= Suc (size (butlast t)) + Suc (depth (get_subtree F (t@[k])) h)"
   by (simp add: x)
 hence "depth (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) h
= Suc (size (butlast t)) + Suc (depth F h - length (t@[k]))"
   using \<open>depth (get_subtree F (t @ [k])) h = depth F h - length (t @ [k])\<close> by linarith
   hence "depth (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) h
= size t + Suc (depth F h - length (t@[k]))"
   proof -
     have "t \<noteq> []"
     proof (rule ccontr)
       assume "\<not> t \<noteq> []"
       hence "last (trace_tine F t) = 0"
         using assms(1) forks_def fork_root0
         using exist_honest_tine honest_indices_iff by fastforce
       hence "last (trace_tine F t) \<in> honest_indices w"
         by (simp add: honest_indices_iff)
       thus "False"
         using assms(3) honest_def by auto
     qed
     hence "size (butlast t) = size t - 1"
       by simp
     thus ?thesis
       by (simp add: \<open>depth (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t @ [k])])) h = Suc (length (butlast t)) + Suc (depth F h - length (t @ [k]))\<close> \<open>t \<noteq> []\<close>)
   qed
 hence "depth (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) h
= size t + depth F h - Suc (size t) + 1"
 proof - 
   have "depth F h \<ge> size (t@[k])"
   proof (rule ccontr)
     assume "\<not> depth F h \<ge> size (t@[k])"
     hence "depth F h < size (t@[k])"
       by simp
     then obtain th' where "th' \<in> tines F \<and> last (trace_tine F th') = h \<and> size th' < size (t@[k])"
       using assms(1) assms(4) exist_honest_tine by fastforce
     obtain th'' where "th'' \<in> tines (get_subtree F (t@[k])) \<and>
        last (trace_tine (get_subtree F (t@[k])) th'') = h"
       using \<open>count (mset_of_tree (get_subtree F (t@[k]))) h = 1\<close>
       using exist_unique_label_exist_unique_tine by blast
     hence "(t@[k])@th'' \<in> tines F"
       using assms tine_append_tine_of_subtree_is_tine
       using tines_def by fastforce
     hence tmp:"(trace_tine F (t@[k])@th'') = trace_tine F (t@[k]) @ (tl (trace_tine (get_subtree F (t@[k])) th''))"
       using trace_tine_append_trace_tine_subtree
       by (metis \<open>\<not> length (t @ [k]) \<le> depth F h\<close> \<open>depth (get_subtree F (t @ [k])) h = depth F h - length (t @ [k])\<close> \<open>th'' \<in> tines (get_subtree F (t @ [k])) \<and> last (trace_tine (get_subtree F (t @ [k])) th'') = h\<close> append_Nil2 append_eq_append_conv_if count1 diff_is_0_eq' length_greater_0_conv list.size(3) mem_Collect_eq nat_le_linear reaching_tine_length_eq_label_depth tines_def)
     hence "last (trace_tine F (t@[k])@th'') = last (trace_tine (get_subtree F (t@[k])) th'')"
     proof (cases "th'' \<noteq> []")
       case True
       hence "size (trace_tine (get_subtree F (t@[k])) th'') > 1"
         using \<open>depth (get_subtree F (t @ [k])) h = depth F h - length (t @ [k])\<close> \<open>th'' \<in> tines (get_subtree F (t @ [k])) \<and> last (trace_tine (get_subtree F (t @ [k])) th'') = h\<close> tines_def by auto
       hence "last (tl (trace_tine (get_subtree F (t@[k])) th'')) =
              last (trace_tine (get_subtree F (t@[k])) th'')"
         by (metis True \<open>trace_tine F (t @ [k]) @ th'' = trace_tine F (t @ [k]) @ tl (trace_tine (get_subtree F (t @ [k])) th'')\<close> last_tl self_append_conv)
       then show ?thesis
         using tmp
         using True by auto
     next
       case False
       hence "th'' = []"
         by simp
       hence "size (trace_tine (get_subtree F (t@[k])) th'') = 1"
         using tinesP.Nil by auto
       hence "last (trace_tine (get_subtree F (t@[k])) th'') \<in> set (trace_tine (get_subtree F (t@[k])) th'')"
         using trace_tine_non_empty by auto
       hence "(trace_tine (get_subtree F (t@[k])) th'') = [h]"
         by (metis \<open>th'' = []\<close> \<open>th'' \<in> tines (get_subtree F (t @ [k])) \<and> last (trace_tine (get_subtree F (t @ [k])) th'') = h\<close> last_ConsL list.exhaust_sel same_append_eq tmp trace_tine_non_empty)
       hence "last (trace_tine F (t@[k])) = h"
         by (metis \<open>th'' = []\<close> \<open>th'' \<in> tines (get_subtree F (t @ [k])) \<and> last (trace_tine (get_subtree F (t @ [k])) th'') = h\<close> append_Nil2 append_eq_append_conv_if assms(2) get_subtree_append last_in_tine_is_label_in_subtree mem_Collect_eq tines_def)
       then show ?thesis
         using False \<open>th'' \<in> tines (get_subtree F (t @ [k])) \<and> last (trace_tine (get_subtree F (t @ [k])) th'') = h\<close> by auto
     qed
     hence "last (trace_tine F (t@[k])@th'') = h"
       by (simp add: \<open>th'' \<in> tines (get_subtree F (t @ [k])) \<and> last (trace_tine (get_subtree F (t @ [k])) th'') = h\<close>)
     hence "depth F h = size ((t@[k])@th'')"
       by (metis One_nat_def \<open>\<not> length (t @ [k]) \<le> depth F h\<close> \<open>depth (get_subtree F (t @ [k])) h = depth F h - length (t @ [k])\<close> \<open>th'' \<in> tines (get_subtree F (t @ [k])) \<and> last (trace_tine (get_subtree F (t @ [k])) th'') = h\<close> append_Nil2 assms(1) assms(2) assms(4) count1 diff_is_0_eq' fork_F forks_def length_greater_0_conv mem_Collect_eq nat_le_linear nat_neq_iff reaching_tine_length_eq_label_depth tines_def)
     thus "False"
       using \<open>\<not> length (t @ [k]) \<le> depth F h\<close> by auto
   qed
   thus ?thesis
     by (simp add: \<open>depth (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t @ [k])])) h = length t + Suc (depth F h - length (t @ [k]))\<close>)
 qed
     thus ?thesis
 using \<open>depth (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t @ [k])])) h = Suc (length (butlast t)) + Suc (depth (get_subtree F (t @ [k])) h)\<close> by linarith
qed

lemma Extend_tree_Cut_subtree_preserves_honest_depth_2:
  assumes "F \<turnstile> w" "t@[k] \<in> tines F" "h \<in> honest_indices w" "t' \<in> tines (Cut_subtree F t k)" "size t = size t'"
  shows "depth F h = depth (Extend_tree (Cut_subtree F t k) t' (get_subtree F (t@[k]))) h"
proof (cases "count (mset_of_tree (get_subtree F (t@[k]))) h = 0")
  case True
  have "count (mset_of_tree (Cut_subtree F t k)) h = 1"
    using Cut_subtree_mset_of_tree True assms(1) assms(2) assms(3) assms(4) fork_F forks_def tines_def
    by auto
  hence "count (mset_of_tree (Extend_tree (Cut_subtree F t k) t' (get_subtree F (t@[k])))) h = 1"
    using Extend_tree_Cut_subtree_preserves_unique_honest assms(1) assms(2) assms(3) assms(4)
    using Extend_tree_Cut_subtree_preserves_unique_honest_2 by blast
  have "Cut_subtree F t k \<le> F"
    using Cut_subtree_hd_prefix assms(2) tines_def by fastforce
  hence "depth (Cut_subtree F t k) h = depth F h"
    using \<open>count (mset_of_tree (Cut_subtree F t k)) h = 1\<close> assms fork_F forks_def prefix_eq_depth_unique by auto  
  have "Cut_subtree F t k \<le> (Extend_tree (Cut_subtree F t k) t' (get_subtree F (t@[k])))"
    using Extend_tree_implies_prefix assms by blast
  hence "depth (Cut_subtree F t k) h  = depth (Extend_tree (Cut_subtree F t k) t' (get_subtree F (t@[k]))) h"
    using \<open>count (mset_of_tree (Cut_subtree F t k)) h = 1\<close> \<open>count (mset_of_tree (Extend_tree (Cut_subtree F t k) t' (get_subtree F (t @ [k])))) h = 1\<close> prefix_eq_depth_unique by blast
  then show ?thesis
    using \<open>depth (Cut_subtree F t k) h = depth F h\<close> by linarith
next
  case False
  hence "count (mset_of_tree (get_subtree F (t@[k]))) h \<le> 1"
    using assms
    by (metis One_nat_def count_subtree_le_count fork_F forks_def get_subtree_is_subtree)
  hence count1:"count (mset_of_tree (get_subtree F (t@[k]))) h = 1" 
    using False
    by linarith 
  hence "depth (get_subtree F (t@[k])) h = depth F h - size (t@[k])"
    by (metis (full_types) One_nat_def assms(1) assms(2) assms(3) fork_F forks_def get_subtree_new_depth mem_Collect_eq tines_def)
 
  hence "depth (Extend_tree (Cut_subtree F t k) t' (get_subtree F (t@[k]))) h
        = Suc (size t') + depth (get_subtree F (t@[k])) h"
  proof - 
    have 0:"tinesP (Cut_subtree F t k) t"
      by (metis Cut_subtree_preserve_cut_tines append_eq_conv_conj assms(2) mem_Collect_eq take_tine_is_tine tines_def)
    have 1:"count (mset_of_tree (Cut_subtree F t k)) h = 0"
      using Cut_subtree_mset_of_tree assms(1) assms(2) assms(3) count1 fork_F forks_def tines_def by auto
    thus ?thesis
      using Extend_tree_new_depth 0
      using assms(4) count1 tine_prefix_imp_both_tines by blast
  qed
 hence "depth (Extend_tree (Cut_subtree F t k) t' (get_subtree F (t@[k]))) h
= Suc (size t') + (depth F h - length (t@[k]))"
   using \<open>depth (get_subtree F (t @ [k])) h = depth F h - length (t @ [k])\<close> by linarith
  thus ?thesis
proof - 
   have "depth F h \<ge> size (t@[k])"
   proof (rule ccontr)
     assume "\<not> depth F h \<ge> size (t@[k])"
     hence "depth F h < size (t@[k])"
       by simp
     then obtain th' where "th' \<in> tines F \<and> last (trace_tine F th') = h \<and> size th' < size (t@[k])"
       using assms(1) assms(3) exist_honest_tine by fastforce
     obtain th'' where "th'' \<in> tines (get_subtree F (t@[k])) \<and>
        last (trace_tine (get_subtree F (t@[k])) th'') = h"
       using \<open>count (mset_of_tree (get_subtree F (t@[k]))) h = 1\<close>
       using exist_unique_label_exist_unique_tine by blast
     hence "(t@[k])@th'' \<in> tines F"
       using assms tine_append_tine_of_subtree_is_tine
       using tines_def by fastforce
     hence tmp:"(trace_tine F (t@[k])@th'') = trace_tine F (t@[k]) @ (tl (trace_tine (get_subtree F (t@[k])) th''))"
       using trace_tine_append_trace_tine_subtree
       by (metis \<open>\<not> length (t @ [k]) \<le> depth F h\<close> \<open>depth (get_subtree F (t @ [k])) h = depth F h - length (t @ [k])\<close> \<open>th'' \<in> tines (get_subtree F (t @ [k])) \<and> last (trace_tine (get_subtree F (t @ [k])) th'') = h\<close> append_Nil2 append_eq_append_conv_if count1 diff_is_0_eq' length_greater_0_conv list.size(3) mem_Collect_eq nat_le_linear reaching_tine_length_eq_label_depth tines_def)
     hence "last (trace_tine F (t@[k])@th'') = last (trace_tine (get_subtree F (t@[k])) th'')"
     proof (cases "th'' \<noteq> []")
       case True
       hence "size (trace_tine (get_subtree F (t@[k])) th'') > 1"
         using \<open>depth (get_subtree F (t @ [k])) h = depth F h - length (t @ [k])\<close> \<open>th'' \<in> tines (get_subtree F (t @ [k])) \<and> last (trace_tine (get_subtree F (t @ [k])) th'') = h\<close> tines_def by auto
       hence "last (tl (trace_tine (get_subtree F (t@[k])) th'')) =
              last (trace_tine (get_subtree F (t@[k])) th'')"
         by (metis True \<open>trace_tine F (t @ [k]) @ th'' = trace_tine F (t @ [k]) @ tl (trace_tine (get_subtree F (t @ [k])) th'')\<close> last_tl self_append_conv)
       then show ?thesis
         using tmp
         using True by auto
     next
       case False
       hence "th'' = []"
         by simp
       hence "size (trace_tine (get_subtree F (t@[k])) th'') = 1"
         using tinesP.Nil by auto
       hence "last (trace_tine (get_subtree F (t@[k])) th'') \<in> set (trace_tine (get_subtree F (t@[k])) th'')"
         using trace_tine_non_empty by auto
       hence "(trace_tine (get_subtree F (t@[k])) th'') = [h]"
         by (metis \<open>th'' = []\<close> \<open>th'' \<in> tines (get_subtree F (t @ [k])) \<and> last (trace_tine (get_subtree F (t @ [k])) th'') = h\<close> last_ConsL list.exhaust_sel same_append_eq tmp trace_tine_non_empty)
       hence "last (trace_tine F (t@[k])) = h"
         by (metis \<open>th'' = []\<close> \<open>th'' \<in> tines (get_subtree F (t @ [k])) \<and> last (trace_tine (get_subtree F (t @ [k])) th'') = h\<close> append_Nil2 append_eq_append_conv_if assms(2) get_subtree_append last_in_tine_is_label_in_subtree mem_Collect_eq tines_def)
       then show ?thesis
         using False \<open>th'' \<in> tines (get_subtree F (t @ [k])) \<and> last (trace_tine (get_subtree F (t @ [k])) th'') = h\<close> by auto
     qed
     hence "last (trace_tine F (t@[k])@th'') = h"
       by (simp add: \<open>th'' \<in> tines (get_subtree F (t @ [k])) \<and> last (trace_tine (get_subtree F (t @ [k])) th'') = h\<close>)
     hence "depth F h = size ((t@[k])@th'')"
       by (metis One_nat_def \<open>\<not> length (t @ [k]) \<le> depth F h\<close> \<open>depth (get_subtree F (t @ [k])) h = depth F h - length (t @ [k])\<close> \<open>th'' \<in> tines (get_subtree F (t @ [k])) \<and> last (trace_tine (get_subtree F (t @ [k])) th'') = h\<close> 
append_Nil2 assms(1) assms(2) assms(3) count1 diff_is_0_eq' fork_F forks_def length_greater_0_conv mem_Collect_eq nat_le_linear nat_neq_iff reaching_tine_length_eq_label_depth tines_def)
     thus "False"
       using \<open>\<not> length (t @ [k]) \<le> depth F h\<close> by auto
   qed
hence "depth (Extend_tree (Cut_subtree F t k) t' (get_subtree F (t@[k]))) h
= Suc (size t') + depth F h - length (t@[k])"
  using \<open>depth (Extend_tree (Cut_subtree F t k) t' (get_subtree F (t @ [k]))) h = Suc (length t') + (depth F h - length (t @ [k]))\<close> by linarith
  hence "... = depth F h + Suc (size t') - size (t@[k])"
    by simp
   thus ?thesis
     by (simp add: \<open>depth (Extend_tree (Cut_subtree F t k) t' (get_subtree F (t @ [k]))) h = Suc (length t') + depth F h - length (t @ [k])\<close> assms(5))
 qed
qed

lemma get_subtree_strictly_inc:
  assumes "tinesP F t" "strictly_inc F" 
  shows "strictly_inc (get_subtree F t)"
  using assms
proof (induction)
  case (Sucs k F t)
  have "strictly_inc (sucs F!k)"
    by (metis (full_types) Sucs.hyps(1) Sucs.prems nth_mem rtree.sel(2) strictly_inc.cases)
  hence "strictly_inc (get_subtree (sucs F ! k) t)"
    by (simp add: Sucs.IH)
  then show ?case
    by (smt Sucs.hyps(1) get_subtree.simps list.simps(5) rtree.exhaust_sel)
next
  case (Nil F)
  then show ?case
    by (smt get_subtree.elims list.simps(4)) 
qed

lemma Cut_subtree_tines:
  assumes "tinesP F t" 
  shows "trace_tine (Cut_subtree F t n) t = trace_tine F t"
  using assms 
proof (induction arbitrary: n)
  case (Sucs k F t)
  obtain l Fs where "F = Tree l Fs"
    using rtree.exhaust_sel by blast
  hence "Cut_subtree F (k#t) n = Tree l ((take k Fs)@(Cut_subtree (Fs!k) t n)#(drop (Suc k) Fs))"
    by auto
  hence "trace_tine (Cut_subtree F (k#t) n) (k#t) = 
trace_tine (Tree l ((take k Fs)@(Cut_subtree (Fs!k) t n)#(drop (Suc k) Fs))) (k#t)"
    by simp
  hence "trace_tine (Cut_subtree F (k#t) n) (k#t) = 
l#(trace_tine (((take k Fs)@(Cut_subtree (Fs!k) t n)#(drop (Suc k) Fs)) ! k) t)"
    using Sucs.hyps(1) \<open>F = Tree l Fs\<close> by auto
  hence "trace_tine (Cut_subtree F (k#t) n) (k#t) = l#(trace_tine (Cut_subtree (Fs!k) t n) t)"
    by (metis Sucs.hyps(1) \<open>F = Tree l Fs\<close> nth_list_update_eq rtree.sel(2) upd_conv_take_nth_drop)
  hence "trace_tine (Cut_subtree F (k#t) n) (k#t) = l#(trace_tine (Fs!k) t)"
    using Sucs
    by (simp add: \<open>F = Tree l Fs\<close>)
  then show ?case
    using Sucs.hyps(1) \<open>F = Tree l Fs\<close> by auto 
next
  case (Nil F)
  then show ?case
    by (smt Cut_subtree.simps(1) list.simps(4) rtree.exhaust_sel trace_tine.simps)
qed

lemma Extend_tree_Cut_subtree_preserves_strictly_inc:
  assumes "F \<turnstile> w" "t@[k] \<in> tines F" "adversarial F t (honest_indices w)"
  shows "strictly_inc (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])]))"
proof -
  have 0:"t \<in> tines F"
    using assms butlast_tinesP
    using butlast_snoc tines_def by fastforce
  hence "strictly_inc (Cut_subtree F t k) "
    using Cut_subtree_strictly_inc assms
    by (metis fork_F forks_def mem_Collect_eq snoc_eq_iff_butlast tines_def)
  have "t \<noteq> []"
  proof (rule ccontr)
    assume "\<not> t \<noteq> []"
    hence "last (trace_tine F t) = label F"
      by (smt get_subtree.simps last_in_tine_is_label_in_subtree list.simps(4) rtree.exhaust_sel)
    hence "lb F t = 0"
      using fork_root0 lb_def assms
      by (metis fork_F forks_def honest_def honest_indices_iff) 
    thus "False"
      using assms
      by (metis \<open>last (trace_tine F t) = label F\<close> fork_F forks_def honest_def honest_indices_iff)
  qed
  hence "butlast t \<in> tines (Cut_subtree F t k)"
    using Cut_subtree_preserve_cut_tines \<open>t \<in> tines F\<close> butlast_tinesP tines_def by auto
  have "strictly_inc (get_subtree F (t@[k]))"
    using assms(1) assms(2) fork_F forks_def get_subtree_strictly_inc tines_def by auto
  hence "strictly_inc_list (trace_tine F (t@[k]))"
    using assms(1) assms(2) strictly_inc_list_trace_tine by blast
  have "trace_tine F t = take (Suc (size t)) (trace_tine F (t@[k]))"
    using assms 0
    by (simp add: take_trace_tine_commutative_2 tines_def)
  hence "last (trace_tine F t) = last (take (Suc (size t)) (trace_tine F (t@[k])))"
    by simp
  hence "last (trace_tine F t)  = (trace_tine F (t@[k])) ! (size t)"
    using assms(2) tines_def by auto
  hence "last (trace_tine F t) < (trace_tine F (t@[k])) ! Suc (size t)"
    using \<open>strictly_inc (get_subtree F (t@[k]))\<close>
    using \<open>strictly_inc_list (trace_tine F (t @ [k]))\<close> assms(2) less_imp_le_nat strictly_inc_list_as_def tines_def by fastforce
  hence "lb F t  < last (trace_tine F (t@[k]))"
    by (metis "0" Cons_nth_drop_Suc \<open>last (trace_tine F t) = trace_tine F (t @ [k]) ! length t\<close> \<open>trace_tine F t = take (Suc (length t)) (trace_tine F (t @ [k]))\<close> append_Nil2 append_eq_conv_conj append_take_drop_id assms(2) lb_def length_append_singleton lessI less_SucI linorder_neqE_nat mem_Collect_eq nat_neq_iff not_less_eq notfull_eq suc_tine_size_eq_label_list take_Suc_nth tines_def)
  hence "lb F t  < label (get_subtree F (t@[k]))"
    by (simp add: last_in_tine_is_label_in_subtree)
  have "(\<forall>f. f \<in> set [get_subtree F (t@[k])] \<longrightarrow> (lb F t) < label f) 
      \<and> (\<forall>f. f \<in> set [get_subtree F (t@[k])] \<longrightarrow> strictly_inc f)"
  using \<open>strictly_inc (get_subtree F (t@[k]))\<close>
  by (simp add: \<open>lb F t < label (get_subtree F (t @ [k]))\<close>)
  hence "strictly_inc (Tree (lb F t) [get_subtree F (t@[k])])"
    using strictly_inc.intros by blast
  have "trace_tine (Cut_subtree F t k) t = trace_tine F t"
    using Cut_subtree_tines
    using "0" tines_def by auto
  hence "trace_tine (Cut_subtree F t k) (butlast t) = trace_tine F (butlast t)"
    using Ouro.take_trace_tine_commutative_2  butlast_conv_take
    by (metis "0" Cut_subtree_preserve_cut_tines \<open>t \<noteq> []\<close>  append_butlast_last_id length_append_singleton length_butlast lessI mem_Collect_eq tines_def) 
  hence "strictly_inc_list (trace_tine (Cut_subtree F t k) (butlast t))"
    using assms(1) fork_F forks_def strictly_inc_imp_strictly_inc_tine strictly_inc_tine_def by auto  
  have "strictly_inc_list (trace_tine (Cut_subtree F t k) t)"
    using "0" \<open>trace_tine (Cut_subtree F t k) t = trace_tine F t\<close> assms(1) strictly_inc_list_trace_tine by auto
  hence "(trace_tine (Cut_subtree F t k) t) ! (size t - 1) < (trace_tine (Cut_subtree F t k) t) ! (size t)"
    by (metis "0" Suc_pred' \<open>t \<noteq> []\<close> \<open>trace_tine (Cut_subtree F t k) t = trace_tine F t\<close> length_greater_0_conv lessI strictly_inc_list_as_def suc_tine_size_eq_label_list tine_prefix_imp_both_tines tine_prefix_refl)
  hence "(trace_tine (Cut_subtree F t k) (butlast t)) ! (size t - 1) < (trace_tine (Cut_subtree F t k) t) ! (size t)"
    by (smt "0" One_nat_def Suc_diff_Suc \<open>butlast t \<in> tines (Cut_subtree F t k)\<close> \<open>trace_tine (Cut_subtree F t k) (butlast t) = trace_tine F (butlast t)\<close> \<open>trace_tine (Cut_subtree F t k) t = trace_tine F t\<close> butlast_conv_take diff_le_self diff_zero last_conv_nth length_butlast length_greater_0_conv lessI less_imp_diff_less mem_Collect_eq suc_tine_size_eq_label_list take_Suc_nth take_trace_tine_commutative tines_def trace_tine_non_empty)
  hence "last (trace_tine (Cut_subtree F t k) (butlast t)) < last (trace_tine (Cut_subtree F t k) t)"
    by (metis One_nat_def Suc_diff_Suc \<open>butlast t \<in> tines (Cut_subtree F t k)\<close> \<open>last (trace_tine F t) = trace_tine F (t @ [k]) ! length t\<close> \<open>t \<noteq> []\<close> \<open>trace_tine (Cut_subtree F t k) t = trace_tine F t\<close> \<open>trace_tine F t = take (Suc (length t)) (trace_tine F (t @ [k]))\<close> diff_zero last_conv_nth length_butlast length_greater_0_conv lessI mem_Collect_eq nth_take suc_tine_size_eq_label_list tines_def trace_tine_non_empty zero_less_diff)
  hence "label (get_subtree (Cut_subtree F t k) (butlast t)) < last (trace_tine F t)"
    using last_in_tine_is_label_in_subtree
    by (simp add: \<open>trace_tine (Cut_subtree F t k) t = trace_tine F t\<close>) 
  hence "label (get_subtree (Cut_subtree F t k) (butlast t)) < lb F t"
    using \<open>last (trace_tine F t) = trace_tine F (t @ [k]) ! length t\<close> \<open>trace_tine F t = take (Suc (length t)) (trace_tine F (t @ [k]))\<close> lb_def by auto
  hence "label (get_subtree (Cut_subtree F t k) (butlast t)) < label ((Tree (lb F t) [get_subtree F (t@[k])]))"
    by simp
  thus ?thesis
    by (simp add: Extend_tree_preserves_strictly_inc \<open>butlast t \<in> tines (Cut_subtree F t k)\<close> \<open>strictly_inc (Cut_subtree F t k)\<close> \<open>strictly_inc (Tree (lb F t) [get_subtree F (t @ [k])])\<close>)
qed

lemma Extend_tree_Cut_subtree_preserves_honest_depth_strictly_inc:
  assumes "F \<turnstile> w" "t@[k] \<in> tines F" "adversarial F t (honest_indices w)" "h1 \<in> honest_indices w" "h2 \<in> honest_indices w" "h1 < h2"
  shows "depth (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) h1 
< depth (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) h2"
proof - 
  have 1:"depth (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) h1 = depth F h1"
    using Extend_tree_Cut_subtree_preserves_honest_depth assms(1) assms(2) assms(3) assms(4) by auto
  have 2: "depth (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) h2 = depth F h2"
    using Extend_tree_Cut_subtree_preserves_honest_depth assms(1) assms(2) assms(3) assms(5) by auto
  thus ?thesis using 1 2
    using assms(1) assms(4) assms(5) assms(6) fork_F forks_def by auto
qed

lemma Extend_tree_Cut_subtree_preserves_limit:
  assumes "F \<turnstile> w" "t@[k] \<in> tines F" "adversarial F t (honest_indices w)" 
  shows "limit (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) (size w)"
proof - 
  have "mset_of_tree (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])]))
 = mset_of_tree (Cut_subtree F t k) + mset_of_tree (Tree (lb F t) [get_subtree F (t@[k])])"
    by (metis Cut_subtree_preserve_cut_tines Extend_tree_mset_of_tree assms(2) butlast_snoc butlast_tinesP mem_Collect_eq tines_def)
  hence "mset_of_tree (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])]))
 = mset_of_tree (Cut_subtree F t k) + {#lb F t#} + mset_of_tree (get_subtree F (t@[k]))"
    by simp
  hence tmp:"mset_of_tree (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])]))
 =mset_of_tree F + {#lb F t#}"
    using Cut_subtree_mset_of_tree assms(2) tines_def by auto
    have"\<forall>x. x \<in># (mset_of_tree F + {#lb F t#}) \<longrightarrow> x \<le> size w"
      by (metis add_diff_cancel_right' assms(1) assms(2) butlast_snoc butlast_tinesP fork_F forks_def lb_in_F mem_Collect_eq more_than_one_mset_mset_diff mset_of_tree_limit tines_def)
  thus ?thesis
    using tmp
    using all_bound_imp_limit by auto
qed

lemma Extend_tree_Cut_subtree_preserves_limit_2:
  assumes "F \<turnstile> w" "t@[k] \<in> tines F" "t' \<in> tines (Cut_subtree F t k)"
  shows "limit (Extend_tree (Cut_subtree F t k) t' (get_subtree F (t@[k]))) (size w)"
proof -
   have "mset_of_tree (Extend_tree (Cut_subtree F t k) t' (get_subtree F (t@[k])))
 = mset_of_tree (Cut_subtree F t k) + mset_of_tree (get_subtree F (t@[k]))"
     using Extend_tree_mset_of_tree assms(3) tine_prefix_imp_both_tines by blast
  hence tmp:"...
 =mset_of_tree F "
    using Cut_subtree_mset_of_tree assms(2) tines_def by auto
    have"\<forall>x. x \<in># (mset_of_tree F) \<longrightarrow> x \<le> size w"
      by (metis add_diff_cancel_right' assms(1) assms(2) butlast_snoc butlast_tinesP fork_F forks_def lb_in_F mem_Collect_eq more_than_one_mset_mset_diff mset_of_tree_limit tines_def)
  thus ?thesis
    by (simp add: \<open>mset_of_tree (Extend_tree (Cut_subtree F t k) t' (get_subtree F (t @ [k]))) = mset_of_tree (Cut_subtree F t k) + mset_of_tree (get_subtree F (t @ [k]))\<close> all_bound_imp_limit tmp)
qed


lemma Extend_tree_same_label:
"label (Extend_tree F t st) = label F"
proof - 
  obtain l Fs where "F = Tree l Fs"
    by (metis rtree.collapse)
  hence "label (Extend_tree F t st) = l"
    by (metis Extend_tree.simps(1) Extend_tree.simps(2) list.exhaust_sel rtree.sel(1))
  thus ?thesis
    by (simp add: \<open>F = Tree l Fs\<close>)
qed

lemma Cut_subtree_same_label:
"label (Cut_subtree F t st) = label F"
proof - 
  obtain l Fs where "F = Tree l Fs"
    by (metis rtree.collapse)
  hence "label (Cut_subtree F t st) = l"
    using Cut_subtree.simps list.exhaust_sel rtree.sel(1) by metis
  thus ?thesis
    by (simp add: \<open>F = Tree l Fs\<close>)
qed

lemma Extend_tree_Cut_subtree_preserves_fork:
  assumes "F \<turnstile> w" "t@[k] \<in> tines F" "adversarial F t (honest_indices w)" 
  shows "(Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) \<turnstile> w"
proof -
  have "t \<noteq> []"
    by (metis (no_types, lifting) assms(1) assms(3) fork_F forks_def honest_def honest_indices_iff last.simps list.simps(4) rtree.exhaust_sel trace_tine.simps)
  hence "label (Cut_subtree F t k) = label F"
    by (metis Cut_subtree.simps(2) list.exhaust rtree.exhaust_sel rtree.sel(1))
  hence "label (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) = label F"
    using \<open>t \<noteq> []\<close> assms Extend_tree_same_label by simp
  hence 0:"label (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) =  0"
    using assms(1) fork_F forks_def by auto
  have 1:"strictly_inc (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])]))"
    using Extend_tree_Cut_subtree_preserves_strictly_inc assms(1) assms(2) assms(3) by auto
  have 2:"limit (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) (size w)"
   by (simp add: Extend_tree_Cut_subtree_preserves_limit assms(1) assms(2) assms(3))
  have 3:"\<forall>h \<in> honest_indices w. count (mset_of_tree (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])]))) h = 1"
   using Extend_tree_Cut_subtree_preserves_unique_honest assms(1) assms(2) assms(3) by blast
  hence 4: "\<forall>i j. i \<in> honest_indices w \<and> j \<in> honest_indices w \<and> i < j \<longrightarrow>
depth (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) i <
depth (Extend_tree (Cut_subtree F t k) (butlast t) (Tree (lb F t) [get_subtree F (t@[k])])) j"
    using Extend_tree_Cut_subtree_preserves_honest_depth_strictly_inc assms(1) assms(2) assms(3) by blast
  thus ?thesis using assms 0 1 2 3
    by (simp add: "4" fork.intros forks_def) 
qed

lemma Extend_tree_with_no_adversarial_fork:
  assumes "F \<turnstile> w" "t \<in> tines F" "adversarial F t (honest_indices w)" "t \<noteq> []"
  shows "(Extend_tree F (butlast t) (Tree (lb F t) [])) \<turnstile> w"
proof -
  have 0:"label (Extend_tree F (butlast t) (Tree (lb F t) [])) = 0"
    using Extend_tree_same_label assms(1) fork_F forks_def by auto
  have 1:"strictly_inc (Extend_tree F (butlast t) (Tree (lb F t) []))"
  proof - 
    have "butlast t \<in> tines F"
      using assms
      by (simp add: butlast_tinesP tines_def)
    hence "label (get_subtree F (butlast t)) = last (trace_tine F (butlast t))"
      by (simp add: last_in_tine_is_label_in_subtree)
    hence "label (get_subtree F (butlast t)) < last (trace_tine F t)"
      by (smt append_butlast_last_id assms(1) assms(2) assms(4) butlast_conv_take diff_Suc_1 length_append_singleton lessI mem_Collect_eq nth_append_length nth_take strictly_inc_list_as_def strictly_inc_list_trace_tine suc_tine_size_eq_label_list take_trace_tine_commutative_2 tines_def trace_tine_non_empty)
    thus ?thesis
      by (metis Extend_tree_preserves_strictly_inc \<open>butlast t \<in> tines F\<close> append_butlast_last_id assms(1) assms(2) diff_Suc_1 exist_num_list fork_F forks_def gr_implies_not0 lb_def length_butlast list.size(3) mem_Collect_eq nth_append_length rtree.sel(1) strictly_inc.intros suc_tine_size_eq_label_list tines_def trace_tine_non_empty)
  qed
  have 2:"limit (Extend_tree F (butlast t) (Tree (lb F t) [])) (size w)"
    by (metis Extend_tree_preserves_limit assms(1) assms(2) butlast_tinesP exist_num_list fork_F forks_def gr_implies_not0 lb_in_F limit.simps list.size(3) mem_Collect_eq mset_of_tree_limit tines_def)
  have 3:"\<forall>h \<in> honest_indices w. count (mset_of_tree (Extend_tree F (butlast t) (Tree (lb F t) []))) h = 1"
  proof - 
    have tmp:"mset_of_tree (Extend_tree F (butlast t) (Tree (lb F t) [])) = 
          mset_of_tree F + {#lb F t#}"
      using Extend_tree_mset_of_tree assms(2) butlast_tinesP tines_def by auto
    have "lb F t \<notin> honest_indices w"
      using assms
      by (metis (full_types) append_Nil2 append_eq_conv_conj honest_def lb_def lessI mem_Collect_eq suc_tine_size_eq_label_list take_Suc_nth tines_def)
    thus ?thesis
      using tmp
      using assms(1) fork_F forks_def by auto
  qed
  hence aux4 : "\<forall>h \<in> honest_indices w. depth (Extend_tree F (butlast t) (Tree (lb F t) [])) h 
                = depth F h"
    by (smt Extend_tree_implies_prefix Extend_tree_mset_of_tree One_nat_def add_is_1 assms(1) assms(2) butlast_tinesP count_union exist_honest_tine last_in_set less_numeral_extra(3) mem_Collect_eq prefix_eq_depth_unique tines_def tines_exist_last_label_exist trace_tine_non_empty)
  hence "\<forall>i j. i\<in> honest_indices w \<and> j \<in> honest_indices w \<and> i < j 
\<longrightarrow> depth (Extend_tree F (butlast t) (Tree (lb F t) [])) i < depth (Extend_tree F (butlast t) (Tree (lb F t) []))j"
    using assms(1) fork_F forks_def by auto
  thus ?thesis
    using 0 1 2 3
    by (simp add: fork_F forks_def)
qed

lemma Extend_tree_trace_tine_1:
  assumes "tinesP F t" "t' \<in> tines F" "size t' \<le> size t"
  shows "trace_tine (Extend_tree F t st) t' = trace_tine F t'"
  using assms
proof (induction arbitrary: t')
  case (Sucs k F t)
    then show ?case 
    proof (cases t')
      case Nil
      then show ?thesis
        by (smt Extend_tree_same_label list.case(1) rtree.exhaust_sel trace_tine.simps)
    next     
      case (Cons k' tx')
       obtain l Fs where "F = Tree l Fs"
        using rtree.exhaust by blast
      hence ex:"Extend_tree F (k#t) st = 
              Tree l ((take k Fs) @ (Extend_tree (Fs ! k) t st) # (drop (Suc k) Fs))"
          by simp
      then show ?thesis      
      proof (cases "k = k'")
        case True 
        have "trace_tine (Extend_tree F (k#t) st) (k'#tx') = 
              l#(trace_tine (Extend_tree (Fs ! k) t st) tx')"
       
          
         by (smt Suc_diff_Suc Sucs.hyps(1) True \<open>F = Tree l Fs\<close> add_right_cancel append_take_drop_id diff_add_inverse ex length_Cons length_append length_drop less_imp_add_positive list.case(2) nth_append_length rtree.sel(2) trace_tine.simps)
        hence "trace_tine (Extend_tree F (k#t) st) (k'#tx') = 
              l#(trace_tine (Fs!k) tx')"
          using Sucs.IH Sucs.prems(1) Sucs.prems(2) True \<open>F = Tree l Fs\<close> local.Cons tinesP_Cons_iff tines_def by auto
        then show ?thesis
          using Sucs.hyps(1) True \<open>F = Tree l Fs\<close> local.Cons by auto 
      next
        case False  
        hence ne: "k' \<noteq> k"
          by simp
        hence "trace_tine (Extend_tree F (k#t) st) (k'#tx') = 
              l#(trace_tine (((take k Fs) @ (Extend_tree (Fs ! k) t st) # (drop (Suc k) Fs)) ! k') tx')"
        proof -
          have "k' < length (take k Fs @ Extend_tree (Fs ! k) t st # drop (Suc k) Fs)"
            by (metis (no_types) Suc_diff_Suc Sucs.hyps(1) Sucs.prems(1) \<open>F = Tree l Fs\<close> append_take_drop_id length_Cons length_append length_drop local.Cons rtree.sel(2) tine_prefix_Subtree tine_prefix_refl)
          then show ?thesis
            by (simp add: ex)
        qed
        hence tmp:"k' < k \<longrightarrow> trace_tine (Extend_tree F (k#t) st) (k'#tx') = 
             l# trace_tine ((take k Fs) ! k') tx'"
          using Sucs.hyps(1) \<open>F = Tree l Fs\<close> nth_list_update_neq_upd_conv_take_nth_drop rtree.sel(2) by fastforce
        have "k' > k \<longrightarrow> trace_tine (Extend_tree F (k#t) st) (k'#tx') = 
             l# trace_tine (Fs ! k') tx'"
          by (metis (full_types) Sucs.hyps(1) Sucs.prems(1) \<open>F = Tree l Fs\<close> \<open>trace_tine (Extend_tree F (k # t) st) (k' # tx') = l # trace_tine ((take k Fs @ Extend_tree (Fs ! k) t st # drop (Suc k) Fs) ! k') tx'\<close> local.Cons mem_Collect_eq ne nth_list_update_neq_upd_conv_take_nth_drop rtree.sel(2) tinesP_Cons_iff tines_def)
        hence "trace_tine (Extend_tree F (k#t) st) (k'#tx') = 
             l# trace_tine (Fs ! k') tx'"
          using False tmp
          by auto
        then show ?thesis
          using Sucs.prems(1) \<open>F = Tree l Fs\<close> local.Cons tinesP_Cons_iff tines_def by auto 
      qed
    qed
next
  case (Nil F)
  then show ?case
    by (smt Extend_tree_same_label list.case(1) list.size(3) rtree.exhaust_sel take_0 take_all trace_tine.simps) 
qed

lemma Extend_tree_trace_tine_tines_le:
  assumes "tinesP F t" "t' \<in> tines F" "size t' \<le> size t"
  shows "t' \<in> tines (Extend_tree F t st)"
  using assms
proof (induction arbitrary: t')
  case (Sucs k F t)
  then obtain l Fs where lFs:"F = Tree l Fs"
    using rtree.exhaust_sel by blast
  hence distri:"Extend_tree F (k#t) st = Tree l ((take k Fs) @ (Extend_tree (Fs ! k) t st) # (drop (Suc k) Fs))"
    by simp
have x:"\<forall>tt. tt \<in> tines (Fs ! k) \<and> (length tt \<le> length t) \<longrightarrow> tt \<in> tines (Extend_tree (Fs ! k) t st)"
  using lFs
  using Sucs.IH by auto  
  then show ?case 
  proof (cases t')
case Nil
  then show ?thesis
    by (simp add: tinesP.Nil tines_def) 
next
  case (Cons k' t'')
  hence 0:"size t'' \<le> size t"
    using Sucs.prems(2) by auto
  have "t'' \<in> tines (Fs!k')"
    using Cons assms
    using Sucs.prems(1) \<open>F = Tree l Fs\<close> tinesP_Cons_iff tines_def by auto
  then show ?thesis 
  proof (cases "k=k'")
    case True
    hence "t'' \<in> tines (Extend_tree (Fs ! k) t st)"
    using Sucs 0 lFs x
    using \<open>t'' \<in> tines (Fs ! k')\<close> by blast
  hence "k#t'' \<in> tines (Extend_tree F (k#t) st)"
    using distri
    by (smt Suc_diff_Suc Sucs.hyps(1) add_right_cancel append_take_drop_id diff_add_inverse lFs length_Cons length_append length_drop less_imp_add_positive mem_Collect_eq nth_append_length rtree.sel(2) tinesP_Cons_iff tines_def)
  then show ?thesis
    using True local.Cons by blast 
  next
    case False
    hence "(((take k Fs) @ (Extend_tree (Fs ! k) t st) # (drop (Suc k) Fs))!k') = Fs ! k'"
      using Sucs.hyps(1) Sucs.prems(1) lFs local.Cons nth_list_update_neq_upd_conv_take_nth_drop tinesP_Cons_iff tines_def by fastforce
    hence "t'' \<in> tines (((take k Fs) @ (Extend_tree (Fs ! k) t st) # (drop (Suc k) Fs))!k')"
      using \<open>t'' \<in> tines (Fs ! k')\<close> by auto
      then show ?thesis using distri
        using Sucs.prems(1) lFs local.Cons tinesP_Cons_iff tines_def by auto
  qed
qed
next
  case (Nil F)
  then show ?case
    by (simp add: tinesP.Nil tines_def) 
qed

definition "list_of S \<equiv> (inv_into (Collect distinct) set S)"

lemma finite_list_of: "finite A \<Longrightarrow> set (inv_into (Collect distinct) set A) = A"
  by (metis f_inv_into_f finite_distinct_list image_iff mem_Collect_eq)

lemma finite_list_of_size_card:
  assumes "finite A"
  shows "size (inv_into (Collect distinct) set A) = card A"
  using finite_list_of distinct_card
  by (metis assms finite_distinct_list image_eqI inv_into_into mem_Collect_eq)

definition tines_for_suffix:: "rtree \<Rightarrow> nat \<Rightarrow> (nat list) set" 
  where "tines_for_suffix F n = {t. tinesP F t \<and> size t = n}"

definition Suffix_pinched:: "rtree \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> rtree" 
  where "Suffix_pinched F n l = 
Tree l (map (get_subtree F) (list_of (tines_for_suffix F n)))"

lemma strictly_inc_Suffix_pinched:
  assumes "strictly_inc F" "\<forall>t \<in> tines F. size t = n \<longrightarrow> l < label (get_subtree F t)"
  shows "strictly_inc (Suffix_pinched F n l)"
proof -
  have 0:"\<forall>m \<in> set (list_of (tines_for_suffix F n)). tinesP F m \<and> size m = n"
    by (metis (mono_tags) finite_list_of finite_Collect_conjI list_of_def mem_Collect_eq tines_def tines_finite tines_for_suffix_def)
  hence 1: "\<forall>f \<in> set (map (get_subtree F) (list_of (tines_for_suffix F n))). l < label f"
    using assms
    by (simp add: tines_def)
  hence "\<forall>f \<in> set (map (get_subtree F) (list_of (tines_for_suffix F n))). strictly_inc f"
    using assms 0
    by (simp add: get_subtree_strictly_inc)
  thus ?thesis
    by (metis "local.1" Suffix_pinched_def assms(1) strictly_inc.simps)
qed

lemma limit_Suffix_pinched:
  assumes "limit F x" "l \<le> x"
  shows "limit (Suffix_pinched F n l) x"
proof -
  have "(Suffix_pinched F n l) = 
Tree l (map (get_subtree F) (list_of (tines_for_suffix F n)))"
    by (simp add: Suffix_pinched_def)
  have "\<forall>f \<in> set (map (get_subtree F) (list_of (tines_for_suffix F n))). \<exists>t. f = get_subtree F t"
    by auto
  hence "\<forall>f \<in> set (map (get_subtree F) (list_of (tines_for_suffix F n))). 
\<forall>ll \<in># mset_of_tree f. ll \<in># mset_of_tree F "
    by (metis count_greater_zero_iff count_inI count_subtree_le_count get_subtree_is_subtree leD)
  hence "\<forall>f \<in> set (map (get_subtree F) (list_of (tines_for_suffix F n))). 
\<forall>ll \<in># mset_of_tree f. ll \<le> x"
    using assms
    using mset_of_tree_limit by blast
hence "\<forall>f \<in> set (map (get_subtree F) (list_of (tines_for_suffix F n))). 
limit f x"
  using all_bound_imp_limit by blast
  then show ?thesis
    using assms
    by (metis \<open>Suffix_pinched F n l = Tree l (map (get_subtree F) (list_of (tines_for_suffix F n)))\<close> limit.intros)
qed

fun reduce_tree:: "rtree \<Rightarrow> nat \<Rightarrow> rtree" 
  where "reduce_tree (Tree l Fs) n = Tree (l - n) (map (\<lambda>x. reduce_tree x n) Fs)"

lemma strictly_inc_reduce_tree:
  assumes "strictly_inc F" "n \<le> label F" 
  shows "strictly_inc (reduce_tree F n)"
  using assms
proof (induction)
  case (1 Fs l)
  have "\<forall>f. f \<in> set Fs \<longrightarrow> l \<le> label f"
    by (simp add: "1.hyps"(1) less_imp_le)
  hence "\<forall>f. f \<in> set Fs \<longrightarrow> n \<le> label f"
    using "1.prems" by auto
  hence "\<forall>f. f \<in> set Fs \<longrightarrow> strictly_inc (reduce_tree f n)"
    using "1.IH" by blast
  have "\<forall>f. f \<in> set Fs \<longrightarrow> l - n < label (reduce_tree f n)"
    by (metis "1.hyps"(1) "1.prems" diff_less_mono less_or_eq_imp_le reduce_tree.simps rtree.exhaust_sel rtree.sel(1))
  then show ?case
    by (metis (no_types, lifting) \<open>\<forall>f. f \<in> set Fs \<longrightarrow> strictly_inc (reduce_tree f n)\<close> imageE reduce_tree.simps set_map strictly_inc.intros) 
qed

lemma reduce_tree_tines:
  assumes "tinesP F t" 
  shows "t \<in> tines (reduce_tree F n)"
  using assms
proof (induction)
  case (Sucs k F t)
  hence "tinesP (sucs F ! k) t"
    by simp
  hence "t \<in> tines (reduce_tree (sucs F ! k) n)"
    by (simp add: Sucs.IH)
  have "(map (\<lambda>x. reduce_tree x n) (sucs F)) ! k = (reduce_tree (sucs F ! k) n)"
    by (simp add: Sucs.hyps(1))

  then show ?case
    by (metis Sucs.IH Sucs.hyps(1) length_map mem_Collect_eq nth_map reduce_tree.simps rtree.collapse rtree.sel(2) tinesP_Cons_iff tines_def) 

next
  case (Nil F)
  then show ?case
    by (simp add: tinesP.Nil tines_def) 
qed

lemma reduce_tree_count:
  assumes "n \<le> label F" "strictly_inc F"
  shows "count (mset_of_tree (reduce_tree F n)) h = count (mset_of_tree F) (h + n)"
  using assms
proof (induction F)
  case (Tree l Fs)
  have ih:"\<forall>f \<in> set Fs.
    n \<le> label f \<and> strictly_inc f"
    using assms
    by (metis Tree.prems(1) Tree.prems(2) dual_order.trans less_imp_le rtree.sel(1) rtree.sel(2) strictly_inc.simps)
  hence ih2:"\<forall>f \<in> set Fs. count (mset_of_tree (reduce_tree f n)) h = count (mset_of_tree f) (h + n)"
    using Tree
    by auto
  have "mset_of_tree (reduce_tree (Tree l Fs) n) =
{#l - n#} +  (\<Sum>x \<in># mset (map (\<lambda>x. reduce_tree x n) Fs). mset_of_tree x)"
    by simp
  hence "count (mset_of_tree (reduce_tree (Tree l Fs) n)) h
= count {#l-n#}  h +
count (\<Sum>x \<in># mset (map (\<lambda>x. reduce_tree x n) Fs). mset_of_tree x) h"
    by simp
 hence "count (mset_of_tree (reduce_tree (Tree l Fs) n)) h
= count {#l-n#}  h +
 (\<Sum>x \<in># mset (map (\<lambda>x. reduce_tree x n) Fs). count (mset_of_tree x) h)"
   using count_mset_of_tree_unfold by auto
  have "(\<Sum>x \<in># mset (map (\<lambda>x. reduce_tree x n) Fs). count (mset_of_tree x) h)
= (\<Sum>x \<in># mset Fs. count (mset_of_tree (reduce_tree x n)) h)"
  using ih
  proof (induction Fs)
    case Nil
    then show ?case
      by auto 
  next
    case (Cons kFs tFs)
    hence "\<forall>f \<in> set (kFs#tFs).
    n \<le> label f \<and> strictly_inc f"
      by simp
    hence "(\<Sum>x\<in>#mset (map (\<lambda>x. reduce_tree x n) tFs). count (mset_of_tree x) h) =
    (\<Sum>x\<in>#mset tFs. count (mset_of_tree (reduce_tree x n)) h)"
      using Cons.IH by auto
    hence "(\<Sum>x\<in>#mset (map (\<lambda>x. reduce_tree x n) tFs) + {#reduce_tree kFs n#}. count (mset_of_tree x) h) =
    (\<Sum>x\<in>#mset tFs + {#kFs#}. count (mset_of_tree (reduce_tree x n)) h)"
      by auto
    hence "(\<Sum>x\<in>#mset (map (\<lambda>x. reduce_tree x n) (kFs#tFs)). count (mset_of_tree x) h) =
    (\<Sum>x\<in>#mset (kFs # tFs). count (mset_of_tree (reduce_tree x n)) h)"
      by auto
    then show ?case
      by simp 
  qed
  hence "count (mset_of_tree (reduce_tree (Tree l Fs) n)) h = count {#l-n#}  h +
count (\<Sum>x \<in># mset Fs. mset_of_tree (reduce_tree x n)) h"
    by (metis \<open>count (mset_of_tree (reduce_tree (Tree l Fs) n)) h = count {#l - n#} h + count (\<Union>#image_mset mset_of_tree (mset (map (\<lambda>x. reduce_tree x n) Fs))) h\<close> list_mset_map_sum_assoc)
  hence "... = count {#l-n#}  h +
 (\<Sum>x \<in># mset Fs. count (mset_of_tree (reduce_tree x n)) h)"
    using \<open>(\<Sum>x\<in>#mset (map (\<lambda>x. reduce_tree x n) Fs). count (mset_of_tree x) h) = (\<Sum>x\<in>#mset Fs. count (mset_of_tree (reduce_tree x n)) h)\<close> \<open>count (mset_of_tree (reduce_tree (Tree l Fs) n)) h = count {#l - n#} h + (\<Sum>x\<in>#mset (map (\<lambda>x. reduce_tree x n) Fs). count (mset_of_tree x) h)\<close> by auto
hence "... = count {#l-n#}  h +
 (\<Sum>x \<in># mset Fs. count (mset_of_tree x) (h + n))"
  using ih2
  by (metis (no_types, lifting) count_eq_zero_iff count_mset_0_iff image_mset_cong)
hence "... = count {#l-n#}  h +
 count (\<Sum>x \<in># mset Fs. mset_of_tree x) (h + n)"
  by (simp add: count_mset_of_tree_unfold)
hence "... = count {#l#}  (h + n) +
 count (\<Sum>x \<in># mset Fs. mset_of_tree x) (h + n)"
  using Tree.prems(1) by auto
  thus ?case
    using \<open>(\<Sum>x\<in>#mset (map (\<lambda>x. reduce_tree x n) Fs). count (mset_of_tree x) h) = (\<Sum>x\<in>#mset Fs. count (mset_of_tree (reduce_tree x n)) h)\<close> \<open>count (mset_of_tree (reduce_tree (Tree l Fs) n)) h = count {#l - n#} h + (\<Sum>x\<in>#mset (map (\<lambda>x. reduce_tree x n) Fs). count (mset_of_tree x) h)\<close> \<open>count {#l - n#} h + (\<Sum>x\<in>#mset Fs. count (mset_of_tree (reduce_tree x n)) h) = count {#l - n#} h + (\<Sum>x\<in>#mset Fs. count (mset_of_tree x) (h + n))\<close> \<open>count {#l - n#} h + (\<Sum>x\<in>#mset Fs. count (mset_of_tree x) (h + n)) = count {#l - n#} h + count (\<Union>#image_mset mset_of_tree (mset Fs)) (h + n)\<close> by auto
qed

lemma reduce_tree_trace_tine:
  assumes "tinesP F t"
  shows  "map (\<lambda>x. x - n) (trace_tine F t) = (trace_tine (reduce_tree F n) t)"
  using assms
proof (induction)
  case (Sucs k F t)
  obtain l Fs where "F = Tree l Fs"
    using rtree.exhaust by blast
  hence "(reduce_tree F n) = Tree (l - n) (map (\<lambda>x. reduce_tree x n) Fs)"
    by simp
  hence "(trace_tine (reduce_tree F n) (k#t)) = 
(l-n) # (trace_tine ((map (\<lambda>x. reduce_tree x n) Fs) ! k) t )"
    using Sucs.hyps(1) \<open>F = Tree l Fs\<close> by auto
hence "(trace_tine (reduce_tree F n) (k#t)) = 
(l-n) # (trace_tine (reduce_tree (Fs ! k) n) t )"
  using Sucs.hyps(1) \<open>F = Tree l Fs\<close> by auto
  hence "... = (l-n) # map (\<lambda>x. x - n) (trace_tine (Fs!k) t)"
    using Sucs.IH \<open>F = Tree l Fs\<close> by auto
hence "... =  map (\<lambda>x. x - n) (l # (trace_tine (Fs!k) t))"
    using Sucs.IH \<open>F = Tree l Fs\<close> by auto
  then show ?case
    using \<open>(l - n) # trace_tine (reduce_tree (Fs ! k) n) t = (l - n) # map (\<lambda>x. x - n) (trace_tine (Fs ! k) t)\<close> \<open>F = Tree l Fs\<close> by fastforce 
next
  case (Nil F)
  then show ?case
    by (metis (no_types, lifting) list.simps(4) list.simps(8) list.simps(9) reduce_tree.simps rtree.exhaust trace_tine.simps) 
qed

lemma reduce_tree_last_trace_tine:
  assumes "tinesP F t"  "n \<le> label F" "strictly_inc F" 
  shows "last (trace_tine F t) = last (trace_tine (reduce_tree F n) t) + n"
  using assms
proof (induction)
  case (Sucs k F t)
  then obtain l Fs where "Tree l Fs = F"
    using rtree.collapse by blast
  hence "k < size Fs"
    using Sucs.hyps(1) by auto
  have "label (Fs!k) \<ge> n"
    by (metis Sucs.prems(1) Sucs.prems(2) \<open>Tree l Fs = F\<close> \<open>k < length Fs\<close> le_trans less_imp_le nth_mem rtree.sel(1) rtree.sel(2) strictly_inc.simps)
  have "strictly_inc (Fs!k)"
    by (metis Sucs.prems(2) \<open>Tree l Fs = F\<close> \<open>k < length Fs\<close> nth_mem rtree.inject strictly_inc.cases)
  hence "last (trace_tine (Fs!k) t) = last (trace_tine (reduce_tree (Fs!k) n) t) + n"
    using Sucs.IH \<open>Tree l Fs = F\<close> \<open>n \<le> label (Fs ! k)\<close> by auto
  hence "last (l# trace_tine (Fs!k) t) = last (trace_tine (reduce_tree (Fs!k) n) t) + n"
    by (simp add: trace_tine_non_empty)
hence "last (trace_tine F (k#t)) = last (trace_tine (reduce_tree (Fs!k) n) t) + n"
  using \<open>Tree l Fs = F\<close> \<open>k < length Fs\<close> by auto
hence "last (trace_tine F (k#t)) = last ((l - n) #trace_tine (reduce_tree (Fs!k) n) t) + n"
  by (simp add: trace_tine_non_empty)
  hence "last (trace_tine F (k#t)) = last (trace_tine (Tree (l-n) (map (\<lambda>x. reduce_tree x n) Fs)) (k#t)) + n"
    by (simp add: \<open>k < length Fs\<close>)
  then show ?case
    using \<open>Tree l Fs = F\<close> by auto 
next
  case (Nil F)
  have "last (trace_tine (reduce_tree F n) []) = label (reduce_tree F n)"
    by (metis (no_types, lifting) last.simps list.case_eq_if rtree.collapse trace_tine.simps)
  hence "last (trace_tine (reduce_tree F n) []) + n = label F"
    by (metis Nil.prems(1) Nil.prems(2) ordered_cancel_comm_monoid_diff_class.le_imp_diff_is_add reduce_tree.simps rtree.sel(1) strictly_inc.cases)
   
  then show ?case
    by (metis (no_types, lifting) last.simps list.case_eq_if rtree.collapse trace_tine.simps) 
qed

lemma count_honest_suffix_pinched_le_1:
  assumes "count (mset_of_tree F) h = 1"
  shows "count (\<Sum>x \<in># mset (map (get_subtree F) (list_of (tines_for_suffix F n))). mset_of_tree x) h \<le> 1"
proof - 
  obtain th where  "th \<in> tines F" "last (trace_tine F th) = h"
    using assms exist_unique_label_exist_unique_tine by blast
  hence thu:"\<forall>t. t \<in> tines F \<and> last (trace_tine F t) = h \<longrightarrow> t=th"
    using assms exist_unique_label_exist_unique_tine by blast
  thus ?thesis
  proof (cases "size th < n")
    case True
    have "\<not> (\<exists>t. size t = n \<and> t \<in> tines F \<and> count (mset_of_tree (get_subtree F t)) h > 0)"
    proof (rule ccontr)
      assume "\<not> \<not> (\<exists>t. size t = n \<and> t \<in> tines F \<and> count (mset_of_tree (get_subtree F t)) h > 0)"
      then obtain t where "size t = n" "t \<in> tines F" "count (mset_of_tree (get_subtree F t)) h > 0"
        by blast
      then obtain t' where "t' \<in> tines (get_subtree F t)" "last (trace_tine (get_subtree F t) t') = h"
        using exist_label_exist_tine by auto
      hence "t@t' \<in> tines F \<and> last (trace_tine F (t@t')) = h"
        by (metis \<open>t \<in> tines F\<close> append_eq_conv_conj get_subtree_append last_in_tine_is_label_in_subtree mem_Collect_eq tine_append_tine_of_subtree_is_tine tines_def)
      hence "t@t' = th" using thu
        by blast
      thus "False"
        using True \<open>length t = n\<close> by auto
    qed
    have "\<forall>t. size t \<noteq> n \<or> t \<notin> tines F \<or> count (mset_of_tree (get_subtree F t)) h \<le> 0"
      using \<open>\<nexists>t. length t = n \<and> t \<in> tines F \<and> 0 < count (mset_of_tree (get_subtree F t)) h\<close> leI 
      by blast
    hence "\<forall>t. size t = n \<and> t \<in> tines F \<longrightarrow> count (mset_of_tree (get_subtree F t)) h = 0"
      by blast
    hence "\<forall>t \<in> set (list_of (tines_for_suffix F n)). count (mset_of_tree (get_subtree F t)) h = 0"
      by (metis (mono_tags) finite_list_of finite_Collect_conjI list_of_def mem_Collect_eq tines_def tines_finite tines_for_suffix_def)
    hence "\<forall>x \<in># mset (map (get_subtree F) (list_of (tines_for_suffix F n))). count (mset_of_tree x) h = 0"
      by simp
    then show ?thesis
      using all_count_zero_imp_count_zero_branch set_mset_mset by fastforce 
  next
    case False
    hence "size th \<ge> n"
      using not_less by blast
    then obtain thf ths where "th = thf@ths" "size thf = n"
      by (metis append_take_drop_id length_take min_absorb2)
    hence "thf \<in> tines F \<and> ths \<in> tines (get_subtree F thf)"
      by (metis \<open>th \<in> tines F\<close> append_eq_conv_conj mem_Collect_eq mod_get_subtree_tines_append_in_tines take_tine_is_tine tines_def)
    hence "thf \<in> set (list_of (tines_for_suffix F n))"
      by (metis (mono_tags) finite_list_of \<open>length thf = n\<close> finite_Collect_conjI list_of_def mem_Collect_eq tines_def tines_finite tines_for_suffix_def)
    then obtain k where k:"size (list_of (tines_for_suffix F n)) > k " "(list_of (tines_for_suffix F n)) ! k = thf"
       using \<open>length thf = n\<close> \<open>th = thf @ ths\<close> \<open>th \<in> tines F\<close>
       by (meson exist_num_list)
     have "count (mset_of_tree (get_subtree F thf)) h \<le> 1"
       using assms
       by (metis count_subtree_le_count get_subtree_is_subtree)
     hence "count (mset_of_tree (get_subtree F thf)) h = 1"
       by (metis \<open>\<And>thesis. (\<And>th. \<lbrakk>th \<in> tines F; last (trace_tine F th) = h\<rbrakk> \<Longrightarrow> thesis) \<Longrightarrow> thesis\<close> \<open>length thf = n\<close> \<open>n \<le> length th\<close> \<open>th = thf @ ths\<close> \<open>thf \<in> tines F \<and> ths \<in> tines (get_subtree F thf)\<close> append_Nil2 append_eq_append_conv_if count_greater_eq_one_iff exist_label_eq_exist_tine get_subtree_append last_in_tine_is_label_in_subtree le_antisym mem_Collect_eq thu tines_def)
     have "\<not> (\<exists>t. size t = n \<and> t \<in> tines F \<and> count (mset_of_tree (get_subtree F t)) h > 0 \<and> t \<noteq> thf)"
    proof (rule ccontr)
      assume "\<not>\<not> (\<exists>t. size t = n \<and> t \<in> tines F \<and> count (mset_of_tree (get_subtree F t)) h > 0 \<and> t \<noteq> thf)"
      then obtain t where "size t = n" "t \<in> tines F" "count (mset_of_tree (get_subtree F t)) h > 0" "t \<noteq> thf"
        by blast
      then obtain t' where "t' \<in> tines (get_subtree F t)" "last (trace_tine (get_subtree F t) t') = h"
        using exist_label_exist_tine by auto
      hence "t@t' \<in> tines F \<and> last (trace_tine F (t@t')) = h"
        by (metis \<open>t \<in> tines F\<close> append_eq_conv_conj get_subtree_append last_in_tine_is_label_in_subtree mem_Collect_eq tine_append_tine_of_subtree_is_tine tines_def)
      hence "t@t' = th" using thu
        by blast
      thus "False"
        using \<open>length t = n\<close> \<open>length thf = n\<close> \<open>t \<noteq> thf\<close> \<open>th = thf @ ths\<close> by auto
    qed
    have "\<forall>t. size t \<noteq> n \<or> t \<notin> tines F \<or> count (mset_of_tree (get_subtree F t)) h \<le> 0 \<or> t = thf"
      using \<open>\<nexists>t. length t = n \<and> t \<in> tines F \<and> 0 < count (mset_of_tree (get_subtree F t)) h \<and> t \<noteq> thf\<close> leI by blast
     hence "\<forall>t. size t = n \<and> t \<in> tines F \<and> t \<noteq> thf \<longrightarrow> count (mset_of_tree (get_subtree F t)) h = 0"
       by blast
     hence "\<forall>t \<in> set (list_of (tines_for_suffix F n)). t \<noteq> thf \<longrightarrow>  count (mset_of_tree (get_subtree F t)) h = 0"
       by (metis (mono_tags) finite_list_of Diff_iff finite_Collect_conjI insertI1 list_of_def mem_Collect_eq tines_def tines_finite tines_for_suffix_def)
     hence notk:"\<forall>k'. k' < size (list_of (tines_for_suffix F n)) \<and>  k \<noteq> k' \<longrightarrow>  count (mset_of_tree (get_subtree F ((list_of (tines_for_suffix F n)) !k'))) h = 0"
     proof -
       have "\<forall>k'. k' < size (list_of (tines_for_suffix F n)) \<and> k \<noteq> k' \<longrightarrow> list_of (tines_for_suffix F n) ! k' \<noteq> thf"
       proof (rule ccontr)
         assume "\<not> (\<forall>k'. k' < size (list_of (tines_for_suffix F n)) \<and> k \<noteq> k' \<longrightarrow> list_of (tines_for_suffix F n) ! k' \<noteq> thf)"
         then obtain k' where k':"k' < size (list_of (tines_for_suffix F n)) \<and> k \<noteq> k' \<and> list_of (tines_for_suffix F n) ! k' = thf"
           by blast
         hence "count (mset (list_of (tines_for_suffix F n))) thf \<ge> 2"
         proof (cases "k < k'")
           case True
           have "list_of (tines_for_suffix F n) = (take k (list_of (tines_for_suffix F n))) @ (drop k (list_of (tines_for_suffix F n))) "
             by simp
           hence "list_of (tines_for_suffix F n) = (take k (list_of (tines_for_suffix F n))) @ (drop k ((take k' (list_of (tines_for_suffix F n))) @ (drop k' (list_of (tines_for_suffix F n)))))"
             using True
             by auto
           hence "list_of (tines_for_suffix F n) = (take k (list_of (tines_for_suffix F n))) @ ((drop k (take k' (list_of (tines_for_suffix F n)))) @ (drop k' (list_of (tines_for_suffix F n))))"
             using True k k'
           proof - 
             have "size (take k' (list_of (tines_for_suffix F n))) > k"
               by (simp add: True k' less_or_eq_imp_le min_absorb2)
             thus ?thesis
               by (smt Nat.add_diff_assoc2 append_assoc append_eq_append_conv append_take_drop_id length_append length_drop less_imp_le_nat)
           qed
           hence "list_of (tines_for_suffix F n) = (take k (list_of (tines_for_suffix F n))) @ (drop k (take k' (list_of (tines_for_suffix F n)))) @ (drop k' (list_of (tines_for_suffix F n)))"
             by blast
           hence "count (mset (list_of (tines_for_suffix F n))) thf = count (mset (take k (list_of (tines_for_suffix F n)))) thf + count (mset (drop k (take k' (list_of (tines_for_suffix F n))))) thf + count (mset (drop k' (list_of (tines_for_suffix F n)))) thf "
             by (metis append.assoc count_union mset_append)
           hence "... \<ge> 2"
           proof -
             have "hd (drop k (take k' (list_of (tines_for_suffix F n)))) = thf"
               using True k k'
               by (simp add: hd_drop_conv_nth)
             hence "count (mset (drop k (take k' (list_of (tines_for_suffix F n))))) thf \<ge> 1"
               using True k(1) by auto
have "hd (drop k' (list_of (tines_for_suffix F n)))  = thf"
               using True k k'
               by (simp add: hd_drop_conv_nth)
             hence "count (mset (drop k' (list_of (tines_for_suffix F n)))) thf \<ge> 1"
               using k' by auto
             thus ?thesis
               using \<open>1 \<le> count (mset (drop k (take k' (list_of (tines_for_suffix F n))))) thf\<close> by linarith
           qed
           then show ?thesis
             using \<open>count (mset (list_of (tines_for_suffix F n))) thf = count (mset (take k (list_of (tines_for_suffix F n)))) thf + count (mset (drop k (take k' (list_of (tines_for_suffix F n))))) thf + count (mset (drop k' (list_of (tines_for_suffix F n)))) thf\<close> by linarith 

         next
           case False
           hence "k' < k"
             using antisym_conv3 k' by blast
  have "list_of (tines_for_suffix F n) = (take k' (list_of (tines_for_suffix F n))) @ (drop k' (list_of (tines_for_suffix F n))) "
             by simp
           hence "list_of (tines_for_suffix F n) = (take k' (list_of (tines_for_suffix F n))) @ (drop k' ((take k (list_of (tines_for_suffix F n))) @ (drop k (list_of (tines_for_suffix F n)))))"
             using False
             by auto
           hence "list_of (tines_for_suffix F n) = (take k' (list_of (tines_for_suffix F n))) @ ((drop k' (take k (list_of (tines_for_suffix F n)))) @ (drop k (list_of (tines_for_suffix F n))))"
           proof - 
             have "size (take k (list_of (tines_for_suffix F n))) > k'"
               by (simp add: \<open>k' < k\<close> k(1) less_or_eq_imp_le min_absorb2)
             thus ?thesis
               by (metis \<open>k' < k\<close> append_assoc append_take_drop_id drop_take le_add_diff_inverse less_or_eq_imp_le take_add)
           qed
           hence "count (mset (list_of (tines_for_suffix F n))) thf = count (mset (take k' (list_of (tines_for_suffix F n)))) thf + count (mset (drop k' (take k (list_of (tines_for_suffix F n))))) thf + count (mset (drop k (list_of (tines_for_suffix F n)))) thf "
             by (metis append.assoc count_union mset_append)
           hence "... \<ge> 2"
           proof -
             have "hd (drop k' (take k (list_of (tines_for_suffix F n)))) = thf"
               by (simp add: \<open>k' < k\<close> hd_drop_conv_nth k')
             hence "count (mset (drop k' (take k (list_of (tines_for_suffix F n))))) thf \<ge> 1"
               using \<open>k' < k\<close> k(1) by auto
             have "hd (drop k (list_of (tines_for_suffix F n)))  = thf"
               by (simp add: hd_drop_conv_nth k(1) k(2))
             hence "count (mset (drop k (list_of (tines_for_suffix F n)))) thf \<ge> 1"
               using k(1) by auto
             thus ?thesis
               using \<open>1 \<le> count (mset (drop k' (take k (list_of (tines_for_suffix F n))))) thf\<close> by linarith
           qed
           then show ?thesis
             using \<open>count (mset (list_of (tines_for_suffix F n))) thf = count (mset (take k' (list_of (tines_for_suffix F n)))) thf + count (mset (drop k' (take k (list_of (tines_for_suffix F n))))) thf + count (mset (drop k (list_of (tines_for_suffix F n)))) thf\<close> by linarith 
         qed
         hence "\<not> distinct (list_of (tines_for_suffix F n))"
           using k' k(1) k(2) nth_eq_iff_index_eq by blast
         hence "card (set (list_of (tines_for_suffix F n))) < size (list_of (tines_for_suffix F n))"
           using card_distinct card_length nat_less_le by blast
         thus "False"
           using list_of_def
           by (metis finite_list_of \<open>\<not> distinct (list_of (tines_for_suffix F n))\<close> card_distinct finite_Collect_conjI finite_list_of_size_card tines_def tines_finite tines_for_suffix_def)
       qed
       thus "\<forall>k'. k' < size (list_of (tines_for_suffix F n)) \<and>  k \<noteq> k' \<longrightarrow>  count (mset_of_tree (get_subtree F ((list_of (tines_for_suffix F n)) !k'))) h = 0"
       by (simp add: \<open>\<forall>t\<in>set (list_of (tines_for_suffix F n)). t \<noteq> thf \<longrightarrow> count (mset_of_tree (get_subtree F t)) h = 0\<close>)
   qed
   have "count (\<Sum>x \<in># mset (map (get_subtree F) (list_of (tines_for_suffix F n))). mset_of_tree x) h 
= count (\<Sum>x \<in># mset (map (get_subtree F) (
(take k (list_of (tines_for_suffix F n)))
@(drop k (list_of (tines_for_suffix F n)))
)). mset_of_tree x) h "
     by simp
   hence "...
= count (\<Sum>x \<in># mset (map (get_subtree F) (
(take k (list_of (tines_for_suffix F n)))
@ (hd (drop k (list_of (tines_for_suffix F n))) # tl (drop k (list_of (tines_for_suffix F n))))
)). mset_of_tree x) h "
     using k(1) by auto
   hence "...
= count (\<Sum>x \<in># mset (map (get_subtree F) (
(take k (list_of (tines_for_suffix F n)))
@ ((list_of (tines_for_suffix F n)) ! k # tl (drop k (list_of (tines_for_suffix F n))))
)). mset_of_tree x) h "
     by (simp add: hd_drop_conv_nth k(1))
hence "...
= count (\<Sum>x \<in># mset (map (get_subtree F) (
((take k (list_of (tines_for_suffix F n)))
@ (tl (drop k (list_of (tines_for_suffix F n))))) 
)) + {#get_subtree F ((list_of (tines_for_suffix F n)) ! k )#}. mset_of_tree x) h "
  by (simp add: hd_drop_conv_nth k(1))
  hence "...
= count (\<Sum>x \<in># mset (map (get_subtree F) (
((take k (list_of (tines_for_suffix F n)))
@ (tl (drop k (list_of (tines_for_suffix F n))))) 
)) . mset_of_tree x) h + count (mset_of_tree (get_subtree F ((list_of (tines_for_suffix F n)) ! k ))) h"
    by (simp add: hd_drop_conv_nth k(1))
 hence "...
= count (\<Sum>x \<in># mset (map (get_subtree F) (
((take k (list_of (tines_for_suffix F n)))
@ (tl (drop k (list_of (tines_for_suffix F n))))) 
)) . mset_of_tree x) h + count (mset_of_tree (get_subtree F thf)) h"
   using k by auto
  hence "... = count (\<Sum>x \<in># mset (map (get_subtree F) (
((take k (list_of (tines_for_suffix F n)))
@ (tl (drop k (list_of (tines_for_suffix F n))))) 
)) . mset_of_tree x) h + 1"
    using \<open>count (mset_of_tree (get_subtree F thf)) h = 1\<close> by linarith
 hence "... = count (\<Sum>x \<in># mset (map (get_subtree F) (
((take k (list_of (tines_for_suffix F n)))
@ ((drop (Suc k) (list_of (tines_for_suffix F n))))) 
)) . mset_of_tree x) h + 1"
   by (simp add: drop_Suc tl_drop)
  hence "... = 1"
  proof -
    have 0: "\<forall>x \<in> set ((take k (list_of (tines_for_suffix F n)))). 
(\<exists> k'. k' \<noteq> k \<and> k' < size (list_of (tines_for_suffix F n)) \<and> x = list_of (tines_for_suffix F n) ! k')"
      by (metis exist_num_list k(1) length_take min_less_iff_conj nat_less_le nth_take)
    have  "\<forall>x \<in> set ((drop (Suc k) (list_of (tines_for_suffix F n)))). 
(\<exists> k'.  k' < size (list_of (tines_for_suffix F n)) \<and> x = list_of (tines_for_suffix F n) ! k')"
      by (metis exist_num_list in_set_dropD)
    hence "\<And>x. x \<in> set ((drop (Suc k) (list_of (tines_for_suffix F n)))) \<Longrightarrow> 
(\<exists> k'.  k' \<noteq> k  \<and> k' < size (list_of (tines_for_suffix F n)) \<and> x = list_of (tines_for_suffix F n) ! k')"
    proof -
      fix x 
      assume "x \<in> set ((drop (Suc k) (list_of (tines_for_suffix F n))))"
      then obtain k' where " k' < size (list_of (tines_for_suffix F n)) " "x = list_of (tines_for_suffix F n) ! k'"
        using \<open>\<forall>x\<in>set (drop (Suc k) (list_of (tines_for_suffix F n))). \<exists>k'<length (list_of (tines_for_suffix F n)). x = list_of (tines_for_suffix F n) ! k'\<close> by blast
      have "k \<noteq> k'"
      proof (rule ccontr)
        assume "\<not> k \<noteq> k'"
        hence "k = k'" by blast
        hence "x = list_of (tines_for_suffix F n) ! k"
          by (simp add: \<open>x = list_of (tines_for_suffix F n) ! k'\<close>)
        hence " x \<in> set ((take (Suc k) (list_of (tines_for_suffix F n))))"
          using \<open>k = k'\<close> \<open>k' < length (list_of (tines_for_suffix F n))\<close> in_set_conv_nth min_less_iff_conj by fastforce
        hence "\<not> distinct (list_of (tines_for_suffix F n))"
          by (metis Cons_nth_drop_Suc \<open>x = list_of (tines_for_suffix F n) ! k\<close> \<open>x \<in> set (drop (Suc k) (list_of (tines_for_suffix F n)))\<close> distinct.simps(2) distinct_drop k(1))
         hence "card (set (list_of (tines_for_suffix F n))) < size (list_of (tines_for_suffix F n))"
           using card_distinct card_length nat_less_le by blast
         have "card (set (list_of (tines_for_suffix F n))) = size (list_of (tines_for_suffix F n))"
           using list_of_def
           by (metis finite_list_of finite_Collect_conjI finite_list_of_size_card tines_def tines_finite tines_for_suffix_def)
         thus "False"
           using list_of_def
           using \<open>\<not> distinct (list_of (tines_for_suffix F n))\<close> card_distinct by blast
       qed
       thus "(\<exists> k'.  k' \<noteq> k  \<and> k' < size (list_of (tines_for_suffix F n)) \<and> x = list_of (tines_for_suffix F n) ! k')"
         using \<open>k' < length (list_of (tines_for_suffix F n))\<close> \<open>x = list_of (tines_for_suffix F n) ! k'\<close> by auto
     qed
    hence "\<forall>x \<in> set (take k (list_of (tines_for_suffix F n))) \<union> set ((drop (Suc k) (list_of (tines_for_suffix F n)))). 
(\<exists> k'. k' \<noteq> k  \<and> k' < size (list_of (tines_for_suffix F n))\<and> x = list_of (tines_for_suffix F n) ! k')"
      using 0
      by blast
    hence "\<forall>x \<in> set ((take k (list_of (tines_for_suffix F n)))
@ ((drop (Suc k) (list_of (tines_for_suffix F n))))) 
. 
(\<exists> k'. k' \<noteq> k  \<and> k' < size (list_of (tines_for_suffix F n))\<and> x = list_of (tines_for_suffix F n) ! k')"
      by simp
    have "\<forall>x \<in> set ((take k (list_of (tines_for_suffix F n)))
@ ((drop (Suc k) (list_of (tines_for_suffix F n))))) 
. 
(\<exists> k'. k' \<noteq> k  \<and> k' < size (list_of (tines_for_suffix F n))\<and> x = list_of (tines_for_suffix F n) ! k' 
\<and> count (mset_of_tree (get_subtree F ((list_of (tines_for_suffix F n)) !k'))) h = 0)"
      using \<open>\<forall>x\<in>set (take k (list_of (tines_for_suffix F n)) @ drop (Suc k) (list_of (tines_for_suffix F n))). \<exists>k'. k' \<noteq> k \<and> k' < length (list_of (tines_for_suffix F n)) \<and> x = list_of (tines_for_suffix F n) ! k'\<close> notk by blast
    hence "\<forall>x \<in> set (take k (list_of (tines_for_suffix F n))) \<union> set ((drop (Suc k) (list_of (tines_for_suffix F n)))). 
count (mset_of_tree (get_subtree F x)) h = 0"
      by force
    hence "\<forall>x \<in># mset 
(take k (list_of (tines_for_suffix F n)))
+ mset ((drop (Suc k) (list_of (tines_for_suffix F n)))). 
count (mset_of_tree (get_subtree F x)) h = 0"
      by force
hence"\<forall> x \<in># mset 
((take k (list_of (tines_for_suffix F n))) @  ((drop (Suc k) (list_of (tines_for_suffix F n))))). 
count (mset_of_tree (get_subtree F x)) h = 0"
  by (simp add: drop_Suc tl_drop)
hence"\<forall> x \<in># mset (map (get_subtree F)
((take k (list_of (tines_for_suffix F n))) @  ((drop (Suc k) (list_of (tines_for_suffix F n)))))). 
count (mset_of_tree x) h = 0"
proof -
  have "\<And>x. x \<in># mset (map (get_subtree F)
((take k (list_of (tines_for_suffix F n))) @  ((drop (Suc k) (list_of (tines_for_suffix F n)))))) \<Longrightarrow>
count (mset_of_tree x) h = 0"
  proof - 
    fix x assume "x \<in>#  mset (map (get_subtree F)
((take k (list_of (tines_for_suffix F n))) @  ((drop (Suc k) (list_of (tines_for_suffix F n))))))"
    then obtain y where "y \<in># mset ((take k (list_of (tines_for_suffix F n))) @  ((drop (Suc k) (list_of (tines_for_suffix F n)))))"
"get_subtree F y = x"
      by auto
    hence "count (mset_of_tree (get_subtree F y)) h = 0"
      using \<open>\<forall>x\<in>#mset (take k (list_of (tines_for_suffix F n)) @ (drop (Suc k) (list_of (tines_for_suffix F n)))). count (mset_of_tree (get_subtree F x)) h = 0\<close> by blast
    thus "count (mset_of_tree x) h = 0"
      by (simp add: \<open>get_subtree F y = x\<close>)
  qed
  thus ?thesis
    by blast
qed
hence"(\<Sum> x \<in># mset (map (get_subtree F)
((take k (list_of (tines_for_suffix F n))) @ ((drop (Suc k) (list_of (tines_for_suffix F n)))))). 
count (mset_of_tree x) h) = 0"
  by (simp add: imageE)
  thus ?thesis
    by (simp add: count_mset_of_tree_unfold)
qed
    then show ?thesis
      using \<open>count (\<Union>#image_mset mset_of_tree (mset (map (get_subtree F) (list_of (tines_for_suffix F n))))) h = count (\<Union>#image_mset mset_of_tree (mset (map (get_subtree F) (take k (list_of (tines_for_suffix F n)) @ drop k (list_of (tines_for_suffix F n)))))) h\<close> \<open>count (\<Union>#image_mset mset_of_tree (mset (map (get_subtree F) (take k (list_of (tines_for_suffix F n)) @ drop k (list_of (tines_for_suffix F n)))))) h = count (\<Union>#image_mset mset_of_tree (mset (map (get_subtree F) (take k (list_of (tines_for_suffix F n)) @ hd (drop k (list_of (tines_for_suffix F n))) # tl (drop k (list_of (tines_for_suffix F n))))))) h\<close> \<open>count (\<Union>#image_mset mset_of_tree (mset (map (get_subtree F) (take k (list_of (tines_for_suffix F n)) @ hd (drop k (list_of (tines_for_suffix F n))) # tl (drop k (list_of (tines_for_suffix F n))))))) h = count (\<Union>#image_mset mset_of_tree (mset (map (get_subtree F) (take k (list_of (tines_for_suffix F n)) @ list_of (tines_for_suffix F n) ! k # tl (drop k (list_of (tines_for_suffix F n))))))) h\<close> \<open>count (\<Union>#image_mset mset_of_tree (mset (map (get_subtree F) (take k (list_of (tines_for_suffix F n)) @ list_of (tines_for_suffix F n) ! k # tl (drop k (list_of (tines_for_suffix F n))))))) h = count (\<Union>#image_mset mset_of_tree (mset (map (get_subtree F) (take k (list_of (tines_for_suffix F n)) @ tl (drop k (list_of (tines_for_suffix F n))))) + {#get_subtree F (list_of (tines_for_suffix F n) ! k)#})) h\<close> \<open>count (\<Union>#image_mset mset_of_tree (mset (map (get_subtree F) (take k (list_of (tines_for_suffix F n)) @ tl (drop k (list_of (tines_for_suffix F n))))) + {#get_subtree F (list_of (tines_for_suffix F n) ! k)#})) h = count (\<Union>#image_mset mset_of_tree (mset (map (get_subtree F) (take k (list_of (tines_for_suffix F n)) @ tl (drop k (list_of (tines_for_suffix F n))))))) h + count (mset_of_tree (get_subtree F (list_of (tines_for_suffix F n) ! k))) h\<close> \<open>count (\<Union>#image_mset mset_of_tree (mset (map (get_subtree F) (take k (list_of (tines_for_suffix F n)) @ tl (drop k (list_of (tines_for_suffix F n))))))) h + 1 = count (\<Union>#image_mset mset_of_tree (mset (map (get_subtree F) (take k (list_of (tines_for_suffix F n)) @ drop (Suc k) (list_of (tines_for_suffix F n)))))) h + 1\<close> \<open>count (\<Union>#image_mset mset_of_tree (mset (map (get_subtree F) (take k (list_of (tines_for_suffix F n)) @ tl (drop k (list_of (tines_for_suffix F n))))))) h + count (mset_of_tree (get_subtree F (list_of (tines_for_suffix F n) ! k))) h = count (\<Union>#image_mset mset_of_tree (mset (map (get_subtree F) (take k (list_of (tines_for_suffix F n)) @ tl (drop k (list_of (tines_for_suffix F n))))))) h + count (mset_of_tree (get_subtree F thf)) h\<close> \<open>count (mset_of_tree (get_subtree F thf)) h \<le> 1\<close> by linarith 
  qed
qed

lemma new_depth_suffix:
  assumes "count (mset_of_tree F) h = 1" "depth F h > n" "ll \<noteq> h" 
  shows "depth (Suffix_pinched F (Suc n) ll) h = depth F h - n \<and> count (mset_of_tree (Suffix_pinched F (Suc n) ll)) h = 1"
  using assms
proof - 
  obtain l Fs where "F = Tree l Fs"
    using rtree.exhaust by auto
  obtain th where th:"tinesP F th" "last (trace_tine F th) = h"
    using assms
    using exist_unique_label_exist_unique_tine tine_prefix_imp_both_tines by blast
  hence "size th = depth F h"
    using assms(1) reaching_tine_length_eq_label_depth by blast
  have "size th > n"
    by (simp add: \<open>length th = depth F h\<close> assms(2))
  obtain thf ths where thap:"th = thf @ ths" "size thf = Suc n"
    by (metis Suc_leI \<open>n < length th\<close> append_take_drop_id length_take min_absorb2)
   hence "tinesP F thf"
      using th tines_def
      by (metis append_eq_conv_conj take_tine_is_tine) 
   hence "ths \<in> tines (get_subtree F thf)"
      using thap \<open>tinesP F th\<close> mod_get_subtree_tines_append_in_tines tines_def by auto
   hence "last (trace_tine (get_subtree F thf) ths) = h"
      using th thap
      by (metis append_eq_conv_conj get_subtree_append last_in_tine_is_label_in_subtree)
    hence "count (mset_of_tree (get_subtree F thf)) h = 1"
      using assms
      by (metis \<open>ths \<in> tines (get_subtree F thf)\<close> count_greater_eq_one_iff count_subtree_le_count exist_label_eq_exist_tine get_subtree_is_subtree le_antisym)
    hence "depth (get_subtree F thf) h = size th - (Suc n)"
      by (simp add: \<open>length th = depth F h\<close> \<open>tinesP F thf\<close> assms(1) get_subtree_new_depth thap(2))
    have "thf \<in> tines_for_suffix F  (Suc n)" 
      using \<open>tinesP F thf\<close> \<open>size thf = Suc n\<close>
      by (simp add: tines_for_suffix_def)
    hence "thf \<in> set (list_of (tines_for_suffix F (Suc n)))"
      by (metis finite_list_of finite_Collect_conjI list_of_def tines_def tines_finite tines_for_suffix_def)
    then obtain f where "f \<in> set (map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))" "f = get_subtree F thf"
      by simp
    then obtain k where "k < size (map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))"
"(map (get_subtree F) (list_of (tines_for_suffix F (Suc n)))) ! k  = f"
      by (meson exist_num_list)
    hence "ths \<in> tines ((map (get_subtree F) (list_of (tines_for_suffix F (Suc n)))) ! k) "
      by (simp add: \<open>f = get_subtree F thf\<close> \<open>ths \<in> tines (get_subtree F thf)\<close>)
    hence "k#ths \<in> tines (Suffix_pinched F (Suc n) ll)"
      using Sucs Suffix_pinched_def \<open>k < length (map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))\<close> tines_def by auto
    hence "last (trace_tine (Suffix_pinched F (Suc n) ll) (k#ths)) = h"
      using Suffix_pinched_def \<open>f = get_subtree F thf\<close> \<open>k < length (map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))\<close> \<open>last (trace_tine (get_subtree F thf) ths) = h\<close> \<open>map (get_subtree F) (list_of (tines_for_suffix F (Suc n))) ! k = f\<close> trace_tine_non_empty by auto
    hence "count (mset_of_tree (Suffix_pinched F (Suc n) ll)) h \<ge> 1"
      by (metis Suffix_pinched_def \<open>count (mset_of_tree (get_subtree F thf)) h = 1\<close> \<open>f = get_subtree F thf\<close> \<open>f \<in> set (map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))\<close> count_brance_le_count_tree)
    hence "count (mset_of_tree (Tree ll (map (get_subtree F) (list_of (tines_for_suffix F (Suc n)))))) h \<le> 1"
    proof  -                             
      have "count (mset_of_tree (Tree ll (map (get_subtree F) (list_of (tines_for_suffix F (Suc n)))))) h 
             = count ({#ll#} + (\<Sum>x \<in># mset (map (get_subtree F) (list_of (tines_for_suffix F (Suc n)))). mset_of_tree x)) h"
        by simp
      hence "count (mset_of_tree (Tree ll (map (get_subtree F) (list_of (tines_for_suffix F (Suc n)))))) h 
             = count (\<Sum>x \<in># mset (map (get_subtree F) (list_of (tines_for_suffix F (Suc n)))). mset_of_tree x) h"
        by (simp add: assms(3))
      hence "... \<le> 1"
        using assms(1) count_honest_suffix_pinched_le_1 by blast
      thus ?thesis
        using \<open>count (mset_of_tree (Tree ll (map (get_subtree F) (list_of (tines_for_suffix F (Suc n)))))) h = count (\<Union>#image_mset mset_of_tree (mset (map (get_subtree F) (list_of (tines_for_suffix F (Suc n)))))) h\<close> by auto
    qed
    hence "count (mset_of_tree (Suffix_pinched F (Suc n) ll)) h = 1"
      using Suffix_pinched_def \<open>1 \<le> count (mset_of_tree (Suffix_pinched F (Suc n) ll)) h\<close> by auto
    hence "depth (Suffix_pinched F (Suc n) ll) h = size (k#ths)"
      using \<open>k # ths \<in> tines (Suffix_pinched F (Suc n) ll)\<close> \<open>last (trace_tine (Suffix_pinched F (Suc n) ll) (k # ths)) = h\<close> reaching_tine_length_eq_label_depth tines_def by fastforce
    thus ?thesis
      by (metis Suc_diff_Suc \<open>count (mset_of_tree (Suffix_pinched F (Suc n) ll)) h = 1\<close> \<open>count (mset_of_tree (get_subtree F thf)) h = 1\<close> \<open>depth (get_subtree F thf) h = length th - Suc n\<close> \<open>last (trace_tine (get_subtree F thf) ths) = h\<close> \<open>length th = depth F h\<close> assms(2) length_Cons mod_get_subtree_tines_append_in_tines reaching_tine_length_eq_label_depth th(1) thap(1))
  qed

lemma honest_depth_suffix:
  assumes "F \<turnstile> w"  "i \<in> honest_indices w" "j \<in> honest_indices w"  "i < j" "ll \<notin> honest_indices w"
 "i \<in># mset_of_tree (Suffix_pinched F (Suc n) ll)" "j \<in># mset_of_tree ((Suffix_pinched F (Suc n) ll))"
  shows "depth (Suffix_pinched F (Suc n) ll) i < depth (Suffix_pinched F (Suc n) ll) j"
proof - 
  have 0:"ll \<noteq> i \<or> ll \<noteq> j"
    using assms(2) assms(5) by auto
  have "depth F i > n \<and> depth F j > n"
  proof (rule ccontr)
    assume "\<not> (depth F i > n \<and> depth F j > n)"
    hence "depth F i \<le> n \<or> depth F j \<le> n"
      using not_less by blast
      thus "False"
      proof (cases "depth F i \<le> n")
        case True
        obtain ti where "ti \<in> tines F" "last (trace_tine F ti) = i" "size ti \<le> n"
          using True assms(1) assms(2) exist_honest_tine by force
        obtain ti' where ti':"ti' \<in> tines  (Suffix_pinched F (Suc n) ll)"
          "last (trace_tine (Suffix_pinched F (Suc n) ll) ti') = i"
          using assms exist_label_eq_exist_tine
          by blast
        have "ti' \<noteq> []"
        proof (rule ccontr)
          assume "\<not> ti' \<noteq> []"
          hence "ti' = []"
            by blast
          hence "(trace_tine (Suffix_pinched F (Suc n) ll) ti') = [ll]"
            using Suffix_pinched_def trace_tine.simps
            by simp
          thus "False"
            using \<open>last (trace_tine (Suffix_pinched F (Suc n) ll) ti') = i\<close> assms(2) assms(5) by auto
        qed
        then obtain hti' tti' where ti's:"ti' = hti' # tti'"
          using list.exhaust by blast
        hence "hti' <size (map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))"
          using ti'
          by (simp add: Suffix_pinched_def tinesP_Cons_iff tines_def)
        hence "trace_tine (Suffix_pinched F (Suc n) ll) ti' = ll # (trace_tine ((map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))!hti') tti')"
          using ti' ti's Suffix_pinched_def trace_tine.simps
          by simp
        hence "last (trace_tine (Suffix_pinched F (Suc n) ll) ti') = last (trace_tine ((map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))!hti') tti')"
          by (simp add: trace_tine_non_empty)
        hence "last (trace_tine ((map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))!hti') tti') = i"
          using ti'(2) by auto
        hence "i \<in># (mset_of_tree ((map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))!hti'))"
          using Suffix_pinched_def exist_label_eq_exist_tine ti'(1) ti's tinesP_Cons_iff tines_def by auto
        hence "count (mset_of_tree ((map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))!hti')) i \<ge> 1"
          by auto
        hence tti'tine:"tti' \<in> tines ((map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))!hti')"
          using Suffix_pinched_def ti'(1) ti's tinesP_Cons_iff tines_def by auto

        obtain tinei where tinei:"tinei \<in> tines_for_suffix F (Suc n)" 
"get_subtree F tinei = ((map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))!hti')"
proof -
assume a1: "\<And>tinei. \<lbrakk>tinei \<in> tines_for_suffix F (Suc n); get_subtree F tinei = map (get_subtree F) (list_of (tines_for_suffix F (Suc n))) ! hti'\<rbrakk> \<Longrightarrow> thesis"
  have "\<forall>n N. ((inv_into (Collect distinct) set N ! n::nat list) \<in> N \<or> \<not> n < length (inv_into (Collect distinct) set N)) \<or> infinite N"
    by (metis finite_list_of nth_mem) 
  then show ?thesis
    using a1 by (metis (no_types) \<open>hti' < length (map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))\<close> finite_Collect_conjI length_map list_of_def nth_map tines_def tines_finite tines_for_suffix_def)
qed
  hence "tinei \<in> tines F \<and> size tinei = Suc n"
    by (simp add: tines_def tines_for_suffix_def)
  hence "tinei @ tti' \<in> tines F"
    using tti'tine tinei tine_append_tine_of_subtree_is_tine tines_def
    by simp
  hence "last (trace_tine F (tinei @ tti')) = i"
    by (metis \<open>last (trace_tine (Suffix_pinched F (Suc n) ll) ti') = last (trace_tine (map (get_subtree F) (list_of (tines_for_suffix F (Suc n))) ! hti') tti')\<close> append_eq_conv_conj get_subtree_append last_in_tine_is_label_in_subtree mem_Collect_eq ti'(2) tinei(2) tines_def)
  hence "tinei@tti' = ti"
    using \<open>last (trace_tine F ti) = i\<close> \<open>ti \<in> tines F\<close> \<open>tinei @ tti' \<in> tines F\<close> assms(1) assms(2) exist_unique_label_exist_unique_tine fork_F forks_def by fastforce
  thus "False"
    using \<open>length ti \<le> n\<close> \<open>tinei \<in> tines F \<and> length tinei = Suc n\<close> by auto
      next
        case False
        hence "depth F j \<le> n"
          using \<open>depth F i \<le> n \<or> depth F j \<le> n\<close> by blast
 obtain tj where "tj \<in> tines F" "last (trace_tine F tj) = i" "size tj \<le> n"
   using assms(1) assms(2) exist_honest_tine
 proof -
   assume a1: "\<And>tj. \<lbrakk>tj \<in> tines F; last (trace_tine F tj) = i; length tj \<le> n\<rbrakk> \<Longrightarrow> thesis"
   have f2: "fork F (honest_indices w) (length w)"
     by (metis assms(1) forks_def)
   have "\<forall>r N n. fork r N n = (label r = 0 \<and> strictly_inc r \<and> limit r n \<and> (\<forall>n. n \<notin> N \<or> count (mset_of_tree r) n = Suc 0) \<and> (\<forall>n. n \<notin> N \<or> (\<forall>na. (na \<notin> N \<or> \<not> n < na) \<or> depth r n < depth r na)))"
     using fork_F by auto
   then have "depth F i < depth F j"
     using f2 assms(2) assms(3) assms(4) by presburger
   then show ?thesis
     using a1 by (metis (no_types) \<open>depth F i \<le> n \<or> depth F j \<le> n\<close> assms(1) assms(2) exist_honest_tine leD le_trans nat_le_linear)
 qed 
        obtain tj' where tj':"tj' \<in> tines  (Suffix_pinched F (Suc n) ll)"
          "last (trace_tine (Suffix_pinched F (Suc n) ll) tj') = j"
          using assms exist_label_eq_exist_tine
          by blast
        have "tj' \<noteq> []"
        proof (rule ccontr)
          assume "\<not> tj' \<noteq> []"
          hence "tj' = []"
            by blast
          hence "(trace_tine (Suffix_pinched F (Suc n) ll) tj') = [ll]"
            using Suffix_pinched_def trace_tine.simps
            by simp
          thus "False"
            using \<open>last (trace_tine (Suffix_pinched F (Suc n) ll) tj') = j\<close> assms(2) assms(5)
            by (simp add: assms(3)) 
        qed
        then obtain htj' ttj' where tj's:"tj' = htj' # ttj'"
          using list.exhaust by blast
        hence "htj' <size (map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))"
          using tj'
          by (simp add: Suffix_pinched_def tinesP_Cons_iff tines_def)
        hence "trace_tine (Suffix_pinched F (Suc n) ll) tj' = ll # (trace_tine ((map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))!htj') ttj')"
          using tj' tj's Suffix_pinched_def trace_tine.simps
          by simp
        hence "last (trace_tine (Suffix_pinched F (Suc n) ll) tj') = last (trace_tine ((map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))!htj') ttj')"
          by (simp add: trace_tine_non_empty)
        hence "last (trace_tine ((map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))!htj') ttj') = j"
          using tj'(2) by auto
        hence "j \<in># (mset_of_tree ((map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))!htj'))"
          using Suffix_pinched_def exist_label_eq_exist_tine tj'(1) tj's tinesP_Cons_iff tines_def by auto
        hence "count (mset_of_tree ((map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))!htj')) j \<ge> 1"
          by auto
        hence tti'tine:"ttj' \<in> tines ((map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))!htj')"
          using Suffix_pinched_def tj'(1) tj's tinesP_Cons_iff tines_def by auto

        obtain tinej where tinei:"tinej \<in> tines_for_suffix F (Suc n)" 
"get_subtree F tinej = ((map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))!htj')"
          by (metis finite_list_of \<open>htj' < length (map (get_subtree F) (list_of (tines_for_suffix F (Suc n))))\<close> finite_Collect_conjI length_map list_of_def nth_map nth_mem tines_def tines_finite tines_for_suffix_def)
  hence "tinej \<in> tines F \<and> size tinej = Suc n"
    by (simp add: tines_def tines_for_suffix_def)
  hence "tinej @ ttj' \<in> tines F"
    using tti'tine tinei tine_append_tine_of_subtree_is_tine tines_def
    by simp
  hence "last (trace_tine F (tinej @ ttj')) = j"
    by (metis \<open>last (trace_tine (Suffix_pinched F (Suc n) ll) tj') = last (trace_tine (map (get_subtree F) (list_of (tines_for_suffix F (Suc n))) ! htj') ttj')\<close> 
append_eq_conv_conj get_subtree_append last_in_tine_is_label_in_subtree mem_Collect_eq tj'(2) tinei(2) tines_def)
  hence "tinej@ttj' = tj"
    using \<open>last (trace_tine F tj) = i\<close> \<open>tj \<in> tines F\<close> \<open>tinej @ ttj' \<in> tines F\<close> assms(1) assms(2) exist_unique_label_exist_unique_tine fork_F forks_def
    by (metis False One_nat_def \<open>\<And>thesis. (\<And>tj. \<lbrakk>tj \<in> tines F; last (trace_tine F tj) = i; length tj \<le> n\<rbrakk> \<Longrightarrow> thesis) \<Longrightarrow> thesis\<close> leD mem_Collect_eq not_less reaching_tine_length_eq_label_depth tines_def) 
    
  thus "False"
    using \<open>length tj \<le> n\<close> \<open>tinej \<in> tines F \<and> length tinej = Suc n\<close> by auto
      qed
    qed
  have 1:"count (mset_of_tree F) i = 1" "count (mset_of_tree F) j = 1"
    using assms
     apply (simp add: fork_F forks_def)
    using assms(1) assms(3) fork_F forks_def by auto
  hence "count (mset_of_tree (Suffix_pinched F (Suc n) ll)) i = 1"
    using assms 0 new_depth_suffix
    by (metis \<open>n < depth F i \<and> n < depth F j\<close>)
    have "count (mset_of_tree (Suffix_pinched F (Suc n) ll)) j = 1"
      using assms 0 new_depth_suffix 1
      by (metis \<open>n < depth F i \<and> n < depth F j\<close>)
    hence "depth F i < depth F j"
      using assms(1) assms(2) assms(3) assms(4) fork_F forks_def by blast
    thus "depth (Suffix_pinched F (Suc n) ll) i < depth (Suffix_pinched F (Suc n) ll) j"
      by (metis "local.1"(1) "local.1"(2) \<open>n < depth F i \<and> n < depth F j\<close> assms(2) assms(3) assms(5) leD le_diff_iff le_less_linear less_imp_le new_depth_suffix)
  qed

inductive prefix_list:: "'a list \<Rightarrow> 'a list \<Rightarrow> bool" where
  Null:"prefix_list [] l"
| Cons: "\<lbrakk>k = k';  prefix_list t t'\<rbrakk> \<Longrightarrow> prefix_list (k#t) (k'#t')"

lemma prefix_list_nth:
  assumes "prefix_list A B" "i < size A"
  shows "A ! i = B ! i"
  using assms
proof (induction arbitrary: i)
  case (Null l)
  then show ?case
    by simp 
next
  case (Cons k k' t t')
  hence "\<forall> i. i < size t\<longrightarrow> t ! i = t' ! i"
    by simp
  hence "\<forall>i. i < size t \<longrightarrow> (k#t) ! (Suc i) = (k'#t') ! Suc i "
    by simp
  hence "\<forall>i. i \<le> size t \<longrightarrow> (k#t) ! i = (k'#t') ! i"
    using Cons
    by (simp add: nth_Cons')
  then show ?case
    using Cons.prems by auto
qed

lemma prefix_list_le_size:
  assumes "prefix_list A B"
  shows "size A \<le> size B"
  using assms
proof (induction)
case (Null l)
  then show ?case by simp
next
  case (Cons k k' t t')
  then show ?case
    by simp 
qed

lemma prefix_list_take:
  assumes "prefix_list A B" 
  shows "take (size A) B = A"
  using assms
proof (induction)
case (Null l)
  then show ?case
    by simp 
next
  case (Cons k k' t t')
  then show ?case
    by auto 
qed

lemma take_prefix_list:
  assumes "take (size A) B = A" "size A \<le> size B"
  shows "prefix_list A B" 
  using assms
proof (induction B arbitrary: A)
  case Nil
  then show ?case
    by (simp add: Null) 
next
  case (Cons b B')
  hence "prefix_list A (b#B') \<and> size A \<le> size (b#B') \<and> take (size A) (b#B') = A"
    by (smt Suc_le_mono diff_Suc_1 length_0_conv length_Cons prefix_list.simps take_Cons')
  then show ?case
    by (smt Nitpick.size_list_simp(2) Suc_le_mono length_Cons list.inject prefix_list.simps take_Suc_Cons) 
qed

lemma prefix_list_longest_common_prefix_tines_1:
 "prefix_list (longest_common_prefix_tines t1 t2) t1"
  using take_prefix_list
  by (metis append_eq_conv_conj append_take_drop_id longest_common_prefix_size_le longest_common_prefix_tines_take_exist) 

lemma prefix_list_longest_common_prefix_tines_2:
 "prefix_list (longest_common_prefix_tines t1 t2) t2"
  using take_prefix_list
  by (metis longest_common_prefix_tines_commute prefix_list_longest_common_prefix_tines_1)

lemma prefix_list_longest_common_prefix_tines:
  assumes  "prefix_list t3 t1" "prefix_list t3 t2"
  shows "prefix_list t3 (longest_common_prefix_tines t1 t2)"
  using assms
proof (induction arbitrary: t2)
  case (Null l)
  then show ?case
    by (simp add: prefix_list.Null) 
next
  case (Cons k k' t t')
  then show ?case 
  proof (induction t2)
    case Nil
then show ?case
  by auto 
next
  case (Cons k'' t'')
  have "k'' = k'"
    using Cons.prems(1) Cons.prems(4) prefix_list.cases by auto
  hence "prefix_list t t' \<and> prefix_list t t''"
    using Cons.prems(2) Cons.prems(4) prefix_list.cases by auto
  hence "prefix_list t (longest_common_prefix_tines t' t'')"
    by (simp add: Cons.prems(3))
  then show ?case
    by (simp add: Cons.prems(1) \<open>k'' = k'\<close> prefix_list.Cons)
qed
qed

lemma longest_common_prefix_tines_take_lt_size_ne:
  assumes "i \<le> size t1 \<and> i \<le> size t2 \<and> i > size (longest_common_prefix_tines t1 t2)"
  shows "take i t1 \<noteq> take i t2"
proof (rule ccontr)
  assume asm:"\<not> take i t1 \<noteq> take i t2"
  obtain j where "j \<le> size t1 \<and> j \<le> size t2 \<and> (longest_common_prefix_tines t1 t2) = take j t1 \<and> (longest_common_prefix_tines t1 t2) = take j t2"
    using longest_common_prefix_tines_take_exist by auto
  hence " size (take j t1) = j"
    by (simp add: min_absorb2)
  hence "j = size (longest_common_prefix_tines t1 t2)"
    by (simp add: \<open>j \<le> length t1 \<and> j \<le> length t2 \<and> longest_common_prefix_tines t1 t2 = take j t1 \<and> longest_common_prefix_tines t1 t2 = take j t2\<close>)
  have ij: "i > j"
    by (simp add: \<open>j = length (longest_common_prefix_tines t1 t2)\<close> assms)
  have "prefix_list (take i t1) t1 \<and> prefix_list (take i t2) t2"
    by (simp add: assms min_absorb2 take_prefix_list)
  hence "prefix_list (take i t1) (longest_common_prefix_tines t1 t2)"
    using asm
    using prefix_list_longest_common_prefix_tines by auto
  hence "size (take i t1) \<le> size (longest_common_prefix_tines t1 t2)"
    using prefix_list_le_size by blast
  hence "i \<le> j"
    by (simp add: \<open>j = length (longest_common_prefix_tines t1 t2)\<close> assms min.absorb2)
  thus "False"
    using ij by auto
qed

lemma Extend_tree_trace_tine_2:
  assumes "tinesP F t" "t' \<in> tines F" "size t' \<ge> size t" "prefix_list t t'"
  shows "trace_tine (Extend_tree F t st) (t'[(size t):= Suc (t' ! (size t))]) = trace_tine F t'"
  using assms
proof (induction arbitrary: t')
  case (Sucs k F t)
  then obtain Fs l where "F = Tree l Fs"
    using rtree.exhaust_sel by blast
  have "size t' \<ge> size (k#t) \<and> prefix_list (k#t) t' \<and> t' \<in> tines F"
    using Sucs
    by blast
  then obtain k' t'' where kt:"t' = k'#t''"
    using prefix_list.cases by blast
  hence kk:"k = k'"
    using Sucs.prems(3) prefix_list.cases by auto
  hence "t'' \<in> tines (Fs!k')"
    using Sucs.prems(1) \<open>F = Tree l Fs\<close> tinesP_Cons_iff tines_def kt by auto
  hence "t'' \<in> tines (Fs!k)"
    using kk by simp
  have "(Extend_tree F (k#t) st) = Tree l ((take k Fs) @ (Extend_tree (Fs ! k) t st) # (drop (Suc k) Fs))"
    by (simp add: \<open>F = Tree l Fs\<close>)
  hence "trace_tine (Extend_tree F (k#t) st) (t'[(size (k#t)):= Suc (t' ! (size (k#t)))]) 
        = trace_tine (Tree l ((take k Fs) @ (Extend_tree (Fs ! k) t st) # (drop (Suc k) Fs))) 
(t'[(size (k#t)):= Suc (t' ! (size (k#t)))]) "
    by simp
  hence "...
        = trace_tine (Tree l ((take k Fs) @ (Extend_tree (Fs ! k) t st) # (drop (Suc k) Fs))) 
        ((k#t'')[(size (k#t)):= Suc ((k#t'') ! (size (k#t)))]) "
    using kt kk
    by blast
  hence "...
        = trace_tine (Tree l ((take k Fs) @ (Extend_tree (Fs ! k) t st) # (drop (Suc k) Fs))) 
        (k# ((t'')[(size t):= Suc ((k#t'') ! (size (k#t)))])) "
    by simp
  hence "...
        = l#trace_tine (((take k Fs) @ (Extend_tree (Fs ! k) t st) # (drop (Suc k) Fs)) ! k) 
        ((t'')[(size t):= Suc ((k#t'') ! (size (k#t)))]) "
  using Sucs.hyps(1) \<open>F = Tree l Fs\<close> by auto
  hence "...
        = l#trace_tine (Extend_tree (Fs ! k) t st) ((t'')[(size t):= Suc ((k#t'') ! (size (k#t)))]) "
  proof -
    have "k = length (take k Fs)"
      using Sucs.hyps(1) \<open>F = Tree l Fs\<close> by force
    then show ?thesis
      by (metis (full_types) nth_append_length)
  qed
 hence "...
        = l#trace_tine (Extend_tree (Fs ! k) t st) ((t'')[(size t):= Suc (t'' ! (size t))]) "
   by simp
   hence "...
        = l#trace_tine (Fs ! k) (t'') "
     using Cons kk kt
      by (smt Suc_le_mono Sucs.IH Sucs.prems(2) Sucs.prems(3) \<open>F = Tree l Fs\<close> \<open>t'' \<in> tines (Fs ! k)\<close> length_Cons list.distinct(1) list.sel(3) prefix_list.cases rtree.sel(2))
   hence "... = trace_tine F t'"
     using Sucs.hyps(1) \<open>F = Tree l Fs\<close> kk kt by auto
   then show ?case
     using \<open>Extend_tree F (k # t) st = Tree l (take k Fs @ Extend_tree (Fs ! k) t st # drop (Suc k) Fs)\<close> \<open>l # trace_tine ((take k Fs @ Extend_tree (Fs ! k) t st # drop (Suc k) Fs) ! k) (t''[length t := Suc ((k # t'') ! length (k # t))]) = l # trace_tine (Extend_tree (Fs ! k) t st) (t''[length t := Suc ((k # t'') ! length (k # t))])\<close> \<open>l # trace_tine (Extend_tree (Fs ! k) t st) (t''[length t := Suc (t'' ! length t)]) = l # trace_tine (Fs ! k) t''\<close> \<open>trace_tine (Tree l (take k Fs @ Extend_tree (Fs ! k) t st # drop (Suc k) Fs)) (k # t''[length t := Suc ((k # t'') ! length (k # t))]) = l # trace_tine ((take k Fs @ Extend_tree (Fs ! k) t st # drop (Suc k) Fs) ! k) (t''[length t := Suc ((k # t'') ! length (k # t))])\<close> kk kt by auto
next
  case (Nil F)
  then obtain Fs l where "F = Tree l Fs"
    by (metis rtree.collapse)
  hence "Extend_tree F [] st = Tree l (st#Fs)"
    by simp
  then show ?case
  proof (cases "t' = []")
case True
  then show ?thesis
    by (simp add: \<open>F = Tree l Fs\<close>) 
next
  case False
  have "t' \<noteq> []"
    by (simp add: False)
  then obtain k' t'' where "t' = k'#t''"
    by (meson neq_Nil_conv)
  hence "trace_tine (Extend_tree F [] st) (t'[0:= Suc (t' ! 0)]) 
        = trace_tine (Extend_tree F [] st) (t'[0:= Suc k'])"
    by simp
hence tmp:"trace_tine (Extend_tree F [] st) (t'[0:= Suc (t' ! 0)]) 
        = trace_tine (Extend_tree F [] st) (([k']@t'')[0:= Suc k'])"
  using \<open>t' = k' # t''\<close>
  by simp 
  hence "trace_tine (Extend_tree F [] st) (t'[0:= Suc (t' ! 0)]) 
        = trace_tine (Extend_tree F [] st) (([k'][0 := Suc k'])@t'')"
    using  list_update_append1 list_update_append
    by simp 
  then show ?thesis
    by (simp add: \<open>F = Tree l Fs\<close> \<open>t' = k' # t''\<close>) 
qed
qed

lemma Extend_tree_trace_tine_3:
  assumes "tinesP F t" "t' \<in> tines F" "size t' > size t" "\<not> prefix_list t t'"
  shows "trace_tine (Extend_tree F t st) t' = trace_tine F t'"
  using assms
proof (induction arbitrary: t')
  case (Sucs k F t)
  then obtain l Fs where "F = Tree l Fs"
    using rtree.exhaust_sel by blast
  hence distri:"Extend_tree F (k#t) st
  = Tree l ((take k Fs) @ (Extend_tree (Fs ! k) t st) # (drop (Suc k) Fs))"
    by simp
  have "\<not> prefix_list (k#t) t'"
    using Sucs by simp
  then show ?case 
  proof (cases "t' = []")
case True
  then show ?thesis
    using Sucs.prems(2) by auto
next
  case False
  then obtain k' t'' where "t' = k'#t''"
    using list.exhaust_sel by auto
  then show ?thesis 
  proof (cases "k' = k")
    case True
    hence "\<not> prefix_list t t''" 
      using False
      using Sucs.prems(3) \<open>t' = k' # t''\<close> prefix_list.Cons by fastforce
    have "t'' \<in> tines (Fs!k)"
      using assms True False
      using Sucs.prems(1) \<open>F = Tree l Fs\<close> \<open>t' = k' # t''\<close> tinesP_Cons_iff tines_def by auto
    hence "trace_tine (Extend_tree (Fs!k) t st) t'' = trace_tine (Fs!k) t''"
      using Sucs.IH Sucs.prems(2) \<open>F = Tree l Fs\<close> \<open>\<not> prefix_list t t''\<close> \<open>t' = k' # t''\<close> by auto
    hence "trace_tine (Extend_tree F (k#t) st) (k#t'') 
= l#(trace_tine (Fs!k) t'')" using distri
      by (smt Suc_diff_Suc Sucs.hyps(1) \<open>F = Tree l Fs\<close> append_take_drop_id diff_add_inverse diff_add_inverse2 length_Cons length_append length_drop less_imp_add_positive list.simps(5) nth_append_length rtree.sel(2) trace_tine.simps)
    hence "trace_tine (Extend_tree F (k#t) st) (k#t'') 
= trace_tine F (k#t'')"
      using Sucs.hyps(1) \<open>F = Tree l Fs\<close> by auto
    then show ?thesis
      by (simp add: True \<open>t' = k' # t''\<close>)
  next
    case False
    note nekk = this
    thm nekk
    hence "trace_tine (Extend_tree F (k#t) st) (k'#t'') 
= l#(trace_tine (((take k Fs) @ (Extend_tree (Fs ! k) t st) # (drop (Suc k) Fs))!k') t'')"
      using distri
      by (smt Suc_diff_Suc Sucs.hyps(1) Sucs.prems(1) \<open>F = Tree l Fs\<close> \<open>t' = k' # t''\<close> append_take_drop_id length_Cons length_append length_drop list.simps(5) rtree.sel(2) tine_prefix_Subtree tine_prefix_refl trace_tine.simps)
    hence "... = l# trace_tine (Fs! k') t''" using nekk
      by (metis Sucs.hyps(1) Sucs.prems(1) \<open>F = Tree l Fs\<close> \<open>t' = k' # t''\<close> list.inject nth_list_update_neq_upd_conv_take_nth_drop rtree.sel(2) tine_prefix_Subtree tine_prefix_refl)
    hence "... = trace_tine F (k'#t'')"
      using Sucs.prems(1) \<open>F = Tree l Fs\<close> \<open>t' = k' # t''\<close> tinesP_Cons_iff tines_def by auto
    then show ?thesis
      by (simp add: \<open>l # trace_tine ((take k Fs @ Extend_tree (Fs ! k) t st # drop (Suc k) Fs) ! k') t'' = l # trace_tine (Fs ! k') t''\<close> \<open>t' = k' # t''\<close> \<open>trace_tine (Extend_tree F (k # t) st) (k' # t'') = l # trace_tine ((take k Fs @ Extend_tree (Fs ! k) t st # drop (Suc k) Fs) ! k') t''\<close>)
  qed
qed
next
case (Nil F)
then show ?case
  using Null by auto 
qed

lemma Extend_tree_trace_extended_tines:
  assumes "tinesP F t" "t' \<in> tines st" 
  shows "(t@(0#t')) \<in> tines (Extend_tree F t st)"
  using assms
proof (induction arbitrary: t')
  case (Sucs k F t)
  then obtain l Fs where "F = Tree l Fs"
    using rtree.exhaust_sel by blast
  hence "(t@(0#t')) \<in> tines (Extend_tree (Fs!k) t st)"
    using Sucs.IH Sucs.prems by auto
  hence "k#(t@(0#t')) \<in> tines (Tree l ((take k Fs) @ (Extend_tree (Fs ! k) t st) # (drop (Suc k) Fs)))"
proof -
  have f1: "\<forall>n rs. length (take n (rs::rtree list)) = min (length rs) n"
using length_take by blast
  obtain nn :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
    "\<forall>x0 x1. (\<exists>v2>0. x1 + v2 = x0) = (0 < nn x0 x1 \<and> x1 + nn x0 x1 = x0)"
    by moura
  then have f2: "0 < nn (length (sucs F)) k \<and> k + nn (length (sucs F)) k = length (sucs F)"
    by (meson Sucs.hyps(1) less_imp_add_positive)
have f3: "Fs = sucs F"
  using \<open>F = Tree l Fs\<close> rtree.sel(2) by presburger
  then have f4: "length (Extend_tree (Fs ! k) t st # drop (Suc k) Fs) = length (drop k Fs)"
    by (simp add: Suc_diff_Suc Sucs.hyps(1))
  then have "min (length Fs) k = k + nn (length (sucs F)) k - nn (length (sucs F)) k"
    using f3 f2 f1 by (metis (no_types) Suc_diff_Suc append_take_drop_id diff_add_inverse diff_add_inverse2 length_append length_drop)
  then show ?thesis
    using f4 f3 f1 by (metis Sucs.hyps(1) \<open>t @ 0 # t' \<in> tines (Extend_tree (Fs ! k) t st)\<close> append_take_drop_id diff_add_inverse2 length_append mem_Collect_eq nth_append_length rtree.sel(2) tinesP.Sucs tines_def)
qed
  then show ?case
    by (simp add: \<open>F = Tree l Fs\<close>) 
next
  case (Nil F)
  then show ?case
    by (metis Extend_tree.simps(1) append_self_conv2 length_greater_0_conv list.distinct(1) mem_Collect_eq nth_Cons_0 rtree.exhaust_sel rtree.sel(2) tinesP_Cons_iff tines_def) 
qed

lemma Extend_tree_trace_renewed_tines:
  assumes "tinesP F t" "t' \<in> tines F" "size t \<le> size t'" "prefix_list t t'"
  shows "(t'[(size t):= Suc (t' ! (size t))]) \<in> tines (Extend_tree F t st)"
  using assms
proof (induction arbitrary: t')
  case (Sucs k F t)
  then obtain Fs l where lFs :"F= Tree l Fs"
    using rtree.exhaust_sel by blast
  hence "Extend_tree F (k#t) st = Tree l ((take k Fs) @ (Extend_tree (Fs ! k) t st) # (drop (Suc k) Fs))"
    by simp
  obtain k' t'' where "t' = k' # t''"
    using assms
    by (metis Sucs.prems(2) list.exhaust_sel list.size(3) take_all take_eq_Nil)
  hence "k = k'"
    using Sucs.prems(3) prefix_list.cases by auto
  hence "prefix_list t t''"
    using Sucs.prems(3) \<open>t' = k' # t''\<close> prefix_list.cases by auto
  hence "(t''[(size t):= Suc (t'' ! (size t))]) \<in> tines (Extend_tree (Fs!k) t st)"
    by (metis Suc_le_mono Sucs.IH Sucs.prems(1) Sucs.prems(2) \<open>\<And>thesis. (\<And>k' t''. t' = k' # t'' \<Longrightarrow> thesis) \<Longrightarrow> thesis\<close> \<open>k = k'\<close> \<open>t' = k' # t''\<close> lFs length_Cons list.inject list.simps(3) mem_Collect_eq rtree.sel(2) tinesP.simps tines_def)
  hence "k#(t''[(size t):= Suc (t'' ! (size t))]) \<in> tines (Extend_tree F (k#t) st)"
proof -
  have f1: "sucs F = Fs"
    by (metis lFs rtree.sel(2))
  then have f2: "length (Extend_tree (Fs ! k) t st # drop (Suc k) Fs) = length (drop k Fs)"
    by (metis Suc_diff_Suc Sucs.hyps(1) length_Cons length_drop)
  then have f3: "k = length (take k Fs) + Suc (length (drop (Suc k) Fs)) - Suc (length (drop (Suc k) Fs))"
    by simp
have "length (sucs F) = length (take k Fs @ drop k Fs)"
  using f1 by (metis append_take_drop_id)
  then have "tinesP (Tree l (take k Fs @ Extend_tree (Fs ! k) t st # drop (Suc k) Fs)) (k # t''[length t := Suc (t'' ! length t)])"
    using f3 f2 by (metis Sucs.hyps(1) \<open>t''[length t := Suc (t'' ! length t)] \<in> tines (Extend_tree (Fs ! k) t st)\<close> diff_add_inverse2 length_append mem_Collect_eq nth_append_length rtree.sel(2) tinesP.simps tines_def)
  then show ?thesis
    by (simp add: \<open>Extend_tree F (k # t) st = Tree l (take k Fs @ Extend_tree (Fs ! k) t st # drop (Suc k) Fs)\<close> tines_def)
qed
  then show ?case
    by (simp add: \<open>k = k'\<close> \<open>t' = k' # t''\<close>) 
next
  case (Nil F)
  then show ?case
    by (smt Extend_tree.simps(1) Suc_less_eq eq_iff id_take_nth_drop le_neq_implies_less length_0_conv length_Cons list.size(3) list_update_beyond list_update_code(2) mem_Collect_eq nth_Cons_Suc rtree.exhaust_sel rtree.sel(2) self_append_conv2 take_eq_Nil tinesP.simps tinesP_Cons_iff tines_def) 
qed

lemma Extend_tree_trace_remaining_tines:
  assumes "tinesP F t" "t' \<in> tines F" "size t \<le> size t'" "\<not> prefix_list t t'"
  shows "t' \<in> tines (Extend_tree F t st)"
  using assms
proof (induction arbitrary: t')
  case (Sucs k F t)
 then obtain Fs l where lFs :"F= Tree l Fs"
    using rtree.exhaust_sel by blast
  hence "Extend_tree F (k#t) st = Tree l ((take k Fs) @ (Extend_tree (Fs ! k) t st) # (drop (Suc k) Fs))"
    by simp
  then obtain k' t'' where "t' = k'#t''"
    using Sucs assms
  proof -
    assume "\<And>k' t''. t' = k' # t'' \<Longrightarrow> thesis"
    then show ?thesis
      by (metis (no_types) Sucs.prems(1) Sucs.prems(2) add_eq_0_iff_both_eq_0 le_Suc_ex length_0_conv tine_prefix_imp_both_tines tine_prefix_refl tinesP.simps)
  qed
  have "t'' \<in> tines (Fs!k') \<and> k' < size Fs"
    using Sucs.prems(1) \<open>t' = k' # t''\<close> lFs tinesP_Cons_iff tines_def by auto
  then show ?case
  proof (cases "k = k'")
    case True
    hence 0:"\<not> prefix_list t t''"
      using Sucs.prems(3) \<open>t' = k' # t''\<close> prefix_list.Cons by fastforce
    have 1:"t'' \<in> tines (Fs!k)"
      by (simp add: True \<open>t'' \<in> tines (Fs ! k') \<and> k' < length Fs\<close>)
    have "size t \<le> size t''"
      using Sucs \<open>t' = k'#t''\<close>
      by simp
    hence "t'' \<in> tines (Extend_tree (Fs!k) t st)"
      using 0 1
      using Sucs.IH lFs by auto
    hence "k#t'' \<in> tines (Tree l ((take k Fs) @ (Extend_tree (Fs ! k) t st) # (drop (Suc k) Fs)))"
    proof -
      have f1: "tinesP (Extend_tree (Fs ! k) t st) t''"
        using \<open>t'' \<in> tines (Extend_tree (Fs ! k) t st)\<close> tine_prefix_imp_both_tines by blast
      have "k' \<le> min (length Fs) k'"
        using \<open>t'' \<in> tines (Fs ! k') \<and> k' < length Fs\<close> by linarith
      then show ?thesis
using f1 by (simp add: True nth_append tinesP_Cons_iff tines_def)
qed
    then show ?thesis
      using True \<open>Extend_tree F (k # t) st = Tree l (take k Fs @ Extend_tree (Fs ! k) t st # drop (Suc k) Fs)\<close> \<open>t' = k' # t''\<close> by auto
   
  next
    case False
    have "t'' \<in> tines (Fs!k')"
      by (simp add: \<open>t'' \<in> tines (Fs ! k') \<and> k' < length Fs\<close>)
    hence 0:"t'' \<in> tines  (((take k Fs) @ (Extend_tree (Fs ! k) t st) # (drop (Suc k) Fs)) ! k')"
      by (metis False Sucs.hyps(1) lFs nth_list_update_neq rtree.sel(2) upd_conv_take_nth_drop)
    hence "k'#t'' \<in> tines (Tree l ((take k Fs) @ (Extend_tree (Fs ! k) t st) # (drop (Suc k) Fs)))"
      using \<open>t'' \<in> tines (Fs ! k') \<and> k' < length Fs\<close> tinesP_Cons_iff tines_def by auto
    then show ?thesis
      by (simp add: \<open>Extend_tree F (k # t) st = Tree l (take k Fs @ Extend_tree (Fs ! k) t st # drop (Suc k) Fs)\<close> \<open>t' = k' # t''\<close>)
  qed
next
  case (Nil F)
  then show ?case
    by (simp add: Null) 
qed
 

lemma Extend_tree_trace_tine_4:
  assumes "tinesP F t" "t' \<in> tines st" 
  shows "trace_tine (Extend_tree F t st) (t@(0#t')) = (trace_tine F t)@(trace_tine st t')"
  using assms
proof (induction arbitrary: t')
  case (Sucs k F t)
  then obtain l Fs where  "F = Tree l Fs"
    using rtree.exhaust_sel by blast
  hence "trace_tine (Extend_tree (Fs!k) t st) (t@(0#t')) = (trace_tine (Fs!k) t)@(trace_tine st t')"
    using Sucs.IH Sucs.prems by auto
  hence "l#trace_tine (Extend_tree (Fs!k) t st) (t@(0#t')) = l#(trace_tine (Fs!k) t)@(trace_tine st t')"
    by simp
  hence "trace_tine (Tree l ((take k Fs) @ (Extend_tree (Fs ! k) t st) # (drop (Suc k) Fs))) (k#(t@(0#t'))) = (l#(trace_tine (Fs!k) t))@(trace_tine st t')"
proof -
  have f1: "(if k < length (take k Fs @ Extend_tree (Fs ! k) t st # drop (Suc k) Fs) then l # trace_tine ((take k Fs @ Extend_tree (Fs ! k) t st # drop (Suc k) Fs) ! k) (t @ 0 # t') else [l]) = trace_tine (Tree l (take k Fs @ Extend_tree (Fs ! k) t st # drop (Suc k) Fs)) (k # t @ 0 # t')"
    by auto
  have f2: "Fs = sucs F"
    by (metis \<open>F = Tree l Fs\<close> rtree.sel(2))
  then have f3: "length (Extend_tree (Fs ! k) t st # drop (Suc k) Fs) = length (drop k Fs)"
    by (simp add: Suc_diff_Suc Sucs.hyps(1))
  then have "k = length (take k Fs)"
    by simp
  then show ?thesis
    using f3 f2 f1 by (metis (no_types) Sucs.hyps(1) \<open>l # trace_tine (Extend_tree (Fs ! k) t st) (t @ 0 # t') = l # trace_tine (Fs ! k) t @ trace_tine st t'\<close> append_Cons append_take_drop_id length_append nth_append_length)
qed
 hence "trace_tine (Extend_tree F (k#t) st) (k#(t@(0#t'))) = (l#(trace_tine (Fs!k) t))@(trace_tine st t')"
   by (simp add: \<open>F = Tree l Fs\<close>)
hence "trace_tine (Extend_tree F (k#t) st) (k#(t@(0#t'))) = (trace_tine F (k#t))@(trace_tine st t')"
  using Sucs.hyps(1) \<open>F = Tree l Fs\<close> by auto
  then show ?case
    by simp 
next
  case (Nil F)
  then show ?case
proof -
  have "trace_tine (Tree (label F) (st # sucs F)) (0 # t') = trace_tine (Extend_tree F [] st) ([] @ 0 # t')"
by (metis (no_types) Extend_tree.simps(1) append_self_conv2 rtree.exhaust_sel)
  then have f1: "(if 0 < length (st # sucs F) then label F # trace_tine ((st # sucs F) ! 0) t' else [label F]) = trace_tine (Extend_tree F [] st) ([] @ 0 # t')"
    by fastforce
  have "trace_tine F [] = (case [] of [] \<Rightarrow> [label F] | n # ns \<Rightarrow> if n < length (sucs F) then label F # trace_tine (sucs F ! n) ns else [label F])"
    by (metis (no_types) rtree.exhaust_sel trace_tine.simps)
  then show ?thesis
    using f1 by fastforce
qed 
qed

lemma Cut_subtree_trace_tine_1:
 assumes "tinesP F t" "t' \<in> tines F" "size t' \<le> size t"
  shows "trace_tine (Cut_subtree F t st) t' = trace_tine F t'"
  using assms
proof (induction arbitrary: t')
  case (Sucs k F t)
  then obtain l Fs where lFs:"F = Tree l Fs"
    using rtree.exhaust_sel by blast
  then show ?case 
  proof (cases "t'")
    case Nil
    then show ?thesis
      by (simp add: lFs) 
  next
    case (Cons k' t'')
    hence "size t'' \<le> size t"
      using Sucs.prems(2) by auto
    have ex:"Cut_subtree F (k#t) st = Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs))"
      using lFs by simp
    then show ?thesis 
    proof (cases "k' = k")
      case True
      hence "trace_tine (Cut_subtree F (k#t) st) t' 
= trace_tine (Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs))) (k'#t'')"
        using ex
        by (simp add: local.Cons)
      hence "trace_tine (Cut_subtree F (k#t) st) t' 
= l#trace_tine (Cut_subtree (Fs!k) t st) t''"
        by (smt Suc_diff_Suc Sucs.hyps(1) True append_take_drop_id diff_add_inverse diff_add_inverse2 lFs length_Cons length_append length_drop less_imp_add_positive list.simps(5) nth_append_length rtree.sel(2) trace_tine.simps)
        then show ?thesis
          using Sucs.IH Sucs.prems(1) True \<open>length t'' \<le> length t\<close> lFs local.Cons tinesP_Cons_iff tines_def by auto
    next
      case False
hence "trace_tine (Cut_subtree F (k#t) st) t' 
= trace_tine (Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs))) (k'#t'')"
        using ex
        by (simp add: local.Cons)
      hence "trace_tine (Cut_subtree F (k#t) st) t' 
= l#trace_tine (((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs))!k') t''"
        using Sucs.prems(1) lFs local.Cons tine_prefix_refl by fastforce
      hence "trace_tine (Cut_subtree F (k#t) st) t' 
= l#trace_tine (Fs!k') t''"
        using False assms
      proof -
        show ?thesis
          by (metis (no_types) False Sucs.hyps(1) Sucs.prems(1) \<open>trace_tine (Cut_subtree F (k # t) st) t' = l # trace_tine ((take k Fs @ Cut_subtree (Fs ! k) t st # drop (Suc k) Fs) ! k') t''\<close> lFs local.Cons mem_Collect_eq nth_list_update_neq_upd_conv_take_nth_drop rtree.sel(2) tinesP_Cons_iff tines_def)
      qed
        then show ?thesis
          using Sucs.IH Sucs.prems(1) False \<open>length t'' \<le> length t\<close> lFs local.Cons tinesP_Cons_iff tines_def by auto
    qed
  qed
next
  case (Nil F)
  then obtain l Fs where "F = Tree l Fs"
    using rtree.exhaust_sel by blast
then show ?case
  using Nil.prems(2) by auto 
qed

lemma Cut_subtree_tines_1:
 assumes "tinesP F t" "t' \<in> tines F" "size t' \<le> size t"
  shows "t' \<in> tines (Cut_subtree F t st)"
  using assms
proof (induction arbitrary: t')
  case (Sucs k F t)
  obtain Fs l where lFs: "F = Tree l Fs"
    using rtree.exhaust_sel by blast
  hence "(Cut_subtree F (k#t) st) = Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs))"
    by auto
  then show ?case 
  proof (cases "t'")
case Nil
  then show ?thesis
    by (simp add: tinesP.Nil tines_def) 
next
  case (Cons k' t'')
 hence 0:"size t'' \<le> size t"
      using Sucs.prems(2) local.Cons by auto
    have 1:"t'' \<in> tines (Fs!k')"
      using Sucs.prems(1) lFs local.Cons tinesP_Cons_iff tines_def by auto
  then show ?thesis 
  proof (cases "k = k'")
    case True
    hence "t'' \<in> tines (Cut_subtree (Fs!k) t st)"
      using 0 1
      using Sucs.IH lFs by auto
    hence "k#t'' \<in> tines (Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs)))"
proof -
  have f1: "sucs F = Fs"
    by (metis lFs rtree.sel(2))
  then have f2: "k \<le> length Fs"
    using Suc_leD Suc_leI Sucs.hyps(1) by blast
  have f3: "length (Cut_subtree (Fs ! k) t st # drop (Suc k) Fs) = length (drop k Fs)"
    using f1 by (metis (no_types) Suc_diff_Suc Sucs.hyps(1) length_Cons length_drop)
have f4: "k = length (take k Fs) + Suc (length (drop (Suc k) Fs)) - Suc (length (drop (Suc k) Fs))"
  using f2 by simp
  have "length (sucs F) = length (take k Fs @ drop k Fs)"
    using f1 by (metis append_take_drop_id)
  then show ?thesis
    using f4 f3 by (metis (no_types) Sucs.hyps(1) \<open>t'' \<in> tines (Cut_subtree (Fs ! k) t st)\<close> diff_add_inverse2 length_append mem_Collect_eq nth_append_length rtree.sel(2) tinesP.simps tines_def)
qed
  then show ?thesis
    using True \<open>Cut_subtree F (k # t) st = Tree l (take k Fs @ Cut_subtree (Fs ! k) t st # drop (Suc k) Fs)\<close> local.Cons by auto 
next
  case False
  hence " ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs)) ! k' = Fs ! k'"
    by (metis Sucs.hyps(1) Sucs.prems(1) lFs local.Cons nth_list_update_neq_upd_conv_take_nth_drop rtree.sel(2) tine_prefix_Subtree tine_prefix_refl)
  hence "k'#t'' \<in> tines (Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs)))"
    using Sucs.prems(1) lFs local.Cons tinesP_Cons_iff tines_def by auto
  then show ?thesis
    by (simp add: \<open>Cut_subtree F (k # t) st = Tree l (take k Fs @ Cut_subtree (Fs ! k) t st # drop (Suc k) Fs)\<close> local.Cons)
qed
qed
next
  case (Nil F)
  then show ?case
    by (simp add: tinesP.Nil tines_def) 
qed

lemma Cut_subtree_trace_tine_2:
  assumes "tinesP F t" "t' \<in> tines F" "size t' > size t" "\<not> prefix_list t t'"
  shows "trace_tine (Cut_subtree F t st) t' = trace_tine F t'"
  using assms
proof (induction arbitrary: t')
  case (Sucs k F t)
  obtain l Fs where "F = Tree l Fs"
    using rtree.exhaust_sel by blast
  then show ?case 
  proof (cases "t'")
case Nil
  then show ?thesis
    using Sucs.prems(2) by auto 
next
  case (Cons k' t'')
  then show ?thesis
  proof (cases "k' = k")
    case True
    hence "\<not> prefix_list t t''"
      using Sucs.prems(3) local.Cons prefix_list.Cons by fastforce
    have "t'' \<in> tines (Fs!k)"
      using Sucs.prems(1) True \<open>F = Tree l Fs\<close> local.Cons tinesP_Cons_iff tines_def by auto
    hence tmp:"trace_tine (Cut_subtree (Fs!k) t st) t'' = trace_tine (Fs!k) t''"
      by (metis (full_types) Cut_subtree_trace_tine_1 Sucs.IH Sucs.hyps(2) \<open>F = Tree l Fs\<close> \<open>\<not> prefix_list t t''\<close> leI rtree.sel(2) tines_def)
    hence "Cut_subtree F (k#t) st 
= Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs))"
      by (simp add: \<open>F = Tree l Fs\<close>)
    hence "trace_tine (Cut_subtree F (k#t) st) (k'#t'') 
= trace_tine (Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs))) (k'#t'')"
      by simp
    hence "... = l # trace_tine (Cut_subtree (Fs!k) t st) t''"
      by (smt Suc_diff_Suc Sucs.hyps(1) True \<open>F = Tree l Fs\<close> append_take_drop_id diff_add_inverse 
diff_add_inverse2 length_Cons length_append length_drop less_imp_add_positive list.simps(5) 
nth_append_length rtree.sel(2) trace_tine.simps)
    hence "... = l # trace_tine (Fs!k) t''"
      using tmp by blast
    hence "... = trace_tine F (k'#t'')"
      using Sucs.hyps(1) True \<open>F = Tree l Fs\<close> by auto
  then show ?thesis
    using \<open>trace_tine (Cut_subtree F (k # t) st) (k' # t'') = trace_tine (Tree l (take k Fs @ Cut_subtree (Fs ! k) t st # drop (Suc k) Fs)) (k' # t'')\<close> \<open>trace_tine (Tree l (take k Fs @ Cut_subtree (Fs ! k) t st # drop (Suc k) Fs)) (k' # t'') = l # trace_tine (Cut_subtree (Fs ! k) t st) t''\<close> local.Cons tmp by auto 
next
  case False
  have "Cut_subtree F (k#t) st 
= Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs))"
    using \<open>F = Tree l Fs\<close> by auto
  hence "trace_tine (Cut_subtree F (k#t) st) (k'#t'') 
= trace_tine (Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs))) (k'#t'')"
    using \<open>F = Tree l Fs\<close> by auto
 hence "trace_tine (Cut_subtree F (k#t) st) (k'#t'') 
= l # (trace_tine (((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs))!k') t'')"
   using Sucs.prems(1) \<open>F = Tree l Fs\<close> local.Cons tine_prefix_refl by fastforce
  hence "... =  l # (trace_tine (Fs!k') t'')"
    by (metis False Sucs.hyps(1) Sucs.prems(1) \<open>F = Tree l Fs\<close> local.Cons nth_list_update_neq_upd_conv_take_nth_drop rtree.sel(2) tine_prefix_Subtree tine_prefix_refl)
  hence "... = trace_tine F (k'#t'')"
    using Sucs.prems(1) \<open>F = Tree l Fs\<close> local.Cons tinesP_Cons_iff tines_def by auto
   
  then show ?thesis
    by (simp add: \<open>l # trace_tine ((take k Fs @ Cut_subtree (Fs ! k) t st # drop (Suc k) Fs) ! k') t'' = l # trace_tine (Fs ! k') t''\<close> \<open>trace_tine (Cut_subtree F (k # t) st) (k' # t'') = l # trace_tine ((take k Fs @ Cut_subtree (Fs ! k) t st # drop (Suc k) Fs) ! k') t''\<close> local.Cons) 
qed
qed
next
  case (Nil F)
  then show ?case
    by (simp add: Null) 
qed

lemma Cut_subtree_tines_2:
  assumes "tinesP F t" "t' \<in> tines F" "size t' > size t" "\<not> prefix_list t t'"
  shows "t' \<in> tines (Cut_subtree F t st)"
  using assms
proof (induction arbitrary: t')
  case (Sucs k F t)
  obtain Fs l where lFs: "F = Tree l Fs"
    using rtree.exhaust_sel by blast
  hence "(Cut_subtree F (k#t) st) = Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs))"
    by auto
  then show ?case 
  proof (cases "t'")
case Nil
  then show ?thesis
    by (simp add: tinesP.Nil tines_def) 
next
  case (Cons k' t'')
 hence 0:"size t'' > size t"
      using Sucs.prems(2) local.Cons by auto
    have 1:"t'' \<in> tines (Fs!k')"
      using Sucs.prems(1) lFs local.Cons tinesP_Cons_iff tines_def by auto
  then show ?thesis 
  proof (cases "k = k'")
    case True
    hence "\<not> prefix_list t t''"
      using Sucs.prems(3) local.Cons prefix_list.Cons by fastforce
    hence "t'' \<in> tines (Cut_subtree (Fs!k) t st)"
      using 0 1 True
      using Sucs.IH lFs
      by simp 
    hence "k#t'' \<in> tines (Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs)))"
proof -
  have f1: "sucs F = Fs"
    by (metis lFs rtree.sel(2))
  then have f2: "k \<le> length Fs"
    using Suc_leD Suc_leI Sucs.hyps(1) by blast
  have f3: "length (Cut_subtree (Fs ! k) t st # drop (Suc k) Fs) = length (drop k Fs)"
    using f1 by (metis (no_types) Suc_diff_Suc Sucs.hyps(1) length_Cons length_drop)
have f4: "k = length (take k Fs) + Suc (length (drop (Suc k) Fs)) - Suc (length (drop (Suc k) Fs))"
  using f2 by simp
  have "length (sucs F) = length (take k Fs @ drop k Fs)"
    using f1 by (metis append_take_drop_id)
  then show ?thesis
    using f4 f3 by (metis (no_types) Sucs.hyps(1) \<open>t'' \<in> tines (Cut_subtree (Fs ! k) t st)\<close> diff_add_inverse2 length_append mem_Collect_eq nth_append_length rtree.sel(2) tinesP.simps tines_def)
qed
  then show ?thesis
    using True \<open>Cut_subtree F (k # t) st = Tree l (take k Fs @ Cut_subtree (Fs ! k) t st # drop (Suc k) Fs)\<close> local.Cons by auto 
next
  case False
  hence " ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs)) ! k' = Fs ! k'"
    by (metis Sucs.hyps(1) Sucs.prems(1) lFs local.Cons nth_list_update_neq_upd_conv_take_nth_drop rtree.sel(2) tine_prefix_Subtree tine_prefix_refl)
  hence "k'#t'' \<in> tines (Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs)))"
    using Sucs.prems(1) lFs local.Cons tinesP_Cons_iff tines_def by auto
  then show ?thesis
    by (simp add: \<open>Cut_subtree F (k # t) st = Tree l (take k Fs @ Cut_subtree (Fs ! k) t st # drop (Suc k) Fs)\<close> local.Cons)
qed
qed
next
  case (Nil F)
  then show ?case
    by (simp add: Null)
qed

lemma Cut_subtree_trace_tine_3_lt:
  assumes "tinesP F t" "t' \<in> tines F" "size t' > size t" "prefix_list t t'" "t' ! size t < st" "t@[st] \<in> tines F"
  shows "trace_tine (Cut_subtree F t st) t' = trace_tine F t'"
  using assms
proof (induction arbitrary: t')
  case (Sucs k F t)
  then show ?case 
  proof (cases "t'")
    case Nil
    then show ?thesis
      using Sucs.prems(2) by auto 
  next
    case (Cons k' t'')
    obtain l Fs where lFs: "F = Tree l Fs"
      using rtree.exhaust by auto 
    hence "k = k'"
      using Sucs.prems(3) prefix_list_nth Cons by fastforce
    have "(Cut_subtree F (k#t) st) = Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs))"
      using lFs by auto
    hence "trace_tine (Cut_subtree F (k#t) st) (k'#t'') = 
trace_tine (Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs))) (k'#t'')"
      by simp
    hence "... = l# trace_tine (((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs))!k') t''"
      using Sucs.hyps(1) \<open>k = k'\<close> lFs list.simps(5) by auto
    hence "... = l# trace_tine (Fs!k') t''"
      by (smt Suc_less_eq Sucs.IH Sucs.prems(1) Sucs.prems(2) Sucs.prems(3) Sucs.prems(4) Sucs.prems(5) append_Cons append_take_drop_id diff_add_inverse diff_add_inverse2 lFs length_Cons length_append length_drop less_imp_add_positive list.distinct(1) list.inject local.Cons mem_Collect_eq nth_Cons_Suc nth_append_length prefix_list.cases rtree.sel(2) tinesP_Cons_iff tines_def)
    then show ?thesis
      using Sucs.hyps(1) \<open>k = k'\<close> lFs local.Cons trace_tine.simps by auto 
  qed
next
  case (Nil F)
  hence "t' ! 0 < st"
    by simp
  then obtain k t'' where "t' = k#t''" "k < st"
    by (metis (full_types) Nil.prems(2) less_not_refl neq_Nil_conv nth_Cons_0)
  have "[]@[st] \<in> tines F"
    using Nil.prems(5) by blast
  hence stlt:"st < size (sucs F)"
    using tine_prefix_refl by fastforce
  obtain Fs l where lFs:"F = Tree l Fs"
    using rtree.exhaust by auto 
  hence "Cut_subtree (Tree l Fs) [] st = Tree l (cut_nth st Fs)"
    by simp
  hence "trace_tine (Cut_subtree (Tree l Fs) [] st) (k#t'') = 
        trace_tine (Tree l (cut_nth st Fs)) (k#t'')"
    by simp
  hence "trace_tine (Cut_subtree (Tree l Fs) [] st) (k#t'') = l # (trace_tine ((cut_nth st Fs)!k) t'')"
    using stlt lFs
  proof -
    obtain nn :: nat and nns :: "nat list" where
f1: "t' = nn # nns \<and> nn < st"
      by (meson \<open>\<And>thesis. (\<And>k t''. \<lbrakk>t' = k # t''; k < st\<rbrakk> \<Longrightarrow> thesis) \<Longrightarrow> thesis\<close>)
    have "st = length (take st (sucs F))"
      using stlt by force
    then have "k < length (cut_nth st Fs)"
      using f1 by (metis (no_types) \<open>t' = k # t''\<close> cut_nth_take_drop_Suc lFs leI length_append less_le_trans list.inject not_add_less1 rtree.sel(2) stlt)
then show ?thesis
  by simp
qed
  hence "trace_tine (Cut_subtree (Tree l Fs) [] st) (k#t'') = l # (trace_tine (((take st Fs) @ (drop (Suc st) Fs))!k) t'')"
    using cut_nth_take_drop_Suc lFs stlt by force
  hence "trace_tine (Cut_subtree (Tree l Fs) [] st) (k#t'') = l # (trace_tine ((take st Fs)!k) t'')"
    using stlt
    by (metis \<open>t' = k # t''\<close> lFs length_take list.inject list.size(3) local.Nil(4) min_less_iff_conj nth_Cons_0 nth_append order.strict_trans rtree.sel(2))
  hence "trace_tine (Cut_subtree (Tree l Fs) [] st) (k#t'') = l # (trace_tine (Fs!k) t'')"
    using stlt
    using \<open>k < st\<close> by auto
  then show ?case
    using Nil.prems(1) \<open>t' = k # t''\<close> lFs tinesP_Cons_iff tines_def by auto
qed

lemma Cut_subtree_tines_3_lt:
  assumes "tinesP F t" "t' \<in> tines F" "size t' > size t" "prefix_list t t'" "t' ! size t < st" "t@[st] \<in> tines F"
  shows "t' \<in> tines (Cut_subtree F t st)"
  using assms
proof (induction arbitrary: t')
  case (Sucs k F t)
  obtain Fs l where lFs: "F = Tree l Fs"
    using rtree.exhaust_sel by blast
  hence "(Cut_subtree F (k#t) st) = Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs))"
    by auto
  then show ?case 
  proof (cases "t'")
    case Nil
  then show ?thesis
    using Sucs.prems(2) by auto
  next
  case (Cons k' t'')
 hence 0:"size t'' > size t"
      using Sucs.prems(2) local.Cons by auto
    have 1:"t'' \<in> tines (Fs!k')"
      using Sucs.prems(1) lFs local.Cons tinesP_Cons_iff tines_def by auto
    have kk:"k = k'"
      using Sucs.prems(3) local.Cons prefix_list_nth by fastforce
    hence "prefix_list t t''"
      using Sucs.prems(3) local.Cons prefix_list.cases by auto
    hence "t'' \<in> tines (Cut_subtree (Fs!k) t st)"
      using 0 1 kk
      using Sucs.IH lFs
      using Sucs.prems(4) Sucs.prems(5) local.Cons tinesP_Cons_iff tines_def by auto
    hence "k#t'' \<in> tines (Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs)))"
proof -
  have f1: "sucs F = Fs"
    by (metis lFs rtree.sel(2))
  then have f2: "k \<le> length Fs"
    using Suc_leD Suc_leI Sucs.hyps(1) by blast
  have f3: "length (Cut_subtree (Fs ! k) t st # drop (Suc k) Fs) = length (drop k Fs)"
    using f1 by (metis (no_types) Suc_diff_Suc Sucs.hyps(1) length_Cons length_drop)
have f4: "k = length (take k Fs) + Suc (length (drop (Suc k) Fs)) - Suc (length (drop (Suc k) Fs))"
  using f2 by simp
  have "length (sucs F) = length (take k Fs @ drop k Fs)"
    using f1 by (metis append_take_drop_id)
  then show ?thesis
    using f4 f3 by (metis (no_types) Sucs.hyps(1) \<open>t'' \<in> tines (Cut_subtree (Fs ! k) t st)\<close> diff_add_inverse2 length_append mem_Collect_eq nth_append_length rtree.sel(2) tinesP.simps tines_def)
qed
  then show ?thesis
    using \<open>Cut_subtree F (k # t) st = Tree l (take k Fs @ Cut_subtree (Fs ! k) t st # drop (Suc k) Fs)\<close> kk local.Cons by auto
qed
next
  case (Nil F)
 hence "t' ! 0 < st"
    by simp
  then obtain k t'' where "t' = k#t''" "k < st"
    by (metis (full_types) Nil.prems(2) less_not_refl neq_Nil_conv nth_Cons_0)
  have "[]@[st] \<in> tines F"
    using Nil.prems(5) by blast
  hence stlt:"st < size (sucs F)"
    using tine_prefix_refl by fastforce
  obtain Fs l where lFs:"F = Tree l Fs"
    using rtree.exhaust by auto 
  hence "Cut_subtree (Tree l Fs) [] st = Tree l (cut_nth st Fs)"
    by simp
  have tmp:"t'' \<in> tines (Fs!k)"
    using Nil.prems(1) \<open>t' = k # t''\<close> lFs tinesP_Cons_iff tines_def by auto
  have "(cut_nth st Fs) ! k = Fs ! k"
    by (metis (no_types, lifting) \<open>\<And>thesis. (\<And>k t''. \<lbrakk>t' = k # t''; k < st\<rbrakk> \<Longrightarrow> thesis) \<Longrightarrow> thesis\<close> \<open>t' = k # t''\<close> cut_nth_take_drop_Suc lFs length_take list.inject min_less_iff_conj nth_append nth_take order.strict_trans rtree.sel(2) stlt)
  hence "k#t'' \<in> tines (Tree l (cut_nth st Fs))"
    using tmp
    by (smt Suc_diff_Suc Suc_less_eq \<open>k < st\<close> add_Suc cut_nth_take_drop_Suc diff_add_inverse diff_add_inverse2 id_take_nth_drop lFs length_Cons length_append length_drop less_imp_Suc_add less_imp_add_positive less_trans_Suc mem_Collect_eq rtree.sel(2) stlt tinesP_Cons_iff tines_def)
  then show ?case
    by (simp add: \<open>t' = k # t''\<close> lFs)
qed

lemma Cut_subtree_trace_tine_3_gt:
  assumes "tinesP F t" "t' \<in> tines F" "size t' > size t" "prefix_list t t'" "t' ! size t > st" "t@[st] \<in> tines F"
  shows "trace_tine (Cut_subtree F t st) (t'[(size t):= t'!(size t) - 1]) = trace_tine F t'"
  using assms
proof (induction arbitrary: t')
  case (Sucs k F t)
  then show ?case 
  proof (cases "t'")
    case Nil
    then show ?thesis
      using Sucs.prems(2) by auto 
  next
    case (Cons k' t'')
    obtain l Fs where lFs: "F = Tree l Fs"
      using rtree.exhaust by auto 
    hence "k = k'"
      using Sucs.prems(3) prefix_list_nth Cons by fastforce
    have "(Cut_subtree F (k#t) st) = Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs))"
      using lFs by auto
    hence "trace_tine (Cut_subtree F (k#t) st) ((k'#t'')[(size (k#t)):= ((k'#t'')!(size (k#t)) - 1)])= 
trace_tine (Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs))) ((k'#t'')[(size (k#t)):= ((k'#t'')!(size (k#t)) - 1)])"
      by simp
    hence "... = l# trace_tine (((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs))!k') 
(t''[(size t):= (t''!(size t) - 1)])"
      using Sucs.hyps(1) \<open>k = k'\<close> lFs by auto
    hence "... = l# trace_tine ((Cut_subtree (Fs!k) t st)) (t''[(size t):= (t''!(size t) - 1)])"
    proof -
      have "k' = min (length Fs) k'"
        using Sucs.hyps(1) \<open>k = k'\<close> lFs by force
      then show ?thesis
        by (metis (no_types) \<open>k = k'\<close> length_take nth_append_length)
    qed
  hence "... = l# trace_tine (Fs!k) t''"
    using Sucs.IH Sucs.prems(1) Sucs.prems(2) Sucs.prems(3) Sucs.prems(4) Sucs.prems(5) lFs local.Cons prefix_list.cases tinesP_Cons_iff tines_def by auto
    then show ?thesis
      using Sucs.hyps(1) \<open>k = k'\<close> \<open>l # trace_tine ((take k Fs @ Cut_subtree (Fs ! k) t st # drop (Suc k) Fs) ! k') (t''[length t := t'' ! length t - 1]) = l # trace_tine (Cut_subtree (Fs ! k) t st) (t''[length t := t'' ! length t - 1])\<close> lFs list.simps(5) local.Cons by auto
  
 qed
next
  case (Nil F)
  hence "t' ! 0 > st"
    by simp
  then obtain k t'' where "t' = k#t''" "k > st"
    by (metis (full_types) Nil.prems(2) less_not_refl neq_Nil_conv nth_Cons_0)
  have "[]@[st] \<in> tines F"
    using Nil.prems(5) by blast
  hence stlt:"st < size (sucs F)"
    using tine_prefix_refl by fastforce
  obtain Fs l where lFs:"F = Tree l Fs"
    using rtree.exhaust by auto 
  hence "Cut_subtree (Tree l Fs) [] st = Tree l (cut_nth st Fs)"
    by simp
  hence "trace_tine (Cut_subtree (Tree l Fs) [] st) ((k#t'')[(size []):= ((k#t'')!(size []) - 1)]) = 
        trace_tine (Tree l (cut_nth st Fs)) ((k#t'')[(size []):= ((k#t'')!(size []) - 1)])"
    by simp
  hence "trace_tine (Cut_subtree (Tree l Fs) [] st) ((k#t'')[(size []):= ((k#t'')!(size []) - 1)]) 
        = trace_tine (Tree l (cut_nth st Fs)) ((k#t'')[0:= (k- 1)])"
    using stlt lFs by simp
hence tmp:"trace_tine (Cut_subtree (Tree l Fs) [] st) ((k#t'')[(size []):= ((k#t'')!(size []) - 1)]) 
        = trace_tine (Tree l (cut_nth st Fs)) ((k-1)#t'')"
  using stlt lFs
  by auto 
 hence "trace_tine (Cut_subtree (Tree l Fs) [] st) ((k#t'')[(size []):= ((k#t'')!(size []) - 1)]) 
        = l # (trace_tine ((cut_nth st Fs) ! (k-1)) t'')"
 proof -
   have "Suc (size (cut_nth st Fs)) = size Fs"
     using cut_nth_take_drop_Suc lFs stlt by fastforce
   hence "(size (cut_nth st Fs)) > k-1"
     by (metis Nil.prems(1) One_nat_def Suc_diff_Suc Suc_less_SucD \<open>st < k\<close> \<open>t' = k # t''\<close> diff_zero lFs less_Suc_eq_0_disj less_imp_Suc_add rtree.sel(2) tine_prefix_Subtree tine_prefix_refl)
   thus ?thesis using tmp
     by simp
 qed
  hence "... = l # (trace_tine (((take st Fs) @ (drop (Suc st) Fs))!(k-1)) t'')"
    using stlt lFs cut_nth_take_drop_Suc
    by (simp add: cut_nth_take_drop_Suc)
hence "... = l # (trace_tine ((drop (Suc st) Fs)!(k-1- st)) t'')"
proof -
  obtain nn :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
    "\<forall>x0 x1. (\<exists>v2. x0 = Suc (x1 + v2)) = (x0 = Suc (x1 + nn x0 x1))"
    by moura
  then have "\<forall>n na. \<not> n < na \<or> na = Suc (n + nn na n)"
    by (meson less_imp_Suc_add)
  then have f1: "k - 1 = st + nn k st"
    by (metis \<open>st < k\<close> diff_Suc_1)
  have "length (take st Fs) = st"
    using lFs stlt by force
  then show ?thesis
    using f1 by (simp add: nth_append)
qed

hence "... = l # (trace_tine ((drop st Fs)!(k-1- st +1)) t'')"
  by (metis Cons_nth_drop_Suc Suc_eq_plus1 lFs nth_Cons_Suc rtree.sel(2) stlt)
  hence "... = l # (trace_tine (Fs!(k-1- st +1 + st)) t'')"
  proof -
    have "length (take st Fs) = st + v2_0 (length (sucs F)) st - v2_0 (length (sucs F)) st"
      using lFs stlt by fastforce
    then show ?thesis
      by (metis (no_types) add.commute append_take_drop_id diff_add_inverse2 not_add_less1 nth_append)
  qed
  hence "... = l # (trace_tine (Fs!k) t'')"
    by (metis Suc_diff_Suc Suc_eq_plus1 \<open>st < k\<close> add.commute diff_Suc_eq_diff_pred le_add_diff_inverse less_imp_le_nat)
  then show ?case
    using Nil.prems(1) \<open>l # trace_tine ((take st Fs @ drop (Suc st) Fs) ! (k - 1)) t'' = l # trace_tine (drop (Suc st) Fs ! (k - 1 - st)) t''\<close> \<open>l # trace_tine (cut_nth st Fs ! (k - 1)) t'' = l # trace_tine ((take st Fs @ drop (Suc st) Fs) ! (k - 1)) t''\<close> \<open>l # trace_tine (drop (Suc st) Fs ! (k - 1 - st)) t'' = l # trace_tine (drop st Fs ! (k - 1 - st + 1)) t''\<close> \<open>l # trace_tine (drop st Fs ! (k - 1 - st + 1)) t'' = l # trace_tine (Fs ! (k - 1 - st + 1 + st)) t''\<close> \<open>t' = k # t''\<close> \<open>trace_tine (Cut_subtree (Tree l Fs) [] st) ((k # t'')[length [] := (k # t'') ! length [] - 1]) = l # trace_tine (cut_nth st Fs ! (k - 1)) t''\<close> lFs tine_prefix_refl by fastforce
qed

lemma Cut_subtree_tines_3_gt:
  assumes "tinesP F t" "t' \<in> tines F" "size t' > size t" "prefix_list t t'" "t' ! size t > st" "t@[st] \<in> tines F"
  shows "(t'[(size t):= t'!(size t) - 1]) \<in> tines (Cut_subtree F t st)"
  using assms
proof (induction arbitrary: t')
  case (Sucs k F t)
  obtain Fs l where lFs: "F = Tree l Fs"
    using rtree.exhaust_sel by blast
  hence "(Cut_subtree F (k#t) st) = Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs))"
    by auto
  then show ?case 
  proof (cases "t'")
    case Nil
  then show ?thesis
    using Sucs.prems(2) by auto
  next
  case (Cons k' t'')
 hence 0:"size t'' > size t"
      using Sucs.prems(2) local.Cons by auto
    have 1:"t'' \<in> tines (Fs!k')"
      using Sucs.prems(1) lFs local.Cons tinesP_Cons_iff tines_def by auto
    have kk:"k = k'"
      using Sucs.prems(3) local.Cons prefix_list_nth by fastforce
    hence ptt:"prefix_list t t''"
      using Sucs.prems(3) local.Cons prefix_list.cases by auto
      have kFs:"k < size Fs"
        using Sucs.hyps(1) lFs by auto
      hence nthx:" ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs)) ! k = 
                (Cut_subtree (Fs!k) t st)"
        by (metis nth_list_update_eq upd_conv_take_nth_drop)
    have "t'' ! size t > st"
      using Sucs.prems(4) local.Cons by auto
    hence tmp:"(t''[(size t):= t''!(size t) - 1]) \<in> tines (Cut_subtree (Fs!k) t st)"
      using 0 1 kk ptt
      using Sucs.IH lFs
      using Sucs.prems(5) tinesP_Cons_iff tines_def by auto
    hence "k#(t''[(size t):= t''!(size t) - 1]) \<in> tines (Tree l ((take k Fs)@(Cut_subtree (Fs!k) t st)#(drop (Suc k) Fs)))"
      using nthx kFs
      using length_append mem_Collect_eq tinesP_Cons_iff tines_def by auto
  then show ?thesis
    using \<open>Cut_subtree F (k # t) st = Tree l (take k Fs @ Cut_subtree (Fs ! k) t st # drop (Suc k) Fs)\<close> kk local.Cons by auto
qed
next
  case (Nil F)
 hence "t' ! 0 > st"
    by simp
  then obtain k t'' where "t' = k#t''" "k > st"
    by (metis (full_types) Nil.prems(2) less_not_refl neq_Nil_conv nth_Cons_0)
  have "[]@[st] \<in> tines F"
    using Nil.prems(5) by blast
  hence stlt:"st < size (sucs F)"
    using tine_prefix_refl by fastforce
  obtain Fs l where lFs:"F = Tree l Fs"
    using rtree.exhaust by auto 
  hence "Cut_subtree (Tree l Fs) [] st = Tree l (cut_nth st Fs)"
    by simp
  have tmp:"t'' \<in> tines (Fs!k)"
    using Nil.prems(1) \<open>t' = k # t''\<close> lFs tinesP_Cons_iff tines_def by auto
  have "(cut_nth st Fs) ! (k-1) = Fs ! k"
    by (smt Suc_diff_Suc Suc_le_eq \<open>st < k\<close> append_take_drop_id cancel_ab_semigroup_add_class.diff_right_commute cut_nth_take_drop_Suc diff_Suc_1 diff_add_inverse diff_add_inverse2 lFs length_append length_drop less_Suc_eq_le less_imp_Suc_add less_imp_add_positive not_add_less1 nth_append rtree.sel(2) stlt)
 hence "(k-1)#t'' \<in> tines (Tree l (cut_nth st Fs))"
   using tmp
   by (smt Cons_nth_drop_Suc Nil.prems(1) One_nat_def Suc_diff_Suc Suc_less_eq \<open>st < k\<close> \<open>t' = k # t''\<close> add_Suc_right cut_nth_take_drop_Suc diff_zero id_take_nth_drop lFs length_append length_drop less_imp_Suc_add mem_Collect_eq rtree.sel(2) stlt tinesP_Cons_iff tines_def zero_less_Suc)
 then show ?case
   by (simp add: \<open>t' = k # t''\<close> lFs)
qed

lemma picked_stricly_inc_prefix_le:
  assumes "strictly_inc F \<and> label F \<le> n"
  shows "\<exists>F'. F' \<le> F \<and> (\<forall>a. a \<le> n \<longrightarrow> count (mset_of_tree F') a = count (mset_of_tree F) a)"
  using assms
proof (induction F)
  case (Tree l Fs)
  hence "\<forall>m. m < size Fs \<longrightarrow> (\<exists>f'. f' \<le> Fs ! m \<and> (\<forall>a. a \<le> n \<longrightarrow> count (mset_of_tree f') n 
          = count (mset_of_tree (Fs!m)) n))"
    by blast
  then obtain toF where "\<forall>m. m < size Fs \<longrightarrow> toF m \<le> Fs ! m \<and> (\<forall>a. a \<le> n \<longrightarrow> count (mset_of_tree (toF m)) a 
          = count (mset_of_tree (Fs!m)) a) "
    by blast
  then show ?case 
  proof (induction Fs)
    case Nil
    then show ?case
      by blast
  next
    case (Cons hf tf)
    then obtain Fx where Fx:"Fx \<le> (Tree l tf) \<and> (\<forall>a. a \<le> n \<longrightarrow> count (mset_of_tree Fx) a = count (mset_of_tree (Tree l tf)) a)"
      by blast
    obtain ll fx where llfx:"Fx = Tree ll fx"
      using rtree.exhaust_sel by blast
    hence "Tree ll (hf#fx) \<le> (Tree l (hf#tf))"
      using Fx
      by (metis less_eq_rtree.transfer match.intros(2) order_refl prefix_Tree_Tree_eq)
    hence "(\<forall>a. a \<le> n \<longrightarrow> count (mset_of_tree (Tree ll (hf#fx))) a = count (mset_of_tree (Tree l (hf#tf))) a)"
      using Fx llfx
      by (metis (full_types) Extend_tree.simps(1) Extend_tree_mset_of_tree count_union tinesP.Nil)
    then show ?case
      by blast 
  qed
qed

lemma prefix_tree_exist_same_size_tine:
  assumes "F \<le> F'" "tinesP F t"
  shows "\<exists>t'. tine_prefix F F' t t' \<and> size t = size t'"
  using assms  unfolding leq_iff_prefix
proof (induction arbitrary: t rule: prefix.induct)
  case (1 Fs Fs' l)
  hence mt:"match (\<lambda>x1 x2. prefix x1 x2 \<and>
                    (\<forall>x. tinesP x1 x \<longrightarrow> (\<exists>t'. tine_prefix x1 x2 x t' \<and> length x = length t')))
     Fs Fs'"
    by simp
  have "(Tree l Fs) \<le> (Tree l Fs')"
    by (metis (mono_tags, lifting) less_eq_rtree.transfer match_mono_aux mt prefix.intros)
  then show ?case 
  proof (cases t)
    case Nil
    have "[] \<in> tines (Tree l Fs)"
      by (simp add: tinesP.Nil tines_def)    
    hence "tine_prefix (Tree l Fs) (Tree l Fs') t []"
      by (metis (mono_tags, lifting) "1.IH" less_eq_rtree.transfer local.Nil match_mono_aux prefix_Tree_Tree_eq tine_prefix.Nil tinesP.Nil) 
   thus ?thesis
     by (simp add: local.Nil)
  next
    case (Cons k tt)
    note Cons2 = this
    thm Cons2
    hence k:"k < size Fs"
      using "1.prems"(1) tinesP_Cons_iff by auto
    have "tinesP (Fs!k) tt" 
      using "1.prems"(1) local.Cons tinesP_Cons_iff by auto   
  then show ?thesis 
  proof (cases Fs)
    case Nil
    then show ?thesis
      using \<open>k < length Fs\<close> by auto 
  next
    case (Cons Fsh Fst)
    hence "\<forall>x1 \<in> set Fs. \<exists>x2 \<in> set Fs'. (prefix x1 x2 \<and>
                    ((\<forall>x. tinesP x1 x \<longrightarrow> (\<exists>t'. tine_prefix x1 x2 x t' \<and> length x = length t'))))"
      using match_Exist Cons mt by fastforce 
    then obtain x2 where x2:"x2 \<in> set Fs'" "(prefix (Fs!k) x2 \<and>
                    ((\<forall>x. tinesP (Fs!k) x \<longrightarrow> (\<exists>t'. tine_prefix (Fs!k) x2 x t' \<and> length x = length t'))))"
      using \<open>k < length Fs\<close> by force
    obtain k2 where k2:"k2 < size Fs' \<and> Fs'!k2 = x2"
      by (meson \<open>x2 \<in> set Fs'\<close> in_set_conv_nth)  
    obtain tt' where "tine_prefix (Fs!k) x2 tt tt' \<and> length tt = length tt'"
      using \<open>prefix (Fs ! k) x2 \<and> ((\<forall>x. tinesP (Fs ! k) x \<longrightarrow> (\<exists>t'. tine_prefix (Fs ! k) x2 x t' \<and> length x = length t')))\<close> \<open>tinesP (Fs ! k) tt\<close> last_in_set tines_exist_last_label_exist trace_tine_non_empty by auto
    hence "tine_prefix (Tree l Fs) (Tree l Fs') (k#tt) (k2#tt') "
      using k k2 Cons2 x2 1 assms
    proof -
      have "k2 < size Fs' \<and> k < size Fs"
        by (simp add: k k2)
      have "tine_prefix (Fs ! k) (Fs' ! k2) tt tt'"
        by (simp add: \<open>tine_prefix (Fs ! k) x2 tt tt' \<and> length tt = length tt'\<close> k2)
      thus ?thesis
        using Subtree \<open>Tree l Fs \<le> Tree l Fs'\<close> \<open>k2 < length Fs' \<and> k < length Fs\<close> by auto
    qed
    then show ?thesis
      by (metis Cons2 \<open>tine_prefix (Fs ! k) x2 tt tt' \<and> length tt = length tt'\<close> length_Cons) 
  qed
  qed
qed

lemma Extend_tree_Cut_subtree_preserves_strictly_inc_2:
  assumes "F \<turnstile> w" "t@[k] \<in> tines F" "t' \<in> tines (Cut_subtree F t k)" "lb (Cut_subtree F t k) t' < lb F (t@[k])"
  shows "strictly_inc (Extend_tree (Cut_subtree F t k) t' (get_subtree F (t@[k])))"
  using assms
  by (metis Cut_subtree_strictly_inc Extend_tree_preserves_strictly_inc fork_F forks_def 
get_subtree_strictly_inc last_in_tine_is_label_in_subtree lb_def le_eq_less_or_eq less_Suc_eq_le 
mem_Collect_eq snoc_eq_iff_butlast suc_tine_size_eq_label_list take_Suc_nth take_all tines_def)

lemma cut_nth_match:
  assumes "match P l l'" "k < size l'" "\<forall>x \<in> set l. \<not> (P x (l' ! k))"
  shows "match P l (cut_nth k l')"
  using assms
proof (induction arbitrary: k)
  case match_Nil_Nil
  then show ?case
    by simp 
next
  case (match_true x y xs ys)
  then show ?case 
  proof (cases k)
    case 0
    then show ?thesis
      using match_true.hyps(1) match_true.prems(2) by auto 
  next
    case (Suc k)
    have "\<forall>z \<in> set (x#xs). \<not> (P z ((y#ys) ! Suc k))"
      using Suc match_true.prems(2) by blast
    hence "\<forall>z \<in> set xs. \<not> (P z (ys ! k))"
      by auto
    hence "match P xs (cut_nth k ys)"
      using Suc match_true.IH match_true.prems(1) by auto
    hence "match P (x#xs) (y#(cut_nth k ys))"
      by (simp add: match.match_true match_true.hyps(1))
    then show ?thesis
      by (simp add: Suc)
  qed
next
  case (match_extra xs ys y)
  then show ?case 
 proof (cases k)
   case 0
   hence "match P xs ys"
     by (simp add: match_extra.hyps)
   have "cut_nth 0 (y#ys) = ys"
     using cut_nth.simps
     by simp
   hence "match P xs (cut_nth 0 (y#ys))"
     by (simp add: match_extra.hyps)
    then show ?thesis
      by (simp add: "0")
  next
    case (Suc k)
    have "\<forall>z \<in> set xs. \<not> (P z ((y#ys) ! Suc k))"
      using Suc match_extra.prems(2) by auto
    hence "\<forall>z \<in> set xs. \<not> (P z (ys ! k))"
      by auto
    hence "match P xs (cut_nth k ys)"
      using Suc match_extra.IH match_extra.prems(1) by auto
    hence "match P xs (y# (cut_nth k ys))"
      by (simp add: match.match_extra)
    thus ?thesis
      by (simp add: Suc)
  qed
qed

lemma exist_sucs_prefix_eq_count:
  assumes "F \<le> F'" "count (mset_of_tree F) a = count (mset_of_tree F') a"
"k' < size (sucs F')" "count (mset_of_tree (sucs F'!k' )) a > 0" 
  shows "\<exists>k. k < size (sucs F) \<and> ((sucs F) ! k) \<le> (sucs F') ! k'"
proof (rule ccontr)
  assume "\<not> (\<exists>k. k < size (sucs F) \<and> ((sucs F) ! k) \<le> (sucs F') ! k')"
  hence "\<forall>k. k < size (sucs F) \<longrightarrow> \<not> ((sucs F) ! k) \<le> (sucs F') ! k'"
    by blast
  hence "\<forall>f \<in> set (sucs F). \<not> f \<le> (sucs F') ! k'"
    using exist_num_list by force
  hence "match prefix (sucs F) (cut_nth k' (sucs F'))"
    using cut_nth_match
    by (metis assms(1) assms(3) less_eq_rtree.transfer prefix_Tree_Tree_eq rtree.collapse)
  hence "Tree (label F) (sucs F) \<le> Tree (label F) (cut_nth k' (sucs F'))"
    by (metis less_eq_rtree.transfer prefix.intros)
  hence "count (mset_of_tree (Tree (label F) (sucs F))) a \<le> count (mset_of_tree (Tree (label F) (cut_nth k' (sucs F')))) a"
    using prefix_count_le by blast
  hence "count (mset_of_tree F) a \<le> count (mset_of_tree (Tree (label F) (cut_nth k' (sucs F')))) a"
    by simp
  hence "count (mset_of_tree F') a \<le> count (mset_of_tree (Tree (label F) (cut_nth k' (sucs F')))) a"
    by (simp add: assms(2))
  hence "count (mset_of_tree (Tree (label F') (sucs F'))) a \<le> 
count (mset_of_tree (Tree (label F) (cut_nth k' (sucs F')))) a"
    by simp
  hence "count ({#label F'#} +     
 (\<Sum>x \<in># mset (sucs F'). mset_of_tree x)) a
\<le> count ( {#label F'#} +
(\<Sum>x \<in># mset (cut_nth k' (sucs F')). mset_of_tree x)) a"
    by (metis assms(1) less_eq_rtree.transfer mset_of_tree.simps prefix_Tree_Tree_eq rtree.collapse)
  hence "count ((\<Sum>x \<in># mset (sucs F'). mset_of_tree x)) a
\<le> count ((\<Sum>x \<in># mset (cut_nth k' (sucs F')). mset_of_tree x)) a"
    by (metis add_le_cancel_left count_union)
  hence "count ((\<Sum>x \<in># mset ((take k' (sucs F')) @ (drop k' (sucs F'))). mset_of_tree x)) a
\<le> count ((\<Sum>x \<in># mset ((take k' (sucs F')) @ (drop (Suc k') (sucs F'))). mset_of_tree x)) a"
    by (simp add: assms(3) cut_nth_take_drop_Suc)
   hence "count ((\<Sum>x \<in># mset (take k' (sucs F')) + mset (drop k' (sucs F')). mset_of_tree x)) a
\<le> count ((\<Sum>x \<in># mset (take k' (sucs F')) + mset (drop (Suc k') (sucs F')). mset_of_tree x)) a"
     by (metis mset_append)
   hence "count (\<Sum>x \<in># mset (drop k' (sucs F')). mset_of_tree x) a
\<le> count (\<Sum>x \<in># mset (drop (Suc k') (sucs F')). mset_of_tree x) a"
     by auto
   hence "count (\<Sum>x \<in># mset ((sucs F' ! k')# (drop (Suc k') (sucs F'))). mset_of_tree x) a
\<le> count (\<Sum>x \<in># mset (drop (Suc k') (sucs F')). mset_of_tree x) a"
     by (simp add: Cons_nth_drop_Suc assms(3))
hence "count (mset_of_tree (sucs F' ! k')) a + count (\<Sum>x \<in># mset (drop (Suc k') (sucs F')). mset_of_tree x) a
\<le> count (\<Sum>x \<in># mset (drop (Suc k') (sucs F')). mset_of_tree x) a"
  by simp
    hence "count (mset_of_tree (sucs F' ! k')) a \<le> 0"
  by simp
  thus "False"
    using assms(4) by linarith
qed

lemma cut_nth_match_head:
  assumes "match P xs ys" "ys \<noteq> []" "\<forall>k. k < size xs \<longrightarrow> \<not> match P (cut_nth k xs) (tl ys)"
  shows "match P xs (tl ys)"
  using assms
proof (induction)
  case match_Nil_Nil
  then show ?case by blast
next
  case (match_true x y xs ys)
  hence "\<forall>k<length (x # xs). \<not> match P (cut_nth k (x # xs)) (tl (y # ys))"
    by blast
  hence "\<forall>k<length (x # xs). \<not> match P (cut_nth k (x # xs)) ys"
    by auto
  then show ?case
    using match_true.hyps(2) by auto 
next
  case (match_extra xs ys y)
  then show ?case
    by (simp add: match.match_extra) 
qed


lemma cut_nth_match_any:
  assumes "match P xs ys" "k' < size ys" "\<forall>k. k < size xs \<longrightarrow> \<not> match P (cut_nth k xs) (cut_nth k' ys)"
  shows "match P xs  (cut_nth k' ys)"
  using assms
proof (induction k' arbitrary: ys xs)
  case 0
  then show ?case
  proof (cases ys)
    case Nil
    then show ?thesis
      using "0.prems"(1) by blast 
  next
    case (Cons hy ty)
    hence "cut_nth 0 (hy#ty) = tl (hy#ty)"
      by auto
    then show ?thesis
      using cut_nth_match_head
      by (metis "0.prems"(1) "0.prems"(3) list.simps(3) local.Cons)
  qed
next
  case (Suc k')
  hence "Suc k' < size ys" "\<forall>k. k < size xs \<longrightarrow> \<not> match P (cut_nth k xs) (cut_nth (Suc k') ys)"
    apply simp
    by (simp add: Suc.prems(3))
  then obtain hy ty where "ys = hy#ty"
    by (meson Suc_lessE length_Suc_conv)
  hence "\<forall>k. k < size xs \<longrightarrow> \<not> match P (cut_nth k xs) (hy#(cut_nth k' ty))"
    using \<open>\<forall>k<length xs. \<not> match P (cut_nth k xs) (cut_nth (Suc k') ys)\<close> by auto
  hence "\<forall>k. k < size xs \<longrightarrow> \<not> match P (cut_nth k xs) (cut_nth k' ty)"
    by (meson match_extra)
  then show ?case 
  proof (cases xs)
case Nil
  then show ?thesis
    by simp 
next
  case (Cons hx tx)
  hence "match P tx ty \<or> match P (hx#tx) ty"
    using Suc.prems(1) \<open>ys = hy # ty\<close> by auto
  then show ?thesis 
  proof (cases "match P (hx#tx) ty")
    case True
    then show ?thesis
      using Suc.IH Suc.prems(2) \<open>\<forall>k<length xs. \<not> match P (cut_nth k xs) (cut_nth k' ty)\<close> \<open>ys = hy # ty\<close> 
local.Cons match_extra
      by (simp add: match_extra) 
  next
    case False
    hence "match P tx ty \<and> P hx hy"
      using Suc.prems(1) \<open>ys = hy # ty\<close> local.Cons by blast
   hence "\<forall>k. ((k>0 \<and> k< size xs) \<longrightarrow>  \<not> match P (cut_nth k xs) (hy#(cut_nth k' ty)))"
     by (simp add: \<open>\<forall>k<length xs. \<not> match P (cut_nth k xs) (hy # cut_nth k' ty)\<close>)
   hence "\<forall>k. (Suc k < size xs \<longrightarrow>  \<not> match P (cut_nth (Suc k) xs) (hy#(cut_nth k' ty)))"
     by auto
   hence "\<forall>k. (k < size tx \<longrightarrow>  \<not> match P (hx#(cut_nth k tx)) (hy#(cut_nth k' ty)))"
     by (simp add: local.Cons)
    hence "\<forall>k. (k < size tx \<longrightarrow>  \<not> match P (cut_nth k tx) (cut_nth k' ty))"
      by (meson \<open>match P tx ty \<and> P hx hy\<close> match_true)
    hence "match P tx (cut_nth k' ty)"
      using Suc.IH Suc.prems(2) \<open>match P tx ty \<and> P hx hy\<close> \<open>ys = hy # ty\<close> by auto
    hence "match P xs (hy#(cut_nth k' ty))"
      by (simp add: \<open>match P tx ty \<and> P hx hy\<close> local.Cons match_true)
    hence "match P xs (cut_nth (Suc k') (hy#ty))"
      by auto
    then show ?thesis
      using \<open>ys = hy # ty\<close> by auto 
  qed
qed
qed

lemma match_inj_exist:
  assumes "match P l l'"
  shows "\<exists>f. inj_on f {0..<(size l)} \<and>  (\<forall>k < size l. f k < size l' \<and> (P (l!k) (l' ! (f k))))"
  using assms
proof (induction)
case match_Nil_Nil
  then show ?case
    by simp 
next
  case (match_true x y xs ys)
  then obtain f where f:"inj_on f {0..<(size xs)} \<and>  (\<forall>k < size xs. f k < size ys \<and> (P (xs!k) (ys ! (f k))))"
    by blast
  then obtain f' where f': "f' = (\<lambda>a. case a of 0 \<Rightarrow> 0 | Suc aa \<Rightarrow> Suc (f aa))"
    by simp
  have "\<And>k. k < size (x#xs) \<Longrightarrow>  
f' k < size (y#ys) \<and> (P ((x#xs)!k) ((y#ys) ! (f' k)))"
 proof -
   fix k assume k:"k < size (x#xs)"
   thus "f' k < size (y#ys) \<and> (P ((x#xs)!k) ((y#ys) ! (f' k)))"
   proof (cases k)
     case 0
     then show ?thesis
       by (simp add: f' match_true.hyps(1))
   next
     case (Suc kk)
     hence "kk < size xs"
       using k by simp
     hence "f kk < size ys \<and> (P (xs!kk) (ys ! (f kk)))"
       using f by auto
     then show ?thesis
       using f'
       by (simp add: Suc)
   qed
 qed
  have "(\<forall>y\<in> {0..<size xs}. \<forall>z\<in> {0..<size xs}. f y = f z \<longrightarrow> y = z)"
    using match_true
    by (meson f inj_on_def)
  have "\<And>y z. y\<in> {0..<size (x#xs)} \<and> z\<in> {0..<size (x#xs)} \<Longrightarrow> (f' y = f' z \<longrightarrow> y = z)"
  proof -
    fix y z 
    assume yz:"y\<in> {0..<size (x#xs)} \<and> z\<in> {0..<size (x#xs)}"
    thus "f' y = f' z \<longrightarrow> y = z"
    proof (cases "y =0 \<or> z = 0")
      case True
      have "y \<noteq> z \<longrightarrow> y  \<noteq> 0 \<or> z \<noteq> 0"
        using True by auto
      hence "y \<noteq> z \<longrightarrow> (\<exists>yy. y = Suc yy) \<or> (\<exists>zz. z = Suc zz)"
        using not0_implies_Suc by blast
      hence "y \<noteq> z \<longrightarrow> (\<exists>yy. y = Suc yy \<and> f' y = Suc (f yy)) \<or> (\<exists>zz. z = Suc zz \<and> f' z= Suc (f zz))"
        using f' by auto
      hence "y \<noteq> z \<longrightarrow> (\<exists>yy. y = Suc yy \<and> f' y = Suc (f yy) \<and> f' y \<noteq> f' z
) \<or> (\<exists>zz. z = Suc zz \<and> f' z= Suc (f zz) \<and> f' z \<noteq> f' y)"
        using True f' by auto
      hence "y \<noteq> z \<longrightarrow> f' y \<noteq> f' z"
        by auto
      then show ?thesis
        by auto 
next
  case False
  then obtain yy zz where Sucyz:"y = Suc yy" "z= Suc zz"
    using not0_implies_Suc by blast
  hence yyzzin:"yy \<in> {0..<size xs}" "zz \<in> {0..<size xs}"
    using yz by auto
  hence "f' y = Suc (f yy) \<and> f' z = Suc (f zz)"
    by (simp add: f' Sucyz)
  hence "y \<noteq> z \<longrightarrow> yy \<noteq> zz"
using yyzzin
  using Sucyz(1) Sucyz(2) by linarith

  then show ?thesis
    using \<open>\<forall>y\<in>{0..<length xs}. \<forall>z\<in>{0..<length xs}. f y = f z \<longrightarrow> y = z\<close> \<open>f' y = Suc (f yy) \<and> f' z = Suc (f zz)\<close> yyzzin(1) yyzzin(2) by auto
qed
qed

  hence "inj_on f' {0..<size (x#xs)}"
    by (meson inj_onI)

  then show ?case
    using \<open>\<And>k. k < length (x # xs) \<Longrightarrow> f' k < length (y # ys) \<and> P ((x # xs) ! k) ((y # ys) ! f' k)\<close> by blast 
next
  case (match_extra xs ys y)
  then obtain f where f:"inj_on f {0..<(size xs)} \<and>  (\<forall>k < size xs. f k < size ys \<and> (P (xs!k) (ys ! (f k))))"
    by blast
  then obtain f' where f': "f' = (\<lambda>a. Suc (f a))"
    by simp
  hence "(\<forall>k < size xs. f' k < size (y#ys) \<and> (P (xs!k) ((y#ys) ! (f' k))))"
    by (simp add: f)
  have "inj_on f' {0..<(size xs)}"
    using f f'
    by (simp add: inj_on_def)
  then show ?case
    using \<open>\<forall>k<length xs. f' k < length (y # ys) \<and> P (xs ! k) ((y # ys) ! f' k)\<close> by blast
qed

lemma sum_mset_index:
"(\<Sum>x \<in># mset Fs. f x) 
= (\<Sum>k \<in> {0..< size Fs}. f (Fs ! k))"
proof (induction Fs)
  case Nil
  then show ?case
    by simp 
next
  case (Cons F Fs)
  hence "(\<Sum>x \<in># mset Fs. f x) 
= (\<Sum>k \<in> {0..< size Fs}. f (Fs ! k))"
    by simp
  hence "(\<Sum>x \<in># mset Fs. f x) + f F = 
(\<Sum>k \<in> {0..< size Fs}. f (Fs ! k)) + f F"
    by simp
  hence "(\<Sum>x \<in># mset (F# Fs). f x) = 
(\<Sum>k \<in> {0..< size Fs}. f (Fs ! k)) + f F"
    by (simp add: Cons.IH add.commute)
  hence "(\<Sum>x \<in># mset (F# Fs). f x) = 
(\<Sum>k \<in> {0..< size Fs}. f ((F#Fs) ! (Suc k))) + f F"
     using \<open>sum_mset (image_mset f (mset (F # Fs))) = (\<Sum>k = 0..<length Fs. f (Fs ! k)) + f F\<close> by auto
   hence "(\<Sum>x \<in># mset (F# Fs). f x) = 
(\<Sum>k \<in> {1..< Suc (size Fs)}. f ((F#Fs) ! k)) + f F"
   by (metis (mono_tags, lifting) One_nat_def \<open>sum_mset (image_mset f (mset (F # Fs))) = (\<Sum>k = 0..<length Fs. f (Fs ! k)) + f F\<close> nth_Cons_Suc sum.cong sum_shift_bounds_Suc_ivl)
  hence"(\<Sum>x \<in># mset (F# Fs). f x) = 
(\<Sum>k \<in> {1..< (size (F#Fs))}. f ((F#Fs) ! k)) + 
(\<Sum>k \<in> {0}. f ((F#Fs) ! 0))"
     using \<open>sum_mset (image_mset f (mset (F # Fs))) = (\<Sum>k = 1..<Suc (length Fs). f ((F # Fs) ! k)) + f F\<close> by auto
   hence "(\<Sum>x \<in># mset (F# Fs). f x) = 
(\<Sum>k \<in> {1..< (size (F#Fs))} \<union> {0}. f ((F#Fs) ! k))"
     by (simp add: add.commute)

   then show ?case
     by (metis One_nat_def \<open>sum_mset (image_mset f (mset (F # Fs))) = (\<Sum>k = 1..<Suc (length Fs). f ((F # Fs) ! k)) + f F\<close> add.commute length_Cons nth_Cons_0 sum.atLeast_Suc_lessThan zero_less_Suc)

qed

lemma count_sum_gt_0: 
  assumes "count (f x) a > 0" "x \<in> A" "finite A"
  shows "count (\<Sum>y \<in> A. f y) a > 0"
proof -
  have "A = (A - {x}) \<union> {x}"
    using assms by auto
  hence  "count (\<Sum>y \<in> (A -{x}) \<union> {x}. f y) a =
count ((\<Sum>y \<in> (A -{x}). f y) + f x) a "
    by (metis add.commute assms(2) assms(3) sum.remove)
  hence "... = count (\<Sum>y \<in> (A -{x}). f y) a + count (f x) a"
    by simp
  thus ?thesis
    using \<open>A = A - {x} \<union> {x}\<close> \<open>count (sum f (A - {x} \<union> {x})) a = count (sum f (A - {x}) + f x) a\<close> assms(1) by auto
qed

lemma exist_prefix_sucs_count_eq:
  assumes "F \<le> F'" "count (mset_of_tree F) a = count (mset_of_tree F') a"
"k' < size (sucs F')" "count (mset_of_tree (sucs F'!k' )) a > 0" 
shows "\<exists>k. k < size (sucs F) \<and> ((sucs F) ! k) \<le> (sucs F') ! k' \<and> 
count (mset_of_tree (sucs F!k )) a  = count (mset_of_tree (sucs F'!k' )) a "
proof (rule ccontr)
  assume assm:"\<not> (\<exists>k. k < size (sucs F) \<and> ((sucs F) ! k) \<le> (sucs F') ! k' \<and> 
count (mset_of_tree (sucs F!k )) a  = count (mset_of_tree (sucs F'!k' )) a)"
  obtain l Fs Fs' where lFs:"F= Tree l Fs" "F' = Tree l Fs'"
    using assms
    using prefix.simps by auto
  hence "\<forall>k. k < size Fs \<and> Fs  ! k \<le> Fs' ! k'  \<longrightarrow>  
count (mset_of_tree (Fs!k )) a  < count (mset_of_tree ( Fs'!k' )) a"
    using assm
    using nat_less_le prefix_count_le by auto
  have "match prefix Fs Fs'" 
    using assms lFs
    by auto
  then obtain f where f: "inj_on f {0..<(size Fs)}" "(\<forall>k < size Fs. f k < size Fs' \<and> (prefix (Fs!k) (Fs' ! (f k))))"
    using match_inj_exist by blast
  then obtain imgf where imgf:"imgf = f ` {0..<size Fs}"
    by simp
  hence "imgf \<subseteq> {0..<size Fs'} \<and> card (imgf) = size Fs"
    using atLeastLessThan_iff f
    using atLeast0LessThan card_image lessThan_iff by fastforce
  hence bijf:"bij_betw f {0..<(size Fs)} imgf"
    by (simp add: bij_betw_def f(1) imgf)
  have "count (mset_of_tree (Tree l Fs)) a = count (mset_of_tree (Tree l Fs')) a"
    using assms lFs by auto
  hence "count ({#l#} + (\<Sum>x \<in># mset Fs. mset_of_tree x)) a 
= count ({#l#} + (\<Sum>x \<in># mset Fs'. mset_of_tree x)) a"
    by simp 
hence "count (\<Sum>x \<in># mset Fs. mset_of_tree x) a 
= count (\<Sum>x \<in># mset Fs'. mset_of_tree x) a"
  by (metis add_left_cancel count_union)
  hence "count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs ! x)) a =
 count (\<Sum>x \<in> {0..<size Fs'}. mset_of_tree (Fs' ! x)) a"
    by (simp add: sum_mset_index)
  have "\<forall>x \<in> {0..<size Fs}. count (mset_of_tree (Fs!x)) a \<le> count (mset_of_tree (Fs' ! (f x))) a"
    using f
    by (simp add: prefix_count_le)
  hence " (\<Sum>x \<in> {0..<size Fs}. count (mset_of_tree (Fs!x)) a) \<le> (\<Sum>x \<in> {0..<size Fs}. count (mset_of_tree (Fs'!(f x))) a)"
    by (meson sum_mono)
  hence "count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a \<le> count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs'!(f x))) a"
    by (simp add: count_sum)
  hence "count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a \<le> count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs'!(f x))) a"
    by (simp add: count_sum)
  hence tmp:"count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a \<le> count (\<Sum>x \<in> imgf. mset_of_tree (Fs'!x)) a"
    using bijf
    by (metis (no_types, lifting) f(1) imgf sum.reindex_cong)
  thus "False"
  proof (cases "k' \<in> imgf")
    case True
    then obtain k where "k \<in> {0..<size Fs}" "f k = k'"
      using imgf by blast    
    have "count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs ! x)) a =
        count (\<Sum>x \<in> ({0..<size Fs'} - imgf) \<union> imgf. mset_of_tree (Fs' ! x)) a"
      by (metis Diff_partition Un_commute \<open>count (\<Sum>x = 0..<length Fs. mset_of_tree (Fs ! x)) a = count (\<Sum>x = 0..<length Fs'. mset_of_tree (Fs' ! x)) a\<close> \<open>imgf \<subseteq> {0..<length Fs'} \<and> card imgf = length Fs\<close>)
    hence "count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs ! x)) a =
        count (\<Sum>x \<in> ({0..<size Fs'} - imgf). mset_of_tree (Fs' ! x)) a
        + count (\<Sum>x \<in> imgf. mset_of_tree (Fs' ! x)) a"
      by (metis \<open>count (\<Sum>x = 0..<length Fs. mset_of_tree (Fs ! x)) a = count (\<Sum>x = 0..<length Fs'. mset_of_tree (Fs' ! x)) a\<close> \<open>imgf \<subseteq> {0..<length Fs'} \<and> card imgf = length Fs\<close> finite_atLeastLessThan imgf plus_multiset.rep_eq sum.subset_diff)
    hence "count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs ! x)) a = count (\<Sum>x \<in> imgf. mset_of_tree (Fs' ! x)) a"
      using tmp by linarith
  have "\<forall>x \<in> {0..<k} \<union>{k<..<size Fs}. count (mset_of_tree (Fs!x)) a \<le> count (mset_of_tree (Fs' ! (f x))) a"
    using \<open>k \<in> {0..<length Fs}\<close>
    by (metis Un_iff atLeastLessThan_iff f(2) greaterThanLessThan_iff leD leI le_less_trans less_eq_rtree.transfer less_imp_le prefix_count_le)
  
 hence "(\<Sum>x \<in>  {0..<k} \<union>{k<..<size Fs}. count (mset_of_tree (Fs!x)) a) \<le> (\<Sum>x \<in>  {0..<k} \<union>{k<..<size Fs}. count (mset_of_tree (Fs'!(f x))) a)"
   by (meson sum_mono)
 hence "(\<Sum>x \<in>  {0..<k} \<union>{k<..<size Fs}. count (mset_of_tree (Fs!x)) a) + (\<Sum>x \<in> {k}. count (mset_of_tree (Fs!x)) a)  
< (\<Sum>x \<in> {0..<k} \<union>{k<..<size Fs}. count (mset_of_tree (Fs'!(f x))) a) + (\<Sum>x \<in> {k}. count (mset_of_tree (Fs'!(f k))) a)"
   by (metis (no_types, lifting) \<open>\<forall>k. k < length Fs \<and> Fs ! k \<le> Fs' ! k' \<longrightarrow> count (mset_of_tree (Fs ! k)) a < count (mset_of_tree (Fs' ! k')) a\<close> \<open>f k = k'\<close> \<open>k \<in> {0..<length Fs}\<close> add_cancel_right_right add_le_less_mono atLeastLessThan_iff empty_iff f(2) finite.emptyI less_eq_rtree.transfer sum.empty sum.insert)
 hence "(\<Sum>x \<in>  {0..<k} \<union>{k<..<size Fs} \<union>{k}. count (mset_of_tree (Fs!x)) a) 
< (\<Sum>x \<in> {0..<k} \<union>{k<..<size Fs} \<union>{k}. count (mset_of_tree (Fs'!(f x))) a)"
    by simp
 hence "(\<Sum>x \<in>  {0..<k} \<union>{k} \<union>{k<..<size Fs}. count (mset_of_tree (Fs!x)) a) < (\<Sum>x \<in>  {0..<k} \<union>{k} \<union>{k<..<size Fs}. count (mset_of_tree (Fs'!(f x))) a)"
   by simp
  hence "(\<Sum>x \<in>  {0..<size Fs}. count (mset_of_tree (Fs!x)) a) < (\<Sum>x \<in>  {0..<size Fs}. count (mset_of_tree (Fs'!(f x))) a)"
      by (smt Un_assoc \<open>k \<in> {0..<length Fs}\<close> atLeast0LessThan ivl_disj_un_singleton(3) ivl_disj_un_two(3) lessThan_iff less_imp_le zero_le)
 hence "count (\<Sum>x \<in>  {0..<size Fs}. mset_of_tree (Fs!x)) a < count (\<Sum>x \<in>  {0..<size Fs}. mset_of_tree (Fs'!(f x))) a"
   by (simp add: count_sum)
  
    hence "count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a < count (\<Sum>x \<in> imgf. mset_of_tree (Fs'!x)) a"
    using bijf f(1) imgf sum.reindex_cong
proof -
have f1: "\<forall>f N Na fa fb. (\<not> inj_on f N \<or> Na \<noteq> f ` N \<or> (\<exists>n. (n::nat) \<in> N \<and> fa (f n::nat) \<noteq> (fb n::nat))) \<or> sum fa Na = sum fb N"
by (meson sum.reindex_cong)
  obtain nn :: "(nat \<Rightarrow> nat) \<Rightarrow> (nat \<Rightarrow> nat) \<Rightarrow> nat set \<Rightarrow> (nat \<Rightarrow> nat) \<Rightarrow> nat" where
"\<forall>x0 x1 x3 x4. (\<exists>v5. v5 \<in> x3 \<and> x1 (x4 v5) \<noteq> x0 v5) = (nn x0 x1 x3 x4 \<in> x3 \<and> x1 (x4 (nn x0 x1 x3 x4)) \<noteq> x0 (nn x0 x1 x3 x4))"
    by moura
  then have f2: "\<forall>f N Na fa fb. \<not> inj_on f N \<or> Na \<noteq> f ` N \<or> nn fb fa N f \<in> N \<and> fa (f (nn fb fa N f)) \<noteq> fb (nn fb fa N f) \<or> sum fa Na = sum fb N"
    using f1 by presburger
  have "(\<not> inj_on f {0..<length Fs} \<or> nn (\<lambda>uub. count (mset_of_tree (Fs' ! f uub)) a) (\<lambda>uub. count (mset_of_tree (Fs' ! uub)) a) {0..<length Fs} f \<in> {0..<length Fs} \<and> count (mset_of_tree (Fs' ! f (nn (\<lambda>uub. count (mset_of_tree (Fs' ! f uub)) a) (\<lambda>uub. count (mset_of_tree (Fs' ! uub)) a) {0..<length Fs} f))) a \<noteq> count (mset_of_tree (Fs' ! f (nn (\<lambda>uub. count (mset_of_tree (Fs' ! f uub)) a) (\<lambda>uub. count (mset_of_tree (Fs' ! uub)) a) {0..<length Fs} f))) a \<or> (\<Sum>uub\<in>f ` {0..<length Fs}. count (mset_of_tree (Fs' ! uub)) a) = (\<Sum>uub = 0..<length Fs. count (mset_of_tree (Fs' ! f uub)) a)) = (\<not> inj_on f {0..<length Fs} \<or> nn (\<lambda>uub. count (mset_of_tree (Fs' ! f uub)) a) (\<lambda>uub. count (mset_of_tree (Fs' ! uub)) a) {0..<length Fs} f \<in> {0..<length Fs} \<and> count (mset_of_tree (Fs' ! f (nn (\<lambda>uub. count (mset_of_tree (Fs' ! f uub)) a) (\<lambda>uub. count (mset_of_tree (Fs' ! uub)) a) {0..<length Fs} f))) a \<noteq> count (mset_of_tree (Fs' ! f (nn (\<lambda>uub. count (mset_of_tree (Fs' ! f uub)) a) (\<lambda>uub. count (mset_of_tree (Fs' ! uub)) a) {0..<length Fs} f))) a \<or> (\<Sum>uub\<in>f ` {0..<length Fs}. count (mset_of_tree (Fs' ! uub)) a) = (\<Sum>uub = 0..<length Fs. count (mset_of_tree (Fs' ! f uub)) a))"
    by fastforce
  then have "nn (\<lambda>n. count (mset_of_tree (Fs' ! f n)) a) (\<lambda>n. count (mset_of_tree (Fs' ! n)) a) {0..<length Fs} f \<in> {0..<length Fs} \<and> count (mset_of_tree (Fs' ! f (nn (\<lambda>n. count (mset_of_tree (Fs' ! f n)) a) (\<lambda>n. count (mset_of_tree (Fs' ! n)) a) {0..<length Fs} f))) a \<noteq> count (mset_of_tree (Fs' ! f (nn (\<lambda>n. count (mset_of_tree (Fs' ! f n)) a) (\<lambda>n. count (mset_of_tree (Fs' ! n)) a) {0..<length Fs} f))) a \<or> (\<Sum>n\<in>f ` {0..<length Fs}. count (mset_of_tree (Fs' ! n)) a) = (\<Sum>n = 0..<length Fs. count (mset_of_tree (Fs' ! f n)) a)"
    using f2 \<open>inj_on f {0..<length Fs}\<close> by force
  then show ?thesis
    by (metis \<open>count (\<Sum>x = 0..<length Fs. mset_of_tree (Fs ! x)) a < count (\<Sum>x = 0..<length Fs. mset_of_tree (Fs' ! f x)) a\<close> count_sum imgf)
qed
  then show "False"
    by (simp add: \<open>count (\<Sum>x = 0..<length Fs. mset_of_tree (Fs ! x)) a < count (\<Sum>x\<in>imgf. mset_of_tree (Fs' ! x)) a\<close> \<open>count (\<Sum>x = 0..<length Fs. mset_of_tree (Fs ! x)) a = count (\<Sum>x\<in>imgf. mset_of_tree (Fs' ! x)) a\<close>)

  next
    case False
    hence k'notin:"k' \<in> ({0..<size Fs'} - imgf)"
      using assms(3) atLeast0LessThan lFs(2) by auto
    hence "count (mset_of_tree (Fs'!k' )) a > 0"
      using assms lFs
      by simp
    hence "(\<Sum>x \<in> ({0..<size Fs'} - imgf) . count (mset_of_tree (Fs' ! x)) a) > 0"
      using k'notin count_sum_gt_0
      by (metis finite_Diff finite_atLeastLessThan gr_zeroI sum_eq_0_iff)
    have "count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs ! x)) a =
 count (\<Sum>x \<in> ({0..<size Fs'} - imgf) \<union> imgf. mset_of_tree (Fs' ! x)) a"
      by (metis Diff_partition Un_commute \<open>count (\<Sum>x = 0..<length Fs. mset_of_tree (Fs ! x)) a = count (\<Sum>x = 0..<length Fs'. mset_of_tree (Fs' ! x)) a\<close> \<open>imgf \<subseteq> {0..<length Fs'} \<and> card imgf = length Fs\<close> imgf)
    hence "count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs ! x)) a =
 count ((\<Sum>x \<in> ({0..<size Fs'} - imgf) . mset_of_tree (Fs' ! x)) + (\<Sum>x \<in> imgf. mset_of_tree (Fs' ! x))) a"
      by (metis Diff_partition Un_commute \<open>imgf \<subseteq> {0..<length Fs'} \<and> card imgf = length Fs\<close> atLeast0LessThan finite_lessThan sum.subset_diff)
  hence "count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs ! x)) a =
 count (\<Sum>x \<in> ({0..<size Fs'} - imgf) . mset_of_tree (Fs' ! x)) a  + count (\<Sum>x \<in> imgf. mset_of_tree (Fs' ! x)) a"
    by simp
hence "count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs ! x)) a =
 count (\<Sum>x \<in> ({0..<size Fs'} - imgf) . mset_of_tree (Fs' ! x)) a  + count (\<Sum>x \<in> imgf. mset_of_tree (Fs' ! x)) a"
    by simp
  hence "count (\<Sum>x \<in> ({0..<size Fs'} - imgf) . mset_of_tree (Fs' ! x)) a = 0"
    using tmp
    by linarith
  thus "False" using assms k'notin
    by (metis \<open>0 < (\<Sum>x\<in>{0..<length Fs'} - imgf. count (mset_of_tree (Fs' ! x)) a)\<close> count_sum not_less_zero)
qed
qed

lemma exist_tine_prefix_eq_size_prefix:
  assumes  "tinesP F' t'" "F \<le> F'" 
"count (mset_of_tree F) a = count (mset_of_tree F') a"
 "last (trace_tine F' t') = a"
shows "\<exists>t \<in> tines F. tine_prefix F F' t t' \<and> size t = size t'"
  using assms
proof (induction F' arbitrary: F t')
  case (Tree l Fs')
  hence lFs':"F \<le> Tree l Fs'"
    using assms
    by blast
  then obtain Fs where lFs:"F = Tree l Fs" "match prefix Fs Fs'"
    by (metis less_eq_rtree.transfer prefix.simps rtree.inject)
  then show ?case 
  proof (cases t')
      case Nil
  then show ?thesis
    using Tree.prems(2) tinesP.Nil tines_def by auto 
  next
    case (Cons k' t')
    note Cons1 = this
    thm Cons1
    hence klt:"k' < size Fs'"
      using Tree.prems(1) tinesP_Cons_iff by auto
    hence "trace_tine (Tree l Fs') (k'#t') = trace_tine (Tree l Fs') ([k']@t')"
      by simp
    hence "trace_tine (Tree l Fs') (k'#t') =
 (trace_tine (Tree l Fs') [k']) @
tl (trace_tine (get_subtree (Tree l Fs') [k']) t')"
      using trace_tine_append_trace_tine_subtree
      by (metis Tree.prems(1) append.left_neutral append_Cons append_eq_conv_conj local.Cons)
    then show ?thesis 
    proof (cases t')
      case Nil
      hence "trace_tine (Tree l Fs') (k'#[]) =
 (trace_tine (Tree l Fs') [k']) @
tl (trace_tine (get_subtree (Tree l Fs') [k']) [])"
        using \<open>trace_tine (Tree l Fs') (k' # t') = trace_tine (Tree l Fs') [k'] @ tl (trace_tine (get_subtree (Tree l Fs') [k']) t')\<close> by auto
      hence "last (trace_tine (Tree l Fs') (k'#[])) = 
last ((trace_tine (Tree l Fs') [k']) @
tl (trace_tine (get_subtree (Tree l Fs') [k']) []))"
        by simp
hence "last (trace_tine (Tree l Fs') (k'#[])) = 
last (trace_tine (Tree l Fs') [k']) "
        by simp
     hence "last (trace_tine (Tree l Fs') (k'#[])) = 
label (get_subtree (Tree l Fs') [k']) "
       using last_in_tine_is_label_in_subtree by blast
     hence "last (trace_tine (Fs'!k') []) = a"
       using \<open>k' < length Fs'\<close> last_in_tine_is_label_in_subtree local.Cons local.Nil local.Tree(5) by auto
     hence "count (mset_of_tree (Fs'!k' )) a > 0"
       using last_in_set tinesP.Nil tines_exist_last_label_exist trace_tine_non_empty by blast
     then obtain k where "k < size Fs \<and> (Fs ! k) \<le> Fs' ! k' \<and> 
count (mset_of_tree (Fs!k)) a  = count (mset_of_tree (Fs'!k' )) a "
       using exist_prefix_sucs_count_eq assms lFs lFs' klt
     proof -
  assume a1: "\<And>k. k < length Fs \<and> Fs ! k \<le> Fs' ! k' \<and> count (mset_of_tree (Fs ! k)) a = count (mset_of_tree (Fs' ! k')) a \<Longrightarrow> thesis"
  obtain nn :: "nat \<Rightarrow> nat \<Rightarrow> rtree \<Rightarrow> rtree \<Rightarrow> nat" where
    f2: "\<forall>x0 x1 x2 x3. (\<exists>v4<length (sucs x3). sucs x3 ! v4 \<le> sucs x2 ! x0 \<and> count (mset_of_tree (sucs x3 ! v4)) x1 = count (mset_of_tree (sucs x2 ! x0)) x1) = (nn x0 x1 x2 x3 < length (sucs x3) \<and> sucs x3 ! nn x0 x1 x2 x3 \<le> sucs x2 ! x0 \<and> count (mset_of_tree (sucs x3 ! nn x0 x1 x2 x3)) x1 = count (mset_of_tree (sucs x2 ! x0)) x1)"
    by moura
  have f3: "0 < count (mset_of_tree (sucs (Tree l Fs') ! k')) a"
    using \<open>0 < count (mset_of_tree (Fs' ! k')) a\<close> by auto
       have "k' < length (sucs (Tree l Fs'))"
         using klt rtree.sel(2) by presburger
       then have "nn k' a (Tree l Fs') (Tree l Fs) < length (sucs (Tree l Fs)) \<and> sucs (Tree l Fs) ! nn k' a (Tree l Fs') (Tree l Fs) \<le> sucs (Tree l Fs') ! k' \<and> count (mset_of_tree (sucs (Tree l Fs) ! nn k' a (Tree l Fs') (Tree l Fs))) a = count (mset_of_tree (sucs (Tree l Fs') ! k')) a"
         using f3 f2 \<open>F = Tree l Fs\<close> \<open>\<And>k' a F' F. \<lbrakk>F \<le> F'; count (mset_of_tree F) a = count (mset_of_tree F') a; k' < length (sucs F'); 0 < count (mset_of_tree (sucs F' ! k')) a\<rbrakk> \<Longrightarrow> \<exists>k<length (sucs F). sucs F ! k \<le> sucs F' ! k' \<and> count (mset_of_tree (sucs F ! k)) a = count (mset_of_tree (sucs F' ! k')) a\<close> lFs' local.Tree(4) by presburger
       then show ?thesis
using a1 by simp
qed
  hence "tine_prefix (Fs!k) (Fs'!k') [] t'"
    by (simp add: local.Nil tine_prefix.Nil tinesP.Nil)
  hence "tine_prefix F (Tree l Fs') [k] (k'#t')"
    using Subtree \<open>k < length Fs \<and> Fs ! k \<le> Fs' ! k' \<and> count (mset_of_tree (Fs ! k)) a = count (mset_of_tree (Fs' ! k')) a\<close> klt lFs' lFs(1) by auto
  thus ?thesis
    by (metis lFs(1) list.size(4) local.Cons local.Nil mem_Collect_eq tine_prefix_imp_both_tines tines_def)
next

  case (Cons kt' tt')
 hence "trace_tine (Tree l Fs') (k'#(kt'#tt')) =
 (trace_tine (Tree l Fs') [k']) @
tl (trace_tine (get_subtree (Tree l Fs') [k']) (kt'#tt'))"
   using \<open>trace_tine (Tree l Fs') (k' # t') = trace_tine (Tree l Fs') [k'] @ tl (trace_tine (get_subtree (Tree l Fs') [k']) t')\<close> by auto
  hence "last (trace_tine (Tree l Fs') (k'#(kt'#tt'))) = last ((trace_tine (Tree l Fs') [k']) @
tl (trace_tine (get_subtree (Tree l Fs') [k']) (kt'#tt')))"
    by auto
hence "last (trace_tine (Tree l Fs') (k'#(kt'#tt'))) = last (tl (trace_tine (get_subtree (Tree l Fs') [k']) (kt'#tt')))"
proof - 
  have "k'#t' \<in> tines (Tree l Fs')"
    using assms Tree.IH
    using Cons1 Tree.prems(1) tines_def by auto
  hence "kt'#tt' \<in> tines (Fs' ! k')"
    by (simp add: local.Cons tinesP_Cons_iff tines_def)
  hence "size (trace_tine (get_subtree (Tree l Fs') [k']) (kt'#tt')) = Suc (size (kt'#tt'))"
    by (metis Cons1 Cons_eq_append_conv Tree.prems(1) local.Cons mod_get_subtree_tines_append_in_tines suc_tine_size_eq_label_list)
  thus ?thesis
    by (metis Nitpick.size_list_simp(2) \<open>last (trace_tine (Tree l Fs') (k' # kt' # tt')) = last (trace_tine (Tree l Fs') [k'] @ tl (trace_tine (get_subtree (Tree l Fs') [k']) (kt' # tt')))\<close> last_appendR length_0_conv list.distinct(1) old.nat.inject trace_tine_non_empty) 
qed
  hence "... = last (trace_tine (Fs' !k') (kt'#tt'))"
    by (smt get_subtree.simps klt last_in_tine_is_label_in_subtree list.simps(5))
  hence "last (trace_tine (Fs' !k') (kt'#tt')) = a"
    using Cons1 Tree.prems(4) \<open>last (trace_tine (Tree l Fs') (k' # kt' # tt')) = last (tl (trace_tine (get_subtree (Tree l Fs') [k']) (kt' # tt')))\<close> local.Cons by auto
 hence  "count (mset_of_tree (Fs'!k' )) a > 0"
   by (metis Cons1 Tree.prems(1) last_in_set local.Cons rtree.sel(2) tinesP_Cons_iff tines_exist_last_label_exist trace_tine_non_empty)
  then obtain k where "k < size Fs \<and> (Fs ! k) \<le> Fs' ! k' \<and> 
count (mset_of_tree (Fs !k )) a  = count (mset_of_tree (Fs'!k' )) a "
    using exist_prefix_sucs_count_eq klt lFs lFs' assms
  proof -
  assume a1: "\<And>k. k < length Fs \<and> Fs ! k \<le> Fs' ! k' \<and> count (mset_of_tree (Fs ! k)) a = count (mset_of_tree (Fs' ! k')) a \<Longrightarrow> thesis"
  obtain nn :: "nat \<Rightarrow> nat \<Rightarrow> rtree \<Rightarrow> rtree \<Rightarrow> nat" where
    f2: "\<forall>x0 x1 x2 x3. (\<exists>v4<length (sucs x3). sucs x3 ! v4 \<le> sucs x2 ! x0 \<and> count (mset_of_tree (sucs x3 ! v4)) x1 = count (mset_of_tree (sucs x2 ! x0)) x1) = (nn x0 x1 x2 x3 < length (sucs x3) \<and> sucs x3 ! nn x0 x1 x2 x3 \<le> sucs x2 ! x0 \<and> count (mset_of_tree (sucs x3 ! nn x0 x1 x2 x3)) x1 = count (mset_of_tree (sucs x2 ! x0)) x1)"
    by moura
  have f3: "0 < count (mset_of_tree (sucs (Tree l Fs') ! k')) a"
    using \<open>0 < count (mset_of_tree (Fs' ! k')) a\<close> rtree.sel(2) by presburger
  have "k' < length (sucs (Tree l Fs'))"
    by (metis klt rtree.sel(2))
  then have "nn k' a (Tree l Fs') (Tree l Fs) < length (sucs (Tree l Fs)) \<and> sucs (Tree l Fs) ! nn k' a (Tree l Fs') (Tree l Fs) \<le> sucs (Tree l Fs') ! k' \<and> count (mset_of_tree (sucs (Tree l Fs) ! nn k' a (Tree l Fs') (Tree l Fs))) a = count (mset_of_tree (sucs (Tree l Fs') ! k')) a"
    using f3 f2 \<open>F = Tree l Fs\<close> \<open>\<And>k' a F' F. \<lbrakk>F \<le> F'; count (mset_of_tree F) a = count (mset_of_tree F') a; k' < length (sucs F'); 0 < count (mset_of_tree (sucs F' ! k')) a\<rbrakk> \<Longrightarrow> \<exists>k<length (sucs F). sucs F ! k \<le> sucs F' ! k' \<and> count (mset_of_tree (sucs F ! k)) a = count (mset_of_tree (sucs F' ! k')) a\<close> lFs' local.Tree(4) by presburger
  then show ?thesis
    using a1 by simp
qed
  then obtain t where "tine_prefix (Fs!k) (Fs'! k') t (kt'#tt')" "size t = size (kt'#tt')"
    by (metis Cons1 Tree.IH Tree.prems(1) \<open>last (trace_tine (Fs' ! k') (kt' # tt')) = a\<close> klt local.Cons nth_mem rtree.sel(2) tinesP_Cons_iff)
  hence "tine_prefix F (Tree l Fs') (k#t) (k'#(kt'#tt'))"
    using Subtree \<open>k < length Fs \<and> Fs ! k \<le> Fs' ! k' \<and> count (mset_of_tree (Fs ! k)) a = count (mset_of_tree (Fs' ! k')) a\<close> klt lFs' lFs(1) by auto
  then show ?thesis
    by (metis Cons1 \<open>length t = length (kt' # tt')\<close> list.size(4) local.Cons mem_Collect_eq tine_prefix_imp_both_tines tines_def)
qed
  qed
qed

lemma mset_cut_i_nth_plus_i:
  assumes "i < size l"
  shows "mset (cut_nth i l) + {#l ! i#} = mset l"
proof - 
  have "(cut_nth i l) = take i l @ drop (Suc i) l"
    by (simp add: assms cut_nth_take_drop_Suc)
  hence "mset (cut_nth i l) + {#l ! i#}  =
mset (take i l @ drop (Suc i) l) + {#l!i#}"
    by simp
  hence "... = mset (take i l) + mset (drop (Suc i) l) + {#l!i#}"
    by auto
  hence "... = mset ((take i l) @ [l!i] @ (drop (Suc i) l))"
    using mset.simps(2) by auto
  thus ?thesis
    by (simp add: Cons_nth_drop_Suc \<open>cut_nth i l = take i l @ drop (Suc i) l\<close> assms)
qed

lemma exist_limit_indices_eq_prefix:
  assumes "strictly_inc F'" "F \<le> F'" "\<forall>a. a < n \<longrightarrow> count (mset_of_tree F) a = count (mset_of_tree F') a"
"k' < size (sucs F')" "\<exists>a. a < n \<and> count (mset_of_tree ((sucs F') ! k')) a > 0"
shows "\<exists>f. f \<in> set (sucs F) \<and> f \<le> ((sucs F') ! k') \<and> 
             (\<forall>a. a < n \<longrightarrow>  count (mset_of_tree f) a = count (mset_of_tree ((sucs F') ! k') ) a)"
proof -
  obtain l Fs Fs' where "F = Tree l Fs" "F' = Tree l Fs'"
    by (metis assms(2) less_eq_rtree.transfer prefix.simps) 
  hence "match prefix Fs Fs'"
    using assms(2) by auto
  then obtain pref where pref:"inj_on pref {0..<size Fs} \<and> 
(\<forall>k < size Fs. pref k < size Fs' \<and> ((Fs!k) \<le> (Fs' ! (pref k))))"
    by (metis less_eq_rtree.transfer match_inj_exist)
  define imgp where "imgp = pref ` {0..<size Fs}"
  hence "imgp \<subseteq> {0..<size Fs'}"
    using atLeast0LessThan pref by blast
have "(\<forall>a < n. count (\<Sum>x \<in># mset Fs. mset_of_tree x) a
      = count (\<Sum>x \<in># mset Fs'. mset_of_tree x) a)"
     by (metis \<open>F = Tree l Fs\<close> \<open>F' = Tree l Fs'\<close> add_left_cancel assms(3) count_union mset_of_tree.simps)
hence"(\<forall>a< n. count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
      = count (\<Sum>x \<in> imgp. mset_of_tree (Fs'!x)) a + count (\<Sum>x \<in> {0..<size Fs'} - imgp. mset_of_tree (Fs'!x)) a)"
proof -
  { fix nn :: nat
    have "count ((\<Sum>n\<in>imgp. mset_of_tree (Fs' ! n)) + (\<Sum>n\<in>{..<length Fs'} - imgp. mset_of_tree (Fs' ! n))) nn = count (\<Union>#image_mset mset_of_tree (mset Fs')) nn"
      by (metis \<open>imgp \<subseteq> {0..<length Fs'}\<close> add.commute atLeast0LessThan finite_lessThan sum.subset_diff sum_mset_index)
    then have "\<not> nn < n \<or> count (\<Sum>n\<in>imgp. mset_of_tree (Fs' ! n)) nn + count (\<Sum>n\<in>{0..<length Fs'} - imgp. mset_of_tree (Fs' ! n)) nn = count (\<Sum>n = 0..<length Fs. mset_of_tree (Fs ! n)) nn"
      by (metis (no_types) \<open>\<forall>a<n. count (\<Union>#image_mset mset_of_tree (mset Fs)) a = count (\<Union>#image_mset mset_of_tree (mset Fs')) a\<close> atLeast0LessThan count_union sum_mset_index) }
  then show ?thesis
    by presburger
qed
  have Fssumindex:"(\<Sum>x \<in># mset Fs. mset_of_tree x) = (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x))"
      by (simp add: sum_mset_index)
    hence "(\<forall>a. count (\<Sum>x \<in># mset Fs. mset_of_tree x) a = count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a)"
      by (simp add: sum_mset_index)
    have "(\<forall>a. count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
\<le> count  (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs'!(pref x))) a)"
      by (smt atLeast0LessThan count_sum diff_zero lessThan_iff pref prefix_count_le sum_mono)
    hence"(\<forall>a. count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
      \<le> count  (\<Sum>x \<in> imgp. mset_of_tree (Fs'!x)) a)"
      by (metis (no_types, lifting) imgp_def pref sum.reindex_cong)
 hence"(\<forall>a. count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
      \<le> count (\<Sum>x \<in> imgp. mset_of_tree (Fs'!x)) a + count (\<Sum>x \<in> {0..<size Fs'} - imgp. mset_of_tree (Fs'!x)) a)"
   using trans_le_add1 by blast
  hence casef:"(\<forall>a < n. count (\<Sum>x \<in> {0..<size Fs'} - imgp. mset_of_tree (Fs'!x)) a = 0)"
    by (metis \<open>\<forall>a. count (\<Sum>x = 0..<length Fs. mset_of_tree (Fs ! x)) a \<le> count (\<Sum>x\<in>imgp. mset_of_tree (Fs' ! x)) a\<close> \<open>\<forall>a<n. count (\<Sum>x = 0..<length Fs. mset_of_tree (Fs ! x)) a = count (\<Sum>x\<in>imgp. mset_of_tree (Fs' ! x)) a + count (\<Sum>x\<in>{0..<length Fs'} - imgp. mset_of_tree (Fs' ! x)) a\<close> add_le_same_cancel1 add_less_same_cancel1 le_neq_implies_less not_add_less1)
  hence caset:"(\<forall>a < n. count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
      = count  (\<Sum>x \<in> imgp. mset_of_tree (Fs'!x)) a)"
    by (simp add: \<open>\<forall>a<n. count (\<Sum>x = 0..<length Fs. mset_of_tree (Fs ! x)) a = count (\<Sum>x\<in>imgp. mset_of_tree (Fs' ! x)) a + count (\<Sum>x\<in>{0..<length Fs'} - imgp. mset_of_tree (Fs' ! x)) a\<close> add_cancel_right_right)
    show ?thesis 
    proof (cases "k' \<in> imgp")
      case True
      then obtain k where "k < size Fs" "pref k = k'"
        using imgp_def by auto
      hence minusk:"pref ` ({0..<size Fs} - {k}) = imgp - {k'}"
        by (smt Diff_insert_absorb atLeast0LessThan image_insert imgp_def inj_on_insert insert_Diff insert_Diff_single lessThan_iff pref)
    have "(\<forall>a <n. count (\<Sum>x \<in> {0..<size Fs} - {k}. mset_of_tree (Fs!x)) a
\<le> count  (\<Sum>x \<in> {0..<size Fs} - {k}. mset_of_tree (Fs'!(pref x))) a)"
      using atLeast0LessThan count_sum diff_zero lessThan_iff pref prefix_count_le sum_mono
      by (smt Diff_iff)
    hence "(\<forall>a <n. count (\<Sum>x \<in> {0..<size Fs} - {k}. mset_of_tree (Fs!x)) a
\<le> count  (\<Sum>x \<in> pref ` ({0..<size Fs} - {k}). mset_of_tree (Fs'!x)) a)"
      by (smt \<open>k < length Fs\<close> atLeast0LessThan inj_on_insert insert_Diff lessThan_iff pref sum.reindex_cong)
hence "(\<forall>a <n. count (\<Sum>x \<in> {0..<size Fs} - {k}. mset_of_tree (Fs!x)) a
\<le> count  (\<Sum>x \<in> imgp - {k'}. mset_of_tree (Fs'!x)) a)"
  using minusk by simp
hence "(\<forall>a <n. count (\<Sum>x \<in> {0..<size Fs} - {k}. mset_of_tree (Fs!x)) a
\<le> count  (\<Sum>x \<in> imgp - {k'}. mset_of_tree (Fs'!x)) a)"
  using minusk by simp

  hence "(\<forall>a < n. count (\<Sum>x \<in># mset Fs. mset_of_tree x) a - count (\<Sum>x \<in> {0..<size Fs} - {k}. mset_of_tree (Fs!x)) a
      \<ge> count (\<Sum>x \<in># mset Fs'. mset_of_tree x) a - count  (\<Sum>x \<in> imgp - {k'}. mset_of_tree (Fs'!x)) a)"
    by (simp add: \<open>\<forall>a<n. count (\<Union>#image_mset mset_of_tree (mset Fs)) a = count (\<Union>#image_mset mset_of_tree (mset Fs')) a\<close> diff_le_mono2)
 hence "(\<forall>a < n. count (\<Sum>x \<in>  {0..<size Fs}. mset_of_tree (Fs!x)) a - count (\<Sum>x \<in> {0..<size Fs} - {k}. mset_of_tree (Fs!x)) a
      \<ge> count (\<Sum>x \<in> imgp. mset_of_tree (Fs'!x)) a - count  (\<Sum>x \<in> imgp - {k'}. mset_of_tree (Fs'!x)) a)"
   by (simp add: \<open>\<forall>a<n. count (\<Sum>x\<in>{0..<length Fs} - {k}. mset_of_tree (Fs ! x)) a \<le> count (\<Sum>x\<in>imgp - {k'}. mset_of_tree (Fs' ! x)) a\<close> caset diff_le_mono2)
  hence "(\<forall>a < n.  
count (\<Sum>x \<in>  ({0..<size Fs} - {k}) \<union> {k}. mset_of_tree (Fs!x)) a 
- count (\<Sum>x \<in> {0..<size Fs} - {k}. mset_of_tree (Fs!x)) a
      \<ge> 
count (\<Sum>x \<in> (imgp - {k'}) \<union> {k'}. mset_of_tree (Fs'!x)) a 
- count  (\<Sum>x \<in> imgp - {k'}. mset_of_tree (Fs'!x)) a)"
    by (metis (no_types, lifting) True Un_commute \<open>k < length Fs\<close> atLeast0LessThan caset insert_Diff insert_is_Un lessThan_iff)
     hence "(\<forall>a < n.  
count ((\<Sum>x \<in>  {0..<size Fs} - {k}. mset_of_tree (Fs!x)) +
 (\<Sum>x \<in>  {k}. mset_of_tree (Fs!x)))  a
- count (\<Sum>x \<in> {0..<size Fs} - {k}. mset_of_tree (Fs!x)) a
      \<ge> 
count ((\<Sum>x \<in> imgp - {k'}. mset_of_tree (Fs'!x)) 
+ (\<Sum>x \<in>  {k'}. mset_of_tree (Fs'!x))) a
- count  (\<Sum>x \<in> imgp - {k'}. mset_of_tree (Fs'!x)) a)"
       using comm_monoid_add_class.sum.union_disjoint
       by (smt Un_Diff_cancel2 Un_commute \<open>imgp \<subseteq> {0..<length Fs'}\<close> add.commute add_cancel_right_right finite.emptyI finite_atLeastLessThan insert_absorb insert_is_Un insert_not_empty subset_eq_atLeast0_lessThan_finite sum.empty sum.insert sum.insert_remove)
 hence "(\<forall>a < n.  
count (\<Sum>x \<in>  {0..<size Fs} - {k}. mset_of_tree (Fs!x)) a +
count (\<Sum>x \<in>  {k}. mset_of_tree (Fs!x))  a
- count (\<Sum>x \<in> {0..<size Fs} - {k}. mset_of_tree (Fs!x)) a
      \<ge> 
count (\<Sum>x \<in> imgp - {k'}. mset_of_tree (Fs'!x)) a + 
count (\<Sum>x \<in>  {k'}. mset_of_tree (Fs'!x)) a
- count  (\<Sum>x \<in> imgp - {k'}. mset_of_tree (Fs'!x)) a)"
   by simp
 hence "(\<forall>a < n.  
count (\<Sum>x \<in>  {k}. mset_of_tree (Fs!x))  a
      \<ge> 
count (\<Sum>x \<in>  {k'}. mset_of_tree (Fs'!x)) a)"
   by simp
hence "(\<forall>a < n.  
count (\<Sum>x \<in>  {k}. mset_of_tree (Fs!x))  a
      =
count (\<Sum>x \<in>  {k'}. mset_of_tree (Fs'!x)) a)"
  using \<open>k < length Fs\<close> \<open>pref k = k'\<close> dual_order.antisym pref prefix_count_le by fastforce
      then show ?thesis
        by (metis (no_types, lifting) \<open>F = Tree l Fs\<close> \<open>F' = Tree l Fs'\<close> \<open>k < length Fs\<close> \<open>pref k = k'\<close> add_cancel_left_right finite.emptyI insert_absorb insert_not_empty nth_mem pref rtree.sel(2) sum.empty sum.insert) 
    next
      case False
      hence "k' \<in> {0..<size Fs'} - imgp"
        using \<open>F' = Tree l Fs'\<close> assms(4) by auto
      hence "(\<forall>a < n. count (mset_of_tree (Fs'!k')) a = 0)"
        using casef count_sum_gt_0
        by (simp add: count_sum)
      then show ?thesis
        using \<open>F' = Tree l Fs'\<close> assms(5) by fastforce 
    qed
  qed

lemma exist_limit_indices_eq_prefix_count:
  assumes "F \<le> F'" "\<forall>a. a < n \<longrightarrow> count (mset_of_tree F) a = count (mset_of_tree F') a"
shows "\<exists>p. 
inj_on p {0..<size (sucs F)} \<and> 
(\<forall>k < size (sucs F). p k < size (sucs F') \<and> ((sucs F!k) \<le> (sucs F' ! (p k))) 
\<and> (\<forall>a. a < n \<longrightarrow> count (mset_of_tree (sucs F!k)) a = count (mset_of_tree  (sucs F' ! (p k))) a))"
proof -
 obtain l Fs Fs' where "F = Tree l Fs" "F' = Tree l Fs'"
    by (metis assms(1) less_eq_rtree.transfer prefix.simps) 
  hence "match prefix Fs Fs'"
    using assms(1) by auto
  then obtain pref where pref:"inj_on pref {0..<size Fs} \<and> 
(\<forall>k < size Fs. pref k < size Fs' \<and> ((Fs!k) \<le> (Fs' ! (pref k))))"
    by (metis less_eq_rtree.transfer match_inj_exist)
  define imgp where "imgp = pref ` {0..<size Fs}"
  hence "imgp \<subseteq> {0..<size Fs'}"
    using atLeast0LessThan pref by blast

have "(\<forall>a < n. count (\<Sum>x \<in># mset Fs. mset_of_tree x) a
      = count (\<Sum>x \<in># mset Fs'. mset_of_tree x) a)"
     by (metis \<open>F = Tree l Fs\<close> \<open>F' = Tree l Fs'\<close> add_left_cancel assms(2) count_union mset_of_tree.simps)
hence"(\<forall>a< n. count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
      = count (\<Sum>x \<in> imgp. mset_of_tree (Fs'!x)) a + count (\<Sum>x \<in> {0..<size Fs'} - imgp. mset_of_tree (Fs'!x)) a)"
proof -
  { fix nn :: nat
    have "count ((\<Sum>n\<in>imgp. mset_of_tree (Fs' ! n)) + (\<Sum>n\<in>{..<length Fs'} - imgp. mset_of_tree (Fs' ! n))) nn = count (\<Union>#image_mset mset_of_tree (mset Fs')) nn"
      by (metis \<open>imgp \<subseteq> {0..<length Fs'}\<close> add.commute atLeast0LessThan finite_lessThan sum.subset_diff sum_mset_index)
    then have "\<not> nn < n \<or> count (\<Sum>n\<in>imgp. mset_of_tree (Fs' ! n)) nn + count (\<Sum>n\<in>{0..<length Fs'} - imgp. mset_of_tree (Fs' ! n)) nn = count (\<Sum>n = 0..<length Fs. mset_of_tree (Fs ! n)) nn"
      by (metis (no_types) \<open>\<forall>a<n. count (\<Union>#image_mset mset_of_tree (mset Fs)) a = count (\<Union>#image_mset mset_of_tree (mset Fs')) a\<close> atLeast0LessThan count_union sum_mset_index) }
  then show ?thesis
    by presburger
qed
  have Fssumindex:"(\<Sum>x \<in># mset Fs. mset_of_tree x) = (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x))"
      by (simp add: sum_mset_index)
  hence "(\<forall>a. count (\<Sum>x \<in># mset Fs. mset_of_tree x) a = count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a)"
      by (simp add: sum_mset_index)
  have "(\<forall>a. count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
\<le> count  (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs'!(pref x))) a)"
      by (smt atLeast0LessThan count_sum diff_zero lessThan_iff pref prefix_count_le sum_mono)
  hence"(\<forall>a. count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
      \<le> count  (\<Sum>x \<in> imgp. mset_of_tree (Fs'!x)) a)"
      by (metis (no_types, lifting) imgp_def pref sum.reindex_cong)
  hence"(\<forall>a. count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
      \<le> count (\<Sum>x \<in> imgp. mset_of_tree (Fs'!x)) a + count (\<Sum>x \<in> {0..<size Fs'} - imgp. mset_of_tree (Fs'!x)) a)"
    using trans_le_add1 by blast
  hence casef:"(\<forall>a < n. count (\<Sum>x \<in> {0..<size Fs'} - imgp. mset_of_tree (Fs'!x)) a = 0)"
    by (metis \<open>\<forall>a. count (\<Sum>x = 0..<length Fs. mset_of_tree (Fs ! x)) a \<le> count (\<Sum>x\<in>imgp. mset_of_tree (Fs' ! x)) a\<close> \<open>\<forall>a<n. count (\<Sum>x = 0..<length Fs. mset_of_tree (Fs ! x)) a = count (\<Sum>x\<in>imgp. mset_of_tree (Fs' ! x)) a + count (\<Sum>x\<in>{0..<length Fs'} - imgp. mset_of_tree (Fs' ! x)) a\<close> add_le_same_cancel1 add_less_same_cancel1 le_neq_implies_less not_add_less1)
  hence caset:"(\<forall>a < n. count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
      = count  (\<Sum>x \<in> imgp. mset_of_tree (Fs'!x)) a)"
    by (simp add: \<open>\<forall>a<n. count (\<Sum>x = 0..<length Fs. mset_of_tree (Fs ! x)) a = count (\<Sum>x\<in>imgp. mset_of_tree (Fs' ! x)) a + count (\<Sum>x\<in>{0..<length Fs'} - imgp. mset_of_tree (Fs' ! x)) a\<close> add_cancel_right_right)

  
  
  have "\<And>k. k < size Fs \<Longrightarrow>
(\<forall>a. a < n \<longrightarrow> count (mset_of_tree (Fs!k)) a = count (mset_of_tree  (Fs' ! (pref k))) a)"
  proof -
    fix k assume "k < size Fs"
    show "(\<forall>a. a < n \<longrightarrow> count (mset_of_tree (Fs!k)) a = count (mset_of_tree  (Fs' ! (pref k))) a)"
    proof (rule ccontr)
      assume "\<not> (\<forall>a<n. count (mset_of_tree (Fs ! k)) a = count (mset_of_tree (Fs' ! pref k)) a)"
      hence "\<exists>a < n. count (mset_of_tree (Fs ! k)) a \<noteq> count (mset_of_tree (Fs' ! pref k)) a"
        by blast
      then obtain a where a:"a < n"  " count (mset_of_tree (Fs ! k)) a < count (mset_of_tree (Fs' ! pref k)) a"
        using \<open>k < length Fs\<close> le_neq_implies_less pref prefix_count_le by blast
      have "pref k \<in> imgp"
        by (simp add: \<open>k < length Fs\<close> imgp_def)
      have "count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
      = count  (\<Sum>x \<in> imgp. mset_of_tree (Fs'!x)) a"
        using caset
        by (simp add: a(1)) 
      hence "count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a - count (mset_of_tree (Fs ! k)) a 
      > count  (\<Sum>x \<in> imgp. mset_of_tree (Fs'!x)) a - count (mset_of_tree (Fs' ! pref k)) a"
        using a(2)
        by (smt \<open>imgp \<subseteq> {0..<length Fs'}\<close> \<open>pref k \<in> imgp\<close> atLeastLessThan_empty_iff2 card_atLeastLessThan count_sum diff_diff_cancel diff_le_mono2 diff_zero finite_Diff insert_Diff nat_less_le subset_eq_atLeast0_lessThan_finite sum.insert_remove trans_le_add1)
      hence tmp:"count (\<Sum>x \<in> {0..<size Fs} - {k}. mset_of_tree (Fs!x)) a 
        > count  (\<Sum>x \<in> imgp - {pref k}. mset_of_tree (Fs'!x)) a "
        by (smt \<open>imgp \<subseteq> {0..<length Fs'}\<close> \<open>k < length Fs\<close> \<open>pref k \<in> imgp\<close> add_diff_cancel_left' atLeast0LessThan count_diff finite_atLeastLessThan insert_Diff insert_Diff_single lessThan_iff subset_eq_atLeast0_lessThan_finite sum.insert_remove)
      have "pref ` ({0..<size Fs} - {k}) =  imgp - {pref k}"
        by (smt Diff_insert_absorb \<open>k < length Fs\<close> atLeast0LessThan image_insert imgp_def inj_on_insert insert_Diff insert_Diff_single lessThan_iff pref)
       have "count (\<Sum>x \<in> {0..<size Fs} - {k}. mset_of_tree (Fs!x)) a
\<le> count  (\<Sum>x \<in> {0..<size Fs} - {k}. mset_of_tree (Fs'!(pref x))) a"
         by (smt Diff_iff atLeast0LessThan count_sum lessThan_iff pref prefix_count_le sum_mono)
       hence  " count (\<Sum>x \<in> {0..<size Fs} - {k}. mset_of_tree (Fs!x)) a
\<le> count  (\<Sum>x \<in> imgp - {pref k}. mset_of_tree (Fs'!x)) a"
         using \<open>pref ` ({0..<size Fs} - {k}) =  imgp - {pref k}\<close>
    imgp_def pref sum.reindex_cong
         by (smt \<open>imgp \<subseteq> {0..<length Fs'}\<close> \<open>k < length Fs\<close> \<open>pref k \<in> imgp\<close> add_diff_cancel_left' atLeast0LessThan finite_atLeastLessThan insert_Diff insert_Diff_single lessThan_iff subset_eq_atLeast0_lessThan_finite sum.insert_remove)

      thus "False"
        using tmp
        using leD by blast
    qed
  qed
  thus ?thesis
    using \<open>F = Tree l Fs\<close> \<open>F' = Tree l Fs'\<close> pref by auto
qed

lemma exist_non_sharing_prefix_tines:
  assumes "strictly_inc F'" "F \<le> F'" "\<forall>a. a < n \<longrightarrow> count (mset_of_tree F) a = count (mset_of_tree F') a"
"t1' \<in> tines F'" "t2' \<in> tines F'" "t1' /\<cong> t2' " "last (trace_tine F' t1') < n" "last (trace_tine F' t2') < n" 
shows "\<exists>t1 t2. t1 \<in> tines F \<and> t2 \<in> tines F \<and> t1 /\<cong> t2 \<and> tine_prefix F F' t1 t1' \<and> tine_prefix F F' t2 t2' \<and> 
size t1 = size t1' \<and> size t2 = size t2'"
proof -
 obtain l Fs Fs' where "F = Tree l Fs" "F' = Tree l Fs'"
    by (metis assms(2) less_eq_rtree.transfer prefix.simps) 
  hence "match prefix Fs Fs'"
    using assms(2) by auto
  then obtain pref where pref:"inj_on pref {0..<size Fs} \<and> 
(\<forall>k < size Fs. pref k < size Fs' \<and> ((Fs!k) \<le> (Fs' ! (pref k))))"
    by (metis less_eq_rtree.transfer match_inj_exist)
  define imgp where "imgp = pref ` {0..<size Fs}"
  hence "imgp \<subseteq> {0..<size Fs'}"
    using atLeast0LessThan pref by blast

have "(\<forall>a < n. count (\<Sum>x \<in># mset Fs. mset_of_tree x) a
      = count (\<Sum>x \<in># mset Fs'. mset_of_tree x) a)"
     by (metis \<open>F = Tree l Fs\<close> \<open>F' = Tree l Fs'\<close> add_left_cancel assms(3) count_union mset_of_tree.simps)
hence"(\<forall>a< n. count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
      = count (\<Sum>x \<in> imgp. mset_of_tree (Fs'!x)) a + count (\<Sum>x \<in> {0..<size Fs'} - imgp. mset_of_tree (Fs'!x)) a)"
proof -
  { fix nn :: nat
    have "count ((\<Sum>n\<in>imgp. mset_of_tree (Fs' ! n)) + (\<Sum>n\<in>{..<length Fs'} - imgp. mset_of_tree (Fs' ! n))) nn = count (\<Union>#image_mset mset_of_tree (mset Fs')) nn"
      by (metis \<open>imgp \<subseteq> {0..<length Fs'}\<close> add.commute atLeast0LessThan finite_lessThan sum.subset_diff sum_mset_index)
    then have "\<not> nn < n \<or> count (\<Sum>n\<in>imgp. mset_of_tree (Fs' ! n)) nn + count (\<Sum>n\<in>{0..<length Fs'} - imgp. mset_of_tree (Fs' ! n)) nn = count (\<Sum>n = 0..<length Fs. mset_of_tree (Fs ! n)) nn"
      by (metis (no_types) \<open>\<forall>a<n. count (\<Union>#image_mset mset_of_tree (mset Fs)) a = count (\<Union>#image_mset mset_of_tree (mset Fs')) a\<close> atLeast0LessThan count_union sum_mset_index) }
  then show ?thesis
    by presburger
qed
  have Fssumindex:"(\<Sum>x \<in># mset Fs. mset_of_tree x) = (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x))"
      by (simp add: sum_mset_index)
  hence "(\<forall>a. count (\<Sum>x \<in># mset Fs. mset_of_tree x) a = count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a)"
      by (simp add: sum_mset_index)
  have "(\<forall>a. count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
\<le> count  (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs'!(pref x))) a)"
      by (smt atLeast0LessThan count_sum diff_zero lessThan_iff pref prefix_count_le sum_mono)
  hence"(\<forall>a. count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
      \<le> count  (\<Sum>x \<in> imgp. mset_of_tree (Fs'!x)) a)"
      by (metis (no_types, lifting) imgp_def pref sum.reindex_cong)
  hence"(\<forall>a. count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
      \<le> count (\<Sum>x \<in> imgp. mset_of_tree (Fs'!x)) a + count (\<Sum>x \<in> {0..<size Fs'} - imgp. mset_of_tree (Fs'!x)) a)"
    using trans_le_add1 by blast
  hence casef:"(\<forall>a < n. count (\<Sum>x \<in> {0..<size Fs'} - imgp. mset_of_tree (Fs'!x)) a = 0)"
    by (metis \<open>\<forall>a. count (\<Sum>x = 0..<length Fs. mset_of_tree (Fs ! x)) a \<le> count (\<Sum>x\<in>imgp. mset_of_tree (Fs' ! x)) a\<close> \<open>\<forall>a<n. count (\<Sum>x = 0..<length Fs. mset_of_tree (Fs ! x)) a = count (\<Sum>x\<in>imgp. mset_of_tree (Fs' ! x)) a + count (\<Sum>x\<in>{0..<length Fs'} - imgp. mset_of_tree (Fs' ! x)) a\<close> add_le_same_cancel1 add_less_same_cancel1 le_neq_implies_less not_add_less1)
  hence caset:"(\<forall>a < n. count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
      = count  (\<Sum>x \<in> imgp. mset_of_tree (Fs'!x)) a)"
    by (simp add: \<open>\<forall>a<n. count (\<Sum>x = 0..<length Fs. mset_of_tree (Fs ! x)) a = count (\<Sum>x\<in>imgp. mset_of_tree (Fs' ! x)) a + count (\<Sum>x\<in>{0..<length Fs'} - imgp. mset_of_tree (Fs' ! x)) a\<close> add_cancel_right_right)
  have "\<And>k. k < size Fs \<Longrightarrow>
(\<forall>a. a < n \<longrightarrow> count (mset_of_tree (Fs!k)) a = count (mset_of_tree  (Fs' ! (pref k))) a)"
  proof -
    fix k assume "k < size Fs"
    show "(\<forall>a. a < n \<longrightarrow> count (mset_of_tree (Fs!k)) a = count (mset_of_tree  (Fs' ! (pref k))) a)"
    proof (rule ccontr)
      assume "\<not> (\<forall>a<n. count (mset_of_tree (Fs ! k)) a = count (mset_of_tree (Fs' ! pref k)) a)"
      hence "\<exists>a < n. count (mset_of_tree (Fs ! k)) a \<noteq> count (mset_of_tree (Fs' ! pref k)) a"
        by blast
      then obtain a where a:"a < n"  " count (mset_of_tree (Fs ! k)) a < count (mset_of_tree (Fs' ! pref k)) a"
        using \<open>k < length Fs\<close> le_neq_implies_less pref prefix_count_le by blast
      have "pref k \<in> imgp"
        by (simp add: \<open>k < length Fs\<close> imgp_def)
      have "count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
      = count  (\<Sum>x \<in> imgp. mset_of_tree (Fs'!x)) a"
        using caset
        by (simp add: a(1)) 
      hence "count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a - count (mset_of_tree (Fs ! k)) a 
      > count  (\<Sum>x \<in> imgp. mset_of_tree (Fs'!x)) a - count (mset_of_tree (Fs' ! pref k)) a"
        using a(2)
        by (smt \<open>imgp \<subseteq> {0..<length Fs'}\<close> \<open>pref k \<in> imgp\<close> atLeastLessThan_empty_iff2 card_atLeastLessThan count_sum diff_diff_cancel diff_le_mono2 diff_zero finite_Diff insert_Diff nat_less_le subset_eq_atLeast0_lessThan_finite sum.insert_remove trans_le_add1)
      hence tmp:"count (\<Sum>x \<in> {0..<size Fs} - {k}. mset_of_tree (Fs!x)) a 
        > count  (\<Sum>x \<in> imgp - {pref k}. mset_of_tree (Fs'!x)) a "
        by (smt \<open>imgp \<subseteq> {0..<length Fs'}\<close> \<open>k < length Fs\<close> \<open>pref k \<in> imgp\<close> add_diff_cancel_left' atLeast0LessThan count_diff finite_atLeastLessThan insert_Diff insert_Diff_single lessThan_iff subset_eq_atLeast0_lessThan_finite sum.insert_remove)
      have "pref ` ({0..<size Fs} - {k}) =  imgp - {pref k}"
        by (smt Diff_insert_absorb \<open>k < length Fs\<close> atLeast0LessThan image_insert imgp_def inj_on_insert insert_Diff insert_Diff_single lessThan_iff pref)
       have "count (\<Sum>x \<in> {0..<size Fs} - {k}. mset_of_tree (Fs!x)) a
\<le> count  (\<Sum>x \<in> {0..<size Fs} - {k}. mset_of_tree (Fs'!(pref x))) a"
         by (smt Diff_iff atLeast0LessThan count_sum lessThan_iff pref prefix_count_le sum_mono)
       hence  " count (\<Sum>x \<in> {0..<size Fs} - {k}. mset_of_tree (Fs!x)) a
\<le> count  (\<Sum>x \<in> imgp - {pref k}. mset_of_tree (Fs'!x)) a"
         using \<open>pref ` ({0..<size Fs} - {k}) =  imgp - {pref k}\<close>
    imgp_def pref sum.reindex_cong
         by (smt \<open>imgp \<subseteq> {0..<length Fs'}\<close> \<open>k < length Fs\<close> \<open>pref k \<in> imgp\<close> add_diff_cancel_left' atLeast0LessThan finite_atLeastLessThan insert_Diff insert_Diff_single lessThan_iff subset_eq_atLeast0_lessThan_finite sum.insert_remove)

      thus "False"
        using tmp
        using leD by blast
    qed
  qed

  thus ?thesis
  proof (cases "t1' = [] \<or> t2' = []")
case True
  then show ?thesis
    by (metis assms(2) assms(3) assms(4) assms(5) assms(7) assms(8) equiv.cases exist_tine_prefix_eq_size_prefix list.simps(3) mem_Collect_eq tine_prefix.Nil tine_prefix_imp_both_tines tines_def) 
next
  case False
  then obtain k1' k2' tt1' tt2' where ktt:"k1' \<noteq> k2'" "t1' = k1'#tt1'" "t2' = k2'#tt2'" "k1' < size Fs'" 
"k2' < size Fs'"
    by (metis \<open>F' = Tree l Fs'\<close> assms(4) assms(5) assms(6) equiv_iff mem_Collect_eq rtree.sel(2) tinesP.cases tines_def)
  hence lasttt:"last (trace_tine (Fs' ! k1') tt1') < n \<and> last (trace_tine (Fs' ! k2') tt2') < n"
    by (smt \<open>F' = Tree l Fs'\<close> assms(4) assms(5) assms(7) assms(8) last.simps list.simps(5) rtree.sel(2) tine_prefix_imp_both_tines tine_prefix_refl tinesP_Cons_iff trace_tine.simps trace_tine_non_empty)
  then obtain a1 a2 where a12:"a1 < n" "a2 < n" "count (mset_of_tree (Fs' ! k1')) a1 >0 "
 "count (mset_of_tree (Fs' ! k2')) a2 >0"
    using \<open>F' = Tree l Fs'\<close> \<open>t1' = k1' # tt1'\<close> \<open>t2' = k2' # tt2'\<close> assms(4) assms(5) last_in_set tinesP_Cons_iff tines_def tines_exist_last_label_exist trace_tine_non_empty by auto
  have tinestt: "tt1' \<in> tines (Fs'!k1') \<and> tt2' \<in> tines (Fs'!k2')" 
    using ktt
    using \<open>F' = Tree l Fs'\<close> assms(4) assms(5) tinesP_Cons_iff tines_def by auto
  define imgp where "imgp = pref ` {0..<size Fs}"
  hence "imgp \<subseteq> {0..<size Fs'}"
    using atLeast0LessThan pref
    using \<open>F = Tree l Fs\<close> \<open>F' = Tree l Fs'\<close> by auto 

have "(\<forall>a < n. count (\<Sum>x \<in># mset Fs. mset_of_tree x) a
      = count (\<Sum>x \<in># mset Fs'. mset_of_tree x) a)"
     by (metis \<open>F = Tree l Fs\<close> \<open>F' = Tree l Fs'\<close> add_left_cancel assms(3) count_union mset_of_tree.simps)
hence"(\<forall>a< n. count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
      = count (\<Sum>x \<in> imgp. mset_of_tree (Fs'!x)) a + count (\<Sum>x \<in> {0..<size Fs'} - imgp. mset_of_tree (Fs'!x)) a)"
proof -
  { fix nn :: nat
    have "count ((\<Sum>n\<in>imgp. mset_of_tree (Fs' ! n)) + (\<Sum>n\<in>{..<length Fs'} - imgp. mset_of_tree (Fs' ! n))) nn = count (\<Union>#image_mset mset_of_tree (mset Fs')) nn"
      by (metis \<open>imgp \<subseteq> {0..<length Fs'}\<close> add.commute atLeast0LessThan finite_lessThan sum.subset_diff sum_mset_index)
    then have "\<not> nn < n \<or> count (\<Sum>n\<in>imgp. mset_of_tree (Fs' ! n)) nn + count (\<Sum>n\<in>{0..<length Fs'} - imgp. mset_of_tree (Fs' ! n)) nn = count (\<Sum>n = 0..<length Fs. mset_of_tree (Fs ! n)) nn"
      by (metis (no_types) \<open>\<forall>a<n. count (\<Union>#image_mset mset_of_tree (mset Fs)) a = count (\<Union>#image_mset mset_of_tree (mset Fs')) a\<close> atLeast0LessThan count_union sum_mset_index) }
  then show ?thesis
    by presburger
qed
  have Fssumindex:"(\<Sum>x \<in># mset Fs. mset_of_tree x) = (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x))"
      by (simp add: sum_mset_index)
  hence "(\<forall>a. count (\<Sum>x \<in># mset Fs. mset_of_tree x) a = count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a)"
      by (simp add: sum_mset_index)
  have "(\<forall>a. count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
\<le> count  (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs'!(pref x))) a)"
    using \<open>\<forall>a. count (\<Sum>x = 0..<length Fs. mset_of_tree (Fs ! x)) a \<le> count (\<Sum>x = 0..<length Fs. mset_of_tree (Fs' ! pref x)) a\<close> by blast

  hence"(\<forall>a. count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
      \<le> count  (\<Sum>x \<in> imgp. mset_of_tree (Fs'!x)) a)"
    using \<open>F = Tree l Fs\<close> \<open>F' = Tree l Fs'\<close>
    by (metis (no_types, lifting) imgp_def pref(1) rtree.sel(2) sum.reindex_cong) 
  hence"(\<forall>a. count (\<Sum>x \<in> {0..<size Fs}. mset_of_tree (Fs!x)) a
      \<le> count (\<Sum>x \<in> imgp. mset_of_tree (Fs'!x)) a + count (\<Sum>x \<in> {0..<size Fs'} - imgp. mset_of_tree (Fs'!x)) a)"
    using trans_le_add1 by blast
  hence casef:"(\<forall>a < n. count (\<Sum>x \<in> {0..<size Fs'} - imgp. mset_of_tree (Fs'!x)) a = 0)"
    by (metis \<open>\<forall>a. count (\<Sum>x = 0..<length Fs. mset_of_tree (Fs ! x)) a \<le> count (\<Sum>x\<in>imgp. mset_of_tree (Fs' ! x)) a\<close> \<open>\<forall>a<n. count (\<Sum>x = 0..<length Fs. mset_of_tree (Fs ! x)) a = count (\<Sum>x\<in>imgp. mset_of_tree (Fs' ! x)) a + count (\<Sum>x\<in>{0..<length Fs'} - imgp. mset_of_tree (Fs' ! x)) a\<close> add_le_same_cancel1 add_less_same_cancel1 le_neq_implies_less not_add_less1)
  hence "(\<forall>a < n. 
\<forall>x \<in> {0..<size Fs'} - imgp.
count (mset_of_tree (Fs'!x)) a = 0)"
    by (simp add: count_sum)
hence "k1' \<notin> {0..<size Fs'} - imgp \<and> k2' \<notin> {0..<size Fs'} - imgp" using a12
  by (metis not_less_zero)
  hence "k1' \<in> imgp \<and> k2' \<in> imgp"
    by (simp add: \<open>k1' < length Fs'\<close> \<open>k2' < length Fs'\<close>)
  then obtain k1 k2 where k12:"k1 < size Fs" "k2 < size Fs" "k1 \<noteq> k2" "pref k1 = k1'" "pref k2 = k2'"
    using atLeast0LessThan imgp_def
    using \<open>k1' \<noteq> k2'\<close> by auto 
  hence k1prefix:"(Fs!k1) \<le> (Fs' ! k1')
\<and> (\<forall>a. a < n \<longrightarrow> count (mset_of_tree (Fs!k1)) a = count (mset_of_tree  (Fs' ! k1')) a)"
    using \<open>\<And>k. k < length Fs \<Longrightarrow> \<forall>a<n. count (mset_of_tree (Fs ! k)) a = count (mset_of_tree (Fs' ! pref k)) a\<close> pref by blast
  hence countk1:"count (mset_of_tree (Fs ! k1)) a1 = count (mset_of_tree (Fs' ! k1')) a1"
    by (simp add: a12(1))
 have k2prefix:"(Fs!k2) \<le> (Fs' ! k2')
\<and> (\<forall>a. a < n \<longrightarrow> count (mset_of_tree (Fs!k2)) a = count (mset_of_tree  (Fs' ! k2')) a)"
   using \<open>\<And>k. k < length Fs \<Longrightarrow> \<forall>a<n. count (mset_of_tree (Fs ! k)) a = count (mset_of_tree (Fs' ! pref k)) a\<close> k12(2) k12(5) pref by auto
   hence countk2:"count (mset_of_tree (Fs ! k2)) a2 = count (mset_of_tree (Fs' ! k2')) a2"
    by (simp add: a12(2))
  have "strictly_inc (Fs!k1) \<and> strictly_inc (Fs!k2)"
    using k12 assms(1)
    by (metis (full_types) \<open>F = Tree l Fs\<close> assms(2) nth_mem prefix_strictly_inc rtree.sel(2) strictly_inc.cases)
  obtain tt1 where 
"tine_prefix (Fs!k1) (Fs'!k1') tt1 tt1'" 
"size tt1 = size tt1'"
    using  tinestt k1prefix lasttt prefix_tree_exist_same_size_tine tines_def countk1 
    by (metis (full_types) exist_tine_prefix_eq_size_prefix mem_Collect_eq)
  hence "tine_prefix F F' (k1#tt1) t1' \<and> size (k1#tt1) = size (t1')"
    by (metis Subtree \<open>F = Tree l Fs\<close> \<open>F' = Tree l Fs'\<close> \<open>\<And>thesis. (\<And>k1' k2' tt1' tt2'. \<lbrakk>k1' \<noteq> k2'; t1' = k1' # tt1'; t2' = k2' # tt2'; k1' < length Fs'; k2' < length Fs'\<rbrakk> \<Longrightarrow> thesis) \<Longrightarrow> thesis\<close> assms(2) k12(1) ktt(2) length_Cons list.inject rtree.sel(2))
  obtain tt2 where
"tine_prefix (Fs!k2) (Fs'!k2') tt2 tt2'" 
"size tt2 = size tt2'"
    using  tinestt k2prefix lasttt prefix_tree_exist_same_size_tine tines_def countk2 
    by (metis (full_types) exist_tine_prefix_eq_size_prefix mem_Collect_eq)
 hence "tine_prefix F F' (k2#tt2) t2' \<and> size (k2#tt2) = size (t2')"
   using Subtree \<open>F = Tree l Fs\<close> \<open>F' = Tree l Fs'\<close> assms(2) k12(2) ktt(3) ktt(5) by auto
  thus ?thesis
    by (metis \<open>tine_prefix F F' (k1 # tt1) t1' \<and> length (k1 # tt1) = length t1'\<close> equiv_iff k12(3) mem_Collect_eq tine_prefix_imp_both_tines tines_def)
qed
qed

lemma shifted_honest_indices:
  assumes "a \<le> size w" "h \<in> honest_indices (drop a w)" "a \<in> honest_indices w"
  shows "h + a \<in> honest_indices w"
proof - 
  have "h \<in> insert 0 (Suc ` {n. n < size (drop a w) \<and> ~ (drop a w)!n})"
    using assms(2) honest_indices_def by blast
  hence "h = 0\<or> h \<in> (Suc ` {n. n < size (drop a w) \<and> ~ (drop a w)!n})"
    by simp
  thus ?thesis
  proof (cases h)
    case 0
  then show ?thesis
    by (simp add: assms(3)) 
next
  case (Suc i)
  hence "i \<in> {n. n < size (drop a w) \<and> ~ (drop a w)!n}"
    using \<open>h = 0 \<or> h \<in> Suc ` {n. n < length (drop a w) \<and> \<not> drop a w ! n}\<close> by blast
  hence "i + a < size w \<and> ~ w!(i+a)"
    by (metis (no_types, lifting) add.commute add_diff_cancel_left' assms(1) le_Suc_ex length_drop mem_Collect_eq nat_add_left_cancel_less nth_drop)
  hence "Suc i + a \<in> (Suc ` {n. n < size w \<and> ~ w!n})"
    by simp
  then show ?thesis
    by (simp add: Suc honest_indices_def)
qed
qed



lemma reduce_tree_limit:
  assumes "limit F n" "n \<ge> a"
  shows "limit (reduce_tree F a) (n-a)"
  using assms 
proof (induction)
  case (1 l n Fs)
  have "reduce_tree (Tree l Fs) a = Tree (l - a) (map (\<lambda>x. reduce_tree x a) Fs)"
    by simp
  have "l - a \<le> n - a"
    using 1
    by simp
  have "\<forall>f \<in> set (map (\<lambda>x. reduce_tree x a) Fs). limit f (n-a)"
    by (simp add: "1.IH" "1.prems")
  then show ?case
    by (metis \<open>l - a \<le> n - a\<close> \<open>reduce_tree (Tree l Fs) a = Tree (l - a) (map (\<lambda>x. reduce_tree x a) Fs)\<close> limit.intros)
qed

lemma strictly_inc_list_le_last:
  assumes "a \<in> set x" "(strictly_inc_list x)"
  shows "a \<le> last x "
proof - 
  obtain k where "k < size x" "x ! k = a"
    using assms
    by (meson exist_num_list)
  have "x \<noteq> []"
    using \<open>k < length x\<close> by auto
  hence "last x = x ! ((size x) -1 )"
    by (simp add: last_conv_nth)
  hence "x ! k \<le> x ! ((size x) - 1)"
    by (metis One_nat_def Suc_leI Suc_pred' \<open>k < length x\<close> add_le_cancel_left assms(1) assms(2) diff_less le_neq_implies_less length_pos_if_in_set less_or_eq_imp_le plus_1_eq_Suc strictly_inc_list_as_def zero_less_Suc)
  thus ?thesis
    using \<open>last x = x ! (length x - 1)\<close> \<open>x ! k = a\<close> by linarith
qed

lemma strictly_inc_list_ge_hd:
  assumes "a \<in> set x" "(strictly_inc_list x)"
  shows "a \<ge> hd x "
proof - 
  obtain k where "k < size x" "x ! k = a"
    using assms
    by (meson exist_num_list)
  have "x \<noteq> []"
    using \<open>k < length x\<close> by auto
  hence "hd x = x ! 0"
    by (simp add: hd_conv_nth)

  hence "x ! k \<ge> x ! 0"
    by (metis One_nat_def Suc_leI Suc_pred' \<open>k < length x\<close> add_le_cancel_left assms(1) assms(2) diff_less le_neq_implies_less length_pos_if_in_set less_or_eq_imp_le plus_1_eq_Suc strictly_inc_list_as_def zero_less_Suc)
  thus ?thesis
    by (simp add: \<open>hd x = x ! 0\<close> \<open>x ! k = a\<close>)

qed

lemma dinstinct_index_distinct_elem:
  assumes "finite A" "x \<noteq> y" "x < card A" "y < card A" "f ((list_of A) ! x)" "f ((list_of A) ! y)"
  shows "\<exists>x'\<in> A. \<exists> y' \<in> A. x' \<noteq> y' \<and> f x' \<and> f y'"
proof -
  have 1:"x < size (list_of A) \<and> y < size (list_of A)"
    using assms
    by (simp add: finite_list_of_size_card list_of_def)
  have "distinct (list_of A)"
    by (simp add: assms(1) card_distinct finite_list_of finite_list_of_size_card list_of_def)
  hence "(list_of A) ! x \<noteq> (list_of A) ! y"
    using assms distinct_def 1
    using nth_eq_iff_index_eq by blast
  have "(list_of A) ! x \<in> A \<and> (list_of A) ! y \<in> A"
    by (metis "1" assms(1) finite_list_of list_of_def nth_mem)
  thus ?thesis
    using \<open>list_of A ! x \<noteq> list_of A ! y\<close> assms(5) assms(6) by auto
qed

fun limit_tine_length_tree:: "rtree \<Rightarrow> nat \<Rightarrow> rtree" where
"limit_tine_length_tree (Tree l Fs) 0 = Tree l []"
| "limit_tine_length_tree (Tree l Fs) (Suc h) = Tree l (map (\<lambda>x. limit_tine_length_tree x h) Fs)" 

lemma limit_tine_length_tree_prefix:
"(limit_tine_length_tree F n) \<le> F"
proof (induction F arbitrary: n)
  case (Tree l Fs)
  then show ?case 
  proof (cases n)
    case 0
    hence "limit_tine_length_tree (Tree l Fs) 0  = Tree l []"
      by simp
    have "match prefix [] Fs"
      by simp
    then show ?thesis
      by (simp add: "0") 
  next
    case (Suc k)
    hence "limit_tine_length_tree (Tree l Fs) (Suc k)  = Tree l  (map (\<lambda>x. limit_tine_length_tree x k) Fs)"
      by simp

    have "\<forall>h. h < size Fs \<longrightarrow> (map (\<lambda>x. limit_tine_length_tree x k) Fs) ! h \<le> Fs ! h"
      using Tree.IH by auto
    hence "match prefix (map (\<lambda>x. limit_tine_length_tree x k) Fs) Fs"
      using prefix_branch_prefix_tree by auto
    then show ?thesis
      by (simp add: Suc) 
  qed
qed 

lemma limit_tine_length_tree_exist_shorter_tine:
  assumes "tinesP F t" "size t \<le> n" "last (trace_tine F t) = x"
  shows "\<exists>t'. t' \<in> tines (limit_tine_length_tree F n) \<and> size t' = size t \<and> 
last (trace_tine (limit_tine_length_tree F n) t') = x"
  using assms
proof (induction arbitrary: n)
  case (Sucs k F t)
  then obtain Fs l where "F = Tree l Fs"
    using rtree.exhaust by blast
  hence "k < size Fs" 
    using Sucs.hyps(1) by auto
  then show ?case 
  proof (cases n)
case 0
  then show ?thesis
    using Sucs.prems(1) by blast 
next
  case (Suc h)
  hence "limit_tine_length_tree F (Suc h) = Tree l (map (\<lambda>x. limit_tine_length_tree x h) Fs)"
    by (simp add: \<open>F = Tree l Fs\<close>)
have "length t \<le> h"
  using Suc Sucs.prems(1) by auto
  have "last (trace_tine (Tree l Fs) (k#t)) = x"
    using Sucs.prems(2) \<open>F = Tree l Fs\<close> by blast
  hence "last ( l # (trace_tine (Fs!k) t)) = x"
    by (simp add: \<open>k < length Fs\<close>)
  hence "last (trace_tine (Fs!k) t) = x"
    by (simp add: trace_tine_non_empty)

  then obtain t' where 
"t' \<in> tines (limit_tine_length_tree (Fs ! k) h) \<and>
         length t' = length t \<and> last (trace_tine (limit_tine_length_tree (Fs ! k) h) t') = x"
    using Sucs.IH \<open>F = Tree l Fs\<close> \<open>length t \<le> h\<close> by auto
  have "k < size( sucs (limit_tine_length_tree (Tree l Fs) n))"
    by (simp add: Suc \<open>k < length Fs\<close>)
  hence "k # t' \<in> tines (limit_tine_length_tree F n)"
    using Suc \<open>F = Tree l Fs\<close> \<open>t' \<in> tines (limit_tine_length_tree (Fs ! k) h) \<and> length t' = length t \<and> last (trace_tine (limit_tine_length_tree (Fs ! k) h) t') = x\<close> tinesP.intros(1) tines_def by auto
  have "last (trace_tine (limit_tine_length_tree (Fs ! k) h) t') 
= last (trace_tine (limit_tine_length_tree (Tree l Fs) (Suc h)) (k#t')) "
    by (simp add: \<open>k < length Fs\<close> trace_tine_non_empty)
  then show ?thesis
    by (metis Suc \<open>F = Tree l Fs\<close> \<open>k # t' \<in> tines (limit_tine_length_tree F n)\<close> \<open>t' \<in> tines (limit_tine_length_tree (Fs ! k) h) \<and> length t' = length t \<and> last (trace_tine (limit_tine_length_tree (Fs ! k) h) t') = x\<close> length_Cons) 
qed
next
  case (Nil F)
  then show ?case
  proof -
    have "\<forall>n r ns nsa ra. trace_tine ra [] = trace_tine (limit_tine_length_tree r n) [] \<or> trace_tine ra [] @ nsa \<noteq> trace_tine r ns"
by (metis (no_types) append_eq_append_conv limit_tine_length_tree_prefix suc_tine_size_eq_label_list tine_providing_label_prefix_Nil2 tine_providing_label_prefix_def tinesP.Nil)
  then show ?thesis
    by (metis Nil.prems(2) append_Nil2 mem_Collect_eq tinesP.Nil tines_def)
qed
qed

lemma limit_tine_length_tree_take_tine:
  assumes "tinesP F t"  
  shows "tinesP (limit_tine_length_tree F n) (take n t)"
  using assms
proof (induction arbitrary: n)
  case (Sucs k F t)
  then show ?case 
  proof (cases n)
    case 0
    then show ?thesis
      by (simp add: tinesP.Nil) 
  next
    case (Suc h)
    obtain l Fs where "F = Tree l Fs"
      using rtree.exhaust by blast
    hence "(limit_tine_length_tree F (Suc h)) =  Tree l (map (\<lambda>x. limit_tine_length_tree x h) Fs)"
      by simp
    hence "tinesP (limit_tine_length_tree (Fs!k) h) (take h t)"
      using Suc Sucs.IH Sucs.prems \<open>F = Tree l Fs\<close> by auto
    hence "tinesP (Tree l (map (\<lambda>x. limit_tine_length_tree x h) Fs)) (k# (take h t))"
      using Sucs.hyps(1) \<open>F = Tree l Fs\<close> tinesP.Sucs by auto

    then show ?thesis
      using Suc \<open>limit_tine_length_tree F (Suc h) = Tree l (map (\<lambda>x. limit_tine_length_tree x h) Fs)\<close> by auto 
  qed
next
  case (Nil F)
  then show ?case
    by (simp add: tinesP.Nil) 
qed

lemma limit_tine_length_tree_eq_trace_tine:
  assumes "tinesP F t" "tinesP (limit_tine_length_tree F n) t"
  shows "trace_tine F t = trace_tine (limit_tine_length_tree F n) t"
  using assms 
proof (induction arbitrary: n)
case (Sucs k F t)
  then show ?case 
proof (cases n)
    case 0
    then show ?thesis
      by (metis Sucs.prems limit_tine_length_tree.simps(1) list.size(3) not_less0 rtree.exhaust_sel rtree.sel(2) tinesP_Cons_iff)
  next
    case (Suc h)
    obtain l Fs where "F = Tree l Fs"
      using rtree.exhaust by blast
    hence "(limit_tine_length_tree F (Suc h)) =  Tree l (map (\<lambda>x. limit_tine_length_tree x h) Fs)"
      by simp
    hence "tinesP (limit_tine_length_tree (Fs!k) h) t"
      using Suc Sucs.prems tinesP.cases by fastforce
    hence "trace_tine (Fs!k) t = trace_tine (limit_tine_length_tree (Fs!k) h) t"
      using Sucs.IH \<open>F = Tree l Fs\<close> by auto
    then show ?thesis
      by (simp add: Suc \<open>F = Tree l Fs\<close>)
  qed
next
  case (Nil F)
  then show ?case
    by (metis (no_types, lifting) less_eq_rtree.transfer limit_tine_length_tree_prefix list.case_eq_if prefix.simps trace_tine.simps) 
qed

lemma height_limit_tine_length_tree:
  "height ( limit_tine_length_tree F n)  = min n (height F)"
proof (induction F arbitrary: n)
  case (Tree l Fs)
  then show ?case 
  proof (cases n)
    case 0
    hence "( limit_tine_length_tree (Tree l Fs) 0) = Tree l []"
      by simp
    hence "tines (limit_tine_length_tree (Tree l Fs) 0) = {[]}"
      by simp
    then show ?thesis
      by (metis "0" \<open>limit_tine_length_tree (Tree l Fs) 0 = Tree l []\<close> flat.cases length_0_conv min_0L rtree.sel(2) singletonD trivial_tree_flat) 
  next
    case (Suc h)
      then show ?thesis
      proof (cases "Fs = []")
        case True
        then show ?thesis
          by (metis (full_types) Suc empty_iff flat.cases insert_iff limit_tine_length_tree.simps(2) list.simps(8) list.size(3) min_0R rtree.sel(2) tines_Nil trivial_tree_flat) 
      next
        case False
        hence "height (Tree l Fs) > 0"
          using height_0_unique by fastforce
        then obtain t where "t \<in> tines (Tree l Fs)" "size t = height (Tree l Fs)"
          by (metis (mono_tags, lifting) Max_in empty_iff finite_imageI height_def image_eqI image_iff mem_Collect_eq tinesP.Nil tines_def tines_finite)
        then obtain k' t' where "t= k'#t'"
          by (metis \<open>0 < height (Tree l Fs)\<close> list.exhaust list.size(3) not_less_zero)
        hence "k' < size Fs \<and> t' \<in> tines (Fs ! k')"
          using \<open>t \<in> tines (Tree l Fs)\<close> tinesP_Cons_iff tines_def by auto
        hence "height (Fs ! k') \<ge> size t'"
          using size_tine_le_height by blast
        have "height (Fs!k') \<le> size t'"
        proof (rule ccontr)
          assume "\<not> height (Fs!k') \<le> size t'"
          hence " height (Fs!k') > size t'"
            by auto
          then obtain t'' where "t'' \<in> tines (Fs!k')" "size t'' > size t'"
            by (metis (mono_tags, lifting) Max_in empty_iff finite_imageI height_def image_iff mem_Collect_eq tinesP.Nil tines_def tines_finite)
          hence "k'#t'' \<in> tines (Tree l Fs)"
            by (simp add: Sucs \<open>k' < length Fs \<and> t' \<in> tines (Fs ! k')\<close> tines_def)
          hence "size (k'#t'') > size t"
            by (simp add: \<open>length t' < length t''\<close> \<open>t = k' # t'\<close>)
          thus "False"
            using \<open>k' # t'' \<in> tines (Tree l Fs)\<close> \<open>length t = height (Tree l Fs)\<close> size_tine_le_height by fastforce
        qed
        hence "height (Fs ! k') = size t'"
          by (simp add: \<open>length t' \<le> height (Fs ! k')\<close> eq_iff)
        hence "height (limit_tine_length_tree (Fs!k') h) = min h (height (Fs ! k'))"
          by (simp add: Tree.IH \<open>k' < length Fs \<and> t' \<in> tines (Fs ! k')\<close>)
        have "(take h) t' \<in> tines (limit_tine_length_tree (Fs!k') h)"
          using limit_tine_length_tree_take_tine
          using \<open>k' < length Fs \<and> t' \<in> tines (Fs ! k')\<close> tines_def by auto
        have "size ((take h) t') = min h (height (Fs ! k'))"
          by (simp add: \<open>height (Fs ! k') = length t'\<close>)
        have
"limit_tine_length_tree (Tree l Fs) (Suc h) = Tree l (map (\<lambda>x. limit_tine_length_tree x h) Fs)"
          by simp
        hence "k'#((take h) t') \<in> tines (limit_tine_length_tree (Tree l Fs) (Suc h))"
          by (metis (full_types) \<open>t = k' # t'\<close> \<open>t \<in> tines (Tree l Fs)\<close> limit_tine_length_tree_take_tine mem_Collect_eq take_Suc_Cons tines_def)
        have  "size (k'#((take h) t') ) = Suc (min h (height (Fs ! k')))"
          using \<open>length (take h t') = min h (height (Fs ! k'))\<close> by auto
        hence "... = min (Suc h) (height (Tree l Fs))"
          using \<open>length t = height (Tree l Fs)\<close> \<open>t = k' # t'\<close> by auto
        hence "\<exists>t. t \<in> tines (limit_tine_length_tree (Tree l Fs) (Suc h)) \<and> 
size t = min (Suc h) (height (Tree l Fs))"
          using \<open>k' # take h t' \<in> tines (limit_tine_length_tree (Tree l Fs) (Suc h))\<close> \<open>length (k' # take h t') = Suc (min h (height (Fs ! k')))\<close> by auto
        
        have "\<forall>t \<in> tines (limit_tine_length_tree (Tree l Fs) (Suc h)). size t \<le> min (Suc h) (height (Tree l Fs))"
        proof (rule ccontr)
          assume "\<not> (\<forall>t \<in> tines (limit_tine_length_tree (Tree l Fs) (Suc h)). size t \<le> min (Suc h) (height (Tree l Fs)))"
          then obtain tf where "tf \<in> tines (limit_tine_length_tree (Tree l Fs) (Suc h)) 
\<and> size tf > min (Suc h) (height (Tree l Fs))"
            using leI by blast
          then obtain kf' tf' where "tf = kf'#tf'"
            by (metis list.exhaust list.size(3) not_less0)
          hence "kf' < size (map (\<lambda>x. limit_tine_length_tree x h) Fs) \<and> 
 tf' \<in> tines ((map (\<lambda>x. limit_tine_length_tree x h) Fs) ! kf')"
            using \<open>limit_tine_length_tree (Tree l Fs) (Suc h) = Tree l (map (\<lambda>x. limit_tine_length_tree x h) Fs)\<close>
            using \<open>tf \<in> tines (limit_tine_length_tree (Tree l Fs) (Suc h)) \<and> min (Suc h) (height (Tree l Fs)) < length tf\<close> rtree.sel(2) tinesP_Cons_iff tines_def by auto
          hence "kf' < size Fs \<and> 
 tf' \<in> tines (limit_tine_length_tree (Fs!kf') h)"
            by auto
          hence "size tf' \<le> height (limit_tine_length_tree (Fs!kf') h)"
            by (simp add: size_tine_le_height)
          hence "size tf' \<le> min h (height (Fs!kf'))"
            by (simp add: Tree.IH \<open>kf' < length Fs \<and> tf' \<in> tines (limit_tine_length_tree (Fs ! kf') h)\<close>)
          hence "size tf \<le> Suc (min h (height (Fs!kf')))"
            by (simp add: \<open>tf = kf' # tf'\<close>)
          hence "size tf \<le> Suc h \<and> size tf \<le> Suc (height (Fs!kf'))"
            by auto
          have "size tf > Suc h \<or> size tf > (height (Tree l Fs))"
            using \<open>tf \<in> tines (limit_tine_length_tree (Tree l Fs) (Suc h)) \<and> min (Suc h) (height (Tree l Fs)) < length tf\<close> by auto
          hence "size tf > (height (Tree l Fs))"
            using \<open>length tf \<le> Suc h \<and> length tf \<le> Suc (height (Fs ! kf'))\<close> not_less by blast
          hence " Suc (height (Fs!kf')) > (height (Tree l Fs))"
            using \<open>length tf \<le> Suc h \<and> length tf \<le> Suc (height (Fs ! kf'))\<close> by linarith
          then obtain tx where "tx \<in> tines (Fs!kf')" "size tx = height (Fs!kf')"
            by (metis (mono_tags, hide_lams) Max_in empty_iff finite_imageI height_def image_iff mem_Collect_eq tinesP.Nil tines_def tines_finite)
          hence "kf'#tx \<in> tines (Tree l Fs)"
            by (simp add: \<open>kf' < length Fs \<and> tf' \<in> tines (limit_tine_length_tree (Fs ! kf') h)\<close> tinesP_Cons_iff tines_def)
          hence "height (Tree l Fs) \<ge> size (kf'#tx)"
            using size_tine_le_height by blast
          then show "False"
            using \<open>height (Tree l Fs) < Suc (height (Fs ! kf'))\<close> \<open>length tx = height (Fs ! kf')\<close> by auto
        qed
        hence "\<forall>x. x > min (Suc h) (height (Tree l Fs)) \<longrightarrow> x \<notin> (size ` (tines (limit_tine_length_tree (Tree l Fs) (Suc h))))"
          by auto

        have "min (Suc h) (height (Tree l Fs))
\<in> (size ` (tines (limit_tine_length_tree (Tree l Fs) (Suc h))))"
          using \<open>\<exists>t. t \<in> tines (limit_tine_length_tree (Tree l Fs) (Suc h)) \<and> 
size t = min (Suc h) (height (Tree l Fs))\<close>
          by (metis imageI)
        hence "Max (size ` (tines (limit_tine_length_tree (Tree l Fs) (Suc h)))) = min (Suc h) (height (Tree l Fs))"
        proof -
          obtain nns :: "nat list" where
            "nns \<in> tines (limit_tine_length_tree (Tree l Fs) (Suc h)) \<and> min (Suc h) (height (Tree l Fs)) = length nns"
            by (metis \<open>\<exists>t. t \<in> tines (limit_tine_length_tree (Tree l Fs) (Suc h)) \<and> length t = min (Suc h) (height (Tree l Fs))\<close>)
          then show "MAXIMUM (tines (limit_tine_length_tree (Tree l Fs) (Suc h))) length = min (Suc h) (height (Tree l Fs))"
            by (metis Max_in \<open>\<forall>x>min (Suc h) (height (Tree l Fs)). x \<notin> length ` tines (limit_tine_length_tree (Tree l Fs) (Suc h))\<close> \<open>min (Suc h) (height (Tree l Fs)) \<in> length ` tines (limit_tine_length_tree (Tree l Fs) (Suc h))\<close> empty_iff finite_imageI height_def le_less size_tine_le_height tines_finite)
        qed
          thus ?thesis
            by (simp add: Suc height_def)
        qed
  qed
qed

lemma strictly_inc_exist_prefix_limit:
  assumes "strictly_inc F" "label F \<le> a"
  shows "\<exists>f. f \<le> F \<and> (\<forall>n. n \<le> a \<longrightarrow> count (mset_of_tree f) n = count (mset_of_tree F) n) \<and> 
limit f a"
  using assms
proof (induction)
  case (1 Fs l)
  then
  show ?case
  proof (induction Fs)
    case Nil
    then show ?case
      by (metis "1.prems" empty_iff limit.intros list.set(1) order_refl rtree.sel(1))
  next
    case (Cons h fs)
    hence "\<forall>f\<in>set (h # fs).
       label f \<le> a \<longrightarrow> (\<exists>f'\<le>f. (\<forall>n\<le>a. count (mset_of_tree f') n = count (mset_of_tree f) n) \<and> limit f' a)"
      by simp
    then obtain fs' where 
"Tree l fs' \<le> Tree l fs" 
"(\<forall>n\<le>a. count (mset_of_tree (Tree l fs')) n = count (mset_of_tree (Tree l fs)) n)"
"limit (Tree l fs') a"
      using Cons
      by (smt insert_iff less_eq_rtree.transfer list.set(2) prefix.simps rtree.sel(1)) 
    have stri:"strictly_inc h"
      using Cons
      by simp
    then show ?case
    proof (cases "label h > a")
      case True
 hence "(\<forall>x \<in># mset_of_tree h. x > a)"
      using dual_order.strict_trans1 strictly_inc_label_le stri by blast
    hence " (\<forall>n\<le>a. count (mset_of_tree h) n = 0)"
      by (meson count_inI leD)
    hence "
(\<forall>n\<le>a. count (mset_of_tree (Tree l fs')) n = count (mset_of_tree (Tree l fs)) n + count (mset_of_tree h) n)"
      using \<open>\<forall>n\<le>a. count (mset_of_tree (Tree l fs')) n = count (mset_of_tree (Tree l fs)) n\<close> by auto
    hence tmp:"
(\<forall>n\<le>a. count (mset_of_tree (Tree l fs')) n = 
count (mset_of_tree (Tree l (h#fs))) n)"
      by simp
    have "(Tree l fs') \<le> (Tree l (h#fs))"
      by (metis (full_types) \<open>Tree l fs' \<le> Tree l fs\<close> less_eq_rtree.transfer match.intros(3) prefix_Tree_Tree_eq)
    hence "
(\<exists>f'. f'\<le> (Tree l (h#fs)) \<and> 
(\<forall>n\<le>a. count (mset_of_tree f') n = count (mset_of_tree (Tree l (h#fs))) n) 
\<and> limit f' a)" using tmp
      using \<open>limit (Tree l fs') a\<close> by blast
  then show ?thesis
    by blast 
next
  case False
  then obtain h' where h':"h'\<le>h \<and> (\<forall>n\<le>a. count (mset_of_tree h') n = count (mset_of_tree h) n) \<and> 
                        limit h' a"
    using Cons
    by force
  hence ptmp:"Tree l (h'#fs') \<le> Tree l (h#fs) "
    by (metis \<open>Tree l fs' \<le> Tree l fs\<close> less_eq_rtree.transfer match.intros(2) prefix.intros prefix_Tree_Tree_eq)
  have "(\<forall>n\<le>a. count (mset_of_tree h') n + count (mset_of_tree (Tree l fs')) n= 
count (mset_of_tree h) n + count (mset_of_tree (Tree l fs)) n)"
    using \<open>\<forall>n\<le>a. count (mset_of_tree (Tree l fs')) n = count (mset_of_tree (Tree l fs)) n\<close> h' by auto
  hence ctmp:"(\<forall>n\<le>a.  count (mset_of_tree (Tree l (h'#fs'))) n= count (mset_of_tree (Tree l (h#fs))) n)"
    by (smt add.assoc add.commute count_union mset.simps(2) mset_of_tree.simps sum_mset.insert)
  have "limit (Tree l (h'#fs')) a"
    using h'
    by (smt \<open>limit (Tree l fs') a\<close> insert_iff limit.simps list.set(2) rtree.sel(1) rtree.sel(2))
then show ?thesis  using ptmp ctmp
  by blast
qed
qed
qed

theorem exist_forkable_substring_length_ge_div:
  assumes "length w = l" 
  shows "\<exists>w' i j.  forkable w' \<and> i \<le> j \<and> j \<le> l \<and> (drop i (take j w)) = w' \<and> length w' \<ge> div_s w"
proof -    
  define dw where "dw = div_s w"
  thus ?thesis
  proof (cases "dw = 0")
    case True
    have "forkable []"
      by (simp add: forkable_adversarial_strings)
    hence  "(drop 0 (take 0 w)) = []"
      by simp
    then show ?thesis
      using True \<open>dw = div_s w\<close> \<open>forkable []\<close> by fastforce 
  next
    case False
  then obtain F t1' t2' where Ft12': "(F, (t1',t2') ) \<in> viable_tines_div_pairs_set w"  
    "optimal_diff_tine_pair (viable_tines_div_pairs_set w) = abs ((int (lb F t1')) - int (lb F t2'))"
    using finite_abs_diff_pair nonempty_abs_diff_pair Max_in imageE optimal_diff_tine_pair_def
    by (metis (no_types, lifting) Min_in prod.exhaust_sel)
  hence Fw:"F \<turnstile> w"
    using viable_tines_div_pairs_set_def by force
  have "lb F t1' > lb F t2' \<longrightarrow> divs F t1' t2' = 0"
    using divs_def by auto
  hence "lb F t1' > lb F t2' \<longrightarrow> div_s w = 0"
    using Ft12'
    using viable_tines_div_pairs_set_def by auto
  have "lb F t1' > lb F t2' \<longrightarrow> divs F t2' t1' \<le> 0"
  proof (rule ccontr)
    assume "\<not> (lb F t1' > lb F t2' \<longrightarrow> divs F t2' t1' \<le> 0)"
    then have "lb F t1' > lb F t2' \<and> divs F t2' t1' > 0"
      by simp
    hence "divs F t1' t2' = 0"
      by (simp add: \<open>lb F t2' < lb F t1' \<longrightarrow> divs F t1' t2' = 0\<close>)
    hence "div_tines F t1' t2' > 0"
      using Ft12'
      by (simp add: \<open>lb F t2' < lb F t1' \<and> 0 < divs F t2' t1'\<close> div_tines_def)
    have "t1' \<in> tines F \<and> t2' \<in> tines F"
      using Ft12'
      using prod.sel(2) viable.cases viable_tines_div_pairs_set_def by auto
    hence "div_tines F t1' t2' \<in> (\<Union>t1 \<in> tines F. 
 (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2}))"
      using Ft12'(1) viable_tines_div_pairs_set_def by auto
    hence "Max (\<Union>t1 \<in> tines F. 
 (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2}))
\<ge> div_tines F t1' t2'"
      using finite_div_tines_set by auto
    hence "div_f F w > 0"
      using \<open>0 < div_tines F t1' t2'\<close> div_f_def by auto
    thus False
      using Ft12'(1) \<open>lb F t2' < lb F t1' \<and> 0 < divs F t2' t1'\<close> \<open>lb F t2' < lb F t1' \<longrightarrow> div_s w = 0\<close> viable_tines_div_pairs_set_def by auto
  qed
  hence"lb F t1' > lb F t2' \<longrightarrow> divs F t2' t1' = 0"
    by simp
  define t1 t2 where "t1 = (if lb F t1' \<le> lb F t2' then t1' else t2')"
                      "t2 = (if lb F t1' \<le> lb F t2' then t2' else t1')"
  hence Ft12:"(F,(t1,t2)) \<in> viable_tines_div_pairs_set w \<and> 
  optimal_diff_tine_pair (viable_tines_div_pairs_set w) = abs ((int (lb F t1)) - int (lb F t2))"
    using Ft12' \<open>lb F t2' < lb F t1' \<longrightarrow> divs F t1' t2' = 0\<close> \<open>lb F t2' < lb F t1' \<longrightarrow> divs F t2' t1' 
          = 0\<close> viable_tines_div_pairs_set_def by force
  hence "lb F t1 \<le> lb F t2"
    by (simp add: \<open>t1 = (if lb F t1' \<le> lb F t2' then t1' else t2')\<close> \<open>t2 = (if lb F t1' \<le> lb F t2' then t2' else t1')\<close>)
  
have "t1 \<in> tines F \<and> t2 \<in> tines  F"
            using Ft12 viable_tines_div_pairs_set_def viable.simps by auto
          define t' where t':"t' = longest_common_prefix_tines t1 t2"

  hence div_sw:"length t1 - length t' = div_s w"
    using viable_tines_div_pairs_set_def divs_def Ft12 \<open>lb F t1 \<le> lb F t2\<close>
    by simp
  define \<alpha>' where alpha:"\<alpha>' = lb F t'"

  define \<beta> where beta:"\<beta> = (if \<exists>h \<in> honest_indices w. h \<ge> lb F t2 then Min {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}  else Suc (size w))"
  
  hence "lb F t1 < \<beta>"
  proof (cases  "\<exists>h \<in> honest_indices w. h \<ge> lb F t2")
    case True
    hence "\<beta> = Min {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}"
      using \<open>\<beta> = (if \<exists>h \<in> honest_indices w. h \<ge> lb F t2 then Min {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}  else Suc (size w))\<close> by auto
    have "finite {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}"
      using finite_honest_indices by auto
    hence "\<beta> \<in> {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}"
      using True
      using Min_in \<open>\<beta> = Min {h \<in> honest_indices w. lb F t2 \<le> h}\<close> by blast
    then show ?thesis
      proof (cases "lb F t1 = lb F t2")
        case True
        have adv:"adversarial F t1 (honest_indices w)"
        proof (rule ccontr)
          assume "\<not> adversarial F t1 (honest_indices w)"
          hence "honest F t1 (honest_indices w)"
            by simp
          hence "count (mset_of_tree F) ( last (trace_tine F t1)) = 1"
            using Fw fork_F
            by (simp add: forks_def honest_def) 
          hence unt:"\<exists>!t \<in> tines F. last (trace_tine F t) = last (trace_tine F t1)"
            by (simp add: exist_unique_label_exist_unique_tine)
          have "last (trace_tine F t2) = last (trace_tine F t1)"
            using True lb_def
            by (smt Ft12 lessI less_eq_Suc_le mem_Collect_eq prod.simps(1) suc_tine_size_eq_label_list take_Suc_nth take_all tines_def viable.simps viable_tines_div_pairs_set_def)
          hence "t1 = t2" using unt
            using \<open>last (trace_tine F t2) = last (trace_tine F t1)\<close> \<open>t1 \<in> tines F \<and> t2 \<in> tines  F\<close>  by blast
          hence "longest_common_prefix_tines t1 t2 = t1"
            by (simp add: longest_common_prefix_tines_self)
          have "t1 \<noteq> t'"
            using False \<open>dw = div_s w\<close> div_sw by auto
          thus "False"
            using \<open>t1 = t2\<close> longest_common_prefix_tines_self t' by auto
        qed
        hence tmp:"last (trace_tine  F t1) \<notin> honest_indices w"
          using honest_def
          by blast
        have"\<beta> \<in> honest_indices w"
          using \<open>\<beta> \<in> {h \<in> honest_indices w. lb F t2 \<le> h}\<close> by blast
        hence "\<beta> \<noteq> last (trace_tine  F t1)"
          using adv lb_in_F_last_trace_tine tmp
          by blast 
        hence "\<beta> \<noteq> lb F t1"
          using Ft12 \<open>t1 \<in> tines F \<and> t2 \<in> tines  F\<close>
          by (simp add: lb_in_F_last_trace_tine tines_def)
        thus "\<beta> > lb F t1"
          using \<open>lb F t1 \<le> lb F t2\<close>
          using \<open>\<beta> \<in> {h \<in> honest_indices w. lb F t2 \<le> h}\<close> by auto
      next
        case False

        then show ?thesis
          using \<open>\<beta> \<in> {h \<in> honest_indices w. lb F t2 \<le> h}\<close> \<open>lb F t1 \<le> lb F t2\<close> by auto 

      qed
  next
    case False
    have "lb F t1 \<le> size w"
      using Ft12'
      by (smt Pair_inject \<open>(F, t1, t2) \<in> viable_tines_div_pairs_set w \<and> optimal_diff_tine_pair (viable_tines_div_pairs_set w) = \<bar>int (lb F t1) - int (lb F t2)\<bar>\<close> lb_limit mem_Collect_eq tines_def viable.cases viable_tines_div_pairs_set_def)
    then show ?thesis
      using beta
      by (simp add: False)
  qed
  have betahonest:"\<beta> \<in> set (trace_tine F t2) \<longrightarrow> \<beta> \<in> honest_indices w"
  proof (cases "\<exists>h \<in> honest_indices w. h \<ge> lb F t2")
    case True
    hence "\<beta> = Min {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}"
      by (simp add: beta)
    hence "\<beta> \<in> {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}"
      by (metis Collect_mem_eq True emptyE eq_Min_iff finite_Collect_conjI finite_honest_indices mem_Collect_eq)
    then show ?thesis
      by blast 
  next
    case False
    then show ?thesis
      using Fw beta fork_imp_trace_tine_label_le_size by fastforce 
  qed
  have "size t' \<le> size t2"
    using \<open>t' = longest_common_prefix_tines t1 t2\<close> longest_common_prefix_size_le longest_common_prefix_tines_commute by fastforce
  hence "\<beta> - \<alpha>' - 1 \<ge> lb F t1 - lb F t'"
    using \<open>lb F t1 < \<beta>\<close> alpha by linarith
  have "take (length t') t1 = t'"
    by (metis \<open>t' = longest_common_prefix_tines t1 t2\<close> length_take longest_common_prefix_tines_take_exist min_absorb2)
  hence "t' \<in> tines F"
    using Ft12 take_tine_is_tine tines_def
    by (smt mem_Collect_eq prod.sel(1) prod.sel(2) viable.cases viable_tines_div_pairs_set_def)    
  hence "\<alpha>' \<in># mset_of_tree F"
    using lb_in_F
    by (simp add: alpha tines_def)
  hence "\<alpha>' \<le> size w"
    using Ft12' limit_mset_of_tree_le viable_tines_div_pairs_set_def
      forks_def fork.simps
    by auto
  hence "\<beta> - 1 \<le> size w"
    proof (cases "\<exists>h\<in>honest_indices w. h \<ge> lb F t2")
    case True
    hence "\<beta> = Min {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}"
      using beta
      by auto
    have "finite {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}"
      using finite_honest_indices by auto
    hence "\<beta> \<in> {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}"
      using True
      using Min_in
      by (metis \<open>\<beta> = Min {h \<in> honest_indices w. lb F t2 \<le> h}\<close> bex_empty less_or_eq_imp_le mem_Collect_eq) 
    hence "\<beta> \<in> honest_indices w"
      by simp
    hence "\<beta> \<le> size w"
      using honest_indices_iff by blast
    then show ?thesis by simp
  next
    case False
    hence "\<beta> = Suc (size w)" using beta by simp
    then show ?thesis
      by simp
  qed
  define wv where wv:"wv = drop \<alpha>' (take (\<beta> - 1) w)"
  hence "size wv = \<beta> - 1 - \<alpha>'"
    using \<open>\<beta> - 1 \<le> length w\<close> by auto
  have "size t' \<le> size  t1"
    using \<open>t' = longest_common_prefix_tines t1 t2\<close> longest_common_prefix_size_le
    by simp 
  hence "take (Suc (length t')) (trace_tine F t1) = trace_tine F (take (length t') t1)"
    using take_trace_tine_commutative Ft12
proof -
have "(F, t1, t2) \<in> viable_tines_div_pairs_set w \<and> optimal_diff_tine_pair (viable_tines_div_pairs_set w) = (if 0 \<le> int (lb F t1) + - 1 * int (lb F t2) then int (lb F t1) + - 1 * int (lb F t2) else - 1 * int (lb F t1) + int (lb F t2))"
  using Ft12 by force
  then have "\<exists>r ns nsa. r \<turnstile> w \<and> (F, t1, t2) = (r, ns, nsa) \<and> div_f r w = div_s w \<and> viable r w ns \<and> viable r w nsa \<and> div_f r w = divs r ns nsa"
    using viable_tines_div_pairs_set_def by force
  then show ?thesis
    by (metis (no_types) Pair_inject \<open>length t' \<le> length t1\<close> mem_Collect_eq take_trace_tine_commutative tines_def viable.cases)
qed
   hence "take (Suc (length t')) (trace_tine F t1) = trace_tine F t'"
     by (simp add: \<open>take (length t') t1 = t'\<close>)
   hence "take (Suc (length t')) (trace_tine F t1) ! length t' = lb F t'"
     using lb_def
     by simp
   hence "trace_tine F t1 ! length t' = lb F t'"
     using lb_def
     by simp
   have "strictly_inc_list (trace_tine F t1)"
     using Ft12 viable_tines_div_pairs_set_def strictly_inc_imp_strictly_inc_tine
   proof -
have "\<forall>r bs ns. \<not> viable r bs ns \<or> (\<exists>ra bsa nsa. r = ra \<and> bs = bsa \<and> ns = nsa \<and> ra \<turnstile> bsa \<and> nsa \<in> tines ra \<and> (\<forall>n. (n \<notin> honest_indices bsa \<or> \<not> n \<le> last (trace_tine ra nsa)) \<or> depth ra n \<le> length  nsa))"
by (meson viable.cases)
  then obtain rr :: "nat list \<Rightarrow> bool list \<Rightarrow> rtree \<Rightarrow> rtree" and bbs :: "nat list \<Rightarrow> bool list \<Rightarrow> rtree \<Rightarrow> bool list" and nns :: "nat list \<Rightarrow> bool list \<Rightarrow> rtree \<Rightarrow> nat list" where
    f1: "\<forall>r bs ns. \<not> viable r bs ns \<or> r = rr ns bs r \<and> bs = bbs ns bs r \<and> ns = nns ns bs r \<and> rr ns bs r \<turnstile> bbs ns bs r \<and> nns ns bs r \<in> tines (rr ns bs r) \<and> (\<forall>n. (n \<notin> honest_indices (bbs ns bs r) \<or> \<not> n \<le> last (trace_tine (rr ns bs r) (nns ns bs r))) \<or> depth (rr ns bs r) n \<le> length (nns ns bs r))"
    by fastforce
have "(F, t1, t2) \<in> viable_tines_div_pairs_set w \<and> optimal_diff_tine_pair (viable_tines_div_pairs_set w) = (if 0 \<le> int (lb F t1) + - 1 * int (lb F t2) then int (lb F t1) + - 1 * int (lb F t2) else - 1 * int (lb F t1) + int (lb F t2))"
  using Ft12 by force
  then show ?thesis
using f1 strictly_inc_list_trace_tine viable_tines_div_pairs_set_def by fastforce
qed
  have "size t' < size (trace_tine F t1)"
    using \<open>size t' \<le> size  t1\<close>
    by (smt Ft12 Pair_inject Suc_le_eq leD leI mem_Collect_eq suc_tine_size_eq_label_list tines_def viable.cases viable_tines_div_pairs_set_def)
  have "length t1< size (trace_tine F t1)"
  proof -
    have "(F, t1, t2) \<in> {p. \<exists>r ns nsa. r \<turnstile> w \<and> p = (r, ns, nsa) \<and> div_f r w = div_s w \<and> viable r w ns \<and> viable r w nsa \<and> div_f r w = divs r ns nsa}"
      using Ft12 viable_tines_div_pairs_set_def by auto
    then have "\<exists>r ns nsa. r \<turnstile> w \<and> (F, t1, t2) = (r, ns, nsa) \<and> div_f r w = div_s w \<and> viable r w ns \<and> viable r w nsa \<and> div_f r w = divs r ns nsa"
      by blast
    then show ?thesis
      by (metis Pair_inject Suc_le_eq le_eq_less_or_eq mem_Collect_eq suc_tine_size_eq_label_list tines_def viable.cases)
  qed
  hence " (length t' + (length t1 - length t')) < size (trace_tine F t1)"
    by (simp add: \<open>length t' \<le> length t1\<close>)
  hence "length t1 - length t' \<le> trace_tine F t1 ! (length t' + (length t1 - length t')) - trace_tine F t1 ! length t'"
    using strictly_inc_dif_ge Ft12 \<open>strictly_inc_list (trace_tine F t1)\<close> \<open>size t' < size (trace_tine F t1)\<close>
    by blast
     hence "lb F t1 - lb F t' \<ge> length t1 - length t'"
       by (simp add: \<open>length t' \<le> length t1\<close> \<open>trace_tine F t1 ! length t' = lb F t'\<close> lb_def)
     hence "size wv \<ge> div_s w"
       by (metis One_nat_def \<open>lb F t1 - lb F t' \<le> \<beta> - \<alpha>' - 1\<close> \<open>length t1 - length t' = div_s w\<close> \<open>length wv = \<beta> - 1 - \<alpha>'\<close> alpha beta diff_commute dual_order.trans)
     have "size t1 \<noteq> size t'"
     proof (rule ccontr)
       assume "\<not> size t1 \<noteq> size t'"
       hence "size t1 = size t'" by blast
       hence "divs F t1 t2 = 0 \<and> divs F t2 t1 = 0"
         using False \<open>dw = div_s w\<close> div_sw by auto
       hence "div_tines F t1 t2 = 0"
         by (simp add: div_tines_def)
       hence "dw = 0"
         using \<open>\<not> length t1 \<noteq> length t'\<close> \<open>dw = div_s w\<close> div_sw by auto
       thus "False"
         using False by auto
     qed
     hence t1gtt':"size t1 > size t'"
       using \<open>length t' \<le> length t1\<close> by linarith
     define k where k:"k = t1 ! (size t')"

           hence t'k:"take (Suc (size t')) t1 = t' @[k] "
             using \<open>take (length t') t1 = t'\<close>
             by (simp add: \<open>length t' < length t1\<close> take_Suc_conv_app_nth)
           have tinesFt1:"tinesP F t1"
             using Ft12 tines_def viable.cases viable_tines_div_pairs_set_def by fastforce
           hence "tinesP F (t' @ [k])"
             using take_tine_is_tine
             by (metis (full_types) \<open>take (Suc (length t')) t1 = t' @ [k]\<close>) 

have "size t2 \<noteq> size t'"
     proof (rule ccontr)
       assume "\<not> size t2 \<noteq> size t'"
       hence "size t2 = size t'" by blast
       hence "t2 = t'"
         using nth_equalityI prefix_list_longest_common_prefix_tines_2 prefix_list_nth t' by fastforce
       hence "lb F t2 = lb F t'"
         by simp
       hence "lb F t1 < lb F t'"
         using \<open>lb F t1 \<le> lb F t2\<close> \<open>length t1 - length t' \<le> lb F t1 - lb F t'\<close> t1gtt' by linarith
       thus "False"
         using False
         using \<open>length t' \<le> length t1\<close> \<open>length t1 - length t' \<le> lb F t1 - lb F t'\<close> \<open>length t1 \<noteq> length t'\<close> by linarith 
     qed
     hence t2gtt':"size t2 > size t'"
       using \<open>length t' \<le> length t2\<close> by linarith
     define k where k:"k = t1 ! (size t')"

           hence t'k:"take (Suc (size t')) t1 = t' @[k] "
             using \<open>take (length t') t1 = t'\<close>
             by (simp add: \<open>length t' < length t1\<close> take_Suc_conv_app_nth)
           have tinesFt1:"tinesP F t1"
             using Ft12 tines_def viable.cases viable_tines_div_pairs_set_def by fastforce
           hence "tinesP F (t' @ [k])"
             using take_tine_is_tine
             by (metis (full_types) \<open>take (Suc (length t')) t1 = t' @ [k]\<close>) 

        have net2t'k:"t2 ! size t' \<noteq> k"
             proof (rule ccontr)
               assume "\<not> t2 ! size t' \<noteq> k"
               hence "t2 ! size t' = k"
                 by auto
               hence "take (Suc (size t')) t2 = take (Suc (size t')) t1"
                 by (metis False \<open>lb F t1 \<le> lb F t2\<close> \<open>length t' < length t1\<close> \<open>length t' \<le> length t2\<close> \<open>length t1 - length t' \<le> trace_tine F t1 ! (length t' + (length t1 - length t')) - trace_tine F t1 ! length t'\<close> \<open>trace_tine F t1 ! length t' = lb F t'\<close> diff_is_0_eq lb_def le_0_eq le_antisym le_eq_less_or_eq nth_equalityI nth_take ordered_cancel_comm_monoid_diff_class.add_diff_inverse prefix_list_longest_common_prefix_tines_2 prefix_list_take t' t'k take_Suc_conv_app_nth)
               thus "False"
                 using t' longest_common_prefix_tines_take_lt_size_ne
                 by (metis length_append_singleton lessI nat_le_linear t'k take_all)
             qed
             define t2tmp where t2tmp:"t2tmp = (if t2 ! size t' < k then t2 else t2[length t' := t2 ! length t' - 1])"

             have preft't2:"prefix_list t' t2"
                 by (simp add: prefix_list_longest_common_prefix_tines_2 t')
             have t'ktinesF:"t' @ [k] \<in> tines F"
                 using k t' tinesFt1
                 using \<open>tinesP F (t' @ [k])\<close> tines_def  by auto
               have tinesFt2:"t2 \<in> tines F"
                 using Ft12
                 using viable.cases viable_tines_div_pairs_set_def by auto
             hence prep1:"t2 ! size t' < k \<longrightarrow> t2 \<in> tines (Cut_subtree F t' k)"
               using tinesFt1 preft't2  net2t'k \<open>t' \<in> tines F\<close> t'ktinesF Cut_subtree_tines_3_lt net2t'k \<open>length t' \<le> length t2\<close>
               by (metis Cut_subtree_tines_1 \<open>tinesP F (t' @ [k])\<close> butlast_snoc butlast_tinesP le_eq_less_or_eq)
             hence prep2:"t2 ! size t' <k \<longrightarrow>
                    trace_tine F t2 = trace_tine (Cut_subtree F t' k) t2"
               using tinesFt1 preft't2  net2t'k \<open>t' \<in> tines F\<close> t'ktinesF Cut_subtree_trace_tine_3_lt net2t'k \<open>length t' \<le> length t2\<close>
               by (metis Cut_subtree_trace_tine_1 \<open>tinesP F (t' @ [k])\<close> butlast_snoc butlast_tinesP le_eq_less_or_eq tinesFt2)
             have prep3:"t2 ! size t' > k \<longrightarrow> t2[length t' := t2 ! length t' - 1] \<in> tines (Cut_subtree F t' k)"
               using tinesFt1 tinesFt2 preft't2  net2t'k \<open>t' \<in> tines F\<close> t'ktinesF Cut_subtree_tines_3_gt net2t'k \<open>length t' \<le> length t2\<close>
               by (metis (full_types) Cut_subtree_tines_1 le_eq_less_or_eq list_update_beyond mem_Collect_eq tines_def)
             hence prep4:"t2 ! size t' > k \<longrightarrow> 
trace_tine (Cut_subtree F t' k) (t2[length t' := t2 ! length t' - 1]) = trace_tine F t2"
               using tinesFt1 tinesFt2 preft't2  net2t'k \<open>t' \<in> tines F\<close> t'ktinesF Cut_subtree_trace_tine_3_lt net2t'k \<open>length t' \<le> length t2\<close>
               by (metis Cut_subtree_trace_tine_1 Cut_subtree_trace_tine_3_gt \<open>tinesP F (t' @ [k])\<close> butlast_snoc butlast_tinesP le_eq_less_or_eq list_update_beyond)
             have tmptines:"t2tmp \<in> tines (Cut_subtree F t' k)"
               using prep1 prep3 net2t'k
               using t2tmp by auto
             have tmptrace_tine:"trace_tine (Cut_subtree F t' k) t2tmp = trace_tine F t2"
               using prep2 prep4 net2t'k
               using t2tmp by auto
             have "prefix_list t' t2tmp"
             proof - 
               have "take (size t') t2 = t'"
                 using \<open>prefix_list t' t2\<close>
                 by (simp add: prefix_list_take)
               hence "take (size t') t2tmp = t'"
                 using t2tmp
                 by auto
               thus ?thesis
                 by (simp add: \<open>length t' \<le> length t2\<close> t2tmp take_prefix_list)
             qed 
             have "viable F w t1" "viable F w t2"
               using Ft12 viable_tines_div_pairs_set_def by auto
             have "honest F t' (honest_indices w)"
     proof (rule ccontr)
       assume "adversarial F t' (honest_indices w)"
       have "t' \<noteq> []"
       proof (rule ccontr)
         assume "\<not> t' \<noteq> []"
         obtain Fs where "F = Tree 0 Fs"
           using Ft12'
         proof -
           assume a1: "\<And>Fs. F = Tree 0 Fs \<Longrightarrow> thesis"
           have "F \<turnstile> w"
             using Ft12'(1) viable_tines_div_pairs_set_def by force
           then show ?thesis
             using a1 by (metis fork_F forks_def rtree.collapse)
         qed
         hence "trace_tine F t' = [0]"
           using \<open>\<not> t' \<noteq> []\<close>
           by simp
         thus "False"
           using \<open>F = Tree 0 Fs\<close> \<open>adversarial F t' (honest_indices w)\<close> honest_def honest_indices_iff by auto
       qed
       hence "size t' > 0"
         by auto

       obtain st en where sten:"t' = st @ [en]"
         using \<open>t' \<noteq> []\<close> rev_exhaust by auto
       hence "st = take ((size t') - 1) t' \<and> 
              [en] = drop (size t' - 1) t'"
         by simp
       hence st:"st \<in> tines F"
         using tines_def tinesP_take_tinesP
         using \<open>t' \<in> tines F\<close> by blast
       thus "False"
       proof -
         define F' where F' :"F' = Extend_tree (Cut_subtree F t' k) (st) (Tree (lb F t') [get_subtree F (t' @ [k])])"
           hence "F' \<turnstile> w"
             using  Ft12 sten Extend_tree_Cut_subtree_preserves_fork \<open>adversarial F t' (honest_indices w)\<close>
             by (smt \<open>tinesP F (t' @ [k])\<close> butlast_snoc mem_Collect_eq prod.sel(1) tines_def viable_tines_div_pairs_set_def)
         hence "trace_tine F' st = trace_tine (Cut_subtree F t' k) st"
           using Extend_tree_trace_tine_1 F'
           by (metis Cut_subtree_preserve_cut_tines \<open>tinesP F (t' @ [k])\<close> butlast_snoc butlast_tinesP mem_Collect_eq order_refl sten tines_def)
         hence "trace_tine F' st = trace_tine F st"
           using sten
           using Cut_subtree_trace_tine_1 \<open>t' \<in> tines F\<close> st tines_def by auto
           have "tinesP (get_subtree F  (t' @ [k])) (drop (Suc (size t')) t1)"
             by (metis (full_types) \<open>tinesP F t1\<close> append_take_drop_id mod_get_subtree_tines_append_in_tines t'k)
           hence tinesend:"tinesP (Tree (lb F t') [get_subtree F (t' @ [k])]) (0#(drop (Suc (size t')) t1))"
             by (simp add: tinesP_Cons_iff)
           hence newtine:  "trace_tine F' (st@(0#(0#(drop (Suc (size t')) t1)))) = (trace_tine (Cut_subtree F t' k) st)
                @ (trace_tine (Tree (lb F t') [get_subtree F (t' @ [k])]) (0#(drop (Suc (size t')) t1)))"
             using Extend_tree_trace_tine_4 F'
             by (metis Cut_subtree_preserve_cut_tines \<open>tinesP F (t' @ [k])\<close> butlast_snoc butlast_tinesP mem_Collect_eq sten tines_def)
           have "(trace_tine (Cut_subtree F t' k) st)
                @ (trace_tine (Tree (lb F t') [get_subtree F (t' @ [k])]) (0#(drop (Suc (size t')) t1)))
                = trace_tine F t1"

         proof -
           have tmp:"(trace_tine (Cut_subtree F t' k) st)
                @ (trace_tine (Tree (lb F t') [get_subtree F (t' @ [k])]) (0#(drop (Suc (size t')) t1))) = 
                (trace_tine F st) @ [lb F t'] @
(trace_tine (get_subtree F (t' @ [k])) (drop (Suc (size t')) t1))"
             using \<open>trace_tine F' st = trace_tine (Cut_subtree F t' k) st\<close> \<open>trace_tine F' st = trace_tine F st\<close> by auto
           hence "(trace_tine (Cut_subtree F t' k) st)
                @ (trace_tine (Tree (lb F t') [get_subtree F (t' @ [k])]) (0#(drop (Suc (size t')) t1))) = 
                (trace_tine F t') @ (trace_tine (get_subtree F (t' @ [k])) (drop (Suc (size t')) t1))"
           proof -
             have "(trace_tine F st) = take (Suc (size st)) (trace_tine F t')" using sten
               by (metis \<open>st = take (length t' - 1) t' \<and> [en] = drop (length t' - 1) t'\<close> \<open>tinesP F (t' @ [k])\<close> butlast_snoc butlast_tinesP diff_Suc_1 diff_le_self length_append_singleton take_trace_tine_commutative)
             hence "(trace_tine F st) @ [lb F t'] = (trace_tine F t')"
               by (metis \<open>\<And>thesis. (\<And>st en. t' = st @ [en] \<Longrightarrow> thesis) \<Longrightarrow> thesis\<close> \<open>st = take (length t' - 1) t' \<and> [en] = drop (length t' - 1) t'\<close> \<open>take (Suc (length t')) (trace_tine F t1) ! length t' = lb F t'\<close> \<open>take (Suc (length t')) (trace_tine F t1) = trace_tine F t'\<close> \<open>tinesP F (t' @ [k])\<close> \<open>trace_tine F' st = trace_tine (Cut_subtree F t' k) st\<close> \<open>trace_tine F' st = trace_tine F st\<close> append1_eq_conv butlast_snoc butlast_tinesP diff_Suc_1 length_append_singleton lessI nth_append_length order_refl sten suc_tine_size_eq_label_list take_Suc_conv_app_nth take_trace_tine_commutative)
             thus ?thesis
               using tmp
               by simp
           qed
          hence "(trace_tine (Cut_subtree F t' k) st)
                @ (trace_tine (Tree (lb F t') [get_subtree F (t' @ [k])]) (0#(drop (Suc (size t')) t1))) = 
                (trace_tine F t') @ tl (trace_tine (get_subtree F t') (drop (size t') t1))"
               using trace_tine_get_subtree_Suc_take_drop_eq_tl
               using \<open>length t' < length t1\<close> \<open>take (length t') t1 = t'\<close> \<open>tinesP F t1\<close> t'k by fastforce
             hence "(trace_tine (Cut_subtree F t' k) st)
                @ (trace_tine (Tree (lb F t') [get_subtree F (t' @ [k])]) (0#(drop (Suc (size t')) t1))) = 
                (trace_tine F t1)"
               by (metis \<open>take (length t') t1 = t'\<close> \<open>tinesP F t1\<close> trace_tine_append_trace_tine_subtree)
             thus ?thesis by simp
           qed
             hence newt1trace_tine:"trace_tine F' (st@(0#(0#(drop (Suc (size t')) t1)))) = trace_tine F t1"
               using newtine
               by auto
             define newt1 where newt1:"newt1 = (st@(0#(0#(drop (Suc (size t')) t1))))"
             hence newt1tines:"newt1 \<in> tines F'"
                using tinesend
                by (metis Cut_subtree_preserve_cut_tines Extend_tree_trace_extended_tines F' \<open>tinesP F (t' @ [k])\<close> butlast_snoc butlast_tinesP mem_Collect_eq sten tines_def)            
             hence newt2tines:"(t2tmp[(size st):= Suc (t2tmp ! (size st))]) \<in> tines F'"
         proof -
           have "size st \<le> size t2 "
             using Suc_le_eq \<open>length t' \<le> length t2\<close> sten by auto
           hence "size st \<le> size t2tmp "
             using t2tmp
             by (simp add: length_list_update)
             thus ?thesis
               using Extend_tree_trace_renewed_tines Ft12 F' \<open>st \<in> tines F\<close>
               tmptines \<open>prefix_list t' t2tmp\<close>
               by (metis Cut_subtree_preserve_cut_tines \<open>t' \<in> tines F\<close> append.assoc append_eq_conv_conj butlast_snoc butlast_tinesP mem_Collect_eq prefix_list_take sten take_prefix_list tines_def)
           qed
          have newt2trace_tine:"trace_tine F' (t2tmp[(size st):= Suc (t2tmp ! (size st))]) = trace_tine F t2"
           proof -
              have "size st \<le> size t2 "
             using Suc_le_eq \<open>length t' \<le> length t2\<close> sten by auto
           hence "size st \<le> size t2tmp "
             using t2tmp
             by (simp add: length_list_update)
             thus ?thesis
               using F' Extend_tree_trace_tine_2 \<open>st \<in> tines F\<close> tmptines
               by (smt Cut_subtree_preserve_cut_tines \<open>length t' \<le> length t2\<close> \<open>prefix_list t' t2tmp\<close> \<open>tinesP F (t' @ [k])\<close> butlast_snoc butlast_tinesP le_eq_less_or_eq length_append_singleton mem_Collect_eq not_less_eq_eq prefix_list_take sten suc_tine_size_eq_label_list take_Suc_conv_app_nth take_prefix_list tinesFt2 tines_def tmptrace_tine)               
           qed
           define newt2 where newt2:"newt2 = (t2tmp[(size st):= Suc (t2tmp ! (size st))])"
           have "viable F w t1"
               using Ft12
               by (simp add: viable_tines_div_pairs_set_def)
             hence "\<forall>h. ((h \<in> honest_indices w \<and> h \<le> last (trace_tine F t1)) \<longrightarrow> depth F h \<le> length t1)"
               using viable.cases by blast
             hence "\<forall>h. (h \<in> honest_indices w \<and> h \<le> last (trace_tine F' newt1)) \<longrightarrow>  
depth F' h \<le> length newt1"
             proof -
               have "\<forall>h. h \<in> honest_indices w \<longrightarrow> depth F h = depth F' h"
                 using F'
                 by (metis Extend_tree_Cut_subtree_preserves_honest_depth \<open>adversarial F t' (honest_indices w)\<close> \<open>viable F w t1\<close> butlast_snoc sten t'ktinesF viable.cases)               
               thus ?thesis
                 by (metis Suc_le_mono \<open>viable F w t1\<close> mem_Collect_eq newt1 newt1tines newt1trace_tine suc_tine_size_eq_label_list tines_def viable.cases)

            qed
            hence v1: "viable F' w newt1"
              using newt1 newt1tines \<open>F' \<turnstile> w\<close>
              using viable.intros by blast
            have "viable F w t2"
               using Ft12
               by (simp add: viable_tines_div_pairs_set_def)
             hence "\<forall>h. ((h \<in> honest_indices w \<and> h \<le> last (trace_tine F t2)) \<longrightarrow> depth F h \<le> length t2)"
               using viable.cases by blast
             hence "\<forall>h. (h \<in> honest_indices w \<and> h \<le> last (trace_tine F' newt2)) \<longrightarrow>  
depth F' h \<le> length  newt2"
             proof -
               have "\<forall>h. h \<in> honest_indices w \<longrightarrow> depth F h = depth F' h"
                 using F'
                 by (metis Extend_tree_Cut_subtree_preserves_honest_depth \<open>adversarial F t' (honest_indices w)\<close> \<open>viable F w t1\<close> butlast_snoc sten t'ktinesF viable.cases)               
               thus ?thesis
                 using newt2trace_tine
                 using \<open>\<forall>h. h \<in> honest_indices w \<and> h \<le> last (trace_tine F t2) \<longrightarrow> depth F h \<le> length t2\<close> newt2
                 by (simp add: t2tmp)

             qed
            have "viable F' w newt2"
               using Ft12 viable_tines_div_pairs_set_def \<open>F' \<turnstile> w\<close> newt2tines
               using \<open>\<forall>h. h \<in> honest_indices w \<and> h \<le> last (trace_tine F' newt2) \<longrightarrow> depth F' h \<le> length newt2\<close> newt2 viable.intros by blast
             have "divs F' newt1 newt2 = 
                    (if lb  F' newt1  \<le> lb F' newt2 
                     then size newt1  - size (longest_common_prefix_tines newt1 newt2) 
                      else 0)"
               by (simp add: divs_def)
             hence "divs F' newt1 newt2 = 
                    (if lb F t1 \<le> lb F t2
                     then size newt1  - size (longest_common_prefix_tines newt1 newt2) 
                      else 0)"
               by (metis \<open>viable F w t1\<close> \<open>viable F w t2\<close> diff_Suc_1 lb_def mem_Collect_eq newt1 newt1tines newt1trace_tine newt2 newt2tines newt2trace_tine suc_tine_size_eq_label_list tines_def viable.simps)
             hence "divs F' newt1 newt2  = size newt1
                    - size (longest_common_prefix_tines newt1 newt2)"
               by (simp add: \<open>lb F t1 \<le> lb F t2\<close>)
             hence "divs F' newt1 newt2 = size t1 - size st"
             proof -
               obtain i where i:"i \<le> size newt1" "i \<le> size newt2" 
                 "take i newt1 = (longest_common_prefix_tines newt1 newt2)"
                 using longest_common_prefix_tines_take_exist by fastforce
               hence "prefix_list (take i newt1) newt1 \<and>  
prefix_list (take i newt2) newt2"
                 by (metis length_take min_absorb2 prefix_list_longest_common_prefix_tines_1 prefix_list_longest_common_prefix_tines_2 prefix_list_take)
               have "i \<ge> size st"
               proof (rule ccontr)
                 assume "\<not> i \<ge> size st"
                 hence "i < size st"
                   by simp
                 hence "take (size st) newt1 \<noteq> take (size st) newt2"
                   using longest_common_prefix_tines_take_lt_size_ne i
                   by (metis Suc_le_mono \<open>length t' \<le> length t1\<close> \<open>length t' \<le> length t2\<close> \<open>viable F w t1\<close> \<open>viable F w t2\<close> le_SucI length_append_singleton length_take mem_Collect_eq min_absorb2 newt1 newt1tines newt1trace_tine newt2 newt2tines newt2trace_tine sten suc_tine_size_eq_label_list tines_def viable.simps)                                  
                 thus "False"
                   by (metis \<open>length t' \<le> length t2\<close> append_eq_conv_conj butlast_snoc butlast_take diff_le_self length_butlast newt1 newt2 order_refl prefix_list_take preft't2 sten t2tmp take_update_cancel)
               qed
               have "i = size st"
               proof (rule ccontr)
                 assume "\<not> i = size st"
                 hence "i > size st"
                   using \<open>length st \<le> i\<close> le_eq_less_or_eq by blast
                 hence "i = Suc (size st)"
                   by (metis Zero_not_Suc append_take_drop_id i(1) i(2) i(3) length_list_update length_take less_le_trans min_absorb2 newt1 newt2 nth_append nth_append_length nth_list_update_eq prefix_list_longest_common_prefix_tines_2 prefix_list_nth)
                 hence "take i newt1 = st@[0]"
                   using newt1 by auto
                 have "take i newt2  = st @[Suc (t2tmp ! (size st))]"
                   using newt2 t2tmp
                   by (metis Zero_not_Suc \<open>i = Suc (length st)\<close> \<open>length st < i\<close> \<open>prefix_list t' t2tmp\<close> \<open>take i newt1 = st @ [0]\<close> i(3) length_append_singleton nth_append_length nth_list_update_eq prefix_list_longest_common_prefix_tines_2 prefix_list_take sten take_update_swap)
                 hence "take i newt2 \<noteq> take i newt1"
                   using \<open>take i newt1 = st @ [0]\<close> by auto
                 thus "False" using i
                   by (metis length_take longest_common_prefix_tines_take_exist min_absorb2)
               qed
               hence "(longest_common_prefix_tines newt1 newt2) = st"
                 using i newt1 newt2 t2tmp by simp
               have "lb F' newt1 \<le> lb F' newt2"
                 using \<open>lb F t1 \<le> lb F t2\<close> newt1trace_tine newt2trace_tine
                 by (metis \<open>divs F' newt1 newt2 = (if lb F t1 \<le> lb F t2 then length newt1 - length (longest_common_prefix_tines newt1 newt2) else 0)\<close> \<open>divs F' newt1 newt2 = (if lb F' newt1 \<le> lb F' newt2 then length newt1 - length (longest_common_prefix_tines newt1 newt2) else 0)\<close> \<open>i = length st\<close> \<open>length t' < length (trace_tine F t1)\<close> \<open>longest_common_prefix_tines newt1 newt2 = st\<close> diff_is_0_eq dual_order.antisym i(1) length_append_singleton mem_Collect_eq newt1 newt1tines not_le order_refl sten suc_tine_size_eq_label_list tines_def) 
               hence "divs F' newt1 newt2 =
length newt1 - length (longest_common_prefix_tines newt1 newt2)"
                 by (simp add: \<open>divs F' newt1 newt2 = length newt1 - length (longest_common_prefix_tines newt1 newt2)\<close>)
  thus ?thesis
    using \<open>longest_common_prefix_tines newt1 newt2 = st\<close> diff_Suc_1 newt1 newt1tines newt1trace_tine suc_tine_size_eq_label_list tinesFt1 tines_def by fastforce
             qed
             have "size t1 - size st > size t1 - size t2"
               using \<open>length t' \<le> length t2\<close> t1gtt' sten by auto 
             have "divs F'  newt1 newt2  > size t1 - size t2"
               by (simp add: \<open>divs F'  newt1 newt2 = length t1 - length st\<close> \<open>length t1 - length t2 < length t1 - length st\<close>)
             hence preft:"divs F' newt1 newt2  > divs F t1 t2"
                 by (metis Suc_diff_le \<open>divs F' newt1 newt2 = length t1 - length st\<close> \<open>lb F t1 \<le> lb F t2\<close> \<open>length t' \<le> length t1\<close> diff_Suc_Suc divs_def length_append_singleton lessI sten t')
             hence ft:"div_tines F' newt1 newt2 > divs F t1 t2"
               using div_tines_def by force
             hence "divs F' newt2 newt1 =
(if lb F' newt2  \<le> lb F' newt1 
then size newt2 - size (longest_common_prefix_tines newt2 newt1) else 0)"
               using divs_def by auto
          hence tmp:"divs F' newt2 newt1  =
(if lb F t2 \<le> lb F t1 
then size newt2 - size st else 0)"
               by (metis \<open>divs F' newt1 newt2 = length newt1 - length (longest_common_prefix_tines newt1 newt2)\<close> \<open>divs F' newt1 newt2 = length t1 - length st\<close> \<open>length t1 - length t2 < length t1 - length st\<close> \<open>viable F w t2\<close> diff_0_eq_0 diff_Suc_1 diff_diff_cancel lb_def longest_common_prefix_tines_commute mem_Collect_eq nat_le_linear newt1 newt1tines newt1trace_tine newt2 newt2tines newt2trace_tine not_less suc_tine_size_eq_label_list tinesFt1 tines_def viable.simps zero_less_diff)           
           hence  tmpp:"lb F t2 \<le> lb F t1 \<longrightarrow> divs F' newt2 newt1 = Suc (divs F t2 t1)" 
                 using \<open>length t1 - length t2 < length t1 - length st\<close>
                 by (smt Suc_diff_le \<open>length t' \<le> length t2\<close> diff_Suc_Suc divs_def length_append_singleton length_list_update lessI longest_common_prefix_tines_commute newt2 sten t' t2tmp)
        
 hence "divs F' newt1 newt2 =
(if lb F' newt1  \<le> lb F' newt2 
then size newt1 - size (longest_common_prefix_tines newt1 newt2) else 0)"
               using divs_def by auto
          hence "divs F' newt1 newt2  =
(if lb F t1 \<le> lb F t2 
then size newt1 - size st else 0)"
            by (metis \<open>divs F' newt1 newt2 = length t1 - length st\<close> \<open>lb F t1 \<le> lb F t2\<close> diff_Suc_1 mem_Collect_eq newt1 newt1tines newt1trace_tine suc_tine_size_eq_label_list tinesFt1 tines_def)
   hence  "lb F t2 = lb F t1 \<longrightarrow> divs F' newt1 newt2  = Suc (divs F t1 t2)"
     by (metis Suc_diff_le \<open>divs F' newt1 newt2 = length t1 - length st\<close> \<open>lb F t1 \<le> lb F t2\<close> \<open>length t' \<le> length t1\<close> diff_Suc_Suc divs_def length_append_singleton sten t')
    hence  "lb F t2 = lb F t1 \<longrightarrow> div_tines F' newt1 newt2  = max (Suc (divs F t1 t2)) (Suc (divs F t2 t1)) "
      using tmpp
      by (simp add: div_tines_def)
hence  "lb F t2 = lb F t1 \<longrightarrow> div_tines F' newt1 newt2  = Suc (max (divs F t1 t2) (divs F t2 t1)) "
      using tmpp
      by (simp add: div_tines_def)

hence  "lb F t2 = lb F t1 \<longrightarrow> div_tines F' newt1 newt2  >  divs F t1 t2"
  using tmpp
  using ft by blast
 hence "lb F t2 = lb F t1 \<longrightarrow>  div_f F' w > divs F t1 t2"
             using newt1tines newt2tines div_tines_le_div_f v1 \<open>viable F' w newt2\<close>
             by (smt leD le_trans nat_less_le newt2)
           hence "lb F t2 = lb F t1 \<longrightarrow>  div_f F' w > div_s w"
             using div_sw
             by (simp add: divs_def t') 
           hence tmpeq:"lb F t2 = lb F t1 \<longrightarrow> div_f F' w > Max ((\<lambda>x. div_f x w) ` (forks_set w))"
             using div_s_def
             by auto 
               
           have "lb F t2 > lb F t1 \<longrightarrow> divs F' newt2 newt1  = 0"
              using tmp by simp
           hence "lb F t2 > lb F t1 \<longrightarrow> div_tines F' newt1 newt2 > divs F t1 t2"
             using preft ft
             by blast
           hence "lb F t2 > lb F t1 \<longrightarrow>  div_f F' w > divs F t1 t2"
             using newt1tines newt2tines div_tines_le_div_f v1 \<open>viable F' w newt2\<close>
             by (smt leD le_trans nat_less_le newt2)
           hence "lb F t2 > lb F t1 \<longrightarrow>  div_f F' w > div_s w"
             using div_sw
             by (simp add: divs_def t') 
           hence tmpgt:"lb F t2 > lb F t1 \<longrightarrow> div_f F' w > Max ((\<lambda>x. div_f x w) ` (forks_set w))"
             using div_s_def
             by auto 
           have "F' \<in> forks_set w"
             using F'
             using \<open>F' \<turnstile> w\<close> forks_set_def by auto
           hence "div_f F' w \<in> ((\<lambda>x. div_f x w) ` (forks_set w))"
             by simp
           thus ?thesis
             using tmpgt tmpeq \<open>lb F t1 \<le> lb F t2\<close>
             using div_f_set_nonempty finite_div_f_set by fastforce
         qed
       qed
       hence "honest F t' (honest_indices w)"
         by simp
       hence "\<alpha>' \<in> honest_indices w"
         using \<open>\<alpha>' = lb F t'\<close>
         using \<open>t' \<in> tines F\<close> honest_def lb_in_F_last_trace_tine tines_def by auto
       obtain F0 where F0:"F0 \<le> F" "(\<forall>a. a \<le> \<alpha>' \<longrightarrow> count (mset_of_tree F0) a = count (mset_of_tree F) a)"
         by blast
       hence "strictly_inc F0"
         using prefix_strictly_inc Ft12
         by (simp add: Ft12 fork_F forks_def viable_tines_div_pairs_set_def)
       have "\<And>t. t \<in> tines F \<and> last (trace_tine F t) \<le> \<alpha>' \<Longrightarrow> (\<exists>tt \<in> tines F0. tine_prefix F0 F tt t \<and> size tt = size t)"
       proof -
         fix t assume "t \<in> tines F \<and> last (trace_tine F t) \<le> \<alpha>'"
         define a where "a = last (trace_tine F t)"
         hence "count (mset_of_tree F) a = count (mset_of_tree F0) a"
           using F0
           using \<open>t \<in> tines F \<and> last (trace_tine F t) \<le> \<alpha>'\<close> by auto
         thus "(\<exists>tt \<in> tines F0. tine_prefix F0 F tt t \<and> size tt = size t)"
           using  exist_tine_prefix_eq_size_prefix
           by (metis F0(1) \<open>a = last (trace_tine F t) \<close> \<open>t \<in> tines F \<and> last (trace_tine F t) \<le> \<alpha>'\<close> mem_Collect_eq tines_def)
       qed
       have ltalphalessdepth:"\<forall>n. n < \<alpha>' \<longrightarrow> (\<forall>t. (t \<in> tines F0 \<and> lb F0 t = n) \<longrightarrow> size t < size t')"
       proof (rule ccontr)
         assume "\<not> (\<forall>n. n < \<alpha>' \<longrightarrow> (\<forall>t. (t \<in> tines F0 \<and> lb F0 t = n) \<longrightarrow> size t < size t'))"
         hence "\<exists>n. n < \<alpha>' \<and> (\<exists>t. (t \<in> tines F0 \<and> lb F0 t = n) \<and> size t \<ge> size t')"
           using not_less by blast
         then obtain n t0tmp where t0tmp: "n < \<alpha>'" "t0tmp \<in> tines F0" "lb F0 t0tmp = n" "size t0tmp \<ge> size t'"
           by auto
         hence sil:"strictly_inc_list (trace_tine F0 t0tmp)"
           using \<open>strictly_inc F0\<close> strictly_inc_imp_strictly_inc_tine strictly_inc_tine_def by blast
         define t0 where t0:"t0  = take (size t') t0tmp"
         hence x:"(trace_tine F0 t0tmp) ! (size t') \<le> (trace_tine F0 t0tmp) !  (size t0tmp)" 
           using t0tmp sil
           using le_eq_less_or_eq strictly_inc_list_as_def tines_def by auto
         have t0t':"size t0 = size t'"
           using t0
           by (simp add: min_absorb2 t0tmp(4))
         have "take (Suc (size t')) (trace_tine F0 t0tmp) = trace_tine F0 t0"
          using t0 t0tmp(2) t0tmp(4) take_trace_tine_commutative tines_def by auto        
        hence "(trace_tine F0 t0tmp) ! (size t') \<le> n"
          using x
          using lb_def t0tmp(3) by auto
        hence "last (trace_tine F0 (take (size t') t0tmp)) \<le> n"
          by (metis \<open>take (Suc (length t')) (trace_tine F0 t0tmp) = trace_tine F0 t0\<close> le_imp_less_Suc mem_Collect_eq suc_tine_size_eq_label_list t0 t0tmp(2) t0tmp(4) take_Suc_nth tines_def)
        hence "last (trace_tine F0 t0) \<le> n"
          using t0 by simp
         thus "False"
         proof (cases t')
           case Nil
           hence "lb F t' = label F"
             by (smt lb_def list.simps(4) list.size(3) nth_Cons_0 rtree.exhaust_sel trace_tine.simps)
           hence "\<alpha>' = label F"
             using alpha
             by auto
           hence "\<alpha>' = 0"
             using Ft12 fork_root0
           proof - 
             have "F \<turnstile> w"
               using Ft12 viable_tines_div_pairs_set_def by auto
             hence "label F = 0"
               using fork_F forks_def by blast
             then show ?thesis
               by (simp add: \<open>\<alpha>' = label F\<close>) 
           qed
           thus ?thesis
             using \<open>n < \<alpha>'\<close> by auto
         next
           case (Cons kk tt)
           thus ?thesis 
           proof -
             have"size t0 > 0"
               using le_zero_eq t0 t0tmp(4)
               by (simp add: local.Cons min_absorb2) 
             then obtain t where t:"tine_prefix F0 F t0 t" "size t = size t0"
               using prefix_tree_exist_same_size_tine t0 F0
               using tines_def
               by (metis (full_types) mem_Collect_eq t0tmp(2) take_tine_is_tine) 
             hence "t \<in> tines F"
               using tine_prefix_imp_both_tines tines_def by auto
             hence "tine_providing_label_prefix F0 F t0 t"
               using t
               using tine_prefix_means_label_prefix by blast
             then obtain ll where "trace_tine F0 t0 @ ll = trace_tine F t"
               using tine_providing_label_prefix_def by auto
             hence "trace_tine F0 t0 = trace_tine F t"
               by (metis add_diff_cancel_left' append_self_conv cancel_comm_monoid_add_class.diff_cancel length_append length_greater_0_conv suc_tine_size_eq_label_list t(1) t(2) tine_prefix_imp_both_tines)
             obtain st en where "t = st@[en]"
               using t
               by (metis \<open>0 < length t0\<close> length_greater_0_conv rev_exhaust)
             obtain st' en' where "t' = st'@[en'] "
               using local.Cons rev_exhaust by blast
             define k where "k = t1 ! size t'"
             hence "prefix_list (t'@[k]) t1"
               by (metis Suc_leI \<open>take (length t') t1 = t'\<close> hd_drop_conv_nth length_append_singleton t1gtt' take_hd_drop take_prefix_list)
             then obtain x where x:"take x t1 = (t'@[k])"
               using prefix_list_take by blast
             have "viable F w t1"
               using Ft12
               by (simp add: viable_tines_div_pairs_set_def)
             hence "t1 \<in> tines F"
               using viable.cases by blast
             hence "tinesP F (take x t1)"
               using take_tine_is_tine tine_prefix_imp_both_tines by blast
             hence "t'@[k] \<in> tines F"
               by (simp add: tines_def x)
             have "size t = size t'"
               using t0t' t
               by auto
             hence ttines:"t \<in> tines (Cut_subtree F t' k) \<and> trace_tine (Cut_subtree F t' k) t = trace_tine F t"
               using Cut_subtree_tines_1 Cut_subtree_trace_tine_1  \<open>t' \<in> tines F\<close>
               by (metis \<open>t \<in> tines F\<close> diff_is_0_eq diff_self_eq_0 tine_prefix_imp_both_tines tine_prefix_refl)           
             define Ftmp where Ftmp:"Ftmp = Extend_tree (Cut_subtree F t' k) t (get_subtree F (t'@[k]))"
           hence "Ftmp \<turnstile> w"
           proof - 
             have 0:"label F = 0" using Fw
               using fork_F forks_def
               by blast
             hence 1:"label Ftmp = 0" using Fw Extend_tree_same_label Cut_subtree_same_label
               by (simp add: Ftmp)
             have 2:"limit Ftmp (size w)"
               using Extend_tree_Cut_subtree_preserves_limit_2 Ftmp Ft12 \<open>t'@[k] \<in> tines F\<close> ttines Fw by simp
             have 3:"strictly_inc Ftmp"
             proof - 
               have "lb (Cut_subtree F t' k) t < \<alpha>'"
                 by (metis \<open>length t = length t'\<close> \<open>take (Suc (length t')) (trace_tine F0 t0tmp) = trace_tine F0 t0\<close> \<open>trace_tine F0 t0 = trace_tine F t\<close> \<open>trace_tine F0 t0tmp ! length t' \<le> n\<close> add_lessD1 lb_def le_Suc_ex lessI nth_take t0tmp(1) ttines)
               hence "lb (Cut_subtree F t' k) t < lb F t'"
                 by (simp add: alpha)
               have "strictly_inc_list (trace_tine F (t'@[k]))"
                 using \<open>t'@[k] \<in> tines F\<close>
                 by (metis \<open>\<And>thesis. (\<And>x. take x t1 = t' @ [k] \<Longrightarrow> thesis) \<Longrightarrow> thesis\<close> \<open>strictly_inc_list (trace_tine F t1)\<close> \<open>t1 \<in> tines F\<close> nat_le_linear strictly_inc_list_take take_all take_trace_tine_commutative tine_prefix_imp_both_tines tine_prefix_refl)
               hence "trace_tine F (t'@[k]) ! size t' < trace_tine F (t'@[k]) ! size (t'@[k])"
                 using \<open>tinesP F (take x t1)\<close> strictly_inc_list_as_def x by auto
               hence "(take (size (t'@[k])) (trace_tine F (t'@[k])))  ! size t' < trace_tine F (t'@[k]) ! size (t'@[k])"
                 by simp
               hence "(trace_tine F (take (size t') (t'@[k])))  ! size t' < trace_tine F (t'@[k]) ! size (t'@[k])"
                 using take_trace_tine_commutative
                 by (metis \<open>tinesP F (take x t1)\<close> length_append_singleton lessI take_trace_tine_commutative_2 x)
               hence "lb F t' < lb F (t'@[k])"
                 by (simp add: lb_def)
               hence "lb (Cut_subtree F t' k) t < lb F (t'@[k])"
                 using Suc_lessD \<open>lb (Cut_subtree F t' k) t < lb F t'\<close> less_trans_Suc by blast
               thus ?thesis using Extend_tree_Cut_subtree_preserves_strictly_inc_2 Ftmp Ft12 \<open>t'@[k] \<in> tines F\<close> ttines Fw  by simp
             qed
             have 4:"\<forall>h \<in> honest_indices w. count (mset_of_tree Ftmp) h = 1"
               using Extend_tree_Cut_subtree_preserves_unique_honest_2 Ftmp Ft12 \<open>t'@[k] \<in> tines F\<close> ttines Fw
               by blast
             have "\<forall>h \<in> honest_indices w. depth Ftmp h = depth F h"
               using Extend_tree_Cut_subtree_preserves_honest_depth_2 Ftmp Ft12 \<open>t'@[k] \<in> tines F\<close> ttines Fw \<open>size t = size t'\<close>
               by auto
             hence "\<forall>i \<in> honest_indices w. \<forall> j \<in> honest_indices w. i < j \<longrightarrow> depth Ftmp i  < depth Ftmp j"
               using Extend_tree_Cut_subtree_preserves_honest_depth_2 Ftmp Ft12 \<open>t'@[k] \<in> tines F\<close> ttines Fw \<open>size t = size t'\<close>
               by (metis fork_F forks_def)
             thus ?thesis
               using 0 1 2 3 4
               by (simp add: fork.intros forks_def)
           qed 
           have "(drop (size (t'@[k])) t1) \<in> tines (get_subtree F (t'@[k]))"
             using \<open>t1 \<in> tines F\<close>
             by (metis append_eq_conv_conj append_take_drop_id get_subtree_tines_append_in_tines mem_Collect_eq tines_def x)
           hence tinesFtmp:"(t@ 0 # (drop (size (t'@[k])) t1)) \<in> tines Ftmp \<and> trace_tine Ftmp (t@ 0 # (drop (size (t'@[k])) t1)) 
                  = trace_tine (Cut_subtree F t' k) t @ trace_tine (get_subtree F (t'@[k])) (drop (size (t'@[k])) t1)"
             using ttines Extend_tree_trace_extended_tines Ftmp \<open>t \<in> tines F\<close>  Extend_tree_trace_tine_4
             by (simp add: tines_def)
           hence tmp:"size (t@ 0 # (drop (size (t'@[k])) t1)) = size t1 \<and> last (trace_tine Ftmp (t@ 0 # (drop (size (t'@[k])) t1))) = lb F t1 "
             by (smt Suc_inject \<open>length t = length t'\<close> \<open>length t' \<le> length t1\<close> \<open>length t1 < length (trace_tine F t1)\<close> \<open>prefix_list (t' @ [k]) t1\<close> \<open>t1 \<in> tines F\<close> \<open>take (length t') t1 = t'\<close> \<open>tinesP F (take x t1)\<close> butlast_snoc butlast_tinesP last_appendR lb_def le_eq_less_or_eq length_append length_append_singleton mem_Collect_eq prefix_list_take suc_tine_size_eq_label_list take_Suc_nth take_all tines_def trace_tine_append_trace_tine_subtree trace_tine_get_subtree_Suc_take_drop_eq_tl trace_tine_non_empty ttines x)
           hence "viable Ftmp w (t@ 0 # (drop (size (t'@[k])) t1))"
           proof -
             have "last (trace_tine Ftmp (t@ 0 # (drop (size (t'@[k])) t1))) = lb F t1"
               using tmp by simp
             hence 0:" last (trace_tine F t1) = last (trace_tine Ftmp (t@ 0 # (drop (size (t'@[k])) t1)))"
               using lb_def
               by (metis Suc_leI \<open>length t1 < length (trace_tine F t1)\<close> \<open>t1 \<in> tines F\<close> suc_tine_size_eq_label_list take_Suc_nth take_all tine_prefix_imp_both_tines tine_prefix_refl) 
             have "\<forall>h. (h \<in> honest_indices w \<and> h \<le> last (trace_tine F t1)) \<longrightarrow> depth F h \<le> length t1"
               using \<open>viable F w t1\<close>
               using viable.cases by blast
             hence "\<forall>h. (h \<in> honest_indices w \<and> h \<le> last (trace_tine Ftmp (t@ 0 # (drop (size (t'@[k])) t1)))) \<longrightarrow> depth F h \<le> length  (t@ 0 # (drop (size (t'@[k])) t1))"
               using tmp 0 by simp
             hence "\<forall>h. (h \<in> honest_indices w \<and> h \<le> last (trace_tine Ftmp (t@ 0 # (drop (size (t'@[k])) t1)))) \<longrightarrow> depth Ftmp h \<le> length  (t@ 0 # (drop (size (t'@[k])) t1))"
               using Extend_tree_Cut_subtree_preserves_honest_depth_2 Ftmp Ft12 \<open>t'@[k] \<in> tines F\<close> ttines Fw \<open>size t = size t'\<close>
               by auto
             thus ?thesis 
               using \<open>Ftmp \<turnstile> w\<close> tinesFtmp
               using viable.intros by blast
           qed
           have "t \<noteq> t'"
             by (metis \<open>take (Suc (length t')) (trace_tine F t1) ! length t' = lb F t'\<close> \<open>take (Suc (length t')) (trace_tine F t1) = trace_tine F (take (length t') t1)\<close> \<open>take (Suc (length t')) (trace_tine F0 t0tmp) = trace_tine F0 t0\<close> \<open>take (length t') t1 = t'\<close> \<open>trace_tine F0 t0 = trace_tine F t\<close> \<open>trace_tine F0 t0tmp ! length t' \<le> n\<close> alpha diff_is_0_eq' lessI less_irrefl_nat nth_take t0tmp(1) zero_less_diff)
           hence "\<not> prefix_list t t2tmp"
             by (metis \<open>length t = length t'\<close> \<open>prefix_list t' t2tmp\<close> prefix_list_take)
           hence t2tmptrace_tine:"trace_tine Ftmp t2tmp = trace_tine (Cut_subtree F t' k) t2tmp \<and> t2tmp \<in> tines Ftmp"
           proof -
              have "size t < size t2 "
             using Suc_le_eq \<open>length t' < length t2\<close>
             by (simp add: \<open>length t = length t'\<close>)
           hence "size t < size t2tmp "
             by (simp add: t2tmp)
             thus ?thesis
               using Ftmp \<open>t \<in> tines F\<close> tmptines
               using Extend_tree_trace_tine_3 \<open>\<not> prefix_list t t2tmp\<close> \<open>k = t1 ! length t'\<close> k tine_prefix_imp_both_tines ttines  Extend_tree_trace_remaining_tines
               by (simp add: tines_def)
           qed
          
           have "viable F w t2"
               using Ft12
               by (simp add: viable_tines_div_pairs_set_def)
             hence tmp:"\<forall>h. ((h \<in> honest_indices w \<and> h \<le> last (trace_tine F t2)) \<longrightarrow> depth F h \<le> length t2)"
               using viable.cases by blast
             hence "\<forall>h. (h \<in> honest_indices w \<and> h \<le> last (trace_tine Ftmp t2tmp)) \<longrightarrow>  
                  depth Ftmp h \<le> length  t2tmp"
             proof -
               have 0:"\<forall>h. h \<in> honest_indices w \<longrightarrow> depth F h = depth Ftmp h"
                 using Ftmp
                 using Extend_tree_Cut_subtree_preserves_honest_depth_2 Fw \<open>length t = length t'\<close> \<open>t' @ [k] \<in> tines F\<close> ttines by auto
               have "size t2 = size t2tmp"
                 by (simp add: t2tmp)
               thus ?thesis 
                 using tmptines tmptrace_tine  tmp t2tmptrace_tine 0 
                 by (simp add: \<open>k = t1 ! length t'\<close> k)
             qed
             hence "viable Ftmp w t2tmp"
               using \<open>Ftmp \<turnstile> w\<close> t2tmptrace_tine viable.simps by blast
             define newt1' newt2' where newt12':"newt1' = (t@ 0 # (drop (size (t'@[k])) t1))" "newt2' = t2tmp"
             hence "last (trace_tine F t1) = last (trace_tine Ftmp newt1')"
               by (metis \<open>k = t1 ! length t'\<close> get_subtree_append k last_append last_in_tine_is_label_in_subtree length_append_singleton t'k tinesFt1 tinesFtmp trace_tine_non_empty)
             hence "lb F t1 = lb Ftmp newt1'"
               by (metis last_ConsR last_is_nth_size lb_def mem_Collect_eq newt12'(1) nth_Cons_Suc suc_tine_size_eq_label_list tinesFt1 tinesFtmp tines_def trace_tine_non_empty)
             have "lb F t2 = lb Ftmp newt2'"
               using newt12'
               using \<open>k = t1 ! length t'\<close> k lb_def t2tmp t2tmptrace_tine tmptrace_tine by auto
             obtain i where i:"i \<le> size newt1'" "i \<le> size newt2'" 
                 "take i newt1' = (longest_common_prefix_tines newt1' newt2')" "take i newt2' = (longest_common_prefix_tines newt1' newt2')"
                 using longest_common_prefix_tines_take_exist by fastforce
               hence "prefix_list (take i newt1') newt1' \<and>  
prefix_list (take i newt2') newt2'"
                 by (metis length_take min_absorb2 prefix_list_longest_common_prefix_tines_1 prefix_list_longest_common_prefix_tines_2 prefix_list_take)               
               have "i < size t"
               proof (rule ccontr)
                 assume "\<not> i < size t"
                 hence "i \<ge> size t"
                   by simp
                 have "take i newt1' = take i newt2'"
                   using i
                   by simp
                 have "size (take i newt1') = i"
                   using i
                   by (metis length_take min_absorb2)
                 hence "take (size t) newt1' = take (size t) newt2'"
                   using \<open>i \<ge> size t\<close> i
                   by (smt \<open>\<not> i < length t\<close> le_trans less_trans linorder_neqE_nat nth_take nth_take_lemma)
                 hence "t = t'"
                   by (simp add: \<open>length t = length t'\<close> \<open>prefix_list t' t2tmp\<close> newt12'(1) newt12'(2) prefix_list_take)
                 thus "False"
                   using \<open>t \<noteq> t'\<close> by blast
         qed
        
         hence "size (longest_common_prefix_tines newt1' newt2') < size t"
           by (metis i(1) i(3) length_take min_absorb2)
               have "lb Ftmp newt1' \<le> lb Ftmp newt2'"
                 using \<open>lb F t1 \<le> lb F t2\<close>
                 by (simp add: \<open>lb F t1 = lb Ftmp newt1'\<close> \<open>lb F t2 = lb Ftmp newt2'\<close>) 
             
             have "divs Ftmp newt1' newt2' =
                    (if lb  Ftmp newt1'  \<le> lb Ftmp newt2' 
                     then size newt1'  - size (longest_common_prefix_tines newt1' newt2') 
                      else 0)"
               by (simp add: divs_def)
             hence "divs Ftmp newt1' newt2' = 
                    (if lb F t1 \<le> lb F t2
                     then size newt1'  - size (longest_common_prefix_tines newt1' newt2') 
                      else 0)"
               using \<open>lb F t1 = lb Ftmp newt1'\<close> \<open>lb F t2 = lb Ftmp newt2'\<close> by presburger
             hence "divs Ftmp newt1' newt2'  = size newt1'
                    - size (longest_common_prefix_tines newt1' newt2')"
               by (simp add: \<open>lb F t1 \<le> lb F t2\<close>)
             hence "divs Ftmp newt1' newt2' > size t1 - size t"
             proof -
               
               have "divs Ftmp newt1' newt2' =
length newt1' - length (longest_common_prefix_tines newt1' newt2')"
    by (simp add: \<open>divs Ftmp newt1' newt2' = length newt1' - length (longest_common_prefix_tines newt1' newt2')\<close>)
  thus ?thesis
    by (smt Suc_less_eq \<open>i < length t\<close> \<open>k = t1 ! length t'\<close> \<open>length t = length t'\<close> \<open>length t' < length (trace_tine F t1)\<close> \<open>take (length t') t1 = t'\<close> \<open>tinesP F (take x t1)\<close> butlast_snoc butlast_tinesP diff_Suc_Suc diff_less_mono2 i(1) i(3) k length_append length_append_singleton length_take less_trans_Suc mem_Collect_eq min_absorb2 newt12'(1) suc_tine_size_eq_label_list t'k t1gtt' tinesFt1 tinesFtmp tines_def trace_tine_append_trace_tine_subtree trace_tine_get_subtree_Suc_take_drop_eq_tl ttines x)
    
         qed
         have "size t1 - size t > size t1 - size t2"
           by (simp add: \<open>length t = length t'\<close> diff_less_mono2 t1gtt' t2gtt')
         have "divs Ftmp  newt1' newt2'  > size t1 - size t2"
           using \<open>length t1 - length t < divs Ftmp newt1' newt2'\<close> \<open>length t1 - length t2 < length t1 - length t\<close> less_trans by blast
         hence preft:"divs Ftmp newt1' newt2'  > divs F t1 t2"
            using \<open>lb F t1 \<le> lb F t2\<close> \<open>length t = length t'\<close> \<open>length t1 - length t < divs Ftmp newt1' newt2'\<close> divs_def t' by auto
        hence ft:"div_tines Ftmp newt1' newt2' > divs F t1 t2"
          using div_tines_def by force
             hence "divs Ftmp newt2' newt1' =
(if lb Ftmp newt2'  \<le> lb Ftmp newt1' 
then size newt2' - size (longest_common_prefix_tines newt2' newt1') else 0)"
               using divs_def by auto
          hence tmp:"divs Ftmp newt2' newt1'  =
(if lb F t2 \<le> lb F t1 
then size newt2' - i else 0)"
            by (metis \<open>lb F t1 = lb Ftmp newt1'\<close> \<open>lb F t2 = lb Ftmp newt2'\<close> i(1) i(3) length_take longest_common_prefix_tines_commute min_absorb2)
          hence  tmpp:"lb F t2 \<le> lb F t1 \<longrightarrow> divs Ftmp newt2' newt1' = size newt2' - i "
            by auto
          hence  "lb F t2 \<le> lb F t1 \<longrightarrow> divs Ftmp newt2' newt1' > size t2 - size t"
            using  \<open>i < size t\<close>

          proof - 
            have "size t2= size newt2'"
              using newt12' t2tmp
              by simp
            hence "size newt2' - i  = size t2 - i"
              by simp
            hence "size newt2' - i  > size t2 - size t" using \<open>i < size t\<close>
              using \<open>length t = length t'\<close> t2gtt' by linarith
            thus ?thesis using tmpp
              by simp
          qed
 hence tmpp:"divs Ftmp newt1' newt2' =
(if lb Ftmp newt1'  \<le> lb Ftmp newt2' 
then size newt1' - size (longest_common_prefix_tines newt1' newt2') else 0)"
               using divs_def by auto
          hence "divs Ftmp newt1' newt2'  =
(if lb F t1 \<le> lb F t2 
then size newt1' - i else 0)"
     by (metis \<open>lb F t1 = lb Ftmp newt1'\<close> \<open>lb F t2 = lb Ftmp newt2'\<close> i(1) i(3) length_take longest_common_prefix_tines_commute min_absorb2)
   hence  "lb F t2 = lb F t1 \<longrightarrow> divs Ftmp newt1' newt2'  > size t1 - size t"
   using  \<open>i < size t\<close>

          proof - 
            have "size t1= size newt1'"
              using newt12'
              by (metis \<open>length t = length t'\<close> add_Suc_shift append_take_drop_id length_Cons length_append length_append_singleton t'k)

            hence "size newt1' - i  = size t1 - i"
              by simp
            hence "size newt1' - i  > size t1 - size t" using \<open>i < size t\<close>
              using \<open>length t = length t'\<close> t1gtt' by linarith
            thus ?thesis using tmpp i
              using \<open>length t1 - length t < divs Ftmp newt1' newt2'\<close> by blast

          qed

    hence  "lb F t2 = lb F t1 \<longrightarrow> div_tines Ftmp newt1' newt2'  > max (divs F t1 t2) (divs F t2 t1) "
      using \<open>lb F t2 \<le> lb F t1 \<longrightarrow> length t2 - length t < divs Ftmp newt2' newt1'\<close> \<open>length t = length t'\<close> div_tines_def divs_def longest_common_prefix_tines_commute t' by auto
    hence  "lb F t2 = lb F t1 \<longrightarrow> div_tines Ftmp newt1' newt2'  >  divs F t1 t2"
      using ft by blast
    have tmp:"div_f Ftmp w \<ge> div_tines Ftmp newt1' newt2' \<and> div_f Ftmp w \<ge> div_tines Ftmp newt2' newt1'"
using newt12' div_tines_le_div_f \<open>viable Ftmp w t2tmp\<close> \<open>viable Ftmp w (t@ 0 # (drop (size (t'@[k])) t1))\<close>
    tinesFtmp t2tmptrace_tine
  by blast 
    hence "lb F t2 = lb F t1 \<longrightarrow>  div_f Ftmp w > divs F t1 t2"
      using \<open>lb F t2 = lb F t1 \<longrightarrow> divs F t1 t2 < div_tines Ftmp newt1' newt2'\<close> dual_order.strict_trans1 by blast
       
           hence "lb F t2 = lb F t1 \<longrightarrow>  div_f Ftmp w > div_s w"
             using div_sw
             by (simp add: divs_def t') 
           hence tmpeq:"lb F t2 = lb F t1 \<longrightarrow> div_f Ftmp w > Max ((\<lambda>x. div_f x w) ` (forks_set w))"
             using div_s_def
             by auto 
               
           have "lb F t2 > lb F t1 \<longrightarrow> divs Ftmp newt2' newt1'  = 0"
             by (simp add: \<open>divs Ftmp newt2' newt1' = (if lb Ftmp newt2' \<le> lb Ftmp newt1' then length newt2' - length (longest_common_prefix_tines newt2' newt1') else 0)\<close> \<open>lb F t1 = lb Ftmp newt1'\<close> \<open>lb F t2 = lb Ftmp newt2'\<close>)
              
           hence "lb F t2 > lb F t1 \<longrightarrow> div_tines Ftmp newt1' newt2' > divs F t1 t2"
             using preft ft
             by blast
           hence "lb F t2 > lb F t1 \<longrightarrow>  div_f Ftmp w > divs F t1 t2"
             using tmp by linarith 
           hence "lb F t2 > lb F t1 \<longrightarrow>  div_f Ftmp w > div_s w"
             using div_sw
             by (simp add: divs_def t') 
           hence tmpgt:"lb F t2 > lb F t1 \<longrightarrow> div_f Ftmp w > Max ((\<lambda>x. div_f x w) ` (forks_set w))"
             using div_s_def
             by auto 
           have "Ftmp \<in> forks_set w"
             using Ftmp
             using \<open>Ftmp \<turnstile> w\<close> forks_set_def by auto
           hence "div_f Ftmp w \<in> ((\<lambda>x. div_f x w) ` (forks_set w))"
             by simp
           thus ?thesis
             using tmpgt tmpeq \<open>lb F t1 \<le> lb F t2\<close>
             using div_f_set_nonempty finite_div_f_set by fastforce
         qed
       qed
     qed
     

       have 14:"\<And>h. h \<in> honest_indices w \<and> h < \<beta>\<Longrightarrow> depth F h \<le> min (size t1) (size t2)"
       proof -
         fix h 
         assume assm:"h \<in> honest_indices w \<and> h < \<beta>"
         hence "h < (if \<exists>h \<in> honest_indices w. h \<ge> lb F t2 then Min {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}  else Suc (size w))"
           using beta by blast
         hence "(\<exists>h \<in> honest_indices w. h \<ge> lb F t2) \<longrightarrow>  h < Min {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}"
           by smt
         hence "(\<exists>h \<in> honest_indices w. h \<ge> lb F t2) \<longrightarrow>  (\<exists>x. x = Min {h. h \<in> honest_indices w \<and> h \<ge> lb F t2})"
           by blast
         obtain th where th:"th \<in> tines F" "last (trace_tine F th) = h"
           using assm
           using Fw exist_honest_tine by blast
         have "viable F w th"
                     using th assm
                     by (simp add: Fw honest_def honest_indices_viable)
         have "(\<exists>h \<in> honest_indices w. h \<ge> lb F t2) \<longrightarrow>  h \<notin> {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}"
         proof (rule ccontr)
           assume "\<not> ((\<exists>h \<in> honest_indices w. h \<ge> lb F t2) \<longrightarrow>  h \<notin> {h. h \<in> honest_indices w \<and> h \<ge> lb F t2})"
           hence "(\<exists>h \<in> honest_indices w. h \<ge> lb F t2) \<and> \<not> h \<notin> {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}"
             by blast
           then obtain h' where "h' \<in> honest_indices w \<and> h' \<ge>lb F t2"
             by blast
           hence "h' \<in> {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}"
             by blast
           hence "{h. h \<in> honest_indices w \<and> h \<ge> lb F t2} \<noteq> {} \<and> finite {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}"
             using finite_honest_indices by auto
           then obtain x where  " x = Min  {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}"
             by blast
           hence "h' < Min  {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}"
             using \<open>\<not> ((\<exists>h\<in>honest_indices w. lb F t2 \<le> h) \<longrightarrow> h \<notin> {h \<in> honest_indices w. lb F t2 \<le> h})\<close> \<open>h < (if \<exists>h\<in>honest_indices w. lb F t2 \<le> h then Min {h \<in> honest_indices w. lb F t2 \<le> h} else Suc (length w))\<close> \<open>{h \<in> honest_indices w. lb F t2 \<le> h} \<noteq> {} \<and> finite {h \<in> honest_indices w. lb F t2 \<le> h}\<close> by auto
           hence " h \<in> {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}"
             using \<open>\<not> ((\<exists>h\<in>honest_indices w. lb F t2 \<le> h) \<longrightarrow> h \<notin> {h \<in> honest_indices w. lb F t2 \<le> h})\<close> by blast
           thus "False"
             using Min.coboundedI \<open>h' < Min {h \<in> honest_indices w. lb F t2 \<le> h}\<close> \<open>h' \<in> {h \<in> honest_indices w. lb F t2 \<le> h}\<close> \<open>{h \<in> honest_indices w. lb F t2 \<le> h} \<noteq> {} \<and> finite {h \<in> honest_indices w. lb F t2 \<le> h}\<close> leD by blast
         qed
         hence "(\<exists>h \<in> honest_indices w. h \<ge> lb F t2) \<longrightarrow>  h \<le> lb F t2"
           using \<open>h \<in> honest_indices w \<and> h < \<beta>\<close> by auto
         hence "\<not> (\<exists>h \<in> honest_indices w. h \<ge> lb F t2) \<longrightarrow>  h \<le> lb F t2"
           using assm linear by blast
         hence rightmin:"h\<le> lb F t2"
           using \<open>(\<exists>h\<in>honest_indices w. lb F t2 \<le> h) \<longrightarrow> h \<le> lb F t2\<close> by blast
         have "depth F h \<le> size t2"
         proof - 
             have via: "viable F w t2"
             using Ft12
             by (simp add: viable_tines_div_pairs_set_def)
           then show ?thesis
             using assm lb_in_F_last_trace_tine rightmin tines_def viable.simps by auto
             
         qed

         then show "depth F h \<le> min (size t1) (size t2)"
         proof (cases "h \<le> lb F t1")
           case True
           hence "depth F h \<le> size t1"
           proof - 
             have "viable F w t1"
             using Ft12
             by (simp add: viable_tines_div_pairs_set_def)
             then show ?thesis
               using viable.simps True assm
               by (simp add: lb_in_F_last_trace_tine tinesFt1)
           qed
           thus ?thesis
             using \<open>depth F h \<le> length t2\<close> min.bounded_iff by blast
           next
             case False
             have "depth F h \<le> size t1"
             proof (rule ccontr)
               assume "\<not> (depth F h \<le> size t1)"
               hence "depth F h > size t1"
                 by linarith
               thus "False"
               proof (cases "size (longest_common_prefix_tines t1 th) > size t'")
                 case True
                 hence "longest_common_prefix_tines th t2 = t'"
                 proof - 
                   have 0:"take (size (longest_common_prefix_tines t1 th)) th = take (size (longest_common_prefix_tines t1 th)) t1"
                     by (simp add: prefix_list_longest_common_prefix_tines_1 prefix_list_longest_common_prefix_tines_2 prefix_list_take)
                   hence "take (size t') th = take (size t') t1"
                     by (metis True append_take_drop_id diff_is_0_eq' le_eq_less_or_eq prefix_list_longest_common_prefix_tines_2 prefix_list_take take0 take_append)
                   hence 1:"take (size t') th = t'"
                     by (simp add: \<open>take (length t') t1 = t'\<close>)
                   have 2:"take (size t') t2 = t'"
                     by (simp add: prefix_list_take preft't2)
                   have "th ! size t' = t1 ! size t'"
                     by (metis True length_take longest_common_prefix_tines_take_exist min_absorb2 nth_take)
                   hence "th ! size t' \<noteq> t2 ! size t'"
                     using k net2t'k by linarith
                   thus "longest_common_prefix_tines th t2 = t'"
                     using 0 1 2
                     by (smt True \<open>length t' \<le> length t2\<close> dual_order.trans le_eq_less_or_eq longest_common_prefix_tines_take_lt_size_ne not_le_imp_less prefix_list_le_size prefix_list_longest_common_prefix_tines_1 prefix_list_longest_common_prefix_tines_2 prefix_list_nth prefix_list_take)
                 qed
                   hence "divs F th t2 = size th - size t'"
                   proof - 
                     have "lb F th \<le> lb F t2" 
                       using th
                       by (simp add: lb_in_F_last_trace_tine rightmin tines_def)
                     thus ?thesis
                       by (simp add: \<open>longest_common_prefix_tines th t2 = t'\<close> divs_def)
                   qed
                   hence "divs F th t2 = depth F h - size t'"
                     using th
                     by (metis Fw One_nat_def assm fork_F forks_def mem_Collect_eq reaching_tine_length_eq_label_depth tines_def)
                   hence "divs F th t2 > divs F t1 t2"
                     using \<open>length t' \<le> length t1\<close> \<open>length t1 < depth F h\<close> divs_def t' by auto
                   have cont:"div_tines F th t2 > dw"
                     using \<open>divs F t1 t2 < divs F th t2\<close> \<open>dw = div_s w\<close> \<open>lb F t1 \<le> lb F t2\<close> div_sw div_tines_def divs_def t' by auto
                   
                   then show ?thesis
                       using cont using \<open>viable F w th\<close>
                       by (smt Ft12 \<open>dw = div_s w\<close> div_tines_le_div_f mem_Collect_eq not_less prod.inject th(1) tinesFt2 viable_tines_div_pairs_set_def)
               next
                 case False
                 hence "divs F t1 th = (if lb F t1 \<le> lb F th then 
                      size t1 - size (longest_common_prefix_tines t1 th) else 0)"
                   by (simp add: divs_def)
                 have "\<forall>h. h \<in> honest_indices w \<and> h \<le> lb F t1 \<longrightarrow> depth F h \<le> size t1"
                     using \<open>viable F w t1\<close> lb_in_F_last_trace_tine tinesFt1 viable.simps by auto
                 hence "lb F t1 < h"
                     using \<open>depth F h > size t1\<close> th lb_in_F_last_trace_tine Ft12
                     using \<open>\<not> depth F h \<le> length t1\<close> assm leI by blast 
                 
                 hence "divs F t1 th = size t1 - size (longest_common_prefix_tines t1 th)"
                
                     using \<open>divs F t1 th = (if lb F t1 \<le> lb F th then length t1 - length (longest_common_prefix_tines t1 th) else 0)\<close> lb_in_F_last_trace_tine th(1) th(2) tines_def by auto

                 hence "... \<ge> size t1 - size t'"
                   using False diff_le_mono2 leI by blast
                 have "divs F th t1 = (if lb F t1 \<ge> lb F th then 
                      size th - size (longest_common_prefix_tines t1 th) else 0)"
                   by (simp add: divs_def longest_common_prefix_tines_commute)
                 hence "divs F th t1 = 0" using \<open>lb F t1 < h\<close> th lb_in_F_last_trace_tine
                   using tines_def by auto
                 hence "div_tines F t1 th \<ge> dw"
                   using \<open>divs F t1 th = length t1 - length (longest_common_prefix_tines t1 th)\<close> \<open>dw = div_s w\<close> \<open>length t1 - length t' \<le> length t1 - length (longest_common_prefix_tines t1 th)\<close> div_sw div_tines_def by auto
                 have "div_f F w \<ge> div_tines F t1 th"
                   using \<open>viable F w t1\<close> \<open>viable F w th\<close>
                   using \<open>t1 \<in> tines F \<and> t2 \<in> tines F\<close> div_tines_le_div_f th(1) by blast
                 hence "div_s w \<ge> div_tines F t1 th"
                   using Fw div_s_ge_div_f
                   using le_trans by blast
                 hence "dw = div_tines F t1 th"
                   using \<open>dw = div_s w\<close> \<open>dw \<le> div_tines F t1 th\<close> le_antisym by blast
                 hence "dw = divs F t1 th"
                   by (simp add: \<open>divs F th t1 = 0\<close> div_tines_def)
                 have cont:"h - lb F t1 < lb F t2 - lb F t1"
                   using \<open>(\<exists>h\<in>honest_indices w. lb F t2 \<le> h) \<longrightarrow> h \<notin> {h \<in> honest_indices w. lb F t2 \<le> h}\<close> \<open>lb F t1 < h\<close> assm by force
                 hence "abs (int (lb F th) - int (lb F t1)) < optimal_diff_tine_pair (viable_tines_div_pairs_set w)"
                   using Ft12 \<open>lb F t1 < h\<close> lb_in_F_last_trace_tine th(1) th(2) tines_def by auto
                 hence "abs (int (lb F th) - int (lb F t1)) < Min ((\<lambda>p. abs (int (lb (fst p) (fst (snd p))) - int (lb (fst p) (snd (snd p))))) ` (viable_tines_div_pairs_set w))"
                   by (simp add: optimal_diff_tine_pair_def)                  
                   have "div_f F w = div_s w"
                     using Ft12
                     using Fw \<open>div_tines F t1 th \<le> div_f F w\<close> \<open>dw = div_s w\<close> \<open>dw = div_tines F t1 th\<close> div_s_ge_div_f by force
                  hence "(F,(t1,th)) \<in> viable_tines_div_pairs_set w"
                    using \<open>viable F w t1\<close> \<open>viable F w th\<close> \<open>dw = divs F t1 th\<close>
                    by (simp add: Fw \<open>dw = div_s w\<close> viable_tines_div_pairs_set_def)
                  hence "abs (int (lb F th) - int (lb F t1)) \<in> ((\<lambda>p. abs (int (lb (fst p) (fst (snd p))) - int (lb (fst p) (snd (snd p))))) ` (viable_tines_div_pairs_set w))"
                    using image_iff by fastforce
                  hence "abs (int (lb F th) - int (lb F t1))  \<ge> Min ((\<lambda>p. abs (int (lb (fst p) (fst (snd p))) - int (lb (fst p) (snd (snd p))))) ` (viable_tines_div_pairs_set w))"
                    using Min_le finite_abs_diff_pair by blast
                 then show ?thesis
                   using \<open>\<bar>int (lb F th) - int (lb F t1)\<bar> < optimal_diff_tine_pair (viable_tines_div_pairs_set w)\<close> optimal_diff_tine_pair_def by auto 
               qed
           qed
           thus ?thesis
             using \<open>depth F h \<le> length t2\<close> min.bounded_iff by blast
         qed
       qed
       define Fp where Fp:"Fp = Suffix_pinched F (Suc (size t')) \<alpha>'"
         hence "strictly_inc Fp"
         proof - 
           have "\<forall>t\<in>tines F. length t = Suc (size t') 
              \<longrightarrow> \<alpha>' < last (trace_tine F t)"
           proof (rule ccontr)
             assume "\<not> (\<forall>t\<in>tines F. length t = Suc (size t') 
              \<longrightarrow> \<alpha>' < last (trace_tine F t))"
             then obtain tc where tc:"tc \<in> tines F" "size tc = Suc (size t')" "\<alpha>' \<ge> last (trace_tine F tc)" 
               using not_le_imp_less by blast
             have "last (trace_tine F tc) \<noteq> \<alpha>'"
             proof (rule ccontr)
               assume "\<not> last (trace_tine F tc) \<noteq> \<alpha>'"
               hence "last (trace_tine F tc) = \<alpha>'"
                 by blast
               have "last (trace_tine F t') \<in> honest_indices w"
                 using \<open>honest F t' (honest_indices w)\<close> honest_def by blast
               hence "count (mset_of_tree F) \<alpha>' = 1"
                 by (metis Fw One_nat_def \<open>t' \<in> tines F\<close> alpha fork_F forks_def lb_in_F_last_trace_tine mem_Collect_eq tines_def)
               hence "tc = t'"
                 using \<open>last (trace_tine F tc) = \<alpha>'\<close> t' tc exist_unique_label_exist_unique_tine
                 using \<open>t' \<in> tines F\<close> alpha lb_in_F_last_trace_tine tines_def by auto
               thus "False"
                 using tc(2) by auto
             qed
             hence "\<alpha>' > last (trace_tine F tc)" using tc
               by simp
             then obtain tc0 where tc0 :"tine_prefix F0 F tc0 tc" "size tc0 = size tc"
               using \<open>\<And>t. t \<in> tines F \<and> last (trace_tine F t) \<le> \<alpha>' \<Longrightarrow> \<exists>tt\<in>tines F0. tine_prefix F0 F tt t \<and> length tt = length t\<close> tc(1) tc(3) by blast
             hence "tine_providing_label_prefix F0 F tc0 tc"
               by (simp add: tine_prefix_means_label_prefix)
             hence "\<exists>i. take i (trace_tine F tc) = trace_tine F0 tc0"
             proof -
               have "length (trace_tine F tc) = length (trace_tine F0 tc0)"
                 using tc0(1) tc0(2) tine_prefix_imp_both_tines by auto
               then show ?thesis
                 by (metis (no_types) \<open>tine_providing_label_prefix F0 F tc0 tc\<close> append_eq_append_conv not_less not_less_iff_gr_or_eq take_all tine_providing_label_prefix_def tine_providing_label_prefix_refl)
             qed
             have "size (trace_tine F tc) = size (trace_tine F0 tc0)"
               using tc0(1) tc0(2) tine_prefix_imp_both_tines by auto
             hence "trace_tine F tc = trace_tine F0 tc0"
               using \<open>tine_providing_label_prefix F0 F tc0 tc\<close> tine_providing_label_prefix_def by auto
             hence "last (trace_tine F0 tc0) < \<alpha>'"
               using \<open>last (trace_tine F tc) < \<alpha>'\<close> by auto
             hence "size tc0 < size t'"
               using lb_in_F_last_trace_tine ltalphalessdepth tc0(1) tine_prefix_imp_both_tines tines_def by auto
             thus "False"
               by (simp add: tc(2) tc0(2))
           qed
           hence "\<forall>t\<in>tines F. length t = Suc (size t') 
              \<longrightarrow> \<alpha>' < label (get_subtree F t)"
             using last_in_tine_is_label_in_subtree by auto
           thus ?thesis
             using strictly_inc_Suffix_pinched
             using Fw \<open>Fp = Suffix_pinched F (Suc (length t')) \<alpha>'\<close> fork_F forks_def by blast
         qed
         define Fwv' where Fwv':"Fwv' = reduce_tree Fp \<alpha>'"
          hence "strictly_inc Fwv'"
            using Suffix_pinched_def \<open>Fp = Suffix_pinched F (Suc (length t')) \<alpha>'\<close> strictly_inc_reduce_tree
            using \<open>strictly_inc Fp\<close> by fastforce           
          have "label Fwv' = 0"
            by (simp add: Fp Fwv' Suffix_pinched_def)

          have depthshift:"\<And>h. h \<in> honest_indices (drop \<alpha>' w) \<Longrightarrow> count (mset_of_tree Fwv') h = 1 \<and> depth Fwv' h = depth F (h + \<alpha>') - size t'"
            proof - 
              fix h 
              assume "h \<in> honest_indices (drop \<alpha>' w)"
              hence "h + \<alpha>' \<in> honest_indices w"
                using \<open>\<alpha>' \<le> length w\<close> \<open>honest F t' (honest_indices w)\<close> \<open>t' \<in> tines F\<close> alpha honest_def lb_in_F_last_trace_tine shifted_honest_indices tines_def by auto
              hence "count (mset_of_tree F) (h+\<alpha>') = 1"
                by (metis Fw One_nat_def fork_F forks_def)
              obtain thalpha where "thalpha \<in> tines F" "last (trace_tine F thalpha) = h + \<alpha>'"
                using Fw \<open>h + \<alpha>' \<in> honest_indices w\<close> exist_honest_tine by blast
              hence "size thalpha \<ge> size t'"
                by (metis (no_types, lifting) Fw \<open>count (mset_of_tree F) (h + \<alpha>') = 1\<close> \<open>h + \<alpha>' \<in> honest_indices w\<close> \<open>honest F t' (honest_indices w)\<close> \<open>t' \<in> tines F\<close> alpha fork_F forks_def honest_def lb_in_F_last_trace_tine le_add2 le_eq_less_or_eq mem_Collect_eq reaching_tine_length_eq_label_depth tines_def)
              thus "count (mset_of_tree Fwv') h = 1 \<and> depth Fwv' h =  depth F (h + \<alpha>') - size t'"
              proof (cases "size thalpha = size t'")
                case True
                hence "h = 0"
                  by (metis Fw Groups.add_ac(2) One_nat_def \<open>h + \<alpha>' \<in> honest_indices w\<close> \<open>honest F t' (honest_indices w)\<close> \<open>last (trace_tine F thalpha) = h + \<alpha>'\<close> \<open>t' \<in> tines F\<close> \<open>thalpha \<in> tines F\<close> add_diff_cancel_left' alpha diff_is_0_eq' fork_F forks_def honest_def lb_in_F_last_trace_tine le_add2 le_eq_less_or_eq less_not_refl mem_Collect_eq reaching_tine_length_eq_label_depth tines_def)
                hence "\<forall>f \<in> set (sucs Fwv'). \<forall>l \<in># mset_of_tree f. l > 0 "
                  using \<open>strictly_inc Fwv'\<close> strictly_inc_sucs_lt by fastforce
                hence  "\<forall>f \<in> set (sucs Fwv'). count (mset_of_tree f) 0 = 0 "
                  by (meson count_inI less_numeral_extra(3))
                have "mset_of_tree Fwv' = {#label Fwv'#} + (\<Sum>x \<in># mset (sucs Fwv'). mset_of_tree x)"
                  by (metis mset_of_tree.simps rtree.collapse)
                hence "count (mset_of_tree Fwv') 0 =
count ({#label Fwv'#} + (\<Sum>x \<in># mset (sucs Fwv'). mset_of_tree x)) 0"
                  by simp
                hence tmp:"count (mset_of_tree Fwv') 0 =
count ({#label Fwv'#}) 0  + count (\<Sum>x \<in># mset (sucs Fwv'). mset_of_tree x) 0"
                  by simp
                have "depth Fwv' 0 = 0" 
                  by (metis Fw \<open>label Fwv' = 0\<close> depth_def depths.elims fork_F fork_root0 forks_def rtree.exhaust_sel rtree.inject)
                then show ?thesis
                  using tmp
                  using Fw \<open>\<forall>f\<in>set (sucs Fwv'). count (mset_of_tree f) 0 = 0\<close> \<open>h = 0\<close> \<open>label Fwv' = 0\<close> fork_root0
                  by (metis One_nat_def True \<open>count (mset_of_tree F) (h + \<alpha>') = 1\<close> \<open>length t' \<le> length thalpha\<close> \<open>t' \<in> tines F\<close> add_cancel_right_left all_count_zero_imp_count_zero_branch alpha count_single diff_is_0_eq' lb_in_F_last_trace_tine mem_Collect_eq plus_1_eq_Suc reaching_tine_length_eq_label_depth tines_def)
                next
                case False
                hence "size thalpha > size t'"
                  using \<open>length t' \<le> length thalpha\<close> by auto
                then obtain kal tal where ktal:"thalpha = kal @ tal" "size kal = Suc (size t')"
                  by (metis (full_types) Suc_leI append_take_drop_id length_take min_absorb2)
                hence "trace_tine F thalpha = 
trace_tine F kal @ tl (trace_tine (get_subtree F kal) tal)"
                  by (metis \<open>thalpha \<in> tines F\<close> append_eq_conv_conj mem_Collect_eq tines_def trace_tine_append_trace_tine_subtree)
                hence "h + \<alpha>'= 
last (trace_tine F kal @ tl (trace_tine (get_subtree F kal) tal))"
                  using \<open>last (trace_tine F thalpha) = h + \<alpha>'\<close> by auto
                hence "h + \<alpha>' \<in># mset_of_tree (get_subtree F kal)"
                proof (cases tal)
                  case Nil
                  hence "label (get_subtree F kal) = last (trace_tine F kal)"
                    by (simp add: last_in_tine_is_label_in_subtree)
                  hence "label (get_subtree F kal) = h + \<alpha>'"
                    using \<open>last (trace_tine F thalpha) = h + \<alpha>'\<close> \<open>thalpha = kal @ tal\<close> local.Nil by auto
                  then show ?thesis
                    by (metis \<open>thalpha = kal @ tal\<close> \<open>thalpha \<in> tines F\<close> append_Nil2 append_eq_append_conv_if exist_label_eq_exist_tine get_subtree_append last_in_tine_is_label_in_subtree local.Nil mem_Collect_eq mod_get_subtree_tines_append_in_tines tines_def) 
                next
                  case (Cons tal1 tal2)
                  hence "h + \<alpha>'= 
last (tl (trace_tine (get_subtree F kal) tal))"
                    by (metis Nitpick.size_list_simp(2) \<open>h + \<alpha>' = last (trace_tine F kal @ tl (trace_tine (get_subtree F kal) tal))\<close> \<open>thalpha = kal @ tal\<close> \<open>thalpha \<in> tines F\<close> add_left_cancel last_appendR length_0_conv list.distinct(1) list.size(3) mem_Collect_eq mod_get_subtree_tines_append_in_tines plus_1_eq_Suc suc_tine_size_eq_label_list tines_def trace_tine_non_empty)
                  then show ?thesis
                    by (metis \<open>last (trace_tine F thalpha) = h + \<alpha>'\<close> \<open>thalpha = kal @ tal\<close> \<open>thalpha \<in> tines F\<close> append_eq_conv_conj exist_label_eq_exist_tine get_subtree_append last_in_tine_is_label_in_subtree mem_Collect_eq mod_get_subtree_tines_append_in_tines tines_def) 
                qed
                have "kal \<in> tines F"
                  by (metis \<open>thalpha = kal @ tal\<close> \<open>thalpha \<in> tines F\<close> append_eq_conv_conj mem_Collect_eq take_tine_is_tine tines_def)
                hence tmp:"kal \<in> (tines_for_suffix F (Suc (size t')))"
                  by (simp add: ktal(2) tines_def tines_for_suffix_def)
                hence "kal \<in> set (list_of (tines_for_suffix F (Suc (size t'))))"
                  using tmp tines_def tines_finite tines_for_suffix_def
                  by (simp add: finite_list_of list_of_def) 
                then obtain alk where alk:"alk < size (sucs Fp)" "sucs Fp ! alk = get_subtree F kal"
                  using Fp ktal Suffix_pinched_def
                  by (smt exist_num_list length_map nth_map rtree.sel(2))
                have notalk:"\<not> (\<exists>alk'. alk' \<noteq> alk \<and> alk' < size (sucs Fp) \<and> count (mset_of_tree (sucs Fp ! alk')) (h+\<alpha>') > 0)"
                proof (rule ccontr)
                  assume "\<not> \<not> (\<exists>alk'. alk' \<noteq> alk \<and> alk' < size (sucs Fp) \<and> count (mset_of_tree (sucs Fp ! alk')) (h+\<alpha>') > 0)"
                  then obtain alk' where alk':"alk' \<noteq> alk \<and> alk' < size (sucs Fp) \<and> count (mset_of_tree (sucs Fp ! alk')) (h+\<alpha>') > 0"
                    by blast
                  hence " alk' < size (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t')))))
\<and> alk < size (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t')))))
\<and> count (mset_of_tree ((map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) ! alk)) (h+\<alpha>') >0
\<and> count (mset_of_tree ((map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) ! alk')) (h+\<alpha>') >0"
                    using Fp Suffix_pinched_def \<open>alk < length (sucs Fp)\<close> \<open>h + \<alpha>' \<in># mset_of_tree (get_subtree F kal)\<close> \<open>sucs Fp ! alk = get_subtree F kal\<close> by auto
                  hence "count (mset_of_tree (get_subtree F ((list_of (tines_for_suffix F (Suc (size t')))) ! alk))) (h+ \<alpha>') > 0
\<and> count (mset_of_tree (get_subtree F ((list_of (tines_for_suffix F (Suc (size t')))) ! alk'))) (h+ \<alpha>') > 0"
                    by auto
                  hence func: "(\<lambda>x. (count (mset_of_tree (get_subtree F x)) (h + \<alpha>') > 0)) ((list_of (tines_for_suffix F (Suc (size t')))) ! alk)
\<and> (\<lambda>x. (count (mset_of_tree (get_subtree F x)) (h + \<alpha>') > 0)) ((list_of (tines_for_suffix F (Suc (size t')))) ! alk')"
                    by simp
                  hence "\<exists>z y. z \<noteq> y \<and> z \<in> (tines_for_suffix F (Suc (size t'))) \<and> y \<in> (tines_for_suffix F (Suc (size t')))
\<and> (\<lambda>x. (count (mset_of_tree (get_subtree F x)) (h + \<alpha>') > 0)) z \<and>
(\<lambda>x. (count (mset_of_tree (get_subtree F x)) (h + \<alpha>') > 0)) y"
                  proof - 
                    have "finite (tines_for_suffix F (Suc (size t')))"
                      using tines_def tines_finite tines_for_suffix_def by auto
                    hence "alk < card (tines_for_suffix F (Suc (size t'))) \<and>  alk' < card (tines_for_suffix F (Suc (size t')))"
                      using alk alk'
                      by (metis \<open>alk' < length (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t'))))) \<and> alk < length (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t'))))) \<and> 0 < count (mset_of_tree (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))) ! alk)) (h + \<alpha>') \<and> 0 < count (mset_of_tree (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))) ! alk')) (h + \<alpha>')\<close> finite_list_of_size_card length_map list_of_def)
                    thus ?thesis using func dinstinct_index_distinct_elem
                      by (metis \<open>alk' < length (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t'))))) \<and> alk < length (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t'))))) \<and> 0 < count (mset_of_tree (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))) ! alk)) (h + \<alpha>') \<and> 0 < count (mset_of_tree (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))) ! alk')) (h + \<alpha>')\<close> \<open>finite (tines_for_suffix F (Suc (length t')))\<close> alk' card_distinct finite_list_of finite_list_of_size_card length_map list_of_def nth_eq_iff_index_eq nth_mem)
                  qed
                  then obtain kal' where "kal' \<in> (tines_for_suffix F (Suc (size t'))) \<and> kal' \<noteq> kal
\<and> (count (mset_of_tree (get_subtree F kal')) (h + \<alpha>') > 0)"
                    by blast
                  then obtain tal' where "tal' \<in> tines (get_subtree F kal')" 
"last (trace_tine (get_subtree F kal') tal') = h + \<alpha>'"
                    using exist_label_exist_tine by auto
                  hence "kal'@tal' \<in> tines F"
                    using \<open>kal' \<in> tines_for_suffix F (Suc (length t')) \<and> kal' \<noteq> kal \<and> 0 < count (mset_of_tree (get_subtree F kal')) (h + \<alpha>')\<close> tine_append_tine_of_subtree_is_tine tines_def tines_for_suffix_def by auto
                  hence "last (trace_tine F (kal'@ tal')) = h + \<alpha>'"
                    by (metis \<open>last (trace_tine (get_subtree F kal') tal') = h + \<alpha>'\<close> append_eq_conv_conj get_subtree_append last_in_tine_is_label_in_subtree mem_Collect_eq tines_def)
                  hence "kal'@tal' = kal@tal"
                    by (metis \<open>count (mset_of_tree F) (h + \<alpha>') = 1\<close> \<open>h + \<alpha>' = last (trace_tine F kal @ tl (trace_tine (get_subtree F kal) tal))\<close> \<open>kal' @ tal' \<in> tines F\<close> \<open>thalpha \<in> tines F\<close> \<open>trace_tine F thalpha = trace_tine F kal @ tl (trace_tine (get_subtree F kal) tal)\<close> exist_unique_label_exist_unique_tine ktal(1))
                  thus "False"
                    using \<open>kal' \<in> tines_for_suffix F (Suc (length t')) \<and> kal' \<noteq> kal \<and> 0 < count (mset_of_tree (get_subtree F kal')) (h + \<alpha>')\<close> ktal(2) tines_for_suffix_def by auto
                qed
                hence "count (mset_of_tree Fp) (h + \<alpha>') = 1"
                proof -
                  have "mset_of_tree Fp = {#label Fp#} + (\<Sum>x \<in># mset (map (get_subtree F) 
(list_of (tines_for_suffix F (Suc (size t'))))).mset_of_tree x)"
                    by (simp add: Fp Suffix_pinched_def)
                  hence "count (mset_of_tree Fp) (h+\<alpha>') =
count (\<Sum>x \<in># mset (map (get_subtree F) 
(list_of (tines_for_suffix F (Suc (size t'))))).mset_of_tree x)  (h+\<alpha>')"
                    by (metis False Fp Suffix_pinched_def \<open>count (mset_of_tree F) (h + \<alpha>') = 1\<close> \<open>last (trace_tine F thalpha) = h + \<alpha>'\<close> \<open>t' \<in> tines F\<close> \<open>thalpha \<in> tines F\<close> add.left_neutral alpha count_add_mset lb_in_F_last_trace_tine mem_Collect_eq reaching_tine_length_eq_label_depth rtree.sel(1) tines_def union_mset_add_mset_left)
 hence "count (mset_of_tree Fp) (h+\<alpha>') =
count (\<Sum>x \<in># mset 

((take alk (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t')))))) @ 
((map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) ! alk) #
(drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))) .mset_of_tree x)  (h+\<alpha>')"
   by (metis Fp Suffix_pinched_def alk(1) count_mset_of_tree_unfold id_take_nth_drop rtree.sel(2))
 hence "... =
count (\<Sum>x \<in># mset (take alk (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))
+ mset (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))
+ {#((map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) ! alk)#}
.mset_of_tree x)  (h+\<alpha>')"
   using add_mset_add_single by auto
hence "... =
count (\<Sum>x \<in># mset (take alk (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))
+ mset (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))
.mset_of_tree x) (h + \<alpha>') + 
count (mset_of_tree ((map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) ! alk))  (h+\<alpha>')"
  using add_mset_add_single by auto
hence "... =
count (\<Sum>x \<in># mset (take alk (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))
+ mset (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))
.mset_of_tree x) (h + \<alpha>') + 
1"
  using alk
  by (metis Fp Suffix_pinched_def \<open>count (\<Union>#image_mset mset_of_tree (mset (take alk (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t'))))) @ map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))) ! alk # drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))))))) (h + \<alpha>') = count (\<Union>#image_mset mset_of_tree (mset (take alk (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))))) + mset (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))))) + {#map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))) ! alk#})) (h + \<alpha>')\<close> \<open>count (mset_of_tree F) (h + \<alpha>') = 1\<close> \<open>count (mset_of_tree Fp) (h + \<alpha>') = count (\<Union>#image_mset mset_of_tree (mset (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t'))))))) (h + \<alpha>')\<close> \<open>count (mset_of_tree Fp) (h + \<alpha>') = count (\<Union>#image_mset mset_of_tree (mset (take alk (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t'))))) @ map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))) ! alk # drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))))))) (h + \<alpha>')\<close> \<open>h + \<alpha>' \<in># mset_of_tree (get_subtree F kal)\<close> add_gr_0 count_greater_eq_one_iff count_greater_zero_iff count_honest_suffix_pinched_le_1 le_add2 le_antisym rtree.sel(2))
  hence "... = 1"
  proof - 
    have "\<not> (\<exists>alk'. alk' \<noteq> alk \<and> alk' < size (sucs Fp) \<and> count (mset_of_tree (sucs Fp ! alk')) (h+\<alpha>') > 0)"
      using notalk by simp
    hence "\<forall>alk'. alk' = alk \<or> alk' \<ge> size (sucs Fp) \<or> count (mset_of_tree (sucs Fp ! alk')) (h+\<alpha>') = 0"
      using le_eq_less_or_eq nat_le_linear by blast
hence "\<forall>alk'. (alk' \<noteq> alk \<and> alk' < size (sucs Fp)) \<longrightarrow> count (mset_of_tree (sucs Fp ! alk')) (h+\<alpha>') = 0"
  by auto
  hence "\<forall>alk'. (alk' \<noteq> alk \<and> alk' < size (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t')))))) \<longrightarrow>
 count (mset_of_tree ((map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) ! alk')) (h+\<alpha>') = 0"
    by (simp add: Fp Suffix_pinched_def)
  have "\<forall>x \<in># mset  (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))).
 \<exists>alk'. alk' < size (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) \<and>
x = (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) ! alk'"
    using exist_num_list by fastforce
  hence tmp:"\<forall>x \<in># mset ((take alk (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t')))))))
. \<exists>alk'. alk' < alk \<and>
x = (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) ! alk'"
    using exist_num_list by fastforce
  have"\<forall>x \<in># mset (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))
. \<exists>alk'.  alk' < size (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) \<and>
x = (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) ! alk'"
    using exist_num_list
    by (metis in_set_dropD set_mset_mset) 
have"\<forall>x \<in># mset (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))
. \<exists>alk'.  alk' < size (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))\<and>
x = (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t')))))) ! alk'"
    using exist_num_list
    by fastforce
hence"\<forall>x \<in># mset (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))
. \<exists>alk'.  alk' < size (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))\<and>
x = ( (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t')))))) ! (alk' + (Suc alk))"
    using exist_num_list
    by (metis Fp Suc_leI Suffix_pinched_def add.commute alk(1) nth_drop rtree.sel(2))
hence"\<forall>x \<in># mset (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))
. \<exists>alk'. alk < alk' \<and> alk' < size (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t')))))) + Suc alk \<and>
x = ( (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t')))))) ! alk' "
    using exist_num_list
    by (metis add.commute nat_add_left_cancel_less not_add_less2 not_less_eq)    
hence tmp1:"\<forall>x \<in># mset (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))
. \<exists>alk'. alk < alk' \<and> alk' < size (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) \<and>
x = ( (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t')))))) ! alk' "
    using exist_num_list
    by (metis \<open>\<forall>x\<in>#mset (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))))). \<exists>alk'<length (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))))). x = map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))) ! (alk' + Suc alk)\<close> length_drop less_diff_conv not_add_less2 not_less_eq)
hence "\<And>x. x \<in># mset (take alk (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))
+ mset (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))
\<Longrightarrow> (\<exists>alk'. (alk' \<noteq> alk \<and> alk' < size (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) )\<and>
x = (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) ! alk') "
proof- 
  fix x
  assume "x \<in># mset (take alk (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))
+ mset (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))"
  hence "x \<in># mset (take alk (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))
\<or> x \<in># mset (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))"
    by auto
    
  hence
"(\<exists>alk'. alk' < alk \<and>
x = (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) ! alk')
 \<or>
(\<exists>alk'. alk < alk' \<and> alk' < size (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) \<and>
x = ( (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t')))))) ! alk') "
    using tmp tmp1
    by blast
hence
"(\<exists>alk'. alk' < alk \<and>
x = (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) ! alk')
 \<or>
(\<exists>alk'. alk < alk' \<and> alk' < size (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) \<and>
x = ( (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t')))))) ! alk') "
    using tmp tmp1
    by blast
hence
"(\<exists>alk'. (alk' < alk \<or> alk < alk' \<and> alk' < size (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) )\<and>
x = (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) ! alk')"
    using tmp tmp1
    by blast
  thus
"(\<exists>alk'. (alk' \<noteq> alk \<and> alk' < size (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) )\<and>
x = (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))) ! alk')"
    using tmp tmp1
    by (metis (no_types, lifting) Fp Suc_lessD Suffix_pinched_def alk(1) less_trans_Suc not_less_iff_gr_or_eq rtree.sel(2))
qed
  hence "\<forall>x \<in># mset (take alk (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))
+ mset (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (size t'))))))
.count (mset_of_tree x) (h + \<alpha>') = 0"
    using \<open>\<forall>alk'. alk' \<noteq> alk \<and> alk' < length (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t'))))) \<longrightarrow> count (mset_of_tree (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))) ! alk')) (h + \<alpha>') = 0\<close> by force
                  then show ?thesis
                    by simp
              qed 
              thus "count (mset_of_tree Fp) (h + \<alpha>') = 1"
                using \<open>count (\<Union>#image_mset mset_of_tree (mset (take alk (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t'))))) @ map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))) ! alk # drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))))))) (h + \<alpha>') = count (\<Union>#image_mset mset_of_tree (mset (take alk (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))))) + mset (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))))) + {#map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))) ! alk#})) (h + \<alpha>')\<close> \<open>count (\<Union>#image_mset mset_of_tree (mset (take alk (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))))) + mset (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))))) + {#map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))) ! alk#})) (h + \<alpha>') = count (\<Union>#image_mset mset_of_tree (mset (take alk (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))))) + mset (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))))))) (h + \<alpha>') + count (mset_of_tree (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))) ! alk)) (h + \<alpha>')\<close> \<open>count (\<Union>#image_mset mset_of_tree (mset (take alk (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))))) + mset (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))))))) (h + \<alpha>') + count (mset_of_tree (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))) ! alk)) (h + \<alpha>') = count (\<Union>#image_mset mset_of_tree (mset (take alk (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))))) + mset (drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))))))) (h + \<alpha>') + 1\<close> \<open>count (mset_of_tree Fp) (h + \<alpha>') = count (\<Union>#image_mset mset_of_tree (mset (take alk (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t'))))) @ map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))) ! alk # drop (Suc alk) (map (get_subtree F) (list_of (tines_for_suffix F (Suc (length t')))))))) (h + \<alpha>')\<close> by linarith
            qed 
            hence "count (mset_of_tree (reduce_tree Fp \<alpha>')) h = 1"
              using reduce_tree_count
              by (metis Fp Suffix_pinched_def \<open>strictly_inc Fp\<close> alpha dual_order.refl order_refl rtree.sel(1))
  
            hence "count (mset_of_tree Fwv') h = 1"
              using Fp Fwv'
              by auto
            have "last (trace_tine (sucs Fp ! alk) tal) = h + \<alpha>'"
              using alk ktal
              by (metis \<open>kal \<in> tines F\<close> \<open>last (trace_tine F thalpha) = h + \<alpha>'\<close> \<open>thalpha \<in> tines F\<close> \<open>trace_tine F thalpha = trace_tine F kal @ tl (trace_tine (get_subtree F kal) tal)\<close> add_left_imp_eq append_Nil2 append_eq_append_conv append_eq_append_conv_if get_subtree_append last_appendR last_in_tine_is_label_in_subtree last_tl mem_Collect_eq plus_1_eq_Suc suc_tine_size_eq_label_list tines_def)
            hence lasthal:"last (trace_tine Fp (alk# tal)) = h + \<alpha>'"
proof -
  have "get_subtree (Suffix_pinched F (Suc (length t')) \<alpha>') (alk # tal) = (case alk # tal of [] \<Rightarrow> Tree (label (Suffix_pinched F (Suc (length t')) \<alpha>')) (take alk (sucs Fp) @ sucs Fp ! alk # drop (Suc alk) (sucs Fp)) | n # ns \<Rightarrow> if n < length (take alk (sucs Fp) @ sucs Fp ! alk # drop (Suc alk) (sucs Fp)) then get_subtree ((take alk (sucs Fp) @ sucs Fp ! alk # drop (Suc alk) (sucs Fp)) ! n) ns else Tree (label (Suffix_pinched F (Suc (length t')) \<alpha>')) (take alk (sucs Fp) @ sucs Fp ! alk # drop (Suc alk) (sucs Fp)))"
    by (metis (no_types) Fp alk(1) get_subtree.simps id_take_nth_drop rtree.exhaust_sel)
  then show ?thesis
    using Fp \<open>last (trace_tine (sucs Fp ! alk) tal) = h + \<alpha>'\<close> alk(1) id_take_nth_drop last_in_tine_is_label_in_subtree by fastforce
qed
  have "(alk# tal) \<in> tines Fp" using alk ktal
    using \<open>thalpha \<in> tines F\<close> mod_get_subtree_tines_append_in_tines tinesP_Cons_iff tines_def by auto
  hence "last (trace_tine Fwv' (alk# tal)) = h"
    using lasthal Fwv' Fp reduce_tree_last_trace_tine
    using Suffix_pinched_def \<open>strictly_inc Fp\<close> add_right_cancel order_refl rtree.sel(1) tines_def by fastforce
  have "(alk# tal) \<in> tines Fwv'"
    using \<open>(alk# tal) \<in> tines Fp\<close> reduce_tree_tines Fwv'
    by (simp add: tines_def)
  hence "depth Fwv' h = size (alk#tal)"
    using \<open>count (mset_of_tree Fwv') h = 1\<close> \<open>last (trace_tine Fwv' (alk # tal)) = h\<close> reaching_tine_length_eq_label_depth tines_def by fastforce
  hence "depth Fwv' h = size (kal@tal) - size t'"
    using \<open>size kal = Suc (size t')\<close>
    by simp
  thus ?thesis
    using \<open>count (mset_of_tree F) (h + \<alpha>') = 1\<close> \<open>count (mset_of_tree Fwv') h = 1\<close> \<open>last (trace_tine F thalpha) = h + \<alpha>'\<close> \<open>thalpha \<in> tines F\<close> ktal(1) reaching_tine_length_eq_label_depth tines_def by fastforce
      qed
    qed
   
          hence "Fwv' \<turnstile> (drop \<alpha>' w)"
          proof -
            have 0:"label Fwv' = 0"
              using Fwv'
              by (simp add: Suffix_pinched_def \<open>Fp = Suffix_pinched F (Suc (length t')) \<alpha>'\<close> rtree.collapse)
       have 2:"\<And>i j. i \<in> honest_indices (drop \<alpha>' w) \<and> j \<in> honest_indices (drop \<alpha>' w) \<and>  i < j \<Longrightarrow> 
  depth Fwv' i < depth Fwv' j"
      proof -
        fix i j
        assume assm:"i \<in> honest_indices (drop \<alpha>' w) \<and> j \<in> honest_indices (drop \<alpha>' w) \<and>  i < j"
  hence "depth Fwv' i = depth F (i + \<alpha>') - size t' \<and> depth Fwv' j = depth F (j + \<alpha>') - size t'"
    using \<open>\<And>h. h \<in> honest_indices (drop \<alpha>' w) \<Longrightarrow> count (mset_of_tree Fwv') h = 1 \<and> depth Fwv' h = depth F (h + \<alpha>') - length t'\<close> by blast
  
    have ijalhonest:"(i + \<alpha>') \<in> honest_indices w \<and> (j + \<alpha>') \<in> honest_indices w"
      using assm
      using \<open>\<alpha>' \<le> length w\<close> \<open>honest F t' (honest_indices w)\<close> \<open>t' \<in> tines F\<close> alpha honest_def lb_in_F_last_trace_tine mem_Collect_eq shifted_honest_indices tines_def by auto
    hence "depth F (i + \<alpha>') < depth F (j + \<alpha>') "
      using \<open>F \<turnstile> w\<close> assm
      by (simp add: fork_F forks_def) 
    have "depth F \<alpha>' \<le> depth F (i + \<alpha>')"
    proof (cases i)
      case 0
      then show ?thesis
        by simp 
    next
      case (Suc ii)
      hence "\<alpha>' < i + \<alpha>'"
        by simp
      then show ?thesis using ijalhonest \<open>\<alpha>' \<in> honest_indices w\<close> fork_F forks_def
        using Fw le_eq_less_or_eq by auto
    qed 
    hence "size t' \<le> depth F (i + \<alpha>')" 
      using \<open>\<alpha>' = lb F t'\<close>
      by (metis Fw One_nat_def \<open>\<alpha>' \<in> honest_indices w\<close> \<open>t' \<in> tines F\<close> fork_F forks_def lb_in_F_last_trace_tine mem_Collect_eq reaching_tine_length_eq_label_depth tines_def)
     hence "depth F (i + \<alpha>') - size t'  < depth F (j + \<alpha>') - size t' "
      using \<open>F \<turnstile> w\<close> assm
      using \<open>depth F (i + \<alpha>') < depth F (j + \<alpha>')\<close> diff_less_mono by blast 
    thus "depth Fwv' i < depth Fwv' j"
      by (simp add: \<open>depth Fwv' i = depth F (i + \<alpha>') - length t' \<and> depth Fwv' j = depth F (j + \<alpha>') - length t'\<close>)
  qed
  have " limit Fp (size w)"
      using Fp Fw \<open>\<alpha>' \<le> length w\<close> fork_F forks_def limit_Suffix_pinched by auto
    
    hence "limit Fwv' ((size w) - \<alpha>')"
      using Fp Fwv'
      using \<open>\<alpha>' \<le> length w\<close> reduce_tree_limit by auto
    hence 3:"limit Fwv' (size (drop \<alpha>' w))"
    by simp

  thus ?thesis using 0 depthshift 2 \<open>strictly_inc Fwv'\<close> 
    by (simp add: fork.intros forks_def)
qed
  obtain Fwv'' where Fwv'':"Fwv'' \<le> Fwv'" 
"(\<forall>n. n \<le> size (drop \<alpha>' (take (\<beta> - 1) w)) \<longrightarrow> count (mset_of_tree Fwv'') n = count (mset_of_tree Fwv') n)"
"limit Fwv'' (size (drop \<alpha>' (take (\<beta> - 1) w)))"
    using strictly_inc_exist_prefix_limit \<open>strictly_inc Fwv'\<close> \<open>Fwv' \<turnstile> (drop \<alpha>' w)\<close>
    by (metis fork_F forks_def le0)
  hence Fwv''fork:"Fwv'' \<turnstile> wv"
  proof -
    have 0:"strictly_inc Fwv''" using Fwv'' \<open>Fwv' \<turnstile> (drop \<alpha>' w)\<close>
      using \<open>strictly_inc Fwv'\<close> prefix_strictly_inc by blast
    have honestimp:"\<And>h. h\<in>honest_indices wv \<Longrightarrow> h\<in>honest_indices (drop \<alpha>' w)"
      by (smt Suc_pred' \<open>length wv = \<beta> - 1 - \<alpha>'\<close> drop_take honest_indices_iff le_imp_less_Suc le_trans nat_add_left_cancel_less nat_le_linear nth_take plus_1_eq_Suc take_all wv)
    hence count1:"\<And>h. h\<in>honest_indices wv \<Longrightarrow> count (mset_of_tree Fwv') h = 1"
      using \<open>Fwv' \<turnstile> drop \<alpha>' w\<close> fork_F forks_def by auto
    hence count11:"\<And>h. h\<in>honest_indices wv \<Longrightarrow> count (mset_of_tree Fwv'') h = 1"
      by (metis Fwv''(2) honest_indices_iff le0 wv)
    have "\<And>i j. i\<in>honest_indices wv \<and> j \<in> honest_indices wv \<and> i < j
\<Longrightarrow> depth Fwv' i < depth Fwv' j"
      using honestimp \<open>Fwv' \<turnstile> (drop \<alpha>' w)\<close>
      using fork_F forks_def by auto
    have "\<And>i j. i\<in>honest_indices wv \<and> j \<in> honest_indices wv \<and> i < j
\<Longrightarrow> depth Fwv' i < depth Fwv' j"
      using honestimp \<open>Fwv' \<turnstile> (drop \<alpha>' w)\<close>
      using fork_F forks_def by auto
    hence 1:"\<And>i j. i\<in>honest_indices wv \<and> j \<in> honest_indices wv \<and> i < j
\<Longrightarrow> depth Fwv'' i < depth Fwv'' j" using count1 count11
      using Fwv''(1) prefix_eq_depth_unique by auto
    have "label Fwv'' = label Fwv'"
      using Fwv''
      by (metis Fwv' less_eq_rtree.abs_eq prefix_Tree_Tree_eq rtree.collapse)
    hence "label Fwv'' = 0"
      using \<open>Fwv' \<turnstile> drop \<alpha>' w\<close> fork_F forks_def by auto
    thus ?thesis using 0 1 count11 \<open>limit Fwv'' (size (drop \<alpha>' (take (\<beta> - 1) w)))\<close> wv
      by (simp add: "1" fork.intros forks_def)
  qed
  obtain mhwv where mhwv: "mhwv = Max (honest_indices wv)"
    by simp
  have "finite  (honest_indices wv) \<and>  (honest_indices wv) \<noteq> {}"
    using finite_honest_indices honest_indices_iff by auto
  hence "mhwv \<in>  (honest_indices wv) "
    using mhwv
    using Max_in by blast
  hence "mhwv \<in> insert 0 (Suc ` {n. n < size wv \<and> ~ wv!n})"
    using honest_indices_def by auto
  hence "mhwv \<in> insert 0 (Suc ` {n. n < size (drop \<alpha>' (take (\<beta> - 1) w)) \<and> ~ (drop \<alpha>' (take (\<beta> - 1) w))!n})"
    using wv by simp
  hence "mhwv =0 \<or> (mhwv \<le> size (drop \<alpha>' (take (\<beta> - 1) w))
\<and>  ~ (drop \<alpha>' (take (\<beta> - 1) w))!(mhwv -1))"
    using \<open>mhwv \<in> honest_indices wv\<close> honest_indices_iff wv by blast
hence smhwv:"mhwv =0 \<or> (mhwv \<le> size (drop \<alpha>' (take (\<beta> - 1) w))
\<and>  ~ (drop \<alpha>' w)!(mhwv -1))"
  by (metis Suc_pred' \<open>length wv = \<beta> - 1 - \<alpha>'\<close> \<open>mhwv \<in> honest_indices wv\<close> drop_take honest_indices_iff le_imp_less_Suc nat_add_left_cancel_less nth_take plus_1_eq_Suc)
hence "mhwv =0 \<or> (mhwv \<le> size (drop \<alpha>' w)
\<and>  ~ (drop \<alpha>' w)!(mhwv -1))"
  by (metis \<open>length wv = \<beta> - 1 - \<alpha>'\<close> drop_take le_trans nat_le_linear take_all wv)
  hence "mhwv \<in> insert 0 (Suc ` {n. n < size (drop \<alpha>' w) \<and> ~ (drop \<alpha>' w)!n})"
    using honest_indices_def honest_indices_iff by presburger
   hence mhwvdrop:"mhwv \<in> honest_indices (drop \<alpha>' w)"
     using honest_indices_def by blast
   then obtain dmhwv tmhwv' where dt:"tmhwv' \<in> tines Fwv'" "last (trace_tine Fwv' tmhwv') = mhwv"
"dmhwv = depth Fwv' mhwv"
     using \<open>Fwv' \<turnstile> drop \<alpha>' w\<close> exist_honest_tine by blast
   hence "size tmhwv' = dmhwv"
     using mhwvdrop \<open>Fwv' \<turnstile> drop \<alpha>' w\<close>
     by (metis One_nat_def fork_F forks_def mem_Collect_eq reaching_tine_length_eq_label_depth tines_def)
 define Fwv where Fwv:"Fwv = limit_tine_length_tree Fwv'' dmhwv"
  hence "Fwv \<le> Fwv''"
    using limit_tine_length_tree_prefix by blast
  hence Fwvhonestunique:"\<And>h. h \<in> (honest_indices wv) \<Longrightarrow> count (mset_of_tree Fwv) h = 1"
  proof - 
    fix h assume hin:"h \<in> honest_indices wv"
    hence "count (mset_of_tree Fwv'') h = 1"
      by (metis Fwv''fork One_nat_def fork_F forks_def)
    then obtain th where "th \<in> tines Fwv''" "last (trace_tine Fwv'' th) = h" 
      using exist_unique_label_exist_unique_tine by blast
    have "h \<le> mhwv" using mhwv using hin
      by (simp add: finite_honest_indices)
    hence "depth Fwv'' h \<le> depth Fwv'' mhwv"
      using Fwv''fork \<open>mhwv \<in> honest_indices wv\<close> fork_F forks_def hin le_eq_less_or_eq by auto
    hence "size th \<le> depth Fwv'' mhwv"
      using \<open>count (mset_of_tree Fwv'') h = 1\<close> \<open>last (trace_tine Fwv'' th) = h\<close> \<open>th \<in> tines Fwv''\<close> reaching_tine_length_eq_label_depth tines_def by auto
    then obtain th' where "th' \<in> tines Fwv" "last (trace_tine Fwv th') = h" "size th' = size th'"
      by (metis Fwv Fwv''(1) Fwv''fork One_nat_def \<open>Fwv' \<turnstile> drop \<alpha>' w\<close> \<open>last (trace_tine Fwv'' th) = h\<close> \<open>mhwv \<in> honest_indices wv\<close> \<open>th \<in> tines Fwv''\<close> dt(3) fork_F forks_def limit_tine_length_tree_exist_shorter_tine mem_Collect_eq mhwvdrop prefix_eq_depth_unique tines_def)
    hence "count (mset_of_tree Fwv) h\<ge> 1"
      using tines_def tines_exist_last_label_exist trace_tine_non_empty by auto
    thus "count (mset_of_tree Fwv) h = 1"
      using \<open>count (mset_of_tree Fwv'') h = 1\<close>
      by (metis \<open>Fwv \<le> Fwv''\<close> dual_order.antisym prefix_count_le)
  qed
    have "Fwv \<turnstile> wv"
    proof -
    have "label Fwv = label Fwv''"
      using Fwv limit_tine_length_tree.simps
      by (metis (full_types) limit_tine_length_tree.elims rtree.sel(1))
    hence 0:"label Fwv = 0"
      using \<open>Fwv'' \<turnstile> wv\<close> fork_F forks_def by auto
    have 1:"strictly_inc Fwv"
      using Fwv Fwv''fork
      using Fwv''(1) \<open>Fwv \<le> Fwv''\<close> \<open>strictly_inc Fwv'\<close> prefix_strictly_inc by auto
    have 2:"limit Fwv (size wv)"
      using Fwv''fork prefix_preserves_limit
      using Fwv''(3) \<open>Fwv \<le> Fwv''\<close> wv by blast

    have "\<And>h. h \<in> (honest_indices wv) \<Longrightarrow> depth Fwv'' h = depth Fwv h"
      using \<open>\<And>h. h \<in> (honest_indices wv) \<Longrightarrow> count (mset_of_tree Fwv) h = 1\<close>
      by (metis Fwv''fork One_nat_def \<open>Fwv \<le> Fwv''\<close> fork_F forks_def prefix_eq_depth_unique)
    hence "\<And>i j. i \<in> (honest_indices wv) \<and> j \<in> honest_indices wv \<and> i < j 
\<Longrightarrow> depth Fwv i < depth Fwv j"
      using Fwv''fork fork_F forks_def by auto
    thus ?thesis using Fwvhonestunique
      by (simp add: "0" "1" "2" fork.intros forks_def)
  qed
    hence "flat Fwv"
    proof - 
      have "take (Suc (size t')) t1 \<in> tines F \<and>  take (Suc (size t')) t2 \<in> tines F"
        using \<open>t1 \<in> tines F \<and> t2 \<in> tines F\<close> take_tine_is_tine tines_def by auto
      have "size (take (Suc (size t')) t1) = Suc (size t') \<and>
size (take (Suc (size t')) t2) = Suc (size t')"
        by (simp add: Suc_leI min_absorb2 t1gtt' t2gtt')
      hence tmp:"(take (Suc (size t')) t1) \<in> tines_for_suffix F (Suc (size t')) \<and> 
(take (Suc (size t')) t2) \<in> tines_for_suffix F (Suc (size t'))"
        using \<open>take (Suc (length t')) t1 \<in> tines F \<and> take (Suc (length t')) t2 \<in> tines F\<close> tines_def tines_for_suffix_def by auto
      then obtain kp1  where kp1:"kp1 < size  (list_of (tines_for_suffix F (Suc (size t'))))"
" (list_of (tines_for_suffix F (Suc (size t')))) ! kp1 = (take (Suc (size t')) t1)"
        by (metis exist_num_list finite_Collect_conjI finite_list_of list_of_def tines_def tines_finite tines_for_suffix_def)
  obtain kp2  where kp2:"kp2 < size  (list_of (tines_for_suffix F (Suc (size t'))))"
" (list_of (tines_for_suffix F (Suc (size t')))) ! kp2 = (take (Suc (size t')) t2)"
    using tmp 
        by (metis exist_num_list finite_Collect_conjI finite_list_of list_of_def tines_def tines_finite tines_for_suffix_def)
      hence "kp1 \<noteq> kp2"
        using \<open>list_of (tines_for_suffix F (Suc (length t'))) ! kp1 = take (Suc (length t')) t1\<close> lessI longest_common_prefix_tines_take_lt_size_ne t' t1gtt' t2gtt' by auto
      hence "(sucs Fp) ! kp1 = get_subtree F (take (Suc (length t')) t1)"
        by (simp add: Fp Suffix_pinched_def \<open>kp1 < length (list_of (tines_for_suffix F (Suc (length t'))))\<close> \<open>list_of (tines_for_suffix F (Suc (length t'))) ! kp1 = take (Suc (length t')) t1\<close>)
      hence kp1tine:"tinesP ((sucs Fp) ! kp1) (drop (Suc (length t')) t1)"
        by (simp add: get_subtree_tines_append_in_tines tinesFt1)
      hence "(sucs Fp) ! kp2 = get_subtree F (take (Suc (length t')) t2)"
        by (simp add: Fp Suffix_pinched_def \<open>kp2 < length (list_of (tines_for_suffix F (Suc (length t'))))\<close> \<open>list_of (tines_for_suffix F (Suc (length t'))) ! kp2 = take (Suc (length t')) t2\<close>)
      hence kp2tine:"tinesP ((sucs Fp) ! kp2) (drop (Suc (length t')) t2)"
        using get_subtree_tines_append_in_tines tinesFt2 tines_def by auto
      define tp1 tp2 where "tp1 = kp1#(drop (Suc (length t')) t1)" "tp2= kp2#(drop (Suc (length t')) t2)"
      hence tptine:"tinesP Fp tp1 \<and> tinesP Fp tp2"
        using kp1 kp2 kp1tine kp2tine
        by (simp add: Fp Sucs Suffix_pinched_def)
      hence "tp1 /\<cong> tp2"
        using \<open>kp1 \<noteq> kp2\<close> equiv_iff tp1_tp2_def(1) tp1_tp2_def(2) by auto 
      have strictp:"strictly_inc_list (trace_tine Fp tp1)" "strictly_inc_list (trace_tine Fp tp2)"
        using \<open>strictly_inc Fp\<close>
        using strictly_inc_imp_strictly_inc_tine strictly_inc_tine_def by auto
      have "last (trace_tine Fp tp1) = last (trace_tine F t1)"
        using tp1_tp2_def kp1 kp2 kp1tine kp2tine
        by (smt Fp Suffix_pinched_def \<open>sucs Fp ! kp1 = get_subtree F (take (Suc (length t')) t1)\<close> \<open>tinesP Fp tp1 \<and> tinesP Fp tp2\<close> get_subtree.simps get_subtree_append last_in_tine_is_label_in_subtree list.simps(5) rtree.sel(2) tinesFt1 tinesP_Cons_iff)
      hence "last (trace_tine Fp tp1) < \<beta>"
        using \<open>lb F t1 < \<beta>\<close> lb_in_F_last_trace_tine tinesFt1 by auto
      hence "\<forall>x. x \<in> set (trace_tine Fp tp1) \<longrightarrow> x \<le> last (trace_tine Fp tp1)"
        using strictp tptine strictly_inc_list_le_last
        by simp
      hence "\<forall>x. x \<in> set (trace_tine Fp tp1) \<longrightarrow> x < \<beta>"
        using \<open>last (trace_tine Fp tp1) < \<beta>\<close> by auto
      hence "\<forall>x. x \<in> set (trace_tine Fp tp1) \<longrightarrow> x \<ge> \<alpha>'"
        using strictp tptine strictly_inc_list_ge_hd
        by (smt Fp Suffix_pinched_def list.case_eq_if list.sel(1) trace_tine.simps) 
      have "last (trace_tine Fp tp2) = last (trace_tine F t2)"
        by (smt Fp Suffix_pinched_def \<open>sucs Fp ! kp2 = get_subtree F (take (Suc (length t')) t2)\<close> alpha get_subtree.simps get_subtree_append last_in_tine_is_label_in_subtree list.simps(5) mem_Collect_eq rtree.sel(2) tinesFt2 tinesP_Cons_iff tines_def tp1_tp2_def(2) tptine)
      have "last (trace_tine F t2) \<le> \<beta>"
      proof (cases "\<exists>h \<in> honest_indices w. h \<ge> lb F t2")
        case True
        hence "\<beta> = Min {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}"
          using beta by simp
        have "{h. h \<in> honest_indices w \<and> h \<ge> lb F t2} \<noteq>{}"
          using True by auto
        have "finite {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}"
          by (simp add: finite_honest_indices)

        hence "\<beta> \<in> {h. h \<in> honest_indices w \<and> h \<ge> lb F t2}"
          using True
          using Min_eq_iff \<open>\<beta> = Min {h \<in> honest_indices w. lb F t2 \<le> h}\<close> by blast
        then show ?thesis
          using lb_in_F_last_trace_tine tinesFt2 tines_def by auto 
      next
        case False
        hence "\<beta> = Suc (size w)"
          using beta by simp
        have "last (trace_tine F t2) \<in># mset_of_tree F"
          using exist_label_eq_exist_tine tinesFt2 by auto
        hence "last (trace_tine F t2) \<le> size w"
          using Fw lb_in_F_last_trace_tine lb_limit tinesFt2 tines_def by auto
        then show ?thesis
          using \<open>\<beta> = Suc (length w)\<close> by auto 
      qed
      hence "last (trace_tine Fp tp2) \<le> \<beta>"
        using beta
        using \<open>last (trace_tine Fp tp2) = last (trace_tine F t2)\<close> by linarith
      hence "\<forall>x. x \<in> set (trace_tine Fp tp2) \<longrightarrow> x \<le> last (trace_tine Fp tp2)"
        using strictp tptine strictly_inc_list_le_last
        by simp
      hence "\<forall>x. x \<in> set (trace_tine Fp tp2) \<longrightarrow> x \<le> \<beta>"
        using \<open>last (trace_tine Fp tp2) \<le> \<beta>\<close> le_trans by blast
      hence "\<forall>x. x \<in> set (trace_tine Fp tp2) \<longrightarrow> x \<ge> \<alpha>'"
        using strictp tptine strictly_inc_list_ge_hd
        by (smt Fp Suffix_pinched_def list.case_eq_if list.sel(1) trace_tine.simps)
      have "\<And>h. h \<in> honest_indices w \<and> h < \<beta> \<Longrightarrow> depth F h - size t' \<le> min (length tp1) (length tp2)"
        using "14" less_diff_conv less_irrefl_nat less_le_trans min_diff t1gtt' t2gtt' tp1_tp2_def(1) tp1_tp2_def(2) by fastforce
      hence "\<And>h. h \<in> honest_indices (drop \<alpha>' w) \<and> h < \<beta> - \<alpha>' \<Longrightarrow> 
        depth Fwv' h  \<le> min (length tp1) (length tp2)"
        by (simp add: \<open>\<alpha>' \<in> honest_indices w\<close> \<open>\<alpha>' \<le> length w\<close> depthshift less_diff_conv shifted_honest_indices)
       have "\<And>h. h \<in> honest_indices wv \<Longrightarrow> h \<in> honest_indices (drop \<alpha>' w)"
          by (smt Max_ge Suc_pred' \<open>finite (honest_indices wv) \<and> honest_indices wv \<noteq> {}\<close> \<open>length wv = \<beta> - 1 - \<alpha>'\<close> \<open>mhwv = 0 \<or> mhwv \<le> length (drop \<alpha>' w) \<and> \<not> drop \<alpha>' w ! (mhwv - 1)\<close> drop_take honest_indices_iff leD le_trans mhwv not_less_eq nth_take wv)
      have "\<forall>h. h \<in> honest_indices wv  \<longrightarrow>
        depth Fwv' h  \<le> min (length tp1) (length tp2)"
      proof - 
              have "\<And>h. h \<in> honest_indices wv \<Longrightarrow> h < \<beta> - \<alpha>'"
          using False \<open>div_s w \<le> length wv\<close> \<open>length wv = \<beta> - 1 - \<alpha>'\<close> dw_def honest_indices_iff by force
        thus ?thesis
          using \<open>\<And>h. h \<in> honest_indices (drop \<alpha>' w) \<and> h < \<beta> - \<alpha>' \<Longrightarrow> depth Fwv' h \<le> min (length tp1) (length tp2)\<close> \<open>\<And>h. h \<in> honest_indices wv \<Longrightarrow> h \<in> honest_indices (drop \<alpha>' w)\<close> by blast
      qed
      have "(trace_tine Fwv' tp1) = 
map (\<lambda>x. x - \<alpha>') (trace_tine Fp tp1)"
        using reduce_tree_trace_tine
        by (simp add: Fwv' tptine)
      have "\<forall>x \<in> set (trace_tine Fwv' tp1). 
\<exists>k. k < size (trace_tine Fwv' tp1) \<and> x = (trace_tine Fwv' tp1) ! k"
        using exist_num_list by fastforce
      hence "\<forall>x \<in> set (trace_tine Fwv' tp1). 
\<exists>k. k < size (trace_tine Fwv' tp1) \<and>
x = (trace_tine Fp tp1) ! k - \<alpha>'"
        using \<open>trace_tine Fwv' tp1 = map (\<lambda>x. x - \<alpha>') (trace_tine Fp tp1)\<close> by fastforce
hence "\<forall>x \<in> set (trace_tine Fwv' tp1). 
x < \<beta> - \<alpha>'"
  by (simp add: \<open>\<forall>x. x \<in> set (trace_tine Fp tp1) \<longrightarrow> \<alpha>' \<le> x\<close> \<open>\<forall>x. x \<in> set (trace_tine Fp tp1) \<longrightarrow> x < \<beta>\<close> \<open>trace_tine Fwv' tp1 = map (\<lambda>x. x - \<alpha>') (trace_tine Fp tp1)\<close> diff_less_mono)
have "(trace_tine Fwv' tp2) = 
map (\<lambda>x. x - \<alpha>') (trace_tine Fp tp2)"
        using reduce_tree_trace_tine
        by (simp add: Fwv' tptine)
      have "\<forall>x \<in> set (trace_tine Fwv' tp2). 
\<exists>k. k < size (trace_tine Fwv' tp2) \<and> x = (trace_tine Fwv' tp2) ! k"
        using exist_num_list by fastforce
      hence "\<forall>x \<in> set (trace_tine Fwv' tp2). 
\<exists>k. k < size (trace_tine Fwv' tp2) \<and>
x = (trace_tine Fp tp2) ! k - \<alpha>'"
        using \<open>trace_tine Fwv' tp2 = map (\<lambda>x. x - \<alpha>') (trace_tine Fp tp2)\<close> length_map by fastforce

   hence "\<forall>x \<in> set (trace_tine Fwv' tp2). 
x \<le> \<beta> - \<alpha>'"
     using \<open>\<forall>x. x \<in> set (trace_tine Fp tp2) \<longrightarrow> x \<le> \<beta>\<close> \<open>trace_tine Fwv' tp2 = map (\<lambda>x. x - \<alpha>') (trace_tine Fp tp2)\<close> by auto
   define tp2cond where 
"tp2cond = (if last (trace_tine Fp tp2) = \<beta> then butlast tp2 else tp2)"
   hence "last (trace_tine Fp tp2cond) < \<beta>"
   proof (cases "last (trace_tine Fp tp2) = \<beta>")
     case True
     hence "(trace_tine Fp tp2) = 
 (trace_tine Fp (tp2cond@[last tp2]))"
       by (metis append_butlast_last_id list.distinct(1) tp1_tp2_def(2) tp2cond_def)
     hence "... = trace_tine Fp (tp2cond) @
    tl (trace_tine (get_subtree Fp (tp2cond)) ([last tp2]))"
       by (metis True append_butlast_last_id append_eq_conv_conj list.distinct(1) tp1_tp2_def(2) tp2cond_def tptine trace_tine_append_trace_tine_subtree)
     hence "... = trace_tine Fp (tp2cond) @ [\<beta>]"
       using True
       by (smt Suc_inject append_butlast_last_id append_eq_append_conv hd_Cons_tl length_Cons list.distinct(1) mod_get_subtree_tines_append_in_tines same_append_eq suc_tine_size_eq_label_list tp1_tp2_def(2) tp2cond_def tptine trace_tine_non_empty)   
     hence "(trace_tine Fp tp2) = (trace_tine Fp (tp2cond)) @ [\<beta>]"
       using \<open>trace_tine Fp (tp2cond @ [last tp2]) = trace_tine Fp tp2cond @ tl (trace_tine (get_subtree Fp tp2cond) [last tp2])\<close> \<open>trace_tine Fp tp2 = trace_tine Fp (tp2cond @ [last tp2])\<close> by auto
     have "size (trace_tine Fp tp2) \<ge> 2"
       using tp1_tp2_def(2) tptine by auto
     hence "(trace_tine Fp tp2) ! (size (trace_tine Fp tp2) - 2) = last (trace_tine Fp (tp2cond))"
       by (simp add: \<open>trace_tine Fp tp2 = trace_tine Fp tp2cond @ [\<beta>]\<close> last_conv_nth trace_tine_non_empty)
     then show ?thesis
       by (metis One_nat_def True add_diff_cancel_left' append_butlast_last_id diff_Suc_Suc last_conv_nth le_eq_less_or_eq le_imp_less_Suc length_append_singleton length_butlast list.simps(3) numeral_2_eq_2 plus_1_eq_Suc strictly_inc_list_as_def strictp(2) suc_tine_size_eq_label_list tp1_tp2_def(2) tptine trace_tine_non_empty)
   next
     case False
     hence "tp2cond = tp2"
       by (simp add: tp2cond_def)
     hence "last (trace_tine Fp tp2) \<noteq> \<beta>"
       by (simp add: False) 
     then show ?thesis
       using \<open>last (trace_tine Fp tp2) \<le> \<beta>\<close> \<open>tp2cond = tp2\<close> by auto 
qed
  have "\<forall>h. h \<in> honest_indices wv  \<longrightarrow>
        depth Fwv' h  \<le> (length tp2cond)"
  proof (cases "last (trace_tine Fp tp2) = \<beta>")
    case True
    hence "last (trace_tine Fwv' tp2) = \<beta> - \<alpha>'"
      by (metis \<open>trace_tine Fwv' tp2 = map (\<lambda>x. x - \<alpha>') (trace_tine Fp tp2)\<close> add_diff_cancel_left' last_conv_nth length_map less_Suc_eq nth_map plus_1_eq_Suc suc_tine_size_eq_label_list tptine trace_tine_non_empty)
    have "\<beta> \<in> honest_indices w"
      using True betahonest
      using \<open>last (trace_tine Fp tp2) = last (trace_tine F t2)\<close> last_conv_nth trace_tine_non_empty by auto
    hence "\<beta> - \<alpha>' \<in> honest_indices (drop \<alpha>' w)"
      using diff_is_0_eq honest_indices_iff by auto
    hence "depth Fwv' (\<beta> - \<alpha>') = depth F \<beta> - size t'"
      using False \<open>div_s w \<le> length wv\<close> \<open>length wv = \<beta> - 1 - \<alpha>'\<close> depthshift dw_def by auto
    hence "depth Fwv' (\<beta> - \<alpha>') = length t2 - size t'"
      using True \<open>last (trace_tine Fp tp2) = last (trace_tine F t2)\<close> \<open>\<beta> \<in> honest_indices w\<close>
      by (metis Fw One_nat_def fork_F forks_def mem_Collect_eq reaching_tine_length_eq_label_depth tinesFt2 tines_def)
    hence "depth Fwv' (\<beta> - \<alpha>') = length tp2"
      by (simp add: Suc_diff_Suc t2gtt' tp1_tp2_def(2))
    have "\<forall>h. h \<in> honest_indices wv  \<longrightarrow>  h <  (\<beta> - \<alpha>')"
      using False \<open>div_s w \<le> length wv\<close> \<open>length wv = \<beta> - 1 - \<alpha>'\<close> dw_def honest_indices_iff by force
    hence "\<forall>h. h \<in> honest_indices wv  \<longrightarrow>  depth Fwv' h <  depth Fwv' (\<beta> - \<alpha>')"
      using \<open>\<beta> - \<alpha>' \<in> honest_indices (drop \<alpha>' w)\<close> \<open>Fwv' \<turnstile> (drop \<alpha>' w)\<close>
\<open>\<And>h. h\<in> honest_indices wv \<Longrightarrow> h \<in> honest_indices (drop \<alpha>' w)\<close>
      by (simp add: fork_F forks_def)
    hence "\<forall>h. h \<in> honest_indices wv  \<longrightarrow>  depth Fwv' h <  length tp2"
      by (simp add: \<open>depth Fwv' (\<beta> - \<alpha>') = length tp2\<close>)
    then show ?thesis
      by (metis Groups.add_ac(2) True \<open>depth Fwv' (\<beta> - \<alpha>') = length t2 - length t'\<close> \<open>depth Fwv' (\<beta> - \<alpha>') = length tp2\<close> diff_diff_left length_Cons length_butlast length_drop less_SucE less_or_eq_imp_le plus_1_eq_Suc tp1_tp2_def(2) tp2cond_def) 
  next
    case False
    then show ?thesis 
      using \<open>\<And>h. h \<in> honest_indices (drop \<alpha>' w) \<and> h < \<beta> - \<alpha>' \<Longrightarrow> depth Fwv' h \<le> min (length tp1) (length tp2)\<close>
      using \<open>\<forall>h. h \<in> honest_indices wv \<longrightarrow> depth Fwv' h \<le> min (length tp1) (length tp2)\<close> tp2cond_def by auto
  qed

have "\<forall>h. h \<in> honest_indices wv  \<longrightarrow>
        depth Fwv' h  \<le> length tp1"
  using \<open>\<forall>h. h \<in> honest_indices wv \<longrightarrow> depth Fwv' h \<le> min (length tp1) (length tp2)\<close> by auto
  have "trace_tine Fwv' tp2cond = map (\<lambda>x. x - \<alpha>') (trace_tine Fp tp2cond)"
    using reduce_tree_trace_tine \<open>last (trace_tine Fp tp2cond) < \<beta> \<close>
    by (simp add: Fwv' butlast_tinesP tp2cond_def tptine)
  hence "last (trace_tine Fwv' tp2cond) = last (trace_tine Fp tp2cond) - \<alpha>'"
    by (simp add: butlast_tinesP last_conv_nth trace_tine_non_empty)
  hence "last (trace_tine Fwv' tp2cond) < \<beta> - \<alpha>'"
    using False \<open>div_s w \<le> length wv\<close> \<open>last (trace_tine Fp tp2cond) < \<beta>\<close> \<open>length wv = \<beta> - 1 - \<alpha>'\<close> diff_is_0_eq' dw_def by linarith

have "trace_tine Fwv' tp1 = map (\<lambda>x. x - \<alpha>') (trace_tine Fp tp1)"
    using reduce_tree_trace_tine 
    by (simp add: Fwv' butlast_tinesP tptine)
  hence "last (trace_tine Fwv' tp1) = last (trace_tine Fp tp1) - \<alpha>'"
    by (simp add: butlast_tinesP last_conv_nth trace_tine_non_empty)
  hence "last (trace_tine Fwv' tp1) < \<beta> - \<alpha>'"
    using \<open>\<forall>x\<in>set (trace_tine Fwv' tp1). x < \<beta> - \<alpha>'\<close> last_in_set trace_tine_non_empty by blast
  have "tp1 /\<cong> tp2cond"
    by (smt \<open>tp1 /\<cong> tp2\<close> butlast.simps(1) butlast.simps(2) equiv.cases equiv_imp list.collapse list.distinct(1) list.sel(1) tp2cond_def)
  have "\<exists> tv1'' tv2''. tv1'' \<in> tines Fwv'' \<and> tv2'' \<in> tines Fwv''
\<and> tv1'' /\<cong> tv2'' \<and> tine_prefix Fwv'' Fwv' tv1'' tp1 \<and> tine_prefix Fwv'' Fwv' tv2'' tp2cond
\<and> size tv1'' = size tp1 \<and> size tv2'' = size tp2cond"
proof - 
    have 0:"strictly_inc Fwv'"
      using \<open>strictly_inc Fwv'\<close> by simp
    have 1:"Fwv'' \<le> Fwv'"
      using \<open>Fwv'' \<le> Fwv'\<close> by simp
    have 2:"\<forall>n. n < \<beta> - \<alpha>' \<longrightarrow> count (mset_of_tree Fwv'') n = count (mset_of_tree Fwv') n"
      using \<open>\<forall>n. n \<le> size (drop \<alpha>' (take (\<beta> - 1) w)) \<longrightarrow> count (mset_of_tree Fwv'') n = count (mset_of_tree Fwv') n\<close>
      by (metis Suc_diff_Suc \<open>length wv = \<beta> - 1 - \<alpha>'\<close> diff_diff_left gr_zeroI le_eq_less_or_eq less_Suc_eq not_less0 plus_1_eq_Suc wv zero_less_diff)
    have "tp1 \<in> tines Fwv' \<and> tp2cond \<in> tines Fwv'"
      by (simp add: Fwv' butlast_tinesP reduce_tree_tines tp2cond_def tptine) 
    thus ?thesis 
    using 0 1 2 exist_non_sharing_prefix_tines 
\<open>last (trace_tine Fwv' tp1) < \<beta> - \<alpha>'\<close> \<open>last (trace_tine Fwv' tp2cond) < \<beta> - \<alpha>'\<close>
\<open>tp1 /\<cong> tp2cond\<close> by auto
qed
  then obtain tv1'' tv2'' where " tv1'' \<in> tines Fwv'' \<and> tv2'' \<in> tines Fwv''
\<and> tv1'' /\<cong> tv2'' \<and> tine_prefix Fwv'' Fwv' tv1'' tp1 \<and> tine_prefix Fwv'' Fwv' tv2'' tp2cond
\<and> size tv1'' = size tp1 \<and> size tv2'' = size tp2cond"
    by auto
  hence   
"\<forall>h. h \<in> honest_indices wv  \<longrightarrow>
        depth Fwv h \<le> length tv1'' \<and> depth Fwv h \<le> length tv2''"
    by (smt Fwv''(1) Fwvhonestunique \<open>Fwv \<le> Fwv''\<close> \<open>\<And>h. h \<in> honest_indices wv \<Longrightarrow> h \<in> honest_indices (drop \<alpha>' w)\<close> \<open>\<forall>h. h \<in> honest_indices wv \<longrightarrow> depth Fwv' h \<le> length tp1\<close> \<open>\<forall>h. h \<in> honest_indices wv \<longrightarrow> depth Fwv' h \<le> length tp2cond\<close> depthshift dual_order.trans prefix_eq_depth_unique)
  hence "dmhwv \<le> length tv1'' \<and> dmhwv \<le> length tv2''"
    by (simp add: \<open>\<forall>h. h \<in> honest_indices wv \<longrightarrow> depth Fwv' h \<le> length tp1\<close> \<open>\<forall>h. h \<in> honest_indices wv \<longrightarrow> depth Fwv' h \<le> length tp2cond\<close> \<open>mhwv \<in> honest_indices wv\<close> \<open>tv1'' \<in> tines Fwv'' \<and> tv2'' \<in> tines Fwv'' \<and> tv1'' /\<cong> tv2'' \<and> tine_prefix Fwv'' Fwv' tv1'' tp1 \<and> tine_prefix Fwv'' Fwv' tv2'' tp2cond \<and> length tv1'' = length tp1 \<and> length tv2'' = length tp2cond\<close> dt(3))
  hence "tinesP Fwv (take dmhwv tv1'') \<and> tinesP Fwv (take dmhwv tv2'')"
    using Fwv \<open>tv1'' \<in> tines Fwv'' \<and> tv2'' \<in> tines Fwv'' \<and> tv1'' /\<cong> tv2'' \<and> tine_prefix Fwv'' Fwv' tv1'' tp1 \<and> tine_prefix Fwv'' Fwv' tv2'' tp2cond \<and> length tv1'' = length tp1 \<and> length tv2'' = length tp2cond\<close> limit_tine_length_tree_take_tine tines_def by auto
  have "height Fwv' \<ge> size (tmhwv')"
    by (simp add: dt(1) size_tine_le_height)
  obtain tmhwv'' where "tmhwv'' \<in> tines Fwv''" "last (trace_tine Fwv'' tmhwv'') = mhwv"
    using Fwv''fork \<open>mhwv \<in> honest_indices wv\<close> exist_honest_tine by blast
  hence "size tmhwv'' = dmhwv"
    by (metis Fwv''(1) Fwv''(2) Max_honest_index_le_size depthshift dt(3) mem_Collect_eq mhwv mhwvdrop prefix_eq_depth_unique reaching_tine_length_eq_label_depth tines_def wv)
  hence "height Fwv'' \<ge> dmhwv"
    using \<open>tmhwv'' \<in> tines Fwv''\<close> size_tine_le_height by blast
  have "height Fwv = min dmhwv (height Fwv'')"
    using  Fwv height_limit_tine_length_tree
    by simp
  hence "height Fwv = dmhwv"
    using \<open>dmhwv \<le> height Fwv''\<close> by linarith
  have "(take dmhwv tv1'') /\<cong> (take dmhwv tv2'')"
    using \<open>dmhwv \<le> length tv1'' \<and> dmhwv \<le> length tv2''\<close> \<open>tv1'' \<in> tines Fwv'' \<and> tv2'' \<in> tines Fwv'' \<and> tv1'' /\<cong> tv2'' \<and> tine_prefix Fwv'' Fwv' tv1'' tp1 \<and> tine_prefix Fwv'' Fwv' tv2'' tp2cond \<and> length tv1'' = length tp1 \<and> length tv2'' = length tp2cond\<close> take_pres_not_equiv by blast
  hence "flat Fwv"
    using \<open>dmhwv \<le> length tv1'' \<and> dmhwv \<le> length tv2''\<close> \<open>height Fwv = dmhwv\<close> \<open>tinesP Fwv (take dmhwv tv1'') \<and> tinesP Fwv (take dmhwv tv2'')\<close> flat.intros tines_def by auto
  hence "forkable wv"
    using \<open>Fwv \<turnstile> wv\<close> forkable.intros by auto
  thus ?thesis
    by (simp add: \<open>flat Fwv\<close>)
qed
  thus ?thesis
    by (metis False \<open>Fwv \<turnstile> wv\<close> \<open>\<beta> - 1 \<le> length w\<close> \<open>div_s w \<le> length wv\<close> \<open>length wv = \<beta> - 1 - \<alpha>'\<close> assms diff_is_0_eq' dw_def forkable.intros le_0_eq nat_le_linear wv)
qed
qed

definition k_cp where  
"k_cp (k::nat) (w:: bool list) = (\<forall>F \<in> forks_set w. \<forall>t1 t2. viable F w t1 \<and> viable F w t2 \<longrightarrow> div_tines F t1 t2 \<le> k)"

lemma "k_cp k w \<equiv> (div_s w \<le> k)"
proof - 
  have "k_cp k w = (\<forall>F \<in> forks_set w. \<forall>t1 t2. viable F w t1 \<and> viable F w t2 \<longrightarrow> div_tines F t1 t2 \<le> k)"
    by (simp add: k_cp_def)
  have "(div_s w \<le> k) = (Max ((\<lambda>x. div_f x w) ` (forks_set w)) \<le> k)"
    by (simp add: div_s_def)
  hence "... = (\<forall>x \<in> ((\<lambda>x. div_f x w) ` (forks_set w)). x \<le> k)"
    using div_f_set_nonempty finite_div_f_set by auto
  hence "... = (\<forall>f \<in> forks_set w. div_f f w \<le> k) "
    by blast
  hence 
"... = (\<forall>F \<in> forks_set w.

Max (\<Union>t1 \<in> tines F.
 (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})) \<le> k)"
    by (simp add: div_f_def)
  hence 
"... = (\<forall>F \<in> forks_set w. \<forall>x \<in> (\<Union>t1 \<in> tines F.
 (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})). x \<le> k)"
    using div_tines_upperbound finite_div_tines_set nonempty_div_tines_set
  proof - 
    have "\<And>F. F \<turnstile> w \<Longrightarrow> Max (\<Union>t1 \<in> tines F.
 (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})) \<le> k
= (\<forall>x \<in> (\<Union>t1 \<in> tines F.
 (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})). x \<le> k)"
    proof - 
      fix F assume "F \<turnstile> w"
      then obtain M where "M \<in> (\<Union>t1 \<in> tines F.
 (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2}))" "M = Max (\<Union>t1 \<in> tines F.
 (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2}))"
        using nonempty_div_tines_set finite_div_tines_set
        by (metis (mono_tags) Max_in)
      hence "(\<forall>x \<in> (\<Union>t1 \<in> tines F.
 (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})). x \<le> M)"
        using finite_div_tines_set by auto
      thus " Max (\<Union>t1 \<in> tines F.
 (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})) \<le> k
= (\<forall>x \<in> (\<Union>t1 \<in> tines F.
 (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})). x \<le> k)"
        using \<open>M = Max (\<Union>t1\<in>tines F. \<Union>t2\<in>tines F. {x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})\<close> \<open>M \<in> (\<Union>t1\<in>tines F. \<Union>t2\<in>tines F. {x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})\<close> order_trans by blast
    qed
    thus ?thesis
      by (simp add: forks_set_def)
  qed
  hence "... = (\<forall>F \<in> forks_set w. (\<forall>t1 t2. viable F w t1 \<and> viable F w t2 \<longrightarrow> div_tines F t1 t2 \<le> k))"
  proof - 
    have "\<And>F. F\<turnstile>w \<Longrightarrow> (\<forall>x \<in> (\<Union>t1 \<in> tines F.
  (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})). x \<le> k) =
      (\<forall>t1 t2. viable F w t1 \<and> viable F w t2 \<longrightarrow> div_tines F t1 t2 \<le> k)"
    proof - 
      fix F
      assume "F\<turnstile> w"
      have "(\<forall>x \<in> (\<Union>t1 \<in> tines F.
  (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})). x \<le> k) \<longrightarrow>
      (\<forall>t1 t2. viable F w t1 \<and> viable F w t2 \<longrightarrow> div_tines F t1 t2 \<le> k)"
      proof (rule ccontr)
        assume "\<not> ((\<forall>x \<in> (\<Union>t1 \<in> tines F.
  (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})). x \<le> k) \<longrightarrow>
      (\<forall>t1 t2. viable F w t1 \<and> viable F w t2 \<longrightarrow> div_tines F t1 t2 \<le> k))"
        hence "(\<forall>x \<in> (\<Union>t1 \<in> tines F.
  (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})). x \<le> k)  
\<and> (\<not> (\<forall>t1 t2. viable F w t1 \<and> viable F w t2 \<longrightarrow> div_tines F t1 t2 \<le> k))"
          by blast
        hence "\<exists>t1 t2. (viable F w t1 \<and> viable F w t2 \<and> \<not> div_tines F t1 t2 \<le> k)"
          by auto
        then obtain t1e t2e where "(viable F w t1e \<and> viable F w t2e \<and> \<not> div_tines F t1e t2e \<le> k)"
          by blast
        hence "t1e \<in> tines F \<and> t2e \<in> tines F"
          using viable.cases by blast
        hence "div_tines F t1e t2e \<in>  (\<Union>t1 \<in> tines F.
  (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2}))"
          using \<open>viable F w t1e \<and> viable F w t2e \<and> \<not> div_tines F t1e t2e \<le> k\<close> by blast
        thus "False"
          using \<open>\<not> ((\<forall>x\<in>\<Union>t1\<in>tines F. \<Union>t2\<in>tines F. {x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2}. x \<le> k) \<longrightarrow> (\<forall>t1 t2. viable F w t1 \<and> viable F w t2 \<longrightarrow> div_tines F t1 t2 \<le> k))\<close> \<open>viable F w t1e \<and> viable F w t2e \<and> \<not> div_tines F t1e t2e \<le> k\<close> by blast
       qed
        have "(\<forall>t1 t2. viable F w t1 \<and> viable F w t2 \<longrightarrow> div_tines F t1 t2 \<le> k) 
\<longrightarrow>
(\<forall>x \<in> (\<Union>t1 \<in> tines F.
  (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})). x \<le> k) "
        proof - 
          have "(\<forall>t1 t2. viable F w t1 \<and> viable F w t2 \<longrightarrow> div_tines F t1 t2 \<le> k) \<Longrightarrow>
            (\<forall>x \<in> (\<Union>t1 \<in> tines F.
            (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})). x \<le> k)"
          proof -
            assume "(\<forall>t1 t2. viable F w t1 \<and> viable F w t2 \<longrightarrow> div_tines F t1 t2 \<le> k)"
            have "\<And>x. x \<in> (\<Union>t1 \<in> tines F.
            (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})) \<Longrightarrow> x \<le> k"
            proof -
              fix x
              assume "x \<in> (\<Union>t1 \<in> tines F.
            (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2}))"
              hence "\<exists>t1 t2. t1 \<in> tines F \<and> t2 \<in> tines F 
\<and>
 viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2"
                by blast
              obtain t1e t2e where "t1e \<in> tines F \<and> t2e \<in> tines F 
\<and>
 viable F w t1e \<and> viable F w t2e \<and> x = div_tines F t1e t2e"
                using \<open>\<exists>t1 t2. t1 \<in> tines F \<and> t2 \<in> tines F \<and> viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2\<close> by blast
              thus "x \<le> k"
                using \<open>\<forall>t1 t2. viable F w t1 \<and> viable F w t2 \<longrightarrow> div_tines F t1 t2 \<le> k\<close> by blast
            qed
            thus "(\<forall>x \<in> (\<Union>t1 \<in> tines F.
            (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})). x \<le> k)"
              by blast
          qed
          thus ?thesis
            by blast
        qed
        thus "(\<forall>x \<in> (\<Union>t1 \<in> tines F.
  (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})). x \<le> k) =
      (\<forall>t1 t2. viable F w t1 \<and> viable F w t2 \<longrightarrow> div_tines F t1 t2 \<le> k)"
          using \<open>(\<forall>x\<in>\<Union>t1\<in>tines F. \<Union>t2\<in>tines F. {x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2}. x \<le> k) \<longrightarrow> (\<forall>t1 t2. viable F w t1 \<and> viable F w t2 \<longrightarrow> div_tines F t1 t2 \<le> k)\<close> by blast
      qed
      hence "\<forall>F \<in> forks_set w. (\<forall>x \<in> (\<Union>t1 \<in> tines F.
  (\<Union>t2 \<in> tines F.{x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2})). x \<le> k) =
      (\<forall>t1 t2. viable F w t1 \<and> viable F w t2 \<longrightarrow> div_tines F t1 t2 \<le> k)"
        by (simp add: forks_set_def)

      thus ?thesis
        by simp
    qed
    thus " k_cp k w \<equiv> div_s w \<le> k "
      using \<open>((MAX x\<in>forks_set w. div_f x w) \<le> k) = (\<forall>x\<in>(\<lambda>x. div_f x w) ` forks_set w. x \<le> k)\<close> \<open>(\<forall>F\<in>forks_set w. Max (\<Union>t1\<in>tines F. \<Union>t2\<in>tines F. {x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2}) \<le> k) = (\<forall>F\<in>forks_set w. \<forall>x\<in>\<Union>t1\<in>tines F. \<Union>t2\<in>tines F. {x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2}. x \<le> k)\<close> \<open>(\<forall>f\<in>forks_set w. div_f f w \<le> k) = (\<forall>F\<in>forks_set w. Max (\<Union>t1\<in>tines F. \<Union>t2\<in>tines F. {x. viable F w t1 \<and> viable F w t2 \<and> x = div_tines F t1 t2}) \<le> k)\<close> \<open>(\<forall>x\<in>(\<lambda>x. div_f x w) ` forks_set w. x \<le> k) = (\<forall>f\<in>forks_set w. div_f f w \<le> k)\<close> \<open>(div_s w \<le> k) = ((MAX x\<in>forks_set w. div_f x w) \<le> k)\<close> \<open>k_cp k w = (\<forall>F\<in>forks_set w. \<forall>t1 t2. viable F w t1 \<and> viable F w t2 \<longrightarrow> div_tines F t1 t2 \<le> k)\<close> by presburger
  qed
 


lemma honest_slot :
  "(1 - (1 - \<epsilon>) / 2) =  (1 + \<epsilon>)/2"
proof -
  have "1 - (1 + - 1 * \<epsilon>) / 2 = 1 + - 1 * ((1 + - 1 * \<epsilon>) / 2)"
    by algebra
  then have "1 - (1 - \<epsilon>) / 2 = 1 + - 1 * ((1 + - 1 * \<epsilon>) / 2)"
    by force
  then show ?thesis
    by auto
qed

fun rev_m :: "bool list \<Rightarrow> int \<times> int" 
  where "rev_m [] = (0,0)"
  |"rev_m (True#w) = (fst (rev_m w) + 1, snd (rev_m w) + 1)"
  |"rev_m (False#w) = (if fst (rev_m w) > snd (rev_m w) \<and> snd (rev_m w) = 0
  then (fst (rev_m w) - 1, 0)
  else 
    (if fst (rev_m w) = 0
    then (0, snd (rev_m w) - 1)
    else (fst (rev_m w) - 1, snd (rev_m w) - 1)))"

lemma rev_m_m_rev:"rev_m l = m (rev l)" 
proof (induction l)
  case Nil
  have 0: "rev_m [] = (0,0)"
    by simp
  have "m [] = (0,0)"
    by (simp add: recursive_margin_Nil)
  then show ?case using 0
    by simp
next
  case (Cons k t)
  hence x:"rev_m t = m (rev t)"
    by simp
  then show ?case
  proof (cases k)
    case True
    then show ?thesis using  theorem_4_19_True_part
      by (simp add: Cons.IH m_def)
next
  case False
  note f = this
  thm f
  then show ?thesis 
  proof (cases "rho_s (rev t) > margin_s (rev t) \<and> margin_s (rev t) = 0")
    case True
    hence 1:"m ((rev t)@[False])= (rho_s (rev t) - 1, 0)"
      using theorem_4_19_False_part
      by simp 
    hence "fst (rev_m t) > snd (rev_m t) \<and> snd (rev_m t) = 0"
      using True by (simp add: Cons.IH m_def)
    then show ?thesis using 1
      by (simp add: Cons.IH False m_def) 
  next
    case False
    note ff=this
    thm ff
    hence 2:"\<not> (rho_s (rev t) > margin_s (rev t) \<and> margin_s (rev t) = 0)"
      by auto
    hence 22:"\<not> (fst (rev_m t) > snd (rev_m t) \<and> snd (rev_m t) = 0)"
      by (simp add: m_def x)
    then show ?thesis using 2 
    proof (cases "rho_s (rev t) = 0")
      case True
      hence 3:"m ((rev t)@[False])= (0, margin_s (rev t) - 1)"
        using f ff theorem_4_19_False_part 2
        by simp
    hence "fst (rev_m t) = 0"
      using True
      by (simp add: m_def x) 
      then show ?thesis using 3
        by (simp add: f m_def x)
    next
      case False
      note fff = this
      thm fff
      hence 4:"m ((rev t)@[False])= (rho_s (rev t) - 1, margin_s (rev t) - 1)"
        using ff theorem_4_19_False_part
        by simp
      hence "\<not> (fst (rev_m t) = 0)"
        using fff
        by (simp add: m_def x) 
    then show ?thesis 
      using 4 x
      by (simp add: f ff m_def) 
    qed
  qed
qed
qed

definition rev_\<rho> :: "bool list \<Rightarrow> int" where
  "rev_\<rho> w = fst (rev_m w)"

definition rev_\<mu> :: "bool list \<Rightarrow> int" where
  "rev_\<mu> w = snd (rev_m w)"

definition rev_\<mu>_bar :: "bool list \<Rightarrow> int" where
  "rev_\<mu>_bar w = min 0 (rev_\<mu> w)"

fun wn :: "nat \<Rightarrow> (bool list) pmf" where
   "wn 0 = return_pmf []"
 | "wn (Suc n) = map_pmf (\<lambda>(x,y). x # y) (pair_pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (wn n))"

definition w_n_pmf :: "nat pmf" where
 "w_n_pmf = map_pmf (\<lambda>b. (if b then 1 else 0)) (bernoulli_pmf ((1-\<epsilon>)/2))"

definition W_n_pmf :: "int pmf" where 
 "W_n_pmf = bind_pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (\<lambda>b. return_pmf ((-1) ^ (if b then 0 else 1)))"

definition w_i_pmf :: "nat \<Rightarrow> (nat \<Rightarrow> bool) pmf" where "w_i_pmf n = Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))"

definition w_pmf where "w_pmf n = map_pmf (\<lambda>f. map f [0..<n]) (w_i_pmf n)"

lemma "0 \<le> p \<Longrightarrow> p \<le> 1 \<Longrightarrow> measure (bernoulli_pmf p) {} =  0"
  by (simp add: measure_pmf_single)

lemma "0 \<le> p \<Longrightarrow> p \<le> 1 \<Longrightarrow> measure (bernoulli_pmf p) {True} =  p"
  by (smt ep_gt_0 ep_le_1 field_sum_of_halves pmf.rep_eq pmf_bernoulli_True)
 
lemma "0 \<le> p \<Longrightarrow> p \<le> 1 \<Longrightarrow> measure (bernoulli_pmf p) {False} =  1 - p"
  by (smt ep_gt_0 ep_le_1 field_sum_of_halves pmf.rep_eq pmf_bernoulli_False)

lemma "0 \<le> p \<Longrightarrow> p \<le> 1 \<Longrightarrow> measure (bernoulli_pmf p) {True,False} =  1"
  by (metis UNIV_bool insert_commute measure_pmf_UNIV)

lemma "\<alpha> \<ge> 1"
proof -
  have "1 + \<epsilon> \<ge> 2 * \<epsilon>"
    using ep_le_1 by simp
  have "2 * \<epsilon> > 0"
    using ep_gt_0 by simp
  thus ?thesis using \<open>1 + \<epsilon> \<ge>  2 * \<epsilon>\<close>
    by (simp add: \<open>\<alpha> = (1 + \<epsilon>) / (2 * \<epsilon>)\<close>)
qed

lemma "measure_pmf.expectation (W_n_pmf) (real_of_int) = -\<epsilon>"
proof -
  have 1:"finite {True, False}"
    by simp
  have 2:"\<And>x. x \<in> {True, False} \<Longrightarrow> finite (set_pmf ((\<lambda>b. return_pmf ((-1) ^ (if b then 0 else 1))) x))"
    by auto
  have "set_pmf (bernoulli_pmf ((1-\<epsilon>)/2)) \<subseteq> {True, False}"
    by (simp add: subsetI)
  hence "measure_pmf.expectation ((bernoulli_pmf ((1-\<epsilon>)/2)) \<bind> (\<lambda>b. return_pmf ((-1) ^ (if b then 0 else 1)))) (real_of_int) = 
      (\<Sum> a\<in> {True, False}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) a *\<^sub>R 
      measure_pmf.expectation ((\<lambda>b. return_pmf ((-1) ^ (if b then 0 else 1))) a) real_of_int)"
    apply-
    apply (rule pmf_expectation_bind)
    by auto
  hence "measure_pmf.expectation (W_n_pmf) (real_of_int) = 
      (\<Sum> a\<in> {True, False}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) a *\<^sub>R 
      measure_pmf.expectation ((\<lambda>b. return_pmf ((-1) ^ (if b then 0 else 1))) a) real_of_int)"
    using W_n_pmf_def by simp
  also have " \<dots> = pmf (bernoulli_pmf ((1-\<epsilon>)/2)) True *\<^sub>R 
      measure_pmf.expectation ((\<lambda>b. return_pmf ((-1) ^ (if b then 0 else 1))) True) real_of_int
            + pmf (bernoulli_pmf ((1-\<epsilon>)/2)) False *\<^sub>R 
      measure_pmf.expectation ((\<lambda>b. return_pmf ((-1) ^ (if b then 0 else 1))) False) real_of_int"
    by simp
  also have "\<dots> = ((1-\<epsilon>)/2) *\<^sub>R (measure_pmf.expectation ((\<lambda>b. return_pmf ((-1) ^ (if b then 0 else 1))) True) real_of_int) 
              +  (1 - (1-\<epsilon>)/2) *\<^sub>R (measure_pmf.expectation ((\<lambda>b. return_pmf ((-1) ^ (if b then 0 else 1))) False) real_of_int)"
    using pmf_bernoulli_True pmf_bernoulli_False
    using ep_gt_0 ep_le_1 by auto
  also have "\<dots> = - \<epsilon>"
    by simp
  finally show ?thesis by simp
qed

lemma finite_False_func_set:
"finite {f. \<forall>x. x \<notin> {..<(n::nat)} \<longrightarrow> f x = False}"
proof (induction n)
  case 0
  have "{..<(0::nat)} = {x. x < (0::nat)}"
    by (simp add: lessThan_def)
  hence "{..<(0::nat)} = {}"  by simp
  hence "\<And>f. 
        (\<forall>x. x \<notin> {..<(0::nat)} \<and> f x = False) \<Longrightarrow> 
          f = (\<lambda>y. False)"
    by auto
  have"{f. \<forall>x. x \<notin> {..<(0::nat)} \<longrightarrow> f x = False} 
           = {(\<lambda>y. False)}"
        by fastforce
  then show ?case by simp
next
  case (Suc n)
  hence "finite {f. \<forall>x. x \<notin> {..<(n::nat)} \<longrightarrow> f x = False}"
    by auto
  have "{f. \<forall>x. x \<notin> {..<(n::nat)} \<longrightarrow> f x = False} = {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False}"
    by auto
  have "{f. \<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False} 
            = {f. \<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False \<and> (f n = True \<or> f n = False)}"
    by simp
  also have "...
            = {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True) 
                \<or> ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = False)}"
    by auto
 also have "...
            = {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True)} 
                \<union> {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = False)}"
   by (simp add: Collect_disj_eq)
    also have "...
            = {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True)} 
                \<union> {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False}"
      by (metis (no_types, hide_lams) atLeast_Suc_greaterThan atLeast_iff greaterThan_def le_less mem_Collect_eq)
    finally have fin:"{f. \<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False} =
{f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True)} 
                \<union> {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False}"
      by blast 
    have "card {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} = card {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True)}"
    proof -
      define g where g:"g = (\<lambda>f x. if x = n then True else f x)"
      define h where h:"h = (\<lambda>f x. if x = n then False else f x)"
      have "\<forall>x y. (x\<in>{f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} \<and> y\<in>{f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} \<and>
                    g x = g y) \<longrightarrow> x = y"
      proof (rule ccontr)
        assume "\<not> (\<forall> x y. (x\<in>{f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} \<and> y\<in>{f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False}
                  \<and> g x = g y) \<longrightarrow> x = y)"
        then obtain x y where 
asm1:"x\<in>{f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} \<and> y\<in>{f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} \<and> g x = g y \<and> x \<noteq> y"
          by blast
        then obtain z where z:"x z \<noteq> y z"
          using asm1 by blast
        have "\<forall>w h. w \<noteq> n \<longrightarrow> g h w = h w"
          using g
          by simp
        hence "z = n"
          using z asm1
          by metis
        hence "x n \<noteq> y n"
          using z by auto
        hence "x n = True \<or> y n = True"
          by auto
        hence "x\<notin>{f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} \<or> y\<notin>{f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False}"
          by blast
        thus "False"
          using asm1 by linarith
      qed
      hence "inj_on g {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False}"
        by (meson inj_onI)
      have "g ` {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} =  
            {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True)}"
      proof - 
        have "\<forall>x. x \<in> {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False}  
            \<longrightarrow> g x \<in> {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True)}"
          by (simp add: g)
        hence sub:"g ` {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} \<subseteq>  
            {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True)}"
          by auto
        have "\<And>x. x n \<Longrightarrow> \<forall>k. g (h x) k= x k"
          by (simp add: g h)
        hence "\<And>x. x n \<Longrightarrow> g (h x) = x"
          by (simp add: g h)
        hence exist:"\<And>x. \<forall>xa\<ge>Suc n. \<not> x xa \<Longrightarrow> x n \<Longrightarrow> (\<forall>z\<ge>n. \<not> (h x) z) \<and> g (h x) = x"
          using h by auto
        hence "\<forall>x. x \<in> {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True)} 
         \<longrightarrow> (\<exists>y. y \<in> {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} \<and> g y = x)"
          apply simp
          apply clarify
          by metis
        thus ?thesis using sub
          using subset_antisym by auto
      qed
      hence "bij_betw g {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True)}"
        using \<open>inj_on g {f. \<forall>x. x \<in> {n..} \<longrightarrow> f x = False}\<close> inj_on_imp_bij_betw by force
      thus ?thesis
        using bij_betw_same_card by blast
    qed 
    hence tmp:"card {f. \<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False}
        \<le> card  {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True)} + card {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False}"
      using fin
      by (metis (mono_tags, lifting) card_Un_le)
    have "card {f. \<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False} \<noteq> 0"
    proof - 
      define f where "f = (\<lambda>x::nat. False)"
      have "f \<in> {f. \<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False}"
        by (simp add: f_def)
      thus ?thesis
        by (smt Suc.IH \<open>card {f. \<forall>x. x \<in> {n..} \<longrightarrow> f x = False} = card {f. (\<forall>x. x \<in> {Suc n..} \<longrightarrow> f x = False) \<and> f n = True}\<close>
 \<open>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False} = {f. \<forall>x. x \<in> {n..} \<longrightarrow> f x = False}\<close> card_eq_0_iff empty_iff f_def fin finite_UnI mem_Collect_eq)
    qed
    hence "finite {f. \<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False}" using tmp card_infinite
      by fastforce
    then show ?case 
    proof -
       have "{f. \<forall>x. x \<notin> {..<(Suc n::nat)} \<longrightarrow> f x = False} = {f. \<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False}"
         by auto
       thus ?thesis
         using \<open>finite {f. \<forall>x. x \<in> {Suc n..} \<longrightarrow> f x = False}\<close> by auto
     qed
   qed

lemma finite_set_w_i_pmf:"finite (set_pmf (w_i_pmf n))"
proof -
  have "set_pmf (Pi_pmf {..<n::nat} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) \<subseteq> {f. \<forall>x. x \<notin> {..<n::nat} \<longrightarrow> f x = False}"
    apply (rule set_Pi_pmf_subset)
    by auto
  thus ?thesis using finite_False_func_set
  proof -
    show ?thesis
      by (metis (no_types) \<open>\<And>n. finite {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}\<close> \<open>set_pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1 - \<epsilon>) / 2))) \<subseteq> {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}\<close> infinite_super w_i_pmf_def)
  qed
qed

lemma expectation_w_pmf_i_minus_ep:
  assumes "i < n" 
  shows "measure_pmf.expectation (w_pmf n) (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1)) \<circ> (\<lambda>l. nth l i)) = -\<epsilon>"
proof -
  have "measure_pmf.expectation (w_pmf n) (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1)) \<circ> (\<lambda>l. nth l i)) 
        = measure_pmf.expectation (map_pmf (\<lambda>f. map f [0..<n]) (w_i_pmf n)) 
          (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1)) \<circ> (\<lambda>l. nth l i))"
    using w_pmf_def by simp
  also have "... 
        = measure_pmf.expectation (bind_pmf (w_i_pmf n) (\<lambda>x. return_pmf (map x [0..<n]))) 
          (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1)) \<circ> (\<lambda>l. nth l i))"
    using w_i_pmf_def
    by (simp add: map_pmf_def)
  also have "... = 
           (\<Sum>a\<in> {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
            measure_pmf.expectation ((\<lambda>x. return_pmf (map x [0..<n])) a) 
            (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1)) \<circ> (\<lambda>l. nth l i)))"
    apply -
    apply (rule pmf_expectation_bind)
    using finite_False_func_set apply auto[1]
    apply simp
    by (smt Collect_mono_iff finite_lessThan set_Pi_pmf_subset set_pmf_eq w_i_pmf_def)
   also have "... = 
           (\<Sum>a\<in> {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
            (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1))) (a i))"
     using nth_map assms by simp
also have "... = 
           (\<Sum>a\<in> {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
            (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1))) (a i))"
  using nth_map assms by simp
  finally have "measure_pmf.expectation (w_pmf n) (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1)) \<circ> (\<lambda>l. nth l i)) = 
           (\<Sum>a\<in> {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
            (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1))) (a i))"
    using nth_map assms
    by (simp add: \<open>measure_pmf.expectation (local.w_i_pmf n \<bind> (\<lambda>x. return_pmf (map x [0..<n]))) (real_of_int \<circ> (\<lambda>b. if b then 1 else - 1) \<circ> (\<lambda>l. l ! i)) = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (local.w_i_pmf n) a *\<^sub>R measure_pmf.expectation (return_pmf (map a [0..<n])) (real_of_int \<circ> (\<lambda>b. if b then 1 else - 1) \<circ> (\<lambda>l. l ! i)))\<close> map_pmf_def w_pmf_def)
  also have "...  =
           (\<Sum>a\<in> {f. \<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
            (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1))) (a i))"
    using assms
  by (metis (no_types, lifting) ivl_disj_un_one(1) ivl_disj_un_singleton(2)) 
  also have  "... = 
           (\<Sum>a\<in> {f. \<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False \<and> (f i = False \<or> f i =True)}.
            pmf (w_i_pmf n) a *\<^sub>R (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1))) (a i))"
    by simp
    also have "... = 
           (\<Sum>a\<in> {f. ((\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False) \<or>
            ((\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True)}.
            pmf (w_i_pmf n) a *\<^sub>R (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1))) (a i))"
      by (metis (mono_tags, hide_lams))
      also have "... = 
           (\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False} \<union>
            {f.(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
            pmf (w_i_pmf n) a *\<^sub>R (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1))) (a i))"
        by (simp add: Collect_disj_eq)
     also have "... = 
           (\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
          pmf (w_i_pmf n) a *\<^sub>R (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1))) (a i))
            + 
          (\<Sum>a\<in>  {f.(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
            pmf (w_i_pmf n) a *\<^sub>R (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1))) (a i))"
     proof - 
       have interemp:"{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False} \<inter> 
              {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True} = {}" 
       proof (rule ccontr)
         assume "\<not> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False} \<inter> 
              {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True} = {}"
         then obtain f where f:"f \<in>
{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False} \<inter> 
              {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}"
           by auto
         hence "f i = False"
           using assms by fastforce
         thus "False" using f assms by fastforce
       qed
       have fin:"finite {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False} "
         using finite_False_func_set
         by simp
       have "{f. (\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and> f i = False} \<subseteq> {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False} \<and>
              {f. (\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and> f i = True} \<subseteq> {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}"
         by blast
       hence "finite {f. (\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and> f i = False} \<and> 
              finite {f. (\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and> f i = True}"
         using fin
         using finite_subset by auto
hence "finite {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False} \<and> 
              finite {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}"
proof -
  have f1: "(\<lambda>p. (\<forall>na. na \<notin> {..<n} \<longrightarrow> \<not> p na) \<and> p i) = (\<lambda>p. (\<forall>na. na \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> \<not> p na) \<and> p i)"
    by (metis (lifting) assms ivl_disj_un_one(1) ivl_disj_un_singleton(2))
  have "(\<lambda>p. (\<forall>na. na \<notin> {..<n} \<longrightarrow> \<not> p na) \<and> \<not> p i) = (\<lambda>p. (\<forall>na. na \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> \<not> p na) \<and> \<not> p i)"
by (metis (lifting) assms ivl_disj_un_one(1) ivl_disj_un_singleton(2))
  then show ?thesis
    using f1 \<open>finite {f. (\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and> f i = False} \<and> finite {f. (\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and> f i = True}\<close> by presburger
qed
  thus ?thesis using infinite_Un sum.union_disjoint interemp
    by (simp add: sum.union_disjoint)
qed

also have "... = 
           (\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
           - pmf (w_i_pmf n) a)
            + 
          (\<Sum>a\<in>  {f.(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
            pmf (w_i_pmf n) a)"
  by simp
  also have "... = 
           (\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
           - (\<Prod>x\<in>{..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))
            + 
          (\<Sum>a\<in>  {f.(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
            (\<Prod>x\<in>{..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
proof - 
  have x:"\<And>a. a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False} \<Longrightarrow>
pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a 
= (if \<forall>x. x \<notin> {..<n} \<longrightarrow> a x = False then (\<Prod>x\<in>{..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)) x)  (a x)) else 0)"
    apply - 
    apply (rule pmf_Pi)
    by simp
  hence F:"\<And>a. a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False} \<Longrightarrow>
pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a 
= (\<Prod>x\<in>{..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)) x)  (a x))"
  proof - 
    fix a 
    assume "a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}"
    have "\<forall>x. x \<notin> {..<n} \<longrightarrow> a x = False"
      using \<open>a \<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}\<close> assms by auto   
    thus "pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a 
= (\<Prod>x\<in>{..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)) x)  (a x))"
      using x \<open>a \<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}\<close> by auto
  qed
 have y:"\<And>a. a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True} \<Longrightarrow>
pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a 
= (if \<forall>x. x \<notin> {..<n} \<longrightarrow> a x = False then (\<Prod>x\<in>{..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)) x)  (a x)) else 0)"
    apply - 
    apply (rule pmf_Pi)
    by simp
  hence T:"\<And>a. a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True} \<Longrightarrow>
pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a 
= (\<Prod>x\<in>{..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)) x)  (a x))"
  proof - 
    fix a 
    assume "a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}"
    have "\<forall>x. x \<notin> {..<n} \<longrightarrow> a x = False"
      using \<open>a \<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}\<close> assms by auto   
    thus "pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a 
= (\<Prod>x\<in>{..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)) x)  (a x))"
      using y \<open>a \<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}\<close> by auto
  qed
  thus ?thesis
    using F w_i_pmf_def by auto 
qed
 also have "... = 
           (\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
           - (\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))
            + 
          (\<Sum>a\<in>  {f.(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
            (\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
   by (metis (no_types, lifting) assms ivl_disj_un_one(1) ivl_disj_un_singleton(2) sum.cong)
 also have "... = 
           (\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
           - (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a i))
            + 
          (\<Sum>a\<in>  {f.(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
            (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a i))"
 proof - 
   have "{i} \<inter> ({..<i} \<union> {i<..<n}) = {}"
     by simp
   hence "\<forall>f. (\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. f x) = (\<Prod>x\<in> {..<i} \<union> {i<..<n}. f x) * f i"
     by (simp add: mult.commute)
   thus ?thesis
     by (smt Groups.mult_ac(2) mult_minus_right sum.cong)
 qed
 also have "... = 
           (\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
           - (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *(1- (1-\<epsilon>)/2))
            + 
          (\<Sum>a\<in>  {f.(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
            (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * ((1-\<epsilon>)/2))"
   using ep_gt_0 ep_le_1 by auto
also have "... = 
           (\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
           - (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *((1+\<epsilon>)/2))
            + 
          (\<Sum>a\<in>  {f.(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
            (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * ((1-\<epsilon>)/2))"
   using ep_gt_0 ep_le_1
   by (simp add: honest_slot) 
also have "... = 
           - (1+\<epsilon>)/2 + 
          (\<Sum>a\<in>  {f.(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
            (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * ((1-\<epsilon>)/2))"
proof - 
  have smp:"{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}=
            {f. \<forall>x. x \<notin> {..<i} \<union> {i<..<n} \<longrightarrow> f x = False}"
    by auto
  have "\<And>a. a \<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False} \<Longrightarrow>
          pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a 
        = (if \<forall>x. x \<notin> {..<i} \<union> {i<..<n} \<longrightarrow> a x = False then (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))
   else 0)"
    apply -
    apply (rule pmf_Pi)
    by blast
  hence pmfpi:"\<And>a. a \<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False} \<Longrightarrow>
          pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a 
        = (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))"
    by auto
  have sbst: "set_pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) \<subseteq> 
{f. (\<forall>x. x \<notin> {..<i} \<union> {i<..<n} \<longrightarrow> f x = False)}"
    apply -
    apply (rule set_Pi_pmf_subset)
    by blast
  then obtain C where C:"set_pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) \<union> C = 
{f. (\<forall>x. x \<notin> {..<i} \<union> {i<..<n} \<longrightarrow> f x = False)} \<and> 
set_pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) \<inter> C = {}"
    by (smt Diff_disjoint Un_Diff_cancel sup.absorb_iff2)
  have fin:"finite {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False} "
         using finite_False_func_set
         by simp
    have "{f. (\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and> f i = False} \<subseteq> {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}"
         by blast
    hence "finite {f. (\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and> f i = False}"
         using fin
         using finite_subset by auto
   hence "finite {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}"
  by (smt Collect_cong assms ivl_disj_un_one(1) ivl_disj_un_singleton(2))
  hence finC: "finite (set_pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)))) \<and> finite C"
      using C
      by (metis infinite_Un smp)
  hence C0: "\<forall>a. a \<in> C \<longrightarrow> pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a = 0"
    using C by (smt disjoint_iff_not_equal mem_Collect_eq set_pmf_eq) 
  hence "(\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
        (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) =
(\<Sum>a\<in> set_pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) \<union> C.
      pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a)"
    using smp C sbst pmfpi by auto
  also have "... = 
(\<Sum>a\<in> set_pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))).
      pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a)
+ 
(\<Sum>a\<in> C. pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a)"
      using C sum.union_disjoint
      by (smt \<open>finite (set_pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1 - \<epsilon>) / 2)))) \<and> finite C\<close>) 
 also have "... = 1 + 
(\<Sum>a\<in> C. pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a)"
   using sum_pmf_eq_1
   by (simp add: sum_pmf_eq_1 \<open>finite (set_pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1 - \<epsilon>) / 2)))) \<and> finite C\<close>)
  finally have sum1:"(\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
 (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) = 1"
    using C0 finC comm_monoid_add_class.sum.neutral_const
    by simp
  have "(\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
           - (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *((1+\<epsilon>)/2)) =
       (\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
           (-1) * (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *((1+\<epsilon>)/2))" 
    by simp
have "(\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
           - (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *((1+\<epsilon>)/2)) =
       (-1) * (\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
            (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) *((1+\<epsilon>)/2)"
  by (simp add: sum_distrib_left sum_distrib_right)  
  thus ?thesis
  proof -
    show ?thesis
      by (metis (no_types) \<open>(\<Sum>a\<in>{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}. - (\<Prod>x\<in>{..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * ((1 + \<epsilon>) / 2)) = - 1 * (\<Sum>a\<in>{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}. \<Prod>x\<in>{..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * ((1 + \<epsilon>) / 2)\<close> divide_minus_left mult.left_neutral mult_minus_left sum1)
  qed
qed
also have "... = 
           - (1+\<epsilon>)/2 + (1-\<epsilon>)/2"
proof -
  have pmfpi1:"\<And>a. pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) a        
= (if \<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> a x = False 
then (\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))  (a x))
   else 0)"
    apply -
    apply (rule pmf_Pi)
    by blast
hence pmfpi2:"\<And>a. a \<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True} \<Longrightarrow>
          pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) a 
        = (\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (a x))"
  by auto
  have sbst': "set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) \<subseteq> 
{f. \<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False}"
    apply -
    apply (rule set_Pi_pmf_subset)
    by blast
  have notin:"\<And>f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False \<Longrightarrow> f \<notin> set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)))"
  proof -
    fix f
    assume asm:"(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False"
    have 
"(\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. 
pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (f x)) = 
(\<Prod>x\<in> {..<i} \<union> {i<..<n} \<union> {i}.
pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (f x))" by auto
    also have "... = 
(\<Prod>x\<in> {..<i} \<union> {i<..<n}.
pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (f x))
* (\<Prod>x\<in> {i}.
pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (f x))"
      by simp
also have "... = 
(\<Prod>x\<in> {..<i} \<union> {i<..<n}.
pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (f x))
* pmf (return_pmf True) (f i)"
  by simp
  finally have 0:"(\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. 
pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (f x)) = 0"
    using asm pmf_return by auto
  have "pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) f 
    =  (\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (f x))"
    using pmfpi1
    by (simp add: asm)
      thus "f \<notin> set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)))"
        using 0
        by (simp add: set_pmf_eq)
    qed
hence same:"(set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
          (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)))) 
- {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}  
= (set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
          (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))))"
      by blast
      have "set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)))
- 
{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}
\<subseteq>
{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False)} - 
{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}" using sbst'
      using notin  by (simp add: Diff_mono)
hence "set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)))
\<subseteq>
{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False)} - 
{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}" using sbst'
  using same by simp
hence "set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)))
\<subseteq>
{f. f\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False)} 
\<and> f \<notin>{ f.(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}}" using sbst'
  by blast
hence "set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)))
\<subseteq> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) 
\<and> f i = True}" using sbst'
  by blast  
then obtain C' where C':"set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) \<union> C' = 
{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) 
\<and> f i = True} \<and> 
set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) \<inter> C' = {}"
  by (smt Diff_disjoint Un_Diff_cancel sup.absorb_iff2)
  have fin:"finite {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False} "
         using finite_False_func_set
         by simp
    have "{f. (\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and> f i = True} \<subseteq> {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}"
         by blast
    hence "finite {f. (\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and> f i = True}"
         using fin
         using finite_subset by auto
   hence "finite {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}"
  by (smt Collect_cong assms ivl_disj_un_one(1) ivl_disj_un_singleton(2))
  hence finC': "finite (set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)))) \<and> finite C'"
    using C'
    by (metis (no_types, lifting) infinite_Un)
  hence C0': "\<forall>a. a \<in> C' \<longrightarrow> pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) a = 0"
    using C'
    by (meson disjoint_iff_not_equal pmf_eq_0_set_pmf)
  hence "(\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
        (\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) =
(\<Sum>a\<in> set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) \<union> C'.
      pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) a)"
    using C' sbst' pmfpi2 by auto
  also have "... = 
(\<Sum>a\<in> set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))).
      pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) a)
+ 
(\<Sum>a\<in> C'. pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) a)"
    using C' sum.union_disjoint
    by (smt finC')
 also have "... = 1 + 
(\<Sum>a\<in> C'. pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) a)"
     using sum_pmf_eq_1
     using finC' by auto
     finally have sum1':"(\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
        (\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) = 1"
    using C0' finC' comm_monoid_add_class.sum.neutral_const
    by simp
  have smp'':"(\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
        (\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
= 
(\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
(\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
     using pmf_return
   proof - 
     have "\<And>a x. a \<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True} \<and> x\<in> {..<i} \<union> {i<..<n} \<Longrightarrow>
 pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (a x) =
pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)"
       by auto
     thus ?thesis by simp
   qed
   thus ?thesis
       by (metis (no_types) mult.left_neutral smp'' sum1' sum_distrib_right)
   qed
   finally have "measure_pmf.expectation (w_pmf n) (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1)) \<circ> (\<lambda>l. nth l i)) 
        = - \<epsilon>"
     using ep_gt_0 ep_le_1
proof -
  assume a1: "measure_pmf.expectation (local.w_pmf n) (real_of_int \<circ> (\<lambda>b. if b then 1 else - 1) \<circ> (\<lambda>l. l ! i)) = - (1 + \<epsilon>) / 2 + (1 - \<epsilon>) / 2"
  have f2: "\<forall>r ra. (ra::real) + r = r + ra"
    by linarith
have f3: "- ((1 + - \<epsilon>) / (1 + 1)) = (\<epsilon> + - 1) / (1 + 1)"
  using honest_slot by auto
  have "\<forall>r ra. - (ra::real) + (r + ra) = r"
    by auto
  then show ?thesis
    using f3 f2 a1 by (metis (no_types) add.inverse_distrib_swap add_uminus_conv_diff one_add_one real_average_minus_second)
qed
     
   thus ?thesis
     by simp
 qed

definition \<Phi>_n :: "nat \<Rightarrow> bool list \<Rightarrow> real" where
  "\<Phi>_n n l= (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (drop (size l - n) l))"

definition \<Phi>_n' :: "nat \<Rightarrow> bool list \<Rightarrow> real" where
  "\<Phi>_n' n l= (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (drop (size l - n) l)) + n * \<epsilon>"

definition \<Delta>_n :: "nat \<Rightarrow> bool list \<Rightarrow> real" where
  "\<Delta>_n n l = \<Phi>_n n l - \<Phi>_n (n-1) l"

definition \<Delta>_n' :: "nat \<Rightarrow> bool list \<Rightarrow> real" where
  "\<Delta>_n' n l = \<Phi>_n n l - \<Phi>_n (n-1) l + \<epsilon>"

lemma rev_rho_s_ge_0: "fst (rev_m l) \<ge> 0"
proof - 
  have "rev_m l = m (rev l)"
    by (simp add: rev_m_m_rev)
  hence "fst (rev_m l) = rho_s (rev l)"
    using m_def by auto
  thus ?thesis
    using rho_s_ge_0 by auto
qed

lemma rev_margin_le_rho_s: "fst (rev_m l) \<ge> snd (rev_m l)"
  using margin_le_rho_s
  by (simp add: m_def rev_m_m_rev)

lemma four_cases: "{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}
= {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (map f [0..<n])) > 0) \<and> (snd (rev_m (map f [0..<n])) < 0)} \<union>
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (map f [0..<n])) > 0) \<and> (snd (rev_m (map f [0..<n])) \<ge> 0)} \<union> 
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (map f [0..<n])) = 0) \<and> (snd (rev_m (map f [0..<n])) < 0)} \<union> 
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (map f [0..<n])) = 0) \<and> (snd (rev_m (map f [0..<n])) = 0)}"
proof -
  have "\<And>l . ((fst (rev_m l) > 0) \<and> (snd (rev_m l) < 0)) \<or>
((fst (rev_m l) > 0) \<and> (snd (rev_m l) \<ge> 0)) \<or>
((fst (rev_m l) = 0) \<and> (snd (rev_m l) < 0)) \<or>
((fst (rev_m l) = 0) \<and> (snd (rev_m l) = 0))"
    by (smt rev_margin_le_rho_s rev_rho_s_ge_0)
  hence "{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False} =
{f. ((\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (map f [0..<n])) > 0) \<and> (snd (rev_m (map f [0..<n])) < 0)))
\<or> 
((\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (map f [0..<n])) > 0) \<and> (snd (rev_m (map f [0..<n])) \<ge> 0))) 
\<or>
((\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (map f [0..<n])) = 0) \<and> (snd (rev_m (map f [0..<n])) < 0))) 
\<or>
((\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (map f [0..<n])) = 0) \<and> (snd (rev_m (map f [0..<n])) = 0)))}"
    by blast
  also have "... = 
{f. \<forall>x. ((x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (map f [0..<n])) > 0) \<and> (snd (rev_m (map f [0..<n])) < 0)))}
\<union>
{f. \<forall>x. ((x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (map f [0..<n])) > 0) \<and> (snd (rev_m (map f [0..<n])) \<ge> 0)))}
\<union>
{f. \<forall>x.
((x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (map f [0..<n])) = 0) \<and> (snd (rev_m (map f [0..<n])) < 0)))}
\<union>
{f. \<forall>x. ((x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (map f [0..<n])) = 0) \<and> (snd (rev_m (map f [0..<n])) = 0)))}"
    by blast
  thus ?thesis
    using calculation by presburger
qed

lemma four_cases_i: 
  assumes "i \<le> n" 
  shows "{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False} 
= {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) < 0)} \<union>
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) \<ge> 0)} \<union> 
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) < 0)} \<union> 
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) = 0)}"
proof -
  have "\<And>l . ((fst (rev_m l) > 0) \<and> (snd (rev_m l) < 0)) \<or>
((fst (rev_m l) > 0) \<and> (snd (rev_m l) \<ge> 0)) \<or>
((fst (rev_m l) = 0) \<and> (snd (rev_m l) < 0)) \<or>
((fst (rev_m l) = 0) \<and> (snd (rev_m l) = 0))"
by (smt rev_margin_le_rho_s rev_rho_s_ge_0)
hence "{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False} =
{f. ((\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (drop i (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) < 0)))
\<or> 
((\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (drop i (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) \<ge> 0))) 
\<or>
((\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (drop i (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) < 0))) 
\<or>
((\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (drop i (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) = 0)))}"
    by blast
  also have "... = 
{f. \<forall>x. ((x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (drop i (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) < 0)))}
\<union>
{f. \<forall>x. ((x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (drop i (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) \<ge> 0)))}
\<union>
{f. \<forall>x.
((x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (drop i (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) < 0)))}
\<union>
{f. \<forall>x. ((x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (drop i (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) = 0)))}"
    by blast
  thus ?thesis
    using calculation by presburger
qed

lemma pair_disj_aux_i:
  assumes "i \<le> n"
 "A = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) < 0)}" 
"B= {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) \<ge> 0)}"
"C = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) < 0)}" 
"D = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) = 0)}"
shows "A \<inter> B = {}" "A \<inter> C = {}" "A \<inter> D = {}" "B \<inter> C = {}" "B \<inter> D = {}" "C \<inter> D = {}"
  using assms by auto

lemma pair_disj_aux:
  assumes 
 "A = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (map f [0..<n])) > 0) \<and> (snd (rev_m (map f [0..<n])) < 0)}" 
"B= {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (map f [0..<n])) > 0) \<and> (snd (rev_m (map f [0..<n])) \<ge> 0)}"
"C = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (map f [0..<n])) = 0) \<and> (snd (rev_m (map f [0..<n])) < 0)}" 
"D = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (map f [0..<n])) = 0) \<and> (snd (rev_m (map f [0..<n])) = 0)}"
shows "A \<inter> B = {}" "A \<inter> C = {}" "A \<inter> D = {}" "B \<inter> C = {}" "B \<inter> D = {}" "C \<inter> D = {}"
  using assms by auto

lemma subs_finite_i:
  assumes "i \<le> n"
 "A = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) < 0)}" 
"B= {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) \<ge> 0)}"
"C = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) < 0)}" 
"D = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) = 0)}"
shows "finite A" "finite B" "finite C" "finite D"
  using assms finite_False_func_set by auto

lemma expectation_Delta_i_le_minus_ep:
  assumes "i \<le> n" "i > 0" 
  shows "measure_pmf.expectation (w_pmf n) (\<Delta>_n i) \<le> -\<epsilon>"
proof -
  have "measure_pmf.expectation (w_pmf n) (\<Delta>_n i) = 
    measure_pmf.expectation 
(bind_pmf 
(w_i_pmf n) 
(\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x))) 
(\<Delta>_n i)"
    using w_i_pmf_def w_pmf_def 
    by (simp add: map_pmf_def) 
  also have "... = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
measure_pmf.expectation ((\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x)) a) (\<Delta>_n i))"
    apply -
    apply (rule pmf_expectation_bind)
    using finite_False_func_set apply auto[1]
    apply simp
    by (smt Collect_mono_iff finite_lessThan set_Pi_pmf_subset set_pmf_eq w_i_pmf_def)
  also have "... = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R  
              (\<Phi>_n i (map a [0..<n]) - \<Phi>_n (i-1) (map a [0..<n])))"
    using expectation_return_pmf
    by (simp add: \<Delta>_n_def) 
  finally have st1:"measure_pmf.expectation (bind_pmf (w_i_pmf n) (\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x))) (\<Delta>_n i)
              = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R  
              (\<Phi>_n i (map a [0..<n]) - \<Phi>_n (i-1) (map a [0..<n])))"
    using expectation_return_pmf
    using \<open>measure_pmf.expectation (local.w_pmf n) (local.\<Delta>_n i) = measure_pmf.expectation (local.w_i_pmf n \<bind> (\<lambda>x. return_pmf (map x [0..<n]))) (local.\<Delta>_n i)\<close> by linarith  

  have 0:"\<And>a i. \<Phi>_n  i (map a [0..<n]) =
 real_of_int (fst (rev_m (drop (length (map a [0..<n]) - i) (map a [0..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (length (map a [0..<n]) - i) (map a [0..<n])))))"
    using \<Phi>_n_def by blast
  hence st2:"measure_pmf.expectation (bind_pmf (w_i_pmf n) (\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x))) (\<Delta>_n i)
              = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R  
              (real_of_int (fst (rev_m (drop (length (map a [0..<n]) - i) (map a [0..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (length (map a [0..<n]) - i) (map a [0..<n]))))) - 
(real_of_int (fst (rev_m (drop (length (map a [0..<n]) - (i-1)) (map a [0..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (length (map a [0..<n]) - (i-1)) (map a [0..<n]))))))))"
    using st1 by auto
  also have 0:"...
= (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. 
pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (drop (n - i) (map a [0..<n])))) -
real_of_int (fst (rev_m (drop (n - (i-1)) (map a [0..<n])))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i) (map a [0..<n]))))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - (i-1)) (map a [0..<n])))))))"
    by (simp add: add.commute add_diff_eq diff_diff_add)
  also have 1:"...
= (\<Sum>a\<in>{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) < 0)} \<union>
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) \<ge> 0)} \<union> 
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) < 0)} \<union> 
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) = 0)}. 
pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (drop (n - i) (map a [0..<n])))) -
real_of_int (fst (rev_m (drop (n - (i-1)) (map a [0..<n])))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i) (map a [0..<n]))))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - (i-1)) (map a [0..<n])))))))"
    using four_cases assms
proof -
  have "n - i +1 \<le> n"
    using assms
    by linarith 
  then show ?thesis
    using four_cases_i
    by presburger 
qed
  also have "...
              =
(\<Sum>a\<in>{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) < 0)} \<union>
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) \<ge> 0)} \<union> 
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) < 0)} \<union> 
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) = 0)}. 
pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (drop (n - i) (map a [0..<n])))) -
real_of_int (fst (rev_m (drop (n - i + 1) (map a [0..<n])))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i) (map a [0..<n]))))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i + 1) (map a [0..<n])))))))"
proof -
{ fix bb :: "nat \<Rightarrow> bool"
have ff1: "n - i + 1 = n + 1 - i"
  by (metis (no_types) Suc_diff_le Suc_eq_plus1 assms(1))
    have "\<forall>n. n + 1 - i = n - (i - 1)"
      using Suc_diff_eq_diff_pred assms(2) by presburger
    then have "(\<Sum>p\<in>{p. (\<forall>na. na \<notin> {..<n} \<longrightarrow> \<not> p na) \<and> 0 < fst (rev_m (drop (n - i + 1) (map p [0..<n]))) \<and> snd (rev_m (drop (n - i + 1) (map p [0..<n]))) < 0} \<union> {p. (\<forall>na. na \<notin> {..<n} \<longrightarrow> \<not> p na) \<and> 0 < fst (rev_m (drop (n - i + 1) (map p [0..<n]))) \<and> 0 \<le> snd (rev_m (drop (n - i + 1) (map p [0..<n])))} \<union> {p. (\<forall>na. na \<notin> {..<n} \<longrightarrow> \<not> p na) \<and> fst (rev_m (drop (n - i + 1) (map p [0..<n]))) = 0 \<and> snd (rev_m (drop (n - i + 1) (map p [0..<n]))) < 0} \<union> {p. (\<forall>na. na \<notin> {..<n} \<longrightarrow> \<not> p na) \<and> fst (rev_m (drop (n - i + 1) (map p [0..<n]))) = 0 \<and> snd (rev_m (drop (n - i + 1) (map p [0..<n]))) = 0}. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (drop (n - i) (map p [0..<n])))) - real_of_int (fst (rev_m (drop (n - (i - 1)) (map p [0..<n])))) + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i) (map p [0..<n]))))) - \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - (i - 1)) (map p [0..<n]))))))) = (\<Sum>p\<in>{p. (\<forall>na. na \<notin> {..<n} \<longrightarrow> \<not> p na) \<and> 0 < fst (rev_m (drop (n - i + 1) (map p [0..<n]))) \<and> snd (rev_m (drop (n - i + 1) (map p [0..<n]))) < 0} \<union> {p. (\<forall>na. na \<notin> {..<n} \<longrightarrow> \<not> p na) \<and> 0 < fst (rev_m (drop (n - i + 1) (map p [0..<n]))) \<and> 0 \<le> snd (rev_m (drop (n - i + 1) (map p [0..<n])))} \<union> {p. (\<forall>na. na \<notin> {..<n} \<longrightarrow> \<not> p na) \<and> fst (rev_m (drop (n - i + 1) (map p [0..<n]))) = 0 \<and> snd (rev_m (drop (n - i + 1) (map p [0..<n]))) < 0} \<union> {p. (\<forall>na. na \<notin> {..<n} \<longrightarrow> \<not> p na) \<and> fst (rev_m (drop (n - i + 1) (map p [0..<n]))) = 0 \<and> snd (rev_m (drop (n - i + 1) (map p [0..<n]))) = 0}. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (drop (n - i) (map p [0..<n])))) - real_of_int (fst (rev_m (drop (n - i + 1) (map p [0..<n])))) + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i) (map p [0..<n]))))) - \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i + 1) (map p [0..<n]))))))) \<or> pmf (local.w_i_pmf n) bb *\<^sub>R (real_of_int (fst (rev_m (drop (n - i) (map bb [0..<n])))) - real_of_int (fst (rev_m (drop (n - (i - 1)) (map bb [0..<n])))) + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i) (map bb [0..<n]))))) - \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - (i - 1)) (map bb [0..<n])))))) = pmf (local.w_i_pmf n) bb *\<^sub>R (real_of_int (fst (rev_m (drop (n - i) (map bb [0..<n])))) - real_of_int (fst (rev_m (drop (n - i + 1) (map bb [0..<n])))) + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i) (map bb [0..<n]))))) - \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i + 1) (map bb [0..<n]))))))"
      using ff1 by presburger }
  then show ?thesis
    by presburger
qed
  finally have  st3:"measure_pmf.expectation (bind_pmf (w_i_pmf n) (\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x))) (\<Delta>_n i)
              =
(\<Sum>a\<in>{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) < 0)} \<union>
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) \<ge> 0)} \<union> 
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) < 0)} \<union> 
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) = 0)}. 
pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (drop (n - i) (map a [0..<n])))) -
real_of_int (fst (rev_m (drop (n - i + 1) (map a [0..<n])))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i) (map a [0..<n]))))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i + 1) (map a [0..<n])))))))"
    by blast
define a1 a2 a3 a4 ex where a1234:
"a1 = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) > 0) 
\<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) < 0)}"
"a2 = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) > 0) 
\<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) \<ge> 0)}"
"a3 = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) = 0) 
\<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) < 0)}"
"a4 = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) = 0) 
\<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) = 0)}"
"ex = (\<lambda>a. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (drop (n-i) (map a [0..<n])))) -
real_of_int (fst (rev_m (drop (n-i+1) (map a [0..<n])))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n-i) (map a [0..<n]))))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n-i+1) (map a [0..<n])))))))"
  hence fina:"finite a1 \<and> finite a2 \<and> finite a3 \<and> finite a4"
     using a1234 finite_False_func_set by fastforce
  hence st4: "measure_pmf.expectation (bind_pmf (w_i_pmf n) (\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x))) (\<Delta>_n i)
              =
(\<Sum>a\<in> a1 \<union> a2 \<union> a3 \<union> a4. ex a)"
    using st3 a1234 by blast
  hence bigdist:"measure_pmf.expectation (bind_pmf (w_i_pmf n) (\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x))) (\<Delta>_n i) =
(\<Sum>a\<in>a1. ex a)+
(\<Sum>a\<in>a2. ex a)+
(\<Sum>a\<in>a3. ex a)+
(\<Sum>a\<in>a4. ex a)"
    using fina pair_disj_aux_i sum.union_disjoint infinite_Un subs_finite_i a1234
  proof -
have f1: "\<forall>P Pa Pb Pc. (((P \<inter> Pa = {} \<or> Pb \<noteq> a1) \<or> Pa \<noteq> a4) \<or> P \<noteq> a3) \<or> Pc \<noteq> a2"
  using a1234(3) a1234(4) diff_le_self pair_disj_aux_i(6) by fastforce
  have f2: "\<forall>P Pa Pb Pc. (((Pc \<inter> Pb = {} \<or> P \<noteq> a1) \<or> Pa \<noteq> a3) \<or> Pb \<noteq> a4) \<or> Pc \<noteq> a2"
    using a1234(2) a1234(4) by fastforce
  have f3: "\<forall>P Pa Pb Pc. (((Pc \<inter> Pb = {} \<or> P \<noteq> a1) \<or> Pa \<noteq> a4) \<or> Pb \<noteq> a3) \<or> Pc \<noteq> a2"
   using a1234 by fastforce 
  have f4: "\<forall>P Pa Pb Pc. (((P \<inter> Pa = {} \<or> P \<noteq> a1) \<or> Pb \<noteq> a3) \<or> Pa \<noteq> a4) \<or> Pc \<noteq> a2"
    using a1234(1) a1234(4) diff_le_self pair_disj_aux_i(3) by fastforce
  have f5: "\<forall>P Pa Pb Pc. (((P \<inter> Pa = {} \<or> P \<noteq> a1) \<or> Pb \<noteq> a4) \<or> Pa \<noteq> a3) \<or> Pc \<noteq> a2"
    using a1234(1) a1234(3) diff_le_self pair_disj_aux_i(2) by fastforce
  have "a1 \<inter> a2 = {}"
    using a1234(1) a1234(2) diff_le_self pair_disj_aux_i(1) by fastforce
  then show ?thesis
    using f5 f4 f3 f2 f1
    by (metis distrib(4) fina infinite_Un st4 sum.union_disjoint sup_bot.right_neutral)
qed
  have "\<forall>x j. x > 0 \<and> j < x \<longrightarrow> drop j [0..<x] = [j..<x]"
     by simp
   hence "\<forall>x j f. x > 0 \<and> j < x \<longrightarrow> drop j (map f [0..<x]) =(map f [j..<x])"
     by (simp add: drop_map)
   hence drop_map_a:"\<forall> a. (drop (n - i) (map a [0..<n])) = map a [(n-i)..<n] \<and> 
(drop (n - i + 1) (map a [0..<n])) = map a [(n-i +1)..<n]"
     by (simp add: drop_map)
  have consmap:"\<forall>a. map a [(n-i)..<n] = (a (n-i)) # (map a [(n - i + 1)..<n])"
    using assms(1) assms(2) upt_conv_Cons by auto
  have "(\<Sum>a\<in>a1. ex a)
= (\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n-i)..<n]))) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n-i)..<n])))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])))))) "
    using a1234 drop_map_a
    by metis 
  hence c1st1:"(\<Sum>a\<in>a1. ex a)  =
(\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n])))) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n]))))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))"
    using consmap
    by (metis a1234(5) drop_map_a)
  hence "\<And>a. a\<in>a1 \<Longrightarrow>  rev_m ((a (n-i))#(map a [(n - i + 1)..<n])) 
        = (fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1), 
          snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1))" 
  proof - 
    fix a
    assume "a \<in> a1"
    have f:" fst (rev_m (map a [(n - i + 1)..<n])) > 0"
      by (metis (mono_tags, lifting) \<open>a \<in> a1\<close> a1234(1) drop_map_a mem_Collect_eq)
   have s:"snd (rev_m (map a [(n - i + 1)..<n])) < 0"
      by (metis (mono_tags, lifting) \<open>a \<in> a1\<close> a1234(1) drop_map_a mem_Collect_eq)
  have "  rev_m (False#(map a [(n - i + 1)..<n])) 
        = (if fst (rev_m (map a [(n - i + 1)..<n])) > snd (rev_m (map a [(n - i + 1)..<n])) 
        \<and> snd (rev_m (map a [(n - i + 1)..<n])) = 0
        then (fst (rev_m (map a [(n - i + 1)..<n])) - 1, 0)
        else 
          (if fst (rev_m (map a [(n - i + 1)..<n])) = 0
          then (0, snd (rev_m (map a [(n - i + 1)..<n])) - 1)
          else (fst (rev_m (map a [(n - i + 1)..<n])) - 1, snd (rev_m (map a [(n - i + 1)..<n])) - 1)))"
    using rev_m.simps(3) by blast
  hence "rev_m (False#(map a [(n - i + 1)..<n])) 
        = (if fst (rev_m (map a [(n - i + 1)..<n])) = 0
          then (0, snd (rev_m (map a [(n - i + 1)..<n])) - 1)
          else (fst (rev_m (map a [(n - i + 1)..<n])) - 1, snd (rev_m (map a [(n - i + 1)..<n])) - 1))"
    using a1234
    by (smt s)
hence falsecase:"rev_m (False#(map a [(n - i + 1)..<n])) 
        = (fst (rev_m (map a [(n - i + 1)..<n])) - 1, snd (rev_m (map a [(n - i + 1)..<n])) - 1)"
  using a1234
  by (smt f)
  thus "rev_m ((a (n-i))#(map a [(n - i + 1)..<n])) 
        = (fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1), 
          snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1))"
    by simp
qed
  hence "\<And>a. a\<in>a1 \<Longrightarrow>  fst (rev_m ((a (n-i))#(map a [(n - i + 1)..<n])))
        = fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1) \<and>
          snd (rev_m ((a (n-i))#(map a [(n - i + 1)..<n]))) =
          snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)"
    by simp 
  have c1unfold:"(\<Sum>a\<in>a1. ex a)  =
(\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))"
    using c1st1
  proof -
have f1: "\<And>p. p \<notin> a1 \<or> pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (p (n - i) # map p [n - i + 1..<n]))) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n]))))) = pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])) + (if p (n - i) then 1 else - 1))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))"
  using \<open>\<And>aa. aa \<in> a1 \<Longrightarrow> fst (rev_m (aa (n - i) # map aa [n - i + 1..<n])) = fst (rev_m (map aa [n - i + 1..<n])) + (if aa (n - i) then 1 else - 1) \<and> snd (rev_m (aa (n - i) # map aa [n - i + 1..<n])) = snd (rev_m (map aa [n - i + 1..<n])) + (if aa (n - i) then 1 else - 1)\<close> by presburger
  have "\<forall>P f fa. \<exists>p. ((p::nat \<Rightarrow> bool) \<in> P \<or> sum f P = sum fa P) \<and> ((f p::real) \<noteq> fa p \<or> sum f P = sum fa P)"
    by (metis (full_types, lifting) sum.cong)
  then obtain bb :: "(nat \<Rightarrow> bool) set \<Rightarrow> ((nat \<Rightarrow> bool) \<Rightarrow> real) \<Rightarrow> ((nat \<Rightarrow> bool) \<Rightarrow> real) \<Rightarrow> nat \<Rightarrow> bool" where
    f2: "\<And>P f fa. (bb P f fa \<in> P \<or> sum f P = sum fa P) \<and> (f (bb P f fa) \<noteq> fa (bb P f fa) \<or> sum f P = sum fa P)"
    by moura
  then have "\<And>P. bb P (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (p (n - i) # map p [n - i + 1..<n]))) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])) + (if p (n - i) then 1 else - 1))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) \<notin> a1 \<or> (\<Sum>p\<in>P. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])) + (if p (n - i) then 1 else - 1))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) = (\<Sum>p\<in>P. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (p (n - i) # map p [n - i + 1..<n]))) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n]))))))"
    using f1
    by (metis (no_types, lifting))
  then have "(\<Sum>p\<in>a1. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])) + (if p (n - i) then 1 else - 1))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) = (\<Sum>p\<in>a1. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (p (n - i) # map p [n - i + 1..<n]))) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n]))))))"
    using f2 by (metis (no_types, lifting))
  then show ?thesis
    using \<open>sum ex a1 = (\<Sum>a\<in>a1. pmf (local.w_i_pmf n) a *\<^sub>R (real_of_int (fst (rev_m (a (n - i) # map a [n - i + 1..<n]))) - real_of_int (fst (rev_m (map a [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (a (n - i) # map a [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map a [n - i + 1..<n]))))))\<close> by presburger
qed
  have roi_minus_dist:"\<And>a b. real_of_int a - real_of_int b = real_of_int (a - b)"
    by simp
  hence "(\<Sum>a\<in>a1. ex a)  =
(\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int 
((fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)) -
(fst (rev_m (map a [(n - i + 1)..<n])))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))"
    using c1unfold by presburger
hence tmp:"(\<Sum>a\<in>a1. ex a)  =
(\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int 
((fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)) -
(fst (rev_m (map a [(n - i + 1)..<n])))))
 + (\<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])))))))"
  by (smt sum.cong)
have "\<And>a. \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)))
 - \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))= 
\<alpha>* real_of_int ((min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1))) - 
(min 0 (snd (rev_m (map a [(n - i + 1)..<n])))))"
  by (simp add: right_diff_distrib)
   hence "(\<Sum>a\<in>a1. ex a)  =
(\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int 
((fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)) -
(fst (rev_m (map a [(n - i + 1)..<n])))))
 + \<alpha> * (real_of_int ((min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1))) - 
(min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))))"
     using tmp
     by auto 
hence "(\<Sum>a\<in>a1. ex a)  =
(\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (if a (n - i) then 1 else -1))
 + \<alpha> * 
(real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)) - 
(min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))))"
  by auto
hence "(\<Sum>a\<in>a1. ex a)  =
(\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (if a (n - i) then 1 else -1))
 + \<alpha> * 
(real_of_int (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1) - 
(snd (rev_m (map a [(n - i + 1)..<n])))))))"
  using a1234
  by (smt drop_map_a mem_Collect_eq real_vector.scale_cancel_left sum.cong) 
hence tmp:"(\<Sum>a\<in>a1. ex a)  =
(\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
(1 * real_of_int (if a (n - i) then 1 else -1)
 + 
\<alpha> * real_of_int (if a (n - i) then 1 else -1)))"
  using a1234
  by (smt sum.cong)
  have "\<And>a::real. (1 * a + \<alpha> * a) = (1+\<alpha>) * a"
    using Real_Vector_Spaces.scaleR_left_distrib
    by (simp add: semiring_normalization_rules(3))
hence "(\<Sum>a\<in>a1. ex a)  =
(\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
((1 + \<alpha>)* real_of_int (if a (n - i) then 1 else -1)))"
  using tmp by auto            
hence "(\<Sum>a\<in>a1. ex a)  =
(\<Sum>a \<in> a1. (1 + \<alpha>) * pmf (w_i_pmf n) a *\<^sub>R
(real_of_int (if a (n - i) then 1 else -1)))"
  by (metis (no_types, lifting) mult_scaleR_right sum.cong)
hence "(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * (\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
(real_of_int (if a (n - i) then 1 else -1)))"
  by (simp add: sum_distrib_left)
  hence tmp:"(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * (\<Sum>a \<in> a1. pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a *
(if a (n - i) then 1 else -1))"
  using w_i_pmf_def
  by (smt of_int_1 of_int_minus real_scaleR_def sum.cong) 
hence tmp:"(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * (\<Sum>a \<in> a1. (\<Prod>x\<in>{..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *
(if a (n - i) then 1 else -1))"
  using w_i_pmf_def finite_False_func_set pmf_Pi'
proof - 
  have 1:"finite {..<n}" by simp
  have "\<And>a. a \<in> a1 \<Longrightarrow>  (\<And>x. x \<notin> {..<n} \<Longrightarrow> a x = False)"
    by (simp add: a1234(1) a1234(2))
  hence "\<And>a. a \<in> a1 \<Longrightarrow> pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a =
        (\<Prod>x\<in>{..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)) x) (a x))"
    using 1 pmf_Pi'
    by (smt prod.cong)
  thus ?thesis
    using tmp by auto
qed
hence "(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * (\<Sum>a \<in> a1. (\<Prod>x\<in>{..<n-i} \<union> {n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *
(if a (n - i) then 1 else -1))"
proof -
  have "{..<n} = {..<n-i} \<union> {n-i} \<union> {n-i<..<n}"
    using assms(1) assms(2) ivl_disj_un_singleton(2) by force
  thus ?thesis
    using tmp
    by simp
qed
hence tmp:"(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * (\<Sum>a \<in> a1. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n} \<union> {n-i}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *
(if a (n - i) then 1 else -1))"
  by simp
hence "(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * (\<Sum>a \<in> a1. ((\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(\<Prod>x\<in> {n-i}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  *
(if a (n - i) then 1 else -1))"
proof - 
  have "finite ({..<n-i} \<union> {n-i<..<n}) \<and> (finite {n-i}) \<and> (({..<n-i} \<union> {n-i<..<n}) \<inter> {n-i} = {})"
    by simp
  thus ?thesis
    using tmp Groups_Big.comm_monoid_mult_class.prod.union_disjoint
    by (metis (no_types, lifting) sum.cong)
qed
  hence "(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * (\<Sum>a \<in> a1. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1))"
  by simp
  hence tmp:"(\<Sum>a\<in>a1 . ex a)  =
(1 + \<alpha>) * (\<Sum>a \<in> a1 \<inter> ({f. f (n-i) \<or> \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1))"
  by auto
  hence "(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * (\<Sum>a \<in> a1  \<inter> ({f. f (n-i)} \<union> {f. \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1))"
  proof - 
    have "{f. f (n-i) \<or> \<not> f (n-i)} = {f. f (n-i)} \<union> {f. \<not> f (n-i)}"
      by auto
    thus ?thesis using tmp
      by auto
  qed
  hence tmp:"(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * (\<Sum>a \<in> (a1 \<inter> {f. f (n-i)}) \<union> (a1 \<inter> {f. \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1))"
    by (simp add: inf_sup_distrib1)
 hence "(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * ((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1))
+ (\<Sum>a \<in> a1 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1)))"
 proof - 
   have 1:"(a1 \<inter> {f. f (n-i)}) \<inter> (a1 \<inter> {f. \<not> f (n-i)}) = {}"
     by blast
   have "finite a1" using a1234
     using fina by linarith
   hence "finite (a1 \<inter> {f. f (n-i)}) \<and> finite (a1 \<inter> {f. \<not> f (n-i)})"
     by auto
   thus ?thesis
     using tmp infinite_Un 1
     by (simp add: sum.union_disjoint)
 qed
hence tmp:"(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * ((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) True) * 1)
+ (\<Sum>a \<in> a1 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) False) * -1))"
  by auto
hence "(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * ((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
((1-\<epsilon>)/2) * 1)
+ (\<Sum>a \<in> a1 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
((1+\<epsilon>)/2) * (-1)))"
proof - 
  have "(((1-\<epsilon>)/2) \<ge>0) \<and> ((1-\<epsilon>)/2) \<le> 1"
    using ep_gt_0 ep_le_1 by simp
  thus ?thesis using pmf_bernoulli_True pmf_bernoulli_False honest_slot tmp
    by (simp add: honest_slot)
qed
hence tmp:"(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * ((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
((1-\<epsilon>)/2)
+ (\<Sum>a \<in> a1 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * (((1+\<epsilon>)/2) * (-1))))"
  by (simp add: sum_distrib_right)
hence "(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * ((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
((1-\<epsilon>)/2)
+ (\<Sum>a \<in> a1 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * (- (1+\<epsilon>)/2)))"
  by (simp add: tmp mult.commute sum_distrib_right)
 hence tmp:"(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * ((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
((1-\<epsilon>)/2)
+ (\<Sum>a \<in> a1 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) * (- (1+\<epsilon>)/2))"
   by (simp add: sum_distrib_right)
  have case1eq:"(\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
=  (\<Sum>a \<in> a1 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) "
  proof - 
    define h where h:"h = (\<lambda>f x. (if x = n-i then \<not> f x else f x))"
    have "(\<forall>x\<in>(a1 \<inter> {f. f (n-i)}). \<forall>y\<in>(a1 \<inter> {f. f (n-i)}). h x = h y \<longrightarrow> x = y)"
    proof (rule ccontr)
      assume "\<not> (\<forall>x\<in>(a1 \<inter> {f. f (n-i)}). \<forall>y\<in>(a1 \<inter> {f. f (n-i)}). h x = h y \<longrightarrow> x = y)"
      then obtain x y where "x\<in>(a1 \<inter> {f. f (n-i)})" "y\<in>(a1 \<inter> {f. f (n-i)})" "h x = h y" "x \<noteq> y"
        by blast
       obtain j where "x j \<noteq> y j"
         using \<open>x \<noteq> y\<close> 
         by blast
       have "(h x) j = (h y) j"
         using \<open>h x = h y\<close>
         by simp
       hence "(\<lambda>f x. (if x = n-i then \<not> f x else f x)) x j = (\<lambda>f x. (if x = n-i then \<not> f x else f x)) y j"
         using h by blast
       hence "(if j = n-i then \<not> x j else x j) = (if j = n-i then \<not> y j else y j)"
         by simp
       hence "x j = y j"
         by presburger
       thus "False"
         using \<open>x j \<noteq> y j\<close> by auto
     qed
     hence "inj_on h (a1 \<inter> {f. f (n-i)})"
       by (meson inj_onI)
     have "h ` (a1 \<inter> {f. f (n-i)}) = a1 \<inter> {f. \<not> f (n-i)}" 
     proof - 
       have "\<And>a. a \<in> h ` (a1 \<inter> {f. f (n-i)}) \<Longrightarrow> a \<in> a1 \<inter> {f. \<not> f (n-i)}"
          proof -
           fix a
           assume "a \<in> h ` (a1 \<inter> {f. f (n-i)}) "
           then obtain b where "b \<in> (a1 \<inter> {f. f (n-i)})" "a = h b"
             by blast
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> b x = False) \<and> (fst (rev_m (drop (n-i+1) (map b [0..<n]))) > 0)
\<and> (snd (rev_m (drop (n-i+1) (map b [0..<n]))) < 0)) \<and> b (n-i)"
             using a1234(1) by blast
           hence condi1:"\<forall>x. (x \<notin> {..<n} \<longrightarrow> h b x = False)"
             using h by simp
           have "n - i + 1 \<le> n"
             using assms
             by linarith
           have ngt:"n - i + 1 > n - i" by simp
           have dropsimp:"(drop (n-i+1) (map (h b) [0..<n])) = (map (h b) [(n-i+1)..<n])"
             using drop_map_a by blast
           have "\<forall>x. n-i+1 \<le> x \<and> x < n \<longrightarrow> h b x = b x"
             using h by auto
           hence "\<And>x. x \<in> set [(n-i+1)..<n] \<Longrightarrow> h b x =b x"
             by simp
           hence "(map (h b) [(n-i+1)..<n]) =  (map b [(n-i+1)..<n])"
             using ngt list.map_cong0
             by blast
           hence "(drop (n-i+1) (map (h b) [0..<n])) = (drop (n-i+1) (map b [0..<n]))"
             using dropsimp
             by (simp add: drop_map)
           hence condi2:"(fst (rev_m (drop (n-i+1) (map (h b) [0..<n]))) > 0) 
\<and> (snd (rev_m (drop (n-i+1) (map (h b) [0..<n]))) < 0)"
             using tmp
             by simp
           have "\<not> h b (n-i)"
             using tmp h
             by auto
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> h b x = False) \<and> (fst (rev_m (drop (n-i+1) (map (h b) [0..<n]))) > 0) 
                    \<and> (snd (rev_m (drop (n-i+1) (map (h b) [0..<n]))) < 0)) \<and> \<not> h b (n-i)"
             using h condi1 condi2 by blast
           hence "h b \<in> a1 \<inter> {f. \<not> f (n-i)}"
             using a1234(1) by blast
           thus "a \<in> a1 \<inter> {f. \<not> f (n-i)}"
             using \<open>a = h b\<close> by simp
         qed
       hence  "h ` (a1 \<inter> {f. f (n-i)}) \<subseteq> a1 \<inter> {f. \<not> f (n-i)}"
         by blast
       have "\<And>a. a \<in> a1 \<inter> {f. \<not> f (n-i)} \<Longrightarrow> a \<in> h ` (a1 \<inter> {f. f (n-i)})"
       proof - 
         fix a assume "a \<in> a1 \<inter> {f. \<not> f (n-i)}"
         hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> a x = False) \<and> (fst (rev_m (drop (n-i+1) (map a [0..<n]))) > 0)
            \<and> (snd (rev_m (drop (n-i+1) (map a [0..<n]))) < 0)) \<and> \<not> a (n-i)"
           using a1234(1) by blast
         obtain b where "b = h a"
           by simp
         hence condi1:"\<forall>x. (x \<notin> {..<n} \<longrightarrow> h a x = False)"
             using h tmp
             using assms(1) assms(2) diff_diff_cancel by auto 
           have "n - i + 1 \<le> n"
             using assms
             by linarith
           have ngt:"n - i + 1 > n - i" by simp
           have dropsimp:"(drop (n-i+1) (map (h a) [0..<n])) = (map (h a) [(n-i+1)..<n])"
             using drop_map_a by blast
           have "\<forall>x. n-i+1 \<le> x \<and> x < n \<longrightarrow> h a x = a x"
             using h by auto
           hence "\<And>x. x \<in> set [(n-i+1)..<n] \<Longrightarrow> h a x =a x"
             by simp
           hence "(map (h a) [(n-i+1)..<n]) =  (map a [(n-i+1)..<n])"
             using ngt list.map_cong0
             by blast
           hence "(drop (n-i+1) (map (h a) [0..<n])) = (drop (n-i+1) (map a [0..<n]))"
             using dropsimp
             by (simp add: drop_map)
           hence condi2:"(fst (rev_m (drop (n-i+1) (map (h a) [0..<n]))) > 0) 
\<and> (snd (rev_m (drop (n-i+1) (map (h a) [0..<n]))) < 0)"
             using tmp
             by simp
           have "h a (n-i)"
             using tmp h
             by auto
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> b x = False) \<and> (fst (rev_m (drop (n-i+1) (map b [0..<n]))) > 0) 
                    \<and> (snd (rev_m (drop (n-i+1) (map b [0..<n]))) < 0)) \<and> b (n-i)"
             using h condi1 condi2 \<open>b = h a\<close> by simp
           hence "b \<in> a1 \<inter> {f. f (n-i)}"
             using a1234(1) by blast
           have "h b = a"
           proof - 
             have "h b = h (h a)"
               using \<open>b = h a\<close> by simp
             hence "h (h a) = (\<lambda>f x. (if x = n-i then \<not> f x else f x)) ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a)"
               by (simp add: h)
             hence "h (h a) = 
(\<lambda>x. (if x = n-i then \<not> ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a) x else ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a) x)) " by simp
             hence "h (h a) = 
(\<lambda>x. (if x = n-i then \<not> (\<lambda>x. (if x = n-i then \<not> a x else a x)) x else (\<lambda>x. (if x = n-i then \<not> a x else a x)) x))" by simp
             hence "\<And>x. h (h a) x = 
(if x = n-i then \<not> (if x = n-i then \<not> a x else a x) else (if x = n-i then \<not> a x else a x))"
               by simp
             hence "\<And>x. h (h a) x = a x"
               by simp
              hence "h (h a) = a"
                by blast
              thus ?thesis
                by (simp add: \<open>b = h a\<close>)
            qed
              hence "(h b) \<in> h ` (a1 \<inter> {f. f (n-i)})"
                using \<open>b \<in> a1 \<inter> {f. f (n - i)}\<close> by blast
              thus "a \<in> h ` (a1 \<inter> {f. f (n-i)})"
                using \<open>h b = a\<close> by simp
            qed
            thus ?thesis
              using \<open>h ` (a1 \<inter> {f. f (n - i)}) \<subseteq> a1 \<inter> {f. \<not> f (n - i)}\<close> by blast
          qed
          hence "bij_betw h  (a1 \<inter> {f. f (n - i)}) (a1 \<inter> {f. \<not> f (n - i)})"
            by (simp add: \<open>inj_on h (a1 \<inter> {f. f (n - i)})\<close> bij_betw_def)
          hence "(\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (h a)) 
              =  sum (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (a1 \<inter> {f. \<not> f (n-i)})"
            using sum.reindex_bij_betw by simp
            hence "(\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (h a)) 
              =  (\<Sum>a \<in> a1 \<inter> { f.\<not> f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) a)"
              by blast
        hence tmp:"(\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x))) 
              =  (\<Sum>a \<in> a1 \<inter> { f.\<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          by simp
        have "\<And>a. a \<in> a1 \<inter> {f. f (n-i)} \<Longrightarrow> 
(\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x)) = 
(\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))"
          by (smt Un_iff greaterThanLessThan_iff h lessThan_iff less_irrefl prod.cong)
        hence "(\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x))) = 
              (\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          by auto
        thus "(\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
              =  (\<Sum>a \<in> a1 \<inter> { f.\<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          using tmp
          by linarith
      qed      
      hence "(\<Sum>a\<in>a1. ex a)  =
            (1 + \<alpha>) * ((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
            ((1-\<epsilon>)/2)
            + (\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) * (- (1+\<epsilon>)/2))"
        using tmp
        by (simp add: case1eq tmp)
      hence "(\<Sum>a\<in>a1. ex a)  =
            (1 + \<alpha>) * ((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
            (((1-\<epsilon>)/2) +  (- (1+\<epsilon>)/2)))"
        by (simp add: linordered_field_class.sign_simps(36))
      hence "(\<Sum>a\<in>a1. ex a)  =
            (1 + \<alpha>) * ((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
            (-\<epsilon>))"
      proof -
        have "\<forall>r ra. - (ra::real) + (r + ra) * inverse 2 = (r + - ra) * inverse 2"
          by auto
        then have "\<forall>r ra. ((ra::real) + - r) * inverse 2 + - (ra + r) * inverse 2 = - r"
          by (metis (no_types) add_diff_cancel_right' add_uminus_conv_diff mult_minus_left)
        then show ?thesis
          using \<open>sum ex a1 = (1 + \<alpha>) * ((\<Sum>a\<in>a1 \<inter> {f. f (n - i)}. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * ((1 - \<epsilon>) / 2 + - (1 + \<epsilon>) / 2))\<close> by fastforce
      qed
   hence "(\<Sum>a\<in>a1. ex a)  =
            (1 + \<alpha>) * (
((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) + 
(\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))))/2
  * (-\<epsilon>))"
     by simp
hence tmp:"(\<Sum>a\<in>a1. ex a)  =
            (1 + \<alpha>) * (
((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) + 
(\<Sum>a \<in> a1 \<inter> {f. \<not>f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))))/2
  * (-\<epsilon>))" using case1eq
  by simp
hence tmp:"(\<Sum>a\<in>a1. ex a)  =
            (1 + \<alpha>) * (
(\<Sum>a \<in> (a1 \<inter> {f. f (n-i)}) \<union> (a1 \<inter> {f. \<not>f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))/2
  * (-\<epsilon>))" using case1eq pair_disj_aux_i sum.union_disjoint infinite_Un
proof - 
  have fin:"finite (a1 \<inter> {f. f (n-i)}) \<and> finite (a1 \<inter> {f. \<not>f (n-i)})"
    using finite_False_func_set a1234(1) ep_gt_0 ep_le_1 by auto
  have "(a1 \<inter> {f. f (n-i)}) \<inter> (a1 \<inter> {f. \<not>f (n-i)}) = {}"
    by auto
  thus ?thesis  using case1eq pair_disj_aux_i sum.union_disjoint infinite_Un tmp fin
    by smt
qed
hence tmp:"(\<Sum>a\<in>a1. ex a)  =
            (1 + \<alpha>) * (
(\<Sum>a \<in> a1. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))/2
  * (-\<epsilon>))"
proof - 
  have "(a1 \<inter> {f. f (n-i)}) \<union> (a1 \<inter> {f. \<not>f (n-i)}) = a1"
    by blast
  thus ?thesis using tmp by simp
qed
  define A where A:"A = (\<Sum>a \<in> a1. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))" 
  hence simplifiedcase1:"(\<Sum>a\<in>a1. ex a)  =
            (1 + \<alpha>) * A/2 * (-\<epsilon>)"
    using tmp by simp
 have "(\<Sum>a\<in>a2. ex a) 
= (\<Sum>a \<in> a2. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n-i)..<n]))) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n-i)..<n])))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])))))) "
    using a1234 drop_map_a
    by metis 
  hence c2st1:"(\<Sum>a\<in>a2. ex a)  =
(\<Sum>a \<in> a2. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n])))) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n]))))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))"
    using consmap
    by (metis a1234(5) drop_map_a)
  hence "\<And>a. a\<in>a2 \<Longrightarrow>          
fst (rev_m (map a [(n - i)..<n])) = fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)
\<and>
(min 0 (snd (rev_m (map a [(n - i)..<n])))) = (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))" 
  proof - 
    fix a
    assume "a \<in> a2"
    have f:" fst (rev_m (map a [(n - i + 1)..<n])) > 0"
      by (metis (mono_tags, lifting) \<open>a \<in> a2\<close> a1234(2) drop_map_a mem_Collect_eq)
   have s:"snd (rev_m (map a [(n - i + 1)..<n])) \<ge> 0"
      by (metis (mono_tags, lifting) \<open>a \<in> a2\<close> a1234(2) drop_map_a mem_Collect_eq)
    have "  rev_m (False#(map a [(n - i + 1)..<n]))
        = (if fst (rev_m (map a [(n - i + 1)..<n])) > snd (rev_m (map a [(n - i + 1)..<n])) 
        \<and> snd (rev_m (map a [(n - i + 1)..<n])) = 0
        then (fst (rev_m (map a [(n - i + 1)..<n])) - 1, 0)
        else 
          (if fst (rev_m (map a [(n - i + 1)..<n])) = 0
          then (0, snd (rev_m (map a [(n - i + 1)..<n])) - 1)
          else (fst (rev_m (map a [(n - i + 1)..<n])) - 1, snd (rev_m (map a [(n - i + 1)..<n])) - 1)))"
    using rev_m.simps(3) by blast
  hence "rev_m (False#(map a [(n - i + 1)..<n]))
        = (if fst (rev_m (map a [(n - i + 1)..<n])) > snd (rev_m (map a [(n - i + 1)..<n])) 
        \<and> snd (rev_m (map a [(n - i + 1)..<n])) = 0
        then (fst (rev_m (map a [(n - i + 1)..<n])) - 1, 0)
        else 
          (fst (rev_m (map a [(n - i + 1)..<n])) - 1, snd (rev_m (map a [(n - i + 1)..<n])) - 1))"
    using a1234
    by (smt f s)
  hence falsecase: "fst (rev_m (False#(map a [(n - i + 1)..<n]))) = fst (rev_m (map a [(n - i + 1)..<n])) - 1
\<and>
(min 0 (snd (rev_m (False#(map a [(n - i + 1)..<n]))))) = (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))" 
    by (smt f fst_conv s snd_conv)
    have "  rev_m (True#(map a [(n - i + 1)..<n])) 
= (fst (rev_m (map a [(n - i + 1)..<n])) + 1, snd (rev_m (map a [(n - i + 1)..<n])) + 1)"
      using rev_m.simps(2) by blast
    hence "fst (rev_m (True#(map a [(n - i + 1)..<n]))) = fst (rev_m (map a [(n - i + 1)..<n])) + 1 \<and>
(min 0 (snd (rev_m (True#(map a [(n - i + 1)..<n]))))) = (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))"
      using s by auto
  thus "fst (rev_m (map a [(n - i)..<n])) = fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)
\<and>
(min 0 (snd (rev_m (map a [(n - i)..<n])))) = (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))" 
    using falsecase
    by (simp add: consmap) 
qed
  hence "(\<Sum>a\<in>a2. ex a)  =
(\<Sum>a \<in> a2. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))"
    using c2st1
    by (metis (no_types, lifting) consmap sum.cong)
hence c2unfold:"(\<Sum>a\<in>a2. ex a)  =
(\<Sum>a \<in> a2. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))))"
  by auto
  hence "(\<Sum>a\<in>a2. ex a)  =
(\<Sum>a \<in> a2. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int 
((fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)) -
(fst (rev_m (map a [(n - i + 1)..<n])))))))"
    using roi_minus_dist
    by presburger
hence "(\<Sum>a\<in>a2. ex a)  =
(\<Sum>a \<in> a2. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (if a (n - i) then 1 else -1))))"
  by auto
  hence tmp:"(\<Sum>a\<in>a2. ex a)  =
 (\<Sum>a \<in> a2. pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a *
(if a (n - i) then 1 else -1))"
    using w_i_pmf_def
  by (smt of_int_1 of_int_minus real_scaleR_def sum.cong) 
hence tmp:"(\<Sum>a\<in>a2. ex a)  =  (\<Sum>a \<in> a2. (\<Prod>x\<in>{..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *
(if a (n - i) then 1 else -1))"
  using w_i_pmf_def finite_False_func_set pmf_Pi'
proof - 
  have 1:"finite {..<n}" by simp
  have "\<And>a. a \<in> a2 \<Longrightarrow>  (\<And>x. x \<notin> {..<n} \<Longrightarrow> a x = False)"
    by (simp add: a1234(1) a1234(2))
  hence "\<And>a. a \<in> a2 \<Longrightarrow> pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a =
        (\<Prod>x\<in>{..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)) x) (a x))"
    using 1 pmf_Pi'
    by (smt prod.cong)
  thus ?thesis
    using tmp by auto
qed
hence "(\<Sum>a\<in>a2. ex a)  = (\<Sum>a \<in> a2. (\<Prod>x\<in>{..<n-i} \<union> {n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *
(if a (n - i) then 1 else -1))"
proof -
  have "{..<n} = {..<n-i} \<union> {n-i} \<union> {n-i<..<n}"
    using assms(1) assms(2) ivl_disj_un_singleton(2) by force
  thus ?thesis
    using tmp
    by simp
qed
hence tmp:"(\<Sum>a\<in>a2. ex a)  =
(\<Sum>a \<in> a2. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n} \<union> {n-i}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *
(if a (n - i) then 1 else -1))"
  by simp
hence "(\<Sum>a\<in>a2. ex a)  = (\<Sum>a \<in> a2. ((\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(\<Prod>x\<in> {n-i}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  *
(if a (n - i) then 1 else -1))"
proof - 
  have "finite ({..<n-i} \<union> {n-i<..<n}) \<and> (finite {n-i}) \<and> (({..<n-i} \<union> {n-i<..<n}) \<inter> {n-i} = {})"
    by simp
  thus ?thesis
    using tmp Groups_Big.comm_monoid_mult_class.prod.union_disjoint
    by (metis (no_types, lifting) sum.cong)
qed
  hence tmp:"(\<Sum>a\<in>a2 . ex a)  =
 (\<Sum>a \<in> a2 \<inter> ({f. f (n-i) \<or> \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1))"
  by auto
  hence "(\<Sum>a\<in>a2. ex a)  =
(\<Sum>a \<in> a2  \<inter> ({f. f (n-i)} \<union> {f. \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1))"
  proof - 
    have "{f. f (n-i) \<or> \<not> f (n-i)} = {f. f (n-i)} \<union> {f. \<not> f (n-i)}"
      by auto
    thus ?thesis using tmp
      by auto
  qed
  hence tmp:"(\<Sum>a\<in>a2. ex a)  =
(\<Sum>a \<in> (a2 \<inter> {f. f (n-i)}) \<union> (a2 \<inter> {f. \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1))"
    by (simp add: inf_sup_distrib1)
 hence "(\<Sum>a\<in>a2. ex a)  =
 ((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1))
+ (\<Sum>a \<in> a2 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1)))"
 proof - 
   have 1:"(a2 \<inter> {f. f (n-i)}) \<inter> (a2 \<inter> {f. \<not> f (n-i)}) = {}"
     by blast
   have "finite a2" using a1234
     using fina by linarith
   hence "finite (a2 \<inter> {f. f (n-i)}) \<and> finite (a2 \<inter> {f. \<not> f (n-i)})"
     by auto
   thus ?thesis
     using tmp infinite_Un 1
     by (simp add: sum.union_disjoint)
 qed
hence tmp:"(\<Sum>a\<in>a2. ex a)  =
((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) True) * 1)
+ (\<Sum>a \<in> a2 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) False) * -1))"
  by auto
hence "(\<Sum>a\<in>a2. ex a)  =
((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
((1-\<epsilon>)/2) * 1)
+ (\<Sum>a \<in> a2 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
((1+\<epsilon>)/2) * (-1)))"
proof - 
  have "(((1-\<epsilon>)/2) \<ge>0) \<and> ((1-\<epsilon>)/2) \<le> 1"
    using ep_gt_0 ep_le_1 by simp
  thus ?thesis using pmf_bernoulli_True pmf_bernoulli_False honest_slot tmp
    by (simp add: honest_slot)
qed
hence tmp:"(\<Sum>a\<in>a2. ex a)  =
((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
((1-\<epsilon>)/2)
+ (\<Sum>a \<in> a2 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * (((1+\<epsilon>)/2) * (-1))))"
  by (simp add: sum_distrib_right)
hence "(\<Sum>a\<in>a2. ex a)  =
((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
((1-\<epsilon>)/2)
+ (\<Sum>a \<in> a2 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * (- (1+\<epsilon>)/2)))"
  by (simp add: tmp mult.commute sum_distrib_right)
 hence tmp:"(\<Sum>a\<in>a2. ex a)  =
((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
((1-\<epsilon>)/2)
+ (\<Sum>a \<in> a2 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) * (- (1+\<epsilon>)/2))"
   by (simp add: sum_distrib_right)
  have case2eq:"(\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
=  (\<Sum>a \<in> a2 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) "
  proof - 
    define h where h:"h = (\<lambda>f x. (if x = n-i then \<not> f x else f x))"
    have "(\<forall>x\<in>(a2 \<inter> {f. f (n-i)}). \<forall>y\<in>(a2 \<inter> {f. f (n-i)}). h x = h y \<longrightarrow> x = y)"
    proof (rule ccontr)
      assume "\<not> (\<forall>x\<in>(a2 \<inter> {f. f (n-i)}). \<forall>y\<in>(a2 \<inter> {f. f (n-i)}). h x = h y \<longrightarrow> x = y)"
      then obtain x y where "x\<in>(a2 \<inter> {f. f (n-i)})" "y\<in>(a2 \<inter> {f. f (n-i)})" "h x = h y" "x \<noteq> y"
        by blast
       obtain j where "x j \<noteq> y j"
         using \<open>x \<noteq> y\<close> 
         by blast
       have "(h x) j = (h y) j"
         using \<open>h x = h y\<close>
         by simp
       hence "(\<lambda>f x. (if x = n-i then \<not> f x else f x)) x j = (\<lambda>f x. (if x = n-i then \<not> f x else f x)) y j"
         using h by blast
       hence "(if j = n-i then \<not> x j else x j) = (if j = n-i then \<not> y j else y j)"
         by simp
       hence "x j = y j"
         by presburger
       thus "False"
         using \<open>x j \<noteq> y j\<close> by auto
     qed
     hence "inj_on h (a2 \<inter> {f. f (n-i)})"
       by (meson inj_onI)
     have "h ` (a2 \<inter> {f. f (n-i)}) = a2 \<inter> {f. \<not> f (n-i)}" 
     proof - 
       have "\<And>a. a \<in> h ` (a2 \<inter> {f. f (n-i)}) \<Longrightarrow> a \<in> a2 \<inter> {f. \<not> f (n-i)}"
          proof -
           fix a
           assume "a \<in> h ` (a2 \<inter> {f. f (n-i)}) "
           then obtain b where "b \<in> (a2 \<inter> {f. f (n-i)})" "a = h b"
             by blast
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> b x = False) \<and> (fst (rev_m (drop (n-i+1) (map b [0..<n]))) > 0)
\<and> (snd (rev_m (drop (n-i+1) (map b [0..<n]))) \<ge> 0)) \<and> b (n-i)"
             using a1234(2) by blast
           hence condi1:"\<forall>x. (x \<notin> {..<n} \<longrightarrow> h b x = False)"
             using h by simp
           have "n - i + 1 \<le> n"
             using assms
             by linarith
           have ngt:"n - i + 1 > n - i" by simp
           have dropsimp:"(drop (n-i+1) (map (h b) [0..<n])) = (map (h b) [(n-i+1)..<n])"
             using drop_map_a by blast
           have "\<forall>x. n-i+1 \<le> x \<and> x < n \<longrightarrow> h b x = b x"
             using h by auto
           hence "\<And>x. x \<in> set [(n-i+1)..<n] \<Longrightarrow> h b x =b x"
             by simp
           hence "(map (h b) [(n-i+1)..<n]) =  (map b [(n-i+1)..<n])"
             using ngt list.map_cong0
             by blast
           hence "(drop (n-i+1) (map (h b) [0..<n])) = (drop (n-i+1) (map b [0..<n]))"
             using dropsimp
             by (simp add: drop_map)
           hence condi2:"(fst (rev_m (drop (n-i+1) (map (h b) [0..<n]))) > 0) 
\<and> (snd (rev_m (drop (n-i+1) (map (h b) [0..<n]))) \<ge> 0)"
             using tmp
             by simp
           have "\<not> h b (n-i)"
             using tmp h
             by auto
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> h b x = False) \<and> (fst (rev_m (drop (n-i+1) (map (h b) [0..<n]))) > 0) 
                    \<and> (snd (rev_m (drop (n-i+1) (map (h b) [0..<n]))) \<ge> 0)) \<and> \<not> h b (n-i)"
             using h condi1 condi2 by blast
           hence "h b \<in> a2 \<inter> {f. \<not> f (n-i)}"
             using a1234(2) by blast
           thus "a \<in> a2 \<inter> {f. \<not> f (n-i)}"
             using \<open>a = h b\<close> by simp
         qed
       hence  "h ` (a2 \<inter> {f. f (n-i)}) \<subseteq> a2 \<inter> {f. \<not> f (n-i)}"
         by blast
       have "\<And>a. a \<in> a2 \<inter> {f. \<not> f (n-i)} \<Longrightarrow> a \<in> h ` (a2 \<inter> {f. f (n-i)})"
       proof - 
         fix a assume "a \<in> a2 \<inter> {f. \<not> f (n-i)}"
         hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> a x = False) \<and> (fst (rev_m (drop (n-i+1) (map a [0..<n]))) > 0)
            \<and> (snd (rev_m (drop (n-i+1) (map a [0..<n]))) \<ge> 0)) \<and> \<not> a (n-i)"
           using a1234(2) by blast
         obtain b where "b = h a"
           by simp
         hence condi1:"\<forall>x. (x \<notin> {..<n} \<longrightarrow> h a x = False)"
             using h tmp
             using assms(1) assms(2) diff_diff_cancel by auto 
           have "n - i + 1 \<le> n"
             using assms
             by linarith
           have ngt:"n - i + 1 > n - i" by simp
           have dropsimp:"(drop (n-i+1) (map (h a) [0..<n])) = (map (h a) [(n-i+1)..<n])"
             using drop_map_a by blast
           have "\<forall>x. n-i+1 \<le> x \<and> x < n \<longrightarrow> h a x = a x"
             using h by auto
           hence "\<And>x. x \<in> set [(n-i+1)..<n] \<Longrightarrow> h a x =a x"
             by simp
           hence "(map (h a) [(n-i+1)..<n]) =  (map a [(n-i+1)..<n])"
             using ngt list.map_cong0
             by blast
           hence "(drop (n-i+1) (map (h a) [0..<n])) = (drop (n-i+1) (map a [0..<n]))"
             using dropsimp
             by (simp add: drop_map)
           hence condi2:"(fst (rev_m (drop (n-i+1) (map (h a) [0..<n]))) > 0) 
\<and> (snd (rev_m (drop (n-i+1) (map (h a) [0..<n]))) \<ge> 0)"
             using tmp
             by simp
           have "h a (n-i)"
             using tmp h
             by auto
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> b x = False) \<and> (fst (rev_m (drop (n-i+1) (map b [0..<n]))) > 0) 
                    \<and> (snd (rev_m (drop (n-i+1) (map b [0..<n]))) \<ge> 0)) \<and> b (n-i)"
             using h condi1 condi2 \<open>b = h a\<close> by simp
           hence "b \<in> a2 \<inter> {f. f (n-i)}"
             using a1234(2) by blast
           have "h b = a"
           proof - 
             have "h b = h (h a)"
               using \<open>b = h a\<close> by simp
             hence "h (h a) = (\<lambda>f x. (if x = n-i then \<not> f x else f x)) ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a)"
               by (simp add: h)
             hence "h (h a) = 
(\<lambda>x. (if x = n-i then \<not> ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a) x else ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a) x)) " by simp
             hence "h (h a) = 
(\<lambda>x. (if x = n-i then \<not> (\<lambda>x. (if x = n-i then \<not> a x else a x)) x else (\<lambda>x. (if x = n-i then \<not> a x else a x)) x))" by simp
             hence "\<And>x. h (h a) x = 
(if x = n-i then \<not> (if x = n-i then \<not> a x else a x) else (if x = n-i then \<not> a x else a x))"
               by simp
             hence "\<And>x. h (h a) x = a x"
               by simp
              hence "h (h a) = a"
                by blast
              thus ?thesis
                by (simp add: \<open>b = h a\<close>)
            qed
              hence "(h b) \<in> h ` (a2 \<inter> {f. f (n-i)})"
                using \<open>b \<in> a2 \<inter> {f. f (n - i)}\<close> by blast
              thus "a \<in> h ` (a2 \<inter> {f. f (n-i)})"
                using \<open>h b = a\<close> by simp
            qed
            thus ?thesis
              using \<open>h ` (a2 \<inter> {f. f (n - i)}) \<subseteq> a2 \<inter> {f. \<not> f (n - i)}\<close> by blast
          qed
          hence "bij_betw h  (a2 \<inter> {f. f (n - i)}) (a2 \<inter> {f. \<not> f (n - i)})"
            by (simp add: \<open>inj_on h (a2 \<inter> {f. f (n - i)})\<close> bij_betw_def)
          hence "(\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (h a)) 
              =  sum (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (a2 \<inter> {f. \<not> f (n-i)})"
            using sum.reindex_bij_betw by simp
            hence "(\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (h a)) 
              =  (\<Sum>a \<in> a2 \<inter> { f.\<not> f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) a)"
              by blast
        hence tmp:"(\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x))) 
              =  (\<Sum>a \<in> a2 \<inter> { f.\<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          by simp
        have "\<And>a. a \<in> a2 \<inter> {f. f (n-i)} \<Longrightarrow> 
(\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x)) = 
(\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))"
          by (smt Un_iff greaterThanLessThan_iff h lessThan_iff less_irrefl prod.cong)
        hence "(\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x))) = 
              (\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          by auto
        thus "(\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
              =  (\<Sum>a \<in> a2 \<inter> { f.\<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          using tmp
          by linarith
      qed      
      hence "(\<Sum>a\<in>a2. ex a)  =
       ((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
            ((1-\<epsilon>)/2)
            + (\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) * (- (1+\<epsilon>)/2))"
        using tmp
        by (simp add: tmp)
      hence "(\<Sum>a\<in>a2. ex a)  =
       ((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
            (((1-\<epsilon>)/2) +  (- (1+\<epsilon>)/2)))"
        by (simp add: linordered_field_class.sign_simps(36))
      hence "(\<Sum>a\<in>a2. ex a)  =
  ((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
            (-\<epsilon>))"
      proof -
        have "\<forall>r ra. - (ra::real) + (r + ra) * inverse 2 = (r + - ra) * inverse 2"
          by auto
        then have "\<forall>r ra. ((ra::real) + - r) * inverse 2 + - (ra + r) * inverse 2 = - r"
          by (metis (no_types) add_diff_cancel_right' add_uminus_conv_diff mult_minus_left)
        then show ?thesis
          using \<open>sum ex a2 =  ((\<Sum>a\<in>a2 \<inter> {f. f (n - i)}. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * ((1 - \<epsilon>) / 2 + - (1 + \<epsilon>) / 2))\<close> by fastforce
      qed
   hence "(\<Sum>a\<in>a2. ex a)  =
       (
((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) + 
(\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))))/2
  * (-\<epsilon>))"
     by simp
hence tmp:"(\<Sum>a\<in>a2. ex a)  =
    (((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) + 
(\<Sum>a \<in> a2 \<inter> {f. \<not>f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))))/2
  * (-\<epsilon>))" using case2eq
  by simp
hence tmp:"(\<Sum>a\<in>a2. ex a)  =
          ((\<Sum>a \<in> (a2 \<inter> {f. f (n-i)}) \<union> (a2 \<inter> {f. \<not>f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))/2
  * (-\<epsilon>))" using  pair_disj_aux_i sum.union_disjoint infinite_Un
proof - 
  have fin:"finite (a2 \<inter> {f. f (n-i)}) \<and> finite (a2 \<inter> {f. \<not>f (n-i)})"
    using finite_False_func_set a1234(2) ep_gt_0 ep_le_1 by auto
  have "(a2 \<inter> {f. f (n-i)}) \<inter> (a2 \<inter> {f. \<not>f (n-i)}) = {}"
    by auto
  thus ?thesis  using pair_disj_aux_i sum.union_disjoint infinite_Un tmp fin
    by smt
qed
hence tmp:"(\<Sum>a\<in>a2. ex a)  =
  ((\<Sum>a \<in> a2. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))/2
  * (-\<epsilon>))"
proof - 
  have "(a2 \<inter> {f. f (n-i)}) \<union> (a2 \<inter> {f. \<not>f (n-i)}) = a2"
    by blast
  thus ?thesis using tmp by simp
qed
  define B where B:"B = (\<Sum>a \<in> a2. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))" 
  hence simplifiedcase2:"(\<Sum>a\<in>a2. ex a)  = B/2 * (-\<epsilon>)"
    using tmp by simp
 have "(\<Sum>a\<in>a3. ex a)
= (\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n-i)..<n]))) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n-i)..<n])))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])))))) "
    using a1234 drop_map_a
    by metis 
  hence c3st1:"(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n])))) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n]))))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))"
    using consmap
    by (metis a1234(5) drop_map_a)
  hence tmp:"\<And>a. a\<in>a3 \<Longrightarrow>  fst (rev_m ((a (n-i))#(map a [(n - i + 1)..<n]))) 
        = fst (rev_m (map a [(n - i + 1)..<n])) + (max 0 (if a (n - i) then 1 else -1)) \<and>
          (min 0 (snd (rev_m (map a [(n - i)..<n])))) = 
          (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))) + (if a (n - i) then 1 else -1))" 
  proof -
    fix a
    assume "a \<in> a3"
    have f:" fst (rev_m (map a [(n - i + 1)..<n])) = 0"
      by (metis (mono_tags, lifting) \<open>a \<in> a3\<close> a1234(3) drop_map_a mem_Collect_eq)
   have s:"snd (rev_m (map a [(n - i + 1)..<n])) < 0"
      by (metis (mono_tags, lifting) \<open>a \<in> a3\<close> a1234(3) drop_map_a mem_Collect_eq)
  have "  rev_m (False#(map a [(n - i + 1)..<n])) 
        = (if fst (rev_m (map a [(n - i + 1)..<n])) > snd (rev_m (map a [(n - i + 1)..<n])) 
        \<and> snd (rev_m (map a [(n - i + 1)..<n])) = 0
        then (fst (rev_m (map a [(n - i + 1)..<n])) - 1, 0)
        else 
          (if fst (rev_m (map a [(n - i + 1)..<n])) = 0
          then (0, snd (rev_m (map a [(n - i + 1)..<n])) - 1)
          else (fst (rev_m (map a [(n - i + 1)..<n])) - 1, snd (rev_m (map a [(n - i + 1)..<n])) - 1)))"
    using rev_m.simps(3) by blast
  hence falsecase:"rev_m (False#(map a [(n - i + 1)..<n]))
        = (0, snd (rev_m (map a [(n - i + 1)..<n])) - 1)"
    using a1234
    by (smt s f)
  have truecase:"  rev_m (True#(map a [(n - i + 1)..<n])) 
        = (fst (rev_m (map a [(n - i + 1)..<n])) + 1, snd (rev_m (map a [(n - i + 1)..<n])) + 1)"
    using rev_m.simps(2) by blast
  hence fstcase:"fst (rev_m ((a (n-i))#(map a [(n - i + 1)..<n]))) 
        = fst (rev_m (map a [(n - i + 1)..<n])) + (max 0 (if a (n - i) then 1 else -1))"
    using falsecase f s
    by auto
  thus "fst (rev_m ((a (n-i))#(map a [(n - i + 1)..<n]))) 
        = fst (rev_m (map a [(n - i + 1)..<n])) + (max 0 (if a (n - i) then 1 else -1)) \<and>
          (min 0 (snd (rev_m (map a [(n - i)..<n])))) = 
          (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))) + (if a (n - i) then 1 else -1))"
    using falsecase f s truecase
    by (smt consmap snd_conv) 
qed
  hence c3unfold:"(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n - i + 1)..<n])) + (max 0 (if a (n - i) then 1 else -1))) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 +  \<alpha> * real_of_int (min 0 (snd (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n]))))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))"
    using c3st1 
proof -
  have "\<And>p. (p (n - i)::bool) # map p [Suc (n - i)..<n] = map p [n - i..<n]"
    by (metis (full_types, lifting) Suc_eq_plus1 consmap)
  then have f1: "\<And>p. p \<notin> a3 \<or> pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n]))))) = ex p"
    by (metis (no_types) Suc_eq_plus1 \<open>\<And>aa. aa \<in> a3 \<Longrightarrow> fst (rev_m (aa (n - i) # map aa [n - i + 1..<n])) = fst (rev_m (map aa [n - i + 1..<n])) + max 0 (if aa (n - i) then 1 else - 1) \<and> min 0 (snd (rev_m (map aa [n - i..<n]))) = min 0 (snd (rev_m (map aa [n - i + 1..<n]))) + (if aa (n - i) then 1 else - 1)\<close> a1234(5) drop_map_a)
  obtain bb :: "(nat \<Rightarrow> bool) set \<Rightarrow> ((nat \<Rightarrow> bool) \<Rightarrow> real) \<Rightarrow> ((nat \<Rightarrow> bool) \<Rightarrow> real) \<Rightarrow> nat \<Rightarrow> bool" where
    f2: "\<And>P f fa. (bb P f fa \<in> P \<or> sum f P = sum fa P) \<and> (f (bb P f fa) \<noteq> fa (bb P f fa) \<or> sum f P = sum fa P)"
    by (metis (full_types, lifting) sum.cong)
  { assume "bb a3 (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) ex \<notin> a3"
    then have ?thesis
  using f2 by (metis (full_types, lifting)) }
  moreover
  { assume "bb a3 (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) ex \<in> a3"
    then have "pmf (local.w_i_pmf n) (bb a3 (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) ex) *\<^sub>R (real_of_int (fst (rev_m (map (bb a3 (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) ex) [n - i + 1..<n])) + max 0 (if bb a3 (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) ex (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map (bb a3 (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) ex) [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (bb a3 (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) ex (n - i) # map (bb a3 (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) ex) [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map (bb a3 (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) ex) [n - i + 1..<n]))))) = ex (bb a3 (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) ex)"
      using f1 by meson
    then have ?thesis
      using f2
      by (metis (no_types, lifting)) }
  ultimately show ?thesis
  by blast
qed
hence c3unfold:"(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n - i + 1)..<n])) + (max 0 (if a (n - i) then 1 else -1))) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 +  \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))) + (if a (n - i) then 1 else -1)) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))"
  using tmp
proof -
have f1: "\<And>p. p \<notin> a3 \<or> pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n]))))) = pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n]))) + (if p (n - i) then 1 else - 1)) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))"
  by (metis consmap tmp)
  obtain bb :: "(nat \<Rightarrow> bool) set \<Rightarrow> ((nat \<Rightarrow> bool) \<Rightarrow> real) \<Rightarrow> ((nat \<Rightarrow> bool) \<Rightarrow> real) \<Rightarrow> nat \<Rightarrow> bool" where
    f2: "\<And>P f fa. (bb P f fa \<in> P \<or> sum f P = sum fa P) \<and> (f (bb P f fa) \<noteq> fa (bb P f fa) \<or> sum f P = sum fa P)"
    by (metis (no_types) sum.cong)
  then have "\<And>f fa. sum f a3 = sum fa a3 \<or> pmf (local.w_i_pmf n) (bb a3 f fa) *\<^sub>R (real_of_int (fst (rev_m (map (bb a3 f fa) [n - i + 1..<n])) + max 0 (if bb a3 f fa (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map (bb a3 f fa) [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (bb a3 f fa (n - i) # map (bb a3 f fa) [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map (bb a3 f fa) [n - i + 1..<n]))))) = pmf (local.w_i_pmf n) (bb a3 f fa) *\<^sub>R (real_of_int (fst (rev_m (map (bb a3 f fa) [n - i + 1..<n])) + max 0 (if bb a3 f fa (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map (bb a3 f fa) [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (map (bb a3 f fa) [n - i + 1..<n]))) + (if bb a3 f fa (n - i) then 1 else - 1)) - \<alpha> * real_of_int (min 0 (snd (rev_m (map (bb a3 f fa) [n - i + 1..<n])))))"
    using f1 by meson
  then have "(\<Sum>p\<in>a3. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n]))) + (if p (n - i) then 1 else - 1)) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) = (\<Sum>p\<in>a3. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n]))))))"
    using f2
    by (metis (no_types, lifting)) 
  then show ?thesis
  using c3unfold by presburger
qed
hence "(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n - i + 1)..<n])) + (max 0 (if a (n - i) then 1 else -1))) -
fst (rev_m (map a [(n - i + 1)..<n])))
 +  \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))) + (if a (n - i) then 1 else -1)) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))"
  using roi_minus_dist
  by blast
hence tmp:"(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n - i + 1)..<n])) + (max 0 (if a (n - i) then 1 else -1))) -
fst (rev_m (map a [(n - i + 1)..<n])))
 +  (\<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))) + (if a (n - i) then 1 else -1)) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])))))))"
  by (smt sum.cong)
  define k where k:"k = (\<lambda>a. \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)))
 - \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])))))"
  hence aux:"\<And>a. k a= 
\<alpha>* real_of_int ((min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1))) - 
(min 0 (snd (rev_m (map a [(n - i + 1)..<n])))))"
  by (simp add: right_diff_distrib)
  have "(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n - i + 1)..<n])) + (max 0 (if a (n - i) then 1 else -1))) -
fst (rev_m (map a [(n - i + 1)..<n])))
 +  k a))" using tmp k
    by (smt a1234(3) drop_map_a mem_Collect_eq real_vector.scale_cancel_left sum.cong)
  hence "(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n - i + 1)..<n])) + (max 0 (if a (n - i) then 1 else -1))) -
fst (rev_m (map a [(n - i + 1)..<n])))
 +  \<alpha>* real_of_int ((min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1))) - 
(min 0 (snd (rev_m (map a [(n - i + 1)..<n])))))))" using aux by auto
hence "(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (max 0 (if a (n - i) then 1 else -1)))
 + \<alpha> * (real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)) - 
(min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))))"
  by simp
hence "(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (max 0 (if a (n - i) then 1 else -1)))
 + \<alpha> * 
(real_of_int (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1) - 
(snd (rev_m (map a [(n - i + 1)..<n])))))))"
  using a1234

  by (smt drop_map_a mem_Collect_eq real_vector.scale_cancel_left sum.cong) 
hence tmp:"(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
(real_of_int (max 0 (if a (n - i) then 1 else -1))
 + 
\<alpha> * real_of_int (if a (n - i) then 1 else -1)))"
  using a1234
  by (smt sum.cong)         
  define k where k:"k = (\<lambda>a .real_of_int (max 0 (if a (n - i) then 1 else -1))
 + \<alpha> * real_of_int (if a (n - i) then 1 else -1))"
  hence tmp:"(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a * k a)"
  using w_i_pmf_def tmp
  by (smt of_int_1 of_int_minus real_scaleR_def sum.cong)
hence tmp2:"(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. (\<Prod>x\<in>{..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * k a)"
  using w_i_pmf_def finite_False_func_set pmf_Pi'
proof - 
  have 1:"finite {..<n}" by simp
  have "\<And>a. a \<in> a3 \<Longrightarrow>  (\<And>x. x \<notin> {..<n} \<Longrightarrow> a x = False)"
    by (simp add: a1234(3))
  hence "\<And>a. a \<in> a3 \<Longrightarrow> pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a =
        (\<Prod>x\<in>{..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)) x) (a x))"
    using 1 pmf_Pi'
    by (smt prod.cong)
  thus ?thesis
    using tmp by auto
qed
hence "(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. (\<Prod>x\<in>{..<n-i} \<union> {n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * k a)"
proof -
  have "{..<n} = {..<n-i} \<union> {n-i} \<union> {n-i<..<n}"
    using assms(1) assms(2) ivl_disj_un_singleton(2) by force
  thus ?thesis using tmp2
    by simp
qed
hence tmp:"(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n} \<union> {n-i}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * k a)"
  by simp
hence "(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. ((\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(\<Prod>x\<in> {n-i}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * k a)"
proof - 
  have "finite ({..<n-i} \<union> {n-i<..<n}) \<and> (finite {n-i}) \<and> (({..<n-i} \<union> {n-i<..<n}) \<inter> {n-i} = {})"
    by simp
  thus ?thesis
    using tmp Groups_Big.comm_monoid_mult_class.prod.union_disjoint
    by (metis (no_types, lifting) sum.cong)
qed
  hence "(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a)"
  by simp
  hence tmp:"(\<Sum>a\<in>a3 . ex a)  =
(\<Sum>a \<in> a3 \<inter> ({f. f (n-i) \<or> \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a)"
  by auto
  hence "(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3  \<inter> ({f. f (n-i)} \<union> {f. \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a)"
  proof - 
    have "{f. f (n-i) \<or> \<not> f (n-i)} = {f. f (n-i)} \<union> {f. \<not> f (n-i)}"
      by auto
    thus ?thesis using tmp
      by auto
  qed
  hence tmp:"(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> (a3 \<inter> {f. f (n-i)}) \<union> (a3 \<inter> {f. \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a)"
    by (simp add: inf_sup_distrib1)
 hence "(\<Sum>a\<in>a3. ex a)  =
((\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a)
+ (\<Sum>a \<in> a3 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a))"
 proof - 
   have 1:"(a3 \<inter> {f. f (n-i)}) \<inter> (a3 \<inter> {f. \<not> f (n-i)}) = {}"
     by blast
   have "finite a3" using a1234
     using fina by linarith
   hence "finite (a3 \<inter> {f. f (n-i)}) \<and> finite (a3 \<inter> {f. \<not> f (n-i)})"
     by auto
   thus ?thesis
     using tmp infinite_Un 1
     by (simp add: sum.union_disjoint)
 qed
hence tmp:"(\<Sum>a\<in>a3. ex a)  =
((\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) True) * (real_of_int (max 0 1)
 + \<alpha> * real_of_int (if a (n - i) then 1 else -1)))
+ (\<Sum>a \<in> a3 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) False) * (real_of_int (max 0 (-1))
 + \<alpha> * real_of_int (if a (n - i) then 1 else -1))))"
  using k  by auto
hence tmp:"(\<Sum>a\<in>a3. ex a)  =
((\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) True) * (real_of_int 1
 + \<alpha> * real_of_int (if a (n - i) then 1 else -1)))
+ (\<Sum>a \<in> a3 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) False) * ((real_of_int 0)
 + \<alpha> * real_of_int (if a (n - i) then 1 else -1))))"
  by linarith  
hence tmp:"(\<Sum>a\<in>a3. ex a)  =
((\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
((1-\<epsilon>)/2) * (1 + \<alpha>))
+ (\<Sum>a \<in> a3 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(1- ((1-\<epsilon>)/2)) * (- \<alpha>)))"
  using pmf_bernoulli_True pmf_bernoulli_False ep_gt_0 ep_le_1  by auto

hence tmp:"(\<Sum>a\<in>a3. ex a)  =
((\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
((1-\<epsilon>)/2) * (1 + \<alpha>))
+ (\<Sum>a \<in> a3 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
((1+\<epsilon>)/2) * (- \<alpha>)))"
proof - 
  have "(((1-\<epsilon>)/2) \<ge>0) \<and> ((1-\<epsilon>)/2) \<le> 1"
    using ep_gt_0 ep_le_1 by simp
  thus ?thesis using pmf_bernoulli_True pmf_bernoulli_False honest_slot tmp
    by (simp add: honest_slot)
qed

  have case3eq:"(\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
=  (\<Sum>a \<in> a3 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) "
  proof - 
    define h where h:"h = (\<lambda>f x. (if x = n-i then \<not> f x else f x))"
    have "(\<forall>x\<in>(a3 \<inter> {f. f (n-i)}). \<forall>y\<in>(a3 \<inter> {f. f (n-i)}). h x = h y \<longrightarrow> x = y)"
    proof (rule ccontr)
      assume "\<not> (\<forall>x\<in>(a3 \<inter> {f. f (n-i)}). \<forall>y\<in>(a3 \<inter> {f. f (n-i)}). h x = h y \<longrightarrow> x = y)"
      then obtain x y where "x\<in>(a3 \<inter> {f. f (n-i)})" "y\<in>(a3 \<inter> {f. f (n-i)})" "h x = h y" "x \<noteq> y"
        by blast
       obtain j where "x j \<noteq> y j"
         using \<open>x \<noteq> y\<close> 
         by blast
       have "(h x) j = (h y) j"
         using \<open>h x = h y\<close>
         by simp
       hence "(\<lambda>f x. (if x = n-i then \<not> f x else f x)) x j = (\<lambda>f x. (if x = n-i then \<not> f x else f x)) y j"
         using h by blast
       hence "(if j = n-i then \<not> x j else x j) = (if j = n-i then \<not> y j else y j)"
         by simp
       hence "x j = y j"
         by presburger
       thus "False"
         using \<open>x j \<noteq> y j\<close> by auto
     qed
     hence "inj_on h (a3 \<inter> {f. f (n-i)})"
       by (meson inj_onI)
     have "h ` (a3 \<inter> {f. f (n-i)}) = a3 \<inter> {f. \<not> f (n-i)}" 
     proof - 
       have "\<And>a. a \<in> h ` (a3 \<inter> {f. f (n-i)}) \<Longrightarrow> a \<in> a3 \<inter> {f. \<not> f (n-i)}"
          proof -
           fix a
           assume "a \<in> h ` (a3 \<inter> {f. f (n-i)}) "
           then obtain b where "b \<in> (a3 \<inter> {f. f (n-i)})" "a = h b"
             by blast
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> b x = False) \<and> (fst (rev_m (drop (n-i+1) (map b [0..<n]))) = 0)
\<and> (snd (rev_m (drop (n-i+1) (map b [0..<n]))) < 0)) \<and> b (n-i)"
             using a1234(3) by blast
           hence condi1:"\<forall>x. (x \<notin> {..<n} \<longrightarrow> h b x = False)"
             using h by simp
           have "n - i + 1 \<le> n"
             using assms
             by linarith
           have ngt:"n - i + 1 > n - i" by simp
           have dropsimp:"(drop (n-i+1) (map (h b) [0..<n])) = (map (h b) [(n-i+1)..<n])"
             using drop_map_a by blast
           have "\<forall>x. n-i+1 \<le> x \<and> x < n \<longrightarrow> h b x = b x"
             using h by auto
           hence "\<And>x. x \<in> set [(n-i+1)..<n] \<Longrightarrow> h b x =b x"
             by simp
           hence "(map (h b) [(n-i+1)..<n]) =  (map b [(n-i+1)..<n])"
             using ngt list.map_cong0
             by blast
           hence "(drop (n-i+1) (map (h b) [0..<n])) = (drop (n-i+1) (map b [0..<n]))"
             using dropsimp
             by (simp add: drop_map)
           hence condi2:"(fst (rev_m (drop (n-i+1) (map (h b) [0..<n]))) = 0) 
\<and> (snd (rev_m (drop (n-i+1) (map (h b) [0..<n]))) < 0)"
             using tmp
             by simp
           have "\<not> h b (n-i)"
             using tmp h
             by auto
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> h b x = False) \<and> (fst (rev_m (drop (n-i+1) (map (h b) [0..<n]))) = 0) 
                    \<and> (snd (rev_m (drop (n-i+1) (map (h b) [0..<n]))) < 0)) \<and> \<not> h b (n-i)"
             using h condi1 condi2 by blast
           hence "h b \<in> a3 \<inter> {f. \<not> f (n-i)}"
             using a1234(3) by blast
           thus "a \<in> a3 \<inter> {f. \<not> f (n-i)}"
             using \<open>a = h b\<close> by simp
         qed
       hence  "h ` (a3 \<inter> {f. f (n-i)}) \<subseteq> a3 \<inter> {f. \<not> f (n-i)}"
         by blast
       have "\<And>a. a \<in> a3 \<inter> {f. \<not> f (n-i)} \<Longrightarrow> a \<in> h ` (a3 \<inter> {f. f (n-i)})"
       proof - 
         fix a assume "a \<in> a3 \<inter> {f. \<not> f (n-i)}"
         hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> a x = False) \<and> (fst (rev_m (drop (n-i+1) (map a [0..<n]))) = 0)
            \<and> (snd (rev_m (drop (n-i+1) (map a [0..<n]))) < 0)) \<and> \<not> a (n-i)"
           using a1234(3) by blast
         obtain b where "b = h a"
           by simp
         hence condi1:"\<forall>x. (x \<notin> {..<n} \<longrightarrow> h a x = False)"
             using h tmp
             using assms(1) assms(2) diff_diff_cancel by auto 
           have "n - i + 1 \<le> n"
             using assms
             by linarith
           have ngt:"n - i + 1 > n - i" by simp
           have dropsimp:"(drop (n-i+1) (map (h a) [0..<n])) = (map (h a) [(n-i+1)..<n])"
             using drop_map_a by blast
           have "\<forall>x. n-i+1 \<le> x \<and> x < n \<longrightarrow> h a x = a x"
             using h by auto
           hence "\<And>x. x \<in> set [(n-i+1)..<n] \<Longrightarrow> h a x =a x"
             by simp
           hence "(map (h a) [(n-i+1)..<n]) =  (map a [(n-i+1)..<n])"
             using ngt list.map_cong0
             by blast
           hence "(drop (n-i+1) (map (h a) [0..<n])) = (drop (n-i+1) (map a [0..<n]))"
             using dropsimp
             by (simp add: drop_map)
           hence condi2:"(fst (rev_m (drop (n-i+1) (map (h a) [0..<n]))) = 0) 
\<and> (snd (rev_m (drop (n-i+1) (map (h a) [0..<n]))) < 0)"
             using tmp
             by simp
           have "h a (n-i)"
             using tmp h
             by auto
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> b x = False) \<and> (fst (rev_m (drop (n-i+1) (map b [0..<n]))) = 0) 
                    \<and> (snd (rev_m (drop (n-i+1) (map b [0..<n]))) < 0)) \<and> b (n-i)"
             using h condi1 condi2 \<open>b = h a\<close> by simp
           hence "b \<in> a3 \<inter> {f. f (n-i)}"
             using a1234(3) by blast
           have "h b = a"
           proof - 
             have "h b = h (h a)"
               using \<open>b = h a\<close> by simp
             hence "h (h a) = (\<lambda>f x. (if x = n-i then \<not> f x else f x)) ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a)"
               by (simp add: h)
             hence "h (h a) = 
(\<lambda>x. (if x = n-i then \<not> ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a) x else ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a) x)) " by simp
             hence "h (h a) = 
(\<lambda>x. (if x = n-i then \<not> (\<lambda>x. (if x = n-i then \<not> a x else a x)) x else (\<lambda>x. (if x = n-i then \<not> a x else a x)) x))" by simp
             hence "\<And>x. h (h a) x = 
(if x = n-i then \<not> (if x = n-i then \<not> a x else a x) else (if x = n-i then \<not> a x else a x))"
               by simp
             hence "\<And>x. h (h a) x = a x"
               by simp
              hence "h (h a) = a"
                by blast
              thus ?thesis
                by (simp add: \<open>b = h a\<close>)
            qed
              hence "(h b) \<in> h ` (a3 \<inter> {f. f (n-i)})"
                using \<open>b \<in> a3 \<inter> {f. f (n - i)}\<close> by blast
              thus "a \<in> h ` (a3 \<inter> {f. f (n-i)})"
                using \<open>h b = a\<close> by simp
            qed
            thus ?thesis
              using \<open>h ` (a3 \<inter> {f. f (n - i)}) \<subseteq> a3 \<inter> {f. \<not> f (n - i)}\<close> by blast
          qed
          hence "bij_betw h  (a3 \<inter> {f. f (n - i)}) (a3 \<inter> {f. \<not> f (n - i)})"
            by (simp add: \<open>inj_on h (a3 \<inter> {f. f (n - i)})\<close> bij_betw_def)
          hence "(\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (h a)) 
              =  sum (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (a3 \<inter> {f. \<not> f (n-i)})"
            using sum.reindex_bij_betw by simp
            hence "(\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (h a)) 
              =  (\<Sum>a \<in> a3 \<inter> { f.\<not> f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) a)"
              by blast
        hence tmp:"(\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x))) 
              =  (\<Sum>a \<in> a3 \<inter> { f.\<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          by simp
        have "\<And>a. a \<in> a3 \<inter> {f. f (n-i)} \<Longrightarrow> 
(\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x)) = 
(\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))"
          by (smt Un_iff greaterThanLessThan_iff h lessThan_iff less_irrefl prod.cong)
        hence "(\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x))) = 
              (\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          by auto
        thus "(\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
              =  (\<Sum>a \<in> a3 \<inter> { f.\<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          using tmp
          by linarith
      qed      
      hence tmp2:"(\<Sum>a\<in>a3. ex a)  =
            ((\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
            ((1-\<epsilon>)/2) * (1 + \<alpha>)
            + (\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) * ((1+\<epsilon>)/2) * (- \<alpha>))"
        using tmp
      proof -
        have "\<forall>r ra. (\<Sum>p\<in>a3 \<inter> {p. \<not> p (n - i)}. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * ra * r) = (\<Sum>p\<in>a3 \<inter> {p. p (n - i)}. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * ra * r)"
          by (metis (no_types) case3eq sum_distrib_right)
        then have "(\<Sum>p\<in>a3 \<inter> {p. p (n - i)}. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * ((1 - \<epsilon>) / 2) * (1 + \<alpha>)) + (\<Sum>p\<in>a3 \<inter> {p. p (n - i)}. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * ((1 + \<epsilon>) / 2) * - \<alpha>) = sum ex a3"
          using tmp by presburger
        then show ?thesis
          by (metis (no_types) sum_distrib_right)
      qed
      define s where "s =  (\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
      
      hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * ((1-\<epsilon>)/2) * (1 + \<alpha>) + s * ((1+\<epsilon>)/2) * (- \<alpha>))"
        using tmp2
        by blast
hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) * (1 + \<alpha>) +  ((1+\<epsilon>)/2) * (- \<alpha>)))"
  by (metis (mono_tags, hide_lams) linordered_field_class.sign_simps(36) mult.commute mult.left_commute)
 hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) + (((1-\<epsilon>)/2)* \<alpha>) +  ((1+\<epsilon>)/2) * (- \<alpha>)))"
   by (metis \<open>\<And>a. 1 * a + \<alpha> * a = (1 + \<alpha>) * a\<close> mult.commute mult.left_neutral)
   hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) * (1 + \<alpha>) +  ((1+\<epsilon>)/2) * (- \<alpha>)))"
  proof -
  have "s * ((1 + \<alpha>) * ((1 - \<epsilon>) / 2) + - \<alpha> * ((1 + \<epsilon>) / 2)) = sum ex a3"
    by (simp add: \<open>sum ex a3 = s * ((1 - \<epsilon>) / 2 * (1 + \<alpha>) + (1 + \<epsilon>) / 2 * - \<alpha>)\<close> mult.commute)
  then show ?thesis
    by (metis \<open>\<And>a. 1 * a + \<alpha> * a = (1 + \<alpha>) * a\<close> mult.commute)
qed 
 hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) + ((((1-\<epsilon>)/2)* \<alpha>) +  ((1+\<epsilon>)/2) * (- \<alpha>))))"
   using \<open>sum ex a3 = s * ((1 - \<epsilon>) / 2 + (1 - \<epsilon>) / 2 * \<alpha> + (1 + \<epsilon>) / 2 * - \<alpha>)\<close> by auto
    hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) + ((((1-\<epsilon>)/2)* \<alpha>) +  (-(1+\<epsilon>)/2) * \<alpha>)))"
      by (metis divide_minus_left mult_minus_left mult_minus_right)
        hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) + ((((1-\<epsilon>)/2) +  (-(1+\<epsilon>)/2)) * \<alpha>)))"
          by (simp add: linordered_field_class.sign_simps(35))
hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) + ((-\<epsilon>) * \<alpha>)))"
  by (metis (no_types, hide_lams) ab_group_add_class.ab_diff_conv_add_uminus add.commute add_diff_cancel_right' divide_minus_left real_average_minus_second)
hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) + ((-\<epsilon>) * (1 + \<epsilon>)/(2*\<epsilon>))))"
  using \<alpha> by simp
hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) + ((1 + \<epsilon>)*((-\<epsilon>/\<epsilon>)/2))))"
  by auto
hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) + ((1 + \<epsilon>)*((-1)/2))))"
  using ep_gt_0  by auto
hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) + (- (1 + \<epsilon>)/2)))"
  by (metis divide_inverse mult_minus1 mult_minus_left mult_minus_right)
  hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (-\<epsilon>))"
    by (metis ab_group_add_class.ab_diff_conv_add_uminus add.commute add_diff_cancel_right' divide_minus_left real_average_minus_second)
hence "(\<Sum>a\<in>a3. ex a)  =
            ((\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * (-\<epsilon>))"
  using s_def by blast
  
hence tmp:"(\<Sum>a\<in>a3. ex a)  =
             (
((\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) + 
(\<Sum>a \<in> a3 \<inter> {f. \<not>f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))))/2
  * (-\<epsilon>))" using case3eq
  by simp
hence tmp:"(\<Sum>a\<in>a3. ex a)  =
         (
(\<Sum>a \<in> (a3 \<inter> {f. f (n-i)}) \<union> (a3 \<inter> {f. \<not>f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))/2
  * (-\<epsilon>))" using case1eq pair_disj_aux_i sum.union_disjoint infinite_Un
proof - 
  have fin:"finite (a3 \<inter> {f. f (n-i)}) \<and> finite (a3 \<inter> {f. \<not>f (n-i)})"
    using finite_False_func_set a1234(3) ep_gt_0 ep_le_1 by auto
  have "(a3 \<inter> {f. f (n-i)}) \<inter> (a3 \<inter> {f. \<not>f (n-i)}) = {}"
    by auto
  thus ?thesis  using case3eq pair_disj_aux_i sum.union_disjoint infinite_Un tmp fin
    by smt
qed
hence tmp:"(\<Sum>a\<in>a3. ex a)  = ((\<Sum>a \<in> a3. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))/2
  * (-\<epsilon>))"
proof - 
  have "(a3 \<inter> {f. f (n-i)}) \<union> (a3 \<inter> {f. \<not>f (n-i)}) = a3"
    by blast
  thus ?thesis using tmp by simp
qed
  define C where C:"C = (\<Sum>a \<in> a3. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))" 
  hence simplifiedcase3:"(\<Sum>a\<in>a3. ex a)  = C/2 * (-\<epsilon>)"
    using tmp by simp
 have tmp:"(\<Sum>a\<in>a4. ex a)
= (\<Sum>a \<in> a4. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n-i)..<n]))) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n-i)..<n])))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])))))) "
    using a1234 drop_map_a
    by metis 
  hence "(\<Sum>a\<in>a4. ex a)
= (\<Sum>a \<in> a4. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n-i)..<n]))) - 0)
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n-i)..<n])))) -
 \<alpha> * 0)) "
    using a1234(4)
  proof -
    have "\<And>a. a \<in> a4 \<Longrightarrow>
 fst (rev_m (map a [(n - i + 1)..<n])) = 0 \<and>
 snd (rev_m (map a [(n - i + 1)..<n])) = 0"
      by (metis (mono_tags, lifting) a1234(4) drop_map_a mem_Collect_eq)
    thus ?thesis using tmp
      by auto
  qed
 hence c4st1:"(\<Sum>a\<in>a4. ex a)
= (\<Sum>a \<in> a4. pmf (w_i_pmf n) a *\<^sub>R
(real_of_int (fst (rev_m (map a [(n-i)..<n])))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n-i)..<n]))))))"
   by auto
   have key:"\<And>a. a\<in>a4 \<Longrightarrow>  
(if (a (n-i)) then 
(fst (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n])))) = 1 \<and> 
(min 0 (snd (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n]))))) = 0
else 
(fst (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n])))) = 0 \<and> 
(min 0 (snd (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n]))))) = -1)"
  proof -
    fix a
    assume "a \<in> a4"
    have f:" fst (rev_m (map a [(n - i + 1)..<n])) = 0"
      by (metis (mono_tags, lifting) \<open>a \<in> a4\<close> a1234(4) drop_map_a mem_Collect_eq)
   have s:"snd (rev_m (map a [(n - i + 1)..<n])) = 0"
      by (metis (mono_tags, lifting) \<open>a \<in> a4\<close> a1234(4) drop_map_a mem_Collect_eq)
  have "rev_m (False#(map a [(n - i + 1)..<n])) 
        = (if fst (rev_m (map a [(n - i + 1)..<n])) > snd (rev_m (map a [(n - i + 1)..<n])) 
        \<and> snd (rev_m (map a [(n - i + 1)..<n])) = 0
        then (fst (rev_m (map a [(n - i + 1)..<n])) - 1, 0)
        else 
          (if fst (rev_m (map a [(n - i + 1)..<n])) = 0
          then (0, snd (rev_m (map a [(n - i + 1)..<n])) - 1)
          else (fst (rev_m (map a [(n - i + 1)..<n])) - 1, snd (rev_m (map a [(n - i + 1)..<n])) - 1)))"
    using rev_m.simps(3) by blast
  hence falsecase:"rev_m (False#(map a [(n - i + 1)..<n]))
        = (0, snd (rev_m (map a [(n - i + 1)..<n])) - 1)"
    using a1234
    by (smt s f)
  have truecase:"rev_m (True#(map a [(n - i + 1)..<n])) 
        = (fst (rev_m (map a [(n - i + 1)..<n])) + 1, snd (rev_m (map a [(n - i + 1)..<n])) + 1)"
    using rev_m.simps(2) by blast
  thus "(if (a (n-i)) then 
(fst (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n])))) = 1 \<and> 
(min 0 (snd (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n]))))) = 0
else 
(fst (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n])))) = 0 \<and> 
(min 0 (snd (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n]))))) = -1)"
    using falsecase f s truecase
    by fastforce
qed 
  define k where k:"k = (\<lambda>a. real_of_int (fst (rev_m (map a [(n-i)..<n])))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n-i)..<n])))))"
 hence tmp:"(\<Sum>a\<in>a4. ex a)  =
(\<Sum>a \<in> a4. pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a * k a)"
   using c4st1 w_i_pmf_def by auto
hence tmp2:"(\<Sum>a\<in>a4. ex a)  =
(\<Sum>a \<in> a4. (\<Prod>x\<in>{..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * k a)"
proof - 
  have 1:"finite {..<n}" by simp
  have "\<And>a. a \<in> a4 \<Longrightarrow>  (\<And>x. x \<notin> {..<n} \<Longrightarrow> a x = False)"
    by (simp add: a1234(4))
  hence "\<And>a. a \<in> a4 \<Longrightarrow> pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a =
        (\<Prod>x\<in>{..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)) x) (a x))"
    using 1 pmf_Pi'
    by (smt prod.cong)
  thus ?thesis
    using tmp by auto
qed
hence "(\<Sum>a\<in>a4. ex a)  =
(\<Sum>a \<in> a4. (\<Prod>x\<in>{..<n-i} \<union> {n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * k a)"
proof -
  have "{..<n} = {..<n-i} \<union> {n-i} \<union> {n-i<..<n}"
    using assms(1) assms(2) ivl_disj_un_singleton(2) by force
  thus ?thesis using tmp2
    by simp
qed
hence tmp:"(\<Sum>a\<in>a4. ex a)  =
(\<Sum>a \<in> a4. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n} \<union> {n-i}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * k a)"
  by simp
hence "(\<Sum>a\<in>a4. ex a)  =
(\<Sum>a \<in> a4. ((\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(\<Prod>x\<in> {n-i}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * k a)"
proof - 
  have "finite ({..<n-i} \<union> {n-i<..<n}) \<and> (finite {n-i}) \<and> (({..<n-i} \<union> {n-i<..<n}) \<inter> {n-i} = {})"
    by simp
  thus ?thesis
    using tmp Groups_Big.comm_monoid_mult_class.prod.union_disjoint
    by (metis (no_types, lifting) sum.cong)
qed
  hence "(\<Sum>a\<in>a4. ex a)  =
(\<Sum>a \<in> a4. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a)"
  by simp
  hence tmp:"(\<Sum>a\<in>a4 . ex a)  =
(\<Sum>a \<in> a4 \<inter> ({f. f (n-i) \<or> \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a)"
  by auto
  hence "(\<Sum>a\<in>a4. ex a)  =
(\<Sum>a \<in> a4  \<inter> ({f. f (n-i)} \<union> {f. \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a)"
  proof - 
    have "{f. f (n-i) \<or> \<not> f (n-i)} = {f. f (n-i)} \<union> {f. \<not> f (n-i)}"
      by auto
    thus ?thesis using tmp
      by auto
  qed
  hence tmp:"(\<Sum>a\<in>a4. ex a)  =
(\<Sum>a \<in> (a4 \<inter> {f. f (n-i)}) \<union> (a4 \<inter> {f. \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a)"
    by (simp add: inf_sup_distrib1)
 hence "(\<Sum>a\<in>a4. ex a)  =
((\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a)
+ (\<Sum>a \<in> a4 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a))"
 proof - 
   have 1:"(a4 \<inter> {f. f (n-i)}) \<inter> (a4 \<inter> {f. \<not> f (n-i)}) = {}"
     by blast
   have "finite a4" using a1234
     using fina by linarith
   hence "finite (a4 \<inter> {f. f (n-i)}) \<and> finite (a4 \<inter> {f. \<not> f (n-i)})"
     by auto
   thus ?thesis
     using tmp infinite_Un 1
     by (simp add: sum.union_disjoint)
 qed
hence "(\<Sum>a\<in>a4. ex a)  =
((\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * (real_of_int (fst (rev_m (map a [(n-i)..<n])))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n-i)..<n]))))))
+ (\<Sum>a \<in> a4 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * (real_of_int (fst (rev_m (map a [(n-i)..<n])))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n-i)..<n])))))))"
  using k  by auto
hence "(\<Sum>a\<in>a4. ex a)  =
((\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * (real_of_int (1)
 + \<alpha> * real_of_int (0)))
+ (\<Sum>a \<in> a4 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * (real_of_int (0)
 + \<alpha> * real_of_int (-1))))"
  using key
  by (smt Int_Collect consmap mult_cancel_left sum.cong)
hence "(\<Sum>a\<in>a4. ex a)  =
((\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * (1))
+ (\<Sum>a \<in> a4 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * (- \<alpha>)))"
  by auto
    hence "(\<Sum>a\<in>a4. ex a)  =
            (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * (1 - \<epsilon>) / 2 * 1) +
    (\<Sum>a\<in>a4 \<inter> {f. \<not> f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * (1 - (1-\<epsilon>)/2) * - \<alpha>)"
       using ep_gt_0 ep_le_1 by auto
 hence "(\<Sum>a\<in>a4. ex a)  =
            (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * (1 - \<epsilon>) / 2 ) +
    (\<Sum>a\<in>a4 \<inter> {f. \<not> f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * (1+\<epsilon>)/2 * - \<alpha>)"
   using honest_slot by auto
   hence "(\<Sum>a\<in>a4. ex a)  =
            (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * ((1 - \<epsilon>) / 2)) +
    (\<Sum>a\<in>a4 \<inter> {f. \<not> f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * (1+\<epsilon>)/2 * - \<alpha>)"
     by auto
    hence "(\<Sum>a\<in>a4. ex a)  =
            (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * ((1 - \<epsilon>) / 2)) +
    (\<Sum>a\<in>a4 \<inter> {f. \<not> f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * ((1+\<epsilon>)/2 * - \<alpha>))"
proof -
have f1: "(\<Sum>p\<in>a4 \<inter> {p. p (n - i)}. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * ((1 - \<epsilon>) / 2)) = (\<Sum>p\<in>a4 \<inter> {p. p (n - i)}. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * (1 - \<epsilon>) / 2)"
  using \<open>sum ex a4 = (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}. (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * ((1 - \<epsilon>) / 2)) + (\<Sum>a\<in>a4 \<inter> {f. \<not> f (n - i)}. (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * (1 + \<epsilon>) / 2 * - \<alpha>)\<close> \<open>sum ex a4 = (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}. (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * (1 - \<epsilon>) / 2) + (\<Sum>a\<in>a4 \<inter> {f. \<not> f (n - i)}. (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * (1 + \<epsilon>) / 2 * - \<alpha>)\<close> add_diff_cancel_right' by linarith
  have f2: "\<And>P. (\<Sum>p\<in>P. \<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * (- \<alpha> * ((1 + \<epsilon>) / 2)) = (\<Sum>p\<in>P. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * ((1 + \<epsilon>) / 2 * - \<alpha>))"
  by (metis (no_types) mult.commute sum_distrib_right)
  have f3: "\<And>P. (\<Sum>p\<in>P. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * (1 - \<epsilon>) / 2 * 1) = (\<Sum>p\<in>P. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * (1 - \<epsilon>) / 2)"
    by auto
  have f4: "\<And>P. - \<alpha> * ((1 + \<epsilon>) / 2 * (\<Sum>p\<in>P. \<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n))) = (\<Sum>p\<in>P. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * (1 - (1 - \<epsilon>) / 2) * - \<alpha>)"
    by (metis (no_types) honest_slot mult.commute sum_distrib_right)
  have "\<And>P. - \<alpha> * ((1 + \<epsilon>) / 2 * (\<Sum>p\<in>P. \<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n))) = (\<Sum>p\<in>P. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * ((1 + \<epsilon>) / 2 * - \<alpha>))"
    using f2
    by (simp add: mult.assoc mult.commute)  
  then show ?thesis
    using f4 f3 f1 \<open>sum ex a4 = (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}. (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * (1 - \<epsilon>) / 2 * 1) + (\<Sum>a\<in>a4 \<inter> {f. \<not> f (n - i)}. (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * (1 - (1 - \<epsilon>) / 2) * - \<alpha>)\<close> by presburger
qed
       hence tmp:"(\<Sum>a\<in>a4. ex a)  =
            (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x))) * ((1 - \<epsilon>) / 2) +
    (\<Sum>a\<in>a4 \<inter> {f. \<not> f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x))) * ((1+\<epsilon>)/2 * - \<alpha>)"
       proof -
         show ?thesis
           by (metis (no_types) \<open>sum ex a4 = (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}. (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * ((1 - \<epsilon>) / 2)) + (\<Sum>a\<in>a4 \<inter> {f. \<not> f (n - i)}. (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * ((1 + \<epsilon>) / 2 * - \<alpha>))\<close> sum_distrib_right)
       qed

  have case4eq:"(\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
=  (\<Sum>a \<in> a4 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) "
  proof - 
    define h where h:"h = (\<lambda>f x. (if x = n-i then \<not> f x else f x))"
    have "(\<forall>x\<in>(a4 \<inter> {f. f (n-i)}). \<forall>y\<in>(a4 \<inter> {f. f (n-i)}). h x = h y \<longrightarrow> x = y)"
    proof (rule ccontr)
      assume "\<not> (\<forall>x\<in>(a4 \<inter> {f. f (n-i)}). \<forall>y\<in>(a4 \<inter> {f. f (n-i)}). h x = h y \<longrightarrow> x = y)"
      then obtain x y where "x\<in>(a4 \<inter> {f. f (n-i)})" "y\<in>(a4 \<inter> {f. f (n-i)})" "h x = h y" "x \<noteq> y"
        by blast
       obtain j where "x j \<noteq> y j"
         using \<open>x \<noteq> y\<close> 
         by blast
       have "(h x) j = (h y) j"
         using \<open>h x = h y\<close>
         by simp
       hence "(\<lambda>f x. (if x = n-i then \<not> f x else f x)) x j = (\<lambda>f x. (if x = n-i then \<not> f x else f x)) y j"
         using h by blast
       hence "(if j = n-i then \<not> x j else x j) = (if j = n-i then \<not> y j else y j)"
         by simp
       hence "x j = y j"
         by presburger
       thus "False"
         using \<open>x j \<noteq> y j\<close> by auto
     qed
     hence "inj_on h (a4 \<inter> {f. f (n-i)})"
       by (meson inj_onI)
     have "h ` (a4 \<inter> {f. f (n-i)}) = a4 \<inter> {f. \<not> f (n-i)}" 
     proof - 
       have "\<And>a. a \<in> h ` (a4 \<inter> {f. f (n-i)}) \<Longrightarrow> a \<in> a4 \<inter> {f. \<not> f (n-i)}"
          proof -
           fix a
           assume "a \<in> h ` (a4 \<inter> {f. f (n-i)}) "
           then obtain b where "b \<in> (a4 \<inter> {f. f (n-i)})" "a = h b"
             by blast
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> b x = False) \<and> (fst (rev_m (drop (n-i+1) (map b [0..<n]))) = 0)
\<and> (snd (rev_m (drop (n-i+1) (map b [0..<n]))) = 0)) \<and> b (n-i)"
             using a1234(4) by blast
           hence condi1:"\<forall>x. (x \<notin> {..<n} \<longrightarrow> h b x = False)"
             using h by simp
           have "n - i + 1 \<le> n"
             using assms
             by linarith
           have ngt:"n - i + 1 > n - i" by simp
           have dropsimp:"(drop (n-i+1) (map (h b) [0..<n])) = (map (h b) [(n-i+1)..<n])"
             using drop_map_a by blast
           have "\<forall>x. n-i+1 \<le> x \<and> x < n \<longrightarrow> h b x = b x"
             using h by auto
           hence "\<And>x. x \<in> set [(n-i+1)..<n] \<Longrightarrow> h b x =b x"
             by simp
           hence "(map (h b) [(n-i+1)..<n]) =  (map b [(n-i+1)..<n])"
             using ngt list.map_cong0
             by blast
           hence "(drop (n-i+1) (map (h b) [0..<n])) = (drop (n-i+1) (map b [0..<n]))"
             using dropsimp
             by (simp add: drop_map)
           hence condi2:"(fst (rev_m (drop (n-i+1) (map (h b) [0..<n]))) = 0) 
\<and> (snd (rev_m (drop (n-i+1) (map (h b) [0..<n]))) = 0)"
             using tmp
             by simp
           have "\<not> h b (n-i)"
             using tmp h
             by auto
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> h b x = False) \<and> (fst (rev_m (drop (n-i+1) (map (h b) [0..<n]))) = 0) 
                    \<and> (snd (rev_m (drop (n-i+1) (map (h b) [0..<n]))) = 0)) \<and> \<not> h b (n-i)"
             using h condi1 condi2 by blast
           hence "h b \<in> a4 \<inter> {f. \<not> f (n-i)}"
             using a1234(4) by blast
           thus "a \<in> a4 \<inter> {f. \<not> f (n-i)}"
             using \<open>a = h b\<close> by simp
         qed
       hence  "h ` (a4 \<inter> {f. f (n-i)}) \<subseteq> a4 \<inter> {f. \<not> f (n-i)}"
         by blast
       have "\<And>a. a \<in> a4 \<inter> {f. \<not> f (n-i)} \<Longrightarrow> a \<in> h ` (a4 \<inter> {f. f (n-i)})"
       proof - 
         fix a assume "a \<in> a4 \<inter> {f. \<not> f (n-i)}"
         hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> a x = False) \<and> (fst (rev_m (drop (n-i+1) (map a [0..<n]))) = 0)
            \<and> (snd (rev_m (drop (n-i+1) (map a [0..<n]))) = 0)) \<and> \<not> a (n-i)"
           using a1234(4) by blast
         obtain b where "b = h a"
           by simp
         hence condi1:"\<forall>x. (x \<notin> {..<n} \<longrightarrow> h a x = False)"
             using h tmp
             using assms(1) assms(2) diff_diff_cancel by auto 
           have "n - i + 1 \<le> n"
             using assms
             by linarith
           have ngt:"n - i + 1 > n - i" by simp
           have dropsimp:"(drop (n-i+1) (map (h a) [0..<n])) = (map (h a) [(n-i+1)..<n])"
             using drop_map_a by blast
           have "\<forall>x. n-i+1 \<le> x \<and> x < n \<longrightarrow> h a x = a x"
             using h by auto
           hence "\<And>x. x \<in> set [(n-i+1)..<n] \<Longrightarrow> h a x =a x"
             by simp
           hence "(map (h a) [(n-i+1)..<n]) =  (map a [(n-i+1)..<n])"
             using ngt list.map_cong0
             by blast
           hence "(drop (n-i+1) (map (h a) [0..<n])) = (drop (n-i+1) (map a [0..<n]))"
             using dropsimp
             by (simp add: drop_map)
           hence condi2:"(fst (rev_m (drop (n-i+1) (map (h a) [0..<n]))) = 0) 
\<and> (snd (rev_m (drop (n-i+1) (map (h a) [0..<n]))) = 0)"
             using tmp
             by simp
           have "h a (n-i)"
             using tmp h
             by auto
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> b x = False) \<and> (fst (rev_m (drop (n-i+1) (map b [0..<n]))) = 0) 
                    \<and> (snd (rev_m (drop (n-i+1) (map b [0..<n]))) = 0)) \<and> b (n-i)"
             using h condi1 condi2 \<open>b = h a\<close> by simp
           hence "b \<in> a4 \<inter> {f. f (n-i)}"
             using a1234(4) by blast
           have "h b = a"
           proof - 
             have "h b = h (h a)"
               using \<open>b = h a\<close> by simp
             hence "h (h a) = (\<lambda>f x. (if x = n-i then \<not> f x else f x)) ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a)"
               by (simp add: h)
             hence "h (h a) = 
(\<lambda>x. (if x = n-i then \<not> ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a) x else ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a) x)) " by simp
             hence "h (h a) = 
(\<lambda>x. (if x = n-i then \<not> (\<lambda>x. (if x = n-i then \<not> a x else a x)) x else (\<lambda>x. (if x = n-i then \<not> a x else a x)) x))" by simp
             hence "\<And>x. h (h a) x = 
(if x = n-i then \<not> (if x = n-i then \<not> a x else a x) else (if x = n-i then \<not> a x else a x))"
               by simp
             hence "\<And>x. h (h a) x = a x"
               by simp
              hence "h (h a) = a"
                by blast
              thus ?thesis
                by (simp add: \<open>b = h a\<close>)
            qed
              hence "(h b) \<in> h ` (a4 \<inter> {f. f (n-i)})"
                using \<open>b \<in> a4 \<inter> {f. f (n - i)}\<close> by blast
              thus "a \<in> h ` (a4 \<inter> {f. f (n-i)})"
                using \<open>h b = a\<close> by simp
            qed
            thus ?thesis
              using \<open>h ` (a4 \<inter> {f. f (n - i)}) \<subseteq> a4 \<inter> {f. \<not> f (n - i)}\<close> by blast
          qed
          hence "bij_betw h  (a4 \<inter> {f. f (n - i)}) (a4 \<inter> {f. \<not> f (n - i)})"
            by (simp add: \<open>inj_on h (a4 \<inter> {f. f (n - i)})\<close> bij_betw_def)
          hence "(\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (h a)) 
              =  sum (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (a4 \<inter> {f. \<not> f (n-i)})"
            using sum.reindex_bij_betw by simp
            hence "(\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (h a)) 
              =  (\<Sum>a \<in> a4 \<inter> { f.\<not> f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) a)"
              by blast
        hence tmp:"(\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x))) 
              =  (\<Sum>a \<in> a4 \<inter> { f.\<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          by simp
        have "\<And>a. a \<in> a4 \<inter> {f. f (n-i)} \<Longrightarrow> 
(\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x)) = 
(\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))"
          by (smt Un_iff greaterThanLessThan_iff h lessThan_iff less_irrefl prod.cong)
        hence "(\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x))) = 
              (\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          by auto
        thus "(\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
              =  (\<Sum>a \<in> a4 \<inter> { f.\<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          using tmp
          by linarith
      qed      
      hence tmp:"(\<Sum>a\<in>a4. ex a)  =
            (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x))) * ((1 - \<epsilon>) / 2) +
    (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x))) * ((1 + \<epsilon>) / 2) * - \<alpha>"
        using tmp
        by simp
      define s where s:"s = (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)))"
hence "(\<Sum>a\<in>a4. ex a)  =
            s * ((1 - \<epsilon>) / 2) +
            s * ((1 + \<epsilon>) / 2) * - \<alpha>"
  using tmp by simp
  hence "(\<Sum>a\<in>a4. ex a)  =
            s * (((1 - \<epsilon>) / 2) + ((1 + \<epsilon>) / 2) * - \<alpha>)"
    by (metis (no_types, hide_lams) linordered_field_class.sign_simps(36) mult.commute mult.left_commute)
 
hence tmp:"(\<Sum>a\<in>a4. ex a)  =
             (
((\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) + 
(\<Sum>a \<in> a4 \<inter> {f. \<not>f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))))/2
   * (((1 - \<epsilon>) / 2) + ((1 + \<epsilon>) / 2) * - \<alpha>))" using case4eq s
  by simp
hence tmp:"(\<Sum>a\<in>a4. ex a)  =
         (
(\<Sum>a \<in> (a4 \<inter> {f. f (n-i)}) \<union> (a4 \<inter> {f. \<not>f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))/2
  * (((1 - \<epsilon>) / 2) + ((1 + \<epsilon>) / 2) * - \<alpha>))" using case1eq pair_disj_aux_i sum.union_disjoint infinite_Un
proof - 
  have fin:"finite (a4 \<inter> {f. f (n-i)}) \<and> finite (a4 \<inter> {f. \<not>f (n-i)})"
    using finite_False_func_set a1234(4) ep_gt_0 ep_le_1 by auto
  have "(a4 \<inter> {f. f (n-i)}) \<inter> (a4 \<inter> {f. \<not>f (n-i)}) = {}"
    by auto
  thus ?thesis  using case3eq pair_disj_aux_i sum.union_disjoint infinite_Un tmp fin
    by smt
qed
hence tmp:"(\<Sum>a\<in>a4. ex a)  = ((\<Sum>a \<in> a4. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))/2
  * (((1 - \<epsilon>) / 2) + ((1 + \<epsilon>) / 2) * - \<alpha>))"
proof - 
  have "(a4 \<inter> {f. f (n-i)}) \<union> (a4 \<inter> {f. \<not>f (n-i)}) = a4"
    by blast
  thus ?thesis using tmp by simp
qed
  define D where D:"D = (\<Sum>a \<in> a4. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))" 
  hence simplifiedcase4:"(\<Sum>a\<in>a4. ex a)  = D/2 * (((1 - \<epsilon>) / 2) + ((1 + \<epsilon>) / 2) * - \<alpha>)"
    using tmp by simp
  have tmp:"measure_pmf.expectation (bind_pmf (w_i_pmf n) (\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x))) (\<Delta>_n i) =
(1 + \<alpha>) * A/2 * (-\<epsilon>)+
B/2 * (-\<epsilon>)+
C/2 * (-\<epsilon>)+
D/2 * (((1 - \<epsilon>) / 2) + ((1 + \<epsilon>) / 2) * - \<alpha>)"
    using bigdist simplifiedcase1 simplifiedcase2 simplifiedcase3 simplifiedcase4 by simp
  hence tmp2:"measure_pmf.expectation (bind_pmf (w_i_pmf n) (\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x))) (\<Delta>_n i) \<le>
A/2 * (-\<epsilon>)+
B/2 * (-\<epsilon>)+
C/2 * (-\<epsilon>)+
D/2 * (-\<epsilon>)"
  proof - 
    have al_ge_1:"\<alpha> \<ge>1"
      using \<alpha> ep_le_1 ep_gt_0
      by simp 
    hence gt0:"1 + \<alpha> > 0"
      by auto
    have "\<forall>x. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) x \<ge> 0"
      by simp
    hence "\<forall>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) \<ge> 0"
      by (simp add: prod_nonneg)
    hence "(\<Sum>a \<in> a1. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) \<ge> 0
\<and> (\<Sum>a \<in> a4. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) \<ge> 0"
      by (simp add: sum_nonneg)
    hence "A/2 \<ge> 0 \<and> D/2 \<ge>0"
      using A D
      by linarith
    hence "(1 + \<alpha>) * (A/2) \<ge> (A/2)"
      using gt0
proof -
  have f1: "\<not> \<alpha> \<le> - 1"
    using \<open>0 < 1 + \<alpha>\<close> by force
  have "(1 + \<alpha> = 0) = (\<alpha> = - 1)"
by fastforce
  then have f2: "(1 + \<alpha>) / ((1 + \<alpha>) * (A / 2)) = 1 / (A / 2)"
    using f1 nonzero_divide_mult_cancel_left by blast
  have f3: "1 + \<alpha> + - 1 = \<alpha>"
    by auto
  have f4: "- (1::real) * 1 = - 1"
    by auto
have f5: "\<not> (1::real) \<le> 0"
by auto
have f6: "\<forall>x0 x1. ((x1::real) < x0) = (\<not> x0 + - 1 * x1 \<le> 0)"
by auto
  have f7: "\<forall>x1. ((0::real) < x1) = (\<not> x1 \<le> 0)"
    by auto
have f8: "\<forall>x2 x3. ((x3::real) \<le> x2) = (0 \<le> x2 + - 1 * x3)"
  by simp
have f9: "0 \<le> \<alpha>"
using \<open>1 \<le> \<alpha>\<close> by linarith
  have f10: "(1 + \<alpha> \<le> 0) = (\<alpha> \<le> - 1)"
    by auto
have "(1 + \<alpha>) / ((1 + \<alpha>) * (A / 2)) + - 1 * (1 / (A / 2)) \<le> 0"
using f2 by auto
  moreover
  { assume "(1 + \<alpha>) * (A / 2) \<le> 0"
    then have "A / 2 \<le> 0"
      using f10 f7 f1 by (meson linordered_field_class.sign_simps(44))
    then have "A / 2 + - 1 * ((1 + \<alpha>) * (A / 2)) \<le> 0"
      using \<open>0 \<le> A / 2 \<and> 0 \<le> D / 2\<close> by fastforce }
  ultimately have "A / 2 + - 1 * ((1 + \<alpha>) * (A / 2)) \<le> 0"
    using f9 f8 f7 f6 f5 f4 f3 by (metis (full_types) frac_less2)
  then show ?thesis
    by fastforce
qed
  hence AA:"(1 + \<alpha>) * A/2 * (- \<epsilon>) \<le> A/2 * (- \<epsilon>)"
    using ep_gt_0 by simp
  have x:"((1 + \<epsilon>) / 2) \<ge> 0"
    using ep_gt_0 by simp
  have "- \<alpha> \<le> - 1" using al_ge_1 by simp 
  hence "((1 + \<epsilon>) / 2) * (- \<alpha>) \<le> ((1 + \<epsilon>) / 2) * (-1)"
    using x
    using ordered_comm_semiring_class.comm_mult_left_mono by blast
hence "((1 + \<epsilon>) / 2) * (- \<alpha>) \<le> - ((1 + \<epsilon>) / 2)"
  by simp
  hence "(1-\<epsilon>)/2 + ((1 + \<epsilon>) / 2) * (- \<alpha>) \<le> (1-\<epsilon>)/2 - ((1 + \<epsilon>) / 2)"
    by simp
hence "(1-\<epsilon>)/2 + ((1 + \<epsilon>) / 2) * (- \<alpha>) \<le> -\<epsilon>"
  by (smt real_average_minus_first)  
hence "D/2 * ((1-\<epsilon>)/2 + ((1 + \<epsilon>) / 2) * (- \<alpha>)) \<le> D/2 * -\<epsilon>" using \<open>A/2 \<ge> 0 \<and> D/2 \<ge>0\<close>
  using ordered_comm_semiring_class.comm_mult_left_mono by blast
  hence "(1 + \<alpha>) * A/2 * (-\<epsilon>)+
B/2 * (-\<epsilon>)+
C/2 * (-\<epsilon>)+
D/2 * (((1 - \<epsilon>) / 2) + ((1 + \<epsilon>) / 2) * - \<alpha>)\<le>
(1 + \<alpha>) * A/2 * (-\<epsilon>)+
B/2 * (-\<epsilon>)+
C/2 * (-\<epsilon>)+
D/2 * (-\<epsilon>)"
    by simp
hence "(1 + \<alpha>) * A/2 * (-\<epsilon>)+
B/2 * (-\<epsilon>)+
C/2 * (-\<epsilon>)+
D/2 * (((1 - \<epsilon>) / 2) + ((1 + \<epsilon>) / 2) * - \<alpha>)\<le>
A/2 * (-\<epsilon>)+
B/2 * (-\<epsilon>)+
C/2 * (-\<epsilon>)+
D/2 * (-\<epsilon>)"
  using AA
  by linarith
  thus ?thesis using tmp
    by simp
qed
  also have "... = (A/2 + B/2 + C/2 + D/2) * (-\<epsilon>)"
    by (simp add: comm_semiring_class.distrib)
  also have "... = (A+B+C+D) / 2 * (-\<epsilon>)"
    by simp
  also have tmp:"... = ((\<Sum>a \<in> a1. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))
+(\<Sum>a \<in> a2. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))
+(\<Sum>a \<in> a3. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))
+(\<Sum>a \<in> a4. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))) /2 * (-\<epsilon>)"
    using A B C D by blast
  also have "... = ((\<Sum>a \<in> a1 \<union> a2. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) +  
(\<Sum>a \<in> a3. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) +                  
(\<Sum>a \<in> a4. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))))/2 * (-\<epsilon>)"
    using fina pair_disj_aux_i sum.union_disjoint infinite_Un subs_finite_i a1234 pair_disj_aux_i
  proof -
    have "finite a1 \<and> finite a2 \<and> a1 \<inter> a2 = {}"
      by (smt a1234(1) a1234(2) disjoint_iff_not_equal fina mem_Collect_eq)
    thus ?thesis 
      using tmp
      by (simp add: sum.union_disjoint)
  qed
 also have "... = ((\<Sum>a \<in> a1 \<union> a2 \<union> a3. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) +                  
(\<Sum>a \<in> a4. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))))/2 * (-\<epsilon>)"
   using fina pair_disj_aux_i sum.union_disjoint infinite_Un subs_finite_i a1234 pair_disj_aux_i
 proof -
   have fin:"finite (a1 \<union> a2) \<and> finite a3"
     using a1234 fina
     by blast
   have "a1 \<inter> a3 = {} \<and> a2 \<inter> a3 = {}"  
     by (smt a1234 disjoint_iff_not_equal fina mem_Collect_eq)
   thus ?thesis
     using fin
     by (simp add: distrib(4) sum.union_disjoint)
 qed
also have tmp:"... = ((\<Sum>a \<in> a1 \<union> a2 \<union> a3 \<union> a4. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))))/2 * (-\<epsilon>)"
    using fina pair_disj_aux_i sum.union_disjoint infinite_Un subs_finite_i a1234 pair_disj_aux_i
  proof -
    have fin:"finite (a1 \<union> a2 \<union> a3) \<and> finite a4"
     using a1234 fina
     by blast
   have "a1 \<inter> a4 = {} \<and> a2 \<inter> a4 = {} \<and> a3 \<inter> a4 = {}"  
     by (smt a1234 disjoint_iff_not_equal fina mem_Collect_eq)
   thus ?thesis
     using fin
     by (simp add: distrib(4) sum.union_disjoint)
 qed
  finally have lecheck:"measure_pmf.expectation (w_pmf n) (\<Delta>_n i) \<le> 
((\<Sum>a \<in> {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))))/2 * (-\<epsilon>)"
  proof - 
    have "n - i + 1 \<le> n"
      using assms by auto
    hence "{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False)} = a1 \<union> a2 \<union> a3 \<union> a4"
      using a1234 four_cases_i assms
      by meson
    thus ?thesis
      using tmp
      using A B C D \<open>((\<Sum>a\<in>a1 \<union> a2. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
(a x)) + (\<Sum>a\<in>a3. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) + 
(\<Sum>a\<in>a4. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x))) / 2 * - \<epsilon> = 
((\<Sum>a\<in>a1 \<union> a2 \<union> a3. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) + 
(\<Sum>a\<in>a4. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x))) / 2 * - \<epsilon>\<close> 
\<open>((\<Sum>a\<in>a1. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) + (\<Sum>a\<in>a2.
 \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) + (\<Sum>a\<in>a3. \<Prod>x\<in>{..<n - i} 
\<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) + (\<Sum>a\<in>a4. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. 
pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x))) / 2 * - \<epsilon> = ((\<Sum>a\<in>a1 \<union> a2. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}.
 pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) + (\<Sum>a\<in>a3. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf 
(bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) + (\<Sum>a\<in>a4. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf
 ((1 - \<epsilon>) / 2)) (a x))) / 2 * - \<epsilon>\<close> \<open>(A / 2 + B / 2 + C / 2 + D / 2) * - \<epsilon> = (A + B + C + D) / 2 * - \<epsilon>\<close>
 \<open>A / 2 * - \<epsilon> + B / 2 * - \<epsilon> + C / 2 * - \<epsilon> + D / 2 * - \<epsilon> = (A / 2 + B / 2 + C / 2 + D / 2) * - \<epsilon>\<close> 
\<open>measure_pmf.expectation (local.w_pmf n) (local.\<Delta>_n i) = measure_pmf.expectation (local.w_i_pmf n 
\<bind> (\<lambda>x. return_pmf (map x [0..<n]))) (local.\<Delta>_n i)\<close> tmp2 by presburger
  qed
  define X where X: "X = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False)}"
  have eqX:"(\<Sum>a \<in> X \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
=  (\<Sum>a \<in> X \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) "
  proof - 
    define h where h:"h = (\<lambda>f x. (if x = n-i then \<not> f x else f x))"
    have "(\<forall>x\<in>(X \<inter> {f. f (n-i)}). \<forall>y\<in>(X \<inter> {f. f (n-i)}). h x = h y \<longrightarrow> x = y)"
    proof (rule ccontr)
      assume "\<not> (\<forall>x\<in>(X \<inter> {f. f (n-i)}). \<forall>y\<in>(X \<inter> {f. f (n-i)}). h x = h y \<longrightarrow> x = y)"
      then obtain x y where "x\<in>(X \<inter> {f. f (n-i)})" "y\<in>(X \<inter> {f. f (n-i)})" "h x = h y" "x \<noteq> y"
        by blast
       obtain j where "x j \<noteq> y j"
         using \<open>x \<noteq> y\<close> 
         by blast
       have "(h x) j = (h y) j"
         using \<open>h x = h y\<close>
         by simp
       hence "(\<lambda>f x. (if x = n-i then \<not> f x else f x)) x j = (\<lambda>f x. (if x = n-i then \<not> f x else f x)) y j"
         using h by blast
       hence "(if j = n-i then \<not> x j else x j) = (if j = n-i then \<not> y j else y j)"
         by simp
       hence "x j = y j"
         by presburger
       thus "False"
         using \<open>x j \<noteq> y j\<close> by auto
     qed
     hence "inj_on h (X \<inter> {f. f (n-i)})"
       by (meson inj_onI)
     have "h ` (X \<inter> {f. f (n-i)}) = X \<inter> {f. \<not> f (n-i)}" 
     proof - 
       have "\<And>a. a \<in> h ` (X \<inter> {f. f (n-i)}) \<Longrightarrow> a \<in> X \<inter> {f. \<not> f (n-i)}"
          proof -
           fix a
           assume "a \<in> h ` (X \<inter> {f. f (n-i)}) "
           then obtain b where "b \<in> (X \<inter> {f. f (n-i)})" "a = h b"
             by blast
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> b x = False)) \<and> b (n-i)"
             using X by blast
           hence condi1:"\<forall>x. (x \<notin> {..<n} \<longrightarrow> h b x = False)"
             using h by simp
           have "\<not> h b (n-i)"
             using tmp h
             by auto
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> h b x = False)) \<and> \<not> h b (n-i)"
             using h condi1 by blast
           hence "h b \<in> X \<inter> {f. \<not> f (n-i)}"
             using X by blast
           thus "a \<in> X \<inter> {f. \<not> f (n-i)}"
             using \<open>a = h b\<close> by simp
         qed
       hence  "h ` (X \<inter> {f. f (n-i)}) \<subseteq> X \<inter> {f. \<not> f (n-i)}"
         by blast
       have "\<And>a. a \<in> X \<inter> {f. \<not> f (n-i)} \<Longrightarrow> a \<in> h ` (X \<inter> {f. f (n-i)})"
       proof - 
         fix a assume "a \<in> X \<inter> {f. \<not> f (n-i)}"
         hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> a x = False)) \<and> \<not> a (n-i)"
           using X by blast
         obtain b where "b = h a"
           by simp
         hence condi1:"\<forall>x. (x \<notin> {..<n} \<longrightarrow> h a x = False)"
             using h tmp
             using assms(1) assms(2) diff_diff_cancel by auto 
           have "h a (n-i)"
             using tmp h
             by auto
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> b x = False)) \<and> b (n-i)"
             using h condi1  \<open>b = h a\<close> by simp
           hence "b \<in> X \<inter> {f. f (n-i)}"
             using X by blast
           have "h b = a"
           proof - 
             have "h b = h (h a)"
               using \<open>b = h a\<close> by simp
             hence "h (h a) = (\<lambda>f x. (if x = n-i then \<not> f x else f x)) ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a)"
               by (simp add: h)
             hence "h (h a) = 
(\<lambda>x. (if x = n-i then \<not> ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a) x else ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a) x)) " by simp
             hence "h (h a) = 
(\<lambda>x. (if x = n-i then \<not> (\<lambda>x. (if x = n-i then \<not> a x else a x)) x else (\<lambda>x. (if x = n-i then \<not> a x else a x)) x))" by simp
             hence "\<And>x. h (h a) x = 
(if x = n-i then \<not> (if x = n-i then \<not> a x else a x) else (if x = n-i then \<not> a x else a x))"
               by simp
             hence "\<And>x. h (h a) x = a x"
               by simp
              hence "h (h a) = a"
                by blast
              thus ?thesis
                by (simp add: \<open>b = h a\<close>)
            qed
              hence "(h b) \<in> h ` (X \<inter> {f. f (n-i)})"
                using \<open>b \<in> X \<inter> {f. f (n - i)}\<close> by blast
              thus "a \<in> h ` (X \<inter> {f. f (n-i)})"
                using \<open>h b = a\<close> by simp
            qed
            thus ?thesis
              using \<open>h ` (X \<inter> {f. f (n - i)}) \<subseteq> X \<inter> {f. \<not> f (n - i)}\<close> by blast
          qed
          hence "bij_betw h  (X \<inter> {f. f (n - i)}) (X \<inter> {f. \<not> f (n - i)})"
            by (simp add: \<open>inj_on h (X \<inter> {f. f (n - i)})\<close> bij_betw_def)
          hence "(\<Sum>a \<in> X \<inter> {f. f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (h a)) 
              =  sum (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (X \<inter> {f. \<not> f (n-i)})"
            using sum.reindex_bij_betw by simp
            hence "(\<Sum>a \<in> X \<inter> {f. f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (h a)) 
              =  (\<Sum>a \<in> X \<inter> { f.\<not> f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) a)"
              by blast
        hence tmp:"(\<Sum>a \<in> X \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x))) 
              =  (\<Sum>a \<in> X \<inter> { f.\<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          by simp
        have "\<And>a. a \<in> X \<inter> {f. f (n-i)} \<Longrightarrow> 
(\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x)) = 
(\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))"
          by (smt Un_iff greaterThanLessThan_iff h lessThan_iff less_irrefl prod.cong)
        hence "(\<Sum>a \<in> X \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x))) = 
              (\<Sum>a \<in> X \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          by auto
        thus "(\<Sum>a \<in> X \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
              =  (\<Sum>a \<in> X \<inter> { f.\<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          using tmp
          by linarith
      qed      
      hence tmp:"(\<Sum>a\<in>X.  (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  =
            (\<Sum>a\<in>X \<inter> {f. f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x))) +
            (\<Sum>a\<in>X \<inter> {f. \<not> f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)))"
      proof - 
        have fin:"finite (X \<inter> {f. f (n - i)}) \<and> finite (X \<inter> {f. \<not> f (n - i)})"
          using finite_False_func_set X by simp
        have U:"(X \<inter> {f. f (n - i)}) \<union> (X \<inter> {f. \<not> f (n - i)}) = X"
          by auto
        have "(X \<inter> {f. f (n - i)}) \<inter> (X \<inter> {f. \<not> f (n - i)}) = {}"
          by auto
        thus ?thesis
          using fin U
          by (smt sum.union_disjoint)


      qed
      hence eq2tmp:"(\<Sum>a\<in>X.  (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  =
            2 * (\<Sum>a\<in>X \<inter> {f. \<not> f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)))"
        using eqX
        by linarith
  have fin:"finite ({..<n-i} \<union> {n-i<..<n})" by simp
  have setpmf:"\<forall>a \<in> (X \<inter> {f. \<not> f (n - i)}). (\<forall>x. x \<notin> ({..<n-i} \<union> {n-i<..<n}) \<longrightarrow> a x = False)"
    using X by auto
  hence "\<forall>a \<in> (X \<inter> {f. \<not> f (n - i)}). pmf (Pi_pmf ({..<n - i} \<union> {n - i<..<n}) False (\<lambda>_. bernoulli_pmf ((1 - \<epsilon>) / 2))) a 
        = (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1 - \<epsilon>) / 2)) x) (a x))"
    using fin pmf_Pi'
    by (smt prod.cong) 
  hence eq2tmp':"(\<Sum>a\<in>X.  (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  =
            2 * 
(\<Sum>a\<in>X \<inter> {f. \<not> f (n - i)}. pmf (Pi_pmf ({..<n - i} \<union> {n - i<..<n}) False (\<lambda>_. bernoulli_pmf ((1 - \<epsilon>) / 2))) a)"
    by (simp add: eq2tmp)
  have setpmf1:"set_pmf (Pi_pmf ({..<n - i} \<union> {n - i<..<n}) False (\<lambda>_. bernoulli_pmf ((1 - \<epsilon>) / 2)))
        \<subseteq> {f. \<forall>x. (x \<notin> ({..<n - i} \<union> {n - i<..<n})\<longrightarrow> f x = False)}"
    apply -
    apply (rule set_Pi_pmf_subset)
    using fin by simp
  have  tmp:"{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False)} \<inter> {f. \<not> f (n - i)}  
          = {f. (\<forall>x. (x \<in> ({..<n - i} \<union> {n-i} \<union> {n - i<..<n})  \<inter> - {n - i}) \<or> f x = False)}"
  proof - 
    have "n - i \<le> n" using assms by simp
    hence "{..<n} = {..<n - i} \<union> {n-i} \<union> {n - i<..<n}"
      using assms(1) assms(2) by force
    thus ?thesis 
      by blast
  qed
  have "({..<n - i} \<union> {n-i} \<union> {n - i<..<n})  \<inter> - {n - i}  = 
          ({..<n - i} \<inter> - {n - i} \<union> {n-i} \<inter> - {n - i} \<union> {n - i<..<n} \<inter> - {n - i}) "
    by blast
  hence "({..<n - i} \<union> {n-i} \<union> {n - i<..<n})  \<inter> - {n - i}  = 
          ({..<n - i} \<union> {n - i<..<n}  \<union> {n-i})  \<inter> - {n - i}  "
    using inf.absorb_iff2 subsetI 
    by auto
hence "({..<n - i} \<union> {n-i} \<union> {n - i<..<n})  \<inter> - {n - i}  = 
         ( {..<n - i} \<union> {n - i<..<n})  \<inter> - {n - i}  "
  using inf.absorb_iff2 subsetI
  by blast 
hence "({..<n - i} \<union> {n-i} \<union> {n - i<..<n})  \<inter> - {n - i}  = 
         ( {..<n - i} \<union> {n - i<..<n})  "
proof - 
have "{n - i<..<n} \<inter>  {n - i} = {}"
  by auto
  hence  "( {..<n - i} \<union> {n - i<..<n})  \<inter> {n - i} = {}"
    by blast
  hence "( {..<n - i} \<union> {n - i<..<n})  \<inter> - {n - i} = ( {..<n - i} \<union> {n - i<..<n})"
    by blast
  thus ?thesis
    by blast
qed

  hence "{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False)} \<inter> {f. \<not> f (n - i)}  
          = {f. (\<forall>x. (x \<in> {..<n - i} \<union> {n - i<..<n}) \<or> f x = False)}"
    using tmp by simp
hence "{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False)} \<inter> {f. \<not> f (n - i)}  
          = {f. (\<forall>x. (x \<in> {..<n - i} \<union> {n - i<..<n}) \<or> f x = False)}"
    using tmp by simp
  hence "{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False)} \<inter> {f. \<not> f (n - i)}  
          = {f. (\<forall>x. (x \<notin> ({..<n - i} \<union> {n - i<..<n})\<longrightarrow> f x = False))}"
    by linarith
  hence setpmf2:"set_pmf (Pi_pmf ({..<n - i} \<union> {n - i<..<n}) False (\<lambda>_. bernoulli_pmf ((1 - \<epsilon>) / 2)))
        \<subseteq> X \<inter> {f. \<not> f (n - i)}"
    using setpmf1 X
    by simp 
  hence "(\<Sum>a\<in>X \<inter> {f. \<not> f (n - i)}. pmf (Pi_pmf ({..<n - i} \<union> {n - i<..<n}) False (\<lambda>_. bernoulli_pmf ((1 - \<epsilon>) / 2))) a) = 1"
    using sum_pmf_eq_1 fin  X finite_False_func_set
  proof -
    have "finite X"
      using X finite_False_func_set by blast
    then show ?thesis
      by (metis infinite_Un setpmf2 sum_pmf_eq_1 sup_inf_absorb)
  qed
 hence eq2:"(\<Sum>a\<in>X.  (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  = 2"
   using eq2tmp' by linarith 
  hence "(\<Sum>a \<in> {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  = 2"
    using X 
    by simp
  thus ?thesis using lecheck by auto
qed

lemma sum_delta:
  assumes "\<forall>x. \<exists>y::real. f x = y"
  shows "(\<Sum>a \<in> {1..(n::nat)}. f a - f (a-1)) = f n - f 0"
proof (induction n)
  case 0
  have "0 < (1::nat)"
    by simp
  hence "{1..(0::nat)} = {}"
    using atLeastatMost_empty by simp
  hence "(\<Sum>a \<in> {1..(0::nat)}. f a - f (a-1)) = 0"
    by simp
  then show ?case 
    by simp
next
  case (Suc n)
  hence "(\<Sum>a \<in> {1..(n::nat)}. f a - f (a-1)) = f n - f 0" by simp
  hence "(\<Sum>a \<in> {1..(n::nat)}. f a - f (a-1)) + (f(n+1) - f n) = f (n+1) - f 0" by simp
  hence "(\<Sum>a \<in> {1..(n::nat)}. f a - f (a-1)) + (\<Sum>a \<in> {n+1::nat}.f a - f (a-1)) = f (n+1) - f 0"
    by simp
  hence "(\<Sum>a \<in> {1..(n::nat)} \<union> {n+1}. f a - f (a-1)) = f (n+1) - f 0"
  by simp
  then show ?case
    by simp
qed

lemma \<Phi>_n_0:
  "\<Phi>_n 0 l = 0"  
  using \<Phi>_n_def by simp

lemma exist_\<Phi>: "\<exists>x::real. \<Phi>_n n l = x"
  by simp

lemma  sum_delta_\<Phi>:
  "(\<Sum>a \<in> {1..(n::nat)}. (\<lambda>x. \<Phi>_n x l) a - (\<lambda>x. \<Phi>_n x l) (a-1)) = (\<lambda>x. \<Phi>_n x l) n - (\<lambda>x. \<Phi>_n x l) 0"
  using exist_\<Phi> sum_delta by simp

lemma swap_sum:"(\<Sum>b \<in> {1..(n::nat)}. 
(\<Sum>a\<in> A.           
 x a *\<^sub>R (\<Delta>_n b (y a))))
= (\<Sum>a\<in> A. (\<Sum>b \<in> {1..(n::nat)}.          
 x a *\<^sub>R (\<Delta>_n b (y a))))"
proof (induction n)
case 0
then show ?case
  by auto 
next
  case (Suc n)
  have "(\<Sum>b \<in> {1..(n::nat)}. 
(\<Sum>a\<in> A.           
 x a *\<^sub>R (\<Delta>_n b (y a))))
= (\<Sum>a\<in> A. (\<Sum>b \<in> {1..(n::nat)}.          
x a *\<^sub>R (\<Delta>_n b (y a))))"
    using Suc.IH by blast
  hence "(\<Sum>b \<in> {1..(n::nat)}. 
(\<Sum>a\<in> A.           
x a *\<^sub>R (\<Delta>_n b (y a)))) + 
(\<Sum>a\<in> A.           
 x a *\<^sub>R (\<Delta>_n (Suc n) (y a)))
=
(\<Sum>a\<in> A. (\<Sum>b \<in> {1..(n::nat)}.          
 x a *\<^sub>R (\<Delta>_n b (y a))))
+
(\<Sum>a\<in> A.           
x a *\<^sub>R (\<Delta>_n (Suc n) (y a)))"
    by linarith
hence "(\<Sum>b \<in> {1..(Suc n::nat)}. 
(\<Sum>a\<in> A.           
 x a *\<^sub>R (\<Delta>_n b (y a))))
=
(\<Sum>a\<in> A. (\<Sum>b \<in> {1..(n::nat)}.          
 x a *\<^sub>R (\<Delta>_n b (y a))))
+
(\<Sum>a\<in> A.           
 x a *\<^sub>R (\<Delta>_n (Suc n) (y a)))"
  by simp
hence "(\<Sum>b \<in> {1..(Suc n::nat)}. 
(\<Sum>a\<in> A.           
x a *\<^sub>R (\<Delta>_n b (y a))))
=
(\<Sum>a\<in> A. (\<Sum>b \<in> {1..(n::nat)}.          
 x a *\<^sub>R (\<Delta>_n b (y a)))
+ x a *\<^sub>R (\<Delta>_n (Suc n) (y a)))"
  by (simp add: sum.distrib)
hence "(\<Sum>b \<in> {1..(Suc n::nat)}. 
(\<Sum>a\<in> A.  x a *\<^sub>R (\<Delta>_n b (y a))))
= (\<Sum>a\<in> A. (\<Sum>b \<in> {1..(Suc n::nat)}.          
 x a *\<^sub>R (\<Delta>_n b (y a))))"
  by simp
  then show ?case
    by blast
qed

lemma expectation_Phi_le_minus_n_ep:
  assumes "i \<le> n" "i > 0" 
  shows "measure_pmf.expectation (w_pmf n) (\<Phi>_n n) \<le> - (n * \<epsilon>)"
proof - 
 have "measure_pmf.expectation (w_pmf n) (\<Phi>_n n) = 
    measure_pmf.expectation 
(bind_pmf 
(w_i_pmf n) 
(\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x))) 
(\<Phi>_n n)"
    using w_i_pmf_def w_pmf_def 
    by (simp add: map_pmf_def) 
  also have "... = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
measure_pmf.expectation ((\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x)) a) (\<Phi>_n n))"
    apply -
    apply (rule pmf_expectation_bind)
    using finite_False_func_set apply auto[1]
    apply simp
    by (smt Collect_mono_iff finite_lessThan set_Pi_pmf_subset set_pmf_eq w_i_pmf_def)
  also have "... = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R  
              (\<Phi>_n n (map a [0..<n])))"
    using expectation_return_pmf
    by (simp add: \<Delta>_n_def) 
  finally have st0:"measure_pmf.expectation (w_pmf n) (\<Phi>_n n) = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R  
              (\<Phi>_n n (map a [0..<n]) - \<Phi>_n 0 (map a [0..<n])))"
    by (simp add: \<Phi>_n_0) 
  have "\<forall>n a. 
(\<Sum>b \<in> {1..(n::nat)}. (\<lambda>x. \<Phi>_n x (map a [0..<n])) b - (\<lambda>x. \<Phi>_n x (map a [0..<n])) (b-1))
= ((\<lambda>x. \<Phi>_n x (map a [0..<n])) n - (\<lambda>x. \<Phi>_n x (map a [0..<n])) 0)"
    using sum_delta_\<Phi>
    by simp
  hence st2:"measure_pmf.expectation (w_pmf n) (\<Phi>_n n) = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R           
(\<Sum>b \<in> {1..(n::nat)}. \<Phi>_n b (map a [0..<n]) - \<Phi>_n (b-1) (map a [0..<n])))"
  using st0
  by auto 
  have "\<forall>a. pmf (w_i_pmf n) a *\<^sub>R           
(\<Sum>b \<in> {1..(n::nat)}. \<Phi>_n b (map a [0..<n]) - \<Phi>_n (b-1) (map a [0..<n]))
=(\<Sum>b \<in> {1..(n::nat)}. pmf (w_i_pmf n) a *\<^sub>R (\<Phi>_n b (map a [0..<n]) - \<Phi>_n (b-1) (map a [0..<n])))"
    using sum_distrib_left
    using scaleR_right.sum by blast 
  hence "measure_pmf.expectation (w_pmf n) (\<Phi>_n n) = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}.           
(\<Sum>b \<in> {1..(n::nat)}. pmf (w_i_pmf n) a *\<^sub>R (\<Phi>_n b (map a [0..<n]) - \<Phi>_n (b-1) (map a [0..<n]))))"
    using st2
    by simp
 hence st3:"measure_pmf.expectation (w_pmf n) (\<Phi>_n n) = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}.           
(\<Sum>b \<in> {1..(n::nat)}. pmf (w_i_pmf n) a *\<^sub>R (\<Delta>_n b (map a [0..<n]))))"
    using \<Delta>_n_def
    by simp
  have "
\<forall>x A y. (\<Sum>b \<in> {1..(n::nat)}. 
(\<Sum>a\<in>A.           
 x a *\<^sub>R (\<Delta>_n b (y a))))
=
(\<Sum>a\<in>A.
(\<Sum>b \<in> {1..(n::nat)}. x a *\<^sub>R (\<Delta>_n b (y a))))"
    using swap_sum
    by blast 
    hence  "(\<Sum>b \<in> {1..(n::nat)}. 
(\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}.           
 (pmf (w_i_pmf n)) a *\<^sub>R (\<Delta>_n b (map a [0..<n]))))
=
(\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}.
(\<Sum>b \<in> {1..(n::nat)}. (pmf (w_i_pmf n)) a *\<^sub>R (\<Delta>_n b (map a [0..<n]))))"
      by force
  hence st4:"measure_pmf.expectation (w_pmf n) (\<Phi>_n n) = (\<Sum>b \<in> {1..(n::nat)}. 
(\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}.           
 pmf (w_i_pmf n) a *\<^sub>R (\<Delta>_n b (map a [0..<n]))))"
    using st3
    by linarith
    have simplified:"\<And>i. measure_pmf.expectation (w_pmf n) (\<Delta>_n i) = 
(\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
        (\<Delta>_n i (map a [0..<n])))"
    proof -
    fix i
    have "measure_pmf.expectation (w_pmf n) (\<Delta>_n i) =
    measure_pmf.expectation
(bind_pmf 
(w_i_pmf n) 
(\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x))) 
(\<Delta>_n i)"
    using w_i_pmf_def w_pmf_def 
    by (simp add: map_pmf_def) 
 also have "... = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
measure_pmf.expectation ((\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x)) a) (\<Delta>_n i))"
    apply -
    apply (rule pmf_expectation_bind)
    using finite_False_func_set apply auto[1]
    apply simp
    by (smt Collect_mono_iff finite_lessThan set_Pi_pmf_subset set_pmf_eq w_i_pmf_def)
  finally have "measure_pmf.expectation (w_pmf n) (\<Delta>_n i) = 
(\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
        (\<Delta>_n i (map a [0..<n])))"
   using expectation_return_pmf
   by simp
  thus "measure_pmf.expectation (w_pmf n) (\<Delta>_n i) = 
(\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
        (\<Delta>_n i (map a [0..<n])))" by simp
qed
  hence "measure_pmf.expectation (w_pmf n) (\<Phi>_n n) = (\<Sum>b \<in> {1..(n::nat)}. 
        measure_pmf.expectation (w_pmf n) (\<Delta>_n b))"
    using st4 by auto
  hence "measure_pmf.expectation (w_pmf n) (\<Phi>_n n) \<le> (\<Sum>b \<in> {1..(n::nat)}. -\<epsilon>)"
    by (smt atLeastAtMost_iff expectation_Delta_i_le_minus_ep le_numeral_extra(2) not_gr_zero sum_mono)
  hence "measure_pmf.expectation (w_pmf n) (\<Phi>_n n) \<le> of_nat (card {1..(n::nat)}) * (- \<epsilon>)"
    using sum_constant by simp
  thus ?thesis by simp
qed

lemma sum_Del_Phi:
  "(\<Sum>i \<in> {1..(n::nat)}. \<Delta>_n' i w) = \<Phi>_n' n w"
proof (induction n)
  case 0
  have "\<Phi>_n' 0 w = 0"
    by (simp add: \<Phi>_n'_def)
  then show ?case
    by simp 
next
  case (Suc n)
  hence "(\<Sum>i \<in> {1..(n::nat)}. \<Delta>_n' i w) = \<Phi>_n' n w"
    by blast
  have "{1..(Suc n::nat)} =  {1..(n::nat)} \<union> {Suc n}"
    by (simp add: atLeastAtMostSuc_conv)
  hence "(\<Sum>i \<in> {1..(Suc n::nat)}. \<Delta>_n' i w) =
(\<Sum>i \<in> {1..(n::nat)} \<union> {Suc n}. \<Delta>_n' i w) "
    by auto
hence "(\<Sum>i \<in> {1..(Suc n::nat)}. \<Delta>_n' i w) =
(\<Sum>i \<in> {1..(n::nat)}. \<Delta>_n' i w) + \<Delta>_n' (Suc n) w "
  by auto
  hence "... = \<Phi>_n' n w + \<Phi>_n (Suc n) w - \<Phi>_n n w + \<epsilon>"
    using Suc.IH \<Delta>_n'_def by auto
 hence "... = (\<Phi>_n n w + n * \<epsilon>) +  \<Phi>_n (Suc n) w - \<Phi>_n n w + \<epsilon>"
   by (simp add: \<Phi>_n'_def \<Phi>_n_def)
   hence "... =  n * \<epsilon> +  \<Phi>_n (Suc n) w + \<epsilon>"
     by (simp add: \<Phi>_n'_def \<Phi>_n_def)
   hence "... =  \<Phi>_n (Suc n) w + n* \<epsilon> + \<epsilon>"
     by simp
hence "... =  \<Phi>_n (Suc n) w + (n* \<epsilon> + \<epsilon>) "
  by simp
  hence "... =  \<Phi>_n (Suc n) w + (n* \<epsilon> + 1*\<epsilon>) "
    by simp
   hence "... = \<Phi>_n (Suc n) w + (Suc n)* \<epsilon> "
    by (metis Suc_eq_plus1 add.commute comm_semiring_class.distrib of_nat_Suc)
  then show ?case
    using \<Phi>_n'_def \<Phi>_n_def \<open>(\<Sum>i = 1..n. local.\<Delta>_n' i w) + local.\<Delta>_n' (Suc n) w = local.\<Phi>_n' n w + local.\<Phi>_n (Suc n) w - local.\<Phi>_n n w + \<epsilon>\<close> by auto
qed

definition \<Delta>_n_pmf' where 
  "\<Delta>_n_pmf' n = map_pmf (\<lambda>x. \<Delta>_n' n x) (w_pmf n)"

definition \<Phi>_n_pmf' where 
  "\<Phi>_n_pmf' n = map_pmf (\<lambda>x. \<Phi>_n' n x) (w_pmf n)"

lemma set_pmf_w_pmf_size:
"set_pmf (w_pmf n) \<subseteq> {l::(bool list). size l = n}"
proof -
  have "set_pmf (w_pmf n) =  (\<lambda>f. map f [0..<n]) ` set_pmf (w_i_pmf n)"
    by (simp add: w_pmf_def)
  have "\<And>a.  size ((\<lambda>f. map f [0..<n]) a) = n "
    by simp
  hence "\<And>a. a \<in> set_pmf (w_pmf n) \<longrightarrow> size a = n"
    using \<open>set_pmf (local.w_pmf n) = (\<lambda>f. map f [0..<n]) ` set_pmf (local.w_i_pmf n)\<close> by auto
  thus ?thesis
    by auto
qed

lemma al_gt_0:"\<alpha> > 0"
proof - 
  have "1 + \<epsilon> > 0"
    using ep_gt_0 by linarith
  have "2 * \<epsilon> > 0"
    by (simp add: ep_gt_0)
  hence "(1+\<epsilon>) / (2 * \<epsilon>) > 0"
    by auto

  thus ?thesis
    by (simp add: \<alpha>)
qed

lemma al_gt_1:"\<alpha> \<ge> 1"
proof - 
  have "1 + \<epsilon> > 0"
    using ep_gt_0 by linarith
  have "2 * \<epsilon> > 0"
    by (simp add: ep_gt_0)
  hence "(1+\<epsilon>) \<ge> (2 * \<epsilon>)"
    using ep_le_1 by auto
  hence "(1+\<epsilon>) / (2 * \<epsilon>) \<ge> 1"
    using ep_le_1 \<open>1 + \<epsilon> > 0\<close> \<open>2 * \<epsilon> > 0\<close>
    by simp
  thus ?thesis
    by (simp add: \<alpha>)
qed

lemma min_0_minus_1_bound:
"(min 0 (n - 1 :: int)) - min 0 n \<le> 1 \<and>(min 0 (n - 1 :: int)) - min 0 n \<ge> -1"
  by auto

lemma \<Delta>_n_bound:
  assumes "size l = n" "n > 0 "
  shows "\<Delta>_n n l \<le> 1 + \<alpha> \<and> \<Delta>_n n l \<ge> - (1 + \<alpha>)"
proof -
  obtain k where "n = Suc k"
    using assms(2) gr0_conv_Suc by auto
  hence "\<Delta>_n n l = 
\<Phi>_n (Suc k) l - \<Phi>_n k l"
    using \<Delta>_n_def by auto
  hence "... = 
 (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (drop (size l - (Suc k)) l))
-  (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (drop (size l - k) l))"
    by (simp add: \<open>local.\<Delta>_n n l = local.\<Phi>_n (Suc k) l - local.\<Phi>_n k l\<close> \<Phi>_n_def)
hence st:"\<Delta>_n n l  = 
 (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m l)
-  (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (tl l))"
  by (metis One_nat_def \<open>local.\<Delta>_n n l = local.\<Phi>_n (Suc k) l - local.\<Phi>_n k l\<close> \<open>n = Suc k\<close> add_diff_cancel_right' assms(1) diff_self_eq_0 drop0 drop_Suc plus_1_eq_Suc)
 
  thus ?thesis
  proof (cases "hd l")
    case True
    note t = this
    thm t
    hence "rev_m l = rev_m (True#(tl l))"
      by (metis (full_types) \<open>n = Suc k\<close> assms(1) length_Suc_conv list.collapse list.discI)

      hence "... =
(fst (rev_m (tl l)) + 1, snd (rev_m (tl l)) + 1)"
        by simp
      hence st1:"\<Delta>_n n l  = 
 (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) 
(fst (rev_m (tl l)) + 1, snd (rev_m (tl l)) + 1)
-  (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (tl l))"
        by (simp add: \<open>rev_m l = rev_m (True # tl l)\<close> st)
      hence "... = 
real_of_int (fst (rev_m (tl l)) + 1) + \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) + 1)))
-  (real_of_int (fst (rev_m (tl l))) + \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l))))))"
        by simp
hence "... = 
real_of_int (fst (rev_m (tl l)) + 1) + \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) + 1)))
-  real_of_int (fst (rev_m (tl l))) - \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)))))"
  by linarith
hence "... = 
real_of_int (fst (rev_m (tl l)) + 1) 
-  real_of_int (fst (rev_m (tl l))) 
+ \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) + 1)))
- \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)))))"
  by linarith
  hence "... = 1 +  \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) + 1)))
- \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)))))"
    by simp
  hence st2:"\<Delta>_n n l  = 1 +  \<alpha> *
((real_of_int (min 0 (snd (rev_m (tl l)) + 1)))
-  (real_of_int (min 0 (snd (rev_m (tl l))))))"
    using st1
    by (simp add: right_diff_distrib)
   then show ?thesis 
  proof -
have le1:"((real_of_int (min 0 (snd (rev_m (tl l)) + 1)))
-  (real_of_int (min 0 (snd (rev_m (tl l)))))) \<le> 1"
  proof (cases "snd (rev_m (tl l)) < -1")
    case True
    then show ?thesis
      by linarith
  next
    case False
    then show ?thesis
      by simp 
  qed
have ge0: "((real_of_int (min 0 (snd (rev_m (tl l)) + 1)))
-  (real_of_int (min 0 (snd (rev_m (tl l)))))) \<ge> 0"
  by simp
hence "1 +  \<alpha> *
((real_of_int (min 0 (snd (rev_m (tl l)) + 1)))
-  (real_of_int (min 0 (snd (rev_m (tl l))))))
\<ge> 1"
  using al_gt_0
  by auto
hence "1 +  \<alpha> *
((real_of_int (min 0 (snd (rev_m (tl l)) + 1)))
-  (real_of_int (min 0 (snd (rev_m (tl l))))))
 \<ge> - (1 + \<alpha>)"
  using al_gt_0 by linarith

  thus ?thesis using st2 le1 ge0
    by (simp add: le1 al_gt_0)
qed
next
  case False
  note f = this
  thm this
  then show ?thesis
  proof (cases "fst (rev_m (tl l)) > snd (rev_m (tl l)) \<and> snd (rev_m (tl l)) = 0")
    case True
    note tt = this
    thm this
    hence "rev_m l = (fst (rev_m (tl l)) - 1, 0)"
      by (metis (mono_tags, hide_lams) f less_numeral_extra(3) list.exhaust_sel list.sel(2) prod.inject rev_m.simps(1) rev_m.simps(3) surjective_pairing)
hence "\<Delta>_n n l  = 
 (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (fst (rev_m (tl l)) - 1, 0)
-  (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (tl l))"
  using st
  by simp
hence "... = 
  real_of_int (fst (rev_m (tl l)) - 1) + \<alpha> * (real_of_int (min 0 (0)))
-  (real_of_int (fst (rev_m (tl l))) + \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l))))))"
  by (simp add: True)
 hence "... = 
  real_of_int (fst (rev_m (tl l)) - 1) + \<alpha> * (real_of_int (min 0 (0)))
-  real_of_int (fst (rev_m (tl l))) - \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l))))) "
   by linarith
  hence "... = real_of_int (fst (rev_m (tl l)) - 1) 
-  real_of_int (fst (rev_m (tl l))) "
    by (simp add: True)
  thus ?thesis
    using \<open>local.\<Delta>_n n l = real_of_int (fst (fst (rev_m (tl l)) - 1, 0)) + \<alpha> * real_of_int (min 0 (snd (fst (rev_m (tl l)) - 1, 0))) - (real_of_int (fst (rev_m (tl l))) + \<alpha> * real_of_int (min 0 (snd (rev_m (tl l)))))\<close> al_gt_0 by auto
  next
    case False
    note ff = this
    thm this
    then show ?thesis 
    proof (cases "fst (rev_m (tl l)) = 0")
      case True
      have "rev_m l = rev_m (False#tl l)"
        by (metis Nitpick.size_list_simp(2) Zero_not_Suc \<open>n = Suc k\<close> assms(1) f list.exhaust_sel)
      hence "rev_m (False#tl l)  = (0, snd (rev_m (tl l)) - 1)"
        using f ff True
        by simp
      hence st1:"\<Delta>_n n l  =
        (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (0, snd (rev_m (tl l)) - 1)
      -  (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (tl l))"
        using st
        by (simp add: \<open>rev_m l = rev_m (False # tl l)\<close>)
hence "... = 
  real_of_int (0) + \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) - 1)))
-  (real_of_int (fst (rev_m (tl l))) + \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l))))))"
  by simp
 hence "... = 
  real_of_int (0) + \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) - 1)))
-  real_of_int (fst (rev_m (tl l))) - \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l))))) "
   by simp
  hence "... = \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) - 1))) - \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l))))) "
    using True by simp
hence "\<Delta>_n n l = \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) - 1)) -  real_of_int (min 0 (snd (rev_m (tl l))))) "
  using st1
  by (metis (no_types, hide_lams) True \<open>real_of_int (fst (0, snd (rev_m (tl l)) - 1)) + \<alpha> * real_of_int (min 0 (snd (0, snd (rev_m (tl l)) - 1))) - (real_of_int (fst (rev_m (tl l))) + \<alpha> * real_of_int (min 0 (snd (rev_m (tl l))))) = real_of_int 0 + \<alpha> * real_of_int (min 0 (snd (rev_m (tl l)) - 1)) - (real_of_int (fst (rev_m (tl l))) + \<alpha> * real_of_int (min 0 (snd (rev_m (tl l)))))\<close> add.left_neutral add_uminus_conv_diff distrib_left mult_minus_right of_int_0)
  thus ?thesis
  proof (cases "snd (rev_m (tl l)) < 1")
    case True
    hence "(real_of_int (min 0 (snd (rev_m (tl l)) - 1)) -  real_of_int (min 0 (snd (rev_m (tl l))))) = -1"
      by linarith
    hence "\<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) - 1)) -  real_of_int (min 0 (snd (rev_m (tl l))))) = - \<alpha>"
      by simp
  then show ?thesis 
    using al_gt_0 mult_cancel_left1 mult_minus_right of_int_1_le_iff of_int_diff of_int_le_1_iff
    using \<open>local.\<Delta>_n n l = \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) - 1)) - real_of_int (min 0 (snd (rev_m (tl l)))))\<close> by auto
  next
    case False
    hence "(real_of_int (min 0 (snd (rev_m (tl l)) - 1)) -  real_of_int (min 0 (snd (rev_m (tl l))))) =
            - real_of_int (min 0 (snd (rev_m (tl l))))"
      by auto
    hence "(real_of_int (min 0 (snd (rev_m (tl l)) - 1)) -  real_of_int (min 0 (snd (rev_m (tl l))))) \<le> 0
\<and> (real_of_int (min 0 (snd (rev_m (tl l)) - 1)) -  real_of_int (min 0 (snd (rev_m (tl l))))) \<ge> - 1"
      by auto
    hence "\<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) - 1)) -  real_of_int (min 0 (snd (rev_m (tl l))))) \<le> 0
\<and> \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) - 1)) -  real_of_int (min 0 (snd (rev_m (tl l))))) \<ge> - \<alpha>"
      by (metis False True int_one_le_iff_zero_less not_le rev_margin_le_rho_s)
    then show ?thesis
      using \<open>local.\<Delta>_n n l = \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) - 1)) - real_of_int (min 0 (snd (rev_m (tl l)))))\<close> by auto 
  qed 
    next
      case False
      hence "rev_m l = (fst (rev_m (tl l)) - 1, snd (rev_m (tl l)) - 1)"
        using ff f
        by (metis (full_types) fst_zero hd_Cons_tl list.sel(2) rev_m.simps(1) rev_m.simps(3) zero_prod_def)
      hence "\<Delta>_n n l =
(\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (fst (rev_m (tl l)) - 1, snd (rev_m (tl l)) - 1)
-  (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (tl l))"
        by (simp add: st)
hence "... =
(\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (fst (rev_m (tl l)) - 1, snd (rev_m (tl l)) - 1)
-  (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (tl l))"
  by (simp add: st)
hence "... =
real_of_int (fst (rev_m (tl l)) - 1) + \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) - 1))) 
-  real_of_int (fst (rev_m (tl l))) - \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)))))"
  by simp
hence "... =
real_of_int (fst (rev_m (tl l)) - 1)  
-  real_of_int (fst (rev_m (tl l))) 
 + \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) - 1)))
- \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)))))"
  by simp
hence "... = - 1
 + \<alpha> * ((real_of_int (min 0 (snd (rev_m (tl l)) - 1)))
- (real_of_int (min 0 (snd (rev_m (tl l))))))"
  by (simp add: right_diff_distrib)
hence "\<Delta>_n n l = - 1
 + \<alpha> * ((real_of_int (min 0 (snd (rev_m (tl l)) - 1)))
- (real_of_int (min 0 (snd (rev_m (tl l))))))"
  by (simp add: \<open>rev_m l = (fst (rev_m (tl l)) - 1, snd (rev_m (tl l)) - 1)\<close> st)

  thus ?thesis
  proof (cases "snd (rev_m (tl l)) < 1")
    case True
    note ttt = this
    thm this
    hence "((real_of_int (min 0 (snd (rev_m (tl l)) - 1)))
- (real_of_int (min 0 (snd (rev_m (tl l))))))
= (real_of_int (snd (rev_m (tl l)) - 1))
- (real_of_int (min 0 (snd (rev_m (tl l)))))"
      by auto 
    then show ?thesis proof (cases "snd (rev_m (tl l)) > 0")
      case True
hence "((real_of_int (min 0 (snd (rev_m (tl l)) - 1)))
- (real_of_int (min 0 (snd (rev_m (tl l))))))
= (real_of_int (snd (rev_m (tl l)) - 1))"
  using ttt by auto
hence "((real_of_int (min 0 (snd (rev_m (tl l)) - 1)))
- (real_of_int (min 0 (snd (rev_m (tl l))))))
< 0 \<and> ((real_of_int (min 0 (snd (rev_m (tl l)) - 1)))
- (real_of_int (min 0 (snd (rev_m (tl l))))))
> - 1"
  by (simp add: True ttt)
hence "\<alpha> * ((real_of_int (min 0 (snd (rev_m (tl l)) - 1)))
- (real_of_int (min 0 (snd (rev_m (tl l))))))
< 0 \<and> ((real_of_int (min 0 (snd (rev_m (tl l)) - 1)))
- \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l))))))
> - \<alpha>"
  using al_gt_1
  by auto
  then show ?thesis
    using True ttt by linarith 
next
  case False
hence "((real_of_int (min 0 (snd (rev_m (tl l)) - 1)))
- (real_of_int (min 0 (snd (rev_m (tl l))))))
= (real_of_int (snd (rev_m (tl l)) - 1)) - (real_of_int (snd (rev_m (tl l))))"
  using ttt by auto
hence "((real_of_int (min 0 (snd (rev_m (tl l)) - 1)))
- (real_of_int (min 0 (snd (rev_m (tl l))))))
= -1"
  using ttt by auto
  then show ?thesis
    using \<open>local.\<Delta>_n n l = - 1 + \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) - 1)) - real_of_int (min 0 (snd (rev_m (tl l)))))\<close> al_gt_1 by auto
qed
next
  case False
  hence "snd (rev_m (tl l)) > 0"
    by auto
  have "\<Delta>_n n l  = 
 (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m l)
-  (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (tl l))"
    by (simp add: st)
hence "\<Delta>_n n l  = 
 real_of_int (fst (rev_m (tl l)) - 1) 

- real_of_int (fst (rev_m (tl l)))
+ \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) - 1))) 
 - \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)))))"
  using \<open>local.\<Delta>_n n l = - 1 + \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) - 1)) - real_of_int (min 0 (snd (rev_m (tl l)))))\<close> \<open>real_of_int (fst (rev_m (tl l)) - 1) - real_of_int (fst (rev_m (tl l))) + \<alpha> * real_of_int (min 0 (snd (rev_m (tl l)) - 1)) - \<alpha> * real_of_int (min 0 (snd (rev_m (tl l)))) = - 1 + \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) - 1)) - real_of_int (min 0 (snd (rev_m (tl l)))))\<close> by linarith
  hence "\<Delta>_n n l  = 
 real_of_int (-1)
+ (\<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) - 1))) 
 - \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l))))))"
    by simp
hence "\<Delta>_n n l  = 
 real_of_int (-1)
+ (\<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) - 1)) - 
 real_of_int (min 0 (snd (rev_m (tl l))))))"
  using \<open>local.\<Delta>_n n l = - 1 + \<alpha> * (real_of_int (min 0 (snd (rev_m (tl l)) - 1)) - real_of_int (min 0 (snd (rev_m (tl l)))))\<close> by linarith
  thus ?thesis
    using \<open>0 < snd (rev_m (tl l))\<close> \<open>local.\<Delta>_n n l = real_of_int (- 1) + (\<alpha> * real_of_int (min 0 (snd (rev_m (tl l)) - 1)) - \<alpha> * real_of_int (min 0 (snd (rev_m (tl l)))))\<close> al_gt_0 by auto    
    qed
  qed
qed
qed
qed

lemma set_pmf_\<Delta>_n_pmf':
  assumes "a \<in> set_pmf (\<Delta>_n_pmf' n)"
  shows "a \<le> 1 + \<alpha> + \<epsilon> \<or> a \<ge> - (1 + \<alpha>) + \<epsilon>"
proof -
  have "set_pmf (\<Delta>_n_pmf' n) = (\<lambda>x. \<Delta>_n' n x) ` set_pmf (w_pmf n)"
    by (simp add: \<Delta>_n_pmf'_def)
  have "\<And>a. a \<in> set_pmf (w_pmf n) \<Longrightarrow> size a = n "
    using set_pmf_w_pmf_size by auto
  hence "\<And>a. a \<in>  set_pmf (w_pmf n)
\<Longrightarrow>
\<Delta>_n n a \<le> 1 + \<alpha> \<and> \<Delta>_n n a \<ge> -1 - \<alpha>"
    using \<Delta>_n_bound
    by (smt \<Delta>_n_def al_gt_1 not_gr_zero zero_diff)
  hence "\<And>a. a \<in>  set_pmf (w_pmf n)
\<Longrightarrow>
\<Delta>_n' n a \<le> 1 + \<alpha> + \<epsilon> \<and> \<Delta>_n' n a \<ge> -1 - \<alpha> + \<epsilon>"
    by (simp add: \<Delta>_n'_def \<Delta>_n_def)
  thus ?thesis
    using al_gt_0 by linarith
qed

lemma abs_\<Delta>_n_pmf'_bound:
  assumes "a \<in> set_pmf (\<Delta>_n_pmf' n)"
  shows "abs a \<le> 1 + \<alpha> + \<epsilon>"
proof -
  have "set_pmf (\<Delta>_n_pmf' n) = (\<lambda>x. \<Delta>_n' n x) ` set_pmf (w_pmf n)"
    by (simp add: \<Delta>_n_pmf'_def)
  have "\<And>a. a \<in> set_pmf (w_pmf n) \<Longrightarrow> size a = n "
    using set_pmf_w_pmf_size by auto
  hence "\<And>a. a \<in>  set_pmf (w_pmf n)
\<Longrightarrow>
\<Delta>_n n a \<le> 1 + \<alpha> \<and> \<Delta>_n n a \<ge> -1 - \<alpha>"
    using \<Delta>_n_bound
    by (smt \<Delta>_n_def al_gt_1 not_gr_zero zero_diff)
  hence "\<And>a. a \<in>  set_pmf (w_pmf n)
\<Longrightarrow>
\<Delta>_n' n a \<le> 1 + \<alpha> + \<epsilon> \<and> \<Delta>_n' n a \<ge> -1 - \<alpha> + \<epsilon>"
    by (simp add: \<Delta>_n'_def \<Delta>_n_def)
  thus ?thesis
    using al_gt_0
    using \<open>set_pmf (local.\<Delta>_n_pmf' n) = local.\<Delta>_n' n ` set_pmf (local.w_pmf n)\<close> assms ep_gt_0 by fastforce 
qed

lemma "\<P>(w in w_pmf n. forkable w) = \<P>(w in w_pmf n. rev_\<mu>_bar (rev w) = 0)"
proof -
  have "\<forall>w. forkable w = (\<exists>F. closed_fork F w \<and> 0 \<le> margin F w)"
    by (simp add: forkable_iff_non_neg_margin)
have "\<forall>w. margin_s w = Max ((\<lambda>f. margin f w) ` (closed_forks_set w))"
  by (simp add: margin_s_def)
  hence "\<forall>w. forkable w = (margin_s w = Max ((\<lambda>f. margin f w) ` (closed_forks_set w))
\<and> (\<exists>F. closed_fork F w \<and> 0 \<le> margin F w)) "
    by (simp add: \<open>\<forall>w. forkable w = (\<exists>F. closed_fork F w \<and> 0 \<le> margin F w)\<close>)
hence "\<forall>w. forkable w = (margin_s w = Max ((\<lambda>f. margin f w) ` (closed_forks_set w))
\<and> (\<exists>F. closed_fork F w \<and> 0 \<le> margin F w \<and> margin F w \<in> ((\<lambda>f. margin f w) ` (closed_forks_set w)))) "
  using closed_forks_set_def by auto
  hence "\<forall>w. forkable w = (margin_s w = Max ((\<lambda>f. margin f w) ` (closed_forks_set w))
\<and> (\<exists>F. closed_fork F w \<and> 0 \<le> margin F w \<and> margin F w \<le> Max ((\<lambda>f. margin f w) ` (closed_forks_set w)))) "
    by (metis Max_ge finite_set_of_margin non_neg_margin_imp_forkable)
   hence "\<forall>w. forkable w = (0 \<le> margin_s w)"
  using exist_closed_fork_for_margin margin_s_def by fastforce
  hence "\<forall>w. forkable w = (0 = min 0 (snd (m w)))"
    using m_def by auto
hence "\<forall>w. forkable w = (0 = min 0 (rev_\<mu> (rev w)))"
  by (simp add: m_def rev_m_m_rev rev_\<mu>_def)
  hence "\<forall>w. forkable w = (0 = rev_\<mu>_bar (rev w))"
    by (simp add: rev_\<mu>_bar_def)
  thus ?thesis
    by simp
qed

lemma \<mu>_bar_0_imp_\<Phi>_n_ge_0:
  assumes  "size w = n" "rev_\<mu>_bar w = 0"
  shows "\<Phi>_n n w \<ge> 0"
proof - 
  have "\<Phi>_n n w = (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (drop (size w - n) w))"
    by (simp add: \<Phi>_n_def)
  hence "\<Phi>_n n w = (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m w)"
  using assms
  by simp
  hence "\<Phi>_n n w = real_of_int (fst (rev_m w)) + \<alpha> * (real_of_int  (rev_\<mu>_bar w))"
    by (simp add: rev_\<mu>_bar_def rev_\<mu>_def)
  hence "\<Phi>_n n w = real_of_int (fst (rev_m w))"
    using assms
    by simp
  hence "\<Phi>_n n w = real_of_int (fst (m (rev w)))"
    using rev_m_m_rev
    by simp
  hence "... = real_of_int (rho_s (rev w))"
    using m_def
    by simp
  hence "... \<ge> 0"
    using rho_s_ge_0
    by simp
  thus ?thesis
    by (simp add: \<open>local.\<Phi>_n n w = real_of_int (fst (m (rev w)))\<close> \<open>real_of_int (fst (m (rev w))) = real_of_int (rho_s (rev w))\<close>)
qed

lemma "\<P>(w in w_pmf n. rev_\<mu>_bar (rev w) = 0) \<le> \<P>(w in w_pmf n. \<Phi>_n n (rev w) \<ge> 0)"
proof - 
  have 1: "{w \<in> UNIV. rev_\<mu>_bar (rev w) = 0} 
= {w \<in> set_pmf (w_pmf n). rev_\<mu>_bar (rev w) = 0} \<union>
{w \<in> UNIV - set_pmf (w_pmf n). rev_\<mu>_bar (rev w) = 0}"
    by blast
  have 2: "{w \<in> set_pmf (w_pmf n). rev_\<mu>_bar (rev w) = 0} \<inter>
{w \<in> UNIV - set_pmf (w_pmf n). rev_\<mu>_bar (rev w) = 0} = {}"
    by blast
  hence "measure_pmf.prob (local.w_pmf n) {w \<in> space (measure_pmf (local.w_pmf n)). rev_\<mu>_bar (rev w) = 0}
= measure_pmf.prob (local.w_pmf n) {w \<in> set_pmf (w_pmf n). rev_\<mu>_bar (rev w) = 0} +
measure_pmf.prob (local.w_pmf n) {w \<in> UNIV - set_pmf (w_pmf n). rev_\<mu>_bar (rev w) = 0}"
    using 1
    by (simp add: finite_measure.finite_measure_Union)
  have "\<forall>x. x \<in> {w \<in> UNIV - set_pmf (w_pmf n). rev_\<mu>_bar (rev w) = 0} \<longrightarrow> pmf (w_pmf n) x = 0"
    by (simp add: set_pmf_iff)
  hence "measure_pmf.prob (local.w_pmf n) {w \<in> UNIV - set_pmf (w_pmf n). rev_\<mu>_bar (rev w) = 0} = 0"
    by (smt "1" Collect_cong DiffE UnCI UnE \<open>measure_pmf.prob (local.w_pmf n) 
{w \<in> space (measure_pmf (local.w_pmf n)). rev_\<mu>_bar (rev w) = 0} = measure_pmf.prob (local.w_pmf n) 
{w \<in> set_pmf (local.w_pmf n). rev_\<mu>_bar (rev w) = 0} + measure_pmf.prob (local.w_pmf n) 
{w \<in> UNIV - set_pmf (local.w_pmf n). rev_\<mu>_bar (rev w) = 0}\<close> measure_prob_cong_0 space_measure_pmf)
  hence "measure_pmf.prob (local.w_pmf n) {w \<in> space (measure_pmf (local.w_pmf n)). rev_\<mu>_bar (rev w) = 0}
= measure_pmf.prob (local.w_pmf n) {w \<in> set_pmf (w_pmf n). rev_\<mu>_bar (rev w) = 0}"
    using \<open>measure_pmf.prob (local.w_pmf n) {w \<in> space (measure_pmf (local.w_pmf n)). rev_\<mu>_bar (rev w) = 0} 
= measure_pmf.prob (local.w_pmf n) {w \<in> set_pmf (local.w_pmf n). rev_\<mu>_bar (rev w) = 0} + 
measure_pmf.prob (local.w_pmf n) {w \<in> UNIV - set_pmf (local.w_pmf n). rev_\<mu>_bar (rev w) = 0}\<close> by linarith

 have 3: "{w \<in> UNIV.  \<Phi>_n n (rev w) \<ge> 0} 
= {w \<in> set_pmf (w_pmf n).  \<Phi>_n n (rev w) \<ge> 0} \<union>
{w \<in> UNIV - set_pmf (w_pmf n).  \<Phi>_n n (rev w) \<ge> 0}"
    by blast
  have 4: "{w \<in> set_pmf (w_pmf n).  \<Phi>_n n (rev w) \<ge> 0} \<inter>
{w \<in> UNIV - set_pmf (w_pmf n).  \<Phi>_n n (rev w) \<ge> 0} = {}"
    by blast
  hence "measure_pmf.prob (local.w_pmf n) {w \<in> space (measure_pmf (local.w_pmf n)).  \<Phi>_n n (rev w) \<ge> 0}
= measure_pmf.prob (local.w_pmf n) {w \<in> set_pmf (w_pmf n).  \<Phi>_n n (rev w) \<ge> 0} +
measure_pmf.prob (local.w_pmf n) {w \<in> UNIV - set_pmf (w_pmf n).  \<Phi>_n n (rev w) \<ge> 0}"
    using 1
    by (metis (mono_tags) "3" UNIV_I finite_measure.finite_measure_Union measure_pmf.finite_measure 
sets_measure_pmf space_measure_pmf)
  have "\<forall>x. x \<in> {w \<in> UNIV - set_pmf (w_pmf n).  \<Phi>_n n (rev w) \<ge> 0} \<longrightarrow> pmf (w_pmf n) x = 0"
    by (simp add: set_pmf_iff)
  hence "measure_pmf.prob (local.w_pmf n) {w \<in> UNIV - set_pmf (w_pmf n).  \<Phi>_n n (rev w) \<ge> 0} = 0"
    by (metis (mono_tags, lifting) DiffE \<open>\<forall>x. x \<in> {w \<in> UNIV - set_pmf (local.w_pmf n). 
rev_\<mu>_bar (rev w) = 0} \<longrightarrow> pmf (local.w_pmf n) x = 0\<close> \<open>measure_pmf.prob (local.w_pmf n) 
{w \<in> UNIV - set_pmf (local.w_pmf n). rev_\<mu>_bar (rev w) = 0} = 0\<close> measure_prob_cong_0)
  hence "measure_pmf.prob (local.w_pmf n) {w \<in> space (measure_pmf (local.w_pmf n)).  \<Phi>_n n (rev w) \<ge> 0}
         = measure_pmf.prob (local.w_pmf n) {w \<in> set_pmf (w_pmf n). \<Phi>_n n (rev w) \<ge> 0}"
    using \<open>measure_pmf.prob (local.w_pmf n) {w \<in> space (measure_pmf (local.w_pmf n)). 
0 \<le> local.\<Phi>_n n (rev w)} = measure_pmf.prob (local.w_pmf n) {w \<in> set_pmf (local.w_pmf n).
 0 \<le> local.\<Phi>_n n (rev w)} + measure_pmf.prob (local.w_pmf n) {w \<in> UNIV - set_pmf (local.w_pmf n). 
0 \<le> local.\<Phi>_n n (rev w)}\<close> by linarith
  have "\<forall>w . w \<in> set_pmf (w_pmf n) \<longrightarrow> size w = n"
    using set_pmf_w_pmf_size by auto
  hence 5:"{w \<in> set_pmf (w_pmf n). rev_\<mu>_bar (rev w) = 0} \<subseteq> {w \<in> set_pmf (w_pmf n). 0 \<le> local.\<Phi>_n n (rev w)}"
    using \<mu>_bar_0_imp_\<Phi>_n_ge_0
    by (simp add: subset_eq)
  hence 6:"{w \<in> set_pmf (w_pmf n). 0 \<le> local.\<Phi>_n n (rev w)} \<in> measure_pmf.events (w_pmf n) 
\<Longrightarrow> measure_pmf.prob (w_pmf n) {w \<in> set_pmf (w_pmf n). rev_\<mu>_bar (rev w) = 0}
 \<le> measure_pmf.prob (w_pmf n) {w \<in> set_pmf (w_pmf n). 0 \<le> local.\<Phi>_n n (rev w)}"
    using Probability_Mass_Function.measure_pmf.finite_measure_mono
    by auto
  have "{w \<in> set_pmf (w_pmf n). 0 \<le> local.\<Phi>_n n (rev w)} \<in> measure_pmf.events (w_pmf n)"
    by simp
    hence "measure_pmf.prob (w_pmf n) {w \<in> set_pmf (w_pmf n). rev_\<mu>_bar (rev w) = 0}
 \<le> measure_pmf.prob (w_pmf n) {w \<in> set_pmf (w_pmf n). 0 \<le> local.\<Phi>_n n (rev w)}"
      using 6 by auto
    thus ?thesis
      using \<open>measure_pmf.prob (local.w_pmf n) {w \<in> space (measure_pmf (local.w_pmf n)). 0 \<le> local.\<Phi>_n n (rev w)} = measure_pmf.prob (local.w_pmf n) {w \<in> set_pmf (local.w_pmf n). 0 \<le> local.\<Phi>_n n (rev w)}\<close> \<open>measure_pmf.prob (local.w_pmf n) {w \<in> space (measure_pmf (local.w_pmf n)). rev_\<mu>_bar (rev w) = 0} = measure_pmf.prob (local.w_pmf n) {w \<in> set_pmf (local.w_pmf n). rev_\<mu>_bar (rev w) = 0}\<close> by linarith
  qed


lemma "\<P>(w in w_pmf n. \<Phi>_n n (rev w) \<ge> 0) = \<P>(w in w_pmf n. \<Phi>_n' n (rev w) \<ge> (n::real) * \<epsilon>)"
proof - 
  have "\<forall>x. \<Phi>_n n x  = (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (drop (size x - n) x))
\<and> 
  \<Phi>_n' n x = (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (drop (size x - n) x)) + n * \<epsilon>"
    using \<Phi>_n'_def \<Phi>_n_def by blast
  hence "\<forall>x. \<Phi>_n' n x = \<Phi>_n n x + n * \<epsilon>"
    by simp
  hence "\<forall>x. \<Phi>_n n x \<ge> 0 \<longleftrightarrow> \<Phi>_n' n x \<ge> (n::real) * \<epsilon>"
    by simp
  thus ?thesis
    by simp
qed

definition cond_exp_pmf :: "'a pmf \<Rightarrow> ('a \<Rightarrow> real) \<Rightarrow> ('a \<Rightarrow> 'b) \<Rightarrow> real pmf" where
   "cond_exp_pmf p X Y =
    map_pmf (\<lambda>y. measure_pmf.expectation (cond_pmf p (Y -` {y})) X) (map_pmf Y p)"

definition cond_exp :: "'a pmf => ('a => real) => ('a => 'b) => 'b => real" where
   "cond_exp p X Y y = measure_pmf.expectation (cond_pmf p (Y -` {y})) X"

theorem Azuma_Hoeffding_Inequality:
  assumes 
"\<And>i x. x \<in> set_pmf P \<and> i < n \<Longrightarrow> 
cond_exp P (X (i+1)) 
(\<lambda>w. map (\<lambda>j. X j w) [0..<i])
((\<lambda>w. map (\<lambda>j. X j w) [0..<i]) x) \<le> X i x"
"\<And>i x. x \<in> set_pmf P \<and> i < n \<Longrightarrow> abs (X (i + 1) x - X i x) \<le> c "
shows "\<And>\<Lambda>. \<P>(x in P. X n x - X 0 x \<ge> \<Lambda>) = exp (- (\<Lambda> * \<Lambda>) / (2 * (n::real) * c))"
  sorry

lemma \<Phi>'_supermartingale_1:
  assumes "w \<in> set_pmf (w_pmf n)" "t<n" 
  shows "(cond_exp (w_pmf n) 
                   (\<Phi>_n' (t + 1))
                   (\<lambda>w. map (\<lambda>x. \<Phi>_n' (nat x) w) [0..<t])
                   (map (\<lambda>x. \<Phi>_n' (nat x) w) [0..<t])) =
         (cond_exp (w_pmf n) 
                   (\<Phi>_n' (t + 1))
                   (\<lambda>w. map (\<lambda>x. w ! nat x) [0..<t])
                   (map (\<lambda>x. w ! nat x) [0..<t]))"
  sorry

lemma \<Phi>'_supermartingale_2:
  assumes "w \<in> set_pmf (w_pmf n)" "t < n"
  shows "cond_exp (w_pmf n) 
                  (\<Phi>_n' (t + 1))
                  (\<lambda>w. map (\<lambda>x. (rev w) ! (nat x) ) [0..<t])
                  (map (\<lambda>x. (rev w) ! (nat x)) [0..<t])
         \<le> \<Phi>_n' t  w"
  sorry

lemma not_inj_on: "\<not> inj_on f A \<longleftrightarrow> \<not> (\<forall>x\<in>A. \<forall>y\<in>A. f x = f y \<longrightarrow> x = y)"
  by (meson inj_onD inj_onI)

lemma inj_on_chain_\<Phi>_n':
"inj_on (\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) {l. size l = t}"
  unfolding inj_on_def
proof clarify
  fix x y :: "bool list"
  assume assm:"t = length x"
    and "length y = length x"
    and " map (\<lambda>z. local.\<Phi>_n' (nat z) x) [1..int (length x)] = 
 map (\<lambda>x. local.\<Phi>_n' (nat x) y) [1..int (length x)]"

  show "x = y" 
  proof (rule ccontr)
    assume "\<not> x = y"
    hence nemp_finite:"{i. i < size x \<and> x ! i \<noteq> y ! i} \<noteq> {} \<and> finite {i. i < size x \<and> x ! i \<noteq> y ! i}"
      using \<open>length y = length x\<close> nth_equalityI by force
    then obtain j where j:"j = Max {i. i < size x \<and> x ! i \<noteq> y ! i}"
      by auto
    hence "j \<in> {i. i < size x \<and> x ! i \<noteq> y ! i}"
      using nemp_finite
      using Max_in by blast
    hence "j < size x"
      by simp
    have gt_j_eq:"\<forall>i . i > j \<and> i < size x \<longrightarrow> x ! i = y ! i"
    proof (rule ccontr)
      assume "\<not> (\<forall>i. j < i \<and> i < length x \<longrightarrow> x ! i = y ! i)"
      then obtain i where "i > j \<and> i < size x"  "x ! i \<noteq> y !i"
        by auto
      hence "i \<in> {i. i < size x \<and> x ! i \<noteq> y ! i} "
        using \<open>x ! i \<noteq> y ! i\<close> by blast
      thus "False"
        using Max_less_iff \<open>j < i \<and> i < length x\<close> \<open>j = Max {i. i < length x \<and> x ! i \<noteq> y ! i}\<close> nemp_finite by blast
    qed
    hence "drop (Suc j) x = drop (Suc j) y"
        by (simp add: \<open>length y = length x\<close> nth_equalityI)

      have "x =  take j x @ x ! j # drop (Suc j) x"
        by (simp add: Cons_nth_drop_Suc \<open>j < length x\<close>)
      hence tmp1:"\<forall>i . i > j \<and> i < size x \<longrightarrow> x ! i = (drop (Suc j) x) ! (i - Suc j)"
        using nth_append
        using Suc_diff_Suc by auto
      have "y =  take j y @ y ! j # drop (Suc j) y"
        by (simp add: \<open>j < length x\<close> \<open>length y = length x\<close> id_take_nth_drop)
      hence "\<forall>i . i > j \<and> i < size x \<longrightarrow> y ! i = (drop (Suc j) y) ! (i - Suc j)"
        using nth_append
        using Suc_diff_Suc
        using \<open>length y = length x\<close> by auto 
      hence tmp:"\<forall>i . i > j \<and> i < size x \<longrightarrow> (drop (Suc j) x) ! (i - Suc j) = (drop (Suc j) y) ! (i - Suc j)"
        using tmp1 gt_j_eq
        by blast
      have "\<forall>i . i < size x - Suc j \<longrightarrow> i + Suc j > j \<and> i + Suc j < size x"
        by (simp add: less_diff_conv)
      hence "\<forall>i . i < size x - Suc j \<longrightarrow> 
        (drop (Suc j) x) ! (i + Suc j - Suc j) = (drop (Suc j) y) ! (i + Suc j- Suc j)"
        using tmp by blast
      hence "\<forall>i . i < size x - Suc j \<longrightarrow>  (drop (Suc j) x)  ! i= (drop (Suc j) y) ! i"
        by simp

      have "(size x - Suc j + 1) \<le> size x"
        using \<open>j < length x\<close> \<open>length y = length x\<close>
        by linarith
      have tmp:" (size x - Suc j + 1) - 1 < size x"
        using \<open>length x - Suc j + 1 \<le> length x\<close> by linarith
      have tmp2:"size [1..int (length x)] = size x"
        by simp 
      hence "(map (\<lambda>z. local.\<Phi>_n' (nat z) x) [1..int (length x)]) ! ((size x - Suc j + 1) - 1) =
 (map (\<lambda>x. local.\<Phi>_n' (nat x) y) [1..int (length x)]) ! ((size x - Suc j + 1) - 1)"
        by (simp add: \<open>map (\<lambda>z. local.\<Phi>_n' (nat z) x) [1..int (length x)] = map (\<lambda>x. local.\<Phi>_n' (nat x) y) [1..int (length x)]\<close>)
      hence tmp3:"(\<lambda>z. local.\<Phi>_n' (nat z) x) ([1..int (length x)] ! ((size x - Suc j + 1) - 1))
= (\<lambda>x. local.\<Phi>_n' (nat x) y) ([1..int (length x)] ! ((size x - Suc j + 1) - 1))"
        using tmp tmp2 nth_map
        by simp
      hence "(\<lambda>z. local.\<Phi>_n' (nat z) x)  (int (size x - Suc j + 1))
= (\<lambda>x. local.\<Phi>_n' (nat x) y) (int (size x - Suc j + 1))"
      proof -
        have "1 + int ((size x - Suc j + 1) - 1) \<le> int (length x)"
          using tmp by auto
        thus ?thesis using  nth_upto tmp tmp2 tmp3
          by (metis Suc_eq_plus1 diff_Suc_1 of_nat_Suc)
      qed

      hence "\<Phi>_n' (nat (int (size x - Suc j + 1))) x = \<Phi>_n' (nat (int (size y - Suc j + 1))) y"
        using \<open>j < length x\<close> \<open>length y = length x\<close>
        by simp 
hence eq:"\<Phi>_n' (size x - Suc j + 1) x = \<Phi>_n' (size y - Suc j + 1) y"
        using \<open>j < length x\<close> \<open>length y = length x\<close>
        by (metis nat_int)
     
      have
"\<Phi>_n' (size x - Suc j + 1) x
= (\<lambda>p. real_of_int (fst p) + 
\<alpha> * (real_of_int (min 0 (snd p)))) 
(rev_m (drop (size x - (size x - Suc j + 1)) x)) + (size x - Suc j + 1) * \<epsilon>"
        using \<Phi>_n'_def by blast
      hence
"...
= (\<lambda>p. real_of_int (fst p) + 
\<alpha> * (real_of_int (min 0 (snd p)))) 
(rev_m (drop j x)) + (size x - Suc j + 1) * \<epsilon>"
        by (simp add: Suc_leI \<open>j < length x\<close>)
      hence comp1:"\<Phi>_n' (size x - Suc j + 1) x = (\<lambda>p. real_of_int (fst p) + 
\<alpha> * (real_of_int (min 0 (snd p)))) 
(rev_m ((x ! j) # (drop (Suc j) x))) + (size x - Suc j + 1) * \<epsilon>"
        using Cons_nth_drop_Suc \<open>j < length x\<close> \<open>local.\<Phi>_n' (length x - Suc j + 1) x = real_of_int (fst (rev_m (drop (length x - (length x - Suc j + 1)) x))) + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (length x - (length x - Suc j + 1)) x)))) + real (length x - Suc j + 1) * \<epsilon>\<close> by fastforce

      
      have
"\<Phi>_n' (size y - Suc j + 1) y
= (\<lambda>p. real_of_int (fst p) + 
\<alpha> * (real_of_int (min 0 (snd p)))) 
(rev_m (drop (size y - (size y - Suc j + 1)) y)) + (size y - Suc j + 1) * \<epsilon>"
        using \<Phi>_n'_def by blast
      hence
"...
= (\<lambda>p. real_of_int (fst p) + 
\<alpha> * (real_of_int (min 0 (snd p)))) 
(rev_m (drop j y)) + (size y - Suc j + 1) * \<epsilon>"
        by (simp add: Suc_leI \<open>j < length x\<close> \<open>length y = length x\<close>)

      hence "\<Phi>_n' (size y - Suc j + 1) y = (\<lambda>p. real_of_int (fst p) + 
\<alpha> * (real_of_int (min 0 (snd p)))) 
(rev_m ((y ! j) # (drop (Suc j) y))) + (size y - Suc j + 1) * \<epsilon>"
        using \<open>j < length x\<close> \<open>length y = length x\<close>
        by (metis Cons_nth_drop_Suc \<open>local.\<Phi>_n' (length y - Suc j + 1) y = real_of_int (fst (rev_m (drop (length y - (length y - Suc j + 1)) y))) + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (length y - (length y - Suc j + 1)) y)))) + real (length y - Suc j + 1) * \<epsilon>\<close>)
      hence "
(\<lambda>p. real_of_int (fst p) + 
\<alpha> * (real_of_int (min 0 (snd p)))) 
(rev_m ((y ! j) # (drop (Suc j) y))) + (size y - Suc j + 1) * \<epsilon>
= 
(\<lambda>p. real_of_int (fst p) + 
\<alpha> * (real_of_int (min 0 (snd p)))) 
(rev_m ((x ! j) # (drop (Suc j) x))) + (size x - Suc j + 1) * \<epsilon>"
        using comp1 eq
        by linarith
      hence "(\<lambda>p. real_of_int (fst p) + 
\<alpha> * (real_of_int (min 0 (snd p)))) 
(rev_m ((y ! j) # (drop (Suc j) y))) 
= 
(\<lambda>p. real_of_int (fst p) + 
\<alpha> * (real_of_int (min 0 (snd p)))) 
(rev_m ((x ! j) # (drop (Suc j) x))) "
        by (simp add: \<open>length y = length x\<close>)
      hence "(\<lambda>p. real_of_int (fst p) + 
\<alpha> * (real_of_int (min 0 (snd p)))) 
(rev_m (True # (drop (Suc j) y))) 
= 
(\<lambda>p. real_of_int (fst p) + 
\<alpha> * (real_of_int (min 0 (snd p)))) 
(rev_m (False # (drop (Suc j) x))) "
      proof (cases "x ! j")
        case True
        hence "\<not> y ! j"
          using \<open>j \<in> {i. i < length x \<and> x ! i \<noteq> y ! i}\<close> by blast
        then show ?thesis
          using True \<open>drop (Suc j) x = drop (Suc j) y\<close> \<open>real_of_int (fst (rev_m (y ! j # drop (Suc j) y))) + \<alpha> * real_of_int (min 0 (snd (rev_m (y ! j # drop (Suc j) y)))) = real_of_int (fst (rev_m (x ! j # drop (Suc j) x))) + \<alpha> * real_of_int (min 0 (snd (rev_m (x ! j # drop (Suc j) x))))\<close> by auto 
next
  case False
 hence "y ! j"
          using \<open>j \<in> {i. i < length x \<and> x ! i \<noteq> y ! i}\<close> by blast
        then show ?thesis
          using False \<open>real_of_int (fst (rev_m (y ! j # drop (Suc j) y))) + \<alpha> * real_of_int (min 0 (snd (rev_m (y ! j # drop (Suc j) y)))) = real_of_int (fst (rev_m (x ! j # drop (Suc j) x))) + \<alpha> * real_of_int (min 0 (snd (rev_m (x ! j # drop (Suc j) x))))\<close> by auto
      qed
hence "(\<lambda>p. real_of_int (fst p) + 
\<alpha> * (real_of_int (min 0 (snd p)))) 
(rev_m (True # (drop (Suc j) x))) 
= 
(\<lambda>p. real_of_int (fst p) + 
\<alpha> * (real_of_int (min 0 (snd p)))) 
(rev_m (False # (drop (Suc j) x))) "
  by (simp add: \<open>drop (Suc j) x = drop (Suc j) y\<close>)
hence st:"(real_of_int (fst (rev_m (drop (Suc j) x)) + 1)) + 
\<alpha> * (real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) + 1))) 
= 
(\<lambda>p. real_of_int (fst p) + 
\<alpha> * (real_of_int (min 0 (snd p)))) 
(rev_m (False # (drop (Suc j) x))) "
  by simp

  then show "False"
      proof (cases "fst (rev_m (drop (Suc j) x)) > snd (rev_m (drop (Suc j) x)) \<and> snd (rev_m (drop (Suc j) x)) = 0")
        case True
        hence "(real_of_int (fst (rev_m (drop (Suc j) x)) + 1)) - real_of_int (fst (rev_m (drop (Suc j) x)) - 1) = 0"
  using st
  by (simp add: True) 

  then show ?thesis
    by linarith
      next
        case False
        note f = this
        thm f
        then show ?thesis 
        proof (cases "fst (rev_m (drop (Suc j) x)) = 0")
          case True
 hence 
 "(real_of_int 1) + 
\<alpha> * (real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) + 1))) 
= 
\<alpha> * (real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) - 1)))"
   using st

  by (simp add: f True) 
hence 
 "(real_of_int 1) 
= 
\<alpha> * (real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) - 1))) - 
\<alpha> * (real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) + 1)))"
  by auto
hence 
 "(real_of_int 1) 
= 
\<alpha> * (real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) - 1))) - 
real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) + 1))"
  by (smt al_gt_0 mult_less_cancel_left_pos of_int_0 of_int_less_iff)
then show ?thesis
proof - 
  have "
(min 0 (snd (rev_m (drop (Suc j) x)) - 1)) <  
(min 0 (snd (rev_m (drop (Suc j) x)) + 1))"
    by (smt True rev_margin_le_rho_s)
  hence " (real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) - 1))) - 
real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) + 1)) < 0"
    by simp
          then show ?thesis
            by (smt \<open>real_of_int 1 + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) + 1)) = \<alpha> * real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) - 1))\<close> al_gt_0 mult_less_cancel_left_pos of_int_1) 
        qed
        next
          case False
hence "(real_of_int (fst (rev_m (drop (Suc j) x)) + 1)) + 
\<alpha> * (real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) + 1))) 
= 
 real_of_int (fst (rev_m (drop (Suc j) x)) - 1) + 
\<alpha> * (real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) - 1))) "
  using st by auto
  hence "(real_of_int (2))
= 
\<alpha> * (real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) - 1))) -
\<alpha> * (real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) + 1))) "
  using st by auto
  hence "(real_of_int (2))
= 
\<alpha> * ( (real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) - 1))) -
 (real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) + 1)))) "
    by (simp add: right_diff_distrib)
  then show ?thesis 
  proof - 
    have "(min 0 (snd (rev_m (drop (Suc j) x)) - 1))
\<le> (min 0 (snd (rev_m (drop (Suc j) x)) + 1))"
      by simp
    hence "( (real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) - 1))) -
 (real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) + 1))))  \<le> 0"
      by simp
    thus ?thesis
      by (smt \<open>real_of_int 2 = \<alpha> * real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) - 1)) - \<alpha> * real_of_int (min 0 (snd (rev_m (drop (Suc j) x)) + 1))\<close> al_gt_0 mult_less_cancel_left_pos of_int_1 of_int_diff)
        qed

      qed
    qed
  qed
qed

lemma map_nth_Suc:
  assumes "i < t"
  shows "[1..int t] ! i = Suc i"
  using assms nth_upto
  by auto

lemma drop_chain_\<Phi>_n':
  assumes "size W \<ge> t"
  shows "(\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) W =
(\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) (drop (size W - t) W)"
proof - 
  have "size ((\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) W) = t
\<and> size ((\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) (drop (size W - t) W)) = t"
    by simp
  have "\<And>i. i < t \<Longrightarrow> ((\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) W) ! i 
= ((\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) (drop (size W - t) W)) ! i"
  proof -
    fix i 
    assume "i < t"
    hence" ((\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) W) ! i 
=   local.\<Phi>_n' (nat ([1..int t] ! i)) W "
      by simp
    hence "((\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) W) ! i 
=   (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (drop (size W - (nat ([1..int t] ! i))) W)) 
+ (nat ([1..int t] ! i)) * \<epsilon>"
      by (simp add: \<Phi>_n'_def)
hence "((\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) W) ! i 
=   (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) 
(rev_m (drop (size W - (Suc i)) W))
+ (nat (Suc i)) * \<epsilon>"
  using nth_upto
  by (metis Suc_leI \<open>i < t\<close> nat_int of_nat_Suc of_nat_le_iff)

hence "((\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) W) ! i 
=   (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) 
(rev_m (drop (size W - t + t - (Suc i)) W))
+ (nat (Suc i)) * \<epsilon>"
  by (simp add: assms)
hence "((\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) W) ! i 
=   (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) 
(rev_m (drop (t - (Suc i)) (drop (size W - t) W)))
+ (nat (Suc i)) * \<epsilon>"
  by (smt Nat.add_diff_assoc2 Suc_leI \<open>i < t\<close> add.commute drop_drop)
hence "((\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) W) ! i 
=   (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) 
(rev_m (drop (size (drop (size W - t) W) - (Suc i)) (drop (size W - t) W)))
+ (nat (Suc i)) * \<epsilon>"
  by (simp add: assms)
hence "((\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) W) ! i 
=   local.\<Phi>_n' (Suc i) (drop (size W - t) W)"
  using \<Phi>_n'_def nat_int by simp 
hence "((\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) W) ! i 
=  local.\<Phi>_n' (nat ([1..int t] ! i)) (drop (size W - t) W)"
using nth_upto
  by (metis Suc_leI \<open>i < t\<close> nat_int of_nat_Suc of_nat_le_iff)

  thus "((\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) W) ! i 
= ((\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) (drop (size W - t) W)) ! i"
   by (simp add: \<open>i < t\<close>)
qed
  thus ?thesis
    by (simp add: list_eq_iff_nth_eq)
qed

lemma nth_int_list:
  assumes "i < t"
  shows  "[1..int t] ! i = int (Suc i)"
  using map_nth_Suc
  by (simp add: assms)

lemma last_t_eq_chain_\<Phi>_n':
  assumes "size W = t" "size W' \<ge> t" "((\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) W) = 
(\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) W'"
  shows "drop (size W' - t) W' = W"
proof (rule ccontr)
  assume "\<not> drop (size W' - t) W' = W"
  then obtain W'' where "W'' =  drop (size W' - t) W'" "W'' \<noteq> W"
    by blast
  hence "(\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) W'
= (\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) W''"
    using assms(2) drop_chain_\<Phi>_n' by blast
  have "W'' \<in> {l. size l = t} \<and> W \<in> {l. size l = t}"
    by (simp add: \<open>W'' = drop (length W' - t) W'\<close> assms(1) assms(2))
  hence "W'' = W" 
    using inj_on_chain_\<Phi>_n'
  proof -
    have "\<exists>f. (f W''::real list) = f W \<and> inj_on f {bs. length bs = t}"
      by (metis \<open>\<And>t. inj_on (\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) {l. length l = t}\<close> \<open>map (\<lambda>x. local.\<Phi>_n' (nat x) W') [1..int t] = map (\<lambda>x. local.\<Phi>_n' (nat x) W'') [1..int t]\<close> assms(3))
    then show ?thesis
      using \<open>W'' \<in> {l. length l = t} \<and> W \<in> {l. length l = t}\<close> not_inj_on by auto
  qed
  thus "False"
    using \<open>W'' \<noteq> W\<close> by auto
qed

lemma Suc_t_reverse_img_chain_\<Phi>_n'_aux:
  assumes "size W \<ge> t+1" "x \<in> ((\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) -`
{(map (\<lambda>x. local.\<Phi>_n' (nat x) W) [1..int t])} \<inter> set_pmf (w_pmf (t + 1)))"
  shows "size x = t+1 \<and> tl x = drop (size W - t) W"
proof - 
  have "x \<in> set_pmf (w_pmf (t + 1))"
    using assms(2) by blast
  hence "size x = t+1"
    using set_pmf_w_pmf_size by auto
  hence "tl x = drop (Suc 0) x"
    by (simp add: drop_Suc)
  hence "tl x = drop (size x - t) x"
    by (simp add: \<open>length x = t + 1\<close>)
  hence tmp:"(\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) (tl x) = 
(map (\<lambda>x. local.\<Phi>_n' (nat x) W) [1..int t])"
    by (metis IntE \<open>length x = t + 1\<close> assms(2) drop_chain_\<Phi>_n' le_add1 vimage_singleton_eq)
  hence "drop (size W - t) W = tl x"
    using last_t_eq_chain_\<Phi>_n' assms
  proof -
  have f1: "\<forall>n na. (na::nat) + n - na = n"
    by (metis Nat.diff_diff_right add_leE diff_diff_cancel diff_self_eq_0 le_refl)
  have f2: "\<forall>n. t \<le> n \<or> \<not> length x \<le> n"
    by (metis \<open>length x = t + 1\<close> add_leE)
have f3: "length x - t = 1"
  using \<open>length x = t + 1\<close> by linarith
  have f4: "\<forall>bs n bsa. (drop (length bs - (length bsa - n)) bs = drop n bsa \<or> map (\<lambda>i. local.\<Phi>_n' (nat i) bs) [1..int (length bsa - n)] \<noteq> map (\<lambda>i. local.\<Phi>_n' (nat i) (drop n bsa)) [1..int (length bsa - n)]) \<or> \<not> length bsa - n \<le> length bs"
    by (metis last_t_eq_chain_\<Phi>_n' length_drop)
  have "t \<le> length W"
    using assms(1) by linarith
  then show ?thesis
    using f4 f3 f2 f1 by (metis Suc_eq_plus1 \<open>tl x = drop (Suc 0) x\<close> diff_diff_cancel diff_self_eq_0 le_refl tmp)
qed
  thus ?thesis
    by (simp add: \<open>length x = t + 1\<close>)
qed

lemma Suc_t_reverse_img_chain_\<Phi>_n':
 assumes "size W = t+1" "x \<in> ((\<lambda>w. map (\<lambda>x. local.\<Phi>_n' (nat x) w) [1..int t]) -`
{(map (\<lambda>x. local.\<Phi>_n' (nat x) W) [1..int t])} \<inter> set_pmf (w_pmf (t + 1)))"
 shows "x = W \<or> x = (\<not> hd W) # (tl W)"
proof - 
  have "size x = t+1 \<and> tl x = drop (size W - t) W"
  using Suc_t_reverse_img_chain_\<Phi>_n'_aux assms
  by (metis order_refl)
  hence "tl x = drop (Suc 0) W"
    by (simp add: assms(1))
  hence "tl x = tl W"
    by (simp add: drop_Suc)
  hence "hd x # tl x = hd x # tl W"
    by simp
  hence "x = True # tl W \<or> x = False # tl W"
    by (metis (full_types) Suc_eq_plus1 Zero_not_Suc \<open>length x = t + 1 \<and> tl x = drop (length W - t) W\<close> length_0_conv list.exhaust_sel)
  thus ?thesis
    by (metis (full_types) \<open>length x = t + 1 \<and> tl x = drop (length W - t) W\<close> assms(1) length_greater_0_conv list.exhaust_sel)
qed

lemma "\<P>(x in (M::'a pmf). P x) = measure_pmf.prob M {x \<in> set_pmf M. P x}"
  by (smt Collect_cong Collect_mem_eq Int_iff inf_top.left_neutral measure_Int_set_pmf mem_Collect_eq space_measure_pmf)

lemma fork_empty_imp_trivial:
  assumes "forks F []"
  shows "F = Tree 0 []"
proof (rule ccontr)
  assume "F \<noteq> Tree 0 []"
  then obtain fs l where "F = Tree l fs" "l \<noteq> 0 \<or> fs \<noteq> []"
    by (metis rtree.exhaust)
  thus False
  proof (cases "l = 0")
    case True
    hence "size fs >0"
      using \<open>l \<noteq> 0 \<or> fs \<noteq> []\<close> by blast
    then obtain f where "f \<in> set fs"
      using nth_mem by blast
    have "strictly_inc (Tree l fs)"
      using assms
      using \<open>F = Tree l fs\<close> fork_F forks_def by blast
    hence "\<forall>f \<in> set fs. label f > l"
      using strictly_inc.cases by blast
    hence "label f > 0"
      using True \<open>f \<in> set fs\<close> by blast
    have "limit F 0"
      using assms
      by (simp add: fork_F forks_def)
    hence "\<And>f. f \<in> set fs \<Longrightarrow> limit f 0"
      using \<open>F = Tree l fs\<close> limit.cases by auto
    have "\<not> limit f 0"
      by (metis \<open>0 < label f\<close> leD limit.cases rtree.sel(1))
    then show ?thesis
      using \<open>\<And>fa. fa \<in> set fs \<Longrightarrow> limit fa 0\<close> \<open>f \<in> set fs\<close> by blast 
  next
    case False
    then show ?thesis
      using forks_def assms forks_set_def
      by (simp add: \<open>F = Tree l fs\<close> fork_F)
  qed
qed

lemma k_cp_violation_upperbound_1:
  assumes "k \<le> L" 
    shows "\<P>(x in w_pmf L. \<not> k_cp k x) \<le>
\<P>(x in w_pmf L. 
\<exists>a b. a \<in> {1..L} \<and> b \<in> {0..L} \<and> a + k - 1 \<le> b \<and> forkable (drop (a-1) (take b x)))"
proof (cases "L")
  case 0
  hence "k = 0"
    using assms by blast
  have "k_cp 0 [] = (\<forall>F\<in>forks_set []. \<forall>t1 t2. viable F [] t1 \<and> viable F [] t2 \<longrightarrow> div_tines F t1 t2 \<le> 0)"
    by (simp add: k_cp_def)
  have "\<forall>F\<in>forks_set []. F = Tree 0 []"
    using fork_empty_imp_trivial forks_set_def by blast
  hence "k_cp 0 [] = ( \<forall>t1 t2. viable (Tree 0 []) [] t1 \<and> viable (Tree 0 []) [] t2 \<longrightarrow> div_tines (Tree 0 []) t1 t2 \<le> 0)"
    by (metis \<open>k_cp 0 [] = (\<forall>F\<in>forks_set []. \<forall>t1 t2. viable F [] t1 \<and> viable F [] t2 \<longrightarrow> div_tines F t1 t2 \<le> 0)\<close> adversarial_strings_contain_trivial_tree_fork div_tines_upperbound height_limit_tine_length_tree limit_tine_length_tree.simps(1) list.distinct(1) list.set_cases min_0L)
  hence "k_cp 0 []"
    by (metis \<open>\<forall>F\<in>forks_set []. F = Tree 0 []\<close> div_tines_upperbound forks_set_def height_limit_tine_length_tree k_cp_def limit_tine_length_tree.simps(1) mem_Collect_eq min_0L)
  hence "\<forall>x. x \<in> set_pmf (w_pmf L) \<longrightarrow> k_cp k []"
    by (simp add: \<open>k = 0\<close>)
  hence "{x \<in> set_pmf (w_pmf L). \<not> k_cp k x} = {}"
    by (simp add: "0" w_pmf_def)
  hence "\<P>(x in w_pmf L. \<not> k_cp k x) = 0"
    by (metis (no_types, lifting) DiffE empty_iff measure_empty measure_prob_cong_0 mem_Collect_eq set_pmf_iff)
  then show ?thesis
    by simp 
next
  case (Suc nat)
  hence "L > 0"
    by simp
  have "
\<forall>w.
length w = L \<longrightarrow>
 (\<exists>w' i j. forkable w' \<and> i \<le> j \<and> j \<le> L \<and> drop i (take j w) = w' \<and> div_s w \<le> length w')"
    using exist_forkable_substring_length_ge_div by blast
  hence imp:"\<And>w. w \<in> {(w::bool list). size w = L} \<and> (\<not> k_cp k w) \<Longrightarrow> 
(\<exists>a b. a \<in> {1..L} \<and> b \<in> {0..L} \<and> a + k - 1 \<le> b \<and> forkable (drop (a-1) (take b w)))"
  proof - 
    fix w assume "w \<in> {(w::bool list). size w = L} \<and> (\<not> k_cp k w)"
    hence "(\<exists>F \<in> forks_set w. \<exists>t1 t2. viable F w t1 \<and> viable F w t2 \<and> \<not> div_tines F t1 t2 \<le> k)"
      using k_cp_def by blast
    then obtain F t1 t2 where 
"F \<in> forks_set w" "viable F w t1" "viable F w t2" "\<not> div_tines F t1 t2 \<le> k"
      by blast
    hence "div_tines F t1 t2 > k"
      by simp
    hence "div_f F w > k"
      using  div_tines_le_div_f
      by (meson \<open>\<not> div_tines F t1 t2 \<le> k\<close> \<open>viable F w t1\<close> \<open>viable F w t2\<close> le_trans not_le_imp_less viable.simps)
    hence "div_s w > k"
      using div_s_ge_div_f
      by (meson \<open>viable F w t1\<close> dual_order.trans leD not_le_imp_less viable.cases)
    hence "\<exists>w' i j. forkable w' \<and> i \<le> j \<and> j \<le> L \<and> drop i (take j w) = w' \<and> k \<le> length w'"
      using \<open>w \<in> {w. length w = L} \<and> \<not> k_cp k w\<close> exist_forkable_substring_length_ge_div by fastforce
    hence  "\<exists>i j. forkable (drop i (take j w)) \<and> i \<le> j \<and> j \<le> L \<and> k \<le> length (drop i (take j w))"
      by blast
    then obtain i j where ij:"forkable (drop i (take j w))"  "i \<le> j"  "j \<le> L" "k \<le> length (drop i (take j w))"
      by blast
    hence "j = 0 \<longrightarrow> k = 0"
      by auto
    hence "j = 0 \<longrightarrow> k = 0 \<and> i = 0"
      using \<open>i \<le> j\<close> by blast
    hence "k = 0 \<longrightarrow> (\<exists>a b. a =1 \<and> b =0 \<and> a + k - 1 \<le> b \<and> forkable (drop (a- 1) (take b w)))"
      by (metis add.right_neutral cancel_comm_monoid_add_class.diff_cancel drop_Nil exist_forkable_substring_length_ge_div take0 take_Nil zero_le)
     hence case0:"k = 0 \<longrightarrow> (\<exists>a b. a \<in> {1..L} \<and> b \<in> {0..L}  \<and> a + k - 1 \<le> b \<and> forkable (drop (a- 1) (take b w)))"
       using \<open>L > 0\<close> by fastforce
     have "size (take j w) = j"
       using ij
       using \<open>w \<in> {w. length w = L} \<and> \<not> k_cp k w\<close> by auto
     hence "size (drop i (take j w)) = j - i"
       by simp
     hence "j-i \<ge> k"
       using ij
       by linarith
     hence "j \<ge> k + i"
       using Nat.le_diff_conv2 ij(2) by blast
     hence tmp:"k> 0 \<longrightarrow>
(\<exists>a b. a = i + 1 \<and> b = j  \<and> a + k - 1 \<le> b \<and> forkable (drop (a- 1) (take b w)))"
       using ij
       by simp
     have "k > 0 \<longrightarrow> j > i"
       using \<open>k \<le> j - i\<close> by linarith
     hence "k >0 \<longrightarrow> i + 1 \<le> L"
       using ij(3) by linarith

     hence casepos:"k > 0 \<longrightarrow> 
(\<exists>a b. a \<in> {1..L} \<and> b \<in> {0..L}  \<and> a + k - 1 \<le> b \<and> forkable (drop (a- 1) (take b w)))"
       by (metis atLeastAtMost_iff ij(3) le_0_eq le_add2 nat_less_le not_less tmp)

     thus "(\<exists>a b. a \<in> {1..L} \<and> b \<in> {0..L} \<and> a + k - 1 \<le> b \<and> forkable (drop (a-1) (take b w)))"
       using case0
       by blast
    qed
    have "\<P>(x in w_pmf L. \<not> k_cp k x) =  measure_pmf.prob (w_pmf L) {x \<in> set_pmf (w_pmf L).  \<not> k_cp k x}"
      by (smt DiffE IntE inf_top.right_neutral measure_prob_cong_0 mem_Collect_eq set_pmf_iff space_measure_pmf)
    have "{x \<in> set_pmf (w_pmf L).  \<not> k_cp k x} 
\<subseteq> {x \<in> set_pmf (w_pmf L). \<exists>a b. a \<in> {1..L} \<and> b \<in> {0..L} \<and> a + k - 1 \<le> b \<and> forkable (drop (a-1) (take b x))}"
      using imp
      by (smt Collect_mono set_pmf_w_pmf_size subsetCE)
    hence " measure_pmf.prob (w_pmf L) {x \<in> set_pmf (w_pmf L).  \<not> k_cp k x}
\<le>  measure_pmf.prob (w_pmf L) 
{x \<in> set_pmf (w_pmf L). \<exists>a b. a \<in> {1..L} \<and> b \<in> {0..L} \<and> a + k - 1 \<le> b \<and> forkable (drop (a-1) (take b x))}"
      by (simp add: measure_pmf.finite_measure_mono)
    thus ?thesis
proof -
  have "\<forall>B p. measure_pmf.prob (p::bool list pmf) (set_pmf p \<inter> B) = measure_pmf.prob p B"
by (simp add: Int_commute measure_Int_set_pmf)
then show ?thesis
by (metis (no_types) Collect_conj_eq Collect_mem_eq \<open>measure_pmf.prob (local.w_pmf L) {x \<in> set_pmf (local.w_pmf L). \<not> k_cp k x} \<le> measure_pmf.prob (local.w_pmf L) {x \<in> set_pmf (local.w_pmf L). \<exists>a b. a \<in> {1..L} \<and> b \<in> {0..L} \<and> a + k - 1 \<le> b \<and> forkable (drop (a - 1) (take b x))}\<close> inf_top.left_neutral space_measure_pmf)
qed   
qed

lemma  k_cp_violation_upperbound_2:
  assumes "k \<le> L" 
  shows "\<P>(x in w_pmf L. 
         \<exists>a b. a \<in> {1..L} \<and> b \<in> {0..L} \<and> a + k - 1 \<le> b \<and> forkable (drop (a-1) (take b x)))
         \<le> (\<Sum>a \<in> {1..L}.(\<Sum> b \<in> {(a+k-1)..L}.\<P>(x in w_pmf L.  forkable (drop (a-1) (take b x)))))"
proof - 
  have "\<P>(x in w_pmf L. 
\<exists>a b. a \<in> {1..L} \<and> b \<in> {0..L} \<and> a + k - 1 \<le> b \<and> forkable (drop (a-1) (take b x)))
= measure_pmf.prob (w_pmf L) {x \<in> set_pmf (w_pmf L). 
\<exists>a b. a \<in> {1..L} \<and> b \<in> {0..L} \<and> a + k - 1 \<le> b \<and> forkable (drop (a-1) (take b x))}"
  proof -
    have "\<forall>B p. measure_pmf.prob (p::bool list pmf) (set_pmf p \<inter> B) = measure_pmf.prob p B"
      by (simp add: Int_commute measure_Int_set_pmf)
    then show ?thesis
      by (simp add: Collect_conj_eq)
  qed
  thus ?thesis sorry
qed



end
end                                                                        