theory Forkable_Strings_Are_Rare

imports
  Ouro
  "HOL-Probability.Conditional_Expectation"
  "HOL-Probability.Central_Limit_Theorem"
begin

context
  fixes \<epsilon>::real  assumes ep_gt_0 : "0 < \<epsilon>" and ep_le_1: "\<epsilon> \<le> 1"  
  fixes \<alpha>::real  assumes \<alpha>:"\<alpha> = (1 + \<epsilon>)/(2*\<epsilon>)"                         
begin

lemma honest_slot :
  "(1 - (1 - \<epsilon>) / 2) =  (1 + \<epsilon>)/2"
proof -
  have "1 - (1 + - 1 * \<epsilon>) / 2 = 1 + - 1 * ((1 + - 1 * \<epsilon>) / 2)"
    by algebra
  then have "1 - (1 - \<epsilon>) / 2 = 1 + - 1 * ((1 + - 1 * \<epsilon>) / 2)"
    by force
  then show ?thesis
    by auto
qed

fun rev_m :: "bool list \<Rightarrow> int \<times> int" 
  where "rev_m [] = (0,0)"
  |"rev_m (True#w) = (fst (rev_m w) + 1, snd (rev_m w) + 1)"
  |"rev_m (False#w) = (if fst (rev_m w) > snd (rev_m w) \<and> snd (rev_m w) = 0
  then (fst (rev_m w) - 1, 0)
  else 
    (if fst (rev_m w) = 0
    then (0, snd (rev_m w) - 1)
    else (fst (rev_m w) - 1, snd (rev_m w) - 1)))"

lemma rev_m_m_rev:"rev_m l = m (rev l)" 
proof (induction l)
  case Nil
  have 0: "rev_m [] = (0,0)"
    by simp
  have "m [] = (0,0)"
    by (simp add: recursive_margin_Nil)
  then show ?case using 0
    by simp
next
  case (Cons k t)
  hence x:"rev_m t = m (rev t)"
    by simp
  then show ?case
  proof (cases k)
    case True
    then show ?thesis using  theorem_4_19_True_part
      by (simp add: Cons.IH m_def)
next
  case False
  note f = this
  thm f
  then show ?thesis 
  proof (cases "rho_s (rev t) > margin_s (rev t) \<and> margin_s (rev t) = 0")
    case True
    hence 1:"m ((rev t)@[False])= (rho_s (rev t) - 1, 0)"
      using theorem_4_19_False_part
      by simp 
    hence "fst (rev_m t) > snd (rev_m t) \<and> snd (rev_m t) = 0"
      using True by (simp add: Cons.IH m_def)
    then show ?thesis using 1
      by (simp add: Cons.IH False m_def) 
  next
    case False
    note ff=this
    thm ff
    hence 2:"\<not> (rho_s (rev t) > margin_s (rev t) \<and> margin_s (rev t) = 0)"
      by auto
    hence 22:"\<not> (fst (rev_m t) > snd (rev_m t) \<and> snd (rev_m t) = 0)"
      by (simp add: m_def x)
    then show ?thesis using 2 
    proof (cases "rho_s (rev t) = 0")
      case True
      hence 3:"m ((rev t)@[False])= (0, margin_s (rev t) - 1)"
        using f ff theorem_4_19_False_part 2
        by simp
    hence "fst (rev_m t) = 0"
      using True
      by (simp add: m_def x) 
      then show ?thesis using 3
        by (simp add: f m_def x)
    next
      case False
      note fff = this
      thm fff
      hence 4:"m ((rev t)@[False])= (rho_s (rev t) - 1, margin_s (rev t) - 1)"
        using ff theorem_4_19_False_part
        by simp
      hence "\<not> (fst (rev_m t) = 0)"
        using fff
        by (simp add: m_def x) 
    then show ?thesis 
      using 4 x
      by (simp add: f ff m_def) 
    qed
  qed
qed
qed

definition rev_\<rho> :: "bool list \<Rightarrow> int" where
  "rev_\<rho> w = fst (rev_m w)"

definition rev_\<mu> :: "bool list \<Rightarrow> int" where
  "rev_\<mu> w = snd (rev_m w)"

definition rev_\<mu>_bar :: "bool list \<Rightarrow> int" where
  "rev_\<mu>_bar w = min 0 (rev_\<mu> w)"

(*building random variables according to the paper*)
definition w_n_pmf :: "nat pmf" where
 "w_n_pmf = map_pmf (\<lambda>b. (if b then 1 else 0)) (bernoulli_pmf ((1-\<epsilon>)/2))"

definition W_n_pmf :: "int pmf" where 
 "W_n_pmf = bind_pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (\<lambda>b. return_pmf ((-1) ^ (if b then 0 else 1)))"

definition w_i_pmf :: "nat \<Rightarrow> (nat \<Rightarrow> bool) pmf" where "w_i_pmf n = Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))"

definition w_pmf where "w_pmf n = map_pmf (\<lambda>f. map f [0..<n]) (w_i_pmf n)"

(*
fun w_list_pmf :: "nat \<Rightarrow> bool list pmf" where
  "w_list_pmf 0 = return_pmf []"
| "w_list_pmf n =
   bind_pmf (w_list_pmf (n - 1)) 
   (\<lambda>xs. bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (\<lambda>x.
   return_pmf (x # xs)))"

definition m_n_pmf :: "nat \<Rightarrow> (int \<times> int) pmf" where
  "m_n_pmf = (\<lambda>n. map_pmf (rev_m) (w_list_pmf n))"

definition \<rho>_n_pmf :: "nat \<Rightarrow> int pmf" where
 "\<rho>_n_pmf = (\<lambda>n. map_pmf (fst \<circ> rev_m) (w_list_pmf n))"

definition \<mu>_n_pmf :: "nat \<Rightarrow> int pmf" where
 "\<mu>_n_pmf = (\<lambda>n. map_pmf (snd \<circ> rev_m) (w_list_pmf n))"

definition \<mu>_n_bar_pmf:: "nat \<Rightarrow> int pmf" where
  "\<mu>_n_bar_pmf = (\<lambda>n. map_pmf (min 0) (\<mu>_n_pmf n))"
*)

lemma "\<alpha> \<ge> 1"
proof -
  have "1 + \<epsilon> \<ge> 2 * \<epsilon>"
    using ep_le_1 by simp
  have "2 * \<epsilon> > 0"
    using ep_gt_0 by simp
  thus ?thesis using \<open>1 + \<epsilon> \<ge>  2 * \<epsilon>\<close>
    by (simp add: \<open>\<alpha> = (1 + \<epsilon>) / (2 * \<epsilon>)\<close>)
qed
(*
lemma "measure_pmf.expectation (w_list_pmf 0) size = 0"
  by simp
*)
lemma "measure_pmf.expectation (W_n_pmf) (real_of_int) = -\<epsilon>"
proof -
  have 1:"finite {True, False}"
    by simp
  have 2:"\<And>x. x \<in> {True, False} \<Longrightarrow> finite (set_pmf ((\<lambda>b. return_pmf ((-1) ^ (if b then 0 else 1))) x))"
    by auto
  have "set_pmf (bernoulli_pmf ((1-\<epsilon>)/2)) \<subseteq> {True, False}"
    by (simp add: subsetI)
  hence "measure_pmf.expectation ((bernoulli_pmf ((1-\<epsilon>)/2)) \<bind> (\<lambda>b. return_pmf ((-1) ^ (if b then 0 else 1)))) (real_of_int) = 
      (\<Sum> a\<in> {True, False}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) a *\<^sub>R 
      measure_pmf.expectation ((\<lambda>b. return_pmf ((-1) ^ (if b then 0 else 1))) a) real_of_int)"
    apply-
    apply (rule pmf_expectation_bind)
    by auto
  hence "measure_pmf.expectation (W_n_pmf) (real_of_int) = 
      (\<Sum> a\<in> {True, False}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) a *\<^sub>R 
      measure_pmf.expectation ((\<lambda>b. return_pmf ((-1) ^ (if b then 0 else 1))) a) real_of_int)"
    using W_n_pmf_def by simp
  also have " \<dots> = pmf (bernoulli_pmf ((1-\<epsilon>)/2)) True *\<^sub>R 
      measure_pmf.expectation ((\<lambda>b. return_pmf ((-1) ^ (if b then 0 else 1))) True) real_of_int
            + pmf (bernoulli_pmf ((1-\<epsilon>)/2)) False *\<^sub>R 
      measure_pmf.expectation ((\<lambda>b. return_pmf ((-1) ^ (if b then 0 else 1))) False) real_of_int"
    by simp
  also have "\<dots> = ((1-\<epsilon>)/2) *\<^sub>R (measure_pmf.expectation ((\<lambda>b. return_pmf ((-1) ^ (if b then 0 else 1))) True) real_of_int) 
              +  (1 - (1-\<epsilon>)/2) *\<^sub>R (measure_pmf.expectation ((\<lambda>b. return_pmf ((-1) ^ (if b then 0 else 1))) False) real_of_int)"
    using pmf_bernoulli_True pmf_bernoulli_False
    using ep_gt_0 ep_le_1 by auto
  also have "\<dots> = - \<epsilon>"
    by simp
  finally show ?thesis by simp
qed

lemma finite_False_func_set:
"finite {f. \<forall>x. x \<notin> {..<(n::nat)} \<longrightarrow> f x = False}"
proof (induction n)
  case 0
  have "{..<(0::nat)} = {x. x < (0::nat)}"
    by (simp add: lessThan_def)
  hence "{..<(0::nat)} = {}"  by simp
  hence "\<And>f. 
        (\<forall>x. x \<notin> {..<(0::nat)} \<and> f x = False) \<Longrightarrow> 
          f = (\<lambda>y. False)"
    by auto
  have"{f. \<forall>x. x \<notin> {..<(0::nat)} \<longrightarrow> f x = False} 
           = {(\<lambda>y. False)}"
        by fastforce
  then show ?case by simp
next
  case (Suc n)
  hence "finite {f. \<forall>x. x \<notin> {..<(n::nat)} \<longrightarrow> f x = False}"
    by auto
  have "{f. \<forall>x. x \<notin> {..<(n::nat)} \<longrightarrow> f x = False} = {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False}"
    by auto
  have "{f. \<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False} 
            = {f. \<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False \<and> (f n = True \<or> f n = False)}"
    by simp
  also have "...
            = {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True) 
                \<or> ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = False)}"
    by auto
 also have "...
            = {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True)} 
                \<union> {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = False)}"
   by (simp add: Collect_disj_eq)
    also have "...
            = {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True)} 
                \<union> {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False}"
      by (metis (no_types, hide_lams) atLeast_Suc_greaterThan atLeast_iff greaterThan_def le_less mem_Collect_eq)
    finally have fin:"{f. \<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False} =
{f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True)} 
                \<union> {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False}"
      by blast 
    have "card {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} = card {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True)}"
    proof -
      define g where g:"g = (\<lambda>f x. if x = n then True else f x)"
      define h where h:"h = (\<lambda>f x. if x = n then False else f x)"
      have "\<forall>x y. (x\<in>{f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} \<and> y\<in>{f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} \<and>
                    g x = g y) \<longrightarrow> x = y"
      proof (rule ccontr)
        assume "\<not> (\<forall> x y. (x\<in>{f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} \<and> y\<in>{f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False}
                  \<and> g x = g y) \<longrightarrow> x = y)"
        then obtain x y where 
asm1:"x\<in>{f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} \<and> y\<in>{f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} \<and> g x = g y \<and> x \<noteq> y"
          by blast
        then obtain z where z:"x z \<noteq> y z"
          using asm1 by blast
        have "\<forall>w h. w \<noteq> n \<longrightarrow> g h w = h w"
          using g
          by simp
        hence "z = n"
          using z asm1
          by metis
        hence "x n \<noteq> y n"
          using z by auto
        hence "x n = True \<or> y n = True"
          by auto
        hence "x\<notin>{f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} \<or> y\<notin>{f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False}"
          by blast
        thus "False"
          using asm1 by linarith
      qed
      hence "inj_on g {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False}"
        by (meson inj_onI)
      have "g ` {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} =  
            {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True)}"
      proof - 
        have "\<forall>x. x \<in> {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False}  
            \<longrightarrow> g x \<in> {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True)}"
          by (simp add: g)
        hence sub:"g ` {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} \<subseteq>  
            {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True)}"
          by auto
        have "\<And>x. x n \<Longrightarrow> \<forall>k. g (h x) k= x k"
          by (simp add: g h)
        hence "\<And>x. x n \<Longrightarrow> g (h x) = x"
          by (simp add: g h)
        hence exist:"\<And>x. \<forall>xa\<ge>Suc n. \<not> x xa \<Longrightarrow> x n \<Longrightarrow> (\<forall>z\<ge>n. \<not> (h x) z) \<and> g (h x) = x"
          using h by auto
        hence "\<forall>x. x \<in> {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True)} 
         \<longrightarrow> (\<exists>y. y \<in> {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} \<and> g y = x)"
          apply simp
          apply clarify
          by metis
        thus ?thesis using sub
          using subset_antisym by auto
      qed
      hence "bij_betw g {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False} {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True)}"
        using \<open>inj_on g {f. \<forall>x. x \<in> {n..} \<longrightarrow> f x = False}\<close> inj_on_imp_bij_betw by force
      thus ?thesis
        using bij_betw_same_card by blast
    qed 
    hence tmp:"card {f. \<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False}
        \<le> card  {f. ((\<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False) \<and> f n = True)} + card {f. \<forall>x. x \<in> {n::nat..} \<longrightarrow> f x = False}"
      using fin
      by (metis (mono_tags, lifting) card_Un_le)
    have "card {f. \<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False} \<noteq> 0"
    proof - 
      define f where "f = (\<lambda>x::nat. False)"
      have "f \<in> {f. \<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False}"
        by (simp add: f_def)
      thus ?thesis
        by (smt Suc.IH \<open>card {f. \<forall>x. x \<in> {n..} \<longrightarrow> f x = False} = card {f. (\<forall>x. x \<in> {Suc n..} \<longrightarrow> f x = False) \<and> f n = True}\<close>
 \<open>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False} = {f. \<forall>x. x \<in> {n..} \<longrightarrow> f x = False}\<close> card_eq_0_iff empty_iff f_def fin finite_UnI mem_Collect_eq)
    qed
    hence "finite {f. \<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False}" using tmp card_infinite
      by fastforce
    then show ?case 
    proof -
       have "{f. \<forall>x. x \<notin> {..<(Suc n::nat)} \<longrightarrow> f x = False} = {f. \<forall>x. x \<in> {Suc n::nat..} \<longrightarrow> f x = False}"
         by auto
       thus ?thesis
         using \<open>finite {f. \<forall>x. x \<in> {Suc n..} \<longrightarrow> f x = False}\<close> by auto
     qed
   qed

lemma finite_set_w_i_pmf:"finite (set_pmf (w_i_pmf n))"
proof -
  have "set_pmf (Pi_pmf {..<n::nat} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) \<subseteq> {f. \<forall>x. x \<notin> {..<n::nat} \<longrightarrow> f x = False}"
    apply (rule set_Pi_pmf_subset)
    by auto
  thus ?thesis using finite_False_func_set
  proof -
    show ?thesis
      by (metis (no_types) \<open>\<And>n. finite {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}\<close> \<open>set_pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1 - \<epsilon>) / 2))) \<subseteq> {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}\<close> infinite_super w_i_pmf_def)
  qed
qed

lemma expectation_w_pmf_i_minus_ep:
  assumes "i < n" 
  shows "measure_pmf.expectation (w_pmf n) (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1)) \<circ> (\<lambda>l. nth l i)) = -\<epsilon>"
proof -
  have "measure_pmf.expectation (w_pmf n) (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1)) \<circ> (\<lambda>l. nth l i)) 
        = measure_pmf.expectation (map_pmf (\<lambda>f. map f [0..<n]) (w_i_pmf n)) 
          (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1)) \<circ> (\<lambda>l. nth l i))"
    using w_pmf_def by simp
  also have "... 
        = measure_pmf.expectation (bind_pmf (w_i_pmf n) (\<lambda>x. return_pmf (map x [0..<n]))) 
          (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1)) \<circ> (\<lambda>l. nth l i))"
    using w_i_pmf_def
    by (simp add: map_pmf_def)
  also have "... = 
           (\<Sum>a\<in> {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
            measure_pmf.expectation ((\<lambda>x. return_pmf (map x [0..<n])) a) 
            (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1)) \<circ> (\<lambda>l. nth l i)))"
    apply -
    apply (rule pmf_expectation_bind)
    using finite_False_func_set apply auto[1]
    apply simp
    by (smt Collect_mono_iff finite_lessThan set_Pi_pmf_subset set_pmf_eq w_i_pmf_def)
   also have "... = 
           (\<Sum>a\<in> {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
            (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1))) (a i))"
     using nth_map assms by simp
also have "... = 
           (\<Sum>a\<in> {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
            (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1))) (a i))"
  using nth_map assms by simp
  finally have "measure_pmf.expectation (w_pmf n) (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1)) \<circ> (\<lambda>l. nth l i)) = 
           (\<Sum>a\<in> {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
            (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1))) (a i))"
    using nth_map assms
    by (simp add: \<open>measure_pmf.expectation (local.w_i_pmf n \<bind> (\<lambda>x. return_pmf (map x [0..<n]))) (real_of_int \<circ> (\<lambda>b. if b then 1 else - 1) \<circ> (\<lambda>l. l ! i)) = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (local.w_i_pmf n) a *\<^sub>R measure_pmf.expectation (return_pmf (map a [0..<n])) (real_of_int \<circ> (\<lambda>b. if b then 1 else - 1) \<circ> (\<lambda>l. l ! i)))\<close> map_pmf_def w_pmf_def)
  also have "...  =
           (\<Sum>a\<in> {f. \<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
            (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1))) (a i))"
    using assms
  by (metis (no_types, lifting) ivl_disj_un_one(1) ivl_disj_un_singleton(2)) 
  also have  "... = 
           (\<Sum>a\<in> {f. \<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False \<and> (f i = False \<or> f i =True)}.
            pmf (w_i_pmf n) a *\<^sub>R (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1))) (a i))"
    by simp
    also have "... = 
           (\<Sum>a\<in> {f. ((\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False) \<or>
            ((\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True)}.
            pmf (w_i_pmf n) a *\<^sub>R (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1))) (a i))"
      by (metis (mono_tags, hide_lams))
      also have "... = 
           (\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False} \<union>
            {f.(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
            pmf (w_i_pmf n) a *\<^sub>R (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1))) (a i))"
        by (simp add: Collect_disj_eq)
     also have "... = 
           (\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
          pmf (w_i_pmf n) a *\<^sub>R (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1))) (a i))
            + 
          (\<Sum>a\<in>  {f.(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
            pmf (w_i_pmf n) a *\<^sub>R (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1))) (a i))"
     proof - 
       have interemp:"{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False} \<inter> 
              {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True} = {}" 
       proof (rule ccontr)
         assume "\<not> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False} \<inter> 
              {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True} = {}"
         then obtain f where f:"f \<in>
{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False} \<inter> 
              {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}"
           by auto
         hence "f i = False"
           using assms by fastforce
         thus "False" using f assms by fastforce
       qed
       have fin:"finite {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False} "
         using finite_False_func_set
         by simp
       have "{f. (\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and> f i = False} \<subseteq> {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False} \<and>
              {f. (\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and> f i = True} \<subseteq> {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}"
         by blast
       hence "finite {f. (\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and> f i = False} \<and> 
              finite {f. (\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and> f i = True}"
         using fin
         using finite_subset by auto
hence "finite {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False} \<and> 
              finite {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}"
proof -
  have f1: "(\<lambda>p. (\<forall>na. na \<notin> {..<n} \<longrightarrow> \<not> p na) \<and> p i) = (\<lambda>p. (\<forall>na. na \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> \<not> p na) \<and> p i)"
    by (metis (lifting) assms ivl_disj_un_one(1) ivl_disj_un_singleton(2))
  have "(\<lambda>p. (\<forall>na. na \<notin> {..<n} \<longrightarrow> \<not> p na) \<and> \<not> p i) = (\<lambda>p. (\<forall>na. na \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> \<not> p na) \<and> \<not> p i)"
by (metis (lifting) assms ivl_disj_un_one(1) ivl_disj_un_singleton(2))
  then show ?thesis
    using f1 \<open>finite {f. (\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and> f i = False} \<and> finite {f. (\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and> f i = True}\<close> by presburger
qed
  thus ?thesis using infinite_Un sum.union_disjoint interemp
    by (simp add: sum.union_disjoint)
qed

also have "... = 
           (\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
           - pmf (w_i_pmf n) a)
            + 
          (\<Sum>a\<in>  {f.(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
            pmf (w_i_pmf n) a)"
  by simp
  also have "... = 
           (\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
           - (\<Prod>x\<in>{..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))
            + 
          (\<Sum>a\<in>  {f.(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
            (\<Prod>x\<in>{..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
proof - 
  have x:"\<And>a. a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False} \<Longrightarrow>
pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a 
= (if \<forall>x. x \<notin> {..<n} \<longrightarrow> a x = False then (\<Prod>x\<in>{..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)) x)  (a x)) else 0)"
    apply - 
    apply (rule pmf_Pi)
    by simp
  hence F:"\<And>a. a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False} \<Longrightarrow>
pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a 
= (\<Prod>x\<in>{..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)) x)  (a x))"
  proof - 
    fix a 
    assume "a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}"
    have "\<forall>x. x \<notin> {..<n} \<longrightarrow> a x = False"
      using \<open>a \<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}\<close> assms by auto   
    thus "pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a 
= (\<Prod>x\<in>{..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)) x)  (a x))"
      using x \<open>a \<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}\<close> by auto
  qed
 have y:"\<And>a. a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True} \<Longrightarrow>
pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a 
= (if \<forall>x. x \<notin> {..<n} \<longrightarrow> a x = False then (\<Prod>x\<in>{..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)) x)  (a x)) else 0)"
    apply - 
    apply (rule pmf_Pi)
    by simp
  hence T:"\<And>a. a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True} \<Longrightarrow>
pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a 
= (\<Prod>x\<in>{..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)) x)  (a x))"
  proof - 
    fix a 
    assume "a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}"
    have "\<forall>x. x \<notin> {..<n} \<longrightarrow> a x = False"
      using \<open>a \<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}\<close> assms by auto   
    thus "pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a 
= (\<Prod>x\<in>{..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)) x)  (a x))"
      using y \<open>a \<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}\<close> by auto
  qed
  thus ?thesis
    using F w_i_pmf_def by auto 
qed
 also have "... = 
           (\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
           - (\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))
            + 
          (\<Sum>a\<in>  {f.(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
            (\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
   by (metis (no_types, lifting) assms ivl_disj_un_one(1) ivl_disj_un_singleton(2) sum.cong)
 also have "... = 
           (\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
           - (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a i))
            + 
          (\<Sum>a\<in>  {f.(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
            (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a i))"
 proof - 
   have "{i} \<inter> ({..<i} \<union> {i<..<n}) = {}"
     by simp
   hence "\<forall>f. (\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. f x) = (\<Prod>x\<in> {..<i} \<union> {i<..<n}. f x) * f i"
     by (simp add: mult.commute)
   thus ?thesis
     by (smt Groups.mult_ac(2) mult_minus_right sum.cong)
 qed
 also have "... = 
           (\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
           - (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *(1- (1-\<epsilon>)/2))
            + 
          (\<Sum>a\<in>  {f.(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
            (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * ((1-\<epsilon>)/2))"
   using ep_gt_0 ep_le_1 by auto
also have "... = 
           (\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
           - (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *((1+\<epsilon>)/2))
            + 
          (\<Sum>a\<in>  {f.(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
            (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * ((1-\<epsilon>)/2))"
   using ep_gt_0 ep_le_1
   by (simp add: honest_slot) 
also have "... = 
           - (1+\<epsilon>)/2 + 
          (\<Sum>a\<in>  {f.(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
            (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * ((1-\<epsilon>)/2))"
proof - 
  have smp:"{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}=
            {f. \<forall>x. x \<notin> {..<i} \<union> {i<..<n} \<longrightarrow> f x = False}"
    by auto
  have "\<And>a. a \<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False} \<Longrightarrow>
          pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a 
        = (if \<forall>x. x \<notin> {..<i} \<union> {i<..<n} \<longrightarrow> a x = False then (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))
   else 0)"
    apply -
    apply (rule pmf_Pi)
    by blast
  hence pmfpi:"\<And>a. a \<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False} \<Longrightarrow>
          pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a 
        = (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))"
    by auto
  have sbst: "set_pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) \<subseteq> 
{f. (\<forall>x. x \<notin> {..<i} \<union> {i<..<n} \<longrightarrow> f x = False)}"
    apply -
    apply (rule set_Pi_pmf_subset)
    by blast
  then obtain C where C:"set_pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) \<union> C = 
{f. (\<forall>x. x \<notin> {..<i} \<union> {i<..<n} \<longrightarrow> f x = False)} \<and> 
set_pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) \<inter> C = {}"
    by (smt Diff_disjoint Un_Diff_cancel sup.absorb_iff2)
  have fin:"finite {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False} "
         using finite_False_func_set
         by simp
    have "{f. (\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and> f i = False} \<subseteq> {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}"
         by blast
    hence "finite {f. (\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and> f i = False}"
         using fin
         using finite_subset by auto
   hence "finite {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}"
  by (smt Collect_cong assms ivl_disj_un_one(1) ivl_disj_un_singleton(2))
  hence finC: "finite (set_pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)))) \<and> finite C"
      using C
      by (metis infinite_Un smp)
  hence C0: "\<forall>a. a \<in> C \<longrightarrow> pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a = 0"
    using C by (smt disjoint_iff_not_equal mem_Collect_eq set_pmf_eq) 
  hence "(\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
        (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) =
(\<Sum>a\<in> set_pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) \<union> C.
      pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a)"
    using smp C sbst pmfpi by auto
  also have "... = 
(\<Sum>a\<in> set_pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))).
      pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a)
+ 
(\<Sum>a\<in> C. pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a)"
      using C sum.union_disjoint
      by (smt \<open>finite (set_pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1 - \<epsilon>) / 2)))) \<and> finite C\<close>) 
 also have "... = 1 + 
(\<Sum>a\<in> C. pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a)"
   using sum_pmf_eq_1
   by (simp add: sum_pmf_eq_1 \<open>finite (set_pmf (Pi_pmf ({..<i} \<union> {i<..<n}) False (\<lambda>_. bernoulli_pmf ((1 - \<epsilon>) / 2)))) \<and> finite C\<close>)
  finally have sum1:"(\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
 (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) = 1"
    using C0 finC comm_monoid_add_class.sum.neutral_const
    by simp
  have "(\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
           - (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *((1+\<epsilon>)/2)) =
       (\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
           (-1) * (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *((1+\<epsilon>)/2))" 
    by simp
have "(\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
           - (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *((1+\<epsilon>)/2)) =
       (-1) * (\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}.
            (\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) *((1+\<epsilon>)/2)"
  by (simp add: sum_distrib_left sum_distrib_right)  
  thus ?thesis
  proof -
    show ?thesis
      by (metis (no_types) \<open>(\<Sum>a\<in>{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}. - (\<Prod>x\<in>{..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * ((1 + \<epsilon>) / 2)) = - 1 * (\<Sum>a\<in>{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}. \<Prod>x\<in>{..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * ((1 + \<epsilon>) / 2)\<close> divide_minus_left mult.left_neutral mult_minus_left sum1)
  qed
qed
also have "... = 
           - (1+\<epsilon>)/2 + (1-\<epsilon>)/2"
proof -
  have pmfpi1:"\<And>a. pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) a        
= (if \<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> a x = False 
then (\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))  (a x))
   else 0)"
    apply -
    apply (rule pmf_Pi)
    by blast
hence pmfpi2:"\<And>a. a \<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True} \<Longrightarrow>
          pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) a 
        = (\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (a x))"
  by auto
  have sbst': "set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) \<subseteq> 
{f. \<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False}"
    apply -
    apply (rule set_Pi_pmf_subset)
    by blast
  have notin:"\<And>f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False \<Longrightarrow> f \<notin> set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)))"
  proof -
    fix f
    assume asm:"(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False"
    have 
"(\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. 
pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (f x)) = 
(\<Prod>x\<in> {..<i} \<union> {i<..<n} \<union> {i}.
pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (f x))" by auto
    also have "... = 
(\<Prod>x\<in> {..<i} \<union> {i<..<n}.
pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (f x))
* (\<Prod>x\<in> {i}.
pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (f x))"
      by simp
also have "... = 
(\<Prod>x\<in> {..<i} \<union> {i<..<n}.
pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (f x))
* pmf (return_pmf True) (f i)"
  by simp
  finally have 0:"(\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. 
pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (f x)) = 0"
    using asm pmf_return by auto
  have "pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) f 
    =  (\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (f x))"
    using pmfpi1
    by (simp add: asm)
      thus "f \<notin> set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)))"
        using 0
        by (simp add: set_pmf_eq)
    qed
hence same:"(set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
          (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)))) 
- {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}  
= (set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
          (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))))"
      by blast
      find_theorems disjoint
      have "set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)))
- 
{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}
\<subseteq>
{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False)} - 
{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}" using sbst'
      using notin  by (simp add: Diff_mono)
hence "set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)))
\<subseteq>
{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False)} - 
{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}" using sbst'
  using same by simp
hence "set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)))
\<subseteq>
{f. f\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False)} 
\<and> f \<notin>{ f.(\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = False}}" using sbst'
  by blast
hence "set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)))
\<subseteq> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) 
\<and> f i = True}" using sbst'
  by blast  
then obtain C' where C':"set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) \<union> C' = 
{f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) 
\<and> f i = True} \<and> 
set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) \<inter> C' = {}"
  by (smt Diff_disjoint Un_Diff_cancel sup.absorb_iff2)
  have fin:"finite {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False} "
         using finite_False_func_set
         by simp
    have "{f. (\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and> f i = True} \<subseteq> {f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}"
         by blast
    hence "finite {f. (\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and> f i = True}"
         using fin
         using finite_subset by auto
   hence "finite {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}"
  by (smt Collect_cong assms ivl_disj_un_one(1) ivl_disj_un_singleton(2))
  hence finC': "finite (set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)))) \<and> finite C'"
    using C'
    by (metis (no_types, lifting) infinite_Un)
  hence C0': "\<forall>a. a \<in> C' \<longrightarrow> pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False 
(\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) a = 0"
    using C'
    by (meson disjoint_iff_not_equal pmf_eq_0_set_pmf)
  hence "(\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
        (\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) =
(\<Sum>a\<in> set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) \<union> C'.
      pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) a)"
    using C' sbst' pmfpi2 by auto
  also have "... = 
(\<Sum>a\<in> set_pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))).
      pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) a)
+ 
(\<Sum>a\<in> C'. pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) a)"
    using C' sum.union_disjoint
    by (smt finC')
 also have "... = 1 + 
(\<Sum>a\<in> C'. pmf (Pi_pmf ({..<i} \<union> {i} \<union> {i<..<n}) False (\<lambda>x. if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2))) a)"
     using sum_pmf_eq_1
     using finC' by auto
     finally have sum1':"(\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
        (\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) = 1"
    using C0' finC' comm_monoid_add_class.sum.neutral_const
    by simp
  have smp'':"(\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
        (\<Prod>x\<in> {..<i} \<union> {i} \<union> {i<..<n}. pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
= 
(\<Sum>a\<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True}.
(\<Prod>x\<in> {..<i} \<union> {i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
     using pmf_return
   proof - 
     have "\<And>a x. a \<in> {f. (\<forall>x. x \<notin> {..<i} \<union> {i} \<union> {i<..<n} \<longrightarrow> f x = False) \<and> f i = True} \<and> x\<in> {..<i} \<union> {i<..<n} \<Longrightarrow>
 pmf (if x = i then (return_pmf True) else bernoulli_pmf ((1-\<epsilon>)/2)) (a x) =
pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)"
       by auto
     thus ?thesis by simp
   qed
   thus ?thesis
       by (metis (no_types) mult.left_neutral smp'' sum1' sum_distrib_right)
   qed
   finally have "measure_pmf.expectation (w_pmf n) (real_of_int \<circ> (\<lambda>b. if b then 1 else (-1)) \<circ> (\<lambda>l. nth l i)) 
        = - \<epsilon>"
     using ep_gt_0 ep_le_1
proof -
  assume a1: "measure_pmf.expectation (local.w_pmf n) (real_of_int \<circ> (\<lambda>b. if b then 1 else - 1) \<circ> (\<lambda>l. l ! i)) = - (1 + \<epsilon>) / 2 + (1 - \<epsilon>) / 2"
  have f2: "\<forall>r ra. (ra::real) + r = r + ra"
    by linarith
have f3: "- ((1 + - \<epsilon>) / (1 + 1)) = (\<epsilon> + - 1) / (1 + 1)"
  using honest_slot by auto
  have "\<forall>r ra. - (ra::real) + (r + ra) = r"
    by auto
  then show ?thesis
    using f3 f2 a1 by (metis (no_types) add.inverse_distrib_swap add_uminus_conv_diff one_add_one real_average_minus_second)
qed
     
   thus ?thesis
     by simp
 qed
(*
lemma set_pmf_w_list_ep_ne_1:
  assumes "\<epsilon> \<noteq> 1"
  shows "set_pmf (w_list_pmf n) = {l:: bool list. size l = n}"
proof (induction n)
  case 0
  then show ?case
    by simp 
next
  case (Suc n)
  hence "set_pmf (w_list_pmf n) = {l:: bool list. size l = n}"
    by simp
  have "set_pmf (w_list_pmf (Suc n)) = set_pmf (bind_pmf (w_list_pmf n) 
   (\<lambda>xs. bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (\<lambda>x.
   return_pmf (x # xs))))"
    by auto
  also have "... = (\<Union>M\<in>set_pmf (w_list_pmf n). set_pmf ((\<lambda>xs. bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
            (\<lambda>x. return_pmf (x # xs))) M))"
    by auto
  also have "... = (\<Union>M\<in> {l:: bool list. size l = n}. 
            set_pmf (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
                        (\<lambda>x. return_pmf (x # M))))"
    using Suc.IH by blast
  also have "... = (\<Union>M\<in> {l:: bool list. size l = n}. 
            (\<Union>N\<in> set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). 
                       set_pmf (return_pmf (N # M))))"
    by simp
  also have "... = (\<Union>M\<in> {l:: bool list. size l = n}. 
            (\<Union>N\<in> {True, False}. 
                       set_pmf (return_pmf (N # M))))"
    using ep_gt_0 ep_le_1 assms set_pmf_bernoulli
    using half_gt_zero_iff by auto
  also have "... = (\<Union>M\<in> {l:: bool list. size l = n}. 
             {True # M, False # M})"
    by auto
  finally have "... = {l:: bool list. size l = Suc n}"
  proof -
    have "\<And>x. x \<in> (\<Union>M\<in> {l:: bool list. size l = n}.
             {True # M, False # M}) \<Longrightarrow> x \<in> {l:: bool list. size l = Suc n}"
    proof -
      fix  x
      assume "x \<in> (\<Union>M\<in> {l:: bool list. size l = n}. {True # M, False # M})"
      obtain a b where "x = a # b"
        using \<open>x \<in> (\<Union>M\<in>{l. length l = n}. {True # M, False # M})\<close> by blast
      hence "b \<in> {l:: bool list. size l = n}"
        using \<open>x \<in> (\<Union>M\<in>{l. length l = n}. {True # M, False # M})\<close> by blast
      hence "size (a#b) = Suc n"
        by simp
      thus "x \<in> {l:: bool list. size l = Suc n}"
        by (simp add: \<open>x = a # b\<close>)
    qed
    have "\<And>x.  x \<in> {l:: bool list. size l = Suc n} \<Longrightarrow> x \<in> (\<Union>M\<in> {l:: bool list. size l = n}.
             {True # M, False # M}) "
    proof -
      fix x
      assume "x \<in> {l:: bool list. size l = Suc n}"
      obtain a b where "x = a # b"
        by (metis (mono_tags, lifting) \<open>x \<in> {l. length l = Suc n}\<close> length_Suc_conv mem_Collect_eq)
      hence "size b = n"
        using \<open>x \<in> {l. length l = Suc n}\<close> by auto
      hence "x \<in> (\<Union>M\<in> {l:: bool list. size l = n}. {a#M})"
        by (simp add: \<open>x = a # b\<close>)
      thus "x \<in> (\<Union>M\<in> {l:: bool list. size l = n}. {True # M, False # M})"
        by blast
    qed
  then show ?thesis
    using \<open>\<And>x. x \<in> (\<Union>M\<in>{l. length l = n}. {True # M, False # M}) \<Longrightarrow> x \<in> {l. length l = Suc n}\<close> by blast
  qed
  thus ?case
    using \<open>(\<Union>M\<in>set_pmf (local.w_list_pmf n). set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2) \<bind> (\<lambda>x. return_pmf (x # M)))) 
= (\<Union>M\<in>{l. length l = n}. set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2) \<bind> (\<lambda>x. return_pmf (x # M))))\<close> 
\<open>(\<Union>M\<in>{l. length l = n}. \<Union>N\<in>set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). set_pmf (return_pmf (N # M))) = 
(\<Union>M\<in>{l. length l = n}. \<Union>N\<in>{True, False}. set_pmf (return_pmf (N # M)))\<close> \<open>(\<Union>M\<in>{l. length l = n}. 
\<Union>N\<in>{True, False}. set_pmf (return_pmf (N # M))) = (\<Union>M\<in>{l. length l = n}. {True # M, False # M})\<close> 
\<open>(\<Union>M\<in>{l. length l = n}. set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2) \<bind> (\<lambda>x. return_pmf (x # M)))) 
= (\<Union>M\<in>{l. length l = n}. \<Union>N\<in>set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). set_pmf (return_pmf (N # M)))\<close> 
\<open>set_pmf (local.w_list_pmf (Suc n)) = set_pmf (local.w_list_pmf n \<bind> (\<lambda>xs. bernoulli_pmf ((1 - \<epsilon>) / 2) 
\<bind> (\<lambda>x. return_pmf (x # xs))))\<close> \<open>set_pmf (local.w_list_pmf n \<bind> (\<lambda>xs. bernoulli_pmf ((1 - \<epsilon>) / 2) 
\<bind> (\<lambda>x. return_pmf (x # xs)))) = (\<Union>M\<in>set_pmf (local.w_list_pmf n). set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)
 \<bind> (\<lambda>x. return_pmf (x # M))))\<close> by presburger
qed
*)
definition \<Phi>_n :: "nat \<Rightarrow> bool list \<Rightarrow> real" where
  "\<Phi>_n n l= (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (drop (size l - n) l))"

definition \<Phi>_n' :: "nat \<Rightarrow> bool list \<Rightarrow> real" where
  "\<Phi>_n' n l= (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (drop (size l - n) l)) + n * \<epsilon>"

definition \<Delta>_n :: "nat \<Rightarrow> bool list \<Rightarrow> real" where
  "\<Delta>_n n l = \<Phi>_n n l - \<Phi>_n (n-1) l"

definition \<Delta>_n' :: "nat \<Rightarrow> bool list \<Rightarrow> real" where
  "\<Delta>_n' n l = \<Phi>_n n l - \<Phi>_n (n-1) l + \<epsilon>"

find_theorems measure_pmf.expectation map_pmf 

lemma rev_rho_s_ge_0: "fst (rev_m l) \<ge> 0"
proof - 
  have "rev_m l = m (rev l)"
    by (simp add: rev_m_m_rev)
  hence "fst (rev_m l) = rho_s (rev l)"
    using m_def by auto
  thus ?thesis
    using rho_s_ge_0 by auto
qed

lemma rev_margin_le_rho_s: "fst (rev_m l) \<ge> snd (rev_m l)"
  using margin_le_rho_s
  by (simp add: m_def rev_m_m_rev)

lemma four_cases: "{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}
= {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (map f [0..<n])) > 0) \<and> (snd (rev_m (map f [0..<n])) < 0)} \<union>
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (map f [0..<n])) > 0) \<and> (snd (rev_m (map f [0..<n])) \<ge> 0)} \<union> 
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (map f [0..<n])) = 0) \<and> (snd (rev_m (map f [0..<n])) < 0)} \<union> 
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (map f [0..<n])) = 0) \<and> (snd (rev_m (map f [0..<n])) = 0)}"
proof -
  have "\<And>l . ((fst (rev_m l) > 0) \<and> (snd (rev_m l) < 0)) \<or>
((fst (rev_m l) > 0) \<and> (snd (rev_m l) \<ge> 0)) \<or>
((fst (rev_m l) = 0) \<and> (snd (rev_m l) < 0)) \<or>
((fst (rev_m l) = 0) \<and> (snd (rev_m l) = 0))"
    by (smt rev_margin_le_rho_s rev_rho_s_ge_0)
  hence "{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False} =
{f. ((\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (map f [0..<n])) > 0) \<and> (snd (rev_m (map f [0..<n])) < 0)))
\<or> 
((\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (map f [0..<n])) > 0) \<and> (snd (rev_m (map f [0..<n])) \<ge> 0))) 
\<or>
((\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (map f [0..<n])) = 0) \<and> (snd (rev_m (map f [0..<n])) < 0))) 
\<or>
((\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (map f [0..<n])) = 0) \<and> (snd (rev_m (map f [0..<n])) = 0)))}"
    by blast
  also have "... = 
{f. \<forall>x. ((x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (map f [0..<n])) > 0) \<and> (snd (rev_m (map f [0..<n])) < 0)))}
\<union>
{f. \<forall>x. ((x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (map f [0..<n])) > 0) \<and> (snd (rev_m (map f [0..<n])) \<ge> 0)))}
\<union>
{f. \<forall>x.
((x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (map f [0..<n])) = 0) \<and> (snd (rev_m (map f [0..<n])) < 0)))}
\<union>
{f. \<forall>x. ((x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (map f [0..<n])) = 0) \<and> (snd (rev_m (map f [0..<n])) = 0)))}"
    by blast
  thus ?thesis
    using calculation by presburger
qed

lemma four_cases_i: 
  assumes "i \<le> n" 
  shows "{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False} 
= {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) < 0)} \<union>
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) \<ge> 0)} \<union> 
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) < 0)} \<union> 
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) = 0)}"
proof -
  have "\<And>l . ((fst (rev_m l) > 0) \<and> (snd (rev_m l) < 0)) \<or>
((fst (rev_m l) > 0) \<and> (snd (rev_m l) \<ge> 0)) \<or>
((fst (rev_m l) = 0) \<and> (snd (rev_m l) < 0)) \<or>
((fst (rev_m l) = 0) \<and> (snd (rev_m l) = 0))"
by (smt rev_margin_le_rho_s rev_rho_s_ge_0)
hence "{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False} =
{f. ((\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (drop i (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) < 0)))
\<or> 
((\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (drop i (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) \<ge> 0))) 
\<or>
((\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (drop i (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) < 0))) 
\<or>
((\<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (drop i (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) = 0)))}"
    by blast
  also have "... = 
{f. \<forall>x. ((x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (drop i (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) < 0)))}
\<union>
{f. \<forall>x. ((x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (drop i (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) \<ge> 0)))}
\<union>
{f. \<forall>x.
((x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (drop i (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) < 0)))}
\<union>
{f. \<forall>x. ((x \<notin> {..<n} \<longrightarrow> f x = False) \<and>
((fst (rev_m (drop i (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) = 0)))}"
    by blast
  thus ?thesis
    using calculation by presburger
qed

lemma pair_disj_aux_i:
  assumes "i \<le> n"
 "A = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) < 0)}" 
"B= {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) \<ge> 0)}"
"C = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) < 0)}" 
"D = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) = 0)}"
shows "A \<inter> B = {}" "A \<inter> C = {}" "A \<inter> D = {}" "B \<inter> C = {}" "B \<inter> D = {}" "C \<inter> D = {}"
  using assms by auto

lemma pair_disj_aux:
  assumes 
 "A = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (map f [0..<n])) > 0) \<and> (snd (rev_m (map f [0..<n])) < 0)}" 
"B= {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (map f [0..<n])) > 0) \<and> (snd (rev_m (map f [0..<n])) \<ge> 0)}"
"C = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (map f [0..<n])) = 0) \<and> (snd (rev_m (map f [0..<n])) < 0)}" 
"D = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (map f [0..<n])) = 0) \<and> (snd (rev_m (map f [0..<n])) = 0)}"
shows "A \<inter> B = {}" "A \<inter> C = {}" "A \<inter> D = {}" "B \<inter> C = {}" "B \<inter> D = {}" "C \<inter> D = {}"
  using assms by auto

lemma subs_finite_i:
  assumes "i \<le> n"
 "A = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) < 0)}" 
"B= {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) \<ge> 0)}"
"C = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) < 0)}" 
"D = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop i (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop i (map f [0..<n]))) = 0)}"
shows "finite A" "finite B" "finite C" "finite D"
  using assms finite_False_func_set by auto

lemma expectation_Delta_i_le_minus_ep:
  assumes "i \<le> n" "i > 0" 
  shows "measure_pmf.expectation (w_pmf n) (\<Delta>_n i) \<le> -\<epsilon>"
proof -
  have "measure_pmf.expectation (w_pmf n) (\<Delta>_n i) = 
    measure_pmf.expectation 
(bind_pmf 
(w_i_pmf n) 
(\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x))) 
(\<Delta>_n i)"
    using w_i_pmf_def w_pmf_def 
    by (simp add: map_pmf_def) 
  also have "... = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
measure_pmf.expectation ((\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x)) a) (\<Delta>_n i))"
    apply -
    apply (rule pmf_expectation_bind)
    using finite_False_func_set apply auto[1]
    apply simp
    by (smt Collect_mono_iff finite_lessThan set_Pi_pmf_subset set_pmf_eq w_i_pmf_def)
  also have "... = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R  
              (\<Phi>_n i (map a [0..<n]) - \<Phi>_n (i-1) (map a [0..<n])))"
    using expectation_return_pmf
    by (simp add: \<Delta>_n_def) 
  finally have st1:"measure_pmf.expectation (bind_pmf (w_i_pmf n) (\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x))) (\<Delta>_n i)
              = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R  
              (\<Phi>_n i (map a [0..<n]) - \<Phi>_n (i-1) (map a [0..<n])))"
    using expectation_return_pmf
    using \<open>measure_pmf.expectation (local.w_pmf n) (local.\<Delta>_n i) = measure_pmf.expectation (local.w_i_pmf n \<bind> (\<lambda>x. return_pmf (map x [0..<n]))) (local.\<Delta>_n i)\<close> by linarith  

  have 0:"\<And>a i. \<Phi>_n  i (map a [0..<n]) =
 real_of_int (fst (rev_m (drop (length (map a [0..<n]) - i) (map a [0..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (length (map a [0..<n]) - i) (map a [0..<n])))))"
    using \<Phi>_n_def by blast
  hence st2:"measure_pmf.expectation (bind_pmf (w_i_pmf n) (\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x))) (\<Delta>_n i)
              = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R  
              (real_of_int (fst (rev_m (drop (length (map a [0..<n]) - i) (map a [0..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (length (map a [0..<n]) - i) (map a [0..<n]))))) - 
(real_of_int (fst (rev_m (drop (length (map a [0..<n]) - (i-1)) (map a [0..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (length (map a [0..<n]) - (i-1)) (map a [0..<n]))))))))"
    using st1 by auto
  also have 0:"...
= (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. 
pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (drop (n - i) (map a [0..<n])))) -
real_of_int (fst (rev_m (drop (n - (i-1)) (map a [0..<n])))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i) (map a [0..<n]))))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - (i-1)) (map a [0..<n])))))))"
    by (simp add: add.commute add_diff_eq diff_diff_add)
  also have 1:"...
= (\<Sum>a\<in>{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) < 0)} \<union>
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) \<ge> 0)} \<union> 
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) < 0)} \<union> 
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) = 0)}. 
pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (drop (n - i) (map a [0..<n])))) -
real_of_int (fst (rev_m (drop (n - (i-1)) (map a [0..<n])))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i) (map a [0..<n]))))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - (i-1)) (map a [0..<n])))))))"
    using four_cases assms
proof -
  have "n - i +1 \<le> n"
    using assms
    by linarith 
  then show ?thesis
    using four_cases_i
    by presburger 
qed
  also have "...
              =
(\<Sum>a\<in>{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) < 0)} \<union>
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) \<ge> 0)} \<union> 
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) < 0)} \<union> 
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) = 0)}. 
pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (drop (n - i) (map a [0..<n])))) -
real_of_int (fst (rev_m (drop (n - i + 1) (map a [0..<n])))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i) (map a [0..<n]))))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i + 1) (map a [0..<n])))))))"
proof -
{ fix bb :: "nat \<Rightarrow> bool"
have ff1: "n - i + 1 = n + 1 - i"
  by (metis (no_types) Suc_diff_le Suc_eq_plus1 assms(1))
    have "\<forall>n. n + 1 - i = n - (i - 1)"
      using Suc_diff_eq_diff_pred assms(2) by presburger
    then have "(\<Sum>p\<in>{p. (\<forall>na. na \<notin> {..<n} \<longrightarrow> \<not> p na) \<and> 0 < fst (rev_m (drop (n - i + 1) (map p [0..<n]))) \<and> snd (rev_m (drop (n - i + 1) (map p [0..<n]))) < 0} \<union> {p. (\<forall>na. na \<notin> {..<n} \<longrightarrow> \<not> p na) \<and> 0 < fst (rev_m (drop (n - i + 1) (map p [0..<n]))) \<and> 0 \<le> snd (rev_m (drop (n - i + 1) (map p [0..<n])))} \<union> {p. (\<forall>na. na \<notin> {..<n} \<longrightarrow> \<not> p na) \<and> fst (rev_m (drop (n - i + 1) (map p [0..<n]))) = 0 \<and> snd (rev_m (drop (n - i + 1) (map p [0..<n]))) < 0} \<union> {p. (\<forall>na. na \<notin> {..<n} \<longrightarrow> \<not> p na) \<and> fst (rev_m (drop (n - i + 1) (map p [0..<n]))) = 0 \<and> snd (rev_m (drop (n - i + 1) (map p [0..<n]))) = 0}. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (drop (n - i) (map p [0..<n])))) - real_of_int (fst (rev_m (drop (n - (i - 1)) (map p [0..<n])))) + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i) (map p [0..<n]))))) - \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - (i - 1)) (map p [0..<n]))))))) = (\<Sum>p\<in>{p. (\<forall>na. na \<notin> {..<n} \<longrightarrow> \<not> p na) \<and> 0 < fst (rev_m (drop (n - i + 1) (map p [0..<n]))) \<and> snd (rev_m (drop (n - i + 1) (map p [0..<n]))) < 0} \<union> {p. (\<forall>na. na \<notin> {..<n} \<longrightarrow> \<not> p na) \<and> 0 < fst (rev_m (drop (n - i + 1) (map p [0..<n]))) \<and> 0 \<le> snd (rev_m (drop (n - i + 1) (map p [0..<n])))} \<union> {p. (\<forall>na. na \<notin> {..<n} \<longrightarrow> \<not> p na) \<and> fst (rev_m (drop (n - i + 1) (map p [0..<n]))) = 0 \<and> snd (rev_m (drop (n - i + 1) (map p [0..<n]))) < 0} \<union> {p. (\<forall>na. na \<notin> {..<n} \<longrightarrow> \<not> p na) \<and> fst (rev_m (drop (n - i + 1) (map p [0..<n]))) = 0 \<and> snd (rev_m (drop (n - i + 1) (map p [0..<n]))) = 0}. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (drop (n - i) (map p [0..<n])))) - real_of_int (fst (rev_m (drop (n - i + 1) (map p [0..<n])))) + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i) (map p [0..<n]))))) - \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i + 1) (map p [0..<n]))))))) \<or> pmf (local.w_i_pmf n) bb *\<^sub>R (real_of_int (fst (rev_m (drop (n - i) (map bb [0..<n])))) - real_of_int (fst (rev_m (drop (n - (i - 1)) (map bb [0..<n])))) + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i) (map bb [0..<n]))))) - \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - (i - 1)) (map bb [0..<n])))))) = pmf (local.w_i_pmf n) bb *\<^sub>R (real_of_int (fst (rev_m (drop (n - i) (map bb [0..<n])))) - real_of_int (fst (rev_m (drop (n - i + 1) (map bb [0..<n])))) + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i) (map bb [0..<n]))))) - \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i + 1) (map bb [0..<n]))))))"
      using ff1 by presburger }
  then show ?thesis
    by presburger
qed
  finally have  st3:"measure_pmf.expectation (bind_pmf (w_i_pmf n) (\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x))) (\<Delta>_n i)
              =
(\<Sum>a\<in>{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) < 0)} \<union>
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) > 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) \<ge> 0)} \<union> 
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) < 0)} \<union> 
{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) = 0) \<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) = 0)}. 
pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (drop (n - i) (map a [0..<n])))) -
real_of_int (fst (rev_m (drop (n - i + 1) (map a [0..<n])))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i) (map a [0..<n]))))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n - i + 1) (map a [0..<n])))))))"
    by blast
define a1 a2 a3 a4 ex where a1234:
"a1 = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) > 0) 
\<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) < 0)}"
"a2 = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) > 0) 
\<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) \<ge> 0)}"
"a3 = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) = 0) 
\<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) < 0)}"
"a4 = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False) \<and> (fst (rev_m (drop (n-i+1) (map f [0..<n]))) = 0) 
\<and> (snd (rev_m (drop (n-i+1) (map f [0..<n]))) = 0)}"
"ex = (\<lambda>a. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (drop (n-i) (map a [0..<n])))) -
real_of_int (fst (rev_m (drop (n-i+1) (map a [0..<n])))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n-i) (map a [0..<n]))))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (drop (n-i+1) (map a [0..<n])))))))"
  hence fina:"finite a1 \<and> finite a2 \<and> finite a3 \<and> finite a4"
     using a1234 finite_False_func_set by fastforce
  hence st4: "measure_pmf.expectation (bind_pmf (w_i_pmf n) (\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x))) (\<Delta>_n i)
              =
(\<Sum>a\<in> a1 \<union> a2 \<union> a3 \<union> a4. ex a)"
    using st3 a1234 by blast
  hence bigdist:"measure_pmf.expectation (bind_pmf (w_i_pmf n) (\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x))) (\<Delta>_n i) =
(\<Sum>a\<in>a1. ex a)+
(\<Sum>a\<in>a2. ex a)+
(\<Sum>a\<in>a3. ex a)+
(\<Sum>a\<in>a4. ex a)"
    using fina pair_disj_aux_i sum.union_disjoint infinite_Un subs_finite_i a1234
  proof -
have f1: "\<forall>P Pa Pb Pc. (((P \<inter> Pa = {} \<or> Pb \<noteq> a1) \<or> Pa \<noteq> a4) \<or> P \<noteq> a3) \<or> Pc \<noteq> a2"
  using a1234(3) a1234(4) diff_le_self pair_disj_aux_i(6) by fastforce
  have f2: "\<forall>P Pa Pb Pc. (((Pc \<inter> Pb = {} \<or> P \<noteq> a1) \<or> Pa \<noteq> a3) \<or> Pb \<noteq> a4) \<or> Pc \<noteq> a2"
    using a1234(2) a1234(4) by fastforce
  have f3: "\<forall>P Pa Pb Pc. (((Pc \<inter> Pb = {} \<or> P \<noteq> a1) \<or> Pa \<noteq> a4) \<or> Pb \<noteq> a3) \<or> Pc \<noteq> a2"
   using a1234 by fastforce 
  have f4: "\<forall>P Pa Pb Pc. (((P \<inter> Pa = {} \<or> P \<noteq> a1) \<or> Pb \<noteq> a3) \<or> Pa \<noteq> a4) \<or> Pc \<noteq> a2"
    using a1234(1) a1234(4) diff_le_self pair_disj_aux_i(3) by fastforce
  have f5: "\<forall>P Pa Pb Pc. (((P \<inter> Pa = {} \<or> P \<noteq> a1) \<or> Pb \<noteq> a4) \<or> Pa \<noteq> a3) \<or> Pc \<noteq> a2"
    using a1234(1) a1234(3) diff_le_self pair_disj_aux_i(2) by fastforce
  have "a1 \<inter> a2 = {}"
    using a1234(1) a1234(2) diff_le_self pair_disj_aux_i(1) by fastforce
  then show ?thesis
    using f5 f4 f3 f2 f1
    by (metis distrib(4) fina infinite_Un st4 sum.union_disjoint sup_bot.right_neutral)
qed
  have "\<forall>x j. x > 0 \<and> j < x \<longrightarrow> drop j [0..<x] = [j..<x]"
     by simp
   hence "\<forall>x j f. x > 0 \<and> j < x \<longrightarrow> drop j (map f [0..<x]) =(map f [j..<x])"
     by (simp add: drop_map)
   hence drop_map_a:"\<forall> a. (drop (n - i) (map a [0..<n])) = map a [(n-i)..<n] \<and> 
(drop (n - i + 1) (map a [0..<n])) = map a [(n-i +1)..<n]"
     by (simp add: drop_map)
  have consmap:"\<forall>a. map a [(n-i)..<n] = (a (n-i)) # (map a [(n - i + 1)..<n])"
    using assms(1) assms(2) upt_conv_Cons by auto
(*case 1 start here*)  
  have "(\<Sum>a\<in>a1. ex a)
= (\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n-i)..<n]))) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n-i)..<n])))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])))))) "
    using a1234 drop_map_a
    by metis 
  hence c1st1:"(\<Sum>a\<in>a1. ex a)  =
(\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n])))) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n]))))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))"
    using consmap
    by (metis a1234(5) drop_map_a)
  hence "\<And>a. a\<in>a1 \<Longrightarrow>  rev_m ((a (n-i))#(map a [(n - i + 1)..<n])) 
        = (fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1), 
          snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1))" 
  proof - 
    fix a
    assume "a \<in> a1"
    have f:" fst (rev_m (map a [(n - i + 1)..<n])) > 0"
      by (metis (mono_tags, lifting) \<open>a \<in> a1\<close> a1234(1) drop_map_a mem_Collect_eq)
   have s:"snd (rev_m (map a [(n - i + 1)..<n])) < 0"
      by (metis (mono_tags, lifting) \<open>a \<in> a1\<close> a1234(1) drop_map_a mem_Collect_eq)
  have "  rev_m (False#(map a [(n - i + 1)..<n])) 
        = (if fst (rev_m (map a [(n - i + 1)..<n])) > snd (rev_m (map a [(n - i + 1)..<n])) 
        \<and> snd (rev_m (map a [(n - i + 1)..<n])) = 0
        then (fst (rev_m (map a [(n - i + 1)..<n])) - 1, 0)
        else 
          (if fst (rev_m (map a [(n - i + 1)..<n])) = 0
          then (0, snd (rev_m (map a [(n - i + 1)..<n])) - 1)
          else (fst (rev_m (map a [(n - i + 1)..<n])) - 1, snd (rev_m (map a [(n - i + 1)..<n])) - 1)))"
    using rev_m.simps(3) by blast
  hence "rev_m (False#(map a [(n - i + 1)..<n])) 
        = (if fst (rev_m (map a [(n - i + 1)..<n])) = 0
          then (0, snd (rev_m (map a [(n - i + 1)..<n])) - 1)
          else (fst (rev_m (map a [(n - i + 1)..<n])) - 1, snd (rev_m (map a [(n - i + 1)..<n])) - 1))"
    using a1234
    by (smt s)
hence falsecase:"rev_m (False#(map a [(n - i + 1)..<n])) 
        = (fst (rev_m (map a [(n - i + 1)..<n])) - 1, snd (rev_m (map a [(n - i + 1)..<n])) - 1)"
  using a1234
  by (smt f)
  thus "rev_m ((a (n-i))#(map a [(n - i + 1)..<n])) 
        = (fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1), 
          snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1))"
    by simp
qed
  hence "\<And>a. a\<in>a1 \<Longrightarrow>  fst (rev_m ((a (n-i))#(map a [(n - i + 1)..<n])))
        = fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1) \<and>
          snd (rev_m ((a (n-i))#(map a [(n - i + 1)..<n]))) =
          snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)"
    by simp 
  have c1unfold:"(\<Sum>a\<in>a1. ex a)  =
(\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))"
    using c1st1
  proof -
have f1: "\<And>p. p \<notin> a1 \<or> pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (p (n - i) # map p [n - i + 1..<n]))) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n]))))) = pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])) + (if p (n - i) then 1 else - 1))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))"
  using \<open>\<And>aa. aa \<in> a1 \<Longrightarrow> fst (rev_m (aa (n - i) # map aa [n - i + 1..<n])) = fst (rev_m (map aa [n - i + 1..<n])) + (if aa (n - i) then 1 else - 1) \<and> snd (rev_m (aa (n - i) # map aa [n - i + 1..<n])) = snd (rev_m (map aa [n - i + 1..<n])) + (if aa (n - i) then 1 else - 1)\<close> by presburger
  have "\<forall>P f fa. \<exists>p. ((p::nat \<Rightarrow> bool) \<in> P \<or> sum f P = sum fa P) \<and> ((f p::real) \<noteq> fa p \<or> sum f P = sum fa P)"
    by (metis (full_types, lifting) sum.cong)
  then obtain bb :: "(nat \<Rightarrow> bool) set \<Rightarrow> ((nat \<Rightarrow> bool) \<Rightarrow> real) \<Rightarrow> ((nat \<Rightarrow> bool) \<Rightarrow> real) \<Rightarrow> nat \<Rightarrow> bool" where
    f2: "\<And>P f fa. (bb P f fa \<in> P \<or> sum f P = sum fa P) \<and> (f (bb P f fa) \<noteq> fa (bb P f fa) \<or> sum f P = sum fa P)"
    by moura
  then have "\<And>P. bb P (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (p (n - i) # map p [n - i + 1..<n]))) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])) + (if p (n - i) then 1 else - 1))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) \<notin> a1 \<or> (\<Sum>p\<in>P. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])) + (if p (n - i) then 1 else - 1))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) = (\<Sum>p\<in>P. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (p (n - i) # map p [n - i + 1..<n]))) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n]))))))"
    using f1
    by (metis (no_types, lifting))
  then have "(\<Sum>p\<in>a1. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])) + (if p (n - i) then 1 else - 1))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) = (\<Sum>p\<in>a1. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (p (n - i) # map p [n - i + 1..<n]))) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n]))))))"
    using f2 by (metis (no_types, lifting))
  then show ?thesis
    using \<open>sum ex a1 = (\<Sum>a\<in>a1. pmf (local.w_i_pmf n) a *\<^sub>R (real_of_int (fst (rev_m (a (n - i) # map a [n - i + 1..<n]))) - real_of_int (fst (rev_m (map a [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (a (n - i) # map a [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map a [n - i + 1..<n]))))))\<close> by presburger
qed
  have roi_minus_dist:"\<And>a b. real_of_int a - real_of_int b = real_of_int (a - b)"
    by simp
  hence "(\<Sum>a\<in>a1. ex a)  =
(\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int 
((fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)) -
(fst (rev_m (map a [(n - i + 1)..<n])))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))"
    using c1unfold by presburger
hence tmp:"(\<Sum>a\<in>a1. ex a)  =
(\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int 
((fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)) -
(fst (rev_m (map a [(n - i + 1)..<n])))))
 + (\<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])))))))"
  by (smt sum.cong)
have "\<And>a. \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)))
 - \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))= 
\<alpha>* real_of_int ((min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1))) - 
(min 0 (snd (rev_m (map a [(n - i + 1)..<n])))))"
  by (simp add: right_diff_distrib)
   hence "(\<Sum>a\<in>a1. ex a)  =
(\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int 
((fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)) -
(fst (rev_m (map a [(n - i + 1)..<n])))))
 + \<alpha> * (real_of_int ((min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1))) - 
(min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))))"
     using tmp
     by auto 
hence "(\<Sum>a\<in>a1. ex a)  =
(\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (if a (n - i) then 1 else -1))
 + \<alpha> * 
(real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)) - 
(min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))))"
  by auto
hence "(\<Sum>a\<in>a1. ex a)  =
(\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (if a (n - i) then 1 else -1))
 + \<alpha> * 
(real_of_int (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1) - 
(snd (rev_m (map a [(n - i + 1)..<n])))))))"
  using a1234
  by (smt drop_map_a mem_Collect_eq real_vector.scale_cancel_left sum.cong) 
hence tmp:"(\<Sum>a\<in>a1. ex a)  =
(\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
(1 * real_of_int (if a (n - i) then 1 else -1)
 + 
\<alpha> * real_of_int (if a (n - i) then 1 else -1)))"
  using a1234
  by (smt sum.cong)
  have "\<And>a::real. (1 * a + \<alpha> * a) = (1+\<alpha>) * a"
    using Real_Vector_Spaces.scaleR_left_distrib
    by (simp add: semiring_normalization_rules(3))
hence "(\<Sum>a\<in>a1. ex a)  =
(\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
((1 + \<alpha>)* real_of_int (if a (n - i) then 1 else -1)))"
  using tmp by auto            
hence "(\<Sum>a\<in>a1. ex a)  =
(\<Sum>a \<in> a1. (1 + \<alpha>) * pmf (w_i_pmf n) a *\<^sub>R
(real_of_int (if a (n - i) then 1 else -1)))"
  by (metis (no_types, lifting) mult_scaleR_right sum.cong)
hence "(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * (\<Sum>a \<in> a1. pmf (w_i_pmf n) a *\<^sub>R
(real_of_int (if a (n - i) then 1 else -1)))"
  by (simp add: sum_distrib_left)
  hence tmp:"(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * (\<Sum>a \<in> a1. pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a *
(if a (n - i) then 1 else -1))"
  using w_i_pmf_def
  by (smt of_int_1 of_int_minus real_scaleR_def sum.cong) 
hence tmp:"(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * (\<Sum>a \<in> a1. (\<Prod>x\<in>{..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *
(if a (n - i) then 1 else -1))"
  using w_i_pmf_def finite_False_func_set pmf_Pi'
proof - 
  have 1:"finite {..<n}" by simp
  have "\<And>a. a \<in> a1 \<Longrightarrow>  (\<And>x. x \<notin> {..<n} \<Longrightarrow> a x = False)"
    by (simp add: a1234(1) a1234(2))
  hence "\<And>a. a \<in> a1 \<Longrightarrow> pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a =
        (\<Prod>x\<in>{..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)) x) (a x))"
    using 1 pmf_Pi'
    by (smt prod.cong)
  thus ?thesis
    using tmp by auto
qed
hence "(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * (\<Sum>a \<in> a1. (\<Prod>x\<in>{..<n-i} \<union> {n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *
(if a (n - i) then 1 else -1))"
proof -
  have "{..<n} = {..<n-i} \<union> {n-i} \<union> {n-i<..<n}"
    using assms(1) assms(2) ivl_disj_un_singleton(2) by force
  thus ?thesis
    using tmp
    by simp
qed
hence tmp:"(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * (\<Sum>a \<in> a1. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n} \<union> {n-i}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *
(if a (n - i) then 1 else -1))"
  by simp
hence "(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * (\<Sum>a \<in> a1. ((\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(\<Prod>x\<in> {n-i}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  *
(if a (n - i) then 1 else -1))"
proof - 
  have "finite ({..<n-i} \<union> {n-i<..<n}) \<and> (finite {n-i}) \<and> (({..<n-i} \<union> {n-i<..<n}) \<inter> {n-i} = {})"
    by simp
  thus ?thesis
    using tmp Groups_Big.comm_monoid_mult_class.prod.union_disjoint
    by (metis (no_types, lifting) sum.cong)
qed
  hence "(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * (\<Sum>a \<in> a1. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1))"
  by simp
  hence tmp:"(\<Sum>a\<in>a1 . ex a)  =
(1 + \<alpha>) * (\<Sum>a \<in> a1 \<inter> ({f. f (n-i) \<or> \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1))"
  by auto
  hence "(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * (\<Sum>a \<in> a1  \<inter> ({f. f (n-i)} \<union> {f. \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1))"
  proof - 
    have "{f. f (n-i) \<or> \<not> f (n-i)} = {f. f (n-i)} \<union> {f. \<not> f (n-i)}"
      by auto
    thus ?thesis using tmp
      by auto
  qed
  hence tmp:"(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * (\<Sum>a \<in> (a1 \<inter> {f. f (n-i)}) \<union> (a1 \<inter> {f. \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1))"
    by (simp add: inf_sup_distrib1)
 hence "(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * ((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1))
+ (\<Sum>a \<in> a1 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1)))"
 proof - 
   have 1:"(a1 \<inter> {f. f (n-i)}) \<inter> (a1 \<inter> {f. \<not> f (n-i)}) = {}"
     by blast
   have "finite a1" using a1234
     using fina by linarith
   hence "finite (a1 \<inter> {f. f (n-i)}) \<and> finite (a1 \<inter> {f. \<not> f (n-i)})"
     by auto
   thus ?thesis
     using tmp infinite_Un 1
     by (simp add: sum.union_disjoint)
 qed
hence tmp:"(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * ((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) True) * 1)
+ (\<Sum>a \<in> a1 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) False) * -1))"
  by auto
hence "(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * ((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
((1-\<epsilon>)/2) * 1)
+ (\<Sum>a \<in> a1 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
((1+\<epsilon>)/2) * (-1)))"
proof - 
  have "(((1-\<epsilon>)/2) \<ge>0) \<and> ((1-\<epsilon>)/2) \<le> 1"
    using ep_gt_0 ep_le_1 by simp
  thus ?thesis using pmf_bernoulli_True pmf_bernoulli_False honest_slot tmp
    by (simp add: honest_slot)
qed
hence tmp:"(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * ((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
((1-\<epsilon>)/2)
+ (\<Sum>a \<in> a1 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * (((1+\<epsilon>)/2) * (-1))))"
  by (simp add: sum_distrib_right)
hence "(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * ((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
((1-\<epsilon>)/2)
+ (\<Sum>a \<in> a1 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * (- (1+\<epsilon>)/2)))"
  by (simp add: tmp mult.commute sum_distrib_right)
 hence tmp:"(\<Sum>a\<in>a1. ex a)  =
(1 + \<alpha>) * ((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
((1-\<epsilon>)/2)
+ (\<Sum>a \<in> a1 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) * (- (1+\<epsilon>)/2))"
   by (simp add: sum_distrib_right)
  have case1eq:"(\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
=  (\<Sum>a \<in> a1 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) "
  proof - 
    define h where h:"h = (\<lambda>f x. (if x = n-i then \<not> f x else f x))"
    have "(\<forall>x\<in>(a1 \<inter> {f. f (n-i)}). \<forall>y\<in>(a1 \<inter> {f. f (n-i)}). h x = h y \<longrightarrow> x = y)"
    proof (rule ccontr)
      assume "\<not> (\<forall>x\<in>(a1 \<inter> {f. f (n-i)}). \<forall>y\<in>(a1 \<inter> {f. f (n-i)}). h x = h y \<longrightarrow> x = y)"
      then obtain x y where "x\<in>(a1 \<inter> {f. f (n-i)})" "y\<in>(a1 \<inter> {f. f (n-i)})" "h x = h y" "x \<noteq> y"
        by blast
       obtain j where "x j \<noteq> y j"
         using \<open>x \<noteq> y\<close> 
         by blast
       have "(h x) j = (h y) j"
         using \<open>h x = h y\<close>
         by simp
       hence "(\<lambda>f x. (if x = n-i then \<not> f x else f x)) x j = (\<lambda>f x. (if x = n-i then \<not> f x else f x)) y j"
         using h by blast
       hence "(if j = n-i then \<not> x j else x j) = (if j = n-i then \<not> y j else y j)"
         by simp
       hence "x j = y j"
         by presburger
       thus "False"
         using \<open>x j \<noteq> y j\<close> by auto
     qed
     hence "inj_on h (a1 \<inter> {f. f (n-i)})"
       by (meson inj_onI)
     have "h ` (a1 \<inter> {f. f (n-i)}) = a1 \<inter> {f. \<not> f (n-i)}" (*may be worth having auxillary*)
     proof - 
       have "\<And>a. a \<in> h ` (a1 \<inter> {f. f (n-i)}) \<Longrightarrow> a \<in> a1 \<inter> {f. \<not> f (n-i)}"
          proof -
           fix a
           assume "a \<in> h ` (a1 \<inter> {f. f (n-i)}) "
           then obtain b where "b \<in> (a1 \<inter> {f. f (n-i)})" "a = h b"
             by blast
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> b x = False) \<and> (fst (rev_m (drop (n-i+1) (map b [0..<n]))) > 0)
\<and> (snd (rev_m (drop (n-i+1) (map b [0..<n]))) < 0)) \<and> b (n-i)"
             using a1234(1) by blast
           hence condi1:"\<forall>x. (x \<notin> {..<n} \<longrightarrow> h b x = False)"
             using h by simp
           have "n - i + 1 \<le> n"
             using assms
             by linarith
           have ngt:"n - i + 1 > n - i" by simp
           have dropsimp:"(drop (n-i+1) (map (h b) [0..<n])) = (map (h b) [(n-i+1)..<n])"
             using drop_map_a by blast
           have "\<forall>x. n-i+1 \<le> x \<and> x < n \<longrightarrow> h b x = b x"
             using h by auto
           hence "\<And>x. x \<in> set [(n-i+1)..<n] \<Longrightarrow> h b x =b x"
             by simp
           hence "(map (h b) [(n-i+1)..<n]) =  (map b [(n-i+1)..<n])"
             using ngt list.map_cong0
             by blast
           hence "(drop (n-i+1) (map (h b) [0..<n])) = (drop (n-i+1) (map b [0..<n]))"
             using dropsimp
             by (simp add: drop_map)
           hence condi2:"(fst (rev_m (drop (n-i+1) (map (h b) [0..<n]))) > 0) 
\<and> (snd (rev_m (drop (n-i+1) (map (h b) [0..<n]))) < 0)"
             using tmp
             by simp
           have "\<not> h b (n-i)"
             using tmp h
             by auto
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> h b x = False) \<and> (fst (rev_m (drop (n-i+1) (map (h b) [0..<n]))) > 0) 
                    \<and> (snd (rev_m (drop (n-i+1) (map (h b) [0..<n]))) < 0)) \<and> \<not> h b (n-i)"
             using h condi1 condi2 by blast
           hence "h b \<in> a1 \<inter> {f. \<not> f (n-i)}"
             using a1234(1) by blast
           thus "a \<in> a1 \<inter> {f. \<not> f (n-i)}"
             using \<open>a = h b\<close> by simp
         qed
       hence  "h ` (a1 \<inter> {f. f (n-i)}) \<subseteq> a1 \<inter> {f. \<not> f (n-i)}"
         by blast
       have "\<And>a. a \<in> a1 \<inter> {f. \<not> f (n-i)} \<Longrightarrow> a \<in> h ` (a1 \<inter> {f. f (n-i)})"
       proof - 
         fix a assume "a \<in> a1 \<inter> {f. \<not> f (n-i)}"
         hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> a x = False) \<and> (fst (rev_m (drop (n-i+1) (map a [0..<n]))) > 0)
            \<and> (snd (rev_m (drop (n-i+1) (map a [0..<n]))) < 0)) \<and> \<not> a (n-i)"
           using a1234(1) by blast
         obtain b where "b = h a"
           by simp
         hence condi1:"\<forall>x. (x \<notin> {..<n} \<longrightarrow> h a x = False)"
             using h tmp
             using assms(1) assms(2) diff_diff_cancel by auto 
           have "n - i + 1 \<le> n"
             using assms
             by linarith
           have ngt:"n - i + 1 > n - i" by simp
           have dropsimp:"(drop (n-i+1) (map (h a) [0..<n])) = (map (h a) [(n-i+1)..<n])"
             using drop_map_a by blast
           have "\<forall>x. n-i+1 \<le> x \<and> x < n \<longrightarrow> h a x = a x"
             using h by auto
           hence "\<And>x. x \<in> set [(n-i+1)..<n] \<Longrightarrow> h a x =a x"
             by simp
           hence "(map (h a) [(n-i+1)..<n]) =  (map a [(n-i+1)..<n])"
             using ngt list.map_cong0
             by blast
           hence "(drop (n-i+1) (map (h a) [0..<n])) = (drop (n-i+1) (map a [0..<n]))"
             using dropsimp
             by (simp add: drop_map)
           hence condi2:"(fst (rev_m (drop (n-i+1) (map (h a) [0..<n]))) > 0) 
\<and> (snd (rev_m (drop (n-i+1) (map (h a) [0..<n]))) < 0)"
             using tmp
             by simp
           have "h a (n-i)"
             using tmp h
             by auto
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> b x = False) \<and> (fst (rev_m (drop (n-i+1) (map b [0..<n]))) > 0) 
                    \<and> (snd (rev_m (drop (n-i+1) (map b [0..<n]))) < 0)) \<and> b (n-i)"
             using h condi1 condi2 \<open>b = h a\<close> by simp
           hence "b \<in> a1 \<inter> {f. f (n-i)}"
             using a1234(1) by blast
           have "h b = a"
           proof - 
             have "h b = h (h a)"
               using \<open>b = h a\<close> by simp
             hence "h (h a) = (\<lambda>f x. (if x = n-i then \<not> f x else f x)) ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a)"
               by (simp add: h)
             hence "h (h a) = 
(\<lambda>x. (if x = n-i then \<not> ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a) x else ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a) x)) " by simp
             hence "h (h a) = 
(\<lambda>x. (if x = n-i then \<not> (\<lambda>x. (if x = n-i then \<not> a x else a x)) x else (\<lambda>x. (if x = n-i then \<not> a x else a x)) x))" by simp
             hence "\<And>x. h (h a) x = 
(if x = n-i then \<not> (if x = n-i then \<not> a x else a x) else (if x = n-i then \<not> a x else a x))"
               by simp
             hence "\<And>x. h (h a) x = a x"
               by simp
              hence "h (h a) = a"
                by blast
              thus ?thesis
                by (simp add: \<open>b = h a\<close>)
            qed
              hence "(h b) \<in> h ` (a1 \<inter> {f. f (n-i)})"
                using \<open>b \<in> a1 \<inter> {f. f (n - i)}\<close> by blast
              thus "a \<in> h ` (a1 \<inter> {f. f (n-i)})"
                using \<open>h b = a\<close> by simp
            qed
            thus ?thesis
              using \<open>h ` (a1 \<inter> {f. f (n - i)}) \<subseteq> a1 \<inter> {f. \<not> f (n - i)}\<close> by blast
          qed
          hence "bij_betw h  (a1 \<inter> {f. f (n - i)}) (a1 \<inter> {f. \<not> f (n - i)})"
            by (simp add: \<open>inj_on h (a1 \<inter> {f. f (n - i)})\<close> bij_betw_def)
          hence "(\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (h a)) 
              =  sum (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (a1 \<inter> {f. \<not> f (n-i)})"
            using sum.reindex_bij_betw by simp
            hence "(\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (h a)) 
              =  (\<Sum>a \<in> a1 \<inter> { f.\<not> f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) a)"
              by blast
        hence tmp:"(\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x))) 
              =  (\<Sum>a \<in> a1 \<inter> { f.\<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          by simp
        have "\<And>a. a \<in> a1 \<inter> {f. f (n-i)} \<Longrightarrow> 
(\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x)) = 
(\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))"
          by (smt Un_iff greaterThanLessThan_iff h lessThan_iff less_irrefl prod.cong)
        hence "(\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x))) = 
              (\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          by auto
        thus "(\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
              =  (\<Sum>a \<in> a1 \<inter> { f.\<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          using tmp
          by linarith
      qed      
      hence "(\<Sum>a\<in>a1. ex a)  =
            (1 + \<alpha>) * ((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
            ((1-\<epsilon>)/2)
            + (\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) * (- (1+\<epsilon>)/2))"
        using tmp
        by (simp add: case1eq tmp)
      hence "(\<Sum>a\<in>a1. ex a)  =
            (1 + \<alpha>) * ((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
            (((1-\<epsilon>)/2) +  (- (1+\<epsilon>)/2)))"
        by (simp add: linordered_field_class.sign_simps(36))
      hence "(\<Sum>a\<in>a1. ex a)  =
            (1 + \<alpha>) * ((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
            (-\<epsilon>))"
      proof -
        have "\<forall>r ra. - (ra::real) + (r + ra) * inverse 2 = (r + - ra) * inverse 2"
          by auto
        then have "\<forall>r ra. ((ra::real) + - r) * inverse 2 + - (ra + r) * inverse 2 = - r"
          by (metis (no_types) add_diff_cancel_right' add_uminus_conv_diff mult_minus_left)
        then show ?thesis
          using \<open>sum ex a1 = (1 + \<alpha>) * ((\<Sum>a\<in>a1 \<inter> {f. f (n - i)}. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * ((1 - \<epsilon>) / 2 + - (1 + \<epsilon>) / 2))\<close> by fastforce
      qed
   hence "(\<Sum>a\<in>a1. ex a)  =
            (1 + \<alpha>) * (
((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) + 
(\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))))/2
  * (-\<epsilon>))"
     by simp
hence tmp:"(\<Sum>a\<in>a1. ex a)  =
            (1 + \<alpha>) * (
((\<Sum>a \<in> a1 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) + 
(\<Sum>a \<in> a1 \<inter> {f. \<not>f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))))/2
  * (-\<epsilon>))" using case1eq
  by simp
hence tmp:"(\<Sum>a\<in>a1. ex a)  =
            (1 + \<alpha>) * (
(\<Sum>a \<in> (a1 \<inter> {f. f (n-i)}) \<union> (a1 \<inter> {f. \<not>f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))/2
  * (-\<epsilon>))" using case1eq pair_disj_aux_i sum.union_disjoint infinite_Un
proof - 
  have fin:"finite (a1 \<inter> {f. f (n-i)}) \<and> finite (a1 \<inter> {f. \<not>f (n-i)})"
    using Forkable_Strings_Are_Rare.finite_False_func_set a1234(1) ep_gt_0 ep_le_1 by auto
  have "(a1 \<inter> {f. f (n-i)}) \<inter> (a1 \<inter> {f. \<not>f (n-i)}) = {}"
    by auto
  thus ?thesis  using case1eq pair_disj_aux_i sum.union_disjoint infinite_Un tmp fin
    by smt
qed
hence tmp:"(\<Sum>a\<in>a1. ex a)  =
            (1 + \<alpha>) * (
(\<Sum>a \<in> a1. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))/2
  * (-\<epsilon>))"
proof - 
  have "(a1 \<inter> {f. f (n-i)}) \<union> (a1 \<inter> {f. \<not>f (n-i)}) = a1"
    by blast
  thus ?thesis using tmp by simp
qed
  define A where A:"A = (\<Sum>a \<in> a1. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))" 
  hence simplifiedcase1:"(\<Sum>a\<in>a1. ex a)  =
            (1 + \<alpha>) * A/2 * (-\<epsilon>)"
    using tmp by simp
(*start case 2 here*)
 have "(\<Sum>a\<in>a2. ex a) 
= (\<Sum>a \<in> a2. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n-i)..<n]))) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n-i)..<n])))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])))))) "
    using a1234 drop_map_a
    by metis 
  hence c2st1:"(\<Sum>a\<in>a2. ex a)  =
(\<Sum>a \<in> a2. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n])))) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n]))))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))"
    using consmap
    by (metis a1234(5) drop_map_a)
  hence "\<And>a. a\<in>a2 \<Longrightarrow>          
fst (rev_m (map a [(n - i)..<n])) = fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)
\<and>
(min 0 (snd (rev_m (map a [(n - i)..<n])))) = (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))" 
  proof - 
    fix a
    assume "a \<in> a2"
    have f:" fst (rev_m (map a [(n - i + 1)..<n])) > 0"
      by (metis (mono_tags, lifting) \<open>a \<in> a2\<close> a1234(2) drop_map_a mem_Collect_eq)
   have s:"snd (rev_m (map a [(n - i + 1)..<n])) \<ge> 0"
      by (metis (mono_tags, lifting) \<open>a \<in> a2\<close> a1234(2) drop_map_a mem_Collect_eq)
    have "  rev_m (False#(map a [(n - i + 1)..<n]))
        = (if fst (rev_m (map a [(n - i + 1)..<n])) > snd (rev_m (map a [(n - i + 1)..<n])) 
        \<and> snd (rev_m (map a [(n - i + 1)..<n])) = 0
        then (fst (rev_m (map a [(n - i + 1)..<n])) - 1, 0)
        else 
          (if fst (rev_m (map a [(n - i + 1)..<n])) = 0
          then (0, snd (rev_m (map a [(n - i + 1)..<n])) - 1)
          else (fst (rev_m (map a [(n - i + 1)..<n])) - 1, snd (rev_m (map a [(n - i + 1)..<n])) - 1)))"
    using rev_m.simps(3) by blast
  hence "rev_m (False#(map a [(n - i + 1)..<n]))
        = (if fst (rev_m (map a [(n - i + 1)..<n])) > snd (rev_m (map a [(n - i + 1)..<n])) 
        \<and> snd (rev_m (map a [(n - i + 1)..<n])) = 0
        then (fst (rev_m (map a [(n - i + 1)..<n])) - 1, 0)
        else 
          (fst (rev_m (map a [(n - i + 1)..<n])) - 1, snd (rev_m (map a [(n - i + 1)..<n])) - 1))"
    using a1234
    by (smt f s)
  hence falsecase: "fst (rev_m (False#(map a [(n - i + 1)..<n]))) = fst (rev_m (map a [(n - i + 1)..<n])) - 1
\<and>
(min 0 (snd (rev_m (False#(map a [(n - i + 1)..<n]))))) = (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))" 
    by (smt f fst_conv s snd_conv)
    have "  rev_m (True#(map a [(n - i + 1)..<n])) 
= (fst (rev_m (map a [(n - i + 1)..<n])) + 1, snd (rev_m (map a [(n - i + 1)..<n])) + 1)"
      using rev_m.simps(2) by blast
    hence "fst (rev_m (True#(map a [(n - i + 1)..<n]))) = fst (rev_m (map a [(n - i + 1)..<n])) + 1 \<and>
(min 0 (snd (rev_m (True#(map a [(n - i + 1)..<n]))))) = (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))"
      using s by auto
  thus "fst (rev_m (map a [(n - i)..<n])) = fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)
\<and>
(min 0 (snd (rev_m (map a [(n - i)..<n])))) = (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))" 
    using falsecase
    by (simp add: consmap) 
qed
  hence "(\<Sum>a\<in>a2. ex a)  =
(\<Sum>a \<in> a2. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))"
    using c2st1
    by (metis (no_types, lifting) consmap sum.cong)
hence c2unfold:"(\<Sum>a\<in>a2. ex a)  =
(\<Sum>a \<in> a2. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))))"
  by auto
  hence "(\<Sum>a\<in>a2. ex a)  =
(\<Sum>a \<in> a2. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int 
((fst (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)) -
(fst (rev_m (map a [(n - i + 1)..<n])))))))"
    using roi_minus_dist
    by presburger
hence "(\<Sum>a\<in>a2. ex a)  =
(\<Sum>a \<in> a2. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (if a (n - i) then 1 else -1))))"
  by auto
  hence tmp:"(\<Sum>a\<in>a2. ex a)  =
 (\<Sum>a \<in> a2. pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a *
(if a (n - i) then 1 else -1))"
    using w_i_pmf_def
  by (smt of_int_1 of_int_minus real_scaleR_def sum.cong) 
hence tmp:"(\<Sum>a\<in>a2. ex a)  =  (\<Sum>a \<in> a2. (\<Prod>x\<in>{..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *
(if a (n - i) then 1 else -1))"
  using w_i_pmf_def finite_False_func_set pmf_Pi'
proof - 
  have 1:"finite {..<n}" by simp
  have "\<And>a. a \<in> a2 \<Longrightarrow>  (\<And>x. x \<notin> {..<n} \<Longrightarrow> a x = False)"
    by (simp add: a1234(1) a1234(2))
  hence "\<And>a. a \<in> a2 \<Longrightarrow> pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a =
        (\<Prod>x\<in>{..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)) x) (a x))"
    using 1 pmf_Pi'
    by (smt prod.cong)
  thus ?thesis
    using tmp by auto
qed
hence "(\<Sum>a\<in>a2. ex a)  = (\<Sum>a \<in> a2. (\<Prod>x\<in>{..<n-i} \<union> {n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *
(if a (n - i) then 1 else -1))"
proof -
  have "{..<n} = {..<n-i} \<union> {n-i} \<union> {n-i<..<n}"
    using assms(1) assms(2) ivl_disj_un_singleton(2) by force
  thus ?thesis
    using tmp
    by simp
qed
hence tmp:"(\<Sum>a\<in>a2. ex a)  =
(\<Sum>a \<in> a2. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n} \<union> {n-i}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) *
(if a (n - i) then 1 else -1))"
  by simp
hence "(\<Sum>a\<in>a2. ex a)  = (\<Sum>a \<in> a2. ((\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(\<Prod>x\<in> {n-i}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  *
(if a (n - i) then 1 else -1))"
proof - 
  have "finite ({..<n-i} \<union> {n-i<..<n}) \<and> (finite {n-i}) \<and> (({..<n-i} \<union> {n-i<..<n}) \<inter> {n-i} = {})"
    by simp
  thus ?thesis
    using tmp Groups_Big.comm_monoid_mult_class.prod.union_disjoint
    by (metis (no_types, lifting) sum.cong)
qed
  hence tmp:"(\<Sum>a\<in>a2 . ex a)  =
 (\<Sum>a \<in> a2 \<inter> ({f. f (n-i) \<or> \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1))"
  by auto
  hence "(\<Sum>a\<in>a2. ex a)  =
(\<Sum>a \<in> a2  \<inter> ({f. f (n-i)} \<union> {f. \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1))"
  proof - 
    have "{f. f (n-i) \<or> \<not> f (n-i)} = {f. f (n-i)} \<union> {f. \<not> f (n-i)}"
      by auto
    thus ?thesis using tmp
      by auto
  qed
  hence tmp:"(\<Sum>a\<in>a2. ex a)  =
(\<Sum>a \<in> (a2 \<inter> {f. f (n-i)}) \<union> (a2 \<inter> {f. \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1))"
    by (simp add: inf_sup_distrib1)
 hence "(\<Sum>a\<in>a2. ex a)  =
 ((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1))
+ (\<Sum>a \<in> a2 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  *
(if a (n - i) then 1 else -1)))"
 proof - 
   have 1:"(a2 \<inter> {f. f (n-i)}) \<inter> (a2 \<inter> {f. \<not> f (n-i)}) = {}"
     by blast
   have "finite a2" using a1234
     using fina by linarith
   hence "finite (a2 \<inter> {f. f (n-i)}) \<and> finite (a2 \<inter> {f. \<not> f (n-i)})"
     by auto
   thus ?thesis
     using tmp infinite_Un 1
     by (simp add: sum.union_disjoint)
 qed
hence tmp:"(\<Sum>a\<in>a2. ex a)  =
((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) True) * 1)
+ (\<Sum>a \<in> a2 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) False) * -1))"
  by auto
hence "(\<Sum>a\<in>a2. ex a)  =
((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
((1-\<epsilon>)/2) * 1)
+ (\<Sum>a \<in> a2 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
((1+\<epsilon>)/2) * (-1)))"
proof - 
  have "(((1-\<epsilon>)/2) \<ge>0) \<and> ((1-\<epsilon>)/2) \<le> 1"
    using ep_gt_0 ep_le_1 by simp
  thus ?thesis using pmf_bernoulli_True pmf_bernoulli_False honest_slot tmp
    by (simp add: honest_slot)
qed
hence tmp:"(\<Sum>a\<in>a2. ex a)  =
((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
((1-\<epsilon>)/2)
+ (\<Sum>a \<in> a2 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * (((1+\<epsilon>)/2) * (-1))))"
  by (simp add: sum_distrib_right)
hence "(\<Sum>a\<in>a2. ex a)  =
((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
((1-\<epsilon>)/2)
+ (\<Sum>a \<in> a2 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * (- (1+\<epsilon>)/2)))"
  by (simp add: tmp mult.commute sum_distrib_right)
 hence tmp:"(\<Sum>a\<in>a2. ex a)  =
((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
((1-\<epsilon>)/2)
+ (\<Sum>a \<in> a2 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) * (- (1+\<epsilon>)/2))"
   by (simp add: sum_distrib_right)
  have case2eq:"(\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
=  (\<Sum>a \<in> a2 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) "
  proof - 
    define h where h:"h = (\<lambda>f x. (if x = n-i then \<not> f x else f x))"
    have "(\<forall>x\<in>(a2 \<inter> {f. f (n-i)}). \<forall>y\<in>(a2 \<inter> {f. f (n-i)}). h x = h y \<longrightarrow> x = y)"
    proof (rule ccontr)
      assume "\<not> (\<forall>x\<in>(a2 \<inter> {f. f (n-i)}). \<forall>y\<in>(a2 \<inter> {f. f (n-i)}). h x = h y \<longrightarrow> x = y)"
      then obtain x y where "x\<in>(a2 \<inter> {f. f (n-i)})" "y\<in>(a2 \<inter> {f. f (n-i)})" "h x = h y" "x \<noteq> y"
        by blast
       obtain j where "x j \<noteq> y j"
         using \<open>x \<noteq> y\<close> 
         by blast
       have "(h x) j = (h y) j"
         using \<open>h x = h y\<close>
         by simp
       hence "(\<lambda>f x. (if x = n-i then \<not> f x else f x)) x j = (\<lambda>f x. (if x = n-i then \<not> f x else f x)) y j"
         using h by blast
       hence "(if j = n-i then \<not> x j else x j) = (if j = n-i then \<not> y j else y j)"
         by simp
       hence "x j = y j"
         by presburger
       thus "False"
         using \<open>x j \<noteq> y j\<close> by auto
     qed
     hence "inj_on h (a2 \<inter> {f. f (n-i)})"
       by (meson inj_onI)
     have "h ` (a2 \<inter> {f. f (n-i)}) = a2 \<inter> {f. \<not> f (n-i)}" (*may be worth having auxillary*)
     proof - 
       have "\<And>a. a \<in> h ` (a2 \<inter> {f. f (n-i)}) \<Longrightarrow> a \<in> a2 \<inter> {f. \<not> f (n-i)}"
          proof -
           fix a
           assume "a \<in> h ` (a2 \<inter> {f. f (n-i)}) "
           then obtain b where "b \<in> (a2 \<inter> {f. f (n-i)})" "a = h b"
             by blast
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> b x = False) \<and> (fst (rev_m (drop (n-i+1) (map b [0..<n]))) > 0)
\<and> (snd (rev_m (drop (n-i+1) (map b [0..<n]))) \<ge> 0)) \<and> b (n-i)"
             using a1234(2) by blast
           hence condi1:"\<forall>x. (x \<notin> {..<n} \<longrightarrow> h b x = False)"
             using h by simp
           have "n - i + 1 \<le> n"
             using assms
             by linarith
           have ngt:"n - i + 1 > n - i" by simp
           have dropsimp:"(drop (n-i+1) (map (h b) [0..<n])) = (map (h b) [(n-i+1)..<n])"
             using drop_map_a by blast
           have "\<forall>x. n-i+1 \<le> x \<and> x < n \<longrightarrow> h b x = b x"
             using h by auto
           hence "\<And>x. x \<in> set [(n-i+1)..<n] \<Longrightarrow> h b x =b x"
             by simp
           hence "(map (h b) [(n-i+1)..<n]) =  (map b [(n-i+1)..<n])"
             using ngt list.map_cong0
             by blast
           hence "(drop (n-i+1) (map (h b) [0..<n])) = (drop (n-i+1) (map b [0..<n]))"
             using dropsimp
             by (simp add: drop_map)
           hence condi2:"(fst (rev_m (drop (n-i+1) (map (h b) [0..<n]))) > 0) 
\<and> (snd (rev_m (drop (n-i+1) (map (h b) [0..<n]))) \<ge> 0)"
             using tmp
             by simp
           have "\<not> h b (n-i)"
             using tmp h
             by auto
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> h b x = False) \<and> (fst (rev_m (drop (n-i+1) (map (h b) [0..<n]))) > 0) 
                    \<and> (snd (rev_m (drop (n-i+1) (map (h b) [0..<n]))) \<ge> 0)) \<and> \<not> h b (n-i)"
             using h condi1 condi2 by blast
           hence "h b \<in> a2 \<inter> {f. \<not> f (n-i)}"
             using a1234(2) by blast
           thus "a \<in> a2 \<inter> {f. \<not> f (n-i)}"
             using \<open>a = h b\<close> by simp
         qed
       hence  "h ` (a2 \<inter> {f. f (n-i)}) \<subseteq> a2 \<inter> {f. \<not> f (n-i)}"
         by blast
       have "\<And>a. a \<in> a2 \<inter> {f. \<not> f (n-i)} \<Longrightarrow> a \<in> h ` (a2 \<inter> {f. f (n-i)})"
       proof - 
         fix a assume "a \<in> a2 \<inter> {f. \<not> f (n-i)}"
         hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> a x = False) \<and> (fst (rev_m (drop (n-i+1) (map a [0..<n]))) > 0)
            \<and> (snd (rev_m (drop (n-i+1) (map a [0..<n]))) \<ge> 0)) \<and> \<not> a (n-i)"
           using a1234(2) by blast
         obtain b where "b = h a"
           by simp
         hence condi1:"\<forall>x. (x \<notin> {..<n} \<longrightarrow> h a x = False)"
             using h tmp
             using assms(1) assms(2) diff_diff_cancel by auto 
           have "n - i + 1 \<le> n"
             using assms
             by linarith
           have ngt:"n - i + 1 > n - i" by simp
           have dropsimp:"(drop (n-i+1) (map (h a) [0..<n])) = (map (h a) [(n-i+1)..<n])"
             using drop_map_a by blast
           have "\<forall>x. n-i+1 \<le> x \<and> x < n \<longrightarrow> h a x = a x"
             using h by auto
           hence "\<And>x. x \<in> set [(n-i+1)..<n] \<Longrightarrow> h a x =a x"
             by simp
           hence "(map (h a) [(n-i+1)..<n]) =  (map a [(n-i+1)..<n])"
             using ngt list.map_cong0
             by blast
           hence "(drop (n-i+1) (map (h a) [0..<n])) = (drop (n-i+1) (map a [0..<n]))"
             using dropsimp
             by (simp add: drop_map)
           hence condi2:"(fst (rev_m (drop (n-i+1) (map (h a) [0..<n]))) > 0) 
\<and> (snd (rev_m (drop (n-i+1) (map (h a) [0..<n]))) \<ge> 0)"
             using tmp
             by simp
           have "h a (n-i)"
             using tmp h
             by auto
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> b x = False) \<and> (fst (rev_m (drop (n-i+1) (map b [0..<n]))) > 0) 
                    \<and> (snd (rev_m (drop (n-i+1) (map b [0..<n]))) \<ge> 0)) \<and> b (n-i)"
             using h condi1 condi2 \<open>b = h a\<close> by simp
           hence "b \<in> a2 \<inter> {f. f (n-i)}"
             using a1234(2) by blast
           have "h b = a"
           proof - 
             have "h b = h (h a)"
               using \<open>b = h a\<close> by simp
             hence "h (h a) = (\<lambda>f x. (if x = n-i then \<not> f x else f x)) ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a)"
               by (simp add: h)
             hence "h (h a) = 
(\<lambda>x. (if x = n-i then \<not> ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a) x else ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a) x)) " by simp
             hence "h (h a) = 
(\<lambda>x. (if x = n-i then \<not> (\<lambda>x. (if x = n-i then \<not> a x else a x)) x else (\<lambda>x. (if x = n-i then \<not> a x else a x)) x))" by simp
             hence "\<And>x. h (h a) x = 
(if x = n-i then \<not> (if x = n-i then \<not> a x else a x) else (if x = n-i then \<not> a x else a x))"
               by simp
             hence "\<And>x. h (h a) x = a x"
               by simp
              hence "h (h a) = a"
                by blast
              thus ?thesis
                by (simp add: \<open>b = h a\<close>)
            qed
              hence "(h b) \<in> h ` (a2 \<inter> {f. f (n-i)})"
                using \<open>b \<in> a2 \<inter> {f. f (n - i)}\<close> by blast
              thus "a \<in> h ` (a2 \<inter> {f. f (n-i)})"
                using \<open>h b = a\<close> by simp
            qed
            thus ?thesis
              using \<open>h ` (a2 \<inter> {f. f (n - i)}) \<subseteq> a2 \<inter> {f. \<not> f (n - i)}\<close> by blast
          qed
          hence "bij_betw h  (a2 \<inter> {f. f (n - i)}) (a2 \<inter> {f. \<not> f (n - i)})"
            by (simp add: \<open>inj_on h (a2 \<inter> {f. f (n - i)})\<close> bij_betw_def)
          hence "(\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (h a)) 
              =  sum (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (a2 \<inter> {f. \<not> f (n-i)})"
            using sum.reindex_bij_betw by simp
            hence "(\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (h a)) 
              =  (\<Sum>a \<in> a2 \<inter> { f.\<not> f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) a)"
              by blast
        hence tmp:"(\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x))) 
              =  (\<Sum>a \<in> a2 \<inter> { f.\<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          by simp
        have "\<And>a. a \<in> a2 \<inter> {f. f (n-i)} \<Longrightarrow> 
(\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x)) = 
(\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))"
          by (smt Un_iff greaterThanLessThan_iff h lessThan_iff less_irrefl prod.cong)
        hence "(\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x))) = 
              (\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          by auto
        thus "(\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
              =  (\<Sum>a \<in> a2 \<inter> { f.\<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          using tmp
          by linarith
      qed      
      hence "(\<Sum>a\<in>a2. ex a)  =
       ((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
            ((1-\<epsilon>)/2)
            + (\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) * (- (1+\<epsilon>)/2))"
        using tmp
        by (simp add: tmp)
      hence "(\<Sum>a\<in>a2. ex a)  =
       ((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
            (((1-\<epsilon>)/2) +  (- (1+\<epsilon>)/2)))"
        by (simp add: linordered_field_class.sign_simps(36))
      hence "(\<Sum>a\<in>a2. ex a)  =
  ((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
            (-\<epsilon>))"
      proof -
        have "\<forall>r ra. - (ra::real) + (r + ra) * inverse 2 = (r + - ra) * inverse 2"
          by auto
        then have "\<forall>r ra. ((ra::real) + - r) * inverse 2 + - (ra + r) * inverse 2 = - r"
          by (metis (no_types) add_diff_cancel_right' add_uminus_conv_diff mult_minus_left)
        then show ?thesis
          using \<open>sum ex a2 =  ((\<Sum>a\<in>a2 \<inter> {f. f (n - i)}. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * ((1 - \<epsilon>) / 2 + - (1 + \<epsilon>) / 2))\<close> by fastforce
      qed
   hence "(\<Sum>a\<in>a2. ex a)  =
       (
((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) + 
(\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))))/2
  * (-\<epsilon>))"
     by simp
hence tmp:"(\<Sum>a\<in>a2. ex a)  =
    (((\<Sum>a \<in> a2 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) + 
(\<Sum>a \<in> a2 \<inter> {f. \<not>f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))))/2
  * (-\<epsilon>))" using case2eq
  by simp
hence tmp:"(\<Sum>a\<in>a2. ex a)  =
          ((\<Sum>a \<in> (a2 \<inter> {f. f (n-i)}) \<union> (a2 \<inter> {f. \<not>f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))/2
  * (-\<epsilon>))" using  pair_disj_aux_i sum.union_disjoint infinite_Un
proof - 
  have fin:"finite (a2 \<inter> {f. f (n-i)}) \<and> finite (a2 \<inter> {f. \<not>f (n-i)})"
    using Forkable_Strings_Are_Rare.finite_False_func_set a1234(2) ep_gt_0 ep_le_1 by auto
  have "(a2 \<inter> {f. f (n-i)}) \<inter> (a2 \<inter> {f. \<not>f (n-i)}) = {}"
    by auto
  thus ?thesis  using pair_disj_aux_i sum.union_disjoint infinite_Un tmp fin
    by smt
qed
hence tmp:"(\<Sum>a\<in>a2. ex a)  =
  ((\<Sum>a \<in> a2. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))/2
  * (-\<epsilon>))"
proof - 
  have "(a2 \<inter> {f. f (n-i)}) \<union> (a2 \<inter> {f. \<not>f (n-i)}) = a2"
    by blast
  thus ?thesis using tmp by simp
qed
  define B where B:"B = (\<Sum>a \<in> a2. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))" 
  hence simplifiedcase2:"(\<Sum>a\<in>a2. ex a)  = B/2 * (-\<epsilon>)"
    using tmp by simp
(*case 3 start here*)
 have "(\<Sum>a\<in>a3. ex a)
= (\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n-i)..<n]))) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n-i)..<n])))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])))))) "
    using a1234 drop_map_a
    by metis 
  hence c3st1:"(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n])))) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n]))))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))"
    using consmap
    by (metis a1234(5) drop_map_a)
  hence tmp:"\<And>a. a\<in>a3 \<Longrightarrow>  fst (rev_m ((a (n-i))#(map a [(n - i + 1)..<n]))) 
        = fst (rev_m (map a [(n - i + 1)..<n])) + (max 0 (if a (n - i) then 1 else -1)) \<and>
          (min 0 (snd (rev_m (map a [(n - i)..<n])))) = 
          (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))) + (if a (n - i) then 1 else -1))" 
  proof -
    fix a
    assume "a \<in> a3"
    have f:" fst (rev_m (map a [(n - i + 1)..<n])) = 0"
      by (metis (mono_tags, lifting) \<open>a \<in> a3\<close> a1234(3) drop_map_a mem_Collect_eq)
   have s:"snd (rev_m (map a [(n - i + 1)..<n])) < 0"
      by (metis (mono_tags, lifting) \<open>a \<in> a3\<close> a1234(3) drop_map_a mem_Collect_eq)
  have "  rev_m (False#(map a [(n - i + 1)..<n])) 
        = (if fst (rev_m (map a [(n - i + 1)..<n])) > snd (rev_m (map a [(n - i + 1)..<n])) 
        \<and> snd (rev_m (map a [(n - i + 1)..<n])) = 0
        then (fst (rev_m (map a [(n - i + 1)..<n])) - 1, 0)
        else 
          (if fst (rev_m (map a [(n - i + 1)..<n])) = 0
          then (0, snd (rev_m (map a [(n - i + 1)..<n])) - 1)
          else (fst (rev_m (map a [(n - i + 1)..<n])) - 1, snd (rev_m (map a [(n - i + 1)..<n])) - 1)))"
    using rev_m.simps(3) by blast
  hence falsecase:"rev_m (False#(map a [(n - i + 1)..<n]))
        = (0, snd (rev_m (map a [(n - i + 1)..<n])) - 1)"
    using a1234
    by (smt s f)
  have truecase:"  rev_m (True#(map a [(n - i + 1)..<n])) 
        = (fst (rev_m (map a [(n - i + 1)..<n])) + 1, snd (rev_m (map a [(n - i + 1)..<n])) + 1)"
    using rev_m.simps(2) by blast
  hence fstcase:"fst (rev_m ((a (n-i))#(map a [(n - i + 1)..<n]))) 
        = fst (rev_m (map a [(n - i + 1)..<n])) + (max 0 (if a (n - i) then 1 else -1))"
    using falsecase f s
    by auto
  thus "fst (rev_m ((a (n-i))#(map a [(n - i + 1)..<n]))) 
        = fst (rev_m (map a [(n - i + 1)..<n])) + (max 0 (if a (n - i) then 1 else -1)) \<and>
          (min 0 (snd (rev_m (map a [(n - i)..<n])))) = 
          (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))) + (if a (n - i) then 1 else -1))"
    using falsecase f s truecase
    by (smt consmap snd_conv) 
qed
  hence c3unfold:"(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n - i + 1)..<n])) + (max 0 (if a (n - i) then 1 else -1))) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 +  \<alpha> * real_of_int (min 0 (snd (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n]))))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))"
    using c3st1 
proof -
  have "\<And>p. (p (n - i)::bool) # map p [Suc (n - i)..<n] = map p [n - i..<n]"
    by (metis (full_types, lifting) Suc_eq_plus1 consmap)
  then have f1: "\<And>p. p \<notin> a3 \<or> pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n]))))) = ex p"
    by (metis (no_types) Suc_eq_plus1 \<open>\<And>aa. aa \<in> a3 \<Longrightarrow> fst (rev_m (aa (n - i) # map aa [n - i + 1..<n])) = fst (rev_m (map aa [n - i + 1..<n])) + max 0 (if aa (n - i) then 1 else - 1) \<and> min 0 (snd (rev_m (map aa [n - i..<n]))) = min 0 (snd (rev_m (map aa [n - i + 1..<n]))) + (if aa (n - i) then 1 else - 1)\<close> a1234(5) drop_map_a)
  obtain bb :: "(nat \<Rightarrow> bool) set \<Rightarrow> ((nat \<Rightarrow> bool) \<Rightarrow> real) \<Rightarrow> ((nat \<Rightarrow> bool) \<Rightarrow> real) \<Rightarrow> nat \<Rightarrow> bool" where
    f2: "\<And>P f fa. (bb P f fa \<in> P \<or> sum f P = sum fa P) \<and> (f (bb P f fa) \<noteq> fa (bb P f fa) \<or> sum f P = sum fa P)"
    by (metis (full_types, lifting) sum.cong)
  { assume "bb a3 (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) ex \<notin> a3"
    then have ?thesis
  using f2 by (metis (full_types, lifting)) }
  moreover
  { assume "bb a3 (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) ex \<in> a3"
    then have "pmf (local.w_i_pmf n) (bb a3 (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) ex) *\<^sub>R (real_of_int (fst (rev_m (map (bb a3 (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) ex) [n - i + 1..<n])) + max 0 (if bb a3 (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) ex (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map (bb a3 (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) ex) [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (bb a3 (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) ex (n - i) # map (bb a3 (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) ex) [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map (bb a3 (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) ex) [n - i + 1..<n]))))) = ex (bb a3 (\<lambda>p. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) ex)"
      using f1 by meson
    then have ?thesis
      using f2
      by (metis (no_types, lifting)) }
  ultimately show ?thesis
  by blast
qed
hence c3unfold:"(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n - i + 1)..<n])) + (max 0 (if a (n - i) then 1 else -1))) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 +  \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))) + (if a (n - i) then 1 else -1)) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))"
  using tmp
proof -
have f1: "\<And>p. p \<notin> a3 \<or> pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n]))))) = pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n]))) + (if p (n - i) then 1 else - 1)) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))"
  by (metis consmap tmp)
  obtain bb :: "(nat \<Rightarrow> bool) set \<Rightarrow> ((nat \<Rightarrow> bool) \<Rightarrow> real) \<Rightarrow> ((nat \<Rightarrow> bool) \<Rightarrow> real) \<Rightarrow> nat \<Rightarrow> bool" where
    f2: "\<And>P f fa. (bb P f fa \<in> P \<or> sum f P = sum fa P) \<and> (f (bb P f fa) \<noteq> fa (bb P f fa) \<or> sum f P = sum fa P)"
    by (metis (no_types) sum.cong)
  then have "\<And>f fa. sum f a3 = sum fa a3 \<or> pmf (local.w_i_pmf n) (bb a3 f fa) *\<^sub>R (real_of_int (fst (rev_m (map (bb a3 f fa) [n - i + 1..<n])) + max 0 (if bb a3 f fa (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map (bb a3 f fa) [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (bb a3 f fa (n - i) # map (bb a3 f fa) [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map (bb a3 f fa) [n - i + 1..<n]))))) = pmf (local.w_i_pmf n) (bb a3 f fa) *\<^sub>R (real_of_int (fst (rev_m (map (bb a3 f fa) [n - i + 1..<n])) + max 0 (if bb a3 f fa (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map (bb a3 f fa) [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (map (bb a3 f fa) [n - i + 1..<n]))) + (if bb a3 f fa (n - i) then 1 else - 1)) - \<alpha> * real_of_int (min 0 (snd (rev_m (map (bb a3 f fa) [n - i + 1..<n])))))"
    using f1 by meson
  then have "(\<Sum>p\<in>a3. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n]))) + (if p (n - i) then 1 else - 1)) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n])))))) = (\<Sum>p\<in>a3. pmf (local.w_i_pmf n) p *\<^sub>R (real_of_int (fst (rev_m (map p [n - i + 1..<n])) + max 0 (if p (n - i) then 1 else - 1)) - real_of_int (fst (rev_m (map p [n - i + 1..<n]))) + \<alpha> * real_of_int (min 0 (snd (rev_m (p (n - i) # map p [n - i + 1..<n])))) - \<alpha> * real_of_int (min 0 (snd (rev_m (map p [n - i + 1..<n]))))))"
    using f2
    by (metis (no_types, lifting)) 
  then show ?thesis
  using c3unfold by presburger
qed
hence "(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n - i + 1)..<n])) + (max 0 (if a (n - i) then 1 else -1))) -
fst (rev_m (map a [(n - i + 1)..<n])))
 +  \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))) + (if a (n - i) then 1 else -1)) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))"
  using roi_minus_dist
  by blast
hence tmp:"(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n - i + 1)..<n])) + (max 0 (if a (n - i) then 1 else -1))) -
fst (rev_m (map a [(n - i + 1)..<n])))
 +  (\<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n]))) + (if a (n - i) then 1 else -1)) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])))))))"
  by (smt sum.cong)
  define k where k:"k = (\<lambda>a. \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)))
 - \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])))))"
  hence aux:"\<And>a. k a= 
\<alpha>* real_of_int ((min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1))) - 
(min 0 (snd (rev_m (map a [(n - i + 1)..<n])))))"
  by (simp add: right_diff_distrib)
  have "(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n - i + 1)..<n])) + (max 0 (if a (n - i) then 1 else -1))) -
fst (rev_m (map a [(n - i + 1)..<n])))
 +  k a))" using tmp k
    by (smt a1234(3) drop_map_a mem_Collect_eq real_vector.scale_cancel_left sum.cong)
  hence "(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n - i + 1)..<n])) + (max 0 (if a (n - i) then 1 else -1))) -
fst (rev_m (map a [(n - i + 1)..<n])))
 +  \<alpha>* real_of_int ((min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1))) - 
(min 0 (snd (rev_m (map a [(n - i + 1)..<n])))))))" using aux by auto
hence "(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (max 0 (if a (n - i) then 1 else -1)))
 + \<alpha> * (real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1)) - 
(min 0 (snd (rev_m (map a [(n - i + 1)..<n]))))))))"
  by simp
hence "(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (max 0 (if a (n - i) then 1 else -1)))
 + \<alpha> * 
(real_of_int (snd (rev_m (map a [(n - i + 1)..<n])) + (if a (n - i) then 1 else -1) - 
(snd (rev_m (map a [(n - i + 1)..<n])))))))"
  using a1234

  by (smt drop_map_a mem_Collect_eq real_vector.scale_cancel_left sum.cong) 
hence tmp:"(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (w_i_pmf n) a *\<^sub>R
(real_of_int (max 0 (if a (n - i) then 1 else -1))
 + 
\<alpha> * real_of_int (if a (n - i) then 1 else -1)))"
  using a1234
  by (smt sum.cong)         
  define k where k:"k = (\<lambda>a .real_of_int (max 0 (if a (n - i) then 1 else -1))
 + \<alpha> * real_of_int (if a (n - i) then 1 else -1))"
  hence tmp:"(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a * k a)"
  using w_i_pmf_def tmp
  by (smt of_int_1 of_int_minus real_scaleR_def sum.cong) 
hence tmp2:"(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. (\<Prod>x\<in>{..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * k a)"
  using w_i_pmf_def finite_False_func_set pmf_Pi'
proof - 
  have 1:"finite {..<n}" by simp
  have "\<And>a. a \<in> a3 \<Longrightarrow>  (\<And>x. x \<notin> {..<n} \<Longrightarrow> a x = False)"
    by (simp add: a1234(3))
  hence "\<And>a. a \<in> a3 \<Longrightarrow> pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a =
        (\<Prod>x\<in>{..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)) x) (a x))"
    using 1 pmf_Pi'
    by (smt prod.cong)
  thus ?thesis
    using tmp by auto
qed
hence "(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. (\<Prod>x\<in>{..<n-i} \<union> {n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * k a)"
proof -
  have "{..<n} = {..<n-i} \<union> {n-i} \<union> {n-i<..<n}"
    using assms(1) assms(2) ivl_disj_un_singleton(2) by force
  thus ?thesis using tmp2
    by simp
qed
hence tmp:"(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n} \<union> {n-i}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * k a)"
  by simp
hence "(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. ((\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(\<Prod>x\<in> {n-i}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * k a)"
proof - 
  have "finite ({..<n-i} \<union> {n-i<..<n}) \<and> (finite {n-i}) \<and> (({..<n-i} \<union> {n-i<..<n}) \<inter> {n-i} = {})"
    by simp
  thus ?thesis
    using tmp Groups_Big.comm_monoid_mult_class.prod.union_disjoint
    by (metis (no_types, lifting) sum.cong)
qed
  hence "(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a)"
  by simp
  hence tmp:"(\<Sum>a\<in>a3 . ex a)  =
(\<Sum>a \<in> a3 \<inter> ({f. f (n-i) \<or> \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a)"
  by auto
  hence "(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> a3  \<inter> ({f. f (n-i)} \<union> {f. \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a)"
  proof - 
    have "{f. f (n-i) \<or> \<not> f (n-i)} = {f. f (n-i)} \<union> {f. \<not> f (n-i)}"
      by auto
    thus ?thesis using tmp
      by auto
  qed
  hence tmp:"(\<Sum>a\<in>a3. ex a)  =
(\<Sum>a \<in> (a3 \<inter> {f. f (n-i)}) \<union> (a3 \<inter> {f. \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a)"
    by (simp add: inf_sup_distrib1)
 hence "(\<Sum>a\<in>a3. ex a)  =
((\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a)
+ (\<Sum>a \<in> a3 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a))"
 proof - 
   have 1:"(a3 \<inter> {f. f (n-i)}) \<inter> (a3 \<inter> {f. \<not> f (n-i)}) = {}"
     by blast
   have "finite a3" using a1234
     using fina by linarith
   hence "finite (a3 \<inter> {f. f (n-i)}) \<and> finite (a3 \<inter> {f. \<not> f (n-i)})"
     by auto
   thus ?thesis
     using tmp infinite_Un 1
     by (simp add: sum.union_disjoint)
 qed
hence tmp:"(\<Sum>a\<in>a3. ex a)  =
((\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) True) * (real_of_int (max 0 1)
 + \<alpha> * real_of_int (if a (n - i) then 1 else -1)))
+ (\<Sum>a \<in> a3 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) False) * (real_of_int (max 0 (-1))
 + \<alpha> * real_of_int (if a (n - i) then 1 else -1))))"
  using k  by auto
hence tmp:"(\<Sum>a\<in>a3. ex a)  =
((\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) True) * (real_of_int 1
 + \<alpha> * real_of_int (if a (n - i) then 1 else -1)))
+ (\<Sum>a \<in> a3 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) False) * ((real_of_int 0)
 + \<alpha> * real_of_int (if a (n - i) then 1 else -1))))"
  by linarith  
hence tmp:"(\<Sum>a\<in>a3. ex a)  =
((\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
((1-\<epsilon>)/2) * (1 + \<alpha>))
+ (\<Sum>a \<in> a3 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(1- ((1-\<epsilon>)/2)) * (- \<alpha>)))"
  using pmf_bernoulli_True pmf_bernoulli_False ep_gt_0 ep_le_1  by auto

hence tmp:"(\<Sum>a\<in>a3. ex a)  =
((\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
((1-\<epsilon>)/2) * (1 + \<alpha>))
+ (\<Sum>a \<in> a3 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
((1+\<epsilon>)/2) * (- \<alpha>)))"
proof - 
  have "(((1-\<epsilon>)/2) \<ge>0) \<and> ((1-\<epsilon>)/2) \<le> 1"
    using ep_gt_0 ep_le_1 by simp
  thus ?thesis using pmf_bernoulli_True pmf_bernoulli_False honest_slot tmp
    by (simp add: honest_slot)
qed

  have case3eq:"(\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
=  (\<Sum>a \<in> a3 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) "
  proof - 
    define h where h:"h = (\<lambda>f x. (if x = n-i then \<not> f x else f x))"
    have "(\<forall>x\<in>(a3 \<inter> {f. f (n-i)}). \<forall>y\<in>(a3 \<inter> {f. f (n-i)}). h x = h y \<longrightarrow> x = y)"
    proof (rule ccontr)
      assume "\<not> (\<forall>x\<in>(a3 \<inter> {f. f (n-i)}). \<forall>y\<in>(a3 \<inter> {f. f (n-i)}). h x = h y \<longrightarrow> x = y)"
      then obtain x y where "x\<in>(a3 \<inter> {f. f (n-i)})" "y\<in>(a3 \<inter> {f. f (n-i)})" "h x = h y" "x \<noteq> y"
        by blast
       obtain j where "x j \<noteq> y j"
         using \<open>x \<noteq> y\<close> 
         by blast
       have "(h x) j = (h y) j"
         using \<open>h x = h y\<close>
         by simp
       hence "(\<lambda>f x. (if x = n-i then \<not> f x else f x)) x j = (\<lambda>f x. (if x = n-i then \<not> f x else f x)) y j"
         using h by blast
       hence "(if j = n-i then \<not> x j else x j) = (if j = n-i then \<not> y j else y j)"
         by simp
       hence "x j = y j"
         by presburger
       thus "False"
         using \<open>x j \<noteq> y j\<close> by auto
     qed
     hence "inj_on h (a3 \<inter> {f. f (n-i)})"
       by (meson inj_onI)
     have "h ` (a3 \<inter> {f. f (n-i)}) = a3 \<inter> {f. \<not> f (n-i)}" (*may be worth having auxillary*)
     proof - 
       have "\<And>a. a \<in> h ` (a3 \<inter> {f. f (n-i)}) \<Longrightarrow> a \<in> a3 \<inter> {f. \<not> f (n-i)}"
          proof -
           fix a
           assume "a \<in> h ` (a3 \<inter> {f. f (n-i)}) "
           then obtain b where "b \<in> (a3 \<inter> {f. f (n-i)})" "a = h b"
             by blast
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> b x = False) \<and> (fst (rev_m (drop (n-i+1) (map b [0..<n]))) = 0)
\<and> (snd (rev_m (drop (n-i+1) (map b [0..<n]))) < 0)) \<and> b (n-i)"
             using a1234(3) by blast
           hence condi1:"\<forall>x. (x \<notin> {..<n} \<longrightarrow> h b x = False)"
             using h by simp
           have "n - i + 1 \<le> n"
             using assms
             by linarith
           have ngt:"n - i + 1 > n - i" by simp
           have dropsimp:"(drop (n-i+1) (map (h b) [0..<n])) = (map (h b) [(n-i+1)..<n])"
             using drop_map_a by blast
           have "\<forall>x. n-i+1 \<le> x \<and> x < n \<longrightarrow> h b x = b x"
             using h by auto
           hence "\<And>x. x \<in> set [(n-i+1)..<n] \<Longrightarrow> h b x =b x"
             by simp
           hence "(map (h b) [(n-i+1)..<n]) =  (map b [(n-i+1)..<n])"
             using ngt list.map_cong0
             by blast
           hence "(drop (n-i+1) (map (h b) [0..<n])) = (drop (n-i+1) (map b [0..<n]))"
             using dropsimp
             by (simp add: drop_map)
           hence condi2:"(fst (rev_m (drop (n-i+1) (map (h b) [0..<n]))) = 0) 
\<and> (snd (rev_m (drop (n-i+1) (map (h b) [0..<n]))) < 0)"
             using tmp
             by simp
           have "\<not> h b (n-i)"
             using tmp h
             by auto
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> h b x = False) \<and> (fst (rev_m (drop (n-i+1) (map (h b) [0..<n]))) = 0) 
                    \<and> (snd (rev_m (drop (n-i+1) (map (h b) [0..<n]))) < 0)) \<and> \<not> h b (n-i)"
             using h condi1 condi2 by blast
           hence "h b \<in> a3 \<inter> {f. \<not> f (n-i)}"
             using a1234(3) by blast
           thus "a \<in> a3 \<inter> {f. \<not> f (n-i)}"
             using \<open>a = h b\<close> by simp
         qed
       hence  "h ` (a3 \<inter> {f. f (n-i)}) \<subseteq> a3 \<inter> {f. \<not> f (n-i)}"
         by blast
       have "\<And>a. a \<in> a3 \<inter> {f. \<not> f (n-i)} \<Longrightarrow> a \<in> h ` (a3 \<inter> {f. f (n-i)})"
       proof - 
         fix a assume "a \<in> a3 \<inter> {f. \<not> f (n-i)}"
         hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> a x = False) \<and> (fst (rev_m (drop (n-i+1) (map a [0..<n]))) = 0)
            \<and> (snd (rev_m (drop (n-i+1) (map a [0..<n]))) < 0)) \<and> \<not> a (n-i)"
           using a1234(3) by blast
         obtain b where "b = h a"
           by simp
         hence condi1:"\<forall>x. (x \<notin> {..<n} \<longrightarrow> h a x = False)"
             using h tmp
             using assms(1) assms(2) diff_diff_cancel by auto 
           have "n - i + 1 \<le> n"
             using assms
             by linarith
           have ngt:"n - i + 1 > n - i" by simp
           have dropsimp:"(drop (n-i+1) (map (h a) [0..<n])) = (map (h a) [(n-i+1)..<n])"
             using drop_map_a by blast
           have "\<forall>x. n-i+1 \<le> x \<and> x < n \<longrightarrow> h a x = a x"
             using h by auto
           hence "\<And>x. x \<in> set [(n-i+1)..<n] \<Longrightarrow> h a x =a x"
             by simp
           hence "(map (h a) [(n-i+1)..<n]) =  (map a [(n-i+1)..<n])"
             using ngt list.map_cong0
             by blast
           hence "(drop (n-i+1) (map (h a) [0..<n])) = (drop (n-i+1) (map a [0..<n]))"
             using dropsimp
             by (simp add: drop_map)
           hence condi2:"(fst (rev_m (drop (n-i+1) (map (h a) [0..<n]))) = 0) 
\<and> (snd (rev_m (drop (n-i+1) (map (h a) [0..<n]))) < 0)"
             using tmp
             by simp
           have "h a (n-i)"
             using tmp h
             by auto
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> b x = False) \<and> (fst (rev_m (drop (n-i+1) (map b [0..<n]))) = 0) 
                    \<and> (snd (rev_m (drop (n-i+1) (map b [0..<n]))) < 0)) \<and> b (n-i)"
             using h condi1 condi2 \<open>b = h a\<close> by simp
           hence "b \<in> a3 \<inter> {f. f (n-i)}"
             using a1234(3) by blast
           have "h b = a"
           proof - 
             have "h b = h (h a)"
               using \<open>b = h a\<close> by simp
             hence "h (h a) = (\<lambda>f x. (if x = n-i then \<not> f x else f x)) ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a)"
               by (simp add: h)
             hence "h (h a) = 
(\<lambda>x. (if x = n-i then \<not> ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a) x else ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a) x)) " by simp
             hence "h (h a) = 
(\<lambda>x. (if x = n-i then \<not> (\<lambda>x. (if x = n-i then \<not> a x else a x)) x else (\<lambda>x. (if x = n-i then \<not> a x else a x)) x))" by simp
             hence "\<And>x. h (h a) x = 
(if x = n-i then \<not> (if x = n-i then \<not> a x else a x) else (if x = n-i then \<not> a x else a x))"
               by simp
             hence "\<And>x. h (h a) x = a x"
               by simp
              hence "h (h a) = a"
                by blast
              thus ?thesis
                by (simp add: \<open>b = h a\<close>)
            qed
              hence "(h b) \<in> h ` (a3 \<inter> {f. f (n-i)})"
                using \<open>b \<in> a3 \<inter> {f. f (n - i)}\<close> by blast
              thus "a \<in> h ` (a3 \<inter> {f. f (n-i)})"
                using \<open>h b = a\<close> by simp
            qed
            thus ?thesis
              using \<open>h ` (a3 \<inter> {f. f (n - i)}) \<subseteq> a3 \<inter> {f. \<not> f (n - i)}\<close> by blast
          qed
          hence "bij_betw h  (a3 \<inter> {f. f (n - i)}) (a3 \<inter> {f. \<not> f (n - i)})"
            by (simp add: \<open>inj_on h (a3 \<inter> {f. f (n - i)})\<close> bij_betw_def)
          hence "(\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (h a)) 
              =  sum (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (a3 \<inter> {f. \<not> f (n-i)})"
            using sum.reindex_bij_betw by simp
            hence "(\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (h a)) 
              =  (\<Sum>a \<in> a3 \<inter> { f.\<not> f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) a)"
              by blast
        hence tmp:"(\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x))) 
              =  (\<Sum>a \<in> a3 \<inter> { f.\<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          by simp
        have "\<And>a. a \<in> a3 \<inter> {f. f (n-i)} \<Longrightarrow> 
(\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x)) = 
(\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))"
          by (smt Un_iff greaterThanLessThan_iff h lessThan_iff less_irrefl prod.cong)
        hence "(\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x))) = 
              (\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          by auto
        thus "(\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
              =  (\<Sum>a \<in> a3 \<inter> { f.\<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          using tmp
          by linarith
      qed      
      hence tmp2:"(\<Sum>a\<in>a3. ex a)  =
            ((\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * 
            ((1-\<epsilon>)/2) * (1 + \<alpha>)
            + (\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) * ((1+\<epsilon>)/2) * (- \<alpha>))"
        using tmp
      proof -
        have "\<forall>r ra. (\<Sum>p\<in>a3 \<inter> {p. \<not> p (n - i)}. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * ra * r) = (\<Sum>p\<in>a3 \<inter> {p. p (n - i)}. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * ra * r)"
          by (metis (no_types) case3eq sum_distrib_right)
        then have "(\<Sum>p\<in>a3 \<inter> {p. p (n - i)}. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * ((1 - \<epsilon>) / 2) * (1 + \<alpha>)) + (\<Sum>p\<in>a3 \<inter> {p. p (n - i)}. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * ((1 + \<epsilon>) / 2) * - \<alpha>) = sum ex a3"
          using tmp by presburger
        then show ?thesis
          by (metis (no_types) sum_distrib_right)
      qed
      define s where "s =  (\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
      
      hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * ((1-\<epsilon>)/2) * (1 + \<alpha>) + s * ((1+\<epsilon>)/2) * (- \<alpha>))"
        using tmp2
        by blast
hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) * (1 + \<alpha>) +  ((1+\<epsilon>)/2) * (- \<alpha>)))"
  by (metis (mono_tags, hide_lams) linordered_field_class.sign_simps(36) mult.commute mult.left_commute)
 hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) + (((1-\<epsilon>)/2)* \<alpha>) +  ((1+\<epsilon>)/2) * (- \<alpha>)))"
   by (metis \<open>\<And>a. 1 * a + \<alpha> * a = (1 + \<alpha>) * a\<close> mult.commute mult.left_neutral)
   hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) * (1 + \<alpha>) +  ((1+\<epsilon>)/2) * (- \<alpha>)))"
  proof -
  have "s * ((1 + \<alpha>) * ((1 - \<epsilon>) / 2) + - \<alpha> * ((1 + \<epsilon>) / 2)) = sum ex a3"
    by (simp add: \<open>sum ex a3 = s * ((1 - \<epsilon>) / 2 * (1 + \<alpha>) + (1 + \<epsilon>) / 2 * - \<alpha>)\<close> mult.commute)
  then show ?thesis
    by (metis \<open>\<And>a. 1 * a + \<alpha> * a = (1 + \<alpha>) * a\<close> mult.commute)
qed 
 hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) + ((((1-\<epsilon>)/2)* \<alpha>) +  ((1+\<epsilon>)/2) * (- \<alpha>))))"
   using \<open>sum ex a3 = s * ((1 - \<epsilon>) / 2 + (1 - \<epsilon>) / 2 * \<alpha> + (1 + \<epsilon>) / 2 * - \<alpha>)\<close> by auto
    hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) + ((((1-\<epsilon>)/2)* \<alpha>) +  (-(1+\<epsilon>)/2) * \<alpha>)))"
      by (metis divide_minus_left mult_minus_left mult_minus_right)
        hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) + ((((1-\<epsilon>)/2) +  (-(1+\<epsilon>)/2)) * \<alpha>)))"
          by (simp add: linordered_field_class.sign_simps(35))
hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) + ((-\<epsilon>) * \<alpha>)))"
  by (metis (no_types, hide_lams) ab_group_add_class.ab_diff_conv_add_uminus add.commute add_diff_cancel_right' divide_minus_left real_average_minus_second)
hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) + ((-\<epsilon>) * (1 + \<epsilon>)/(2*\<epsilon>))))"
  using \<alpha> by simp
hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) + ((1 + \<epsilon>)*((-\<epsilon>/\<epsilon>)/2))))"
  by auto
hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) + ((1 + \<epsilon>)*((-1)/2))))"
  using ep_gt_0  by auto
hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (((1-\<epsilon>)/2) + (- (1 + \<epsilon>)/2)))"
  by (metis divide_inverse mult_minus1 mult_minus_left mult_minus_right)
  hence "(\<Sum>a\<in>a3. ex a)  =
            (s  * (-\<epsilon>))"
    by (metis ab_group_add_class.ab_diff_conv_add_uminus add.commute add_diff_cancel_right' divide_minus_left real_average_minus_second)
hence "(\<Sum>a\<in>a3. ex a)  =
            ((\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * (-\<epsilon>))"
  using s_def by blast
  
hence tmp:"(\<Sum>a\<in>a3. ex a)  =
             (
((\<Sum>a \<in> a3 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) + 
(\<Sum>a \<in> a3 \<inter> {f. \<not>f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))))/2
  * (-\<epsilon>))" using case3eq
  by simp
hence tmp:"(\<Sum>a\<in>a3. ex a)  =
         (
(\<Sum>a \<in> (a3 \<inter> {f. f (n-i)}) \<union> (a3 \<inter> {f. \<not>f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))/2
  * (-\<epsilon>))" using case1eq pair_disj_aux_i sum.union_disjoint infinite_Un
proof - 
  have fin:"finite (a3 \<inter> {f. f (n-i)}) \<and> finite (a3 \<inter> {f. \<not>f (n-i)})"
    using Forkable_Strings_Are_Rare.finite_False_func_set a1234(3) ep_gt_0 ep_le_1 by auto
  have "(a3 \<inter> {f. f (n-i)}) \<inter> (a3 \<inter> {f. \<not>f (n-i)}) = {}"
    by auto
  thus ?thesis  using case3eq pair_disj_aux_i sum.union_disjoint infinite_Un tmp fin
    by smt
qed
hence tmp:"(\<Sum>a\<in>a3. ex a)  = ((\<Sum>a \<in> a3. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))/2
  * (-\<epsilon>))"
proof - 
  have "(a3 \<inter> {f. f (n-i)}) \<union> (a3 \<inter> {f. \<not>f (n-i)}) = a3"
    by blast
  thus ?thesis using tmp by simp
qed
  define C where C:"C = (\<Sum>a \<in> a3. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))" 
  hence simplifiedcase3:"(\<Sum>a\<in>a3. ex a)  = C/2 * (-\<epsilon>)"
    using tmp by simp
(*case 4 start here*)
 have tmp:"(\<Sum>a\<in>a4. ex a)
= (\<Sum>a \<in> a4. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n-i)..<n]))) -
real_of_int (fst (rev_m (map a [(n - i + 1)..<n]))))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n-i)..<n])))) -
 \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n - i + 1)..<n])))))) "
    using a1234 drop_map_a
    by metis 
  hence "(\<Sum>a\<in>a4. ex a)
= (\<Sum>a \<in> a4. pmf (w_i_pmf n) a *\<^sub>R
((real_of_int (fst (rev_m (map a [(n-i)..<n]))) - 0)
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n-i)..<n])))) -
 \<alpha> * 0)) "
    using a1234(4)
  proof -
    have "\<And>a. a \<in> a4 \<Longrightarrow>
 fst (rev_m (map a [(n - i + 1)..<n])) = 0 \<and>
 snd (rev_m (map a [(n - i + 1)..<n])) = 0"
      by (metis (mono_tags, lifting) a1234(4) drop_map_a mem_Collect_eq)
    thus ?thesis using tmp
      by auto
  qed
 hence c4st1:"(\<Sum>a\<in>a4. ex a)
= (\<Sum>a \<in> a4. pmf (w_i_pmf n) a *\<^sub>R
(real_of_int (fst (rev_m (map a [(n-i)..<n])))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n-i)..<n]))))))"
   by auto
   have key:"\<And>a. a\<in>a4 \<Longrightarrow>  
(if (a (n-i)) then 
(fst (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n])))) = 1 \<and> 
(min 0 (snd (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n]))))) = 0
else 
(fst (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n])))) = 0 \<and> 
(min 0 (snd (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n]))))) = -1)"
  proof -
    fix a
    assume "a \<in> a4"
    have f:" fst (rev_m (map a [(n - i + 1)..<n])) = 0"
      by (metis (mono_tags, lifting) \<open>a \<in> a4\<close> a1234(4) drop_map_a mem_Collect_eq)
   have s:"snd (rev_m (map a [(n - i + 1)..<n])) = 0"
      by (metis (mono_tags, lifting) \<open>a \<in> a4\<close> a1234(4) drop_map_a mem_Collect_eq)
  have "rev_m (False#(map a [(n - i + 1)..<n])) 
        = (if fst (rev_m (map a [(n - i + 1)..<n])) > snd (rev_m (map a [(n - i + 1)..<n])) 
        \<and> snd (rev_m (map a [(n - i + 1)..<n])) = 0
        then (fst (rev_m (map a [(n - i + 1)..<n])) - 1, 0)
        else 
          (if fst (rev_m (map a [(n - i + 1)..<n])) = 0
          then (0, snd (rev_m (map a [(n - i + 1)..<n])) - 1)
          else (fst (rev_m (map a [(n - i + 1)..<n])) - 1, snd (rev_m (map a [(n - i + 1)..<n])) - 1)))"
    using rev_m.simps(3) by blast
  hence falsecase:"rev_m (False#(map a [(n - i + 1)..<n]))
        = (0, snd (rev_m (map a [(n - i + 1)..<n])) - 1)"
    using a1234
    by (smt s f)
  have truecase:"rev_m (True#(map a [(n - i + 1)..<n])) 
        = (fst (rev_m (map a [(n - i + 1)..<n])) + 1, snd (rev_m (map a [(n - i + 1)..<n])) + 1)"
    using rev_m.simps(2) by blast
  thus "(if (a (n-i)) then 
(fst (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n])))) = 1 \<and> 
(min 0 (snd (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n]))))) = 0
else 
(fst (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n])))) = 0 \<and> 
(min 0 (snd (rev_m ((a (n-i)) # (map a [(n - i + 1)..<n]))))) = -1)"
    using falsecase f s truecase
    by fastforce
qed 
  define k where k:"k = (\<lambda>a. real_of_int (fst (rev_m (map a [(n-i)..<n])))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n-i)..<n])))))"
 hence tmp:"(\<Sum>a\<in>a4. ex a)  =
(\<Sum>a \<in> a4. pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a * k a)"
   using c4st1 w_i_pmf_def by auto
hence tmp2:"(\<Sum>a\<in>a4. ex a)  =
(\<Sum>a \<in> a4. (\<Prod>x\<in>{..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * k a)"
proof - 
  have 1:"finite {..<n}" by simp
  have "\<And>a. a \<in> a4 \<Longrightarrow>  (\<And>x. x \<notin> {..<n} \<Longrightarrow> a x = False)"
    by (simp add: a1234(4))
  hence "\<And>a. a \<in> a4 \<Longrightarrow> pmf (Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))) a =
        (\<Prod>x\<in>{..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2)) x) (a x))"
    using 1 pmf_Pi'
    by (smt prod.cong)
  thus ?thesis
    using tmp by auto
qed
hence "(\<Sum>a\<in>a4. ex a)  =
(\<Sum>a \<in> a4. (\<Prod>x\<in>{..<n-i} \<union> {n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * k a)"
proof -
  have "{..<n} = {..<n-i} \<union> {n-i} \<union> {n-i<..<n}"
    using assms(1) assms(2) ivl_disj_un_singleton(2) by force
  thus ?thesis using tmp2
    by simp
qed
hence tmp:"(\<Sum>a\<in>a4. ex a)  =
(\<Sum>a \<in> a4. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n} \<union> {n-i}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * k a)"
  by simp
hence "(\<Sum>a\<in>a4. ex a)  =
(\<Sum>a \<in> a4. ((\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(\<Prod>x\<in> {n-i}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  * k a)"
proof - 
  have "finite ({..<n-i} \<union> {n-i<..<n}) \<and> (finite {n-i}) \<and> (({..<n-i} \<union> {n-i<..<n}) \<inter> {n-i} = {})"
    by simp
  thus ?thesis
    using tmp Groups_Big.comm_monoid_mult_class.prod.union_disjoint
    by (metis (no_types, lifting) sum.cong)
qed
  hence "(\<Sum>a\<in>a4. ex a)  =
(\<Sum>a \<in> a4. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a)"
  by simp
  hence tmp:"(\<Sum>a\<in>a4 . ex a)  =
(\<Sum>a \<in> a4 \<inter> ({f. f (n-i) \<or> \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a)"
  by auto
  hence "(\<Sum>a\<in>a4. ex a)  =
(\<Sum>a \<in> a4  \<inter> ({f. f (n-i)} \<union> {f. \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a)"
  proof - 
    have "{f. f (n-i) \<or> \<not> f (n-i)} = {f. f (n-i)} \<union> {f. \<not> f (n-i)}"
      by auto
    thus ?thesis using tmp
      by auto
  qed
  hence tmp:"(\<Sum>a\<in>a4. ex a)  =
(\<Sum>a \<in> (a4 \<inter> {f. f (n-i)}) \<union> (a4 \<inter> {f. \<not> f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a)"
    by (simp add: inf_sup_distrib1)
 hence "(\<Sum>a\<in>a4. ex a)  =
((\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a)
+ (\<Sum>a \<in> a4 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * k a))"
 proof - 
   have 1:"(a4 \<inter> {f. f (n-i)}) \<inter> (a4 \<inter> {f. \<not> f (n-i)}) = {}"
     by blast
   have "finite a4" using a1234
     using fina by linarith
   hence "finite (a4 \<inter> {f. f (n-i)}) \<and> finite (a4 \<inter> {f. \<not> f (n-i)})"
     by auto
   thus ?thesis
     using tmp infinite_Un 1
     by (simp add: sum.union_disjoint)
 qed
hence "(\<Sum>a\<in>a4. ex a)  =
((\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * (real_of_int (fst (rev_m (map a [(n-i)..<n])))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n-i)..<n]))))))
+ (\<Sum>a \<in> a4 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * (real_of_int (fst (rev_m (map a [(n-i)..<n])))
 + \<alpha> * real_of_int (min 0 (snd (rev_m (map a [(n-i)..<n])))))))"
  using k  by auto
hence "(\<Sum>a\<in>a4. ex a)  =
((\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * (real_of_int (1)
 + \<alpha> * real_of_int (0)))
+ (\<Sum>a \<in> a4 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * (real_of_int (0)
 + \<alpha> * real_of_int (-1))))"
  using key
  by (smt Int_Collect consmap mult_cancel_left sum.cong)
hence "(\<Sum>a\<in>a4. ex a)  =
((\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * (1))
+ (\<Sum>a \<in> a4 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) * 
(pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a (n-i)))  * (- \<alpha>)))"
  by auto
    hence "(\<Sum>a\<in>a4. ex a)  =
            (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * (1 - \<epsilon>) / 2 * 1) +
    (\<Sum>a\<in>a4 \<inter> {f. \<not> f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * (1 - (1-\<epsilon>)/2) * - \<alpha>)"
       using ep_gt_0 ep_le_1 by auto
 hence "(\<Sum>a\<in>a4. ex a)  =
            (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * (1 - \<epsilon>) / 2 ) +
    (\<Sum>a\<in>a4 \<inter> {f. \<not> f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * (1+\<epsilon>)/2 * - \<alpha>)"
   using honest_slot by auto
   hence "(\<Sum>a\<in>a4. ex a)  =
            (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * ((1 - \<epsilon>) / 2)) +
    (\<Sum>a\<in>a4 \<inter> {f. \<not> f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * (1+\<epsilon>)/2 * - \<alpha>)"
     by auto
    hence "(\<Sum>a\<in>a4. ex a)  =
            (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * ((1 - \<epsilon>) / 2)) +
    (\<Sum>a\<in>a4 \<inter> {f. \<not> f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * ((1+\<epsilon>)/2 * - \<alpha>))"
proof -
have f1: "(\<Sum>p\<in>a4 \<inter> {p. p (n - i)}. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * ((1 - \<epsilon>) / 2)) = (\<Sum>p\<in>a4 \<inter> {p. p (n - i)}. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * (1 - \<epsilon>) / 2)"
  using \<open>sum ex a4 = (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}. (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * ((1 - \<epsilon>) / 2)) + (\<Sum>a\<in>a4 \<inter> {f. \<not> f (n - i)}. (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * (1 + \<epsilon>) / 2 * - \<alpha>)\<close> \<open>sum ex a4 = (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}. (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * (1 - \<epsilon>) / 2) + (\<Sum>a\<in>a4 \<inter> {f. \<not> f (n - i)}. (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * (1 + \<epsilon>) / 2 * - \<alpha>)\<close> add_diff_cancel_right' by linarith
  have f2: "\<And>P. (\<Sum>p\<in>P. \<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * (- \<alpha> * ((1 + \<epsilon>) / 2)) = (\<Sum>p\<in>P. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * ((1 + \<epsilon>) / 2 * - \<alpha>))"
  by (metis (no_types) mult.commute sum_distrib_right)
  have f3: "\<And>P. (\<Sum>p\<in>P. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * (1 - \<epsilon>) / 2 * 1) = (\<Sum>p\<in>P. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * (1 - \<epsilon>) / 2)"
    by auto
  have f4: "\<And>P. - \<alpha> * ((1 + \<epsilon>) / 2 * (\<Sum>p\<in>P. \<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n))) = (\<Sum>p\<in>P. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * (1 - (1 - \<epsilon>) / 2) * - \<alpha>)"
    by (metis (no_types) honest_slot mult.commute sum_distrib_right)
  have "\<And>P. - \<alpha> * ((1 + \<epsilon>) / 2 * (\<Sum>p\<in>P. \<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n))) = (\<Sum>p\<in>P. (\<Prod>n\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (p n)) * ((1 + \<epsilon>) / 2 * - \<alpha>))"
    using f2
    by (simp add: mult.assoc mult.commute)  (* > 1.0 s, timed out *)
  then show ?thesis
    using f4 f3 f1 \<open>sum ex a4 = (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}. (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * (1 - \<epsilon>) / 2 * 1) + (\<Sum>a\<in>a4 \<inter> {f. \<not> f (n - i)}. (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * (1 - (1 - \<epsilon>) / 2) * - \<alpha>)\<close> by presburger
qed
       hence tmp:"(\<Sum>a\<in>a4. ex a)  =
            (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x))) * ((1 - \<epsilon>) / 2) +
    (\<Sum>a\<in>a4 \<inter> {f. \<not> f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x))) * ((1+\<epsilon>)/2 * - \<alpha>)"
       proof -
         show ?thesis
           by (metis (no_types) \<open>sum ex a4 = (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}. (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * ((1 - \<epsilon>) / 2)) + (\<Sum>a\<in>a4 \<inter> {f. \<not> f (n - i)}. (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) * ((1 + \<epsilon>) / 2 * - \<alpha>))\<close> sum_distrib_right)
       qed

  have case4eq:"(\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
=  (\<Sum>a \<in> a4 \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) "
  proof - 
    define h where h:"h = (\<lambda>f x. (if x = n-i then \<not> f x else f x))"
    have "(\<forall>x\<in>(a4 \<inter> {f. f (n-i)}). \<forall>y\<in>(a4 \<inter> {f. f (n-i)}). h x = h y \<longrightarrow> x = y)"
    proof (rule ccontr)
      assume "\<not> (\<forall>x\<in>(a4 \<inter> {f. f (n-i)}). \<forall>y\<in>(a4 \<inter> {f. f (n-i)}). h x = h y \<longrightarrow> x = y)"
      then obtain x y where "x\<in>(a4 \<inter> {f. f (n-i)})" "y\<in>(a4 \<inter> {f. f (n-i)})" "h x = h y" "x \<noteq> y"
        by blast
       obtain j where "x j \<noteq> y j"
         using \<open>x \<noteq> y\<close> 
         by blast
       have "(h x) j = (h y) j"
         using \<open>h x = h y\<close>
         by simp
       hence "(\<lambda>f x. (if x = n-i then \<not> f x else f x)) x j = (\<lambda>f x. (if x = n-i then \<not> f x else f x)) y j"
         using h by blast
       hence "(if j = n-i then \<not> x j else x j) = (if j = n-i then \<not> y j else y j)"
         by simp
       hence "x j = y j"
         by presburger
       thus "False"
         using \<open>x j \<noteq> y j\<close> by auto
     qed
     hence "inj_on h (a4 \<inter> {f. f (n-i)})"
       by (meson inj_onI)
     have "h ` (a4 \<inter> {f. f (n-i)}) = a4 \<inter> {f. \<not> f (n-i)}" (*may be worth having auxillary*)
     proof - 
       have "\<And>a. a \<in> h ` (a4 \<inter> {f. f (n-i)}) \<Longrightarrow> a \<in> a4 \<inter> {f. \<not> f (n-i)}"
          proof -
           fix a
           assume "a \<in> h ` (a4 \<inter> {f. f (n-i)}) "
           then obtain b where "b \<in> (a4 \<inter> {f. f (n-i)})" "a = h b"
             by blast
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> b x = False) \<and> (fst (rev_m (drop (n-i+1) (map b [0..<n]))) = 0)
\<and> (snd (rev_m (drop (n-i+1) (map b [0..<n]))) = 0)) \<and> b (n-i)"
             using a1234(4) by blast
           hence condi1:"\<forall>x. (x \<notin> {..<n} \<longrightarrow> h b x = False)"
             using h by simp
           have "n - i + 1 \<le> n"
             using assms
             by linarith
           have ngt:"n - i + 1 > n - i" by simp
           have dropsimp:"(drop (n-i+1) (map (h b) [0..<n])) = (map (h b) [(n-i+1)..<n])"
             using drop_map_a by blast
           have "\<forall>x. n-i+1 \<le> x \<and> x < n \<longrightarrow> h b x = b x"
             using h by auto
           hence "\<And>x. x \<in> set [(n-i+1)..<n] \<Longrightarrow> h b x =b x"
             by simp
           hence "(map (h b) [(n-i+1)..<n]) =  (map b [(n-i+1)..<n])"
             using ngt list.map_cong0
             by blast
           hence "(drop (n-i+1) (map (h b) [0..<n])) = (drop (n-i+1) (map b [0..<n]))"
             using dropsimp
             by (simp add: drop_map)
           hence condi2:"(fst (rev_m (drop (n-i+1) (map (h b) [0..<n]))) = 0) 
\<and> (snd (rev_m (drop (n-i+1) (map (h b) [0..<n]))) = 0)"
             using tmp
             by simp
           have "\<not> h b (n-i)"
             using tmp h
             by auto
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> h b x = False) \<and> (fst (rev_m (drop (n-i+1) (map (h b) [0..<n]))) = 0) 
                    \<and> (snd (rev_m (drop (n-i+1) (map (h b) [0..<n]))) = 0)) \<and> \<not> h b (n-i)"
             using h condi1 condi2 by blast
           hence "h b \<in> a4 \<inter> {f. \<not> f (n-i)}"
             using a1234(4) by blast
           thus "a \<in> a4 \<inter> {f. \<not> f (n-i)}"
             using \<open>a = h b\<close> by simp
         qed
       hence  "h ` (a4 \<inter> {f. f (n-i)}) \<subseteq> a4 \<inter> {f. \<not> f (n-i)}"
         by blast
       have "\<And>a. a \<in> a4 \<inter> {f. \<not> f (n-i)} \<Longrightarrow> a \<in> h ` (a4 \<inter> {f. f (n-i)})"
       proof - 
         fix a assume "a \<in> a4 \<inter> {f. \<not> f (n-i)}"
         hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> a x = False) \<and> (fst (rev_m (drop (n-i+1) (map a [0..<n]))) = 0)
            \<and> (snd (rev_m (drop (n-i+1) (map a [0..<n]))) = 0)) \<and> \<not> a (n-i)"
           using a1234(4) by blast
         obtain b where "b = h a"
           by simp
         hence condi1:"\<forall>x. (x \<notin> {..<n} \<longrightarrow> h a x = False)"
             using h tmp
             using assms(1) assms(2) diff_diff_cancel by auto 
           have "n - i + 1 \<le> n"
             using assms
             by linarith
           have ngt:"n - i + 1 > n - i" by simp
           have dropsimp:"(drop (n-i+1) (map (h a) [0..<n])) = (map (h a) [(n-i+1)..<n])"
             using drop_map_a by blast
           have "\<forall>x. n-i+1 \<le> x \<and> x < n \<longrightarrow> h a x = a x"
             using h by auto
           hence "\<And>x. x \<in> set [(n-i+1)..<n] \<Longrightarrow> h a x =a x"
             by simp
           hence "(map (h a) [(n-i+1)..<n]) =  (map a [(n-i+1)..<n])"
             using ngt list.map_cong0
             by blast
           hence "(drop (n-i+1) (map (h a) [0..<n])) = (drop (n-i+1) (map a [0..<n]))"
             using dropsimp
             by (simp add: drop_map)
           hence condi2:"(fst (rev_m (drop (n-i+1) (map (h a) [0..<n]))) = 0) 
\<and> (snd (rev_m (drop (n-i+1) (map (h a) [0..<n]))) = 0)"
             using tmp
             by simp
           have "h a (n-i)"
             using tmp h
             by auto
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> b x = False) \<and> (fst (rev_m (drop (n-i+1) (map b [0..<n]))) = 0) 
                    \<and> (snd (rev_m (drop (n-i+1) (map b [0..<n]))) = 0)) \<and> b (n-i)"
             using h condi1 condi2 \<open>b = h a\<close> by simp
           hence "b \<in> a4 \<inter> {f. f (n-i)}"
             using a1234(4) by blast
           have "h b = a"
           proof - 
             have "h b = h (h a)"
               using \<open>b = h a\<close> by simp
             hence "h (h a) = (\<lambda>f x. (if x = n-i then \<not> f x else f x)) ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a)"
               by (simp add: h)
             hence "h (h a) = 
(\<lambda>x. (if x = n-i then \<not> ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a) x else ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a) x)) " by simp
             hence "h (h a) = 
(\<lambda>x. (if x = n-i then \<not> (\<lambda>x. (if x = n-i then \<not> a x else a x)) x else (\<lambda>x. (if x = n-i then \<not> a x else a x)) x))" by simp
             hence "\<And>x. h (h a) x = 
(if x = n-i then \<not> (if x = n-i then \<not> a x else a x) else (if x = n-i then \<not> a x else a x))"
               by simp
             hence "\<And>x. h (h a) x = a x"
               by simp
              hence "h (h a) = a"
                by blast
              thus ?thesis
                by (simp add: \<open>b = h a\<close>)
            qed
              hence "(h b) \<in> h ` (a4 \<inter> {f. f (n-i)})"
                using \<open>b \<in> a4 \<inter> {f. f (n - i)}\<close> by blast
              thus "a \<in> h ` (a4 \<inter> {f. f (n-i)})"
                using \<open>h b = a\<close> by simp
            qed
            thus ?thesis
              using \<open>h ` (a4 \<inter> {f. f (n - i)}) \<subseteq> a4 \<inter> {f. \<not> f (n - i)}\<close> by blast
          qed
          hence "bij_betw h  (a4 \<inter> {f. f (n - i)}) (a4 \<inter> {f. \<not> f (n - i)})"
            by (simp add: \<open>inj_on h (a4 \<inter> {f. f (n - i)})\<close> bij_betw_def)
          hence "(\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (h a)) 
              =  sum (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (a4 \<inter> {f. \<not> f (n-i)})"
            using sum.reindex_bij_betw by simp
            hence "(\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (h a)) 
              =  (\<Sum>a \<in> a4 \<inter> { f.\<not> f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) a)"
              by blast
        hence tmp:"(\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x))) 
              =  (\<Sum>a \<in> a4 \<inter> { f.\<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          by simp
        have "\<And>a. a \<in> a4 \<inter> {f. f (n-i)} \<Longrightarrow> 
(\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x)) = 
(\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))"
          by (smt Un_iff greaterThanLessThan_iff h lessThan_iff less_irrefl prod.cong)
        hence "(\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x))) = 
              (\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          by auto
        thus "(\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
              =  (\<Sum>a \<in> a4 \<inter> { f.\<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          using tmp
          by linarith
      qed      
      hence tmp:"(\<Sum>a\<in>a4. ex a)  =
            (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x))) * ((1 - \<epsilon>) / 2) +
    (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x))) * ((1 + \<epsilon>) / 2) * - \<alpha>"
        using tmp
        by simp
      define s where s:"s = (\<Sum>a\<in>a4 \<inter> {f. f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)))"
hence "(\<Sum>a\<in>a4. ex a)  =
            s * ((1 - \<epsilon>) / 2) +
            s * ((1 + \<epsilon>) / 2) * - \<alpha>"
  using tmp by simp
  hence "(\<Sum>a\<in>a4. ex a)  =
            s * (((1 - \<epsilon>) / 2) + ((1 + \<epsilon>) / 2) * - \<alpha>)"
    by (metis (no_types, hide_lams) linordered_field_class.sign_simps(36) mult.commute mult.left_commute)
 
hence tmp:"(\<Sum>a\<in>a4. ex a)  =
             (
((\<Sum>a \<in> a4 \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) + 
(\<Sum>a \<in> a4 \<inter> {f. \<not>f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))))/2
   * (((1 - \<epsilon>) / 2) + ((1 + \<epsilon>) / 2) * - \<alpha>))" using case4eq s
  by simp
hence tmp:"(\<Sum>a\<in>a4. ex a)  =
         (
(\<Sum>a \<in> (a4 \<inter> {f. f (n-i)}) \<union> (a4 \<inter> {f. \<not>f (n-i)}). (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))/2
  * (((1 - \<epsilon>) / 2) + ((1 + \<epsilon>) / 2) * - \<alpha>))" using case1eq pair_disj_aux_i sum.union_disjoint infinite_Un
proof - 
  have fin:"finite (a4 \<inter> {f. f (n-i)}) \<and> finite (a4 \<inter> {f. \<not>f (n-i)})"
    using Forkable_Strings_Are_Rare.finite_False_func_set a1234(4) ep_gt_0 ep_le_1 by auto
  have "(a4 \<inter> {f. f (n-i)}) \<inter> (a4 \<inter> {f. \<not>f (n-i)}) = {}"
    by auto
  thus ?thesis  using case3eq pair_disj_aux_i sum.union_disjoint infinite_Un tmp fin
    by smt
qed
hence tmp:"(\<Sum>a\<in>a4. ex a)  = ((\<Sum>a \<in> a4. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))/2
  * (((1 - \<epsilon>) / 2) + ((1 + \<epsilon>) / 2) * - \<alpha>))"
proof - 
  have "(a4 \<inter> {f. f (n-i)}) \<union> (a4 \<inter> {f. \<not>f (n-i)}) = a4"
    by blast
  thus ?thesis using tmp by simp
qed
  define D where D:"D = (\<Sum>a \<in> a4. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))" 
  hence simplifiedcase4:"(\<Sum>a\<in>a4. ex a)  = D/2 * (((1 - \<epsilon>) / 2) + ((1 + \<epsilon>) / 2) * - \<alpha>)"
    using tmp by simp
(*big sum*)
  have tmp:"measure_pmf.expectation (bind_pmf (w_i_pmf n) (\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x))) (\<Delta>_n i) =
(1 + \<alpha>) * A/2 * (-\<epsilon>)+
B/2 * (-\<epsilon>)+
C/2 * (-\<epsilon>)+
D/2 * (((1 - \<epsilon>) / 2) + ((1 + \<epsilon>) / 2) * - \<alpha>)"
    using bigdist simplifiedcase1 simplifiedcase2 simplifiedcase3 simplifiedcase4 by simp
  hence tmp2:"measure_pmf.expectation (bind_pmf (w_i_pmf n) (\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x))) (\<Delta>_n i) \<le>
A/2 * (-\<epsilon>)+
B/2 * (-\<epsilon>)+
C/2 * (-\<epsilon>)+
D/2 * (-\<epsilon>)"
  proof - 
    have al_ge_1:"\<alpha> \<ge>1"
      using \<alpha> ep_le_1 ep_gt_0
      by simp 
    hence gt0:"1 + \<alpha> > 0"
      by auto
    have "\<forall>x. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) x \<ge> 0"
      by simp
    hence "\<forall>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)) \<ge> 0"
      by (simp add: prod_nonneg)
    hence "(\<Sum>a \<in> a1. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) \<ge> 0
\<and> (\<Sum>a \<in> a4. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) \<ge> 0"
      by (simp add: sum_nonneg)
    hence "A/2 \<ge> 0 \<and> D/2 \<ge>0"
      using A D
      by linarith
    hence "(1 + \<alpha>) * (A/2) \<ge> (A/2)"
      using gt0
proof -
  have f1: "\<not> \<alpha> \<le> - 1"
    using \<open>0 < 1 + \<alpha>\<close> by force
  have "(1 + \<alpha> = 0) = (\<alpha> = - 1)"
by fastforce
  then have f2: "(1 + \<alpha>) / ((1 + \<alpha>) * (A / 2)) = 1 / (A / 2)"
    using f1 nonzero_divide_mult_cancel_left by blast
  have f3: "1 + \<alpha> + - 1 = \<alpha>"
    by auto
  have f4: "- (1::real) * 1 = - 1"
    by auto
have f5: "\<not> (1::real) \<le> 0"
by auto
have f6: "\<forall>x0 x1. ((x1::real) < x0) = (\<not> x0 + - 1 * x1 \<le> 0)"
by auto
  have f7: "\<forall>x1. ((0::real) < x1) = (\<not> x1 \<le> 0)"
    by auto
have f8: "\<forall>x2 x3. ((x3::real) \<le> x2) = (0 \<le> x2 + - 1 * x3)"
  by simp
have f9: "0 \<le> \<alpha>"
using \<open>1 \<le> \<alpha>\<close> by linarith
  have f10: "(1 + \<alpha> \<le> 0) = (\<alpha> \<le> - 1)"
    by auto
have "(1 + \<alpha>) / ((1 + \<alpha>) * (A / 2)) + - 1 * (1 / (A / 2)) \<le> 0"
using f2 by auto
  moreover
  { assume "(1 + \<alpha>) * (A / 2) \<le> 0"
    then have "A / 2 \<le> 0"
      using f10 f7 f1 by (meson linordered_field_class.sign_simps(44))
    then have "A / 2 + - 1 * ((1 + \<alpha>) * (A / 2)) \<le> 0"
      using \<open>0 \<le> A / 2 \<and> 0 \<le> D / 2\<close> by fastforce }
  ultimately have "A / 2 + - 1 * ((1 + \<alpha>) * (A / 2)) \<le> 0"
    using f9 f8 f7 f6 f5 f4 f3 by (metis (full_types) frac_less2)
  then show ?thesis
    by fastforce
qed
  hence AA:"(1 + \<alpha>) * A/2 * (- \<epsilon>) \<le> A/2 * (- \<epsilon>)"
    using ep_gt_0 by simp
  have x:"((1 + \<epsilon>) / 2) \<ge> 0"
    using ep_gt_0 by simp
  have "- \<alpha> \<le> - 1" using al_ge_1 by simp 
  hence "((1 + \<epsilon>) / 2) * (- \<alpha>) \<le> ((1 + \<epsilon>) / 2) * (-1)"
    using x
    using ordered_comm_semiring_class.comm_mult_left_mono by blast
hence "((1 + \<epsilon>) / 2) * (- \<alpha>) \<le> - ((1 + \<epsilon>) / 2)"
  by simp
  hence "(1-\<epsilon>)/2 + ((1 + \<epsilon>) / 2) * (- \<alpha>) \<le> (1-\<epsilon>)/2 - ((1 + \<epsilon>) / 2)"
    by simp
hence "(1-\<epsilon>)/2 + ((1 + \<epsilon>) / 2) * (- \<alpha>) \<le> -\<epsilon>"
  by (smt real_average_minus_first)  
hence "D/2 * ((1-\<epsilon>)/2 + ((1 + \<epsilon>) / 2) * (- \<alpha>)) \<le> D/2 * -\<epsilon>" using \<open>A/2 \<ge> 0 \<and> D/2 \<ge>0\<close>
  using ordered_comm_semiring_class.comm_mult_left_mono by blast
  hence "(1 + \<alpha>) * A/2 * (-\<epsilon>)+
B/2 * (-\<epsilon>)+
C/2 * (-\<epsilon>)+
D/2 * (((1 - \<epsilon>) / 2) + ((1 + \<epsilon>) / 2) * - \<alpha>)\<le>
(1 + \<alpha>) * A/2 * (-\<epsilon>)+
B/2 * (-\<epsilon>)+
C/2 * (-\<epsilon>)+
D/2 * (-\<epsilon>)"
    by simp
hence "(1 + \<alpha>) * A/2 * (-\<epsilon>)+
B/2 * (-\<epsilon>)+
C/2 * (-\<epsilon>)+
D/2 * (((1 - \<epsilon>) / 2) + ((1 + \<epsilon>) / 2) * - \<alpha>)\<le>
A/2 * (-\<epsilon>)+
B/2 * (-\<epsilon>)+
C/2 * (-\<epsilon>)+
D/2 * (-\<epsilon>)"
  using AA
  by linarith
  thus ?thesis using tmp
    by simp
qed
  also have "... = (A/2 + B/2 + C/2 + D/2) * (-\<epsilon>)"
    by (simp add: comm_semiring_class.distrib)
  also have "... = (A+B+C+D) / 2 * (-\<epsilon>)"
    by simp
  also have tmp:"... = ((\<Sum>a \<in> a1. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))
+(\<Sum>a \<in> a2. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))
+(\<Sum>a \<in> a3. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))
+(\<Sum>a \<in> a4. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))) /2 * (-\<epsilon>)"
    using A B C D by blast
  also have "... = ((\<Sum>a \<in> a1 \<union> a2. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) +  
(\<Sum>a \<in> a3. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) +                  
(\<Sum>a \<in> a4. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))))/2 * (-\<epsilon>)"
    using fina pair_disj_aux_i sum.union_disjoint infinite_Un subs_finite_i a1234 pair_disj_aux_i
  proof -
    have "finite a1 \<and> finite a2 \<and> a1 \<inter> a2 = {}"
      by (smt a1234(1) a1234(2) disjoint_iff_not_equal fina mem_Collect_eq)
    thus ?thesis 
      using tmp
      by (simp add: sum.union_disjoint)
  qed
 also have "... = ((\<Sum>a \<in> a1 \<union> a2 \<union> a3. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) +                  
(\<Sum>a \<in> a4. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))))/2 * (-\<epsilon>)"
   using fina pair_disj_aux_i sum.union_disjoint infinite_Un subs_finite_i a1234 pair_disj_aux_i
 proof -
   have fin:"finite (a1 \<union> a2) \<and> finite a3"
     using a1234 fina
     by blast
   have "a1 \<inter> a3 = {} \<and> a2 \<inter> a3 = {}"  
     by (smt a1234 disjoint_iff_not_equal fina mem_Collect_eq)
   thus ?thesis
     using fin
     by (simp add: distrib(4) sum.union_disjoint)
 qed
also have tmp:"... = ((\<Sum>a \<in> a1 \<union> a2 \<union> a3 \<union> a4. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))))/2 * (-\<epsilon>)"
    using fina pair_disj_aux_i sum.union_disjoint infinite_Un subs_finite_i a1234 pair_disj_aux_i
  proof -
    have fin:"finite (a1 \<union> a2 \<union> a3) \<and> finite a4"
     using a1234 fina
     by blast
   have "a1 \<inter> a4 = {} \<and> a2 \<inter> a4 = {} \<and> a3 \<inter> a4 = {}"  
     by (smt a1234 disjoint_iff_not_equal fina mem_Collect_eq)
   thus ?thesis
     using fin
     by (simp add: distrib(4) sum.union_disjoint)
 qed
  finally have lecheck:"measure_pmf.expectation (w_pmf n) (\<Delta>_n i) \<le> 
((\<Sum>a \<in> {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))))/2 * (-\<epsilon>)"
  proof - 
    have "n - i + 1 \<le> n"
      using assms by auto
    hence "{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False)} = a1 \<union> a2 \<union> a3 \<union> a4"
      using a1234 four_cases_i assms
      by meson
    thus ?thesis
      using tmp
      using A B C D \<open>((\<Sum>a\<in>a1 \<union> a2. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
(a x)) + (\<Sum>a\<in>a3. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) + 
(\<Sum>a\<in>a4. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x))) / 2 * - \<epsilon> = 
((\<Sum>a\<in>a1 \<union> a2 \<union> a3. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) + 
(\<Sum>a\<in>a4. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x))) / 2 * - \<epsilon>\<close> 
\<open>((\<Sum>a\<in>a1. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) + (\<Sum>a\<in>a2.
 \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) + (\<Sum>a\<in>a3. \<Prod>x\<in>{..<n - i} 
\<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) + (\<Sum>a\<in>a4. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. 
pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x))) / 2 * - \<epsilon> = ((\<Sum>a\<in>a1 \<union> a2. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}.
 pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) + (\<Sum>a\<in>a3. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf 
(bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)) + (\<Sum>a\<in>a4. \<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf
 ((1 - \<epsilon>) / 2)) (a x))) / 2 * - \<epsilon>\<close> \<open>(A / 2 + B / 2 + C / 2 + D / 2) * - \<epsilon> = (A + B + C + D) / 2 * - \<epsilon>\<close>
 \<open>A / 2 * - \<epsilon> + B / 2 * - \<epsilon> + C / 2 * - \<epsilon> + D / 2 * - \<epsilon> = (A / 2 + B / 2 + C / 2 + D / 2) * - \<epsilon>\<close> 
\<open>measure_pmf.expectation (local.w_pmf n) (local.\<Delta>_n i) = measure_pmf.expectation (local.w_i_pmf n 
\<bind> (\<lambda>x. return_pmf (map x [0..<n]))) (local.\<Delta>_n i)\<close> tmp2 by presburger
  qed
  define X where X: "X = {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False)}"
  have eqX:"(\<Sum>a \<in> X \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
=  (\<Sum>a \<in> X \<inter> {f. \<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) "
  proof - 
    define h where h:"h = (\<lambda>f x. (if x = n-i then \<not> f x else f x))"
    have "(\<forall>x\<in>(X \<inter> {f. f (n-i)}). \<forall>y\<in>(X \<inter> {f. f (n-i)}). h x = h y \<longrightarrow> x = y)"
    proof (rule ccontr)
      assume "\<not> (\<forall>x\<in>(X \<inter> {f. f (n-i)}). \<forall>y\<in>(X \<inter> {f. f (n-i)}). h x = h y \<longrightarrow> x = y)"
      then obtain x y where "x\<in>(X \<inter> {f. f (n-i)})" "y\<in>(X \<inter> {f. f (n-i)})" "h x = h y" "x \<noteq> y"
        by blast
       obtain j where "x j \<noteq> y j"
         using \<open>x \<noteq> y\<close> 
         by blast
       have "(h x) j = (h y) j"
         using \<open>h x = h y\<close>
         by simp
       hence "(\<lambda>f x. (if x = n-i then \<not> f x else f x)) x j = (\<lambda>f x. (if x = n-i then \<not> f x else f x)) y j"
         using h by blast
       hence "(if j = n-i then \<not> x j else x j) = (if j = n-i then \<not> y j else y j)"
         by simp
       hence "x j = y j"
         by presburger
       thus "False"
         using \<open>x j \<noteq> y j\<close> by auto
     qed
     hence "inj_on h (X \<inter> {f. f (n-i)})"
       by (meson inj_onI)
     have "h ` (X \<inter> {f. f (n-i)}) = X \<inter> {f. \<not> f (n-i)}" (*may be worth having auxillary*)
     proof - 
       have "\<And>a. a \<in> h ` (X \<inter> {f. f (n-i)}) \<Longrightarrow> a \<in> X \<inter> {f. \<not> f (n-i)}"
          proof -
           fix a
           assume "a \<in> h ` (X \<inter> {f. f (n-i)}) "
           then obtain b where "b \<in> (X \<inter> {f. f (n-i)})" "a = h b"
             by blast
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> b x = False)) \<and> b (n-i)"
             using X by blast
           hence condi1:"\<forall>x. (x \<notin> {..<n} \<longrightarrow> h b x = False)"
             using h by simp
           have "\<not> h b (n-i)"
             using tmp h
             by auto
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> h b x = False)) \<and> \<not> h b (n-i)"
             using h condi1 by blast
           hence "h b \<in> X \<inter> {f. \<not> f (n-i)}"
             using X by blast
           thus "a \<in> X \<inter> {f. \<not> f (n-i)}"
             using \<open>a = h b\<close> by simp
         qed
       hence  "h ` (X \<inter> {f. f (n-i)}) \<subseteq> X \<inter> {f. \<not> f (n-i)}"
         by blast
       have "\<And>a. a \<in> X \<inter> {f. \<not> f (n-i)} \<Longrightarrow> a \<in> h ` (X \<inter> {f. f (n-i)})"
       proof - 
         fix a assume "a \<in> X \<inter> {f. \<not> f (n-i)}"
         hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> a x = False)) \<and> \<not> a (n-i)"
           using X by blast
         obtain b where "b = h a"
           by simp
         hence condi1:"\<forall>x. (x \<notin> {..<n} \<longrightarrow> h a x = False)"
             using h tmp
             using assms(1) assms(2) diff_diff_cancel by auto 
           have "h a (n-i)"
             using tmp h
             by auto
           hence tmp:"(\<forall>x. (x \<notin> {..<n} \<longrightarrow> b x = False)) \<and> b (n-i)"
             using h condi1  \<open>b = h a\<close> by simp
           hence "b \<in> X \<inter> {f. f (n-i)}"
             using X by blast
           have "h b = a"
           proof - 
             have "h b = h (h a)"
               using \<open>b = h a\<close> by simp
             hence "h (h a) = (\<lambda>f x. (if x = n-i then \<not> f x else f x)) ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a)"
               by (simp add: h)
             hence "h (h a) = 
(\<lambda>x. (if x = n-i then \<not> ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a) x else ((\<lambda>f x. (if x = n-i then \<not> f x else f x)) a) x)) " by simp
             hence "h (h a) = 
(\<lambda>x. (if x = n-i then \<not> (\<lambda>x. (if x = n-i then \<not> a x else a x)) x else (\<lambda>x. (if x = n-i then \<not> a x else a x)) x))" by simp
             hence "\<And>x. h (h a) x = 
(if x = n-i then \<not> (if x = n-i then \<not> a x else a x) else (if x = n-i then \<not> a x else a x))"
               by simp
             hence "\<And>x. h (h a) x = a x"
               by simp
              hence "h (h a) = a"
                by blast
              thus ?thesis
                by (simp add: \<open>b = h a\<close>)
            qed
              hence "(h b) \<in> h ` (X \<inter> {f. f (n-i)})"
                using \<open>b \<in> X \<inter> {f. f (n - i)}\<close> by blast
              thus "a \<in> h ` (X \<inter> {f. f (n-i)})"
                using \<open>h b = a\<close> by simp
            qed
            thus ?thesis
              using \<open>h ` (X \<inter> {f. f (n - i)}) \<subseteq> X \<inter> {f. \<not> f (n - i)}\<close> by blast
          qed
          hence "bij_betw h  (X \<inter> {f. f (n - i)}) (X \<inter> {f. \<not> f (n - i)})"
            by (simp add: \<open>inj_on h (X \<inter> {f. f (n - i)})\<close> bij_betw_def)
          hence "(\<Sum>a \<in> X \<inter> {f. f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (h a)) 
              =  sum (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (X \<inter> {f. \<not> f (n-i)})"
            using sum.reindex_bij_betw by simp
            hence "(\<Sum>a \<in> X \<inter> {f. f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) (h a)) 
              =  (\<Sum>a \<in> X \<inter> { f.\<not> f (n-i)}. (\<lambda>a. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) a)"
              by blast
        hence tmp:"(\<Sum>a \<in> X \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x))) 
              =  (\<Sum>a \<in> X \<inter> { f.\<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          by simp
        have "\<And>a. a \<in> X \<inter> {f. f (n-i)} \<Longrightarrow> 
(\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x)) = 
(\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))"
          by (smt Un_iff greaterThanLessThan_iff h lessThan_iff less_irrefl prod.cong)
        hence "(\<Sum>a \<in> X \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (h a x))) = 
              (\<Sum>a \<in> X \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          by auto
        thus "(\<Sum>a \<in> X \<inter> {f. f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x))) 
              =  (\<Sum>a \<in> X \<inter> { f.\<not> f (n-i)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))"
          using tmp
          by linarith
      qed      
      hence tmp:"(\<Sum>a\<in>X.  (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  =
            (\<Sum>a\<in>X \<inter> {f. f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x))) +
            (\<Sum>a\<in>X \<inter> {f. \<not> f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)))"
      proof - 
        have fin:"finite (X \<inter> {f. f (n - i)}) \<and> finite (X \<inter> {f. \<not> f (n - i)})"
          using finite_False_func_set X by simp
        have U:"(X \<inter> {f. f (n - i)}) \<union> (X \<inter> {f. \<not> f (n - i)}) = X"
          by auto
        have "(X \<inter> {f. f (n - i)}) \<inter> (X \<inter> {f. \<not> f (n - i)}) = {}"
          by auto
        thus ?thesis
          using fin U
          by (smt sum.union_disjoint)


      qed
      hence eq2tmp:"(\<Sum>a\<in>X.  (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  =
            2 * (\<Sum>a\<in>X \<inter> {f. \<not> f (n - i)}.
       (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (a x)))"
        using eqX
        by linarith
  have fin:"finite ({..<n-i} \<union> {n-i<..<n})" by simp
  have setpmf:"\<forall>a \<in> (X \<inter> {f. \<not> f (n - i)}). (\<forall>x. x \<notin> ({..<n-i} \<union> {n-i<..<n}) \<longrightarrow> a x = False)"
    using X by auto
  hence "\<forall>a \<in> (X \<inter> {f. \<not> f (n - i)}). pmf (Pi_pmf ({..<n - i} \<union> {n - i<..<n}) False (\<lambda>_. bernoulli_pmf ((1 - \<epsilon>) / 2))) a 
        = (\<Prod>x\<in>{..<n - i} \<union> {n - i<..<n}. pmf ((\<lambda>_. bernoulli_pmf ((1 - \<epsilon>) / 2)) x) (a x))"
    using fin pmf_Pi'
    by (smt prod.cong) 
  hence eq2tmp':"(\<Sum>a\<in>X.  (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  =
            2 * 
(\<Sum>a\<in>X \<inter> {f. \<not> f (n - i)}. pmf (Pi_pmf ({..<n - i} \<union> {n - i<..<n}) False (\<lambda>_. bernoulli_pmf ((1 - \<epsilon>) / 2))) a)"
    by (simp add: eq2tmp)
  have setpmf1:"set_pmf (Pi_pmf ({..<n - i} \<union> {n - i<..<n}) False (\<lambda>_. bernoulli_pmf ((1 - \<epsilon>) / 2)))
        \<subseteq> {f. \<forall>x. (x \<notin> ({..<n - i} \<union> {n - i<..<n})\<longrightarrow> f x = False)}"
    apply -
    apply (rule set_Pi_pmf_subset)
    using fin by simp
  have  tmp:"{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False)} \<inter> {f. \<not> f (n - i)}  
          = {f. (\<forall>x. (x \<in> ({..<n - i} \<union> {n-i} \<union> {n - i<..<n})  \<inter> - {n - i}) \<or> f x = False)}"
  proof - 
    have "n - i \<le> n" using assms by simp
    hence "{..<n} = {..<n - i} \<union> {n-i} \<union> {n - i<..<n}"
      using assms(1) assms(2) by force
    thus ?thesis 
      by blast
  qed
  have "({..<n - i} \<union> {n-i} \<union> {n - i<..<n})  \<inter> - {n - i}  = 
          ({..<n - i} \<inter> - {n - i} \<union> {n-i} \<inter> - {n - i} \<union> {n - i<..<n} \<inter> - {n - i}) "
    by blast
  hence "({..<n - i} \<union> {n-i} \<union> {n - i<..<n})  \<inter> - {n - i}  = 
          ({..<n - i} \<union> {n - i<..<n}  \<union> {n-i})  \<inter> - {n - i}  "
    using inf.absorb_iff2 subsetI 
    by auto
hence "({..<n - i} \<union> {n-i} \<union> {n - i<..<n})  \<inter> - {n - i}  = 
         ( {..<n - i} \<union> {n - i<..<n})  \<inter> - {n - i}  "
  using inf.absorb_iff2 subsetI
  by blast 
hence "({..<n - i} \<union> {n-i} \<union> {n - i<..<n})  \<inter> - {n - i}  = 
         ( {..<n - i} \<union> {n - i<..<n})  "
proof - 
have "{n - i<..<n} \<inter>  {n - i} = {}"
  by auto
  hence  "( {..<n - i} \<union> {n - i<..<n})  \<inter> {n - i} = {}"
    by blast
  hence "( {..<n - i} \<union> {n - i<..<n})  \<inter> - {n - i} = ( {..<n - i} \<union> {n - i<..<n})"
    by blast
  thus ?thesis
    by blast
qed

  hence "{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False)} \<inter> {f. \<not> f (n - i)}  
          = {f. (\<forall>x. (x \<in> {..<n - i} \<union> {n - i<..<n}) \<or> f x = False)}"
    using tmp by simp
hence "{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False)} \<inter> {f. \<not> f (n - i)}  
          = {f. (\<forall>x. (x \<in> {..<n - i} \<union> {n - i<..<n}) \<or> f x = False)}"
    using tmp by simp
  hence "{f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False)} \<inter> {f. \<not> f (n - i)}  
          = {f. (\<forall>x. (x \<notin> ({..<n - i} \<union> {n - i<..<n})\<longrightarrow> f x = False))}"
    by linarith
  hence setpmf2:"set_pmf (Pi_pmf ({..<n - i} \<union> {n - i<..<n}) False (\<lambda>_. bernoulli_pmf ((1 - \<epsilon>) / 2)))
        \<subseteq> X \<inter> {f. \<not> f (n - i)}"
    using setpmf1 X
    by simp 
  hence "(\<Sum>a\<in>X \<inter> {f. \<not> f (n - i)}. pmf (Pi_pmf ({..<n - i} \<union> {n - i<..<n}) False (\<lambda>_. bernoulli_pmf ((1 - \<epsilon>) / 2))) a) = 1"
    using sum_pmf_eq_1 fin  X finite_False_func_set
  proof -
    have "finite X"
      using X finite_False_func_set by blast
    then show ?thesis
      by (metis infinite_Un setpmf2 sum_pmf_eq_1 sup_inf_absorb)
  qed
 hence eq2:"(\<Sum>a\<in>X.  (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  = 2"
   using eq2tmp' by linarith 
  hence "(\<Sum>a \<in> {f. \<forall>x. (x \<notin> {..<n} \<longrightarrow> f x = False)}. (\<Prod>x\<in>{..<n-i} \<union> {n-i<..<n}. pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (a x)))  = 2"
    using X 
    by simp
  thus ?thesis using lecheck by auto
qed

lemma sum_delta:
  assumes "\<forall>x. \<exists>y::real. f x = y"
  shows "(\<Sum>a \<in> {1..(n::nat)}. f a - f (a-1)) = f n - f 0"
proof (induction n)
  case 0
  have "0 < (1::nat)"
    by simp
  hence "{1..(0::nat)} = {}"
    using atLeastatMost_empty by simp
  hence "(\<Sum>a \<in> {1..(0::nat)}. f a - f (a-1)) = 0"
    by simp
  then show ?case 
    by simp
next
  case (Suc n)
  hence "(\<Sum>a \<in> {1..(n::nat)}. f a - f (a-1)) = f n - f 0" by simp
  hence "(\<Sum>a \<in> {1..(n::nat)}. f a - f (a-1)) + (f(n+1) - f n) = f (n+1) - f 0" by simp
  hence "(\<Sum>a \<in> {1..(n::nat)}. f a - f (a-1)) + (\<Sum>a \<in> {n+1::nat}.f a - f (a-1)) = f (n+1) - f 0"
    by simp
  hence "(\<Sum>a \<in> {1..(n::nat)} \<union> {n+1}. f a - f (a-1)) = f (n+1) - f 0"
  by simp
  then show ?case
    by simp
qed

lemma \<Phi>_n_0:
  "\<Phi>_n 0 l = 0"  
  using \<Phi>_n_def by simp

lemma exist_\<Phi>: "\<exists>x::real. \<Phi>_n n l = x"
  by simp

lemma  sum_delta_\<Phi>:
  "(\<Sum>a \<in> {1..(n::nat)}. (\<lambda>x. \<Phi>_n x l) a - (\<lambda>x. \<Phi>_n x l) (a-1)) = (\<lambda>x. \<Phi>_n x l) n - (\<lambda>x. \<Phi>_n x l) 0"
  using exist_\<Phi> sum_delta by simp

lemma swap_sum:"(\<Sum>b \<in> {1..(n::nat)}. 
(\<Sum>a\<in> A.           
 x a *\<^sub>R (\<Delta>_n b (y a))))
= (\<Sum>a\<in> A. (\<Sum>b \<in> {1..(n::nat)}.          
 x a *\<^sub>R (\<Delta>_n b (y a))))"
proof (induction n)
case 0
then show ?case
  by auto 
next
  case (Suc n)
  have "(\<Sum>b \<in> {1..(n::nat)}. 
(\<Sum>a\<in> A.           
 x a *\<^sub>R (\<Delta>_n b (y a))))
= (\<Sum>a\<in> A. (\<Sum>b \<in> {1..(n::nat)}.          
x a *\<^sub>R (\<Delta>_n b (y a))))"
    using Suc.IH by blast
  hence "(\<Sum>b \<in> {1..(n::nat)}. 
(\<Sum>a\<in> A.           
x a *\<^sub>R (\<Delta>_n b (y a)))) + 
(\<Sum>a\<in> A.           
 x a *\<^sub>R (\<Delta>_n (Suc n) (y a)))
=
(\<Sum>a\<in> A. (\<Sum>b \<in> {1..(n::nat)}.          
 x a *\<^sub>R (\<Delta>_n b (y a))))
+
(\<Sum>a\<in> A.           
x a *\<^sub>R (\<Delta>_n (Suc n) (y a)))"
    by linarith
hence "(\<Sum>b \<in> {1..(Suc n::nat)}. 
(\<Sum>a\<in> A.           
 x a *\<^sub>R (\<Delta>_n b (y a))))
=
(\<Sum>a\<in> A. (\<Sum>b \<in> {1..(n::nat)}.          
 x a *\<^sub>R (\<Delta>_n b (y a))))
+
(\<Sum>a\<in> A.           
 x a *\<^sub>R (\<Delta>_n (Suc n) (y a)))"
  by simp
hence "(\<Sum>b \<in> {1..(Suc n::nat)}. 
(\<Sum>a\<in> A.           
x a *\<^sub>R (\<Delta>_n b (y a))))
=
(\<Sum>a\<in> A. (\<Sum>b \<in> {1..(n::nat)}.          
 x a *\<^sub>R (\<Delta>_n b (y a)))
+ x a *\<^sub>R (\<Delta>_n (Suc n) (y a)))"
  by (simp add: sum.distrib)
hence "(\<Sum>b \<in> {1..(Suc n::nat)}. 
(\<Sum>a\<in> A.  x a *\<^sub>R (\<Delta>_n b (y a))))
= (\<Sum>a\<in> A. (\<Sum>b \<in> {1..(Suc n::nat)}.          
 x a *\<^sub>R (\<Delta>_n b (y a))))"
  by simp
  then show ?case
    by blast
qed

lemma expectation_Phi_le_minus_n_ep:
  assumes "i \<le> n" "i > 0" 
  shows "measure_pmf.expectation (w_pmf n) (\<Phi>_n n) \<le> - (n * \<epsilon>)"
proof - 
 have "measure_pmf.expectation (w_pmf n) (\<Phi>_n n) = 
    measure_pmf.expectation 
(bind_pmf 
(w_i_pmf n) 
(\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x))) 
(\<Phi>_n n)"
    using w_i_pmf_def w_pmf_def 
    by (simp add: map_pmf_def) 
  also have "... = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
measure_pmf.expectation ((\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x)) a) (\<Phi>_n n))"
    apply -
    apply (rule pmf_expectation_bind)
    using finite_False_func_set apply auto[1]
    apply simp
    by (smt Collect_mono_iff finite_lessThan set_Pi_pmf_subset set_pmf_eq w_i_pmf_def)
  also have "... = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R  
              (\<Phi>_n n (map a [0..<n])))"
    using expectation_return_pmf
    by (simp add: \<Delta>_n_def) 
  finally have st0:"measure_pmf.expectation (w_pmf n) (\<Phi>_n n) = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R  
              (\<Phi>_n n (map a [0..<n]) - \<Phi>_n 0 (map a [0..<n])))"
    by (simp add: \<Phi>_n_0) 
  have "\<forall>n a. 
(\<Sum>b \<in> {1..(n::nat)}. (\<lambda>x. \<Phi>_n x (map a [0..<n])) b - (\<lambda>x. \<Phi>_n x (map a [0..<n])) (b-1))
= ((\<lambda>x. \<Phi>_n x (map a [0..<n])) n - (\<lambda>x. \<Phi>_n x (map a [0..<n])) 0)"
    using sum_delta_\<Phi>
    by simp
  hence st2:"measure_pmf.expectation (w_pmf n) (\<Phi>_n n) = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R           
(\<Sum>b \<in> {1..(n::nat)}. \<Phi>_n b (map a [0..<n]) - \<Phi>_n (b-1) (map a [0..<n])))"
  using st0
  by auto 
  have "\<forall>a. pmf (w_i_pmf n) a *\<^sub>R           
(\<Sum>b \<in> {1..(n::nat)}. \<Phi>_n b (map a [0..<n]) - \<Phi>_n (b-1) (map a [0..<n]))
=(\<Sum>b \<in> {1..(n::nat)}. pmf (w_i_pmf n) a *\<^sub>R (\<Phi>_n b (map a [0..<n]) - \<Phi>_n (b-1) (map a [0..<n])))"
    using sum_distrib_left
    using scaleR_right.sum by blast 
  hence "measure_pmf.expectation (w_pmf n) (\<Phi>_n n) = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}.           
(\<Sum>b \<in> {1..(n::nat)}. pmf (w_i_pmf n) a *\<^sub>R (\<Phi>_n b (map a [0..<n]) - \<Phi>_n (b-1) (map a [0..<n]))))"
    using st2
    by simp
 hence st3:"measure_pmf.expectation (w_pmf n) (\<Phi>_n n) = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}.           
(\<Sum>b \<in> {1..(n::nat)}. pmf (w_i_pmf n) a *\<^sub>R (\<Delta>_n b (map a [0..<n]))))"
    using \<Delta>_n_def
    by simp
  have "
\<forall>x A y. (\<Sum>b \<in> {1..(n::nat)}. 
(\<Sum>a\<in>A.           
 x a *\<^sub>R (\<Delta>_n b (y a))))
=
(\<Sum>a\<in>A.
(\<Sum>b \<in> {1..(n::nat)}. x a *\<^sub>R (\<Delta>_n b (y a))))"
    using swap_sum
    by blast 
    hence  "(\<Sum>b \<in> {1..(n::nat)}. 
(\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}.           
 (pmf (w_i_pmf n)) a *\<^sub>R (\<Delta>_n b (map a [0..<n]))))
=
(\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}.
(\<Sum>b \<in> {1..(n::nat)}. (pmf (w_i_pmf n)) a *\<^sub>R (\<Delta>_n b (map a [0..<n]))))"
      by force
  hence st4:"measure_pmf.expectation (w_pmf n) (\<Phi>_n n) = (\<Sum>b \<in> {1..(n::nat)}. 
(\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}.           
 pmf (w_i_pmf n) a *\<^sub>R (\<Delta>_n b (map a [0..<n]))))"
    using st3
    by linarith
    have simplified:"\<And>i. measure_pmf.expectation (w_pmf n) (\<Delta>_n i) = 
(\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
        (\<Delta>_n i (map a [0..<n])))"
    proof -
    fix i
    have "measure_pmf.expectation (w_pmf n) (\<Delta>_n i) =
    measure_pmf.expectation
(bind_pmf 
(w_i_pmf n) 
(\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x))) 
(\<Delta>_n i)"
    using w_i_pmf_def w_pmf_def 
    by (simp add: map_pmf_def) 
 also have "... = (\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
measure_pmf.expectation ((\<lambda>x. return_pmf ((\<lambda>f. map f [0..<n]) x)) a) (\<Delta>_n i))"
    apply -
    apply (rule pmf_expectation_bind)
    using finite_False_func_set apply auto[1]
    apply simp
    by (smt Collect_mono_iff finite_lessThan set_Pi_pmf_subset set_pmf_eq w_i_pmf_def)
  finally have "measure_pmf.expectation (w_pmf n) (\<Delta>_n i) = 
(\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
        (\<Delta>_n i (map a [0..<n])))"
   using expectation_return_pmf
   by simp
  thus "measure_pmf.expectation (w_pmf n) (\<Delta>_n i) = 
(\<Sum>a\<in>{f. \<forall>x. x \<notin> {..<n} \<longrightarrow> f x = False}. pmf (w_i_pmf n) a *\<^sub>R 
        (\<Delta>_n i (map a [0..<n])))" by simp
qed
  hence "measure_pmf.expectation (w_pmf n) (\<Phi>_n n) = (\<Sum>b \<in> {1..(n::nat)}. 
        measure_pmf.expectation (w_pmf n) (\<Delta>_n b))"
    using st4 by auto
  hence "measure_pmf.expectation (w_pmf n) (\<Phi>_n n) \<le> (\<Sum>b \<in> {1..(n::nat)}. -\<epsilon>)"
    by (smt atLeastAtMost_iff expectation_Delta_i_le_minus_ep le_numeral_extra(2) not_gr_zero sum_mono)
  hence "measure_pmf.expectation (w_pmf n) (\<Phi>_n n) \<le> of_nat (card {1..(n::nat)}) * (- \<epsilon>)"
    using sum_constant by simp
  thus ?thesis by simp
qed

lemma sum_Del_Phi:
  "(\<Sum>i \<in> {1..(n::nat)}. \<Delta>_n' i w) = \<Phi>_n' n w"
proof (induction n)
  case 0
  have "\<Phi>_n' 0 w = 0"
    by (simp add: \<Phi>_n'_def)
  then show ?case
    by simp 
next
  case (Suc n)
  hence "(\<Sum>i \<in> {1..(n::nat)}. \<Delta>_n' i w) = \<Phi>_n' n w"
    by blast
  have "{1..(Suc n::nat)} =  {1..(n::nat)} \<union> {Suc n}"
    by (simp add: atLeastAtMostSuc_conv)
  hence "(\<Sum>i \<in> {1..(Suc n::nat)}. \<Delta>_n' i w) =
(\<Sum>i \<in> {1..(n::nat)} \<union> {Suc n}. \<Delta>_n' i w) "
    by auto
hence "(\<Sum>i \<in> {1..(Suc n::nat)}. \<Delta>_n' i w) =
(\<Sum>i \<in> {1..(n::nat)}. \<Delta>_n' i w) + \<Delta>_n' (Suc n) w "
  by auto
  hence "... = \<Phi>_n' n w + \<Phi>_n (Suc n) w - \<Phi>_n n w + \<epsilon>"
    using Suc.IH \<Delta>_n'_def by auto
 hence "... = (\<Phi>_n n w + n * \<epsilon>) +  \<Phi>_n (Suc n) w - \<Phi>_n n w + \<epsilon>"
   by (simp add: \<Phi>_n'_def \<Phi>_n_def)
   hence "... =  n * \<epsilon> +  \<Phi>_n (Suc n) w + \<epsilon>"
     by (simp add: \<Phi>_n'_def \<Phi>_n_def)
   hence "... =  \<Phi>_n (Suc n) w + n* \<epsilon> + \<epsilon>"
     by simp
hence "... =  \<Phi>_n (Suc n) w + (n* \<epsilon> + \<epsilon>) "
  by simp
  hence "... =  \<Phi>_n (Suc n) w + (n* \<epsilon> + 1*\<epsilon>) "
    by simp
   hence "... = \<Phi>_n (Suc n) w + (Suc n)* \<epsilon> "
    by (metis Suc_eq_plus1 add.commute comm_semiring_class.distrib of_nat_Suc)
  then show ?case
    using \<Phi>_n'_def \<Phi>_n_def \<open>(\<Sum>i = 1..n. local.\<Delta>_n' i w) + local.\<Delta>_n' (Suc n) w = local.\<Phi>_n' n w + local.\<Phi>_n (Suc n) w - local.\<Phi>_n n w + \<epsilon>\<close> by auto
qed

definition \<Delta>_n_pmf' where 
  "\<Delta>_n_pmf' n = map_pmf (\<lambda>x. \<Delta>_n' n x) (w_pmf n)"

definition \<Phi>_n_pmf' where 
  "\<Phi>_n_pmf' n = map_pmf (\<lambda>x. \<Phi>_n' n x) (w_pmf n)"

(*
definition w_n_pmf :: "nat pmf" where
 "w_n_pmf = map_pmf (\<lambda>b. (if b then 1 else 0)) (bernoulli_pmf ((1-\<epsilon>)/2))"

definition W_n_pmf :: "int pmf" where 
 "W_n_pmf = bind_pmf (bernoulli_pmf ((1-\<epsilon>)/2)) (\<lambda>b. return_pmf ((-1) ^ (if b then 0 else 1)))"

definition w_i_pmf :: "nat \<Rightarrow> (nat \<Rightarrow> bool) pmf" where "w_i_pmf n = Pi_pmf {..<n} False (\<lambda>_. bernoulli_pmf ((1-\<epsilon>)/2))"

definition w_pmf where "w_pmf n = map_pmf (\<lambda>f. map f [0..<n]) (w_i_pmf n)"
*)

find_theorems real_cond_exp
lemma "real_cond_exp (\<Phi>_n_pmf' (Suc n)) (\<Phi>_n_pmf' n) f \<le> f"


lemma 
"set_pmf (w_pmf n) \<subseteq> {l::(bool list). size l = n}"
proof (cases "\<epsilon> = 1")
  case True
  then show ?thesis sorry
next
  case False
  hence "\<epsilon> < 1"
    using ep_le_1 by auto

  then show ?thesis 
    sorry
qed

lemma 
  assumes "size l = n"
  shows "\<Delta>_n n l \<le> 1 + \<alpha> \<and> \<Delta>_n n l \<ge> - (1 + \<alpha>)"

  find_theorems 
find_theorems \<Phi>_n

lemma 
  assumes "a \<in> set_pmf (\<Delta>_n_pmf' n)"
  shows "a \<le> 1 + \<alpha> + \<epsilon> \<or> a \<ge> - (1 + \<alpha>) + \<epsilon>"
proof -
  have "set_pmf (\<Delta>_n_pmf' n) = (\<lambda>x. \<Delta>_n' n x) ` set_pmf (w_pmf n)"
    by (simp add: \<Delta>_n_pmf'_def)



  hence ""                     

    find_theorems w_pmf

  find_theorems set_pmf map_pmf

lemma "\<P>(X in \<Delta>_n_pmf' n. X \<le> 1 + \<alpha> + \<epsilon> \<and> X \<ge> - (1 + \<alpha>) + \<epsilon>) = 1"
 

lemma "\<P>(w in w_list_pmf n. rev_\<mu>_bar w = 0) \<le> \<P>(X in \<Phi>_n_pmf n. X \<ge> 0)"
  sorry

definition \<Delta>_n_pmf where 
  "\<Delta>_n_pmf n = map_pmf (\<lambda>x. \<Delta>_n n x) (w_list_pmf n)"

definition \<Phi>_n_pmf where 
  "\<Phi>_n_pmf n = map_pmf (\<lambda>x. \<Phi>_n n x) (w_list_pmf n)"

lemma "\<P>(X in \<Phi>_n_pmf n. X \<ge> 0) = \<P>(X in \<Phi>_n_pmf' n. X \<ge> \<epsilon> * n)"
  sorry

(*then apply hoeffding*)

lemma "\<P>(w in w_list_pmf n. rev_\<mu> w \<ge> 0) = exp ((-2) * \<epsilon> ^ 4 / (1 + 35 * \<epsilon>) * n)"
  sorry

end


























(*
lemma set_pmf_w_list_ep_eq_1:
  assumes "\<epsilon> = 1"
  shows "set_pmf (w_list_pmf n) = {l:: bool list. size l = n \<and> (\<forall>a \<in> set l. \<not> a)}"
proof (induction n)
  case 0
  then show ?case by auto
next
case (Suc n)
  hence "set_pmf (w_list_pmf n) = {l:: bool list. size l = n \<and> (\<forall>a \<in> set l. \<not> a)}"
    by simp
  have "set_pmf (w_list_pmf (Suc n)) = set_pmf (bind_pmf (w_list_pmf n) 
   (\<lambda>xs. bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (\<lambda>x.
   return_pmf (x # xs))))"
    by auto
  also have "... = (\<Union>M\<in>set_pmf (w_list_pmf n). set_pmf ((\<lambda>xs. bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
            (\<lambda>x. return_pmf (x # xs))) M))"
    by auto
  also have "... = (\<Union>M\<in> {l:: bool list. size l = n \<and> (\<forall>a \<in> set l. \<not> a)}. 
            set_pmf (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
                        (\<lambda>x. return_pmf (x # M))))"
    using Suc.IH by blast
  also have "... = (\<Union>M\<in> {l:: bool list. size l = n \<and> (\<forall>a \<in> set l. \<not> a)}. 
            (\<Union>N\<in> set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). 
                       set_pmf (return_pmf (N # M))))"
    by simp
  also have "... = (\<Union>M\<in> {l:: bool list. size l = n \<and> (\<forall>a \<in> set l. \<not> a)}. 
            (\<Union>N\<in> set_pmf (bernoulli_pmf 0). 
                       set_pmf (return_pmf (N # M))))"
    using ep_gt_0 ep_le_1 assms set_pmf_bernoulli
    using half_gt_zero_iff by auto
   also have "... = (\<Union>M\<in> {l:: bool list. size l = n \<and> (\<forall>a \<in> set l. \<not> a)}. 
            (\<Union>N\<in> {False}. 
                       set_pmf (return_pmf (N # M))))"
   proof -
     have "set_pmf (bernoulli_pmf 0) = {False}"
       by (smt Diff_UNIV Diff_eq_empty_iff Diff_insert0 UNIV_bool insert_commute mem_Collect_eq 
pmf_bernoulli_True set_pmf_eq set_pmf_subset_singleton set_return_pmf)
     thus ?thesis by simp
   qed
  also have "... = (\<Union>M\<in> {l:: bool list. size l = n \<and> (\<forall>a \<in> set l. \<not> a) }. 
             {False # M})"
    by auto
  finally have "... = {l:: bool list. size l = Suc n \<and> (\<forall>a \<in> set l. \<not> a)}"
    proof -
      have "\<And>x. x \<in> {l:: bool list. size l = Suc n \<and> (\<forall>a \<in> set l. \<not> a)} \<Longrightarrow>
          x \<in> (\<Union>M\<in> {l:: bool list. size l = n \<and> (\<forall>a \<in> set l. \<not> a) }. 
             {False # M}) "
      proof - 
        fix x 
        assume "x \<in> {l:: bool list. size l = Suc n \<and> (\<forall>a \<in> set l. \<not> a)}"
        hence "x \<noteq> [] \<and> (\<forall>a \<in> set x. \<not> a)"
          by auto
        then obtain l where "x = False # l"
          by (metis (full_types) list.set_intros(1) neq_Nil_conv)
        hence "size l = n \<and> (\<forall>a \<in> set l. \<not> a)"
          using \<open>x \<in> {l. length l = Suc n \<and> (\<forall>a\<in>set l. \<not> a)}\<close> by auto
        thus "x \<in> (\<Union>M\<in> {l:: bool list. size l = n \<and> (\<forall>a \<in> set l. \<not> a) }. 
             {False # M})"
          using \<open>x = False # l\<close> by blast
      qed
      have "\<And>x. x \<in> (\<Union>M\<in> {l:: bool list. size l = n \<and> (\<forall>a \<in> set l. \<not> a) }. 
             {False # M}) \<Longrightarrow>  x \<in> {l:: bool list. size l = Suc n \<and> (\<forall>a \<in> set l. \<not> a)}"
      proof -
        fix x
        assume "x \<in> (\<Union>M\<in> {l:: bool list. size l = n \<and> (\<forall>a \<in> set l. \<not> a) }. 
             {False # M})"
        obtain l where "x = False # l"
          using \<open>x \<in> (\<Union>M\<in>{l. length l = n \<and> (\<forall>a\<in>set l. \<not> a)}. {False # M})\<close> by blast
        hence "l \<in> {l. length l = n \<and> (\<forall>a\<in>set l. \<not> a)}"
          using \<open>x \<in> (\<Union>M\<in>{l. length l = n \<and> (\<forall>a\<in>set l. \<not> a)}. {False # M})\<close> by blast
        hence "length l = n \<and> (\<forall>a\<in>set l. \<not> a)"
          by blast
        hence "size x = Suc n \<and> (\<forall>a\<in>set (False#l). \<not> a)"
          by (metis One_nat_def Suc_eq_plus1 \<open>x = False # l\<close> insertE list.simps(15) list.size(4))
        thus "x \<in> {l:: bool list. size l = Suc n \<and> (\<forall>a \<in> set l. \<not> a)}"
          using \<open>x = False # l\<close> by blast
      qed
      thus ?thesis
        by (smt Collect_cong Sup_set_def \<open>\<And>x. x \<in> {l. length l = Suc n \<and> (\<forall>a\<in>set l. \<not> a)} \<Longrightarrow> x \<in> (\<Union>M\<in>{l. length l = n \<and> (\<forall>a\<in>set l. \<not> a)}. {False # M})\<close> mem_Collect_eq)
    qed
    thus ?case
      by (metis Suc_eq_plus1 \<open>(\<Union>M\<in>set_pmf (local.w_list_pmf n). set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2) \<bind> (\<lambda>x. return_pmf (x # M)))) = (\<Union>M\<in>{l. length l = n \<and> (\<forall>a\<in>set l. \<not> a)}. set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2) \<bind> (\<lambda>x. return_pmf (x # M))))\<close> \<open>(\<Union>M\<in>{l. length l = n \<and> (\<forall>a\<in>set l. \<not> a)}. \<Union>N\<in>set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). set_pmf (return_pmf (N # M))) = (\<Union>M\<in>{l. length l = n \<and> (\<forall>a\<in>set l. \<not> a)}. \<Union>N\<in>set_pmf (bernoulli_pmf 0). set_pmf (return_pmf (N # M)))\<close> \<open>(\<Union>M\<in>{l. length l = n \<and> (\<forall>a\<in>set l. \<not> a)}. \<Union>N\<in>set_pmf (bernoulli_pmf 0). set_pmf (return_pmf (N # M))) = (\<Union>M\<in>{l. length l = n \<and> (\<forall>a\<in>set l. \<not> a)}. \<Union>N\<in>{False}. set_pmf (return_pmf (N # M)))\<close> \<open>(\<Union>M\<in>{l. length l = n \<and> (\<forall>a\<in>set l. \<not> a)}. \<Union>N\<in>{False}. set_pmf (return_pmf (N # M))) = (\<Union>M\<in>{l. length l = n \<and> (\<forall>a\<in>set l. \<not> a)}. {False # M})\<close> \<open>(\<Union>M\<in>{l. length l = n \<and> (\<forall>a\<in>set l. \<not> a)}. set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2) \<bind> (\<lambda>x. return_pmf (x # M)))) = (\<Union>M\<in>{l. length l = n \<and> (\<forall>a\<in>set l. \<not> a)}. \<Union>N\<in>set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). set_pmf (return_pmf (N # M)))\<close> \<open>set_pmf (local.w_list_pmf (Suc n)) = set_pmf (local.w_list_pmf n \<bind> (\<lambda>xs. bernoulli_pmf ((1 - \<epsilon>) / 2) \<bind> (\<lambda>x. return_pmf (x # xs))))\<close> \<open>set_pmf (local.w_list_pmf n \<bind> (\<lambda>xs. bernoulli_pmf ((1 - \<epsilon>) / 2) \<bind> (\<lambda>x. return_pmf (x # xs)))) = (\<Union>M\<in>set_pmf (local.w_list_pmf n). set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2) \<bind> (\<lambda>x. return_pmf (x # M))))\<close> assms set_pmf_eq)
  qed

lemma finite_set_fixed_size_bool_list:
"finite {l:: bool list. size l = n}"
proof (induction n)
  case 0
  have "{l:: bool list. size l = 0} = {[]}"
    by simp
  then show ?case
    by simp 
next
  case (Suc n)
  hence "finite ((\<lambda>l. True # l)`{l:: bool list. size l = n})" by simp
  have "finite ((\<lambda>l. False # l)`{l:: bool list. size l = n})" by (simp add: Suc.IH)
  have "finite
((\<lambda>l. True # l) ` {l:: bool list. size l = n} \<union> (\<lambda>l. False # l) ` {l:: bool list. size l = n})"
    by (simp add: \<open>finite (op # False ` {l. length l = n})\<close> \<open>finite (op # True ` {l. length l = n})\<close>)
  have "\<And>x. x \<in> {l:: bool list. size l = Suc n} \<Longrightarrow> x \<in> ((\<lambda>l. True # l) ` {l:: bool list. size l = n} \<union> (\<lambda>l. False # l) ` {l:: bool list. size l = n})"
  proof -
    fix x
    assume  "x \<in> {l:: bool list. size l = Suc n}"
    then obtain a b where ab: "x = a#b" "size b = n" "(a = True) \<or> (a = False)"
      by (meson length_Suc_conv mem_Collect_eq)
    hence "b \<in> {l:: bool list. size l = n}" by simp
    hence "a#b \<in> ((\<lambda>l. True # l) ` {l:: bool list. size l = n} \<union> (\<lambda>l. False # l) ` {l:: bool list. size l = n})"
      by blast
    thus "x \<in> ((\<lambda>l. True # l) ` {l:: bool list. size l = n} \<union> (\<lambda>l. False # l) ` {l:: bool list. size l = n})"
      using ab(1) by blast
  qed
  hence "{l:: bool list. size l = Suc n} \<subseteq> ((\<lambda>l. True # l) ` {l:: bool list. size l = n} \<union> (\<lambda>l. False # l) ` {l:: bool list. size l = n})"
    by blast
      then show ?case
        using \<open>finite (op # True ` {l. length l = n} \<union> op # False ` {l. length l = n})\<close> finite_subset by blast
qed

lemma finite_set_w_list_pmf:
"finite (set_pmf (w_list_pmf n))"
  using Forkable_Strings_Are_Rare.set_pmf_w_list_ep_eq_1 finite_set_fixed_size_bool_list set_pmf_w_list_ep_ne_1 by fastforce

lemma singleton_set_w_list_pmf_ep_eq_1:
  assumes "\<epsilon> = 1"
  shows "card (set_pmf (w_list_pmf n)) = 1"
proof (rule ccontr)
  assume "card (set_pmf (w_list_pmf n)) \<noteq> 1"
  hence "card (set_pmf (w_list_pmf n)) > 1 \<or> card (set_pmf (w_list_pmf n)) = 0"
    by linarith
  obtain x where "length x = n \<and> (\<forall>a\<in>set x. \<not> a)"
    using length_replicate by fastforce
  hence "x \<in> set_pmf (w_list_pmf n)"
    using assms set_pmf_w_list_ep_eq_1 by blast
  hence "set_pmf (w_list_pmf n) \<noteq> {}"
    by blast
  hence "card (set_pmf (w_list_pmf n)) \<noteq> 0"
    using card_empty finite_set_w_list_pmf
    by simp
  hence "card (set_pmf (w_list_pmf n)) > 1"
    using \<open>1 < card (set_pmf (local.w_list_pmf n)) \<or> card (set_pmf (local.w_list_pmf n)) = 0\<close> by auto
  hence "card (set_pmf (w_list_pmf n)) \<ge> 2"
    by linarith
  then obtain y where "y \<in> set_pmf (w_list_pmf n)" "y \<noteq> x"
    by (smt Collect_cong One_nat_def \<open>card (set_pmf (local.w_list_pmf n)) \<noteq> 1\<close> 
\<open>x \<in> set_pmf (local.w_list_pmf n)\<close> card.insert card_empty empty_iff finite.intros(1) mem_Collect_eq 
set_pmf_eq set_return_pmf singletonD singletonI)
  hence "length y = n \<and> (\<forall>a\<in>set y. \<not> a)"
    using assms set_pmf_w_list_ep_eq_1 by blast
  have "n \<noteq> 0"
    using \<open>length x = n \<and> (\<forall>a\<in>set x. \<not> a)\<close> \<open>length y = n \<and> (\<forall>a\<in>set y. \<not> a)\<close> \<open>y \<noteq> x\<close> by auto
  then obtain i where "i < n \<and> x ! i \<noteq> y ! i"
    using \<open>length x = n \<and> (\<forall>a\<in>set x. \<not> a)\<close> \<open>length y = n \<and> (\<forall>a\<in>set y. \<not> a)\<close> \<open>y \<noteq> x\<close> nth_equalityI
    by smt 
  hence "x ! i \<in> set x \<and> y ! i \<in> set y"
    by (simp add: \<open>i < n \<and> x ! i \<noteq> y ! i\<close> \<open>length x = n \<and> (\<forall>a\<in>set x. \<not> a)\<close> \<open>length y = n \<and> (\<forall>a\<in>set y. \<not> a)\<close>)
  hence "\<not> (x ! i) \<and> \<not> (y ! i)"
    using \<open>length x = n \<and> (\<forall>a\<in>set x. \<not> a)\<close> \<open>length y = n \<and> (\<forall>a\<in>set y. \<not> a)\<close> by blast
  thus "False"
    using \<open>i < n \<and> x ! i \<noteq> y ! i\<close> by auto
qed

lemma nth_w_expectation_eq_minus_epsilon_aux:
 shows "finite (set_pmf 
    (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (\<lambda>x. return_pmf (x # l))))"
proof (cases "\<epsilon> = 1")
  case True
    have "set_pmf (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (\<lambda>x. return_pmf (x # l))) 
= (\<Union>M\<in>set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). set_pmf ((\<lambda>x. return_pmf (x # l)) M))"
      by simp
    also have "... 
= (\<Union>M\<in>set_pmf (bernoulli_pmf 0). set_pmf ((\<lambda>x. return_pmf (x # l)) M))"
      by (simp add: True)
    also have "... 
= (\<Union>M\<in>{False}. set_pmf ((\<lambda>x. return_pmf (x # l)) M))"
      by (smt Diff_UNIV Diff_eq_empty_iff Diff_insert0 UNIV_bool insert_commute mem_Collect_eq 
pmf_bernoulli_True set_pmf_eq set_pmf_subset_singleton set_return_pmf)
    also have "... 
      = set_pmf (return_pmf (False # l))"
      by simp
  then show ?thesis
    by simp 
next
  case False
    have "set_pmf (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (\<lambda>x. return_pmf (x # l))) 
= (\<Union>M\<in>set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). set_pmf ((\<lambda>x. return_pmf (x # l)) M))"
      by simp
    also have "... 
= (\<Union>M\<in>{True, False}. set_pmf ((\<lambda>x. return_pmf (x # l)) M))"
      using False ep_gt_0 ep_le_1 by auto
    also have "... 
      = set_pmf (return_pmf (True # l)) \<union> set_pmf (return_pmf (False # l))"
      by simp
  then show ?thesis
    by simp 
qed

lemma nth_w_expectation_eq_minus_epsilon_aux2: 
  assumes "a \<in> set_pmf (w_list_pmf n)" 
  shows "measure_pmf.expectation (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
(\<lambda>x. return_pmf (x # a))) (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l 0)) = - \<epsilon>"
  using assms 
proof -
  have "measure_pmf.expectation (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
(\<lambda>x. return_pmf (x # a))) (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l 0)) 
= (\<Sum>b\<in>{True, False}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) b
                    *\<^sub>R measure_pmf.expectation (return_pmf (b#a)) 
                      (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l 0)))"
    apply -
    apply (rule pmf_expectation_bind)
    apply simp
     apply auto[1]
    by (simp add: subset_eq)
also have "... = ((1 - \<epsilon>) / 2)
                    *\<^sub>R measure_pmf.expectation (return_pmf [True]) 
                      (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l 0)) +
                 (1 - (1 - \<epsilon>) / 2)
                    *\<^sub>R measure_pmf.expectation (return_pmf [False]) 
                      (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l 0))"
  using ep_gt_0 ep_le_1 pmf_bernoulli_True pmf_bernoulli_False
  by auto
also have "... = ((1 - \<epsilon>) / 2)
                    *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l 0)) [True] +
                 (1 - (1 - \<epsilon>) / 2)
                    *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l 0)) [False]"
  by auto
also have "... = ((1 - \<epsilon>) / 2) *\<^sub>R 1 +
                 (1 - (1 - \<epsilon>) / 2) *\<^sub>R (-1)"
  by auto 
  finally have "... = - \<epsilon>"
    by simp
  thus ?thesis
    using \<open>((1 - \<epsilon>) / 2) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. l ! 0)) [True] + (1 - (1 - \<epsilon>) / 2) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. l ! 0)) [False] = ((1 - \<epsilon>) / 2) *\<^sub>R 1 + (1 - (1 - \<epsilon>) / 2) *\<^sub>R - 1\<close> \<open>((1 - \<epsilon>) / 2) *\<^sub>R measure_pmf.expectation (return_pmf [True]) (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. l ! 0)) + (1 - (1 - \<epsilon>) / 2) *\<^sub>R measure_pmf.expectation (return_pmf [False]) (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. l ! 0)) = ((1 - \<epsilon>) / 2) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. l ! 0)) [True] + (1 - (1 - \<epsilon>) / 2) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. l ! 0)) [False]\<close> \<open>(\<Sum>b\<in>{True, False}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) b *\<^sub>R measure_pmf.expectation (return_pmf (b # a)) (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. l ! 0))) = ((1 - \<epsilon>) / 2) *\<^sub>R measure_pmf.expectation (return_pmf [True]) (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. l ! 0)) + (1 - (1 - \<epsilon>) / 2) *\<^sub>R measure_pmf.expectation (return_pmf [False]) (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. l ! 0))\<close> \<open>measure_pmf.expectation (bernoulli_pmf ((1 - \<epsilon>) / 2) \<bind> (\<lambda>x. return_pmf (x # a))) (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. l ! 0)) = (\<Sum>b\<in>{True, False}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) b *\<^sub>R measure_pmf.expectation (return_pmf (b # a)) (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. l ! 0)))\<close> by linarith
qed

lemma Sum_dist:
  assumes "A = B \<union> C" "finite A" "B \<inter> C = {}" 
  shows "(\<Sum>a\<in> A. f a) = (\<Sum>a\<in> B. f a) + (\<Sum>a\<in> C. f a)"
  by (metis Un_infinite assms(1) assms(2) assms(3) infinite_Un sum.union_disjoint)

lemma sum_set_w_list_eq_1:
  "(\<Sum>a\<in> set_pmf (w_list_pmf n). pmf (w_list_pmf n) a) = 1"
  by (simp add: finite_set_w_list_pmf sum_pmf_eq_1)

lemma nth_w_expectation_eq_minus_epsilon_aux3:
  assumes "i < n"
  shows "(\<Sum>a\<in> set_pmf (w_list_pmf n). 
      pmf (w_list_pmf n) a *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)) = - \<epsilon>"
  using assms
proof (induction n arbitrary: i)
  case 0
  then show ?case
    by blast
next
  case (Suc n)
  hence st:"\<forall>j < n. (\<Sum>a\<in> set_pmf (w_list_pmf n). 
      pmf (w_list_pmf n) a *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! j)) = - \<epsilon>"  
    by blast
  have "w_list_pmf (Suc n)
          = bind_pmf (w_list_pmf n) 
            (\<lambda>xs. bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (\<lambda>x.
            return_pmf (x # xs)))"
    by simp
  hence "(\<Sum>a\<in> set_pmf (w_list_pmf (Suc n)).
         pmf (w_list_pmf (Suc n)) a *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)) 
      = (\<Sum>a\<in> set_pmf (w_list_pmf (Suc n)).
        (\<integral>y. 
          pmf (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (\<lambda>x.
            return_pmf (x # y))) a 
          \<partial>measure_pmf 
          (w_list_pmf n)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
    by (simp add: pmf_bind)
hence "(\<Sum>a\<in> set_pmf (w_list_pmf (Suc n)).
         pmf (w_list_pmf (Suc n)) a *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)) 
      = (\<Sum>a\<in> set_pmf (w_list_pmf (Suc n)).
        (\<integral>y. 
            (\<integral>x. pmf (return_pmf (x # y)) a 
            \<partial>measure_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)))
          \<partial>measure_pmf (w_list_pmf n)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
  by (simp add: pmf_bind)
hence 0:"(\<Sum>a\<in> set_pmf (w_list_pmf (Suc n)).
         pmf (w_list_pmf (Suc n)) a *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)) 
      = (\<Sum>a\<in> set_pmf (w_list_pmf (Suc n)).
        (\<integral>y. 
            (\<integral>x. (\<lambda>x. pmf (return_pmf (x # y)) a) x 
            \<partial>measure_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)))
          \<partial>measure_pmf (w_list_pmf n)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
  by simp
  have "finite {True,False}" by simp
  have "\<And>a. a \<in> set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) \<Longrightarrow> a \<in> {True, False}"
    by simp
  hence 1:"\<And>f y a. (\<integral>x. (\<lambda>x. pmf (return_pmf (x # y)) a) x 
            \<partial>measure_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2))) = 
(\<Sum>x\<in>{True, False}. (\<lambda>x. pmf (return_pmf (x # y)) a) x * pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) x)" using integral_measure_pmf_real \<open>finite {True,False}\<close>
    by blast
  hence "\<And>a. (\<integral>y. 
            (\<integral>x. (\<lambda>x. pmf (return_pmf (x # y)) a) x 
            \<partial>measure_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)))
          \<partial>measure_pmf (w_list_pmf n)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)
      = (\<integral>y. 
            (\<Sum>x\<in>{True, False}. (\<lambda>x. pmf (return_pmf (x # y)) a) x * pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) x)
          \<partial>measure_pmf (w_list_pmf n)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)"
    by simp
  hence 2:" (\<Sum>a\<in> set_pmf (w_list_pmf (Suc n)).
        (\<integral>y. 
            (\<integral>x. (\<lambda>x. pmf (return_pmf (x # y)) a) x 
            \<partial>measure_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)))
          \<partial>measure_pmf (w_list_pmf n)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))
=(\<Sum>a\<in> set_pmf (w_list_pmf (Suc n)).
        (\<integral>y. 
            (\<Sum>x\<in>{True, False}. (\<lambda>x. pmf (return_pmf (x # y)) a) x * pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) x)
          \<partial>measure_pmf (w_list_pmf n)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
    by simp
  have "\<And>a y. (\<Sum>x\<in>{True, False}. (\<lambda>x. pmf (return_pmf (x # y)) a) x * pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) x) = 
  (\<lambda>x. pmf (return_pmf (x # y)) a) True * pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) True
              + (\<lambda>x. pmf (return_pmf (x # y)) a) False * pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) False"
        by (smt finite.emptyI finite.insertI insertCI insert_absorb singleton_insert_inj_eq sum.empty sum.insert)
    hence " \<And>a.
        (\<integral>y. 
            (\<Sum>x\<in>{True, False}. (\<lambda>x. pmf (return_pmf (x # y)) a) x * pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) x)
          \<partial>measure_pmf (w_list_pmf n)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)
        = 
        (\<integral>y. 
             (\<lambda>x. pmf (return_pmf (x # y)) a) True * pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) True
            + (\<lambda>x. pmf (return_pmf (x # y)) a) False * pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) False
          \<partial>measure_pmf (w_list_pmf n)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)"
      by auto
    hence 3:"(\<Sum>a\<in> set_pmf (w_list_pmf (Suc n)).
        (\<integral>y. 
            (\<Sum>x\<in>{True, False}. (\<lambda>x. pmf (return_pmf (x # y)) a) x * pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) x)
          \<partial>measure_pmf (w_list_pmf n)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))
          =(\<Sum>a\<in> set_pmf (w_list_pmf (Suc n)).
        (\<integral>y. 
             (\<lambda>x. pmf (return_pmf (x # y)) a) True * pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) True
            + (\<lambda>x. pmf (return_pmf (x # y)) a) False * pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) False
          \<partial>measure_pmf (w_list_pmf n)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
      by simp
    also have "...
          =(\<Sum>a\<in> set_pmf (w_list_pmf (Suc n)).
        (\<integral>y. 
             (\<lambda>x. pmf (return_pmf (x # y)) a) True * ((1 - \<epsilon>) / 2)
            + (\<lambda>x. pmf (return_pmf (x # y)) a) False * (1 - (1 - \<epsilon>) / 2)
          \<partial>measure_pmf (w_list_pmf n)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
      using pmf_bernoulli_True pmf_bernoulli_False ep_gt_0 ep_le_1    
      by auto
also have "...
          =(\<Sum>a\<in> set_pmf (w_list_pmf (Suc n)).
        (\<integral>y. 
              pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
            + pmf (return_pmf (False # y)) a * (1 - (1 - \<epsilon>) / 2)
          \<partial>measure_pmf (w_list_pmf n)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
  by auto
finally have f1:"...
          =(\<Sum>a\<in> set_pmf (w_list_pmf (Suc n)).
        (\<integral>y. 
              pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
            + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2)
          \<partial>measure_pmf (w_list_pmf n)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
  by (simp add: honest_slot)
 have f2:"\<forall>a. (\<integral>y. 
              pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
            + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2)
          \<partial>measure_pmf (w_list_pmf n))  = (\<Sum>y\<in>(set_pmf (w_list_pmf n)). 
(pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
 + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
 * pmf (w_list_pmf n) y)"
 proof -
  have "finite (set_pmf (w_list_pmf n))"
    by (simp add: finite_set_w_list_pmf)
  thus ?thesis
    using integral_measure_pmf_real by blast
qed
  hence f3:"(\<Sum>a\<in> set_pmf (w_list_pmf (Suc n)).
        (\<integral>y. 
              pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
            + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2)
          \<partial>measure_pmf (w_list_pmf n)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))
= (\<Sum>a\<in> set_pmf (w_list_pmf (Suc n)).
        (\<Sum>y\<in>(set_pmf (w_list_pmf n)). 
(pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
 + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
 * pmf (w_list_pmf n) y) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)) "
    by simp
  have f4:"(\<Sum>a\<in> set_pmf (w_list_pmf (Suc n)).
        (\<Sum>y\<in>(set_pmf (w_list_pmf n)). 
(pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
 + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
 * pmf (w_list_pmf n) y) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)) = -\<epsilon>"
  proof (cases "\<epsilon> = 1")
    case True
    hence "set_pmf (w_list_pmf (Suc n)) = {l:: bool list. size l = Suc n \<and> (\<forall>x \<in> set l. \<not> x)}"
      using set_pmf_w_list_ep_eq_1 by auto
    have "set_pmf (w_list_pmf n) = {l:: bool list. size l = n \<and> (\<forall>x \<in> set l. \<not> x)}"
      using True set_pmf_w_list_ep_eq_1 by auto
    have c:"card (set_pmf (w_list_pmf (Suc n))) = 1 \<and> card (set_pmf (w_list_pmf n)) = 1"
      using singleton_set_w_list_pmf_ep_eq_1 True by simp
    obtain a y where ay: "a \<in> set_pmf (w_list_pmf (Suc n)) \<and> y \<in> set_pmf (w_list_pmf n)"
      by (meson empty_iff c set_pmf_not_empty subsetI subset_antisym)
    have single:"set_pmf (w_list_pmf (Suc n)) = {a} \<and> set_pmf (w_list_pmf n) = {y}" using c
      by (metis ay card_1_singletonE singletonD)
    hence "a = False # y"
    proof -
      obtain ha ta where hta:"a = ha#ta"
        using ay by auto
      hence "ha = False"
        using True ay set_pmf_w_list_ep_eq_1 by auto
      have "size ta = n \<and> (\<forall>x \<in> set ta. \<not> x)"
        using \<open>a = ha # ta\<close> \<open>set_pmf (local.w_list_pmf (Suc n)) = {l. length l = Suc n \<and> (\<forall>x\<in>set l. \<not> x)}\<close> ay length_Cons by auto
      hence "ta \<in> set_pmf (w_list_pmf n)"
        using \<open>set_pmf (local.w_list_pmf n) = {l. length l = n \<and> (\<forall>x\<in>set l. \<not> x)}\<close> by blast
      thus ?thesis using hta
        using \<open>ha = False\<close> single by blast
    qed
    hence "(\<Sum>a\<in> set_pmf (w_list_pmf (Suc n)).
        (\<Sum>y\<in>(set_pmf (w_list_pmf n)). 
(pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
 + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
 * pmf (w_list_pmf n) y) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)) =
(\<Sum>a\<in> {False#y}.
        (\<Sum>y\<in>{y}. 
(pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
 + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
 * pmf (w_list_pmf n) y) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
      using single by presburger
  also have "... =
(pmf (return_pmf (True # y)) (False#y) * ((1 - \<epsilon>) / 2)
 + pmf (return_pmf (False # y)) (False#y) * ((1 + \<epsilon>) / 2))
 * pmf (w_list_pmf n) y
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) ((False#y) ! i)"
    by (simp add: cancel_comm_monoid_add_class.diff_cancel)
 also have "... =
(0 * ((1 - \<epsilon>) / 2)
 + 1 * ((1 + \<epsilon>) / 2))
 * (\<Sum>y\<in>{y}.pmf (w_list_pmf n) y)
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) ((False#y) ! i)"
   by simp
also have "... =
(1 + \<epsilon>) / 2
 * (\<Sum>y\<in>set_pmf (w_list_pmf n).pmf (w_list_pmf n) y)
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) ((False#y) ! i)"
  using single by auto
also have "... =
(1 + \<epsilon>) / 2
 * 1
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) ((False#y) ! i)"
  by (simp add: sum_set_w_list_eq_1)
 also have "... =
(1 + \<epsilon>) / 2
 * 1
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) ((False#y) ! i)"
  by (simp add: sum_set_w_list_eq_1)
  finally have "... = - \<epsilon>"
  proof (cases i)
    case 0
    then show ?thesis
      by (simp add: True) 
  next
    case (Suc k)
    hence "(False # y) ! i = y ! k"
      by simp
    hence " 
      pmf (w_list_pmf n) y *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (y ! k) = - \<epsilon>"
      using st
      using Suc Suc.prems single by auto
hence " (\<Sum>y\<in> set_pmf (w_list_pmf n).
      pmf (w_list_pmf n) y) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (y ! k) = - \<epsilon>"
  using single by auto
    then show ?thesis
      using True \<open>(False # y) ! i = y ! k\<close> sum_set_w_list_eq_1 zero_neq_numeral by auto 
  qed
  thus ?thesis
    using \<open>(0 * ((1 - \<epsilon>) / 2) + 1 * ((1 + \<epsilon>) / 2)) * sum (pmf (local.w_list_pmf n)) {y} *\<^sub>R 
(real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) ((False # y) ! i) = (1 + \<epsilon>) / 2 * sum (pmf 
(local.w_list_pmf n)) (set_pmf (local.w_list_pmf n)) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 
else 1))) ((False # y) ! i)\<close> \<open>(1 + \<epsilon>) / 2 * sum (pmf (local.w_list_pmf n)) (set_pmf (local.w_list_pmf 
n)) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) ((False # y) ! i) = (1 + \<epsilon>) / 2 * 1 *\<^sub>R 
(real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) ((False # y) ! i)\<close> \<open>(\<Sum>a\<in>set_pmf (local.w_list_pmf 
(Suc n)). (\<Sum>y\<in>set_pmf (local.w_list_pmf n). (pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2) + pmf 
(return_pmf (False # y)) a * ((1 + \<epsilon>) / 2)) * pmf (local.w_list_pmf n) y) *\<^sub>R (real_of_int \<circ> 
(\<lambda>b. (- 1) ^ (if b then 0 else 1))) (a ! i)) = (\<Sum>a\<in>{False # y}. (\<Sum>y\<in>{y}. (pmf (return_pmf 
(True # y)) a * ((1 - \<epsilon>) / 2) + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2)) * pmf 
(local.w_list_pmf n) y) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) (a ! i))\<close> 
\<open>(\<Sum>a\<in>{False # y}. (\<Sum>y\<in>{y}. (pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2) + pmf (return_pmf 
(False # y)) a * ((1 + \<epsilon>) / 2)) * pmf (local.w_list_pmf n) y) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ 
(if b then 0 else 1))) (a ! i)) = (pmf (return_pmf (True # y)) (False # y) * ((1 - \<epsilon>) / 2) + pmf 
(return_pmf (False # y)) (False # y) * ((1 + \<epsilon>) / 2)) * pmf (local.w_list_pmf n) y *\<^sub>R 
(real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) ((False # y) ! i)\<close> 
\<open>(pmf (return_pmf (True # y)) (False # y) * ((1 - \<epsilon>) / 2) + pmf (return_pmf (False # y)) 
(False # y) * ((1 + \<epsilon>) / 2)) * pmf (local.w_list_pmf n) y *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ 
(if b then 0 else 1))) ((False # y) ! i) = (0 * ((1 - \<epsilon>) / 2) + 1 * ((1 + \<epsilon>) / 2)) * sum (pmf 
(local.w_list_pmf n)) {y} *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) ((False # y) ! i)\<close> 
    by linarith
next
  case False
  hence "\<epsilon> < 1"
    using ep_le_1 by auto
  hence dist:"set_pmf (w_list_pmf n) = {l:: bool list. size l = n} \<and> set_pmf (w_list_pmf (Suc n)) = {l:: bool list. size l = Suc n}"
    using set_pmf_w_list_ep_ne_1 by blast
  have "{l:: bool list. size l = Suc n} = {l:: bool list. size l = Suc n \<and> ((hd l = True) \<or> (hd l = False))}"
    by simp
  hence "{l:: bool list. size l = Suc n} = 
{l:: bool list. size l = Suc n \<and> (hd l = True) \<or> 
size l = Suc n \<and> (hd l = False)}"
    by blast
   hence u:"{l:: bool list. size l = Suc n} = 
{l:: bool list. size l = Suc n \<and> (hd l = True)} \<union> 
{l:: bool list. size l = Suc n \<and> (hd l = False)}"
     by blast
   have "{l:: bool list. size l = Suc n \<and> (hd l = True)} =
{l:: bool list. \<exists> k t. l = k # t \<and> size t = n \<and> (k = True)}"
     by (metis Collect_cong Suc_length_conv list.sel(1))
hence T:
"{l:: bool list. size l = Suc n \<and> (hd l = True)} =
(Cons True) `  {l:: bool list. size l = n}"
  by auto
have "{l:: bool list. size l = Suc n \<and> (hd l = False)} =
{l:: bool list. \<exists> k t. l = k # t \<and> size t = n \<and> (k = False)}"
     by (metis Collect_cong Suc_length_conv list.sel(1))
hence F:
"{l:: bool list. size l = Suc n \<and> (hd l = False)} =
(Cons False) `  {l:: bool list. size l = n}"
  by auto
  hence uni:"{l:: bool list. size l = Suc n} = 
(Cons True) `  {l:: bool list. size l = n} \<union> 
(Cons False) `  {l:: bool list. size l = n}"
    using T u
    by simp
  have int:"(Cons True) `  {l:: bool list. size l = n} \<inter> 
(Cons False) `  {l:: bool list. size l = n} = {}"
    by blast
  have "(\<Sum>a\<in> set_pmf (w_list_pmf (Suc n)).
        (\<Sum>y\<in>(set_pmf (w_list_pmf n)). 
(pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
 + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
 * pmf (w_list_pmf n) y) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)) = 
(\<Sum>a\<in> set_pmf (w_list_pmf (Suc n)).
        (\<Sum>y\<in>{l:: bool list. size l = n}. 
(pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
 + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
 * pmf (w_list_pmf n) y) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
    using dist by simp
  also have "... = 
(\<Sum>a\<in> {l:: bool list. size l = Suc n}.
        (\<Sum>y\<in>{l:: bool list. size l = n}. 
(pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
 + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
 * pmf (w_list_pmf n) y) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
    using dist
    by (simp add: dist)
  finally have "... = -\<epsilon>"
  proof -
    have st:"(\<Sum>a\<in> {l:: bool list. size l = Suc n}.
        (\<Sum>y\<in>{l:: bool list. size l = n}. 
(pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
 + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
 * pmf (w_list_pmf n) y) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)) =
(\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}.
        (\<Sum>y\<in>{l:: bool list. size l = n}.
(pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
 + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
 * pmf (w_list_pmf n) y) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))
+ (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}.
        (\<Sum>y\<in>{l:: bool list. size l = n}.
(pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
 + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
 * pmf (w_list_pmf n) y) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
      using Sum_dist
      by (smt dist finite_set_w_list_pmf int uni)
    have "\<forall>a \<in> (Cons True) `  {l:: bool list. size l = n}.
\<forall>y. pmf (return_pmf (False # y)) a = 0"
      by auto
    hence tmp:"\<forall>a \<in> (Cons True) `  {l:: bool list. size l = n}.
\<forall>y \<in> {l:: bool list. size l = n}. pmf (return_pmf (False # y)) a = 0"
      by auto
    hence tmp2:"\<forall>f g. (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}.
  g a (\<Sum>y\<in>{l:: bool list. size l = n}. f a y (pmf (return_pmf (False # y)) a)))
  = (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}.
  g a (\<Sum>y\<in>{l:: bool list. size l = n}. f a y 0))"
    proof -
      have "\<forall>bs. bs \<in> op # True ` {bs. length bs = n} \<longrightarrow> (\<forall>bsa. bsa \<in> {bs. length bs = n} \<longrightarrow> pmf (return_pmf (False # bsa)) bs = 0)"
        using tmp by blast
      then show ?thesis
        by force
    qed
    define f where "f = (\<lambda>a y x.
(pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
 + x * ((1 + \<epsilon>) / 2))
 * pmf (w_list_pmf n) y)"
hence "\<forall>g. (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}.
       g a (\<Sum>y\<in>{l:: bool list. size l = n}.
       f a y (pmf (return_pmf (False # y)) a))) 
       = (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}.
       g a (\<Sum>y\<in>{l:: bool list. size l = n}.
       f a y 0))"
      proof -
        show ?thesis
          using tmp2 by blast
      qed
hence "(\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}.
       (\<Sum>y\<in>{l:: bool list. size l = n}.
       f a y (pmf (return_pmf (False # y)) a)) *\<^sub>R 
        (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)) 
       = (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}.
        (\<Sum>y\<in>{l:: bool list. size l = n}. 
       f a y 0)  *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
  by (smt sum.cong tmp)
hence t1:"(\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}.
        (\<Sum>y\<in>{l:: bool list. size l = n}.
      (pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
      + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
      * pmf (w_list_pmf n) y) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))
      = (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}.
        (\<Sum>y\<in>{l:: bool list. size l = n}.
      (pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
      + 0 * ((1 + \<epsilon>) / 2))
      * pmf (w_list_pmf n) y) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
  by (smt real_scaleR_def real_vector.scale_zero_left sum.cong tmp)
  also have t2:"... = (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}.
        (\<Sum>y\<in>{l:: bool list. size l = n}.
      (pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2))
      * pmf (w_list_pmf n) y) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
    by auto
also have t3:"... = (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}.
        (\<Sum>y\<in> {tl a} \<union> {l:: bool list. l \<noteq> tl a \<and> size l = n} .
      (pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2))
      * pmf (w_list_pmf n) y) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
proof -
  have "\<forall> a \<in> (Cons True) `  {l:: bool list. size l = n}. a = True # tl a"
    by simp
  hence "\<forall> a \<in> (Cons True) `  {l:: bool list. size l = n}. 
tl a \<in> {l:: bool list. size l = n}"
    by simp
 hence "\<forall> a \<in> (Cons True) `  {l:: bool list. size l = n}. {l:: bool list. size l = n} = {tl a} \<union> {l:: bool list.  l \<noteq> tl a \<and> size l = n}"
   by blast
  thus ?thesis
    by (metis (no_types, lifting) sum.cong)
qed
    also have t4:"... = (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}.
        ((\<Sum>y\<in> {tl a}.
      (pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2))
      * pmf (w_list_pmf n) y) + (\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n} .
      (pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2))
      * pmf (w_list_pmf n) y)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
    proof -
      have emp:"\<forall>a\<in> (Cons True) `  {l:: bool list. size l = n}. {tl a} \<inter> {l:: bool list. l \<noteq> tl a \<and> size l = n} = {}"
        by simp
      have "\<forall>a\<in> (Cons True) `  {l:: bool list. size l = n}. finite ({tl a} \<union> {l:: bool list. l \<noteq> tl a \<and> size l = n})"
        using finite_set_fixed_size_bool_list by auto
      hence "\<forall>a\<in> (Cons True) `  {l:: bool list. size l = n}.
        (\<Sum>y\<in> {tl a} \<union> {l:: bool list. l \<noteq> tl a \<and> size l = n} .
      (pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2))
      * pmf (w_list_pmf n) y) = (\<Sum>y\<in> {tl a}.
      (pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2))
      * pmf (w_list_pmf n) y) + (\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n} .
      (pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2))
      * pmf (w_list_pmf n) y)"
        using Sum_dist emp
        by simp
      thus ?thesis
        by (smt real_vector.scale_cancel_right sum.cong)
    qed
also have t5:"... = (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}.
        ((\<Sum>y\<in> {tl a}.
      (pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2))
      * pmf (w_list_pmf n) y) + (\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n} .
      (pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2))
      * pmf (w_list_pmf n) y)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
  by blast
  finally have t6:"... = (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}.
        
      ((\<Sum>y\<in> {tl a}.pmf (return_pmf (True # (tl a))) a * (1 - \<epsilon>) / 2 * pmf (w_list_pmf n) (tl a)) + 

(\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n} .
      (pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2))
      * pmf (w_list_pmf n) y)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
  by auto
  also have t7:"... = (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}.
      (pmf (return_pmf (True # (tl a))) a * (1 - \<epsilon>) / 2 * pmf (w_list_pmf n) (tl a) + 
(\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n} .
      (pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2))
      * pmf (w_list_pmf n) y)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
    by auto
also have t8:"... = (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}.  
      pmf (return_pmf (True # (tl a))) a * (1 - \<epsilon>) / 2 * pmf (w_list_pmf n) (tl a)
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
proof -
  have "\<forall>a\<in> (Cons True) `  {l:: bool list. size l = n}. \<forall>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n}.
        pmf (return_pmf (True # y)) a  = 0"
    by simp
have "\<forall>a\<in> (Cons True) `  {l:: bool list. size l = n}. \<forall>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n}.
        pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
      * pmf (w_list_pmf n) y = 0"
    by simp
  hence "(\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}. 
        (\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n}.
        pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
      * pmf (w_list_pmf n) y)) = 0"
    using sum_not_0 by blast
  hence "(\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}. 
        (\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n}.
        pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
      * pmf (w_list_pmf n) y) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)) = 0"
    using sum_not_0 by fastforce
  hence "(\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}. 
     pmf (return_pmf (True # (tl a))) a * (1 - \<epsilon>) / 2 * pmf (w_list_pmf n) (tl a)
      *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))
+
(\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}. 
        (\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n}.
        pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
      * pmf (w_list_pmf n) y) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))
= 
(\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}. 
     pmf (return_pmf (True # (tl a))) a * (1 - \<epsilon>) / 2 * pmf (w_list_pmf n) (tl a)
      *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
    by simp
hence last_aux:"(\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}. 
     pmf (return_pmf (True # (tl a))) a * (1 - \<epsilon>) / 2 * pmf (w_list_pmf n) (tl a)
      *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)
+
        (\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n}.
        pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
      * pmf (w_list_pmf n) y) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))
= 
(\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}. 
     pmf (return_pmf (True # (tl a))) a * (1 - \<epsilon>) / 2 * pmf (w_list_pmf n) (tl a)
      *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
  by (simp add: sum.distrib)
  have "(\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}. 
     pmf (return_pmf (True # (tl a))) a * (1 - \<epsilon>) / 2 * pmf (w_list_pmf n) (tl a)
      *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)
+

        (\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n}.
        pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
      * pmf (w_list_pmf n) y) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))
= 
(\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}. 
     (pmf (return_pmf (True # (tl a))) a * (1 - \<epsilon>) / 2 * pmf (w_list_pmf n) (tl a)
      +
     (\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n}.
     pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
      * pmf (w_list_pmf n) y))
      *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
proof -
  have di:"\<forall>(a::real) (b::real) (c::real).  a *\<^sub>R c + b *\<^sub>R c = (a+b) *\<^sub>R c"
    by (simp add: ring_class.ring_distribs(2))
  define g1 where 
g1:"g1 = (\<lambda>x. pmf (return_pmf (True # (tl x))) x * (1 - \<epsilon>) / 2 * pmf (w_list_pmf n) (tl x))"
  define g2 where 
g2:"g2 = (\<lambda>x. (\<Sum>y\<in> {l:: bool list. l \<noteq> tl x \<and> size l = n}.
        pmf (return_pmf (True # y)) x * ((1 - \<epsilon>) / 2)
      * pmf (w_list_pmf n) y))" 
  define g3 where 
g3:"g3 = (\<lambda>x. (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (x ! i))"
  have "\<forall>a\<in> (Cons True) `  {l:: bool list. size l = n}. 
(g1 a *\<^sub>R g3 a) + (g2 a *\<^sub>R g3 a)
     = (g1 a + g2 a) *\<^sub>R g3 a"
    using g1 g2 g3 di
    by blast
  hence "(\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}.
      (g1 a *\<^sub>R g3 a) + (g2 a *\<^sub>R g3 a))
     = (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}. (g1 a + g2 a) *\<^sub>R g3 a)"
    using di by auto
  thus ?thesis
    using g1 g2 g3
    by (simp add: Groups.mult_ac(2) Groups.mult_ac(3))
qed
  thus ?thesis
    using last_aux by linarith
qed
  also have t9:"... = (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}. 
          (1 - \<epsilon>) / 2 * pmf (w_list_pmf n) (tl a)
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
  proof - 
    have "\<forall>a\<in> (Cons True) `  {l:: bool list. size l = n}. pmf (return_pmf (True # (tl a))) a = 1"
      by simp
    thus ?thesis
      by (metis (no_types, lifting) calculation real_scaleR_def scaleR_one sum.cong t1 t3 times_divide_eq_right)
  qed
also have t10:"... = (1 - \<epsilon>) / 2 * (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}. 
           pmf (w_list_pmf n) (tl a)
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
  by (simp add: sum_distrib_left)
also have t11:"... = (1 - \<epsilon>) / 2 * (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}. 
           pmf (w_list_pmf n) (tl a)
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
  by (simp add: sum_distrib_left)
    hence h1:"(\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}.
        (\<Sum>y\<in>{l:: bool list. size l = n}.
(pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
 + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
 * pmf (w_list_pmf n) y) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))
= (1 - \<epsilon>) / 2 * (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}. 
pmf (w_list_pmf n) (tl a) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
      using calculation t1 t2 t3 t4 by linarith
    have h2:"(\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}.
        (\<Sum>y\<in>{l:: bool list. size l = n}.
(pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
 + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
 * pmf (w_list_pmf n) y) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))
= (1 + \<epsilon>) / 2 * (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}. 
pmf (w_list_pmf n) (tl a) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
    proof -
      have tmp3:"\<forall>a \<in> (Cons False) `  {l:: bool list. size l = n}.
\<forall>y \<in> {l:: bool list. size l = n}. pmf (return_pmf (True # y)) a = 0"
      by auto
    hence tmp4:"\<forall>f g. (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}.
  g a (\<Sum>y\<in>{l:: bool list. size l = n}. f a y (pmf (return_pmf (True # y)) a)))
  = (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}.
  g a (\<Sum>y\<in>{l:: bool list. size l = n}. f a y 0))"
      by (smt sum.cong)
    define f where "f = (\<lambda>a y x.
(x * ((1 - \<epsilon>) / 2)
 + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
 * pmf (w_list_pmf n) y)"
hence "\<forall>g. (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}.
       g a (\<Sum>y\<in>{l:: bool list. size l = n}.
       f a y (pmf (return_pmf (True # y)) a))) 
       = (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}.
       g a (\<Sum>y\<in>{l:: bool list. size l = n}.
       f a y 0))"
      proof -
        show ?thesis
          using tmp4 by blast
      qed
hence tt:"(\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}.
       (\<Sum>y\<in>{l:: bool list. size l = n}.
       f a y (pmf (return_pmf (True # y)) a)) *\<^sub>R 
        (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)) 
       = (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}.
        (\<Sum>y\<in>{l:: bool list. size l = n}. 
       f a y 0)  *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
  by (smt sum.cong tmp3)
hence t11:"(\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}.
        (\<Sum>y\<in>{l:: bool list. size l = n}.
      (pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2)
      + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
      * pmf (w_list_pmf n) y) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))
      = (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}.
        (\<Sum>y\<in>{l:: bool list. size l = n}.
      (0 * ((1 - \<epsilon>) / 2)
      + pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
      * pmf (w_list_pmf n) y) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
  by (smt real_scaleR_def real_vector.scale_zero_left sum.cong tmp3)
  also have t12:"... = (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}.
        (\<Sum>y\<in>{l:: bool list. size l = n}.
      (pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
      * pmf (w_list_pmf n) y) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
    by auto
also have t13:"... = (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}.
        (\<Sum>y\<in> {tl a} \<union> {l:: bool list. l \<noteq> tl a \<and> size l = n} .
      (pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
      * pmf (w_list_pmf n) y) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
proof -
  have "\<forall> a \<in> (Cons False) `  {l:: bool list. size l = n}. a = False # tl a"
    by simp
  hence "\<forall> a \<in> (Cons False) `  {l:: bool list. size l = n}. 
tl a \<in> {l:: bool list. size l = n}"
    by simp
 hence "\<forall> a \<in> (Cons False) `  {l:: bool list. size l = n}. {l:: bool list. size l = n} = {tl a} \<union> {l:: bool list.  l \<noteq> tl a \<and> size l = n}"
   by blast
  thus ?thesis
    by (metis (no_types, lifting) sum.cong)
qed
    also have t14:"... = (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}.
        ((\<Sum>y\<in> {tl a}.
      (pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
      * pmf (w_list_pmf n) y) + (\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n} .
      (pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
      * pmf (w_list_pmf n) y)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
    proof -
      have emp:"\<forall>a\<in> (Cons False) `  {l:: bool list. size l = n}. {tl a} \<inter> {l:: bool list. l \<noteq> tl a \<and> size l = n} = {}"
        by simp
      have "\<forall>a\<in> (Cons False) `  {l:: bool list. size l = n}. finite ({tl a} \<union> {l:: bool list. l \<noteq> tl a \<and> size l = n})"
        using finite_set_fixed_size_bool_list by auto
      hence "\<forall>a\<in> (Cons False) `  {l:: bool list. size l = n}.
        (\<Sum>y\<in> {tl a} \<union> {l:: bool list. l \<noteq> tl a \<and> size l = n} .
      (pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
      * pmf (w_list_pmf n) y) = (\<Sum>y\<in> {tl a}.
      (pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
      * pmf (w_list_pmf n) y) + (\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n} .
      (pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
      * pmf (w_list_pmf n) y)"
        using Sum_dist emp
        by simp
      thus ?thesis
        by (smt real_vector.scale_cancel_right sum.cong)
    qed
also have t15:"... = (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}.
        ((\<Sum>y\<in> {tl a}.
      (pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
      * pmf (w_list_pmf n) y) + (\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n} .
      (pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
      * pmf (w_list_pmf n) y)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
  by blast
  finally have t16:"... = (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}.
      ((\<Sum>y\<in> {tl a}.pmf (return_pmf (False # (tl a))) a * (1 + \<epsilon>) / 2 * pmf (w_list_pmf n) (tl a)) 
+ (\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n} .
      (pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
      * pmf (w_list_pmf n) y)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
  by auto
  also have t17:"... = (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}.
      (pmf (return_pmf (False # (tl a))) a * (1 + \<epsilon>) / 2 * pmf (w_list_pmf n) (tl a) + 
(\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n} .
      (pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2))
      * pmf (w_list_pmf n) y)) 
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
    by auto
also have t18:"... = (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}.  
      pmf (return_pmf (False # (tl a))) a * (1 + \<epsilon>) / 2 * pmf (w_list_pmf n) (tl a)
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
proof -
  have "\<forall>a\<in> (Cons False) `  {l:: bool list. size l = n}. \<forall>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n}.
        pmf (return_pmf (False # y)) a  = 0"
    by simp
have "\<forall>a\<in> (Cons False) `  {l:: bool list. size l = n}. \<forall>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n}.
        pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2)
      * pmf (w_list_pmf n) y = 0"
    by simp
  hence tt1:"(\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}. 
        (\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n}.
        pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2)
      * pmf (w_list_pmf n) y)) = 0"
    using sum_not_0 by blast
  hence eq0:"(\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}. 
        (\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n}.
        pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2)
      * pmf (w_list_pmf n) y) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)) = 0"
    using sum_not_0 by fastforce
  hence tt2:"(\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}. 
     pmf (return_pmf (False # (tl a))) a * (1 + \<epsilon>) / 2 * pmf (w_list_pmf n) (tl a)
      *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))
+
(\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}. 
        (\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n}.
        pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2)
      * pmf (w_list_pmf n) y) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))
= 
(\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}. 
     pmf (return_pmf (False # (tl a))) a * (1 + \<epsilon>) / 2 * pmf (w_list_pmf n) (tl a)
      *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
    by simp
hence last_aux2:"(\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}. 
     pmf (return_pmf (False # (tl a))) a * (1 + \<epsilon>) / 2 * pmf (w_list_pmf n) (tl a)
      *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)
+
        (\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n}.
        pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2)
      * pmf (w_list_pmf n) y) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))
= 
(\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}. 
     pmf (return_pmf (False # (tl a))) a * (1 + \<epsilon>) / 2 * pmf (w_list_pmf n) (tl a)
      *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
  by (simp add: sum.distrib)
  have last_aux3:"(\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}. 
     pmf (return_pmf (False # (tl a))) a * (1 + \<epsilon>) / 2 * pmf (w_list_pmf n) (tl a)
      *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)
+
        (\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n}.
        pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2)
      * pmf (w_list_pmf n) y) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))
= 
(\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}. 
     (pmf (return_pmf (False # (tl a))) a * (1 + \<epsilon>) / 2 * pmf (w_list_pmf n) (tl a)
      +
     (\<Sum>y\<in> {l:: bool list. l \<noteq> tl a \<and> size l = n}.
     pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2)
      * pmf (w_list_pmf n) y))
      *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
proof -
  have di:"\<forall>(a::real) (b::real) (c::real).  a *\<^sub>R c + b *\<^sub>R c = (a+b) *\<^sub>R c"
    by (simp add: ring_class.ring_distribs(2))
  define g1 where 
g1:"g1 = (\<lambda>x. pmf (return_pmf (False # (tl x))) x * (1 + \<epsilon>) / 2 * pmf (w_list_pmf n) (tl x))"
  define g2 where 
g2:"g2 = (\<lambda>x. (\<Sum>y\<in> {l:: bool list. l \<noteq> tl x \<and> size l = n}.
        pmf (return_pmf (False # y)) x * ((1 + \<epsilon>) / 2)
      * pmf (w_list_pmf n) y))" 
  define g3 where 
g3:"g3 = (\<lambda>x. (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (x ! i))"
  have "\<forall>a\<in> (Cons False) `  {l:: bool list. size l = n}. 
(g1 a *\<^sub>R g3 a) + (g2 a *\<^sub>R g3 a)
     = (g1 a + g2 a) *\<^sub>R g3 a"
    using g1 g2 g3 di
    by blast
  hence tt4:"(\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}.
      (g1 a *\<^sub>R g3 a) + (g2 a *\<^sub>R g3 a))
     = (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}. (g1 a + g2 a) *\<^sub>R g3 a)"
    using di by auto
  thus ?thesis
    using g1 g2 g3
    by (simp add: Groups.mult_ac(2) Groups.mult_ac(3))
qed
  thus ?thesis
    using \<open>(\<Sum>a\<in>op # False ` {l. length l = n}. pmf (return_pmf (False # tl a)) a * (1 + \<epsilon>) / 2 * pmf (local.w_list_pmf n) (tl a) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) (a ! i) + (\<Sum>y\<in>{l. l \<noteq> tl a \<and> length l = n}. pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2) * pmf (local.w_list_pmf n) y) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) (a ! i)) = (\<Sum>a\<in>op # False ` {l. length l = n}. (pmf (return_pmf (False # tl a)) a * (1 + \<epsilon>) / 2 * pmf (local.w_list_pmf n) (tl a) + (\<Sum>y\<in>{l. l \<noteq> tl a \<and> length l = n}. pmf (return_pmf (False # y)) a * ((1 + \<epsilon>) / 2) * pmf (local.w_list_pmf n) y)) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) (a ! i))\<close> last_aux2 by linarith
qed
  also have t19:"... = (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}. 
          (1 + \<epsilon>) / 2 * pmf (w_list_pmf n) (tl a)
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
  proof - 
    have "\<forall>a\<in> (Cons False) `  {l:: bool list. size l = n}. pmf (return_pmf (False # (tl a))) a = 1"
      by simp
    thus ?thesis
       by (metis (no_types, lifting) calculation real_scaleR_def scaleR_one sum.cong times_divide_eq_right)
  qed
also have t20:"... = (1 + \<epsilon>) / 2 * (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}. 
           pmf (w_list_pmf n) (tl a)
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
  by (simp add: sum_distrib_left)
also have t21:"... = (1 + \<epsilon>) / 2 * (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}. 
           pmf (w_list_pmf n) (tl a)
          *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))"
  by (simp add: sum_distrib_left)
  thus ?thesis
    using calculation t11 t12 t13 t14 by linarith
qed
  have "(1 - \<epsilon>) / 2 * (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}.
pmf (w_list_pmf n) (tl a) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))
+ (1 + \<epsilon>) / 2 * (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}. 
pmf (w_list_pmf n) (tl a) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i)) = -\<epsilon>"
  proof (cases i)
    case 0
    have "(\<forall>a\<in> (Cons True) `  {l:: bool list. size l = n}. a ! i = True) \<and>
(\<forall>a\<in> (Cons False) `  {l:: bool list. size l = n}. a ! i = False)"
      using "0" by auto
    hence "(1 - \<epsilon>) / 2 * (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}. 
pmf (w_list_pmf n) (tl a) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))
+ (1 + \<epsilon>) / 2 * (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}. 
pmf (w_list_pmf n) (tl a) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! i))
= 
(1 - \<epsilon>) / 2 * (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}. 
pmf (w_list_pmf n) (tl a) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) True)
+ (1 + \<epsilon>) / 2 * (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}. 
pmf (w_list_pmf n) (tl a) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) False)"
      by (smt sum.cong)
    also have "... = 
(1 - \<epsilon>) / 2 * (\<Sum>a\<in> (Cons True) `  {l:: bool list. size l = n}. 
pmf (w_list_pmf n) (tl a) *\<^sub>R 1)
+ (1 + \<epsilon>) / 2 * (\<Sum>a\<in> (Cons False) `  {l:: bool list. size l = n}. 
pmf (w_list_pmf n) (tl a) *\<^sub>R (-1))"
  by (simp add: of_int_1 of_int_minus)
  also have "... =  
(1 - \<epsilon>) / 2 * (\<Sum>a\<in> {l:: bool list. size l = n}. 
pmf (w_list_pmf n) a *\<^sub>R 1)
+ (1 + \<epsilon>) / 2 * (\<Sum>a\<in>  {l:: bool list. size l = n}. 
pmf (w_list_pmf n) a *\<^sub>R (-1))"
    by (simp add: sum.reindex)
also have "... =  
(1 - \<epsilon>) / 2 * (\<Sum>a\<in> set_pmf (w_list_pmf n). 
pmf (w_list_pmf n) a *\<^sub>R 1)
+ (1 + \<epsilon>) / 2 * (\<Sum>a\<in>  set_pmf (w_list_pmf n). 
pmf (w_list_pmf n) a) *\<^sub>R (-1)"
    using set_pmf_w_list_ep_ne_1 False
  proof -
    have "\<forall>n. {bs. length bs = n} = set_pmf (local.w_list_pmf n)"
      using False set_pmf_w_list_ep_ne_1 by blast
    then show ?thesis
      by (metis (no_types) scaleR_left.sum)
  qed
also have "... =  
(1 - \<epsilon>) / 2  
- (1 + \<epsilon>) / 2"
  using sum_set_w_list_eq_1
    by simp
  then show ?thesis
    by (smt \<open>(1 - \<epsilon>) / 2 * (\<Sum>a\<in>op # True ` {l. length l = n}. pmf (local.w_list_pmf n) (tl a) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) (a ! i)) + (1 + \<epsilon>) / 2 * (\<Sum>a\<in>op # False ` {l. length l = n}. pmf (local.w_list_pmf n) (tl a) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) (a ! i)) = (1 - \<epsilon>) / 2 * (\<Sum>a\<in>op # True ` {l. length l = n}. pmf (local.w_list_pmf n) (tl a) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) True) + (1 + \<epsilon>) / 2 * (\<Sum>a\<in>op # False ` {l. length l = n}. pmf (local.w_list_pmf n) (tl a) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) False)\<close> \<open>(1 - \<epsilon>) / 2 * (\<Sum>a\<in>op # True ` {l. length l = n}. pmf (local.w_list_pmf n) (tl a) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) True) + (1 + \<epsilon>) / 2 * (\<Sum>a\<in>op # False ` {l. length l = n}. pmf (local.w_list_pmf n) (tl a) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) False) = (1 - \<epsilon>) / 2 * (\<Sum>a\<in>op # True ` {l. length l = n}. pmf (local.w_list_pmf n) (tl a) *\<^sub>R 1) + (1 + \<epsilon>) / 2 * (\<Sum>a\<in>op # False ` {l. length l = n}. pmf (local.w_list_pmf n) (tl a) *\<^sub>R - 1)\<close> \<open>(1 - \<epsilon>) / 2 * (\<Sum>a\<in>op # True ` {l. length l = n}. pmf (local.w_list_pmf n) (tl a) *\<^sub>R 1) + (1 + \<epsilon>) / 2 * (\<Sum>a\<in>op # False ` {l. length l = n}. pmf (local.w_list_pmf n) (tl a) *\<^sub>R - 1) = (1 - \<epsilon>) / 2 * (\<Sum>a\<in>{l. length l = n}. pmf (local.w_list_pmf n) a *\<^sub>R 1) + (1 + \<epsilon>) / 2 * (\<Sum>a\<in>{l. length l = n}. pmf (local.w_list_pmf n) a *\<^sub>R - 1)\<close> \<open>(1 - \<epsilon>) / 2 * (\<Sum>a\<in>{l. length l = n}. pmf (local.w_list_pmf n) a *\<^sub>R 1) + (1 + \<epsilon>) / 2 * (\<Sum>a\<in>{l. length l = n}. pmf (local.w_list_pmf n) a *\<^sub>R - 1) = (1 - \<epsilon>) / 2 * (\<Sum>a\<in>set_pmf (local.w_list_pmf n). pmf (local.w_list_pmf n) a *\<^sub>R 1) + (1 + \<epsilon>) / 2 * sum (pmf (local.w_list_pmf n)) (set_pmf (local.w_list_pmf n)) *\<^sub>R - 1\<close> honest_slot real_sum_of_halves)
next
  case (Suc k)
  have t22:"(1 - \<epsilon>) / 2 *
           (\<Sum>a\<in> (Cons True) ` {l. length l = n}.
              pmf (w_list_pmf n) (tl a) 
*\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) (a ! i)) +
        (1 + \<epsilon>) / 2 *
           (\<Sum>a\<in> (Cons False) ` {l. length l = n}.
              pmf (w_list_pmf n) (tl a) 
*\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) (a ! i)) =
(1 - \<epsilon>) / 2 *
           (\<Sum>a\<in> (Cons True) ` {l. length l = n}.
              pmf (w_list_pmf n) (tl a) 
*\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) ((tl a) ! k)) +
        (1 + \<epsilon>) / 2 *
           (\<Sum>a\<in> (Cons False) ` {l. length l = n}.
              pmf (w_list_pmf n) (tl a) 
*\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) ((tl a) ! k))"
    by (smt Suc imageE list.sel(3) mult_compare_simps(14) nth_Cons_Suc sum.cong)
  also have t23: "... = (1 - \<epsilon>) / 2 *
           (\<Sum>a\<in> {l. length l = n}.
              pmf (w_list_pmf n) a 
*\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) (a ! k)) +
        (1 + \<epsilon>) / 2 *
           (\<Sum>a\<in> {l. length l = n}.
              pmf (w_list_pmf n) a 
*\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) (a ! k))"
    by (simp add: sum.reindex)
also have t24:"... = (1 - \<epsilon>) / 2 *
           (-\<epsilon>) +
        (1 + \<epsilon>) / 2 *
           (-\<epsilon>)"
proof -
  have "k < n"
    using Suc Suc.prems by blast
  then show ?thesis
    using False Suc.IH set_pmf_w_list_ep_ne_1 by auto 
qed
  finally have "... = - \<epsilon>"
    by (metis Groups.add_ac(2) honest_slot real_scaleR_def scaleR_collapse)
  then show ?thesis
    using t22 t23 t24 by linarith
qed
  thus ?thesis
    using h1 h2 st by linarith
qed
  thus ?thesis
    using False set_pmf_w_list_ep_ne_1 by presburger
qed
  thus ?case
    using "0" "2" "3" \<open>(\<Sum>a\<in>set_pmf (local.w_list_pmf (Suc n)). measure_pmf.expectation (local.w_list_pmf n) (\<lambda>y. pmf (return_pmf (True # y)) a * pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) True + pmf (return_pmf (False # y)) a * pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) False) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) (a ! i)) = (\<Sum>a\<in>set_pmf (local.w_list_pmf (Suc n)). measure_pmf.expectation (local.w_list_pmf n) (\<lambda>y. pmf (return_pmf (True # y)) a * ((1 - \<epsilon>) / 2) + pmf (return_pmf (False # y)) a * (1 - (1 - \<epsilon>) / 2)) *\<^sub>R (real_of_int \<circ> (\<lambda>b. (- 1) ^ (if b then 0 else 1))) (a ! i))\<close> f3 honest_slot by presburger
qed


lemma nth_w_expectation_eq_minus_epsilon_aux4: 
  assumes "a \<in> set_pmf (w_list_pmf n)" "i > 0" "i \<le> n"
  shows "measure_pmf.expectation (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
        (\<lambda>x. return_pmf (x # a))) (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i)) 
        = (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l (i-1))) a"
  using assms 
proof -
  have 0:"measure_pmf.expectation (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
        (\<lambda>x. return_pmf (x # a))) (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i)) 
        =
(\<Sum>c\<in> {True, False}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) c *\<^sub>R measure_pmf.expectation ((\<lambda>x. return_pmf (x # a)) c)
 (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i)))"
  apply -
    apply (rule pmf_expectation_bind)
    apply auto
    done
  also have 1: "... = 
 pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) True 
*\<^sub>R measure_pmf.expectation (return_pmf (True # a))
 (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i)) + 
pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) False
*\<^sub>R measure_pmf.expectation (return_pmf (False # a))
 (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i))
" 
    using insertCI by auto
  also have 2:"... = 
 ((1 - \<epsilon>) / 2)
*\<^sub>R measure_pmf.expectation (return_pmf (True # a))
 (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i)) + 
(1 - ((1 - \<epsilon>) / 2)) 
*\<^sub>R measure_pmf.expectation (return_pmf (False # a))
 (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i))
"
    using ep_gt_0 ep_le_1 by auto
 also have 3:"... = 
 ((1 - \<epsilon>) / 2)
*\<^sub>R (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i)) (True # a) + 
(1 - ((1 - \<epsilon>) / 2)) 
*\<^sub>R  
 (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i)) (False # a)
"
   by auto
  finally have "... = (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l (i-1))) a"
  proof -
    obtain j where j:"i = Suc j"
      using Suc_pred' assms(2) by blast
    hence "(\<lambda>l. nth l i) (False # a) = a ! j"
      by simp
    hence F:"(real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i)) (False # a) =
        (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l (i-1))) a"
      by (simp add: \<open>i = Suc j\<close>)
    hence "(\<lambda>l. nth l i) (True # a) = a ! j"
      using j by simp
    hence "(real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i)) (True # a) =
        (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l (i-1))) a"
      by (simp add: \<open>i = Suc j\<close>)
    thus ?thesis
      by (smt F scaleR_collapse)
  qed
  thus ?thesis
    using "0" "1" "2" "3" by linarith
qed

lemma nth_w_expectation_eq_minus_epsilon:
  assumes "i < n"
  shows "measure_pmf.expectation (w_list_pmf n) 
    (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i)) = -\<epsilon>"
  using assms
proof (induction n)
  case 0
  then show ?case by simp
next
  case (Suc n)
  note Sucn = this
    thm Sucn 
  have "w_list_pmf (Suc n) =
   bind_pmf (w_list_pmf n) 
    (\<lambda>xs. bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (\<lambda>x.
    return_pmf (x # xs)))"
    by simp
  have st:"measure_pmf.expectation (w_list_pmf (Suc n)) 
      (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i))
    = measure_pmf.expectation (bind_pmf (w_list_pmf n) 
    (\<lambda>xs. bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (\<lambda>x.
    return_pmf (x # xs)))) 
      (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i))"    
    by simp
also have 1:"...
    = (\<Sum>a\<in> set_pmf (w_list_pmf n). 
      pmf (w_list_pmf n) a *\<^sub>R measure_pmf.expectation (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
(\<lambda>x. return_pmf (x # a))) (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i)))"
  apply -
  apply (rule pmf_expectation_bind)
  apply (simp add: finite_set_w_list_pmf)
  apply (simp add: nth_w_expectation_eq_minus_epsilon_aux)
  by simp
  finally have 2:"... = - \<epsilon>"
  proof (cases i)
    case 0
    hence "\<And>a. a \<in> set_pmf (w_list_pmf n) \<Longrightarrow> measure_pmf.expectation (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
(\<lambda>x. return_pmf (x # a))) (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l 0)) = 
(\<Sum>c\<in> {True, False}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) c *\<^sub>R measure_pmf.expectation ((\<lambda>x. return_pmf (x # a)) c)
 (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l 0)))"
      apply -
      apply (rule pmf_expectation_bind)
      by auto
hence "\<And>a. a \<in> set_pmf (w_list_pmf n) \<Longrightarrow> measure_pmf.expectation (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
(\<lambda>x. return_pmf (x # a))) (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l 0))
= - \<epsilon> "
  using ep_gt_0 ep_le_1 by auto  
  hence "(\<Sum>a\<in> set_pmf (w_list_pmf n). 
      pmf (w_list_pmf n) a *\<^sub>R measure_pmf.expectation (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
(\<lambda>x. return_pmf (x # a))) (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l 0))) =
(\<Sum>a\<in> set_pmf (w_list_pmf n). 
      pmf (w_list_pmf n) a *\<^sub>R (- \<epsilon>))"
    by simp
  also have "... = (\<Sum>a\<in> set_pmf (w_list_pmf n). 
      pmf (w_list_pmf n) a) *\<^sub>R (- \<epsilon>)"
    by (metis scaleR_left.sum)
  thus ?thesis
    using "0" calculation sum_set_w_list_eq_1 by auto
next
  case (Suc j)
  hence "\<And>a. a \<in> set_pmf (w_list_pmf n) \<Longrightarrow> measure_pmf.expectation (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
(\<lambda>x. return_pmf (x # a))) (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i)) = 
(\<Sum>c\<in> {True, False}. pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) c *\<^sub>R 
measure_pmf.expectation ((\<lambda>x. return_pmf (x # a)) c)
 (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i)))"
    apply -
    apply (rule pmf_expectation_bind)
    by auto
hence "\<And>a. a \<in> set_pmf (w_list_pmf n) \<Longrightarrow> measure_pmf.expectation (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
(\<lambda>x. return_pmf (x # a))) (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i))
= (pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) True *\<^sub>R 
measure_pmf.expectation ((\<lambda>x. return_pmf (x # a)) True)
 (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i)))
+
(pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) False *\<^sub>R 
measure_pmf.expectation ((\<lambda>x. return_pmf (x # a)) False)
 (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i)))"
  by (smt bind_pmf_cong empty_iff finite.emptyI finite.insertI insert_absorb singleton_insert_inj_eq sum.empty sum.insert)
hence "\<And>a. a \<in> set_pmf (w_list_pmf n) \<Longrightarrow> measure_pmf.expectation (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
(\<lambda>x. return_pmf (x # a))) (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i))
= ((1 - \<epsilon>) / 2) *\<^sub>R 
measure_pmf.expectation ((\<lambda>x. return_pmf (x # a)) True)
 (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i))
+
(1 - (1 - \<epsilon>) / 2) *\<^sub>R 
measure_pmf.expectation ((\<lambda>x. return_pmf (x # a)) False)
 (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i))"
  using ep_gt_0 ep_le_1 pmf_bernoulli_True pmf_bernoulli_False
  by auto
hence "\<And>a. a \<in> set_pmf (w_list_pmf n) \<Longrightarrow> measure_pmf.expectation (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
(\<lambda>x. return_pmf (x # a))) (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i))
= (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! j)"
  using local.Suc
  by (smt expectation_return_pmf nth_Cons_Suc o_apply scaleR_collapse scaleR_one)
  hence "(\<Sum>a\<in> set_pmf (w_list_pmf n). 
      pmf (w_list_pmf n) a *\<^sub>R measure_pmf.expectation (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
(\<lambda>x. return_pmf (x # a))) (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i))) =
(\<Sum>a\<in> set_pmf (w_list_pmf n). 
      pmf (w_list_pmf n) a *\<^sub>R  (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1))) (a ! j))"
    by auto
  thus "(\<Sum>a\<in> set_pmf (w_list_pmf n). 
      pmf (w_list_pmf n) a *\<^sub>R measure_pmf.expectation (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
(\<lambda>x. return_pmf (x # a))) (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i))) =
      - \<epsilon>"
  using Sucn
  using Suc nth_w_expectation_eq_minus_epsilon_aux3 by auto
qed

(*"measure_pmf.expectation (w_list_pmf n) 
    (real_of_int \<circ> (\<lambda>b. (-1) ^ (if b then 0 else 1)) \<circ> (\<lambda>l. nth l i)) = -\<epsilon>"*)
  thus ?case
    using "1" diff_Suc_1 w_list_pmf.simps(2) by presburger
qed

lemma proposition_4_18_mod_aux:
"margin_s w \<ge> 0 \<Longrightarrow> forkable w "
proof -
  have 0:"margin_s w \<ge> 0 \<longrightarrow> Max ((\<lambda>f. margin f w) ` (closed_forks_set w)) \<ge> 0"
    by (simp add: margin_s_def)
    obtain f where "closed_fork f w"
      using exist_closed_fork by auto
    have 1:"closed_forks_set w \<noteq> {} "
      using theorem_4_19_exist_part by auto
    have 2:"finite ((\<lambda>f. margin f w) ` (closed_forks_set w))"
      by (simp add: finite_set_of_margin)
    then obtain ma where "ma = margin_s w"
      by simp
    hence "ma = Max ((\<lambda>f. margin f w) ` (closed_forks_set w))"
      using margin_s_def by blast
    hence "ma \<in> ((\<lambda>f. margin f w) ` (closed_forks_set w))"
      by (simp add: \<open>closed_forks_set w \<noteq> {}\<close> \<open>finite ((\<lambda>f. margin f w) ` closed_forks_set w)\<close>)
    hence "margin_s w \<ge> 0 \<longrightarrow> 
  (\<exists>x \<in> ((\<lambda>f. margin f w) ` (closed_forks_set w)). x \<ge> 0)"
      using \<open>ma = margin_s w\<close> by auto
    thus "margin_s w \<ge> 0 \<Longrightarrow> forkable w"
    using exist_closed_fork_for_margin non_neg_margin_imp_forkable by force
qed

lemma proposition_4_18_mod_aux_2:
"forkable w \<Longrightarrow> margin_s w \<ge> 0"
proof -
    obtain f where "closed_fork f w"
      using exist_closed_fork by auto
    have 1:"closed_forks_set w \<noteq> {} "
      using theorem_4_19_exist_part by auto
    have 2:"finite ((\<lambda>f. margin f w) ` (closed_forks_set w))"
      by (simp add: finite_set_of_margin)
    then obtain ma where "ma = margin_s w"
      by simp
    hence "ma = Max ((\<lambda>f. margin f w) ` (closed_forks_set w))"
      using margin_s_def by blast
    hence "ma \<in> ((\<lambda>f. margin f w) ` (closed_forks_set w))"
      by (simp add: \<open>closed_forks_set w \<noteq> {}\<close> \<open>finite ((\<lambda>f. margin f w) ` closed_forks_set w)\<close>)
     have "forkable w \<longrightarrow> (\<exists>F. closed_fork F w \<and> margin F w \<ge> 0)"
    using proposition_4_18 by blast
hence "forkable w \<longrightarrow> (\<exists>F \<in> (closed_forks_set w). margin F w \<ge> 0)"
  using closed_forks_set_def by blast
hence 3:"forkable w \<longrightarrow> (\<exists>x \<in> ((\<lambda>f. margin f w) ` (closed_forks_set w)). x \<ge> 0)"
  using closed_forks_set_def by blast
  have "\<forall>x \<in> ((\<lambda>f. margin f w) ` (closed_forks_set w)). Max ((\<lambda>f. margin f w) ` (closed_forks_set w)) \<ge> x"
    using 1 2 by simp
  hence "forkable w \<longrightarrow>  Max ((\<lambda>f. margin f w) ` (closed_forks_set w)) \<ge> 0"
    using 3
    by auto
  thus "forkable w \<Longrightarrow> margin_s w \<ge> 0"
    using margin_s_def by simp  
qed

lemma proposition_4_18_mod:
  "forkable w \<longleftrightarrow> margin_s w \<ge> 0"
  using proposition_4_18_mod_aux proposition_4_18_mod_aux_2 by blast

lemma "\<P>(w in w_list_pmf n. forkable (rev w)) = \<P>(w in w_list_pmf n. rev_\<mu> w \<ge> 0)"
proof - 
  have "\<forall>w. forkable (rev w) \<longleftrightarrow> margin_s (rev w) \<ge> 0"
    using proposition_4_18_mod by auto
  hence "\<forall>w. forkable (rev w) \<longleftrightarrow> snd (m (rev w)) \<ge> 0"
    using m_def by auto
  hence "\<forall>w. forkable (rev w) \<longleftrightarrow> snd (rev_m w) \<ge> 0"
    using rev_m_m_rev by auto
  hence "\<forall>w. forkable (rev w) \<longleftrightarrow> rev_\<mu> w \<ge> 0"
    using rev_\<mu>_def
    by auto
  thus ?thesis by auto
qed

lemma "rev_\<mu> w \<ge> 0 \<longleftrightarrow> rev_\<mu>_bar w = 0"
  using rev_\<mu>_bar_def by auto

lemma "\<P>(w in w_list_pmf n. rev_\<mu> w \<ge> 0) = \<P>(w in w_list_pmf n. rev_\<mu>_bar w = 0)"
  by (simp add: min_def rev_\<mu>_bar_def)

definition \<Phi>_n :: "nat \<Rightarrow> bool list \<Rightarrow> real" where
  "\<Phi>_n n l= (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (drop (size l - n) l))"

(*then we focus on event that rev_\<mu>_bar w = 0*)

lemma delta_expectation_aux:
  shows "measure_pmf.expectation (w_list_pmf (Suc n))
  (\<Phi>_n (Suc n) - \<Phi>_n n) \<le> -\<epsilon>"
proof -
  have "measure_pmf.expectation (w_list_pmf (Suc n))
  (\<Phi>_n (Suc n) - \<Phi>_n n) = 
 measure_pmf.expectation (bind_pmf (w_list_pmf n) 
    (\<lambda>xs. bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (\<lambda>x.
    return_pmf (x # xs)))) (\<Phi>_n (Suc n) - \<Phi>_n n)"
    by simp
  hence "measure_pmf.expectation (w_list_pmf (Suc n))
  (\<Phi>_n (Suc n) - \<Phi>_n n) = 
 measure_pmf.expectation (bind_pmf (w_list_pmf n) 
    (\<lambda>xs. bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (\<lambda>x.
    return_pmf (x # xs)))) (\<Phi>_n (Suc n) - \<Phi>_n n)"
    by simp
  also have "... = (\<Sum>a\<in>set_pmf (w_list_pmf n). 
        pmf (w_list_pmf n) a *\<^sub>R 
      measure_pmf.expectation ((\<lambda>xs. bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
    (\<lambda>x. return_pmf (x # xs))) a) (\<Phi>_n (Suc n) - \<Phi>_n n))"
    apply -
      apply (rule pmf_expectation_bind)
    apply (simp add: finite_set_w_list_pmf)
     apply auto[1]
    by simp
  also have "... = (\<Sum>a\<in>set_pmf (w_list_pmf n). 
        pmf (w_list_pmf n) a *\<^sub>R 
      (\<Sum>b \<in> set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). 
      pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) b *\<^sub>R 
      measure_pmf.expectation ((\<lambda>x. return_pmf (x # a)) b) 
      (\<Phi>_n (Suc n) - \<Phi>_n n)))"
  proof - 
    have "\<And>a. a\<in>set_pmf (w_list_pmf n) \<Longrightarrow> 
    measure_pmf.expectation (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
    (\<lambda>x. return_pmf (x # a))) (\<Phi>_n (Suc n) - \<Phi>_n n) =
    (\<Sum>b \<in> set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). 
      pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) b *\<^sub>R 
      measure_pmf.expectation ((\<lambda>x. return_pmf (x # a)) b) 
      (\<Phi>_n (Suc n) - \<Phi>_n n))"
      apply -
       apply (rule pmf_expectation_bind)
      apply simp
       apply simp
      by simp
    thus ?thesis
      by auto
  qed
  also have "... = (\<Sum>a\<in>set_pmf (w_list_pmf n). 
        pmf (w_list_pmf n) a *\<^sub>R 
      (\<Sum>b \<in> set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). 
      pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) b *\<^sub>R 
     (\<Phi>_n (Suc n) (b # a) - \<Phi>_n n a)))"
  proof - 
    have "\<And>a b. size a = n \<Longrightarrow> \<Phi>_n n (b#a) = \<Phi>_n n a"
      by (metis One_nat_def Suc_eq_plus1_left \<Phi>_n_def add_diff_cancel_right' cancel_comm_monoid_add_class.diff_cancel drop_Suc_Cons length_Cons)
    thus ?thesis
      using set_pmf_w_list_ep_eq_1 set_pmf_w_list_ep_ne_1 by fastforce
  qed
  also have "... = (\<Sum>a\<in>set_pmf (w_list_pmf n). 
        pmf (w_list_pmf n) a *\<^sub>R 
      (\<Sum>b \<in> set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). 
      pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) b *\<^sub>R 
((\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (drop (size (b#a) - (Suc n)) (b#a))) -  
(\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (drop (size a - n) a)))))"
    by (simp add: \<Phi>_n_def)
also have "... = (\<Sum>a\<in>set_pmf (w_list_pmf n). 
        pmf (w_list_pmf n) a *\<^sub>R 
      (\<Sum>b \<in> set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). 
      pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) b *\<^sub>R 
((\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (b#a)) -  
(\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m a))))"
proof - 
  have "\<And>a b. a\<in>set_pmf (w_list_pmf n) \<Longrightarrow> size a = n \<and> size (b#a) = Suc n"
    using set_pmf_w_list_ep_eq_1 set_pmf_w_list_ep_ne_1 by auto
  hence "\<And>a b. a\<in>set_pmf (w_list_pmf n) \<Longrightarrow> drop (size a - n) a = a \<and> drop (size (b#a) - Suc n) (b#a) = b#a"
    by simp
  thus ?thesis
    by simp
qed
  finally have "... \<le> - \<epsilon>"
  proof -
    have "\<And>a. a\<in>set_pmf (w_list_pmf n) \<Longrightarrow>  (\<Sum>b \<in> set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)).
      pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) b *\<^sub>R 
((\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (b#a)) -  
(\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m a))) \<le> -\<epsilon>"
    proof -
      fix a
      assume "a \<in>set_pmf (w_list_pmf n)"
(*
proof by 4 cases of expectation
 but how?
not by induction obviously
separating set_pmf (w_list_pmf k) and (\<Sum> b\<in> (bernoulli_pmf ((1 - \<epsilon>) / 2))
in 4 cases 
lambda > 0, mu < 0
lambda > 0, mu \<ge> 0
lambda = 0, mu < 0
lambda = 0, mu = 0
(all for wi)


*)


      thus "(\<Sum>b \<in> set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)).
      pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) b *\<^sub>R 
((\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m (b#a)) -  
(\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (rev_m a))) \<le> -\<epsilon>"
      

lemma drop_expectation_eq:
  fixes g :: "bool list \<Rightarrow> int"
  assumes "i \<le> k" "k + x =  n"
  shows "measure_pmf.expectation (w_list_pmf n)
  (g \<circ> (drop (n-i))) = 
    measure_pmf.expectation (w_list_pmf k)
  (g \<circ> (drop (k-i)))"
  using assms
proof (induction x arbitrary : n k)
  case 0
  then show ?case
    by simp
next
  case (Suc x)
  hence "measure_pmf.expectation (w_list_pmf (n-1))
  (g \<circ> (drop (n-1-i))) = 
    measure_pmf.expectation (w_list_pmf k)
  (g \<circ> (drop (k-i)))"
    by force
  have ni:"0 < n - i"
    using Suc.prems(1) Suc.prems(2) by linarith
  obtain nn where "n = Suc nn"
    using Suc.prems(2) add_Suc_right by blast
  have "measure_pmf.expectation (w_list_pmf n) (g \<circ> (drop (n-i))) = 
  measure_pmf.expectation (bind_pmf (w_list_pmf nn) 
    (\<lambda>xs. bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (\<lambda>x.
    return_pmf (x # xs))))  (g \<circ> (drop (n-i)))"
    using \<open>n = Suc nn\<close> by auto
  also have "... = (\<Sum>a\<in>set_pmf (w_list_pmf nn). 
        pmf (w_list_pmf nn) a *\<^sub>R 
      measure_pmf.expectation ((\<lambda>xs. bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (\<lambda>x.
    return_pmf (x # xs))) a) (g \<circ> (drop (n-i))))"
    apply -
      apply (rule pmf_expectation_bind)
    apply (simp add: finite_set_w_list_pmf)
    apply auto[1]
    by simp
  also have "... = (\<Sum>a\<in>set_pmf (w_list_pmf nn). 
        pmf (w_list_pmf nn) a *\<^sub>R (\<Sum>b\<in> set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). 
pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
          b *\<^sub>R 
measure_pmf.expectation 
((\<lambda>x. return_pmf (x # a)) b) (g \<circ> (drop (n-i)))))"
  proof -
    have "\<And>a. measure_pmf.expectation (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
            (\<lambda>x. return_pmf (x # a))) (g \<circ> (drop (n-i))) = (\<Sum>b \<in> set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
          b *\<^sub>R measure_pmf.expectation ((\<lambda>x. return_pmf (x # a)) b) (g \<circ> (drop (n-i))))"
      apply -
      apply (rule pmf_expectation_bind)
      apply simp
      apply simp
      by auto
    thus ?thesis
      by simp
  qed 
also have "... = (\<Sum>a\<in>set_pmf (w_list_pmf nn). 
        pmf (w_list_pmf nn) a *\<^sub>R (\<Sum>b\<in> set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). 
pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
          b *\<^sub>R (g (drop (1 + n-i-1) (b#a)))))"
  using expectation_return_pmf ni by simp
also have "... = (\<Sum>a\<in>set_pmf (w_list_pmf nn). 
        pmf (w_list_pmf nn) a *\<^sub>R (\<Sum>b\<in> set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). 
pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
          b *\<^sub>R (g (drop (Suc (n-i-1)) (b#a)))))"
proof -
  have "1+ n - i - 1 = 1 + (n-i-1)"
    using cancel_ab_semigroup_add_class.diff_right_commute ni by auto
  thus ?thesis
    using Suc_eq_plus1_left by presburger
qed
  also have "... = (\<Sum>a\<in>set_pmf (w_list_pmf nn). 
        pmf (w_list_pmf nn) a *\<^sub>R (\<Sum>b\<in> set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). 
pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
          b *\<^sub>R (g \<circ> (drop (n-i-1))) a))"
    by simp
also have "... = (\<Sum>a\<in>set_pmf (w_list_pmf nn). 
        pmf (w_list_pmf nn) a *\<^sub>R (\<Sum>b\<in> set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). 
pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
          b) *\<^sub>R (g \<circ> (drop (n-i-1))) a)"
  by (metis (no_types) scaleR_left.sum)
 also have "... = (\<Sum>a\<in>set_pmf (w_list_pmf nn). 
        pmf (w_list_pmf nn) a *\<^sub>R (g \<circ> (drop (n-i-1))) a)"
   by (simp add: sum_pmf_eq_1)
  ultimately have "measure_pmf.expectation (w_list_pmf n) (g \<circ> (drop (n-i))) = (\<Sum>a\<in>set_pmf (w_list_pmf nn). 
        pmf (w_list_pmf nn) a *\<^sub>R (g \<circ> (drop (n-i-1))) a)"
    by simp
  hence "measure_pmf.expectation (w_list_pmf n) (g \<circ> (drop (n-i))) = 
          measure_pmf.expectation (w_list_pmf nn) (g \<circ> (drop (n-1-i)))"
  proof - 
      have "(\<Sum>bs\<in>set_pmf (local.w_list_pmf nn). pmf (local.w_list_pmf nn) bs *\<^sub>R real_of_int ((g \<circ> drop (n - 1 - i)) bs)) = measure_pmf.expectation (local.w_list_pmf nn) (\<lambda>bs. real_of_int ((g \<circ> drop (n - 1 - i)) bs))"
        by (metis (no_types) finite_set_w_list_pmf integral_measure_pmf)
      then show ?thesis
        using \<open>measure_pmf.expectation (local.w_list_pmf n) (\<lambda>x. real_of_int ((g \<circ> drop (n - i)) x)) = (\<Sum>a\<in>set_pmf (local.w_list_pmf nn). pmf (local.w_list_pmf nn) a *\<^sub>R real_of_int ((g \<circ> drop (n - i - 1)) a))\<close> by auto        
    qed
    thus ?case
      using \<open>measure_pmf.expectation (local.w_list_pmf (n - 1)) (\<lambda>x. real_of_int ((g \<circ> drop (n - 1 - i)) x)) = measure_pmf.expectation (local.w_list_pmf k) (\<lambda>x. real_of_int ((g \<circ> drop (k - i)) x))\<close> \<open>n = Suc nn\<close> by force
qed


lemma delta_expectation:
  assumes "i < n"
  shows "measure_pmf.expectation (w_list_pmf n)
  (\<Phi>_n (Suc i) - \<Phi>_n i) \<le> -\<epsilon>"
proof -
  have "n > 0"
    using assms by simp
  then obtain k where nk:"n = Suc k"
    using gr0_conv_Suc by auto
  hence ik:"i \<le> k"
    using assms by auto
  hence "measure_pmf.expectation (w_list_pmf (Suc k)) 
      (\<Phi>_n (Suc i) - \<Phi>_n i)
    = measure_pmf.expectation (bind_pmf (w_list_pmf k) 
    (\<lambda>xs. bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (\<lambda>x.
    return_pmf (x # xs)))) 
      (\<Phi>_n (Suc i) - \<Phi>_n i)"
    by simp
  hence "measure_pmf.expectation (w_list_pmf (Suc k)) 
      (\<Phi>_n (Suc i) - \<Phi>_n i)
    = measure_pmf.expectation (bind_pmf (w_list_pmf k) 
    (\<lambda>xs. bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (\<lambda>x.
    return_pmf (x # xs)))) 
      (\<Phi>_n (Suc i) - \<Phi>_n i)"
    by simp
  also have "... = (\<Sum>a\<in>set_pmf (w_list_pmf k). 
        pmf (w_list_pmf k) a *\<^sub>R measure_pmf.expectation ((\<lambda>xs. bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) (\<lambda>x.
    return_pmf (x # xs))) a) (\<Phi>_n (Suc i) - \<Phi>_n i))"
    apply -
    apply (rule pmf_expectation_bind)
    apply (simp add: finite_set_w_list_pmf)
    using nth_w_expectation_eq_minus_epsilon_aux apply blast
    by simp
  also have "... = (\<Sum>a\<in>set_pmf (w_list_pmf k). 
        pmf (w_list_pmf k) a *\<^sub>R measure_pmf.expectation (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
            (\<lambda>x. return_pmf (x # a))) (\<Phi>_n (Suc i) - \<Phi>_n i))"
    by simp
  also have " ... = (\<Sum>a\<in>set_pmf (w_list_pmf k). 
        pmf (w_list_pmf k) a *\<^sub>R (\<Sum>b\<in> set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
          b *\<^sub>R measure_pmf.expectation ((\<lambda>x. return_pmf (x # a)) b) (\<Phi>_n (Suc i) - \<Phi>_n i)))"
  proof -
    have "\<And>a. measure_pmf.expectation (bind_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
            (\<lambda>x. return_pmf (x # a))) (\<Phi>_n (Suc i) - \<Phi>_n i) = (\<Sum>b \<in> set_pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)). pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) 
          b *\<^sub>R measure_pmf.expectation ((\<lambda>x. return_pmf (x # a)) b) (\<Phi>_n (Suc i) - \<Phi>_n i))"
      apply -
      apply (rule pmf_expectation_bind)
      apply simp
      apply simp
      by auto
    thus ?thesis
      by simp
  qed
  hence "... = (\<Sum>a\<in>set_pmf (w_list_pmf k). 
        pmf (w_list_pmf k) a *\<^sub>R (\<Sum> b\<in> (bernoulli_pmf ((1 - \<epsilon>) / 2)). 
        pmf (bernoulli_pmf ((1 - \<epsilon>) / 2)) b 
          *\<^sub>R (\<Phi>_n (Suc i) (b # a) - \<Phi>_n i a)))" 
  proof -
    have "\<And>a. a\<in>set_pmf (w_list_pmf k) \<Longrightarrow> size a \<ge> i" using ik
    have "\<And>a b. a\<in>set_pmf (w_list_pmf k) \<Longrightarrow>  \<Phi>_n i (b#a) = \<Phi>_n i a"


(*
proof by 4 cases of expectation
 but how?
not by induction obviously
separating set_pmf (w_list_pmf k) and (\<Sum> b\<in> (bernoulli_pmf ((1 - \<epsilon>) / 2))
in 4 cases 
lambda > 0, mu < 0
lambda > 0, mu \<ge> 0
lambda = 0, mu < 0
lambda = 0, mu = 0
(all for wi)


*)
    have ""
  
(*lemma pmf_expectation_bind:
  fixes p :: "'a pmf" and f :: "'a \<Rightarrow> 'b pmf"
    and  h :: "'b \<Rightarrow> 'c::{banach, second_countable_topology}"
  assumes "finite A" "\<And>x. x \<in> A \<Longrightarrow> finite (set_pmf (f x))" "set_pmf p \<subseteq> A"
  shows "measure_pmf.expectation (p \<bind> f) h =
           (\<Sum>a\<in>A. pmf p a *\<^sub>R measure_pmf.expectation (f a) h)"*)
  then show ?case sorry
qed

(*4 cases*)
(*
definition \<Phi>_n_pmf :: "nat \<Rightarrow> real pmf" where
  "\<Phi>_n_pmf n = map_pmf 
    (\<lambda>p. real_of_int (fst p) + \<alpha> * (real_of_int (min 0 (snd p)))) (m_n_pmf n)"
*)
definition \<Phi> :: "bool list \<Rightarrow> real" where
  "\<Phi> l = real_of_int (rev_\<rho> l) + \<alpha> * (real_of_int (min 0 (rev_\<mu> l)))"

lemma 
  "\<alpha> * (min 0 (rev_\<mu> l)) \<le> rev_\<rho> l + \<alpha> * rev_\<mu>_bar l"
  sorry

definition \<Delta>_n_pmf where "\<Delta>_n_pmf n =
  bind_pmf (w_list_pmf (n + 1)) (\<lambda>xs.
  return_pmf (\<Phi> (*pmf*) xs - \<Phi> (tl xs)))"
(*trying expectation*)
(*lemma "prob_space.expectation (w_list_pmf n) (\<lambda>l. real (count (mset l) True)) < -\<epsilon>"
  sorry
*)
(*expectation:: ??'a measure \<Rightarrow> (??'a \<Rightarrow> ??'b) \<Rightarrow> ??'b*)

(*\<Delta> in the paper*)
lemma 
  assumes "n > 0"
  shows "prob_space.expectation (\<Delta>_n_pmf n) (id) < -\<epsilon>"
proof -
  have "prob_space.expectation (\<Delta>_n_pmf n) (id) = "
(*then we proof this in 4 cases
1) \<lambda> > 0, \<mu> < 0
2) \<lambda> > 0, \<mu> \<ge> 0
3) \<lambda> = 0, \<mu> < 0
4) \<lambda> = 0. \<mu> = 0
*)


(*sum them up n times*)
lemma "prob_space.expectation (w_list_pmf n) (\<lambda>l. \<Phi> l) \<le> - (real n * \<epsilon>)"
  sorry
(*because 
\<Phi> l =
\<Phi> l - \<Phi> (tl l) + 
\<Phi> (tl l) - \<Phi> (tl (tl l)) +
.
.
.
\<Phi> [last l] - \<Phi> [] 

then 
E[\<Phi> l] = E [\<Sum> i < n. \<Phi> (drop i l) - \<Phi> (drop (i + 1) l)]
= \<Sum> i < n. E [\<Phi> (drop i l) - \<Phi> (drop (i + 1) l)]
\<le> \<Sum> i < n. E [\<Delta>_n_pmf (i+1)]
\<le> -\<epsilon>
*)

(*still research on conditioned expectation? 
E[mod_\<Phi> n+1| mod_\<Phi> n .... mod_\<Phi> 1] = E[mod_\<Phi> n+1| W_n_pmf ...] \<le> mod_\<Phi> ...*)
definition mod_\<Delta>_n_pmf where 
  "mod_\<Delta>_n_pmf n = map_pmf (\<lambda>x. x + \<epsilon>) (\<Delta>_n_pmf n)"

lemma "\<P>(X in mod_\<Delta>_n_pmf n. X \<le> 1 + \<alpha> + \<epsilon> \<and> X \<ge> - (1 + \<alpha>) + \<epsilon>) = 1"
  sorry

definition mod_\<Phi>_n_pmf :: "nat \<Rightarrow> real pmf" where
  "mod_\<Phi>_n_pmf n = map_pmf (\<lambda>x. x + n * \<epsilon>) (\<Phi>_n_pmf n)"

definition mod_\<Phi> :: "bool list \<Rightarrow> real" where
  "mod_\<Phi> l =  \<Phi> l + (size l) * \<epsilon>"

lemma "\<P>(w in w_list_pmf n. rev_\<mu>_bar w = 0) \<le> \<P>(X in \<Phi>_n_pmf n. X \<ge> 0)"
  sorry

lemma "\<P>(X in \<Phi>_n_pmf n. X \<ge> 0) = \<P>(X in mod_\<Phi>_n_pmf n. X \<ge> \<epsilon> * n)"
  sorry

(*then apply hoeffding*)

lemma "\<P>(w in w_list_pmf n. rev_\<mu> w \<ge> 0) = exp ((-2) * \<epsilon> ^ 4 / (1 + 35 * \<epsilon>) * n)"
  sorry
*)